

PROGRAM CorrectFlux

IMPLICIT NONE
INTEGER, PARAMETER :: dp = SELECTED_REAL_KIND(P=15)
REAL (KIND=dp), PARAMETER :: CLUM = 2.99792458e8_dp          ! Celerity of light c (m s-1)
REAL (KIND=dp), PARAMETER :: PI  = 3.1415926535897932384_dp  ! Pi
CHARACTER(LEN=5) :: esp, poubstr
CHARACTER(LEN=7) :: quant1up, quant1lo, quant2up, quant2lo
REAL (KIND=dp) :: freqGhz, mu, emist, emisl, emisa
INTEGER :: io, nobs
REAL (KIND=dp) :: freq, sdisk, beam_PACS, bff_PACS, obs, obs1, obs2, FWHM_HIFI, beam_HIFI, bff_HIFI, &
   FWHM_APET, beam_APET, bff_APET
CHARACTER(LEN=5) :: instru, instru1, instru2
CHARACTER(LEN=11) :: freqstr

sdisk = PI*(0.83_dp/2.0_dp)*(0.4_dp/2.0_dp)
beam_PACS = 9.4_dp*9.4_dp
bff_PACS = sdisk/beam_PACS

OPEN(28, file = "./lines_light", action = 'read')
OPEN(56, file = "./lines_light_corr", action = 'write', status = 'replace')

DO
   ! Read in '_light' file.
   READ(28, '(A5,X,A7,X,A7,X,A3,X,A7,X,A7,X,A1,X,f11.2,X,0pf8.4,X,A1,X,1pe12.5,X,e12.5,X,e12.5)',IOSTAT=io) &
      esp, quant1up, quant1lo, poubstr, quant2up, quant2lo, poubstr, freqGhz, mu, poubstr, emist, emisl, emisa
   IF (io /= 0) THEN ! Check if there is still something to read.
      EXIT
   ENDIF
   freq = freqGhz * 1.0e9_dp

   nobs = 1
   SELECT CASE (esp)
      CASE ("co")
         write(freqstr,'(f11.2)') freqGhz
         freqstr = TRIM(ADJUSTL(freqstr))
         SELECT CASE (freqstr)
            CASE ('1726.61', '1956.02', '2185.14')
               ! PACS observation
               instru = "PACS:"
               obs = emisa * bff_PACS
            CASE ('1151.99')
               ! HIFI observation
               instru = "HIFI:"
               FWHM_HIFI = 19.0_dp*1120.0_dp/freqGhz
               beam_HIFI = PI * (FWHM_HIFI/2.0_dp)**2
               bff_HIFI = sdisk/beam_HIFI
               obs = emisa * bff_HIFI
            CASE ('345.80', '461.03', '691.46')
               ! APE T-12m observation
               instru = "APET:"      
               SELECT CASE (freqstr)
                  CASE ('345.80')
                     FWHM_APET = 17.8_dp
                  CASE ('461.03')
                     FWHM_APET = 13.1_dp
                  CASE ('691.46')
                     FWHM_APET = 8.7_dp
               END SELECT
               beam_APET = PI * (FWHM_APET/2.0_dp)**2
               bff_APET = sdisk/beam_APET
               obs = emisa * bff_APET
            CASE ('806.65')
               nobs = 2
               ! HIFI observation
               instru1 = "HIFI:"
               FWHM_HIFI = 26.6_dp*800.0_dp/freqGhz
               beam_HIFI = PI * (FWHM_HIFI/2.0_dp)**2
               bff_HIFI = sdisk/beam_HIFI
               obs1 = emisa * bff_HIFI
               ! APE T-12m observation         
               instru2 = "APET:"
               FWHM_APET = 7.7_dp
               beam_APET = PI * (FWHM_APET/2.0_dp)**2
               bff_APET = sdisk/beam_APET
               obs2 = emisa * bff_APET
            CASE DEFAULT
               PRINT *, "Error, transition not known."
               STOP
         END SELECT
      CASE ("c*o")
         ! APE T-12m observation         
         instru = "APET:"
         FWHM_APET = 18.6_dp
         beam_APET = PI * (FWHM_APET/2.0_dp)**2
         bff_APET = sdisk/beam_APET
         obs = emisa * bff_APET
      CASE ("c+")
         ! HIFI observation
         instru = "HIFI:"
         FWHM_HIFI = 11.2_dp*1910.0_dp/freqGhz
         beam_HIFI = PI * (FWHM_HIFI/2.0_dp)**2
         bff_HIFI = sdisk/beam_HIFI
         obs = emisa * bff_HIFI
      CASE ("o")
         ! PACS observation
         instru = "PACS:"
         obs = emisa * bff_PACS
      CASE ("hco+")
         ! APE T-12m observation         
         instru = "APET:"
         FWHM_APET = 17.3_dp
         beam_APET = PI * (FWHM_APET/2.0_dp)**2
         bff_APET = sdisk/beam_APET
         obs = emisa * bff_APET
      CASE ("hcn")
         ! APE T-12m observation         
         instru = "APET:"
         FWHM_APET = 17.4_dp
         beam_APET = PI * (FWHM_APET/2.0_dp)**2
         bff_APET = sdisk/beam_APET
         obs = emisa * bff_APET
      CASE DEFAULT
         PRINT *, "Error, species not known."
         STOP
   ENDSELECT

   ! Write in '_light_corr' file.
   IF (nobs == 1) THEN
      WRITE(56,'(A5,X,A7,X,A7,X,A3,X,A7,X,A7,X,A1,X,f11.2,X,0pf8.4,X,A1,X,1pe12.5,X,e12.5,X,e12.5,X,A1,X,A5,X,e12.5)') &
         esp, quant1up, quant1lo,'-->', quant2up, quant2lo,'|', freqGhz, mu, '|', emist, emisl, emisa,'|',instru,obs
   ELSEIF (nobs == 2) THEN
      WRITE(56,&
         '(A5,X,A7,X,A7,X,A3,X,A7,X,A7,X,A1,X,f11.2,X,0pf8.4,X,A1,X,1pe12.5,X,e12.5,X,e12.5,X,A1,X,A5,X,e12.5,X,A1,X,A5,X,e12.5)') &
         esp, quant1up, quant1lo,'-->', quant2up, quant2lo,'|', freqGhz, mu, '|', emist, emisl, emisa,'|', &
         instru1,obs1,',',instru2,obs2
   ELSE
   ENDIF
ENDDO

CLOSE(28)
CLOSE(56)

END PROGRAM CorrectFlux
