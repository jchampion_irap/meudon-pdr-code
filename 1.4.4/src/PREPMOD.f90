!
! PREP.f90 code modified by Jason Champion (IRAP)
!
!--------+---------+---------+---------+---------+---------+---------+---------+
!
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

! JLB - 24 May 2008

!%%%%%%%%%%%%%%%
PROGRAM stdprep
!%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PREP_VAR
   USE PREP_LECTUR
   USE PREP_ABONDA
   USE PREP_COLDEN
   USE PREP_EMISSI
   USE PREP_CHOFRO
   USE PREP_AUTRE
   USE PREP_ANALYS
   USE PREP_SPHERE

   IMPLICIT NONE

   INTEGER :: iargc

   INTEGER :: ii, j
   INTEGER :: ifin, ifalse
   INTEGER :: ipoint, ii1

   CHARACTER (LEN=75) :: texte
   CHARACTER (LEN=50) :: fichi
   CHARACTER (LEN=50) :: fichout

   LOGICAL :: file_exists
   INTEGER :: iloc, nsp, iline, itrans
   REAL(KIND=dp) :: xmu
   CHARACTER(LEN=8) :: yspectab(10) ! 10 species maximum.
   CHARACTER(LEN=8) :: PoubSp
   INTEGER :: nlines(10), linesnum(50)

!--------+---------+---------+---------+---------+---------+---------+-*-------+
   PRINT *
   PRINT *, "***********************************"
   PRINT *, "*                                 *"
   PRINT *, "*   PDR MODEL RESULTS ANALYSIS    *"
   PRINT *, "*                                 *"
   PRINT *, "***********************************"

   !========================================================
   ! Check if there will be interaction with user or not
   !========================================================
   INQUIRE(FILE="./prepin", EXIST=file_exists)
   IF (.not. file_exists) THEN
      PRINT *
      PRINT *, 'Do you want to keep track of this run?'
      PRINT *, '1: yes,   0: no'
      READ *, irec
      IF (irec == 1) THEN
         PRINT *, '   Commands are recorded infile "prepin"'
         OPEN (8, file='prepin', action='write', status='replace')
         WRITE (8,*) irec-1
      ENDIF
   ELSE
      PRINT *, '   Reading of the infile "prepin"'
      OPEN (8, file='prepin', action='read')
      READ (8,*) irec
   ENDIF

   !========================================================
   ! Choose the input file (.bin file)
   !========================================================

   IF (iargc() .eq. 1) THEN
      CALL getarg(1,fichier)
   ELSE
      IF (.not. file_exists) THEN
         PRINT *
         PRINT *, 'Enter name of "bin" file'
         READ (*,'(a50)') fichi
      ELSE
         READ (8,*) fichi
      ENDIF
      fichi = TRIM(ADJUSTL(fichi))
      IF (irec == 1) THEN
         WRITE (8,'(a50)') fichi
      ENDIF
      ipoint = INDEX(fichi,'.',back=.true.)
      IF (ipoint > 1) THEN
         modele = fichi(1:ipoint-1)
      ENDIF
      out_dir = TRIM(out_dir)//TRIM(modele)//"/"
      fichier = TRIM(out_dir)//TRIM(fichi)
   ENDIF

   !========================================================
   ! READ INPUT DATAFILE
   !========================================================

   CALL PLECTUR

   !========================================================
   ! EXTRACTION OF DATA AND ANALYSIS
   !========================================================

   ifin = 1
   DO WHILE (ifin == 1)

      ! Chemistry analysis
      IF (.not. file_exists) THEN
         PRINT *, 'Would you enter the chemistry analysis module?'
         PRINT *, '1: yes,   0: no'
         READ *, ii
         IF (irec == 1) THEN
            WRITE (8,*) ii
         ENDIF
      ELSE
         READ (8,*) ii
      ENDIF

      IF (ii == 1) THEN
         CALL ANALYS
         IF (.not. file_exists) THEN
            PRINT *, 'Do you want to continue?'
            PRINT *, '1: yes,   0: no'
            READ *, ifin
         ELSE
            READ (8,*) ifin 
         ENDIF
         CYCLE
      ENDIF

      !--- Choose the output file name ---------------------
      IF (.not. file_exists) THEN
         PRINT *
         PRINT *, 'Enter output file name'
         READ (*,'(a50)') fichout
         fichout = TRIM(ADJUSTL(fichout))
         IF (irec == 1) THEN
            WRITE (8,'(a50)') fichout
         ENDIF
      ELSE
         READ (8,*) fichout
         fichout = TRIM(ADJUSTL(fichout))
      ENDIF

      fichier = TRIM(out_dir)//fichout
      PRINT *
      PRINT *,'Output written in: ', TRIM(ADJUSTL(fichier))
      OPEN (25, file = fichier, status = 'replace')

      IF (.not. file_exists) THEN
         PRINT *, '   Enter a comment line'
         READ (*,'(a75)') texte
         IF (irec == 1) THEN
            WRITE (8,'(a75)') texte
         ENDIF
      ELSE
         READ (8,*) texte
         texte = TRIM(ADJUSTL(texte))
      ENDIF
      WRITE (25,'(7x,a75)') texte

      !--- Choose X variable (first axis) -----------------
      ifalse = 1
      DO WHILE (ifalse == 1)
         IF (.not. file_exists) THEN
            PRINT *
            PRINT *, '   Which variable is x?'
            PRINT *, '   1: tau      2: Av'
            PRINT *, '   3: NH       4: N(H2)'
            READ *, ii
            PRINT *, '  x variable is number:', ii
            IF (irec == 1) THEN
               WRITE (8,*) ii
            ENDIF
         ELSE
            READ (8,*) ii
         ENDIF

         IF (ii == 1) THEN
            x(0:npo) = tau(0:npo)
            textx = 'tau           '
            ifalse = 0
         ELSE IF (ii == 2) THEN
            x(0:npo) = visualext(0:npo)
            textx = 'Av            '
            ifalse = 0
         ELSE IF (ii == 3) THEN
            x(0:npo) = codesp(i_h,0:npo) + 2.0_dp * codesp(i_h2,0:npo)
            textx = 'NH            '
            ifalse = 0
         ELSE IF (ii == 4) THEN
            x(0:npo) = codesp(i_h2,0:npo)
            textx = 'N(H2)         '
            ifalse = 0
         ELSE
            PRINT *, '     ****  WARNING: value not allowed ******'
            PRINT *, '  ii =', ii
         ENDIF
      ENDDO

      !--- Choose the other quantities -------------------------------------
      iimax = 0

      ifalse = 1
      DO WHILE (ifalse == 1)
         IF (.not. file_exists) THEN
            PRINT *
            PRINT *, '   Select next variable kind'
            PRINT *, '     0: end'
            PRINT *, '     1: Abundance  2: Column density   3: Emissivity'
            PRINT *, '     4: Heating - Cooling'
            PRINT *, '     5: Else (T, density, ions, ...)'
            PRINT *, '     6: Pseudo-Spherical cloud (Experimental)'
            READ *, ii1
            IF (irec == 1) THEN
               WRITE (8,*) ii1
            ENDIF
         ELSE
            READ (8,*) ii1
         ENDIF

         !!!!!!!!!!!!!!!!!
         IF (ii1 == 0) THEN
            ifalse = 0
            CYCLE
         ELSE IF (ii1 == 1) THEN
            CALL ABONDA
         ELSE IF (ii1 == 2) THEN
            CALL COLDEN
         ELSE IF (ii1 == 3) THEN
            IF (.not. file_exists) THEN
               CALL EMISSI
            ELSE
               READ (8,*) iloc
               READ (8,*) fichout
               READ (8,*) xmu
               ! Get species and line numerotation.
               nsp = 0
               iline = 0
               DO
                  itrans = 999
                  READ (8,*) PoubSp
                  IF (lle("-1",trim(adjustl(PoubSp))) .and. lge("-1",trim(adjustl(PoubSp)))) THEN
                     EXIT
                  ELSE
                     nsp = nsp+1
                     yspectab(nsp) = PoubSp
                     nlines(nsp) = 0
                  ENDIF
                  READ (8,*) PoubSp
                  DO WHILE (itrans /=0)
                     READ (8,*) itrans
                     IF (itrans /= 0) THEN
                        nlines(nsp) = nlines(nsp) + 1
                        iline = iline + 1
                        linesnum(iline) = itrans
                     ENDIF
                  ENDDO
               ENDDO
               CALL EMISSI_WITH_FILE(iloc, fichout, xmu, yspectab, nsp, nlines, linesnum)
            ENDIF
         ELSE IF (ii1 == 4) THEN
            CALL CHOFRO
         ELSE IF (ii1 == 5) THEN
            CALL AUTRE
         ELSE IF (ii1 == 6) THEN
            CALL SPHERE
         ELSE
            PRINT *, '     ****  ATTENTION: value not allowed ******'
            PRINT *, '  ii1 =', ii1
            CYCLE
         ENDIF

         IF (iimax < ncolf) THEN
            IF (.not. file_exists) THEN
               PRINT *
               PRINT *, 'Do you want to continue?'
               PRINT *, '1: yes,   0: no'
               READ *, ifalse
               IF (irec == 1) THEN
                  WRITE (8,*) ifalse
               ENDIF
            ELSE
               READ (8,*) ifalse
            ENDIF
         ELSE
            PRINT *, '  WARNING: NCOLF too small !'
            PRINT *, '  Modify and run again'
            ifalse = 0
         ENDIF
      ENDDO

      !-----------------------------------------------------------------------
      ! WRITE OUTPUTS
      !-----------------------------------------------------------------------
      PRINT *
      PRINT *, '   There are ', iimax,' y columns in file ', fichout

      WRITE (25,'(2x,153a17)') textx, (texty(ii), ii=1,iimax)
      DO j = 0, npo
         WRITE (25,'(1p,e19.10,152e17.8E3)') x(j), (y(ii,j), ii=1,iimax)
      ENDDO

      CLOSE (25)

      !-----------------------------------------------------------------------
      ! PREPARATION FOR WRITTING ANOTHER FILE
      !-----------------------------------------------------------------------
      IF (.not. file_exists) THEN
         PRINT *
         PRINT *, 'Do you want a new file?'
         PRINT *, '1: yes,   0: no'
         READ *, ifin
         IF (irec == 1) THEN
            WRITE (8,*) ifin
         ENDIF
      ELSE
         READ (8,*) ifin
      ENDIF
   ENDDO

END PROGRAM stdprep
