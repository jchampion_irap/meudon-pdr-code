
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


MODULE PXDR_TRANSFER

! +new formulation for the non-embedded case
!  but incuding R' effects   (spring 2005) jlb
! +theory & code revision (june-july 2005) jrg
! +embedded sources preliminary coding (oct 2005) jlb+jrg

   USE PXDR_CONSTANTES

   IMPLICIT NONE

   REAL (KIND=dp), PUBLIC                            :: flu_rad0
   REAL (KIND=dp), PUBLIC                            :: flu_rad1
   REAL (KIND=dp), PUBLIC                            :: flu_rad2
   REAL (KIND=dp), PUBLIC                            :: int_rad0
   REAL (KIND=dp), PUBLIC                            :: int_rad1
   REAL (KIND=dp), PUBLIC                            :: int_rad2
   REAL (KIND=dp)                                    :: alpha_m
   REAL (KIND=dp)                                    :: alpha_p

   REAL (KIND=dp), PUBLIC                             :: G0m
   REAL (KIND=dp), PUBLIC                             :: G0p

   PRIVATE

   PUBLIC :: ETIN90, ETIN901S, RF_INIT, RF_BOUND

!------------------------------------------------------------------------

CONTAINS

SUBROUTINE ETIN90 (nwlu)

 USE PXDR_CONSTANTES
 USE PXDR_CHEM_DATA
 USE PXDR_PROFIL
 USE PXDR_AUXILIAR
 USE PXDR_STR_DATA
 USE PXDR_RF_TOOLS

! Radiative transfer computation. See file transfer_S.pdf for definitions
! and comments. Index conventions:
!       l goes from 0 to lleg (first Legendre Pol. index)
!       m goes from +/- 1 to +/- mleg (second Legendre Pol. index)
!       i goes from +/- 1 to +/- nmui (discrete angles for boundary conditions)
!       n goes from 0 to npo (optical depth grid)
! 2 sides version: tmax finite and radiation are impiging from mu_i > 0 and mu_i < 0

 IMPLICIT NONE

 INTEGER, INTENT (IN)                          :: nwlu     ! Number of wavelength computed

 INTEGER                                       :: l, m, n, n1, n3

 ! Radiative quantities
 INTEGER                                       :: iwlg
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: tl       ! Full optical depth
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: dtl      ! Full optical depth increment
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: dedtl    ! Full optical depth increment
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: sigl     ! Phase function

 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: albed    ! Reduced albedo (including gas)

 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: kap_gas
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: kap_dus
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: sca_dus
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: emm_dus
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: Camp     ! Legendre amplitudes

 type(rf_type)                                 :: rf_tmp, rf_lamI

 ! Legendre polynomial related variables
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: xmunl    ! -1 power l  = (-1)**l
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: pli      ! P_l(mu_i)
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: p_0
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: q_0

 ! Matrix A coefficients
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: hl
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: wr, wi
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: xkm      ! Legendre absissa
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Smk_m    ! integrated xkm
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Smk_p    ! integrated xkm
 ! Smk_m = Emk_m_mag * grand + s => Exp(-Smk_m) = G**Emk_m_mag * Emk_m_cmp
 !       with G = exp(-grand) and Emk_m_cmp = exp(-s)
 INTEGER (KIND=ilk), DIMENSION(:,:), ALLOCATABLE :: Emk_m_mag! Exp(-Smk_m) 0 -> t - power of G
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Emk_m_cmp! Exp(-Smk_m) 0 -> t - remaining part
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Emk_m_val! Exp(-Smk_m) 0 -> t - Total value
 INTEGER (KIND=ilk), DIMENSION(:,:), ALLOCATABLE :: Emk_p_mag! Exp(-Smk_m) 0 -> t - power of G
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Emk_p_cmp! Exp(-Smk_m) 0 -> t - remaining part
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Emk_p_val! Exp(-Smk_m) 0 -> t - Total value
 INTEGER (KIND=ilk), DIMENSION(:), ALLOCATABLE :: md_m, md_p
 INTEGER,        DIMENSION(:,:),   ALLOCATABLE :: ndp_m, ndq_m
 INTEGER,        DIMENSION(:,:),   ALLOCATABLE :: ndp_p, ndq_p
 INTEGER (KIND=ilk), DIMENSION(:,:), ALLOCATABLE :: nn_m, nn_p
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: T_m, T_p
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: R_m, R_p
 INTEGER (KIND=ilk)                            :: k_ilk, i_ilk

 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: QmG
 REAL (KIND=dp), DIMENSION(:,:,:), ALLOCATABLE :: rlm      ! Eigenvectors matrix
 REAL (KIND=dp), DIMENSION(:,:,:), ALLOCATABLE :: rlmp     ! derivative
 REAL (KIND=dp), DIMENSION(:,:,:), ALLOCATABLE :: rlmi     ! inverse of Rlm(t)

 ! Matrix product of R^-1 x R'
 REAL (KIND=dp), DIMENSION(:,:,:), ALLOCATABLE :: rlmip
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Srm

! Source function contributions

 ! Source function expansion coeficients (as function of l and m respectively)
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: sfl, sfm, sfl_g, sfl_d
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: ggm      ! Inhomogeneous contribution (g vector in notes)
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Git      ! Product R^-1*A^-1*g vector, same DIMENSIONs as Qit

 ! Boundary conditions related variables
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Tim      ! Auxiliary
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Bim      ! BC matrix
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: Iinc     ! Incident radiation field

 ! Iteration related variables
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Qit
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Qit_o
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Dit
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Yit
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Yit_o
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: Hit

! LAPACK related variables

 ! Eigenvalues only
 CHARACTER (LEN=1)                             :: JOBZ  = 'N'
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: WORK         ! Real work array
 INTEGER,        DIMENSION(:),     ALLOCATABLE :: IWORK        ! Work array
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: aam, bbm
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: aamf, bbmf
 INTEGER                                       :: info
 CHARACTER (LEN=1)                             :: FACT  = 'E'  ! Equilibrate system
 CHARACTER (LEN=1)                             :: TRANS = 'N'  ! Transpose
 INTEGER                                       :: nrhs
 CHARACTER (LEN=1)                             :: EQUED        ! Kind of equilibration
 REAL (KIND=dp), DIMENSION(:), ALLOCATABLE     :: R, C         ! Equilibration factors
 REAL (KIND=dp), DIMENSION(:), ALLOCATABLE     :: FERR, BERR   ! Errors (estimates)
 REAL (KIND=dp)                                :: RCOND        ! Condition number
 INTEGER, DIMENSION(:), ALLOCATABLE            :: indx

 ! Auxiliaries
 INTEGER                                       :: nl, nnit
 INTEGER                                       :: it, i, i0
 INTEGER                                       :: nb_m, nb_p
 INTEGER (KIND=ilk)                            :: n0, le_n
 INTEGER                                       :: i1, i2
 REAL (KIND=dp)                                :: ht, ymui
 REAL (KIND=dp)                                :: enor_m, enor_p
 REAL (KIND=dp)                                :: taux1, taux2
 REAL (KIND=dp)                                :: taux3, taux4
 REAL (KIND=dp)                                :: wlth, wlthcm
 REAL (KIND=dp)                                :: deriv_d
 REAL (KIND=dp), DIMENSION(:), ALLOCATABLE     :: argu
 REAL (KIND=dp), DIMENSION(-mleg:mleg)         :: auxil
 INTEGER, DIMENSION(:,:), ALLOCATABLE          :: indmi, indmj

 ! Thermalized (black body) emission of dust and gas
 REAL (KIND=dp)                                :: bbgas

 ! Computation of emerging specific intensities
 REAL (KIND=dp), DIMENSION(:), ALLOCATABLE     :: fl_0, fl_max

!-----------------------------------------------------------------------------

  ! Number of Legendred coefficient (used for loops that run from 1 to lleg+1)
  nl = lleg + 1

  IF (ALLOCATED(g_absL)) THEN
     DEALLOCATE (g_absL)
  ENDIF

  IF (ALLOCATED(intJm)) THEN
     DEALLOCATE (intJm, intJp)
     DEALLOCATE (bbspm, bbspp)
  ENDIF

  IF (ALLOCATED(fluxm)) DEALLOCATE(fluxm)
  IF (ALLOCATED(fluxp)) DEALLOCATE(fluxp)
  IF (ALLOCATED(I_esc_0)) DEALLOCATE(I_esc_0)
  IF (ALLOCATED(I_esc_max)) DEALLOCATE(I_esc_max)
  IF (ALLOCATED(abs_tot)) DEALLOCATE(abs_tot)
  IF (ALLOCATED(intJmp)) DEALLOCATE(intJmp)

  ALLOCATE (xmunl(0:lleg))
  ALLOCATE (pli(0:lleg,-nmui:nmui))
  ALLOCATE (p_0(0:lleg), q_0(0:lleg))
  ALLOCATE (hl(0:lleg,0:npo))
  ALLOCATE (wr(lleg+1), wi(lleg+1))
  ALLOCATE (xkm(-mleg:mleg,0:npo))
  ALLOCATE (Smk_m(0:npo,mleg))
  ALLOCATE (Smk_p(0:npo,mleg))
  ALLOCATE (Emk_m_mag(0:npo,mleg))
  ALLOCATE (Emk_m_cmp(0:npo,mleg))
  ALLOCATE (Emk_m_val(0:npo,mleg))
  ALLOCATE (Emk_p_mag(0:npo,mleg))
  ALLOCATE (Emk_p_cmp(0:npo,mleg))
  ALLOCATE (Emk_p_val(0:npo,mleg))
  ALLOCATE (T_m(0:npo,mleg))
  ALLOCATE (T_p(0:npo,mleg))

  ALLOCATE (QmG(0:npo))
  ALLOCATE (rlm(0:lleg,-mleg:mleg,0:npo))
  ALLOCATE (rlmp(0:lleg,-mleg:mleg,0:npo))
  ALLOCATE (rlmi(0:lleg,-mleg:mleg,0:npo))
  ALLOCATE (rlmip(0:lleg,-mleg:mleg,0:npo))
  ALLOCATE (sfm(-mleg:mleg,0:npo))
  ALLOCATE (sfl(0:lleg,0:npo))
  ALLOCATE (sfl_g(0:lleg,0:npo),sfl_d(0:lleg,0:npo))
  ALLOCATE (ggm(-mleg:mleg,0:npo))
  ALLOCATE (Srm(mleg,0:npo))
  ALLOCATE (sigl(0:lleg,0:npo))
  ALLOCATE (dtl(0:npo))
  ALLOCATE (dedtl(0:npo))
  ALLOCATE (tl(0:npo,0:nwlu))
  ALLOCATE (albed(0:npo,0:nwlu))
  ALLOCATE (kap_gas(0:npo))
  ALLOCATE (kap_dus(0:npo))
  ALLOCATE (sca_dus(0:npo))
  ALLOCATE (emm_dus(0:npo))
  ALLOCATE (g_absL(0:npo,0:nwlu))
  ALLOCATE (intJm(0:npo,0:nwlu))
  ALLOCATE (intJp(0:npo,0:nwlu))
  ALLOCATE (intJmp(0:npo,0:nwlu))
  ALLOCATE (bbspm(0:npo,0:nwlu))
  ALLOCATE (bbspp(0:npo,0:nwlu))
  ALLOCATE (R(nl), C(nl))
  ALLOCATE (FERR(1), BERR(1))
  ALLOCATE (WORK(4*nl))
  ALLOCATE (IWORK(nl))
  ALLOCATE (aam(nl,nl), bbm(nl,1))
  ALLOCATE (aamf(nl,nl), bbmf(nl,1))
  ALLOCATE (indx(nl))
  ALLOCATE (Tim(-nmui:nmui,-mleg:mleg))
  ALLOCATE (Bim(-nmui:nmui,-mleg:mleg))
  ALLOCATE (Iinc(-nmui:nmui))
  ALLOCATE (Qit(-mleg:mleg,0:npo))
  ALLOCATE (Qit_o(-mleg:mleg,0:npo))
  ALLOCATE (Git(-mleg:mleg,0:npo))
  ALLOCATE (Dit(-mleg:mleg,0:npo))
  ALLOCATE (Yit(-mleg:mleg,0:npo))
  ALLOCATE (Yit_o(-mleg:mleg,0:npo))
  ALLOCATE (Hit(-nmui:nmui))
  ALLOCATE (Camp(-nmui:nmui))
  ALLOCATE (argu(0:npo))
  ALLOCATE (indmi(mleg,0:npo), indmj(mleg,0:npo))
  ALLOCATE (fl_0(0:lleg))
  ALLOCATE (fl_max(0:lleg))
  ALLOCATE (I_esc_0(0:nwlu,-nmui:nmui))
  ALLOCATE (I_esc_max(0:nwlu,-nmui:nmui))
  ALLOCATE (fluxm(0:nwlu))
  ALLOCATE (fluxp(0:nwlu))
  ALLOCATE (abs_tot(0:nwlu))

  p_0(:)         = 0.0_dp
  q_0(:)         = 0.0_dp
  sigl(:,:)      = 0.0_dp
  Tim(:,:)       = 0.0_dp
  Bim(:,:)       = 0.0_dp
  Iinc(:)        = 0.0_dp
  xkm(:,:)       = 0.0_dp
  Qit(:,:)       = 0.0_dp
  Qit_o(:,:)     = 0.0_dp
  Yit(0,0:npo)   = 0.0_dp
  Yit_o(0,0:npo) = 0.0_dp
  I_esc_0(:,:)   = 0.0_dp
  I_esc_max(:,:) = 0.0_dp

  ! This is just (-1)**l
  xmunl(0) = 1.0_dp
  DO l = 1, lleg
     xmunl(l) = - xmunl(l-1)
  ENDDO

! STEP 0 - Initialise radiative quantities
!  (boundary conditions and optical depth grid)

  ! uvspec.out is the total absorption through the cloud. multiply by
  !            incident radition field to compute spectrum.
  fichier = TRIM(out_dir)//TRIM(modele)//'.Iesc'
  PRINT *, " Escaping intensities in:", TRIM(fichier)
  OPEN (36, file=fichier, status="unknown")
  fichier = TRIM(out_dir)//TRIM(modele)//'.uv'
  PRINT *, " Absorption in:", TRIM(fichier)
  OPEN (37, file=fichier, status="unknown")
  fichier = TRIM(out_dir)//TRIM(modele)//'.flin'
  PRINT *, " External field:", TRIM(fichier)
  OPEN (60, file=fichier, status="unknown")
  WRITE(60,*) "9      # Number of columns"

  ! Compute absorption by gas
  CALL ABSGAZ (nwlu)

! STEP 1 - Compute P_l(mu_i) - see recurence in Num Rec
  WRITE(iscrn,'("       Point             Angstroms       erg cm-2 s-1 A-1 sr-1")')
  ! Example on screen : iwlg =   100 wlth =        925.259  intJmp: edge =   4.5370E-08 middle =  9.0686E-26

  pli(0,:) = 1.0_dp
  DO i = 1, nmui
     ymui = xmui_M(i)
     pli(1,-i) = -ymui
     pli(1,i) = ymui
     DO l = 1, lleg-1
        pli(l+1,-i) = (-ymui*(2.0_dp*l+1.0_dp) * pli(l,-i) - l * pli(l-1,-i)) / (l+1.0_dp)
        pli(l+1,i) = xmunl(l+1) * pli(l+1,-i)
     ENDDO
  ENDDO

  p_0(0) = 1.0_dp
  q_0(1) = p_0(0) / 2.0_dp
  DO l = 1, lleg/2
     p_0(2*l) = - (2.0_dp*l-1) * p_0(2*l-2) / (2.0_dp*l)
     q_0(2*l+1) = p_0(2*l) / (2.0_dp*l+2.0_dp)
  ENDDO

  DO iwlg = 0, nwlu
     ! sigl = gg**l (Formule 48, Henyey-Greenstein phase function)
     DO n = 0, npo
        sigl(0,n) = 1.0_dp
        DO l = 1, lleg
           sigl(l,n) = sigl(l-1,n) * du_prop%gcos(iwlg,n)
        ENDDO
     ENDDO

     ! a - Reset to 0 most variables
     hl = 0.0_dp
     wr = 0.0_dp
     wi = 0.0_dp
     tl(:,iwlg) = 0.0_dp
     dtl = 0.0_dp
     dedtl = 0.0_dp
     albed(:,iwlg) = 0.0_dp
     kap_gas = 0.0_dp
     kap_dus = 0.0_dp
     sca_dus = 0.0_dp
     emm_dus = 0.0_dp

     wlth = rf_wl%Val(iwlg)
     wlthcm = wlth * 1.0e-8_dp

     ! Compute local values of absorption coef for dust and gas and dust scatering coef
     ! Compute effective albedo !
     DO n = 0, npo
        kap_gas(n) = g_absL(n,iwlg)
        kap_dus(n) = du_prop%abso(iwlg,n) * dens_o(n)
        sca_dus(n) = du_prop%scat(iwlg,n) * dens_o(n)
        emm_dus(n) = du_prop%emis(iwlg,n) * dens_o(n)
        albed(n,iwlg) = sca_dus(n) / (kap_dus(n) + kap_gas(n) + sca_dus(n))
     ENDDO

     ! b - Compute optical depth grid
     !     Convert Delta Tau_v (dust only at V) to
     !     Delta tau_lambda (dust + gaz at Lambda)
     !     17 Oct 2011: use dd_cm (ds) for integration
     tl(0,iwlg) = 0.0_dp
     DO n = 0, npo-1
        ht = ((kap_dus(n) + kap_gas(n) + sca_dus(n)) &
           + (kap_dus(n+1) + kap_gas(n+1) + sca_dus(n+1))) &
           * 0.5_dp * dd_cm(n+1)
        dtl(n) = ht
        tl(n+1,iwlg) = tl(n,iwlg) + ht
     ENDDO
     DO n = 1, npo-1
        dedtl(n) = dtl(n-1) + dtl(n)
     ENDDO
     ! Compute total attenuation by dust + gas through whole cloud
     taux1 = tl(npo,iwlg)
     abs_tot(iwlg) = EXP(-taux1)
     WRITE (37,"(1p,3e17.7E3)") wlth, abs_tot(iwlg), taux1

! STEP 2a - Compute matrix A(tau)

     DO n = 0, npo
        ! Formulae A2 and A3 of Roberge:
        hl(0,n) = 1.0_dp - albed(n,iwlg)
        DO l = 1, lleg
           hl(l,n) = (2.0_dp*l+1.0_dp) * (1.0_dp - albed(n,iwlg) * sigl(l,n))
        ENDDO
        DO l = 1, lleg
           wi(l) = DBLE(l) / SQRT(hl(l-1,n)*hl(l,n))
        ENDDO
        wr = 0.0_dp

        ! Compute eigenvalues (tridiagonal symetric matrix) - LAPACK
        CALL DSTEVD(JOBZ, nl, wr, wi, aam, nl, WORK, 4*nl, IWORK, nl, info )

        ! order eigenvalues by decreasing order
        wi = wr
        DO l = 1, nl
           wr(l) = wi(nl+1-l)
        ENDDO

        ! km = 1 / lamda_m in increasing order
        DO m = 1, mleg
           xkm(m,n) = 1.0_dp / wr(m)
           xkm(-m,n) = -xkm(m,n)
        ENDDO
     ENDDO   ! end loop in npo

     ! Calcul des Rlm: Formule A9 de Roberge 1983
     ! Eigenvectors computed only from recurrence relations.
     DO n = 0, npo
        DO m = 1, mleg
           rlm(0,m,n)  = 1.0_dp
           rlm(0,-m,n) = 1.0_dp
           rlm(1,m,n)  = (1.0_dp - albed(n,iwlg)) / xkm(m,n)
           rlm(1,-m,n) = -rlm(1,m,n)                             !jrg
           DO l = 2, lleg
              rlm(l,m,n) = (hl(l-1,n) * rlm(l-1,m,n) &
                         - (l-1) * xkm(m,n) * rlm(l-2,m,n)) / (l*xkm(m,n))
              rlm(l,-m,n) = xmunl(l) * rlm(l,m,n)
           ENDDO
        ENDDO
     ENDDO   ! end loop in npo

! STEP 2c - Compute Srm - used for mean intensity

     CALL AUX_Srm(q_0, rlm, Srm)

! STEP 3 - Compute Smk_m = Sum (xkm) (left to right) and Smk_p (right to left)
!        - Simple Simpson rule

     Smk_m(0,:) = 0.0_dp
     Smk_p(npo,:) = 0.0_dp
     DO m = 1, mleg
        DO n = 1, npo
           Smk_m(n,m) = Smk_m(n-1,m) + 0.5_dp * dtl(n-1) * (xkm(m,n) + xkm(m,n-1))
        ENDDO
        DO n = npo-1, 0, -1
           Smk_p(n,m) = Smk_p(n+1,m) + 0.5_dp * dtl(n) * (xkm(m,n) + xkm(m,n+1))
        ENDDO
     ENDDO

     DO m = 1, mleg
        Emk_m_mag(:,m) = INT(MIN(Smk_m(:,m),1.0e18_dp) / grand, ilk)
        Emk_m_cmp(:,m) = EXP(-(Smk_m(:,m)-grand*Emk_m_mag(:,m)))
        Emk_m_val(:,m) = expgr(MIN(mgr,Emk_m_mag(:,m))) * Emk_m_cmp(:,m)
        Emk_p_mag(:,m) = INT(MIN(Smk_p(:,m),1.0e18_dp) / grand, ilk)
        Emk_p_cmp(:,m) = EXP(-(Smk_p(:,m)-grand*Emk_p_mag(:,m)))
        Emk_p_val(:,m) = expgr(MIN(mgr,Emk_p_mag(:,m))) * Emk_p_cmp(:,m)
     ENDDO
     ALLOCATE (md_m(mleg))
     ALLOCATE (md_p(mleg))
     md_m(:) = Emk_m_mag(npo,:)
     md_p(:) = Emk_p_mag(0,:)
     ALLOCATE (ndp_m(0:npo+1,mleg))
     ALLOCATE (ndq_m(0:npo+1,mleg))
     ALLOCATE (ndp_p(0:npo+1,mleg))
     ALLOCATE (ndq_p(0:npo+1,mleg))
     ALLOCATE (R_m(npo+1,mleg))
     ALLOCATE (R_p(npo+1,mleg))
     ALLOCATE (nn_m(npo+1,mleg))
     ALLOCATE (nn_p(npo+1,mleg))
     ndp_m = 0
     ndq_m = 0
     ndp_p = 0
     ndq_p = 0

     DO m = 1, mleg
        le_n = Emk_m_mag(0,m)
        nb_m = 1
        ndp_m(1,m) = 0
        nn_m(nb_m,m) = le_n
        DO n = 1, npo
           IF (Emk_m_mag(n,m) == le_n) THEN
              CYCLE
           ELSE
              ndq_m(nb_m,m) = n - 1
              nn_m(nb_m,m) = le_n
              le_n = Emk_m_mag(n,m)
              nb_m = nb_m + 1
              ndp_m(nb_m,m) = n
           ENDIF
        ENDDO
        ndq_m(nb_m,m) = npo
        nn_m(nb_m,m) = le_n
        md_m(m) = nb_m
     ENDDO

     DO m = 1, mleg
        le_n = Emk_p_mag(npo,m)
        nb_p = 1
        ndq_p(1,m) = npo
        nn_p(nb_p,m) = le_n
        DO n = npo-1, 0, -1
           IF (Emk_p_mag(n,m) == le_n) THEN
              CYCLE
           ELSE
              ndp_p(nb_p,m) = n + 1
              nn_p(nb_p,m) = le_n
              le_n = Emk_p_mag(n,m)
              nb_p = nb_p + 1
              ndq_p(nb_p,m) = n
           ENDIF
        ENDDO
        ndp_p(nb_p,m) = 0
        nn_p(nb_p,m) = le_n
        md_p(m) = nb_p
     ENDDO

! STEP 4 - Compute R**-1    !Loop to all cloud opacity points

     CALL AUX_Rlmi(rlm, hl, rlmi)

! STEP 5 - Compute source function contribution g~ = R^-1*A^-1*g (in notes)
!          JLB : Check Javier correction (not yet done here)

     DO n = 0, npo
        ! Ex. Black body emission of thermalized gas
        IF (wlthcm*tgaz(n) > 1.0e-2_dp) THEN
           bbgas = 2.0_dp*xhp*clum**2 / (wlthcm)**5
           bbgas = bbgas * 1.0e-8_dp / (EXP(hcsurk/(wlthcm*tgaz(n))) -1.0_dp)
        ELSE
           bbgas = 0.0_dp
        ENDIF

! 5a - Assign source function, isotropic  (PROVISIONAL)

        ! METHOD B1: approx. to continuum thermal isotropic emission
        sfl_d(0,n) = emm_dus(n) / (kap_dus(n) + kap_gas(n) + sca_dus(n))

        ! METHOD B2: approx. to line thermal isotropic emission
        sfl_g(0,n) = bbgas * kap_gas(n) / (kap_dus(n) + kap_gas(n) + sca_dus(n))

        ! Add both contributions
!       sfl(0,n) = sfl_g(0,n) + sfl_d(0,n)
        sfl(0,n) = sfl_d(0,n)
!       sfl(0,n) = 0.0_dp

        sfl(1:lleg,n) = 0.0_dp
     ENDDO

     ! Put them in m,n desired order
     sfm(-mleg:-1,:) = sfl(0:mleg-1,:)
     sfm(1:mleg,:) = sfl(mleg:lleg,:)

! 5b - Compute vector g

     DO n = 0, npo
        DO m = -mleg, -1
           l = mleg + m
           ggm(m,n) = -sfm(m,n) / ( 1.0_dp - albed(n,iwlg) * sigl(l,n) )
        ENDDO
        DO m = 1, mleg
           l = mleg + m - 1
           ggm(m,n) = -sfm(m,n) / ( 1.0_dp - albed(n,iwlg) * sigl(l,n) )
        ENDDO
     ENDDO

! 5d - Compute matrix product  R^-1*A^-1*g = Git

     CALL AUX_GIT(rlmi, ggm, xkm, Git)

! STEP 6 - Compute derivatives of Rlm

! 6c - derivative of Rlm

     DO n = 2, npo-2
        DO m = 1, mleg
           rlmp(0,m,n) = 0.0_dp
           rlmp(0,-m,n) = 0.0_dp
           n1 = n - 2
           n3 = n + 2
           DO l = 1, lleg
              ! Direct variable
              deriv_d = -rlm(l,m,n1) * (dtl(n)+dtl(n+1)) / ((dtl(n-2)+dtl(n-1)) * (dtl(n-2)+dtl(n-1)+dtl(n)+dtl(n+1))) &
                      - rlm(l,m,n)  * (dtl(n-2)+dtl(n-1)-dtl(n)-dtl(n+1)) / ((dtl(n)+dtl(n+1)) * (dtl(n-2)+dtl(n-1))) &
                      + rlm(l,m,n3) * (dtl(n-2)+dtl(n-1)) / ((dtl(n-2)+dtl(n-1)+dtl(n)+dtl(n+1)) * (dtl(n)+dtl(n+1)))
              rlmp(l,m,n) = deriv_d
              rlmp(l,-m,n) = xmunl(l) * rlmp(l,m,n)
           ENDDO
        ENDDO
     ENDDO

     ! Boundary condition. Replace par linear extrapolation (TODO)
     DO m = 1, mleg
        rlmp(0,m,0) = 0.0_dp
        rlmp(0,-m,0) = 0.0_dp
        rlmp(0,m,1) = 0.0_dp
        rlmp(0,-m,1) = 0.0_dp
        rlmp(0,m,npo-1) = 0.0_dp
        rlmp(0,-m,npo-1) = 0.0_dp
        rlmp(0,m,npo) = 0.0_dp
        rlmp(0,-m,npo) = 0.0_dp
        DO l = 1, lleg
           rlmp(l,m,0) = rlmp(l,m,2)
           rlmp(l,-m,0) = xmunl(l) * rlmp(l,m,0)
           rlmp(l,m,1) = rlmp(l,m,2)
           rlmp(l,-m,1) = xmunl(l) * rlmp(l,m,1)
           rlmp(l,m,npo-1) = rlmp(l,m,npo-2)
           rlmp(l,-m,npo-1) = xmunl(l) * rlmp(l,m,npo-1)
           rlmp(l,m,npo) = rlmp(l,m,npo-2)
           rlmp(l,-m,npo) = xmunl(l) * rlmp(l,m,npo-2)
        ENDDO
     ENDDO

! 6d - Multiply by R**-1

     CALL AUX_Rlmip(rlmi, rlmp, rlmip)

! STEP 7 - Compute Tim

     DO m = 1, mleg
        DO i = 1, nmui
           taux1 = 0.0_dp
           taux2 = 0.0_dp
           taux3 = 0.0_dp
           taux4 = 0.0_dp
           DO l = 0, lleg
              taux1 = taux1 + (2.0_dp * l + 1.0_dp) * rlm(l,-m,0) * pli(l,-i)
              taux2 = taux2 + (2.0_dp * l + 1.0_dp) * rlm(l,m,0) * pli(l,-i)
              taux3 = taux3 + (2.0_dp * l + 1.0_dp) * rlm(l,-m,npo) * pli(l,i)
              taux4 = taux4 + (2.0_dp * l + 1.0_dp) * rlm(l,m,npo) * pli(l,i)
           ENDDO
           Tim(-i,-m) = taux1
           Tim(-i,m)  = taux2
           Tim(i,-m)  = taux3
           Tim(i,m)   = taux4
        ENDDO
     ENDDO

! STEP 8 - Compute Bim (Boundary conditions matrix)

     DO m = 1, mleg
        DO i = 1, nmui
           Bim(-i,-m) = Tim(-i,-m)
           Bim(-i,m) = Tim(-i,m) * Emk_m_val(npo,m)
           Bim(i,-m) = Tim(i,-m) * Emk_m_val(npo,m)
           Bim(i,m) = Tim(i,m)
        ENDDO
     ENDDO

! STEP 9 - Set boundary conditions

     ! Add potential // source (star)
     IF (F_Norm_RF == 1) THEN
        fluxm(iwlg) = alpha_m * rf_star_m%Val(iwlg)
        fluxp(iwlg) = 0.0_dp
     ELSE IF (F_Norm_RF == 2) THEN
        fluxm(iwlg) = 0.0_dp
        fluxp(iwlg) = alpha_p * rf_star_p%Val(iwlg)
     ELSE IF (F_Norm_RF == 3) THEN
        fluxm(iwlg) = alpha_m * rf_star_m%Val(iwlg)
        fluxp(iwlg) = alpha_p * rf_star_p%Val(iwlg)
     ELSE
        fluxm(iwlg) = 0.0_dp
        fluxp(iwlg) = 0.0_dp
     ENDIF
     WRITE (60, '(1p,9e15.6E3)') wlth, fluxm(iwlg), rf_incIm%Val(iwlg) &
                                   , fluxp(iwlg), rf_incIp%Val(iwlg) &
                                   , sfl_d(0,0), sfl_d(0,npo), sfl_g(0,0), sfl_g(0,npo)

     ! 19 XII 2005 - Use "REAL" units (specific intensity)
     Iinc(-nmui:-1) = rf_incIm%Val(iwlg)
     Iinc(1:nmui)   = rf_incIp%Val(iwlg)

     ! JLB - 4 IX 08 : Checked after outgoing intensities have been computed
     !                 xmui_M(1) = 0.99... so theta = 0 is on +1 and theta = pi on -1
     ! Dont forget to update also subroutine AUX_YIT1 where Iinc(1) is used
     Iinc(-1) = rf_incIm%Val(iwlg) + fluxm(iwlg)
     Iinc(1)  = rf_incIp%Val(iwlg) + fluxp(iwlg)

! STEP 10 - Start iteration with an appropriate guess for Y and Q

! 10a - Compute Y_o = R^-1 x F_approx
!       F_approx ~ sfm / 1 - albed + Iexp(kt), THEN it IS wavelength and tau dependent

     CALL AUX_YIT1(Iinc, xkm, tl(:,iwlg), albed(:,iwlg), rlmi, sfm, Yit_o)

! 10b - Compute Q_o = R^-1 x R' x Y_o

     CALL AUX_QIT(rlmip, Yit_o, Qit)

!    nnit = 200
!    nnit = 20
     nnit = MAX(10, ifaf)

     !Git = 0.0_dp !old procedure if Git = 0 and Qit = 0
     !Qit = 0.0_dp !tests javi

     DO it = 1, nnit ! begin loop :: 101

! STEP 11 - Compute D  - Simple trapezoidal rule

        DO m = 1, mleg
           QmG(0:npo) = Qit(-m,0:npo) - Git(-m,0:npo)
           DO k_ilk = 1, md_m(m)
              n = ndp_m(k_ilk,m) + 1
              IF (n > npo) CYCLE
              T_m(n,m) = (QmG(n-1) / Emk_m_cmp(n-1,m) &
                       + QmG(n) / Emk_m_cmp(n,m)) * dtl(n-1)
              DO n = ndp_m(k_ilk,m)+2, ndq_m(k_ilk,m)
                 T_m(n,m) = T_m(n-1,m) + (QmG(n-1) / Emk_m_cmp(n-1,m) &
                                       + QmG(n) / Emk_m_cmp(n,m)) * dtl(n-1)
              ENDDO
              i0 = ndq_m(k_ilk,m)+1
              IF (i0 > npo) THEN
                 R_m(k_ilk,m) = 0.0_dp
              ELSE
                 T_m(i0,m) = T_m(i0-1,m) + (QmG(i0-1) / Emk_m_cmp(i0-1,m)) * dtl(i0-1)
                 R_m(k_ilk,m) = (QmG(i0) / Emk_m_cmp(i0,m)) * dtl(i0-1)
              ENDIF
           ENDDO
        ENDDO
        T_m(:,:) = T_m(:,:) * 0.5_dp
        R_m(:,:) = R_m(:,:) * 0.5_dp

        Dit(-mleg:-1,0) = 0.0_dp
        DO m = -mleg, -1
           DO k_ilk = 1, md_m(-m)
              taux1 = 0.0_dp
              DO i_ilk = 1, k_ilk-1
                 n0 = nn_m(k_ilk,-m) - nn_m(i_ilk,-m)
                 taux1 = taux1 + T_m(ndq_m(i_ilk,-m)+1,-m) * expgr(MIN(mgr,n0))
                 n0 = nn_m(k_ilk,-m) - nn_m(i_ilk+1,-m)
                 taux1 = taux1 + R_m(i_ilk,-m) * expgr(MIN(mgr,n0))
              ENDDO
              n = ndp_m(k_ilk,-m)
              Dit(m,n) = taux1 * Emk_m_cmp(n,-m)
              DO n = ndp_m(k_ilk,-m)+1, ndq_m(k_ilk,-m)
                 Dit(m,n) = (taux1 + T_m(n,-m)) * Emk_m_cmp(n,-m)
              ENDDO
           ENDDO
        ENDDO

        DO m = 1, mleg
           QmG(0:npo) = Qit(m,0:npo) - Git(m,0:npo)
           DO k_ilk = 1, md_p(m)
              n = ndq_p(k_ilk,m) - 1
              IF (n < 0) CYCLE
              T_p(n,m) = (QmG(n+1) / Emk_p_cmp(n+1,m) &
                       + QmG(n) / Emk_p_cmp(n,m)) * dtl(n)
              DO n = ndq_p(k_ilk,m)-2, ndp_p(k_ilk,m), -1
                 T_p(n,m) = T_p(n+1,m) + (QmG(n+1) / Emk_p_cmp(n+1,m) &
                                       + QmG(n) / Emk_p_cmp(n,m)) * dtl(n)
              ENDDO
              i0 = ndp_p(k_ilk,m) - 1
              IF (i0 == -1) THEN
                 R_p(k_ilk,m) = 0.0_dp
              ELSE
                 T_p(i0,m) = T_p(i0+1,m) + (QmG(i0+1) / Emk_p_cmp(i0+1,m)) * dtl(i0)
                 R_p(k_ilk,m) = (QmG(i0) / Emk_p_cmp(i0,m)) * dtl(i0)
              ENDIF
           ENDDO
        ENDDO
        T_p(:,:) = T_p(:,:) * 0.5_dp
        R_p(:,:) = R_p(:,:) * 0.5_dp

        Dit(1:mleg,npo) = 0.0_dp
        DO m = 1, mleg
           DO k_ilk = 1, md_p(m)
              taux1 = 0.0_dp
              DO i_ilk = 1, k_ilk-1
                 n0 = nn_p(k_ilk,m) - nn_p(i_ilk,m)
                 taux1 = taux1 + T_p(ndp_p(i_ilk,m)-1,m) * expgr(MIN(mgr,n0))
                 n0 = nn_p(k_ilk,m) - nn_p(i_ilk+1,m)
                 taux1 = taux1 + R_p(i_ilk,m) * expgr(MIN(mgr,n0))
              ENDDO
              n = ndq_p(k_ilk,m)
              Dit(m,n) = taux1 * Emk_p_cmp(n,m)
              DO n = ndq_p(k_ilk,m)-1, ndp_p(k_ilk,m), -1
                 Dit(m,n) = (taux1 + T_p(n,m)) * Emk_p_cmp(n,m)
              ENDDO
           ENDDO
        ENDDO

! STEP 12 - Compute Hit

        Hit = 0.0_dp
        DO i = 1, nmui
           taux1 = 0.0_dp
           taux2 = 0.0_dp
           DO m = 1, mleg
              taux1 = taux1 + Dit(-m,npo) * Tim(i,-m)
              taux2 = taux2 + Dit(m,0) * Tim(-i,m)
           ENDDO
           Hit(-i) = Iinc(-i) - taux2
           Hit(i) = Iinc(i) + taux1
        ENDDO

! STEP 13 - solve using LAPACK

        aam(1:nmui,1:mleg) = Bim(-nmui:-1,-mleg:-1)
        aam(1:nmui,mleg+1:2*mleg) = Bim(-nmui:-1,1:mleg)
        aam(nmui+1:nl,1:mleg) = Bim(1:nmui,-mleg:-1)
        aam(nmui+1:nl,mleg+1:nl) = Bim(1:nmui,1:mleg)

        bbm(1:nmui,1) = Hit(-nmui:-1)
        bbm(nmui+1:nl,1) = Hit(1:nmui)

        nrhs = 1
        CALL DGESVX (FACT, TRANS, nl, nrhs, aam, nl, aamf, nl, indx, EQUED, &
             R, C, bbm, nl, bbmf, nl, RCOND, FERR, BERR, WORK, IWORK, info)

        Camp(-nmui:-1) = bbmf(1:nmui,1)
        Camp(1:nmui) = bbmf(nmui+1:nl,1)

! STEP 14 - Compute Y

        DO n = 0, npo
           DO m = 1, mleg
              Yit(-m,n) = Camp(-m) * Emk_m_val(n,m) - Dit(-m,n)
              Yit(m,n)  = Camp(m)  * Emk_p_val(n,m) + Dit(m,n)
           ENDDO
        ENDDO

! STEP 15 - Compute new Q

        Qit_o = Qit

        CALL AUX_QIT(rlmip, Yit, Qit)

! STEP 16 - convergence ?

        auxil(0) = 0.0_dp
        taux2 = 0.0_dp
        DO n = 0, npo
           DO m = -mleg, -1
              IF (Qit(m,n) /= 0.0_dp) THEN
                 auxil(m) = ABS((Qit(m,n)-Qit_o(m,n))/Qit(m,n))
              ELSE
                 auxil(m) = 0.0_dp
              ENDIF
           ENDDO
           DO m = 1, mleg
              IF (Qit(m,n) /= 0.0_dp) THEN
                 auxil(m) = ABS((Qit(m,n)-Qit_o(m,n))/Qit(m,n))
              ELSE
                 auxil(m) = 0.0_dp
              ENDIF
           ENDDO
           taux2 = taux2 + SUM(auxil)
        ENDDO

        IF (taux2 < 1.0e-3_dp) THEN
           EXIT      !EXITs sub-iteration loop
        ENDIF

     ENDDO        ! Exits loop to find q  :: 101

! STEP 17 - Compute radiation field - mean intensity
!           intJm is 0.5 SUM(-1->0) of I
!           intJp is 0.5 SUM(0->1) of I
!           intJmp is 0.5 SUM(-1->1) of I
!           Thus intJm + intJp = J in erg cm-2 s-1 A-1 sterad-1
!           one gets u in erg cm-3 A-1 by u = J * 4 pi / c

     DO n = 0, npo
        taux1 = 0.0_dp
        taux2 = 0.0_dp
        taux3 = 0.0_dp
        DO m = 1, mleg
           taux1 = taux1 + Yit(-m,n) * (1.0_dp + Srm(m,n)) &
                         + Yit(m,n) * (1.0_dp - Srm(m,n))
           taux2 = taux2 + Yit(-m,n) * (1.0_dp - Srm(m,n)) &
                         + Yit(m,n) * (1.0_dp + Srm(m,n))
           taux3 = taux3 + Yit(-m,n) +  Yit(m,n)
        ENDDO

        ! 19 XII 2005 - Change normalisation
        intJm(n,iwlg) = 0.5_dp * taux1
        intJp(n,iwlg) = 0.5_dp * taux2
        intJmp(n,iwlg) = taux3
     ENDDO

     ! Compute escape probabilities
     ! Half system only
     DO n = 0, npo        !position
        CALL AUX_BP(rlm, pli, n, aam)
        bbm(1:mleg,1) = 1.0_dp

        CALL DGESVX (FACT, TRANS, mleg, nrhs, aam, nl, aamf, nl, indx, EQUED, &
                     R, C, bbm, nl, bbmf, nl, RCOND, FERR, BERR, WORK, IWORK, info)

        taux1 = 0.0_dp
        DO m = mleg, 1, -1
           taux1 = taux1 + bbmf(m,1) * Emk_p_val(n,m)
        ENDDO
        bbspp(n,iwlg) = taux1
     ENDDO

     DO n = 0, npo
        CALL AUX_BM(rlm, pli, n, aam)
        bbm(1:mleg,1) = 1.0_dp

        CALL DGESVX (FACT, TRANS, mleg, nrhs, aam, nl, aamf, nl, indx, EQUED, &
                     R, C, bbm, nl, bbmf, nl, RCOND, FERR, BERR, WORK, IWORK, info)

        taux1 = 0.0_dp
        DO m = mleg, 1, -1
           taux1 = taux1 + bbmf(m,1) * Emk_m_val(n,m)
        ENDDO
        bbspm(n,iwlg) = taux1
     ENDDO

     enor_m = bbspm(0,iwlg)
     enor_p = bbspp(npo,iwlg)
     IF (enor_p == 0.0_dp) THEN
        enor_p = 1.0_dp
     ENDIF

     DO n = 0, npo
        bbspm(n,iwlg) = bbspm(n,iwlg) / enor_m
        bbspp(n,iwlg) = bbspp(n,iwlg) / enor_p
     ENDDO

     IF (MOD(iwlg,100) == 0) THEN
        WRITE(iscrn,'("iwlg = ",I6," wlth[A] = ",F16.3,"  intJmp: edge = ",1p,E12.4," middle =",1p,E12.4)') &
              iwlg, wlth, intJmp(0,iwlg), intJmp(INT(npo/2),iwlg)
     ENDIF

     ! Compute emerging specific intensities
     DO l = 0, lleg
        fl_0(l) = 0.0_dp
        fl_max(l) = 0.0_dp
        DO m = 1, mleg
           fl_0(l) = fl_0(l) + rlm(l,m,0) * Yit(m,0) + rlm(l,-m,0) * Yit(-m,0)
           fl_max(l) = fl_max(l) + rlm(l,m,npo) * Yit(m,npo) + rlm(l,-m,npo) * Yit(-m,npo)
        ENDDO
     ENDDO

     DO i = 1, nmui
        I_esc_0(iwlg,-i) = 0.0_dp
        I_esc_0(iwlg,i)  = 0.0_dp
        I_esc_max(iwlg,-i) = 0.0_dp
        I_esc_max(iwlg,i)  = 0.0_dp
        DO l = 0, lleg
           I_esc_0(iwlg,-i) = I_esc_0(iwlg,-i) + (2.0_dp*l+1.0_dp) * fl_0(l) * pli(l,-i)
           I_esc_0(iwlg,i)  = I_esc_0(iwlg,i)  + (2.0_dp*l+1.0_dp) * fl_0(l) * pli(l,i)
           I_esc_max(iwlg,-i) = I_esc_max(iwlg,-i) + (2.0_dp*l+1.0_dp) * fl_max(l) * pli(l,-i)
           I_esc_max(iwlg,i)  = I_esc_max(iwlg,i)  + (2.0_dp*l+1.0_dp) * fl_max(l) * pli(l,i)
        ENDDO
     ENDDO
     WRITE (36,'(1p,163e15.6)') wlth, (I_esc_0(iwlg,i), i=-nmui,nmui), (I_esc_max(iwlg,i), i=-nmui,nmui)

     DEALLOCATE (md_m)
     DEALLOCATE (md_p)
     DEALLOCATE (ndp_m)
     DEALLOCATE (ndq_m)
     DEALLOCATE (ndp_p)
     DEALLOCATE (ndq_p)
     DEALLOCATE (R_m)
     DEALLOCATE (R_p)
     DEALLOCATE (nn_m)
     DEALLOCATE (nn_p)
  ENDDO    !end wavelenght loop :: 99  !too slow !!

  CLOSE (36)
  CLOSE (37)
  CLOSE (60)

  IF (F_W_RF_ALB == 1) THEN
     ! Write here full radiation field
     fichier = TRIM(out_dir)//TRIM(modele)//'.rf'
     OPEN(60, file=fichier, status="unknown" , form = 'unformatted')
     WRITE (60) rf_wl%upper-rf_wl%lower+1 ! number of pixels in wavelength grid
     WRITE (60) rf_wl%Val                 ! Wavelength grid in Angstroms
     DO n = 0, npo
        WRITE (60) tau_o(n)*tautav, intJmp(n,:) * J_to_u
     ENDDO
     CLOSE (60)
  ENDIF

  ! Free memory
  DEALLOCATE (xmunl)
  DEALLOCATE (pli)
  DEALLOCATE (p_0, q_0)
  DEALLOCATE (hl)
  DEALLOCATE (wr, wi)
  DEALLOCATE (xkm)
  DEALLOCATE (Emk_m_mag)
  DEALLOCATE (Emk_m_cmp)
  DEALLOCATE (Emk_m_val)
  DEALLOCATE (Emk_p_mag)
  DEALLOCATE (Emk_p_cmp)
  DEALLOCATE (Emk_p_val)
  DEALLOCATE (Smk_m, Smk_p)
  DEALLOCATE (QmG)
  DEALLOCATE (rlm, rlmp, rlmi, rlmip)
  DEALLOCATE (sfl, sfm, sfl_g, sfl_d)
  DEALLOCATE (Srm)
  DEALLOCATE (sigl)
  DEALLOCATE (tl, dtl, dedtl)
  DEALLOCATE (albed)
  DEALLOCATE (kap_gas, kap_dus, sca_dus, emm_dus)
  DEALLOCATE (g_absL)
  DEALLOCATE (aam, bbm)
  DEALLOCATE (aamf, bbmf)
  DEALLOCATE (Tim, Bim)
  DEALLOCATE (Iinc)
  DEALLOCATE (ggm)
  DEALLOCATE (Dit)
  DEALLOCATE (Qit, Qit_o, Git, Yit, Yit_o)
  DEALLOCATE (Hit)
  DEALLOCATE (R, C)
  DEALLOCATE (FERR, BERR)
  DEALLOCATE (WORK, IWORK)
  DEALLOCATE (Camp)
  DEALLOCATE (indx)
  DEALLOCATE (argu)
  DEALLOCATE (indmi, indmj)
  DEALLOCATE (fl_0)
  DEALLOCATE (fl_max)

!=-=-=-=-=-=-=-=-=-=-=-= NOT CHECKED FROM HERE jrg =-=-=-=-=-=-=-=-=-=-=-=-=

! 20 V 2003 - Compute here the effective radiation field scaling
! B - cut-off at 2400 A:

  CALL RF_CREATE(rf_lamI,0,nwlu)

  CALL RF_CREATE(rf_tmp,1,2)
  CALL RF_LOCATE(rf_wl, wlth0, i1)
  rf_tmp%Val(1) = 1.0_dp
  rf_tmp%Ord(1) = rf_wl%Ord(i1)
  CALL RF_LOCATE(rf_wl, wl_hab, i2)
  rf_tmp%Val(2) = 1.0_dp
  rf_tmp%Ord(2) = rf_wl%Ord(i2)

  int_rad1 = 0.0_dp
  int_rad2 = 0.0_dp

  ! Compute mean intensity from right side (use rf_u as auxiliary variable here)
  DO iwlg = 0, nwlu
     rf_u%Val(iwlg) = intJp(npo,iwlg)
  ENDDO
  mean_rad2 = SECT_INT(rf_tmp, rf_u, wl_pointer, wl_hab)

  DO iwlg = 0, nwlu
     rf_u%Val(iwlg) = intJmp(npo,iwlg) * J_to_u
  ENDDO
  rf_lamI%Val(0:nwlu) = rf_wl%Val(0:nwlu) * rf_u%Val(0:nwlu)

  int_rad2 = SECT_INT(rf_tmp, rf_u, wl_pointer, wl_hab)
  flu_rad2 = SECT_INT(rf_tmp, rf_lamI, wl_pointer, wl_hab) / (1.0e8_dp * xhp)

  PRINT *, " "
  PRINT *, " Scaled field at cloud edge"
  WRITE (iscrn,'("  U2:",1pe12.4," erg cm-3,     ","from 912 to",0pf9.2," Angstrom")') int_rad2, wl_hab
  WRITE (iscrn,'("  F2:",1pe12.4," phot cm-2 s-1,","from 912 to",0pf9.2," Angstrom")') flu_rad2, wl_hab
  WRITE (iwrit2,'("  U2:",1pe12.4," erg cm-3,     ","from 912 to",0pf9.2," Angstrom")') int_rad2, wl_hab
  WRITE (iwrit2,'("  F2:",1pe12.4," phot cm-2 s-1,","from 912 to",0pf9.2," Angstrom")') flu_rad2, wl_hab

  radp = int_rad2 / int_rad0

  ! Compute mean intensity from left side (use rf_u as auxiliary variable here)
  DO iwlg = 0, nwlu
     rf_u%Val(iwlg) = intJm(0,iwlg)
  ENDDO
  mean_rad1 = SECT_INT(rf_tmp, rf_u, wl_pointer, wl_hab)

  DO iwlg = 0, nwlu
     rf_u%Val(iwlg) = intJmp(0,iwlg) * J_to_u
  ENDDO
  rf_lamI%Val(0:nwlu) = rf_wl%Val(0:nwlu) * rf_u%Val(0:nwlu)

  int_rad1 = SECT_INT(rf_tmp, rf_u, wl_pointer, wl_hab)
  flu_rad1 = SECT_INT(rf_tmp, rf_lamI, wl_pointer, wl_hab) / (1.0e8_dp * xhp)

  WRITE (iscrn,'("  U1:",1pe12.4," erg cm-3,     ","from 912 to",0pf9.2," Angstrom")') int_rad1, wl_hab
  WRITE (iscrn,'("  F1:",1pe12.4," phot cm-2 s-1,","from 912 to",0pf9.2," Angstrom")') flu_rad1, wl_hab
  WRITE (iwrit2,'("  U1:",1pe12.4," erg cm-3,     ","from 912 to",0pf9.2," Angstrom")') int_rad1, wl_hab
  WRITE (iwrit2,'("  F1:",1pe12.4," phot cm-2 s-1,","from 912 to",0pf9.2," Angstrom")') flu_rad1, wl_hab

  radm = int_rad1 / int_rad0

  G0m = int_rad1 / u_habing
  G0p = int_rad2 / u_habing
  PRINT *, " "
  WRITE (iscrn,'("  G_0m :",1pe12.4," (in Habing units)")') G0m
  WRITE (iscrn,'("  G_0p :",1pe12.4," (in Habing units)")') G0p
  WRITE(iwrit2,'(/," U0:",1pe13.4, " erg cm-3")') int_rad0
  WRITE(iwrit2,'(" G_0m =", 1pe12.3, " in Habing units",/)') G0m
  WRITE(iwrit2,'(" G_0p =", 1pe12.3, " in Habing units",/)') G0p

  ! set here wavelength threshold for integrated intensity.
  CALL RF_LOCATE(rf_wl, wl_hab, i2)
  rf_tmp%Val(2) = 1.0_dp
  rf_tmp%Ord(2) = rf_wl%Ord(i2)
  int_rad(0) = SECT_INT(rf_tmp, rf_u, wl_pointer, wl_hab)

  PRINT *, " "
  PRINT *, " Effective Radiation field strength AT the edge:"
  WRITE (iscrn,'("  left :",1pe12.4," ",a15)') radm, ISRF_name
  WRITE (iscrn,'(" right :",1pe12.4," ",a15)') radp, ISRF_name
  WRITE (iwrit2,'("  left :",1pe12.4," ",a15)') radm, ISRF_name
  WRITE (iwrit2,'(" right :",1pe12.4," ",a15)') radp, ISRF_name
  PRINT *, " "

  CALL RF_FREE (rf_tmp)
  CALL RF_FREE (rf_lamI)

END SUBROUTINE ETIN90

!---------------------------------------------------------------------------

SUBROUTINE ETIN901S (nwlu)

 USE PXDR_CONSTANTES
 USE PXDR_CHEM_DATA
 USE PXDR_PROFIL
 USE PXDR_AUXILIAR
 USE PXDR_STR_DATA
 USE PXDR_RF_TOOLS

! Radiative transfer computation. See file transfer_S.pdf for definitions
! and comments. Index conventions:
!       l goes from 0 to lleg (first Legendre Pol. index)
!       m goes from +/- 1 to +/- mleg (second Legendre Pol. index)
!       i goes from +/- 1 to +/- nmui (discrete angles for boundary conditions)
!       n goes from 0 to npo (optical depth grid)
! 1 side version: tmax is infinity and radiation is impiging from mu_i > 0 only

 IMPLICIT NONE

 INTEGER, INTENT (IN)                          :: nwlu     ! Number of wavelength computed

 INTEGER                                       :: l, m, n, n1, n3

 ! Radiative quantities
 INTEGER                                       :: iwlg
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: tl       ! Full optical depth
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: dtl      ! Full optical depth increment
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: dedtl    ! Full optical depth increment
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: sigl     ! Phase function

 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: albed    ! Reduced albedo (including gas)

 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: kap_gas
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: kap_dus
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: sca_dus
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: emm_dus
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: Camp     ! Legendre amplitudes

 type(rf_type)                                 :: rf_tmp, rf_lamI

 ! Legendre polynomial related variables
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: xmunl    ! -1 power l  = (-1)**l
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: pli      ! P_l(mu_i)
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: p_0
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: q_0

 ! Matrix A coefficients
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: hl
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: wr, wi
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: xkm      ! Legendre absissa
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Smk_m    ! integrated xkm
 ! Smk_m = Emk_m_mag * grand + s => Exp(-Smk_m) = G**Emk_m_mag * Emk_m_cmp
 !       with G = exp(-grand) and Emk_m_cmp = exp(-s)
 INTEGER (KIND=ilk), DIMENSION(:,:), ALLOCATABLE :: Emk_m_mag! Exp(-Smk_m) 0 -> t - power of G
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Emk_m_cmp! Exp(-Smk_m) 0 -> t - remaining part
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Emk_m_val! Exp(-Smk_m) 0 -> t - Total value
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Emk_p_val! Exp(-Smk_m) 0 -> t - Total value
 INTEGER (KIND=ilk), DIMENSION(:), ALLOCATABLE :: md_m
 INTEGER,        DIMENSION(:,:),   ALLOCATABLE :: ndp_m, ndq_m
 INTEGER (KIND=ilk), DIMENSION(:,:), ALLOCATABLE :: nn_m
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: T_m
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: R_m
 INTEGER (KIND=ilk)                            :: k_ilk, i_ilk

 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: QmG
 REAL (KIND=dp), DIMENSION(:,:,:), ALLOCATABLE :: rlm      ! Eigenvectors matrix
 REAL (KIND=dp), DIMENSION(:,:,:), ALLOCATABLE :: rlmp     ! derivative
 REAL (KIND=dp), DIMENSION(:,:,:), ALLOCATABLE :: rlmi     ! inverse of Rlm(t)

 ! Matrix product of R^-1 x R'
 REAL (KIND=dp), DIMENSION(:,:,:), ALLOCATABLE :: rlmip
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Srm

! Source function contributions

 ! Source function expansion coeficients (as function of l and m respectively)
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: sfl, sfm, sfl_g, sfl_d
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: ggm      ! Inhomogeneous contribution (g vector in notes)
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Git      ! Product R^-1*A^-1*g vector, same DIMENSIONs as Qit

 ! Boundary conditions related variables
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Tim      ! Auxiliary
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Bim      ! BC matrix
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: Iinc     ! Incident radiation field

 ! Iteration related variables
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Qit
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Qit_o
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Dit
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Yit
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: Yit_o
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: Hit

! LAPACK related variables

 ! Eigenvalues only
 CHARACTER (LEN=1)                             :: JOBZ  = 'N'
 REAL (KIND=dp), DIMENSION(:),     ALLOCATABLE :: WORK         ! Real work array
 INTEGER,        DIMENSION(:),     ALLOCATABLE :: IWORK        ! Work array
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: aam, bbm
 REAL (KIND=dp), DIMENSION(:,:),   ALLOCATABLE :: aamf, bbmf
 INTEGER                                       :: info
 CHARACTER (LEN=1)                             :: FACT  = 'E'  ! Equilibrate system
 CHARACTER (LEN=1)                             :: TRANS = 'N'  ! Transpose
 INTEGER                                       :: nrhs
 CHARACTER (LEN=1)                             :: EQUED        ! Kind of equilibration
 REAL (KIND=dp), DIMENSION(:), ALLOCATABLE     :: R, C         ! Equilibration factors
 REAL (KIND=dp), DIMENSION(:), ALLOCATABLE     :: FERR, BERR   ! Errors (estimates)
 REAL (KIND=dp)                                :: RCOND        ! Condition number
 INTEGER, DIMENSION(:), ALLOCATABLE            :: indx

 ! Auxiliaries
 INTEGER                                       :: nl, nnit
 INTEGER                                       :: it, i, i0
 INTEGER                                       :: nb_m
 INTEGER (KIND=ilk)                            :: n0, le_n
 INTEGER                                       :: i1, i2
 REAL (KIND=dp)                                :: ht, ymui
 REAL (KIND=dp)                                :: enor_m, enor_p
 REAL (KIND=dp)                                :: taux1, taux2, taux3
 REAL (KIND=dp)                                :: taux4
 REAL (KIND=dp)                                :: wlth, wlthcm
 REAL (KIND=dp)                                :: deriv_d
 REAL (KIND=dp), DIMENSION(:), ALLOCATABLE     :: argu
 REAL (KIND=dp), DIMENSION(-mleg:mleg)         :: auxil
 INTEGER, DIMENSION(:,:), ALLOCATABLE          :: indmi, indmj

 ! Thermalized (black body) emission of dust and gas
 REAL (KIND=dp)                                :: bbgas

 ! Computation of emerging specific intensities
 REAL (KIND=dp), DIMENSION(:), ALLOCATABLE     :: fl_0

!-----------------------------------------------------------------------------

  ! Number of Legendred coefficient (used for loops that run from 1 to lleg+1)
  nl = lleg + 1

  IF (ALLOCATED(g_absL)) THEN
     DEALLOCATE (g_absL)
  ENDIF

  IF (ALLOCATED(intJm)) THEN
     DEALLOCATE (intJm, intJp)
     DEALLOCATE (bbspm, bbspp)
  ENDIF

  IF (ALLOCATED(fluxm)) DEALLOCATE(fluxm)
  IF (ALLOCATED(I_esc_0)) DEALLOCATE(I_esc_0)
  IF (ALLOCATED(abs_tot)) DEALLOCATE(abs_tot)
  IF (ALLOCATED(intJmp)) DEALLOCATE(intJmp)

  ALLOCATE (xmunl(0:lleg))
  ALLOCATE (pli(0:lleg,-nmui:nmui))
  ALLOCATE (p_0(0:lleg), q_0(0:lleg))
  ALLOCATE (hl(0:lleg,0:npo))
  ALLOCATE (wr(lleg+1), wi(lleg+1))
  ALLOCATE (xkm(-mleg:mleg,0:npo))
  ALLOCATE (Smk_m(0:npo,mleg))
  ALLOCATE (Emk_m_mag(0:npo,mleg))
  ALLOCATE (Emk_m_cmp(0:npo,mleg))
  ALLOCATE (Emk_m_val(0:npo,mleg))
  ALLOCATE (Emk_p_val(0:npo,mleg))
  ALLOCATE (T_m(0:npo,mleg))

  ALLOCATE (QmG(0:npo))
  ALLOCATE (rlm(0:lleg,-mleg:mleg,0:npo))
  ALLOCATE (rlmp(0:lleg,-mleg:mleg,0:npo))
  ALLOCATE (rlmi(0:lleg,-mleg:mleg,0:npo))
  ALLOCATE (rlmip(0:lleg,-mleg:mleg,0:npo))
  ALLOCATE (sfm(-mleg:mleg,0:npo))
  ALLOCATE (sfl(0:lleg,0:npo))
  ALLOCATE (sfl_g(0:lleg,0:npo),sfl_d(0:lleg,0:npo))
  ALLOCATE (ggm(-mleg:mleg,0:npo))
  ALLOCATE (Srm(mleg,0:npo))
  ALLOCATE (sigl(0:lleg,0:npo))
  ALLOCATE (dtl(0:npo))
  ALLOCATE (dedtl(0:npo))
  ALLOCATE (tl(0:npo,0:nwlu))
  ALLOCATE (albed(0:npo,0:nwlu))
  ALLOCATE (kap_gas(0:npo))
  ALLOCATE (kap_dus(0:npo))
  ALLOCATE (sca_dus(0:npo))
  ALLOCATE (emm_dus(0:npo))
  ALLOCATE (g_absL(0:npo,0:nwlu))
  ALLOCATE (intJm(0:npo,0:nwlu))
  ALLOCATE (intJp(0:npo,0:nwlu))
  ALLOCATE (intJmp(0:npo,0:nwlu))
  ALLOCATE (bbspm(0:npo,0:nwlu))
  ALLOCATE (bbspp(0:npo,0:nwlu))
  ALLOCATE (R(nl), C(nl))
  ALLOCATE (FERR(1), BERR(1))
  ALLOCATE (WORK(4*nl))
  ALLOCATE (IWORK(nl))
  ALLOCATE (aam(nl,nl), bbm(nl,1))
  ALLOCATE (aamf(nl,nl), bbmf(nl,1))
  ALLOCATE (indx(nl))
  ALLOCATE (Tim(-nmui:nmui,-mleg:mleg))
  ALLOCATE (Bim(-nmui:nmui,-mleg:mleg))
  ALLOCATE (Iinc(-nmui:nmui))
  ALLOCATE (Qit(-mleg:mleg,0:npo))
  ALLOCATE (Qit_o(-mleg:mleg,0:npo))
  ALLOCATE (Git(-mleg:mleg,0:npo))
  ALLOCATE (Dit(-mleg:mleg,0:npo))
  ALLOCATE (Yit(-mleg:mleg,0:npo))
  ALLOCATE (Yit_o(-mleg:mleg,0:npo))
  ALLOCATE (Hit(-nmui:nmui))
  ALLOCATE (Camp(-nmui:nmui))
  ALLOCATE (argu(0:npo))
  ALLOCATE (indmi(mleg,0:npo), indmj(mleg,0:npo))
  ALLOCATE (fl_0(0:lleg))
  ALLOCATE (I_esc_0(0:nwlu,-nmui:nmui))
  ALLOCATE (fluxm(0:nwlu))
  ALLOCATE (abs_tot(0:nwlu))

  p_0(:)         = 0.0_dp
  q_0(:)         = 0.0_dp
  sigl(:,:)      = 0.0_dp
  Tim(:,:)       = 0.0_dp
  Bim(:,:)       = 0.0_dp
  Iinc(:)        = 0.0_dp
  xkm(:,:)       = 0.0_dp
  Qit(:,:)       = 0.0_dp
  Qit_o(:,:)     = 0.0_dp
  Yit(0,0:npo)   = 0.0_dp
  Yit_o(0,0:npo) = 0.0_dp
  I_esc_0(:,:)   = 0.0_dp

  ! This is just (-1)**l
  xmunl(0) = 1.0_dp
  DO l = 1, lleg
     xmunl(l) = - xmunl(l-1)
  ENDDO

! STEP 0 - Initialise radiative quantities
!  (boundary conditions and optical depth grid)

  ! uvspec.out is the total absorption through the cloud. multiply by
  !            incident radition field to compute spectrum.
  fichier = TRIM(out_dir)//TRIM(modele)//'.Iesc'
  PRINT *, " Escaping intensities in:", TRIM(fichier)
  OPEN (36, file=fichier, status="unknown")
  fichier = TRIM(out_dir)//TRIM(modele)//'.uv'
  PRINT *, " Absorption in:", TRIM(fichier)
  OPEN (37, file=fichier, status="unknown")
  fichier = TRIM(out_dir)//TRIM(modele)//'.flin'
  PRINT *, " External field:", TRIM(fichier)
  OPEN (60, file=fichier, status="unknown")
  WRITE(60,*) "4      # Number of columns"

  ! Compute absorption by gas
  CALL ABSGAZ (nwlu)

! STEP 1 - Compute P_l(mu_i) - see recurence in Num Rec
  WRITE(iscrn,'("       Point             Angstroms       erg cm-2 s-1 A-1 sr-1")')
  ! Example on screen : iwlg =   100 wlth =        925.259  intJmp: edge =   4.5370E-08 middle =  9.0686E-26

  pli(0,:) = 1.0_dp
  DO i = 1, nmui
     ymui = xmui_M(i)
     pli(1,-i) = -ymui
     pli(1,i) = ymui
     DO l = 1, lleg-1
        pli(l+1,-i) = (-ymui*(2.0_dp*l+1.0_dp) * pli(l,-i) - l * pli(l-1,-i)) / (l+1.0_dp)
        pli(l+1,i) = xmunl(l+1) * pli(l+1,-i)
     ENDDO
  ENDDO

  p_0(0) = 1.0_dp
  q_0(1) = p_0(0) / 2.0_dp
  DO l = 1, lleg/2
     p_0(2*l) = - (2.0_dp*l-1) * p_0(2*l-2) / (2.0_dp*l)
     q_0(2*l+1) = p_0(2*l) / (2.0_dp*l+2.0_dp)
  ENDDO

  DO iwlg = 0, nwlu
     ! sigl = gg**l (Formule 48, Henyey-Greenstein phase function)
     DO n = 0, npo
        sigl(0,n) = 1.0_dp
        DO l = 1, lleg
           sigl(l,n) = sigl(l-1,n) * du_prop%gcos(iwlg,n)
        ENDDO
     ENDDO

     ! a - Reset to 0 most variables
     hl = 0.0_dp
     wr = 0.0_dp
     wi = 0.0_dp
     tl(:,iwlg) = 0.0_dp
     dtl = 0.0_dp
     dedtl = 0.0_dp
     albed(:,iwlg) = 0.0_dp
     kap_gas = 0.0_dp
     kap_dus = 0.0_dp
     sca_dus = 0.0_dp
     emm_dus = 0.0_dp

     wlth = rf_wl%Val(iwlg)
     wlthcm = wlth * 1.0e-8_dp

     ! Compute local values of absorption coef for dust and gas and dust scatering coef
     ! Compute effective albedo !
     DO n = 0, npo
        kap_gas(n) = g_absL(n,iwlg)
        kap_dus(n) = du_prop%abso(iwlg,n) * dens_o(n)
        sca_dus(n) = du_prop%scat(iwlg,n) * dens_o(n)
        emm_dus(n) = du_prop%emis(iwlg,n) * dens_o(n)
        albed(n,iwlg) = sca_dus(n) / (kap_dus(n) + kap_gas(n) + sca_dus(n))
     ENDDO

     ! b - Compute optical depth grid
     !     Convert Delta Tau_v (dust only at V) to
     !     Delta tau_lambda (dust + gaz at Lambda)
     tl(0,iwlg) = 0.0_dp
     DO n = 0, npo-1
        ht = ((kap_dus(n) + kap_gas(n) + sca_dus(n)) &
           + (kap_dus(n+1) + kap_gas(n+1) + sca_dus(n+1))) &
           * 0.5_dp * dd_cm(n+1)
        dtl(n) = ht
        tl(n+1,iwlg) = tl(n,iwlg) + ht
     ENDDO
     DO n = 1, npo-1
        dedtl(n) = dtl(n-1) + dtl(n)
     ENDDO
     ! Compute total attenuation by dust + gas through whole cloud
     taux1 = tl(npo,iwlg)
     abs_tot(iwlg) = EXP(-taux1)
     WRITE (37,"(1p,3e17.7E3)") wlth, abs_tot(iwlg), taux1

! STEP 2a - Compute matrix A(tau)

     DO n = 0, npo
        ! Formulae A2 and A3 of Roberge:
        hl(0,n) = 1.0_dp - albed(n,iwlg)
        DO l = 1, lleg
           hl(l,n) = (2.0_dp*l+1.0_dp) * (1.0_dp - albed(n,iwlg) * sigl(l,n))
        ENDDO
        DO l = 1, lleg
           wi(l) = DBLE(l) / SQRT(hl(l-1,n)*hl(l,n))
        ENDDO
        wr = 0.0_dp

        ! Compute eigenvalues (tridiagonal symetric matrix) - LAPACK
        CALL DSTEVD(JOBZ, nl, wr, wi, aam, nl, WORK, 4*nl, IWORK, nl, info )

        ! order eigenvalues by decreasing order
        wi = wr
        DO l = 1, nl
           wr(l) = wi(nl+1-l)
        ENDDO

        ! km = 1 / lamda_m in increasing order
        DO m = 1, mleg
           xkm(m,n) = 1.0_dp / wr(m)
           xkm(-m,n) = -xkm(m,n)
        ENDDO
     ENDDO   ! end loop in npo

     ! Calcul des Rlm: Formule A9 de Roberge 1983
     ! Eigenvectors computed only from recurrence relations.
     DO n = 0, npo
        DO m = 1, mleg
           rlm(0,m,n)  = 1.0_dp
           rlm(0,-m,n) = 1.0_dp
           rlm(1,m,n)  = (1.0_dp - albed(n,iwlg)) / xkm(m,n)
           rlm(1,-m,n) = -rlm(1,m,n)                             !jrg
           DO l = 2, lleg
              rlm(l,m,n) = (hl(l-1,n) * rlm(l-1,m,n) &
                         - (l-1) * xkm(m,n) * rlm(l-2,m,n)) / (l*xkm(m,n))
              rlm(l,-m,n) = xmunl(l) * rlm(l,m,n)
           ENDDO
        ENDDO
     ENDDO   ! end loop in npo

! STEP 2c - Compute Srm - used for mean intensity

     CALL AUX_Srm(q_0, rlm, Srm)

! STEP 3 - Compute Smk_m = Sum (xkm) (left to right)
!        - Simple Simpson rule

     Smk_m(0,:) = 0.0_dp
     DO m = 1, mleg
        DO n = 1, npo
           Smk_m(n,m) = Smk_m(n-1,m) + 0.5_dp * dtl(n-1) * (xkm(m,n) + xkm(m,n-1))
        ENDDO
     ENDDO

     DO m = 1, mleg
        Emk_m_mag(:,m) = INT(MIN(Smk_m(:,m),1.0e18_dp) / grand, ilk)
        Emk_m_cmp(:,m) = EXP(-(Smk_m(:,m)-grand*Emk_m_mag(:,m)))
        Emk_m_val(:,m) = expgr(MIN(mgr,Emk_m_mag(:,m))) * Emk_m_cmp(:,m)
     ENDDO
     ALLOCATE (md_m(mleg))
     md_m(:) = Emk_m_mag(npo,:)
     ALLOCATE (ndp_m(0:npo+1,mleg))
     ALLOCATE (ndq_m(0:npo+1,mleg))
     ALLOCATE (R_m(npo+1,mleg))
     ALLOCATE (nn_m(npo+1,mleg))
     ndp_m = 0
     ndq_m = 0

     DO m = 1, mleg
        le_n = Emk_m_mag(0,m)
        nb_m = 1
        ndp_m(1,m) = 0
        nn_m(nb_m,m) = le_n
        DO n = 1, npo
           IF (Emk_m_mag(n,m) == le_n) THEN
              CYCLE
           ELSE
              ndq_m(nb_m,m) = n - 1
              nn_m(nb_m,m) = le_n
              le_n = Emk_m_mag(n,m)
              nb_m = nb_m + 1
              ndp_m(nb_m,m) = n
           ENDIF
        ENDDO
        ndq_m(nb_m,m) = npo
        nn_m(nb_m,m) = le_n
        md_m(m) = nb_m
     ENDDO

     Emk_p_val = 0.0_dp      ! Tau_max = infty

! STEP 4 - Compute R**-1    !Loop to all cloud opacity points

     CALL AUX_Rlmi(rlm, hl, rlmi)

! STEP 5 - Compute source function contribution g~ = R^-1*A^-1*g (in notes)
!          JLB : Check Javier correction (not yet done here)

     DO n = 0, npo
        ! Ex. Black body emission of thermalized gas
        IF (wlthcm*tgaz(n) > 1.0e-2_dp) THEN
           bbgas = 2.0_dp*xhp*clum**2 / (wlthcm)**5
           bbgas = bbgas * 1.0e-8_dp / (EXP(hcsurk/(wlthcm*tgaz(n))) -1.0_dp)
        ELSE
           bbgas = 0.0_dp
        ENDIF

! 5a - Assign source function, isotropic  (PROVISIONAL)

        ! METHOD B1: approx. to continuum thermal isotropic emission
        sfl_d(0,n) = emm_dus(n) / (kap_dus(n) + kap_gas(n) + sca_dus(n))

        ! METHOD B2: approx. to line thermal isotropic emission
        sfl_g(0,n) = bbgas * kap_gas(n) / (kap_dus(n) + kap_gas(n) + sca_dus(n))

        ! Add both contributions
!       sfl(0,n) = sfl_g(0,n) + sfl_d(0,n)
        sfl(0,n) = sfl_d(0,n)
!       sfl(0,n) = 0.0_dp

        sfl(1:lleg,n) = 0.0_dp
     ENDDO

     ! Put them in m,n desired order
     sfm(-mleg:-1,:) = sfl(0:mleg-1,:)
     sfm(1:mleg,:) = sfl(mleg:lleg,:)

! 5b - Compute vector g

     DO n = 0, npo
        DO m = -mleg, -1
           l = mleg + m
           ggm(m,n) = -sfm(m,n) / ( 1.0_dp - albed(n,iwlg) * sigl(l,n) )
        ENDDO
        DO m = 1, mleg
           l = mleg + m - 1
           ggm(m,n) = -sfm(m,n) / ( 1.0_dp - albed(n,iwlg) * sigl(l,n) )
        ENDDO
     ENDDO

! 5d - Compute matrix product  R^-1*A^-1*g = Git

     CALL AUX_GIT(rlmi, ggm, xkm, Git)

! STEP 6 - Compute derivatives of Rlm

! 6c - derivative of Rlm

     DO n = 2, npo-2
        DO m = 1, mleg
           rlmp(0,m,n) = 0.0_dp
           rlmp(0,-m,n) = 0.0_dp
           n1 = n - 2
           n3 = n + 2
           DO l = 1, lleg
              ! Direct variable
              deriv_d = -rlm(l,m,n1) * (dtl(n)+dtl(n+1)) / ((dtl(n-2)+dtl(n-1)) * (dtl(n-2)+dtl(n-1)+dtl(n)+dtl(n+1))) &
                      - rlm(l,m,n)  * (dtl(n-2)+dtl(n-1)-dtl(n)-dtl(n+1)) / ((dtl(n)+dtl(n+1)) * (dtl(n-2)+dtl(n-1))) &
                      + rlm(l,m,n3) * (dtl(n-2)+dtl(n-1)) / ((dtl(n-2)+dtl(n-1)+dtl(n)+dtl(n+1)) * (dtl(n)+dtl(n+1)))
              rlmp(l,m,n) = deriv_d
              rlmp(l,-m,n) = xmunl(l) * rlmp(l,m,n)
           ENDDO
        ENDDO
     ENDDO

     ! Boundary condition. Replace par linear extrapolation (TODO)
     DO m = 1, mleg
        rlmp(0,m,0) = 0.0_dp
        rlmp(0,-m,0) = 0.0_dp
        rlmp(0,m,1) = 0.0_dp
        rlmp(0,-m,1) = 0.0_dp
        rlmp(0,m,npo-1) = 0.0_dp
        rlmp(0,-m,npo-1) = 0.0_dp
        rlmp(0,m,npo) = 0.0_dp
        rlmp(0,-m,npo) = 0.0_dp
        DO l = 1, lleg
           rlmp(l,m,0) = rlmp(l,m,2)
           rlmp(l,-m,0) = xmunl(l) * rlmp(l,m,0)
           rlmp(l,m,1) = rlmp(l,m,2)
           rlmp(l,-m,1) = xmunl(l) * rlmp(l,m,1)
           rlmp(l,m,npo-1) = rlmp(l,m,npo-2)
           rlmp(l,-m,npo-1) = xmunl(l) * rlmp(l,m,npo-1)
           rlmp(l,m,npo) = rlmp(l,m,npo-2)
           rlmp(l,-m,npo) = xmunl(l) * rlmp(l,m,npo-2)
        ENDDO
     ENDDO

! 6d - Multiply by R**-1

     CALL AUX_Rlmip(rlmi, rlmp, rlmip)

! STEP 7 - Compute Tim

     DO m = 1, mleg
        DO i = 1, nmui
           taux1 = 0.0_dp
           taux4 = 0.0_dp
           DO l = 0, lleg
              taux1 = taux1 + (2.0_dp * l + 1.0_dp) * rlm(l,-m,0) * pli(l,-i)
              taux4 = taux4 + (2.0_dp * l + 1.0_dp) * rlm(l,m,npo) * pli(l,i)
           ENDDO
           Tim(-i,-m) = taux1
           Tim(i,m)   = taux4
        ENDDO
     ENDDO

! STEP 8 - Compute Bim (Boundary conditions matrix)

     DO m = 1, mleg
        DO i = 1, nmui
           Bim(-i,-m) = Tim(-i,-m)
           Bim(-i,m) = 0.0_dp
           Bim(i,-m) = 0.0_dp
           Bim(i,m) = Tim(i,m)
        ENDDO
     ENDDO

! STEP 9 - Set boundary conditions

     ! Add potential // source (star)
     IF (F_Norm_RF == 1 .OR. F_Norm_RF == 3) THEN
        fluxm(iwlg) = alpha_m * rf_star_m%Val(iwlg)
     ELSE
        fluxm(iwlg) = 0.0_dp
     ENDIF
     WRITE (60, '(1p,4e15.6E3)') wlth, fluxm(iwlg), rf_incIm%Val(iwlg)

     ! 19 XII 2005 - Use "REAL" units (specific intensity)
     Iinc(-nmui:-1) = rf_incIm%Val(iwlg)
     Iinc(1:nmui)   = 0.0_dp

     ! JLB 4 IX 08 - Corrected. See other routine for comment
     Iinc(-1) = rf_incIm%Val(iwlg) + fluxm(iwlg)

! STEP 10 - Start iteration with an appropriate guess for Y and Q

! 10a - Compute Y_o = R^-1 x F_approx
!       F_approx ~ sfm / 1 - albed + Iexp(kt), THEN it IS wavelength and tau dependent

     CALL AUX_YIT1(Iinc, xkm, tl(:,iwlg), albed(:,iwlg), rlmi, sfm, Yit_o)

! 10b - Compute Q_o = R^-1 x R' x Y_o

     CALL AUX_QIT(rlmip, Yit_o, Qit)

!    nnit = 200
     nnit = MAX(10, ifaf)

     !Git = 0.0_dp !old procedure if Git = 0 and Qit = 0
     !Qit = 0.0_dp !tests javi

     DO it = 1, nnit ! begin loop :: 101

! STEP 11 - Compute D  - Simple trapezoidal rule

        DO m = 1, mleg
           QmG(0:npo) = Qit(-m,0:npo) - Git(-m,0:npo)
           DO k_ilk = 1, md_m(m)
              n = ndp_m(k_ilk,m) + 1
              IF (n > npo) CYCLE
              T_m(n,m) = (QmG(n-1) / Emk_m_cmp(n-1,m) &
                       + QmG(n) / Emk_m_cmp(n,m)) * dtl(n-1)
              DO n = ndp_m(k_ilk,m)+2, ndq_m(k_ilk,m)
                 T_m(n,m) = T_m(n-1,m) + (QmG(n-1) / Emk_m_cmp(n-1,m) &
                                       + QmG(n) / Emk_m_cmp(n,m)) * dtl(n-1)
              ENDDO
              i0 = ndq_m(k_ilk,m)+1
              IF (i0 > npo) THEN
                 R_m(k_ilk,m) = 0.0_dp
              ELSE
                 T_m(i0,m) = T_m(i0-1,m) + (QmG(i0-1) / Emk_m_cmp(i0-1,m)) * dtl(i0-1)
                 R_m(k_ilk,m) = (QmG(i0) / Emk_m_cmp(i0,m)) * dtl(i0-1)
              ENDIF
           ENDDO
        ENDDO
        T_m(:,:) = T_m(:,:) * 0.5_dp
        R_m(:,:) = R_m(:,:) * 0.5_dp

        Dit(-mleg:-1,0) = 0.0_dp
        DO m = -mleg, -1
           DO k_ilk = 1, md_m(-m)
              taux1 = 0.0_dp
              DO i_ilk = 1, k_ilk-1
                 n0 = nn_m(k_ilk,-m) - nn_m(i_ilk,-m)
                 taux1 = taux1 + T_m(ndq_m(i_ilk,-m)+1,-m) * expgr(MIN(mgr,n0))
                 n0 = nn_m(k_ilk,-m) - nn_m(i_ilk+1,-m)
                 taux1 = taux1 + R_m(i_ilk,-m) * expgr(MIN(mgr,n0))
              ENDDO
              n = ndp_m(k_ilk,-m)
              Dit(m,n) = taux1 * Emk_m_cmp(n,-m)
              DO n = ndp_m(k_ilk,-m)+1, ndq_m(k_ilk,-m)
                 Dit(m,n) = (taux1 + T_m(n,-m)) * Emk_m_cmp(n,-m)
              ENDDO
           ENDDO
        ENDDO

        Dit(1:mleg,:) = 0.0_dp

! STEP 12 - Compute Hit

        Hit = 0.0_dp
        DO i = 1, nmui
           taux1 = 0.0_dp
           taux2 = 0.0_dp
           DO m = 1, mleg
              taux1 = taux1 + Dit(-m,npo) * Tim(i,-m)
           ENDDO
           Hit(-i) = Iinc(-i) - taux2
           Hit(i) = 0.0_dp + taux1
        ENDDO

! STEP 13 - solve using LAPACK

        aam(1:nmui,1:mleg) = Bim(-nmui:-1,-mleg:-1)
        aam(1:nmui,mleg+1:2*mleg) = Bim(-nmui:-1,1:mleg)
        aam(nmui+1:nl,1:mleg) = Bim(1:nmui,-mleg:-1)
        aam(nmui+1:nl,mleg+1:nl) = Bim(1:nmui,1:mleg)

        bbm(1:nmui,1) = Hit(-nmui:-1)
        bbm(nmui+1:nl,1) = Hit(1:nmui)

        nrhs = 1
        CALL DGESVX (FACT, TRANS, nl, nrhs, aam, nl, aamf, nl, indx, EQUED, &
             R, C, bbm, nl, bbmf, nl, RCOND, FERR, BERR, WORK, IWORK, info)

        Camp(-nmui:-1) = bbmf(1:nmui,1)
        Camp(1:nmui) = bbmf(nmui+1:nl,1)

! STEP 14 - Compute Y

        DO n = 0, npo
           DO m = 1, mleg
              Yit(-m,n) = Camp(-m) * Emk_m_val(n,m) - Dit(-m,n)
              Yit(m,n)  = 0.0_dp
           ENDDO
        ENDDO

! STEP 15 - Compute new Q

        Qit_o = Qit

        CALL AUX_QIT(rlmip, Yit, Qit)

! STEP 16 - convergence ?

        auxil(0) = 0.0_dp
        taux2 = 0.0_dp
        DO n = 0, npo
           DO m = -mleg, -1
              IF (Qit(m,n) /= 0.0_dp) THEN
                 auxil(m) = ABS((Qit(m,n)-Qit_o(m,n))/Qit(m,n))
              ELSE
                 auxil(m) = 0.0_dp
              ENDIF
           ENDDO
           DO m = 1, mleg
              IF (Qit(m,n) /= 0.0_dp) THEN
                 auxil(m) = ABS((Qit(m,n)-Qit_o(m,n))/Qit(m,n))
              ELSE
                 auxil(m) = 0.0_dp
              ENDIF
           ENDDO
           taux2 = taux2 + SUM(auxil)
        ENDDO

        IF (taux2 < 1.0e-3_dp) THEN
           EXIT      !EXITs sub-iteration loop
        ENDIF

     ENDDO        ! Exits loop to find q  :: 101

! STEP 17 - Compute radiation field - mean intensity
!           intJm is 0.5 SUM(-1->0) of I
!           intJp is 0.5 SUM(0->1) of I
!           Thus intJm + intJp = J in erg cm-2 s-1 A-1 sterad-1
!           one gets u in erg cm-3 A-1 by u = J * 4 pi / c

     DO n = 0, npo
        taux1 = 0.0_dp
        taux2 = 0.0_dp
        taux3 = 0.0_dp
        DO m = 1, mleg
           taux1 = taux1 + Yit(-m,n) * (1.0_dp + Srm(m,n))
           taux2 = taux2 + Yit(-m,n) * (1.0_dp - Srm(m,n))
           taux3 = taux3 + Yit(-m,n)
        ENDDO

        ! 19 XII 2005 - Change normalisation
        intJm(n,iwlg) = 0.5_dp * taux1
        intJp(n,iwlg) = 0.5_dp * taux2
        intJmp(n,iwlg) = taux3
     ENDDO

     ! Compute escape probabilities
     ! Half system only
     DO n = 0, npo        !position
        CALL AUX_BP(rlm, pli, n, aam)
        bbm(1:mleg,1) = 1.0_dp

        CALL DGESVX (FACT, TRANS, mleg, nrhs, aam, nl, aamf, nl, indx, EQUED, &
                     R, C, bbm, nl, bbmf, nl, RCOND, FERR, BERR, WORK, IWORK, info)

        taux1 = 0.0_dp
        DO m = mleg, 1, -1
           taux1 = taux1 + bbmf(m,1) * Emk_p_val(n,m)
        ENDDO
        bbspp(n,iwlg) = taux1
     ENDDO

     DO n = 0, npo
        CALL AUX_BM(rlm, pli, n, aam)
        bbm(1:mleg,1) = 1.0_dp

        CALL DGESVX (FACT, TRANS, mleg, nrhs, aam, nl, aamf, nl, indx, EQUED, &
                     R, C, bbm, nl, bbmf, nl, RCOND, FERR, BERR, WORK, IWORK, info)

        taux1 = 0.0_dp
        DO m = mleg, 1, -1
           taux1 = taux1 + bbmf(m,1) * Emk_m_val(n,m)
        ENDDO
        bbspm(n,iwlg) = taux1
     ENDDO

     enor_m = bbspm(0,iwlg)
     enor_p = bbspp(npo,iwlg)
     IF (enor_p == 0.0_dp) THEN
        enor_p = 1.0_dp
     ENDIF

     DO n = 0, npo
        bbspm(n,iwlg) = bbspm(n,iwlg) / enor_m
        bbspp(n,iwlg) = bbspp(n,iwlg) / enor_p
     ENDDO

     IF (MOD(iwlg,100) == 0) THEN
        WRITE(iscrn,'("iwlg = ",I6," wlth[A] = ",F16.3,"  intJmp: edge = ",1p,E12.4," middle =",1p,E12.4)') &
              iwlg, wlth, intJmp(0,iwlg), intJmp(INT(npo/2),iwlg)
     ENDIF

     ! Compute emerging specific intensities
     DO l = 0, lleg
        fl_0(l) = 0.0_dp
        DO m = 1, mleg
           fl_0(l) = fl_0(l) + rlm(l,m,0) * Yit(m,0) + rlm(l,-m,0) * Yit(-m,0)
        ENDDO
     ENDDO

     DO i = 1, nmui
        I_esc_0(iwlg,-i) = 0.0_dp
        I_esc_0(iwlg,i)  = 0.0_dp
        DO l = 0, lleg
           I_esc_0(iwlg,-i) = I_esc_0(iwlg,-i) + (2.0_dp*l+1.0_dp) * fl_0(l) * pli(l,-i)
           I_esc_0(iwlg,i)  = I_esc_0(iwlg,i)  + (2.0_dp*l+1.0_dp) * fl_0(l) * pli(l,i)
        ENDDO
     ENDDO
     WRITE (36,'(1p,32e15.6)') wlth, (I_esc_0(iwlg,i), i=-nmui,nmui)

     DEALLOCATE (md_m)
     DEALLOCATE (ndp_m)
     DEALLOCATE (ndq_m)
     DEALLOCATE (R_m)
     DEALLOCATE (nn_m)
  ENDDO    !end wavelenght loop :: 99  !too slow !!

  CLOSE (36)
  CLOSE (37)
  CLOSE (60)

  IF (F_W_RF_ALB == 1) THEN
     ! Write here full radiation field
     fichier = TRIM(out_dir)//TRIM(modele)//'.rf'
     OPEN(60, file=fichier, status="unknown" , form = 'unformatted')
     WRITE (60) rf_wl%upper-rf_wl%lower+1 ! number of pixels in wavelength grid
     WRITE (60) rf_wl%Val                 ! Wavelength grid in Angstroms
     DO n = 0, npo
        WRITE (60) tau_o(n)*tautav, intJmp(n,:) * J_to_u
     ENDDO
     CLOSE (60)
  ENDIF

  ! Free memory
  DEALLOCATE (xmunl)
  DEALLOCATE (pli)
  DEALLOCATE (p_0, q_0)
  DEALLOCATE (hl)
  DEALLOCATE (wr, wi)
  DEALLOCATE (xkm)
  DEALLOCATE (Emk_m_mag)
  DEALLOCATE (Emk_m_cmp)
  DEALLOCATE (Emk_m_val)
  DEALLOCATE (Emk_p_val)
  DEALLOCATE (Smk_m)
  DEALLOCATE (QmG)
  DEALLOCATE (rlm, rlmp, rlmi, rlmip)
  DEALLOCATE (sfl, sfm, sfl_g, sfl_d)
  DEALLOCATE (Srm)
  DEALLOCATE (sigl)
  DEALLOCATE (tl, dtl)
  DEALLOCATE (albed)
  DEALLOCATE (kap_gas, kap_dus, sca_dus, emm_dus)
  DEALLOCATE (g_absL)
  DEALLOCATE (aam, bbm)
  DEALLOCATE (aamf, bbmf)
  DEALLOCATE (Tim, Bim)
  DEALLOCATE (Iinc)
  DEALLOCATE (ggm)
  DEALLOCATE (Dit)
  DEALLOCATE (Qit, Qit_o, Git, Yit, Yit_o)
  DEALLOCATE (Hit)
  DEALLOCATE (R, C)
  DEALLOCATE (FERR, BERR)
  DEALLOCATE (WORK, IWORK)
  DEALLOCATE (Camp)
  DEALLOCATE (indx)
  DEALLOCATE (argu)
  DEALLOCATE (indmi, indmj)
  DEALLOCATE (fl_0)

!=-=-=-=-=-=-=-=-=-=-=-= NOT CHECKED FROM HERE jrg =-=-=-=-=-=-=-=-=-=-=-=-=

! 20 V 2003 - Compute here the effective radiation field scaling
! B - cut-off at 2400 A:

  CALL RF_CREATE(rf_lamI,0,nwlu)

  CALL RF_CREATE(rf_tmp,1,2)
  CALL RF_LOCATE(rf_wl, wlth0, i1)
  rf_tmp%Val(1) = 1.0_dp
  rf_tmp%Ord(1) = rf_wl%Ord(i1)
  CALL RF_LOCATE(rf_wl, wl_hab, i2)
  rf_tmp%Val(2) = 1.0_dp
  rf_tmp%Ord(2) = rf_wl%Ord(i2)

  int_rad1 = 0.0_dp
  int_rad2 = 0.0_dp

  ! Compute mean intensity from left side (use rf_u as auxiliary variable here)
  DO iwlg = 0, nwlu
     rf_u%Val(iwlg) = intJm(0,iwlg)
  ENDDO
  mean_rad1 = SECT_INT(rf_tmp, rf_u, wl_pointer, wl_hab)

  DO iwlg = 0, nwlu
     rf_u%Val(iwlg) = intJmp(0,iwlg) * J_to_u
  ENDDO
  rf_lamI%Val(0:nwlu) = rf_wl%Val(0:nwlu) * rf_u%Val(0:nwlu)

  int_rad1 = SECT_INT(rf_tmp, rf_u, wl_pointer, wl_hab)
  flu_rad1 = SECT_INT(rf_tmp, rf_lamI, wl_pointer, wl_hab) / (1.0e8_dp * xhp)

  WRITE (iscrn,'("  U1:",1pe12.4," erg cm-3,     ","from 912 to",0pf9.2," Angstrom")') int_rad1, wl_hab
  WRITE (iscrn,'("  F1:",1pe12.4," phot cm-2 s-1,","from 912 to",0pf9.2," Angstrom")') flu_rad1, wl_hab
  WRITE (iwrit2,'("  U1:",1pe12.4," erg cm-3,     ","from 912 to",0pf9.2," Angstrom")') int_rad1, wl_hab
  WRITE (iwrit2,'("  F1:",1pe12.4," phot cm-2 s-1,","from 912 to",0pf9.2," Angstrom")') flu_rad1, wl_hab

  radm = int_rad1 / int_rad0

  G0m = int_rad1 / u_habing
  G0p = 0.0_dp
  PRINT *, " "
  WRITE (iscrn,'("  G_0m :",1pe12.4," (in Habing units)")') G0m
  WRITE(iwrit2,'(/," U0:",1pe13.4, " erg cm-3")') int_rad0
  WRITE(iwrit2,'(" G_0m =", 1pe12.3, " in Habing units",/)') G0m

  ! set here wavelength threshold for integrated intensity.
  CALL RF_LOCATE(rf_wl, wl_hab, i2)
  rf_tmp%Val(2) = 1.0_dp
  rf_tmp%Ord(2) = rf_wl%Ord(i2)
  int_rad(0) = SECT_INT(rf_tmp, rf_u, wl_pointer, wl_hab)

  PRINT *, " "
  PRINT *, " Effective Radiation field strength AT the edge:"
  WRITE (iscrn,'("  left :",1pe12.4," ",a15)') radm, ISRF_name
  WRITE (iwrit2,'("  left :",1pe12.4," ",a15)') radm, ISRF_name
  PRINT *, " "

  CALL RF_FREE (rf_tmp)
  CALL RF_FREE (rf_lamI)

END SUBROUTINE ETIN901S

!**********************************************************************************************************
SUBROUTINE RF_INIT
!**********************************************************************************************************
! purpose : Initialise model dependant quantities related to Radiation Field (RF)
!   * Wavelength grid                    : rf_wl (need not have constant step)
!   * Incident ISRF (specific intensity) : rf_incIm and rf_incIp
!   * Dust absorption curve              : rf_extpou (Fitzpatrick fit)

!---------------------------Declaration of PARAMETERs-----------------------------------------------------
    USE PXDR_CONSTANTES
    USE PXDR_CHEM_DATA
    USE PXDR_PROFIL
    USE PXDR_AUXILIAR
    USE PXDR_STR_DATA
    USE PXDR_DRAINE_DUST
    USE PXDR_RF_TOOLS
    USE PXDR_READCHIM
    USE PXDR_DUSTEM

    IMPLICIT NONE

    REAL (KIND=dp)                             :: wl_cut_D = 2000.0_dp !  Draine field cutoff wavelength
    REAL (KIND=dp)                             :: wl_cut_F             !  Fitzpatrick cutoff wavelength (Ang)
    INTEGER                                    :: I_cut_F              !  Index of Fitzpatrick cutoff position in grid
    REAL (KIND=dp)                             :: wl_cut_F2            !  WL cutoff below which grains cross section is constant
    INTEGER                                    :: I_cut_F2             !  Index of the wavelength defined above
    REAL (KIND=dp)                             :: wl_cut               !  Dummy WL cutoff 
    INTEGER                                    :: I_cut                !  Index of dummy cutoff position in grid

    INTEGER                                    :: mm
    INTEGER                                    :: num                  !  Dummy number
    INTEGER                                    :: ii, i, j, k
    INTEGER                                    :: nb_pt                !  Number of points in a file
    INTEGER                                    :: np_before            !  Number of points before Lyman lim.
    INTEGER                                    :: add_1, iloc, iord    !  Used by RF_EXTAND
    REAL (KIND=dp)                             :: Energy, sigm
    REAL (KIND=dp)                             :: wl, rf
    TYPE (RF_TYPE)                             :: rf_tmp

    REAL(KIND=dp)                              :: wl0, wl1, dopl, dwl
    REAL(KIND=dp), PARAMETER                   :: wlrinc = 1.2_dp
    INTEGER                                    :: njl, nvl

    ! Auxiliary variables for DUSTEM field

    REAL(KIND=dp)                              :: r1, r2, r3, r4
    REAL(KIND=dp)                              :: wlth, fact
    REAL(KIND=dp)                              :: absor, scater, extinct
    REAL(KIND=dp)                              :: Dr_int1, Dr_int2
    INTEGER                                    :: il1, il2

    !==================================================================================
    ! INITIALIZATION of the radiation field grid
    !==================================================================================
    ! Compute H ionisation potential --------------------------------------------------
    hiop  = 2.0_dp * xpi*xpi * xqe**4 * xme / (xhp*xhp)
    wlth0 = 1.0e8_dp * xhp * clum / hiop
    PRINT *, " true wlth0 =", wlth0
    wlth0 = praih(nraih,2)
    !wlthm = wlth0 * pwlg**nwlg
    wlthm = 2.2e10_dp             ! Beyond OH logest wavelength
    ! Set initial grid step
    pwlg = EXP(LOG(wlthm / wlth0) / DBLE(nwlg))
    PRINT *, "  wlth0 =", wlth0, " wlthm =", wlthm, " pwlg =", pwlg

    ! Initialisation of wavelengths ---------------------------------------------------
    ! rf_wl%Ord is a permutation of (rf_wl%lower : rf_wl%upper) giving the
    !           order in which rf_wl%Val have been entered

    IF (.NOT. ASSOCIATED(rf_wl%Val)) THEN
       CALL RF_CREATE (rf_wl, 0, nwlg)
       rf_wl%Val = wlth0 * ( pwlg**(/ (ii, ii=0,nwlg) /) )
       rf_wl%Ord = (/ (ii, ii=0,nwlg) /)
    ENDIF

    !================================================================================
    ! Add all "special values"
    !================================================================================

    !---  Habing field limit --------------------------------------------------------
    CALL RF_EXTAND (rf_wl, wl_hab, add_1, iloc, iord)
    nwlg = nwlg + add_1

    !--- 6 eV field limit -----------------------------------------------------------
    CALL RF_EXTAND (rf_wl, wl_6ev, add_1, iloc, iord)
    nwlg = nwlg + add_1

    !--- Draine Field cut-off ------------------------------------------------------
    CALL RF_EXTAND (rf_wl, wl_cut_D, add_1, iloc, iord)
    nwlg = nwlg + add_1

    !--- Fitzpatrick & Massa extinction curve ---------------------------------------
    wl_cut_F = 1.0e4_dp / 5.9_dp
    CALL RF_EXTAND (rf_wl, wl_cut_F, add_1, iloc, iord)
    nwlg = nwlg + add_1

    ! Radiation field read in a file
    IF (srcpp(1:2) == 'F_') THEN
       fichier = TRIM(data_dir)//'Astrodata/'//srcpp
       WRITE(iscrn,*) "Opening file : ", TRIM(fichier)
       OPEN(iread2, file = fichier, status = 'old')
       READ (iread2,*) rstar
       READ (iread2,*) teff
       rstar = rstar * rsun
       READ (iread2,*) nb_pt
       READ (iread2,*)
       PRINT *, nb_pt

       ! Warning: for historical reasons data in file are per nanometers
       DO ii = 1, nb_pt
          READ (iread2,*) wl, rf
          wl = wl * 10.0_dp      ! convert to Angstrom
          IF (wl < wlth0) CYCLE
          CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)   ! add wl in grid
          nwlg = nwlg + add_1
          IF (add_1 /= 0) THEN
             print *, " add:", nwlg, wl
          ENDIF
       ENDDO
       CLOSE (iread2)
    ENDIF

    ! Special : If the name of the line of sight is sig1.9, we force grains cross section
    !           to be 1.9E-21 cm-2 below 1070 Ang (9.346 microm-1) and then use the Galactic curve
    IF (los_ext == 'sig1.9') THEN
       wl_cut_F2 = 1.0e4_dp / 9.346_dp
       CALL RF_EXTAND (rf_wl, wl_cut_F2, add_1, iloc, iord)
       nwlg = nwlg + add_1
    ENDIF

    !--- Upper limit of Fitzpatrick and Massa fit
    CALL RF_EXTAND (rf_wl, FP_up_wl, add_1, iloc, iord)
    nwlg = nwlg + add_1

    !--- B and V Photometric band value
    CALL RF_EXTAND (rf_wl, B_band_wl, add_1, iloc, iord)
    nwlg = nwlg + add_1
    CALL RF_EXTAND (rf_wl, V_band_wl, add_1, iloc, iord)
    nwlg = nwlg + add_1

    !--- Wavelength of transitions for detailed balance species
    DO k = 1, nspl
       DO ii = 1, spec_lin(k)%ntr
          wlth = spec_lin(k)%lac(ii) * 1.0e8_dp
          CALL RF_EXTAND (rf_wl, wlth, add_1, iloc, iord)
          nwlg = nwlg + add_1
       ENDDO
    ENDDO

    !--- Photodestruction sections in data/section --------------------------------

    ! Loop on all photodestruction reactions computed with cross sections
    DO k = 1, nphot_i - i_mandat

       fichier = TRIM(data_dir)//'Sections/'//TRIM(csect_files(k))//".dat"
       WRITE(iscrn,*) "Opening file : ",TRIM(fichier)
       OPEN(iread2, file = fichier, status = 'old')

       ! First count number of point above threshold
       READ(iread2,"(2/)")
       READ(iread2,*) nb_pt  ! number of points in the file
       READ(iread2,*)

       ! Remove points below Lyman limit
       np_before = 0
       DO ii = 1, nb_pt
          READ (iread2,*) num, wl ! Energy or wavelength in Angstrom
          IF (wl < wlth0) THEN
             np_before = np_before + 1
          ENDIF
       ENDDO
       nb_pt = nb_pt - np_before

       ! Then READ correct number of cross section and interpolate last to wlth0
       IF (ASSOCIATED(phdest(k)%rf_esp%Val)) THEN
         CALL RF_FREE(phdest(k)%rf_esp)
       ENDIF
       CALL RF_CREATE (phdest(k)%rf_esp,1,nb_pt+1)

       REWIND(iread2)
       READ(iread2,"(4/)")

       IF (np_before > 0) THEN ! jump points to remove
          DO ii = 1, np_before
             READ(iread2,*) ! Excluded photo destruction data
          ENDDO
       ENDIF

       DO ii = 1, nb_pt ! store data
          READ (iread2,*) num, Energy, sigm
          wl = Energy
          IF (ii == nb_pt) THEN
             phdest(k)%wl_cut_esp = wl
          ENDIF

          CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
          nwlg = nwlg + add_1
          phdest(k)%rf_esp%Val(ii+1) = sigm   ! sigm's unit is cm^-2
          phdest(k)%rf_esp%Ord(ii+1) = iord
       ENDDO
       CLOSE(iread2)

       ! Eventual interpolation at wlth0
       ! Default : If no value is given for section at wlth0, the section
       ! at the next point is used
       IF (phdest(k)%r1 == 's') THEN
          phdest(k)%rf_esp%Val(1) = phdest(k)%rf_esp%Val(2) !Possible interpolation here
       ELSE
          phdest(k)%rf_esp%Val(1) = phdest(k)%rf_esp%Val(2)
       ENDIF
       CALL RF_LOCATE (rf_wl, wlth0, ii)
       phdest(k)%rf_esp%Ord(1) = rf_wl%Ord(ii)

       CALL RF_REVERS (wl_pointer)
       IF (rf_wl%Val(nwlg) > wlthm) THEN
          wlthm = rf_wl%Val(nwlg)
       ENDIF
    ENDDO

    !--- External radiation field -----------------------------------------
    !
    ! In addition to an isotropic contribution (scales by radm and/or radp)
    ! we add a normal source. Three cases are possible:
    !    1 - srcpp = "F_XXXXX" : Name of a file containing the RF (e.g. Kurucz)
    !    2 - srcpp = "PP"      : Stands for "Plane Parallel" => No isotropic
    !                            contribution, but same energy density at the edge
    !    3 - srcpp = Star type : Analytic Black Body with prescribed spectral type
    !
    ! This normal RF subtends a solid angle such that "geom" = 1 - cos(theta)
    !
    ! Flag "F_Norm_RF" indicates where the radiations comes from:
    !    F_Norm_RF = 1 : from left side (or "m" - minus)
    !    F_Norm_RF = 2 : from right side (or "p" - plus)
    !    F_Norm_RF = 3 : from both

    ! Radiation field read in a file
    IF (srcpp(1:2) == 'F_') THEN
       PRINT *, srcpp
       fichier = TRIM(data_dir)//'Astrodata/'//srcpp
       WRITE(iscrn,*) "Opening file : ", TRIM(fichier)
       OPEN(iread2, file = fichier, status = 'old')
       READ (iread2,*) rstar
       READ (iread2,*) teff
       PRINT *, rstar, teff
       rstar = rstar * rsun
       READ (iread2,*) nb_pt
       READ (iread2,*)
       PRINT *, nb_pt

       IF (ALLOCATED(star_rf)) THEN
          DEALLOCATE(star_rf)
       ENDIF
       ALLOCATE (star_rf(nb_pt,2))

       ! Warning: for historical reasons data in file are per nanometers
       DO ii = 1, nb_pt
          READ (iread2,*) wl, rf
          wl = wl * 10.0_dp      ! convert to Angstrom
          rf = rf * 0.1_dp       ! convert to Angstr-1
          star_rf(ii,1) = wl
          star_rf(ii,2) = rf
       ENDDO
       CLOSE (iread2)

       IF (d_sour < 0.0_dp) THEN
          F_Norm_RF = 1
       ELSE IF (d_sour > 0.0_dp) THEN
          F_Norm_RF = 2
       ENDIF

    ELSE IF (srcpp(1:2) == 'PP') THEN
       F_Norm_RF = 3
       PRINT *, " radm_ini =", radm_ini
       PRINT *, " radp_ini =", radp_ini
       PRINT *, " F_Norm_RF =", F_Norm_RF

    ! If srcpp has no special value and d_sour = 0, there is no perpendicular source
    ELSE IF (ABS(d_sour) > 1.0e-10_dp) THEN

       CALL STAR

       IF (d_sour < 0.0_dp) THEN
          F_Norm_RF = 1
       ELSE IF (d_sour > 0.0_dp) THEN
          F_Norm_RF = 2
       ENDIF

    ENDIF

    WRITE (iscrn,*) " F_Norm_RF = ", F_Norm_RF

    ! Dilution factor of the star emissivity as seen from the cloud
    ! geom is (1 - cos(theta_0)) so cos(theta_0) is (1.0-geom)
    ! A second order development is adequate in most cases.
    !
    ! geom = 1 in special case "PP": see analytical derivation in notes
    IF (srcpp(1:2) == 'PP') THEN
       geom = 1.0_dp
    ELSE IF (ABS(d_sour) == 0.0_dp) THEN
       geom = 0.0_dp
    ELSE
       geom = 0.5_dp * (rstar/(ABS(d_sour)*pccm))**2
    ENDIF

    !--- Dust properties from DUSTEM --------------------------------

    ! First, read wavelength grid (always useful)
    fichier = "../DUSTEM_PDR/les_QABS/LAMBDA.DAT"
    OPEN(iread2, file = fichier, status = 'old')
    READ(iread2,'(///,i20)') nwl_ir
    DO ii = 1, nwl_ir
       READ (iread2,*) wl
       wl = wl * 1.0e4_dp
       IF (wl < wlth0) THEN
          CYCLE
       ENDIF
       CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
       nwlg = nwlg + add_1
    ENDDO
    CLOSE(iread2)

    ! Then fill the rest if we are indeed using DUSTEM
    IF (F_dustem == 1) THEN

       ! Initial call to DUSTEM to create an input file
       PRINT *, " Run initial DUSTEM:"
       CALL SYSTEM(lancer_dustem)

       PRINT *, " READ DUSTEM absorption, diffusion and emissivities"
       fichier = TRIM(data_dir)//"EXTINCTION_DUSTEM.RES"
       WRITE(iscrn,*) "Opening file : ", TRIM(fichier)
       OPEN(iread2, file = fichier, status = 'old')
       READ (iread2,*) nwl_ir
       PRINT *, " nwl_ir:", nwl_ir
       IF (ALLOCATED(wl_ir)) THEN
          DEALLOCATE (wl_ir)
          DEALLOCATE (wl_ir_l)
       ENDIF
       ALLOCATE (wl_ir(1:nwl_ir+1))
       ALLOCATE (wl_ir_l(1:nwl_ir+1))

       IF (ALLOCATED(dustem_in%abso)) THEN
          DEALLOCATE (dustem_in%abso)
          DEALLOCATE (dustem_in%scat)
          DEALLOCATE (dustem_in%emis)
          DEALLOCATE (dustem_in%albe)
          DEALLOCATE (dustem_in%gcos)
       ENDIF
       ALLOCATE (dustem_in%abso(nwl_ir+1,0:1))
       ALLOCATE (dustem_in%scat(nwl_ir+1,0:1))
       ALLOCATE (dustem_in%emis(nwl_ir+1,0:1))
       ALLOCATE (dustem_in%albe(nwl_ir+1,0:1))
       ALLOCATE (dustem_in%gcos(nwl_ir+1,0:1))

       DO ii = 1, nwl_ir
          READ (iread2,*) wl, r1, r2, rf, r3, r4
          wl = wl * 1.0e4_dp            ! convert to Angstrom
          wl_ir(ii) = wl
          wl_ir_l(ii) = LOG(wl)
          IF (wl > wlth0) THEN
             CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
             nwlg = nwlg + add_1
          ENDIF
          dustem_in%abso(ii,0:1) = r1
          dustem_in%scat(ii,0:1) = r2
          dustem_in%emis(ii,0:1) = rf / (4.0_dp * xpi * wl)
          dustem_in%albe(ii,0:1) = r3
          dustem_in%gcos(ii,0:1) = r4
       ENDDO
       CLOSE (iread2)
       PRINT *, "  End reading DUSTEM file"
       wl_ir(nwl_ir+1) = wlthm
       wl_ir_l(nwl_ir+1) = LOG(wlthm)
       CALL EXTEND_DUST(1)
    ENDIF

    !--- Grains absorption cross sections ----------------
    !    (interpolated from Draines data for each radius)
    IF (ALLOCATED(sigabs)) THEN
       DO mm = 1, npg
          CALL RF_FREE(sigabs(mm))
       ENDDO
       DEALLOCATE(sigabs)
    ENDIF
    ALLOCATE(sigabs(npg))
    DO mm = 1, npg
       NULLIFY (sigabs(mm)%val,sigabs(mm)%ord)                     !SG
    ENDDO
    DO mm = 1, npg
       IF (ASSOCIATED(sigabs(mm)%Val)) THEN
          CALL RF_FREE(sigabs(mm))
       ENDIF
       CALL RF_CREATE(sigabs(mm),1,jwlmax_gr-jwlmin_gr+2)
    ENDDO

    DO ii = 1, jwlmax_gr-jwlmin_gr+2
       IF (ii == 1) THEN
          wl = wlth0
       ELSE IF (ii == jwlmax_gr-jwlmin_gr+2) THEN
          wl = wlthm
       ELSE
          wl = wavgrd(ii+jwlmin_gr-1)
       ENDIF
       CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
       nwlg = nwlg + add_1
       DO mm = 1, npg
          sigabs(mm)%Val(ii) = Qabs_Dr(ii,mm) * xpi * adust(mm)**2 / x_nc(mm)
          sigabs(mm)%Ord(ii) = iord
       ENDDO
    ENDDO

    CALL RF_REVERS (wl_pointer)

    !---------------------------------------------------------------------------
    ! ADD LINE WAVELENGTH IN THE GRID
    !---------------------------------------------------------------------------

    !--- Atomic hydrogen (Lyman lines) -----------------------------------------
    !     PARAMETER "dopl" is the step in lambda mesured in Doppler width
    IF (jlyman == 1) THEN
       DO ii = 1, nraih
          wl0 = praih(ii,2)
          dopl = 0.3_dp * wl0 * vturb / clum
          CALL RF_EXTAND (rf_wl, wl0, add_1, iloc, iord)
          nwlg = nwlg + add_1

          dwl = 0.2_dp * dopl
          wl = wl0
          DO
             IF (ABS(wl-wl0)/dopl < wlrwid) THEN
                wl1 = rf_wl%Val(iloc+1)
                IF ((wl1-wl) > 2.0_dp*dwl) THEN
                   wl = wl + dwl
                   CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                   nwlg = nwlg + add_1
                ELSE IF ((wl1-wl) > dwl) THEN
                   wl = wl + (wl1-wl) * 0.5_dp
                   CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                   nwlg = nwlg + add_1
                   wl = wl1
                   iloc = iloc + 1
                ELSE
                   wl = wl1
                   iloc = iloc + 1
                ENDIF
                dwl = 0.2_dp * dopl + (wlrinc - 1.0_dp) * (wl-wl0)
             ELSE
                EXIT
             ENDIF
          ENDDO

          CALL RF_LOCATE (rf_wl, wl0, iloc)

          dwl = 0.2_dp * dopl
          wl = wl0
          DO
             IF (ABS(wl-wl0)/dopl < wlrwid .AND. &
                wl > wlth0) THEN
                wl1 = rf_wl%Val(iloc-1)
                IF ((wl-wl1) > 2.0_dp*dwl) THEN
                   wl = wl - dwl
                   CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                   nwlg = nwlg + add_1
                ELSE IF ((wl-wl1) > dwl) THEN
                   wl = wl - (wl-wl1) * 0.5_dp
                   CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                   nwlg = nwlg + add_1
                   wl = wl1
                   iloc = iloc - 1
                ELSE
                   wl = wl1
                   iloc = iloc - 1
                ENDIF
                dwl = 0.2_dp * dopl + (wlrinc - 1.0_dp) * (wl0-wl)
             ELSE
                EXIT
             ENDIF
          ENDDO

       ENDDO
    ENDIF

    !---  CO lines , only lowest J --------------------------------

    DO ii = 1, nrco
       njl = irco(ii)
       IF (njl >= jfgkco) THEN
          CYCLE
       ENDIF
       wl0 = prco(ii,2)
       dopl = 0.3_dp * wl0 * vturb / clum
       CALL RF_EXTAND (rf_wl, wl0, add_1, iloc, iord)
       nwlg = nwlg + add_1

       dwl = 0.2_dp * dopl
       wl = wl0
       DO
          IF (ABS(wl-wl0)/dopl < wlrwid) THEN
             wl1 = rf_wl%Val(iloc+1)
             IF ((wl1-wl) > 2.0_dp*dwl) THEN
                wl = wl + dwl
                CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                nwlg = nwlg + add_1
             ELSE IF ((wl1-wl) > dwl) THEN
                wl = wl + (wl1-wl) * 0.5_dp
                CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                nwlg = nwlg + add_1
                wl = wl1
                iloc = iloc + 1
             ELSE
                wl = wl1
                iloc = iloc + 1
             ENDIF
             dwl = 0.2_dp * dopl + (wlrinc - 1.0_dp) * (wl-wl0)
          ELSE
             EXIT
          ENDIF
       ENDDO

       CALL RF_LOCATE (rf_wl, wl0, iloc)

       dwl = 0.2_dp * dopl
       wl = wl0
       DO
          IF (ABS(wl-wl0)/dopl < wlrwid .AND. &
             wl > wlth0) THEN
             wl1 = rf_wl%Val(iloc-1)
             IF ((wl-wl1) > 2.0_dp*dwl) THEN
                wl = wl - dwl
                CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                nwlg = nwlg + add_1
             ELSE IF ((wl-wl1) > dwl) THEN
                wl = wl - (wl-wl1) * 0.5_dp
                CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                nwlg = nwlg + add_1
                wl = wl1
                iloc = iloc - 1
             ELSE
                wl = wl1
                iloc = iloc - 1
             ENDIF
             dwl = 0.2_dp * dopl + (wlrinc - 1.0_dp) * (wl0-wl)
          ELSE
             EXIT
          ENDIF
       ENDDO

    ENDDO

    !--- H2 lines , only lowest J -------------------------------------

    DO ii = 1, nrh2bt
       nvl = irah2b(ii,1)
       njl = irah2b(ii,2)
       IF (nvl > 0 .OR. njl >= lfgkh2) THEN
          CYCLE
       ENDIF
       wl0 = prah2b(ii,2)
       dopl = 0.3_dp * wl0 * vturb / clum
       CALL RF_EXTAND (rf_wl, wl0, add_1, iloc, iord)
       nwlg = nwlg + add_1

       dwl = 0.2_dp * dopl
       wl = wl0
       DO
          IF (ABS(wl-wl0)/dopl < wlrwid) THEN
             wl1 = rf_wl%Val(iloc+1)
             IF ((wl1-wl) > 2.0_dp*dwl) THEN
                wl = wl + dwl
                CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                nwlg = nwlg + add_1
             ELSE IF ((wl1-wl) > dwl) THEN
                wl = wl + (wl1-wl) * 0.5_dp
                CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                nwlg = nwlg + add_1
                wl = wl1
                iloc = iloc + 1
             ELSE
                wl = wl1
                iloc = iloc + 1
             ENDIF
             dwl = 0.2_dp * dopl + (wlrinc - 1.0_dp) * (wl-wl0)
          ELSE
             EXIT
          ENDIF
       ENDDO

       CALL RF_LOCATE (rf_wl, wl0, iloc)

       dwl = 0.2_dp * dopl
       wl = wl0
       DO
          IF (ABS(wl-wl0)/dopl < wlrwid .AND. &
             wl > wlth0) THEN
             wl1 = rf_wl%Val(iloc-1)
             IF ((wl-wl1) > 2.0_dp*dwl) THEN
                wl = wl - dwl
                CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                nwlg = nwlg + add_1
             ELSE IF ((wl-wl1) > dwl) THEN
                wl = wl - (wl-wl1) * 0.5_dp
                CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                nwlg = nwlg + add_1
                wl = wl1
                iloc = iloc - 1
             ELSE
                wl = wl1
                iloc = iloc - 1
             ENDIF
             dwl = 0.2_dp * dopl + (wlrinc - 1.0_dp) * (wl0-wl)
          ELSE
             EXIT
          ENDIF
       ENDDO
    ENDDO

    DO ii = 1, nrh2ct
       nvl = irah2c(ii,1)
       njl = irah2c(ii,2)
       IF (nvl > 0 .OR. njl >= lfgkh2) THEN
          CYCLE
       ENDIF
       wl0 = prah2c(ii,2)
       dopl = 0.3_dp * wl0 * vturb / clum
       CALL RF_EXTAND (rf_wl, wl0, add_1, iloc, iord)
       nwlg = nwlg + add_1

       dwl = 0.2_dp * dopl
       wl = wl0
       DO
          IF (ABS(wl-wl0)/dopl < wlrwid) THEN
             wl1 = rf_wl%Val(iloc+1)
             IF ((wl1-wl) > 2.0_dp*dwl) THEN
                wl = wl + dwl
                CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                nwlg = nwlg + add_1
             ELSE IF ((wl1-wl) > dwl) THEN
                wl = wl + (wl1-wl) * 0.5_dp
                CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                nwlg = nwlg + add_1
                wl = wl1
                iloc = iloc + 1
             ELSE
                wl = wl1
                iloc = iloc + 1
             ENDIF
             dwl = 0.2_dp * dopl + (wlrinc - 1.0_dp) * (wl-wl0)
          ELSE
             EXIT
          ENDIF
       ENDDO

       CALL RF_LOCATE (rf_wl, wl0, iloc)

       dwl = 0.2_dp * dopl
       wl = wl0
       DO
          IF (ABS(wl-wl0)/dopl < wlrwid .AND. &
             wl > wlth0) THEN
             wl1 = rf_wl%Val(iloc-1)
             IF ((wl-wl1) > 2.0_dp*dwl) THEN
                wl = wl - dwl
                CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                nwlg = nwlg + add_1
             ELSE IF ((wl-wl1) > dwl) THEN
                wl = wl - (wl-wl1) * 0.5_dp
                CALL RF_EXTAND (rf_wl, wl, add_1, iloc, iord)
                nwlg = nwlg + add_1
                wl = wl1
                iloc = iloc - 1
             ELSE
                wl = wl1
                iloc = iloc - 1
             ENDIF
             dwl = 0.2_dp * dopl + (wlrinc - 1.0_dp) * (wl0-wl)
          ELSE
             EXIT
          ENDIF
       ENDDO
    ENDDO

    PRINT *, "   nwlg =", nwlg

    IF (rf_wl%upper /= nwlg) THEN
       PRINT *, "Probleme"
    ENDIF

    CALL RF_REVERS (wl_pointer)

    !-------------------------------------------------------------------------------------
    !****    From here, the set of wavelength is complete and should not be modified ****
    !-------------------------------------------------------------------------------------

    !-------------------------------------------
    ! Initialisation of external radiation Field
    !-------------------------------------------

    IF (ASSOCIATED(rf_incIm%Val)) THEN
       CALL RF_FREE(rf_incIm)
    ENDIF
    CALL RF_CREATE (rf_incIm, 0, nwlg)
    IF (ASSOCIATED(rf_incIp%Val)) THEN
       CALL RF_FREE(rf_incIp)
    ENDIF
    CALL RF_CREATE (rf_incIp, 0, nwlg)

    IF (ASSOCIATED(rf_u%Val)) THEN
       CALL RF_FREE(rf_u)
    ENDIF
    CALL RF_CREATE (rf_u, 0, nwlg)

    !----------------------------------------------------------------------------------
    ! Fitzpatrick and Massa extinction curves
    ! We use the Fitzpatrick and Massa expressions for the grains extinction
    ! Their expression gives E(lambda-V) / E(B-V)
    ! rf_expou%Val = [1 - 1/Rv * E(lambda-V) / E(B-V)]
    !
    ! Modification 24th june 2010
    ! If the name of the line of sight is sig1.9, we use :
    ! - from 912 to 1070 Ang (9.35 mum-1) : sigma_dust = 1.9E-21 (=> E(lambda-V) / E(B-V) = 8.8648)
    ! - above 1070 Ang                    : Galactic extinction curve
    !----------------------------------------------------------------------------------

    IF (ASSOCIATED(rf_extpou%Val)) THEN
       CALL RF_FREE(rf_extpou)
    ENDIF
    CALL RF_CREATE (rf_extpou, 0, nwlg)
    IF (ASSOCIATED(rf_tmp%Val)) THEN
       CALL RF_FREE(rf_tmp)
    ENDIF
    CALL RF_CREATE (rf_tmp, 0, nwlg)
    rf_tmp%Val = 1.0e4_dp / rf_wl%Val

    ! Create extinction curve from analytical fits
    ! 1 - UV slope and 2200 A bump
    wl_cut = FP_up_wl
    CALL RF_LOCATE (rf_wl, wl_cut, I_cut)
    rf_extpou%Val(0:I_cut) = cc1 + cc2 * rf_tmp%Val(0:I_cut) + cc3 * (rf_tmp%Val(0:I_cut) * rf_tmp%Val(0:I_cut) &
                           / ((rf_tmp%Val(0:I_cut) * rf_tmp%Val(0:I_cut) - y0 * y0)**2 &
                           + xgam * xgam * rf_tmp%Val(0:I_cut) * rf_tmp%Val(0:I_cut)))

    ! 2 - Far UV rise
    CALL RF_LOCATE (rf_wl, wl_cut_F, I_cut_F)
    rf_extpou%Val(0:I_cut_F) = rf_extpou%Val(0:I_cut_F)                              &
                             +  cc4 * (0.5392_dp * (rf_tmp%Val(0:I_cut_F)-5.9_dp)**2 &
                             + 0.0564_dp * (rf_tmp%Val(0:I_cut_F)-5.9_dp)**3)

    ! 3 - IR analytical fit (using Galactic values from FP (2009) ApJ 699, 1209)
    !     Equation (5), used with alpha from BD+61 2365
    wl_cut = 1.0e4_dp
    CALL RF_LOCATE (rf_wl, wl_cut, I_cut)
     rf_extpou%Val(I_cut:) = (0.349_dp + 2.087_dp * rrv) / (1.0_dp + (1.0_dp/(0.507_dp*rf_tmp%Val(I_cut:))**2.28_dp)) - rrv

    ! 4 - Visible: use 3 linear interpolation from 1 micron to V, from V to B and from B to UV cutoff (Ext = 0 at V and 1 at B)
    wl_cut = 1.0e4_dp
    CALL RF_LOCATE (rf_wl, wl_cut, I_cut)
    wl_cut = V_band_wl
    CALL RF_LOCATE (rf_wl, wl_cut, I_cut_F)
    rf_extpou%Val(I_cut_F:I_cut) = rf_extpou%Val(I_cut) - rf_extpou%Val(I_cut) &
                                 * (rf_tmp%Val(I_cut_F:I_cut) - 1.0_dp) &
                                 / ( 1.0e4_dp / V_band_wl -  1.0_dp)
    I_cut = I_cut_F
    wl_cut = B_band_wl
    CALL RF_LOCATE (rf_wl, wl_cut, I_cut_F)
    rf_extpou%Val(I_cut_F:I_cut) = rf_extpou%Val(I_cut) + (1.0_dp - rf_extpou%Val(I_cut)) &
                                 * (rf_tmp%Val(I_cut_F:I_cut) - 1.0e4_dp / V_band_wl) &
                                 / ( 1.0e4_dp / B_band_wl -  1.0e4_dp / V_band_wl)
    I_cut = I_cut_F
    wl_cut = FP_up_wl
    CALL RF_LOCATE (rf_wl, wl_cut, I_cut_F)
    rf_extpou%Val(I_cut_F:I_cut) = rf_extpou%Val(I_cut) + (rf_extpou%Val(I_cut_F) - 1.0_dp) &
                                 * (rf_tmp%Val(I_cut_F:I_cut) - 1.0e4_dp / B_band_wl) &
                                 / ( 1.0e4_dp / FP_up_wl -  1.0e4_dp / B_band_wl)

    IF (los_ext == "sig1.9") THEN
       CALL RF_LOCATE (rf_wl, wl_cut_F2, I_cut_F2)
       rf_extpou%Val(:I_cut_F2) = 8.8648_dp ! value to get grain abs. cross section = 1.9E-21 cm2
    ENDIF

    rf_extpou%Val = 1.0_dp + rf_extpou%Val / rrv
    rf_extpou%Ord = (/ (ii, ii=rf_extpou%lower,rf_extpou%upper) /)

    CALL RF_FREE (rf_tmp)

    !--------------------------------------
    ! Variables used in PDR_DUSTEM - part 1
    !--------------------------------------

    IF (F_dustem == 1) THEN
       ! Initialise points where DUSTEM is called
       print *, " nav_dec =", nav_dec
       IF (MOD(npo,nav_dec) == 0) THEN
          nav_dust = 1 + npo / nav_dec
       ELSE
          nav_dust = 2 + npo / nav_dec
       ENDIF
       IF (ALLOCATED(iav_dust)) THEN
          DEALLOCATE (iav_dust)
          DEALLOCATE (av_dust)
       ENDIF
       ALLOCATE (iav_dust(nav_dust))
       ALLOCATE (av_dust(nav_dust))
       DO ii = 1, nav_dust-1
          iav_dust(ii) = nav_dec * (ii-1)
          av_dust(ii) = tau_o(nav_dec * (ii-1))
       ENDDO
       iav_dust(nav_dust) = npo
       av_dust(nav_dust) = tau_o(npo)
    ENDIF

    !----------------------------------------------------------------
    ! Set dust properties (absorption, scattering, emission, g factor
    !----------------------------------------------------------------

    PRINT *, " Enter DUST_PROPERTIES"
    CALL DUST_PROPERTIES

    !--------------------------------------------------------------------------------
    ! A first approximation of distances is needed for the following initialisations
    !--------------------------------------------------------------------------------

    IF (ALLOCATED(d_cm)) THEN
       DEALLOCATE(d_cm)
       DEALLOCATE(dd_cm)
    ENDIF
    ALLOCATE(d_cm(0:npo))
    ALLOCATE(dd_cm(0:npo))
    d_cm(:) = 0.0_dp
    dd_cm(:) = 0.0_dp
    CALL RF_LOCATE (rf_wl, V_band_wl, iloc)
    DO i = 1, npo ! d_cm(0) = 0
       dd_cm(i) = (1.0_dp / ((du_prop%abso(iloc,i) + du_prop%scat(iloc,i)) * dens_o(i)) &
                  + 1.0_dp / ((du_prop%abso(iloc,i-1) + du_prop%scat(iloc,i-1)) * dens_o(i-1))) &
                  * 0.5_dp * dtau_o(i)
       d_cm(i) = d_cm(i-1) + dd_cm(i)
    ENDDO

    !----------------------------------------
    ! Set radiation field boundary conditions
    !----------------------------------------

    CALL RF_BOUND

    !---------------------------------------------------------------------
    ! Create a first very crude approximation to the radiation field
    !---------------------------------------------------------------------

    IF (ALLOCATED(intJmp)) THEN
       DEALLOCATE (intJmp)
    ENDIF
    IF (ALLOCATED(intJm)) THEN
       DEALLOCATE (intJm, intJp)
       DEALLOCATE (bbspm, bbspp)
    ENDIF
    ALLOCATE (intJm(0:npo,0:nwlg))
    ALLOCATE (intJp(0:npo,0:nwlg))
    ALLOCATE (intJmp(0:npo,0:nwlg))
    ALLOCATE (bbspm(0:npo,0:nwlg))
    ALLOCATE (bbspp(0:npo,0:nwlg))
    ! This loop takes some code from subroutine DUST_PROPERTIES
    ! to compute a dust extinction coefficient from Draine's data.
    ! This is used to compute a wavelength dependant optical depth.
    DO j = 0, nwlg
       wlth = rf_wl%Val(j)
       DO i = jwlmin_gr, jwlmax_gr+1
          IF (wavgrd(i) > wlth) THEN
             il1 = i-1
             il2 = i-jwlmin_gr
             EXIT
          ENDIF
          IF (wavgrd(jwlmax_gr) < wlth) THEN
             il1 = jwlmax_gr
             il2 = jwlmax_gr - jwlmin_gr + 1
          ENDIF
       ENDDO
       IF (il1 < jwlmax_gr) THEN
          fact = (wlth - wavgrd(il1)) / (wavgrd(il1+1) - wavgrd(il1))

          absor = 0.0_dp
          scater = 0.0_dp
          DO mm = 1, npg
             Dr_int1 = Qabs_Dr(il2,mm) + (Qabs_Dr(il2+1,mm) - Qabs_Dr(il2,mm)) * fact
             absor = absor + wwmg(mm) * Dr_int1 * xpi * adust(mm)**2
             Dr_int2 = Qsca_Dr(il2,mm) + (Qsca_Dr(il2+1,mm) - Qsca_Dr(il2,mm)) * fact
             scater = scater + wwmg(mm) * Dr_int2 * xpi * adust(mm)**2
          ENDDO
       ELSE
          fact = (LOG(wlth) - LOG(wavgrd(il1))) / (LOG(wavgrd(il1+1)) - LOG(wavgrd(il1)))
          absor = 0.0_dp
          scater = 0.0_dp
          DO mm = 1, npg
             Dr_int1 = LOG(Qabs_Dr(il2,mm)) + (LOG(Qabs_Dr(il2+1,mm)) - LOG(Qabs_Dr(il2,mm))) * fact
             absor = absor + wwmg(mm) * EXP(Dr_int1) * xpi * adust(mm)**2
             Dr_int2 = LOG(Qsca_Dr(il2,mm)) + (LOG(Qsca_Dr(il2+1,mm)) - LOG(Qsca_Dr(il2,mm))) * fact
             scater = scater + wwmg(mm) * EXP(Dr_int2) * xpi * adust(mm)**2
          ENDDO
       ENDIF
       extinct = agrnrm * (absor + scater)

       DO ii = 0, npo
          bbspm(ii,j) = EXP(-extinct*d_cm(ii))
          intJm(ii,j) = rf_incIm%Val(j) * bbspm(ii,j)
          bbspp(ii,j) = EXP(-extinct*(d_cm(npo) - d_cm(ii)))
          intJp(ii,j) = rf_incIp%Val(j) * bbspp(ii,j)
          intJmp(ii,j) = (intJm(ii,j) + intJp(ii,j)) * 0.5_dp
       ENDDO

    ENDDO

    !--------------------------------------
    ! Variables used in PDR_DUSTEM - part 2
    !--------------------------------------

    IF (F_dustem == 1) THEN
       IF (ALLOCATED(J_dust)) THEN
          DEALLOCATE(J_dust)
       ENDIF
       ALLOCATE (J_dust(nav_dust,0:nwlg))
       DO ii = 1, nav_dust
          J_dust(ii,:) = intJmp(iav_dust(ii),:)
       ENDDO
    ENDIF

    !---------------------------------------------------------------------
    ! Create RF_TYPE objects and tables used in FBTH (stv5c.f)
    !---------------------------------------------------------------------

    IF (ALLOCATED (si_)) THEN
       DEALLOCATE(si_)
    ENDIF
    ALLOCATE (si_(rf_wl%lower:rf_wl%upper))
    IF (ALLOCATED (si_QU)) THEN
       DEALLOCATE(si_QU)
    ENDIF
    ALLOCATE (si_QU(npg,rf_wl%lower:rf_wl%upper))
    IF (ALLOCATED (si_QUl)) THEN
       DEALLOCATE(si_QUl)
    ENDIF
    ALLOCATE (si_QUl(npg,rf_wl%lower:rf_wl%upper))
    IF (ALLOCATED (si_QUl2)) THEN
       DEALLOCATE(si_QUl2)
    ENDIF
    ALLOCATE (si_QUl2(npg,rf_wl%lower:rf_wl%upper))

    PRINT *, "##########   nwlg =", nwlg

END SUBROUTINE RF_INIT

!******************
SUBROUTINE RF_BOUND
!******************

    USE PXDR_CONSTANTES
    USE PXDR_CHEM_DATA
    USE PXDR_PROFIL
    USE PXDR_AUXILIAR
    USE PXDR_STR_DATA
    USE PXDR_DRAINE_DUST
    USE PXDR_RF_TOOLS

    IMPLICIT NONE

    REAL (KIND=dp)                             :: wl_cut_D =1.0e5_dp   ! Draine field cutoff wavelength (extended)
    INTEGER                                    :: I_cut_D              ! Draine field cutoff position in grid
    REAL (KIND=dp)                             :: wl_cut_V = 8000.0_dp ! Visible cutoff wavelength
    INTEGER                                    :: I_cut_V              ! Index in wavelength Grid of visible cutoff
    INTEGER                                    :: ii, j, k
    TYPE (RF_TYPE)                             :: rf_tmp, rf_lamI, rf_Isum

    REAL(KIND=dp), DIMENSION (:), ALLOCATABLE  :: perp_RF_m, perp_RF_p
    REAL(KIND=dp)                              :: wlth, wlthcm
    INTEGER                                    :: i1, i2, i_l, iwlg

    ! Declarations for intergalactic radiation field -------------------------------------------------------------
    ! Note : To use an intergalactic radiation field, the user has to provide the 3 following parameters
    !        Intergalactic spectra are READ from haardt96.dat in the data directory
    !        To not use an intergalactic radiation field, program has to be compiled with F_haardt96 = 0
    INTEGER, PARAMETER                         :: F_haardt96 = 0      ! FLAG 1 activate intergalactic radiation field
    INTEGER, PARAMETER                         :: F_haardt96_only = 0 ! FLAG 1 suppress Draine radiation field
                                                                      !      0 adds intergal. spect. to Draine rad. field
    ! Other quantities used to initialize the intergalactic spectrum
    REAL (KIND=dp)                             :: redshift = 2.43_dp  ! Redshift of the intergalactic rad. field
    INTEGER                                    :: nredshift           ! number of redshifts with intergalactic spectra provided
    INTEGER, PARAMETER                         :: nlignmax_haardt96=1000 ! Max number of wavelength in haardt96.dat
    INTEGER                                    :: nlign_haardt96      ! number of wavelength in haardt96.dat
    REAL (KIND=dp), ALLOCATABLE                :: tab_redshift(:)     ! table of known redshifts
    REAL (KIND=dp), ALLOCATABLE                :: lam_haardt(:)       ! table of the wavelenght in Ang. in datafile
    REAL (KIND=dp), ALLOCATABLE                :: I_haardt(:,:)       ! spectra in function of wavelength and redshift
                                                                      ! READ in haardt96.dat in erg s-1 cm-2 A-1 str-1
    REAL (KIND=dp), ALLOCATABLE                :: I_intergal(:)       ! spectrum at the proper redshift
    INTEGER                                    :: num_redshift        ! number of the CLOSEst redshift in data to the one searched
    REAL (KIND=dp)                             :: fact                ! factor used in linear interpolation

    ! Variables used to scale the normal incident radiation field
    REAL (KIND=dp)                             :: mean_rad1_real
    REAL (KIND=dp)                             :: mean_rad2_real
    REAL (KIND=dp)                             :: targ_s_m, targ_l_m, targ_s_p, targ_l_p

   TYPE (RF_TYPE)                              :: rf_incI_c1          !  Component 1 of incident radiation field
                                                                      !  (scaled by radm, [resp. radp])
   TYPE (RF_TYPE)                              :: rf_incI_c2          !  Component 2 of incident radiation field(unscaled)
   TYPE (RF_TYPE)                              :: rf_incI_c3          !  Component 3 of incident radiation field (unscaled)
   TYPE (RF_TYPE)                              :: rf_incI_c4          !  Component 4 of incident radiation field (unscaled)

   ! Variables pour ajouter le champs IR cree par DUSTEM.
   REAL (KIND=dp)                              :: a_IR        ! Coefficients to fit the amount of pah
   REAL (KIND=dp)                              :: b_IR        ! Coefficients to fit the amount of vsg
   REAL (KIND=dp)                              :: c_IR        ! Coefficients to fit the amount of bg
   REAL (KIND=dp)                              :: NH_IR       ! Column density to fit the IR data.
   REAL (KIND=dp)                              :: pah_IR      ! 4*pi*I_lambda des PAH (erg s-1 H-1 A-1 sr-1)
   REAL (KIND=dp)                              :: vsg_IR      ! 4*pi*I_lambda des VSG (erg s-1 H-1 A-1 sr-1)
   REAL (KIND=dp)                              :: bg_IR       ! 4*pi*I_lambda des BG  (erg s-1 H-1 A-1 sr-1)
   REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)   :: wl_dustem   ! Longueurs d'onde pour lesquels on a des donnees (A)
   REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)   :: IR_dustem   ! 4*pi*I_lambda totale (erg s-1 H-1 A-1 sr-1)
   INTEGER, PARAMETER                          :: nwlg_dustem = 1500 ! Nombre de points en longueur d'onde
   INTEGER                                     :: j_IR        ! Integer pour faire les boucles.

!-------------- Execution  ------------------------------------------------------------

! Use the correct number of Legendre polynomial roots

    ALLOCATE (xmui_M(nmui))
!   xmui_M = xmui32
!   xmui_M = xmui15
    xmui_M = xmui10
!   xmui_M = xmui06

    !===================================================================================
    ! CREATION OF THE INCIDENT RADIATION FIELD
    ! The radiation field from Far-UV to submillimeter is build by 4 components
    ! - component 1 : Far UV
    ! - component 2 : Near UV, Visible, near I.R.
    ! - component 3 : CMB
    ! - component 4 : I.R.
    !===================================================================================
    IF (ASSOCIATED(rf_incI_c1%Val)) THEN
       CALL RF_FREE (rf_incI_c1)
    ENDIF
    CALL RF_CREATE (rf_incI_c1, 0, nwlg)
    IF (ASSOCIATED(rf_incI_c2%Val)) THEN
       CALL RF_FREE (rf_incI_c2)
    ENDIF
    CALL RF_CREATE (rf_incI_c2, 0, nwlg)
    IF (ASSOCIATED(rf_incI_c3%Val)) THEN
       CALL RF_FREE (rf_incI_c3)
    ENDIF
    CALL RF_CREATE (rf_incI_c3, 0, nwlg)
    IF (ASSOCIATED(rf_incI_c4%Val)) THEN
       CALL RF_FREE (rf_incI_c4)
    ENDIF
    CALL RF_CREATE (rf_incI_c4, 0, nwlg)

    ! Draine Formula: Specific intensity
    ! Radiation field in erg cm-2 s-1 A-1 sterad-1

    !--------------------------------------------------------------------------------
    ! Component 1 : Far-UV
    !--------------------------------------------------------------------------------
    IF (F_ISRF == 1) THEN  ! Mathis
       ISRF_name = "MMP83+JB  "
       ! Far UV (fit JLB Jan 2008)
       CALL RF_LOCATE (rf_wl, wl_cut_V, I_cut_V)
       rf_incI_c1%Val(0:i_cut_V) = (TANH(4.07e-3_dp*rf_wl%Val(0:i_cut_V)-4.5991_dp) + 1.0_dp) &
                              * 107.192_dp * rf_wl%Val(0:i_cut_V)**(-2.89_dp)
       rf_incI_c1%Val(i_cut_V+1:nwlg) = 2.0_dp * 107.192_dp * rf_wl%Val(i_cut_V+1:nwlg)**(-2.89_dp)
       rf_incI_c1%Ord = (/ (ii, ii=rf_incI_c1%lower,rf_incI_c1%upper) /)

    ELSE IF (F_ISRF == 2) THEN ! Draine

       ISRF_name = "Draine  "
       ! Draine's formula extended to 10000.0 A to give a smooth transition to Visible
       ! 2 Fev 2012 - JLB - Still not enough for Rad = 1.0e5 (required by Leiden Workshop)
       !                    Extend to 1.0e5 A = 10 micron
       CALL RF_LOCATE (rf_wl, wl_cut_D, I_cut_D)
       ! Draine 1978 radiation field
       ! Expression from M. Kopp PhD thesis page 284, from Sternberg and Dalgarno 1985.
       rf_incI_c1%Val(0:I_cut_D) = 6.36E7_dp / rf_wl%Val(0:I_cut_D)**4 &
                              - 1.0237E11_dp / rf_wl%Val(0:I_cut_D)**5 &
                              + 4.0812E13_dp / rf_wl%Val(0:I_cut_D)**6
       rf_incI_c1%Val = rf_incI_c1%Val / (4.0 * xpi) ! rf_incI is now in erg cm-2 s-1 str-1 A-1
       rf_incI_c1%Ord = (/ (ii, ii=rf_incI_c1%lower,rf_incI_c1%upper) /)
    ENDIF

    !--------------------------------------------------------------------------------
    ! Component 2 : Near-UV, visible, near I.R
    !--------------------------------------------------------------------------------
    ! 3 diluted Black Bodies (from Mathis, Mezger and Panagia 1983, A&A, 128, 212)
    ! Values revised by JLB to take account of far UV.
    !    6 IV 2010
    ! Fit redone by JLB to bring into accordance with DIRBE data provided by Francois Boulanger
    ! Note: still 3 BB component although the first 2 are almost degenerate. Suppress one?
    !--------------------------------------------------------------------------------
    DO ii = 0, nwlg
    !  rf_incI_c2%Val(ii) = 1.05e-14_dp * BBL(rf_wl%Val(ii),7127.0_dp) &
    !                     + 1.28e-13_dp * BBL(rf_wl%Val(ii),4043.0_dp) &
    !                     + 3.30e-13_dp * BBL(rf_wl%Val(ii),2930.0_dp)
       rf_incI_c2%Val(ii) = 2.07e-14_dp * BBL(rf_wl%Val(ii),6184.0_dp) &
                          + 1.09e-14_dp * BBL(rf_wl%Val(ii),6123.0_dp) &
                          + 1.52e-12_dp * BBL(rf_wl%Val(ii),2539.0_dp)
    ENDDO
    rf_incI_c2%Ord = (/ (ii, ii=rf_incI_c1%lower,rf_incI_c1%upper) /)

    !--------------------------------------------------------------------------------
    ! Component 3 : CMB
    !--------------------------------------------------------------------------------
    rf_incI_c3%Ord = (/ (ii, ii=rf_incI_c1%lower,rf_incI_c1%upper) /)
    DO ii = 0, nwlg
       rf_incI_c3%Val(ii) = BBL(rf_wl%Val(ii),tcmb)
    ENDDO

    !--------------------------------------------------------------------------------
    ! Component 4 : Dust contribution to the IR
    !--------------------------------------------------------------------------------
    ! Manuel Gonzalez-Garcia, 20 november 2008
    ! This component has been estimated thanks to the DUSTEM code
    !--------------------------------------------------------------------------------
    ! Coefficients to fit IR Interstellar field from DUSTEM code are:
    a_IR = 52.5593_dp
    b_IR = 75.0854_dp
    c_IR = 16.1213_dp
    NH_IR = 1.0e20_dp
    ! Allocate the variable IR, which will contain data of the IR interstellar field.
    ALLOCATE(IR_dustem(nwlg_dustem))
    ALLOCATE(wl_dustem(nwlg_dustem))
    OPEN(iwrtmp,file='./../data/Astrodata/IR_field_dustem.dat',status='old')
    DO ii = 1, nwlg_dustem
       READ(iwrtmp,*) wl_dustem(ii), pah_IR, vsg_IR, bg_IR
       IR_dustem(ii) = (a_IR*pah_IR + b_IR*vsg_IR + c_IR*bg_IR) * NH_IR
    ENDDO
    CLOSE(iwrtmp)

    rf_incI_c4%Ord = (/ (ii, ii=rf_incI_c1%lower,rf_incI_c1%upper) /)
    DO ii = 0,nwlg
       ! 31 Oct 2011 - Change upper value
       ! IF (rf_wl%Val(ii) < 1.0e4_dp .OR. rf_wl%Val(ii) > 8.0e6_dp ) THEN
       IF (rf_wl%Val(ii) < 1.0e4_dp) THEN
          rf_incI_c4%Val(ii) = 0.0_dp
       ELSE
          DO j_IR = 1, nwlg_dustem-1
             IF (rf_wl%Val(ii) >= wl_dustem(j_IR) .AND. rf_wl%Val(ii) < wl_dustem(j_IR+1)) THEN
                rf_incI_c4%Val(ii) = IR_dustem(j_IR) &
                                   + (IR_dustem(j_IR+1) - IR_dustem(j_IR)) &
                                   * (rf_wl%Val(ii)-wl_dustem(j_IR)) &
                                   / (wl_dustem(j_IR+1)-wl_dustem(j_IR))
             ENDIF
          ENDDO
       ENDIF
    ENDDO

    DEALLOCATE(IR_dustem)
    DEALLOCATE(wl_dustem)

    !===================================================================================
    ! BUILD FULL SPECTRUM (unscaled)
    !===================================================================================
    rf_incIm%Ord = rf_incI_c1%Ord
    rf_incIm%Val = rf_incI_c1%Val + rf_incI_c2%Val + rf_incI_c3%Val + rf_incI_c4%Val
    ! This file is written in the unit used by DUSTEM:
    !           x: Lambda in micrometer
    !           y: J_nu in erg s-1 cm-2 Hz-1 (note that integration on sterad is done)
    OPEN (iwrtmp,FILE=TRIM(dustem_dat)//'ISRF.DAT',STATUS="unknown")
    WRITE(iwrtmp,'("#",/,"#",/,"#",/,"#",/,"#",/,"#",/,i8)') nwlg
    DO ii = 0, nwlg
       WRITE (iwrtmp,*) 1.0e-4_dp*rf_wl%Val(ii), J_to_u*1.0e-8_dp*rf_incIm%Val(ii)*(rf_wl%Val(ii))**2
    ENDDO
    CLOSE (iwrtmp)

    !===================================================================================
    ! COMPUTE THE RADIATION FIELD INTEGRATED ENERGY IN FREE SPACE (TAKEN FROM STV2Z.F90)
    !===================================================================================

     CALL RF_CREATE(rf_lamI,0,nwlg)
     rf_lamI%Val = rf_wl%Val * rf_incIm%Val

    !--------------------------------------------------------------------------------
    ! Integration of UV radiation field from Lyman up to Habing limit
    ! (wl_lym = 911 Å ; wl_hab = 2400 Å) - BG 04/2013
    !     change to wl_6ev to compute up to 6 eV
    !--------------------------------------------------------------------------------
     CALL RF_CREATE(rf_tmp,1,2)
     CALL RF_LOCATE(rf_wl, wlth0, i1)
     rf_tmp%Val(1) = 1.0_dp
     rf_tmp%Ord(1) = rf_wl%Ord(i1)
     CALL RF_LOCATE(rf_wl, wl_hab, i2)
     rf_tmp%Val(2) = 1.0_dp
     rf_tmp%Ord(2) = rf_wl%Ord(i2)
     int_rad0 = SECT_INT(rf_tmp, rf_incIm, wl_pointer, wl_hab) * J_to_u
     flu_rad0 = SECT_INT(rf_tmp, rf_lamI, wl_pointer, wl_hab) * J_to_u / (1.0e8_dp * xhp)
     CALL RF_FREE (rf_lamI)
     CALL RF_FREE (rf_tmp)

     PRINT *, " "
     PRINT *, " Standard field in free space:"
     WRITE (iscrn, '("  U0:",1pe12.4," erg cm-3,     ","from 912 to",0pf9.2," Angstrom")') int_rad0, wl_hab
     WRITE (iscrn, '("  F0:",1pe12.4," phot cm-2 s-1,","from 912 to",0pf9.2," Angstrom")') flu_rad0, wl_hab
     WRITE (iwrit2,'("  U0:",1pe12.4," erg cm-3,     ","from 912 to",0pf9.2," Angstrom")') int_rad0, wl_hab
     WRITE (iwrit2,'("  F0:",1pe12.4," phot cm-2 s-1,","from 912 to",0pf9.2," Angstrom")') flu_rad0, wl_hab

    ! Now scale left and right side differently:

    rf_incIm%Val = radm_ini * rf_incI_c1%Val + rf_incI_c2%Val + rf_incI_c3%Val + rf_incI_c4%Val
    rf_incIp%Val = radp_ini * rf_incI_c1%Val + rf_incI_c2%Val + rf_incI_c3%Val + rf_incI_c4%Val

    ! 30 oct 2011: try enhancing also the IR field to pump CO
!   rf_incIm%Val = radm_ini * rf_incI_c1%Val + rf_incI_c2%Val + rf_incI_c3%Val + radm_ini * rf_incI_c4%Val
!   rf_incIp%Val = radp_ini * rf_incI_c1%Val + rf_incI_c2%Val + rf_incI_c3%Val + radp_ini * rf_incI_c4%Val

    ! 27 June 2011 write scaled radiation field
    ! This file is written in the unit used by DUSTEM:
    !           x: Lambda in micrometer
    !           y: J_nu in erg s-1 cm-2 Hz-1 (note that integration on sterad is done)
    OPEN (iwrtmp,FILE=TRIM(dustem_dat)//'ISRF.DAT',STATUS="unknown")
    WRITE(iwrtmp,'("#",/,"#",/,"#",/,"#",/,"#",/,"#",/,i8)') nwlg
    DO ii = 0, nwlg
       WRITE (iwrtmp,*) 1.0e-4_dp*rf_wl%Val(ii), J_to_u*1.0e-8_dp*rf_incIm%Val(ii)*(rf_wl%Val(ii))**2
    ENDDO
    CLOSE (iwrtmp)

    !===================================================================================
    ! NORMAL - EXTERNAL RADIATION FIELD
    ! An external radiation field, e.g. from a star, is added if d_sour .NE. 0 or srcpp is set to a flag
    ! d_sour > 0 : the star is on the back side of the cloud
    ! d_sour < 0 : the star is on the observer side of the cloud
    !===================================================================================

    IF (F_Norm_RF /= 0) THEN

       ! Everywhere in this block: tag "m" for left side, "p" for right side
       ! rf_star is the additional contribution to boundary conditions on most normal
       ! term in the Legendre expansion.
       IF (ASSOCIATED(rf_star_m%Val)) THEN
          CALL RF_FREE(rf_star_m)
       ENDIF
       IF (ASSOCIATED(rf_star_p%Val)) THEN
          CALL RF_FREE(rf_star_p)
       ENDIF
       CALL RF_CREATE(rf_star_m,0,nwlg)
       CALL RF_CREATE(rf_star_p,0,nwlg)
       rf_star_m%Ord = (/ (ii, ii=rf_star_m%lower,rf_star_m%upper) /)
       rf_star_p%Ord = (/ (ii, ii=rf_star_p%lower,rf_star_p%upper) /)
       ALLOCATE (perp_RF_m(0:nwlg))
       ALLOCATE (perp_RF_p(0:nwlg))

       IF (srcpp(1:2) == 'F_') THEN
          i_l = 1
          DO iwlg = 0, nwlg
             wlth = rf_wl%Val(iwlg)
             CALL INTERP_X (wlth, perp_RF_m(iwlg), i_l)
          ENDDO
          perp_RF_p = perp_RF_m
       ELSE IF (srcpp(1:2) == 'PP') THEN
          ! The factor 3/2 here is a trick that leads to a cancelation of a factor 2 below
          DO iwlg = 0, nwlg
             perp_RF_m(iwlg) = 1.5_dp * rf_incIm%Val(iwlg)
             perp_RF_p(iwlg) = 1.5_dp * rf_incIp%Val(iwlg)
          ENDDO
       ELSE
          DO iwlg = 0, nwlg
             wlthcm = rf_wl%Val(iwlg) * 1.0e-8_dp
             perp_RF_m(iwlg) = 1.0e-8_dp*2.0_dp*xhp*clum*clum / wlthcm**5 / (EXP(hcsurk/(teff*wlthcm)) - 1.0_dp)
          ENDDO
          perp_RF_p = perp_RF_m
       ENDIF

       IF (F_Norm_RF == 1 .OR. F_Norm_RF == 3) THEN
          DO iwlg = 0, nwlg
             rf_star_m%Val(iwlg) = ((perp_RF_m(iwlg) - rf_incIm%Val(iwlg)) * 2.0_dp * geom / (2.0_dp - xmui_M(1) - xmui_M(2)))
          ENDDO
       ENDIF
       IF (F_Norm_RF == 2 .OR. F_Norm_RF == 3) THEN
          DO iwlg = 0, nwlg
             rf_star_p%Val(iwlg) = ((perp_RF_p(iwlg) - rf_incIp%Val(iwlg)) * 2.0_dp * geom / (2.0_dp - xmui_M(1) - xmui_M(2)))
          ENDDO
       ENDIF

       ! Switch off isotropic contribution for Plane Parallel RF
       IF (srcpp(1:2) == 'PP') THEN
          DO iwlg = 0, nwlg
             rf_incIm%Val(iwlg) = 0.0_dp
             rf_incIp%Val(iwlg) = 0.0_dp
          ENDDO
       ENDIF

       CALL RF_CREATE(rf_Isum,0,nwlg)
       rf_Isum%Ord = (/ (ii, ii=rf_star_m%lower,rf_star_m%upper) /)
       CALL RF_CREATE(rf_tmp,1,2)
       CALL RF_LOCATE(rf_wl, wlth0, i1)
       rf_tmp%Val(1) = 1.0_dp
       rf_tmp%Ord(1) = rf_wl%Ord(i1)
       CALL RF_LOCATE(rf_wl, wl_hab, i2)
       rf_tmp%Val(2) = 1.0_dp
       rf_tmp%Ord(2) = rf_wl%Ord(i2)

       ! mean_rad1_real is the mean energy density resulting from integration on 2 pi sterad
       ! i.e. from left side only - analytical exact result
       mean_rad1_real = 0.0_dp
       IF (F_Norm_RF == 1 .OR. F_Norm_RF == 3) THEN
          DO iwlg = 0, nwlg
             rf_Isum%Val(iwlg) = perp_RF_m(iwlg) * geom + rf_incIm%Val(iwlg) * (1.0_dp - geom)
          ENDDO
          mean_rad1_real = SECT_INT(rf_tmp, rf_Isum, wl_pointer, wl_hab)
          ! Correct here the factor 3/2 used above for "'PP"
          IF (srcpp(1:2) == 'PP') THEN
             mean_rad1_real = mean_rad1_real * 1.0_dp / 3.0_dp
          ENDIF
       ENDIF

       ! mean_rad2_real is the mean energy density resulting from integration on 2 pi sterad
       ! i.e. from right side only - analytical exact result
       mean_rad2_real = 0.0_dp
       IF (F_Norm_RF == 2 .OR. F_Norm_RF == 3) THEN
          DO iwlg = 0, nwlg
             rf_Isum%Val(iwlg) = perp_RF_p(iwlg) * geom + rf_incIp%Val(iwlg) * (1.0_dp - geom)
          ENDDO
          mean_rad2_real = SECT_INT(rf_tmp, rf_Isum, wl_pointer, wl_hab)
          ! Correct here the factor 3/2 used above for "'PP"
          IF (srcpp(1:2) == 'PP') THEN
             mean_rad2_real = mean_rad2_real * 1.0_dp / 3.0_dp
          ENDIF
       ENDIF
       PRINT *, " i2:", i2, mean_rad1_real, mean_rad2_real

       ! We compute the numerical approximation to the integral twice (for two
       ! values of the normal contribution), then do a linear interpolation to
       ! the analytical value.

       alpha_m = 1.0_dp
       alpha_p = 1.0_dp
       IF (isoneside == 1) THEN
          CALL ETIN901S (i2)
          targ_s_m = mean_rad1 - mean_rad1_real
          PRINT *, " targ_s:_m", targ_s_m
       ELSE
          CALL ETIN90 (i2)
          targ_s_m = mean_rad1 - mean_rad1_real
          targ_s_p = mean_rad2 - mean_rad2_real
          PRINT *, " targ_s_m:", targ_s_m, " targ_s_p:", targ_s_p
       ENDIF

       alpha_m = 2.0_dp
       alpha_p = 2.0_dp
       IF (isoneside == 1) THEN
          CALL ETIN901S (i2)
          targ_l_m = mean_rad1 - mean_rad1_real
          PRINT *, " targ_l:_m", targ_l_m
       ELSE
          CALL ETIN90 (i2)
          targ_l_m = mean_rad1 - mean_rad1_real
          targ_l_p = mean_rad2 - mean_rad2_real
          PRINT *, " targ_l_m:", targ_l_m, " targ_l_p:", targ_l_p
       ENDIF

       IF (isoneside == 1) THEN
          alpha_m = 1.0_dp - targ_s_m / (targ_l_m - targ_s_m)
          CALL ETIN901S (i2)
          PRINT *, alpha_m, mean_rad1, mean_rad1_real
       ELSE
          IF (F_Norm_RF == 1 .OR. F_Norm_RF == 3) THEN
             alpha_m = 1.0_dp - targ_s_m / (targ_l_m - targ_s_m)
          ELSE
             alpha_m = 1.0_dp
          ENDIF
          IF (F_Norm_RF == 2 .OR. F_Norm_RF == 3) THEN
             alpha_p = 1.0_dp - targ_s_p / (targ_l_p - targ_s_p)
          ELSE
             alpha_p = 1.0_dp
          ENDIF
          CALL ETIN90 (i2)
          PRINT *, alpha_m, mean_rad1, mean_rad1_real
          PRINT *, alpha_p, mean_rad2, mean_rad2_real
       ENDIF

    ENDIF

    !===================================================================================
    ! INTERGALACTIC RADIATION FIELD (Haardt & Madau 1996, ApJ, 461, 20)
    !===================================================================================

    IF (F_haardt96 == 1) THEN

       IF (F_haardt96_only == 1) THEN
          ! On ne prend en compte que Haardt & Madau 96
          ! aussi on commence par "effacer" Draine
          rf_incIm%Val(0:nwlg) = 0.0_dp
          rf_incIp%Val(0:nwlg) = 0.0_dp
          ! Sinon, le champ de intergalactique viendra s'ajouter a Draine
       ENDIF

       ! Modification of the temperature of the CMB
       tcmb = tcmb0 * (1.0_dp + redshift)

       fichier = TRIM(data_dir)//TRIM("Astrodata/haardt96.dat")
       WRITE(iscrn,"('Opening file :',A30)") TRIM(fichier)
       OPEN (iwrtmp, file = fichier, status="old", action="READ")
       READ(iwrtmp,*)           ! saute ligne de commentaire
       READ(iwrtmp,*)           ! saute ligne de commentaire
       READ(iwrtmp,*) nredshift ! number of redshifts in table
       ! Allocation de memoire pour stocker les donnees de la table
       ALLOCATE(tab_redshift(nredshift))
       ALLOCATE (lam_haardt(nlignmax_haardt96))
       ALLOCATE (I_haardt(nlignmax_haardt96,nredshift))
       ALLOCATE(I_intergal(nlignmax_haardt96))
       ! Cherche la liste des redshifts disponibles dans la table
       READ(iwrtmp,*) (tab_redshift(ii), ii=1, nredshift)

       !Lecture des donnees
       DO ii = 1, nlignmax_haardt96
          READ(iwrtmp,*,END=110) lam_haardt(ii), (I_haardt(ii,k), k=1, nredshift)
!          PRINT *,ii,lam_haardt(ii),I_haardt(ii,26)
       ENDDO
110    CONTINUE
       nlign_haardt96 = ii - 1 ! number of wavelength in haardt96.dat
       CLOSE(iwrtmp)

       ! Search in haardt96.dat for an interval of redshift containing the whished redshift
       ! If searched redshift outside of data :
       IF (redshift < tab_redshift(1)) THEN
          PRINT *,"Redshift too small compared to data"
          PRINT *,"Switch to smallest redshift in data :", tab_redshift(1)
          redshift = tab_redshift(1)
       ENDIF
       IF (redshift > tab_redshift(nredshift)) THEN
          PRINT *,"Redshift too big compared to data"
          PRINT *,"Switch to highest redshift in data :",tab_redshift(nredshift)
          redshift = tab_redshift(nredshift)
       ENDIF
       ! Searched redshift inside data range
       ! On traite les cas particuliers :
       ! Si le redshift cherche est le premier de la liste
       IF (redshift == tab_redshift(1)) THEN
          I_intergal(:) = I_haardt(:,1)
       ENDIF
       ! Si le redshift cherche est le dernier de la liste
       IF (redshift == tab_redshift(nredshift)) THEN
          I_intergal(:) = I_haardt(:,nredshift)
       ENDIF
       ! Cas general
       IF ( tab_redshift(1) < redshift .AND. redshift < tab_redshift(nredshift)) THEN
          num_redshift = 1
          DO WHILE (redshift > tab_redshift(num_redshift))
             num_redshift = num_redshift + 1
             PRINT *,num_redshift, redshift, tab_redshift(num_redshift), tab_redshift(num_redshift+1)
          ENDDO
          ! Interpolation lineaire du redshift
          IF (redshift == tab_redshift(num_redshift-1)) THEN
             I_intergal(:) = I_haardt(:,num_redshift-1)
             PRINT *,"Exact redshift found in data :",num_redshift,redshift, tab_redshift(num_redshift-1)
          ELSE IF (redshift == tab_redshift(num_redshift)) THEN
             I_intergal(:) = I_haardt(:,num_redshift)
             PRINT *,"Exact redshift found in data :",num_redshift,redshift, tab_redshift(num_redshift)
          ELSE IF ((tab_redshift(num_redshift-1) < redshift) .AND. (redshift < tab_redshift(num_redshift))) THEN
             fact = (redshift - tab_redshift(num_redshift-1)) / (tab_redshift(num_redshift) - tab_redshift(num_redshift-1))
             I_intergal(:) = I_haardt(:,num_redshift-1) * (1.0_dp-fact) + I_haardt(:,num_redshift) * fact
             PRINT *,"Interpolation of intergal. spect. between z :",tab_redshift(num_redshift-1), tab_redshift(num_redshift)
          ENDIF
       ENDIF

       ! Remplissage du tableau des intensites avec interpolation lineaire aux lam. necessaires
       k = 0
       DO WHILE (rf_wl%Val(k) < lam_haardt(1))
          PRINT *,"Warning : Data for intergal. spectrum do not begin at a small enough lambda"
          PRINT *,"          Extrapolation"
          rf_incIm%Val(k) = rf_incIm%Val(k) + I_intergal(1)
          rf_incIp%Val(k) = rf_incIp%Val(k) + I_intergal(1)
          k = k + 1
       ENDDO
       j = 1
       DO ii = k, nwlg
          DO WHILE (rf_wl%Val(ii) > lam_haardt(j))
             j = j + 1
          ENDDO
          IF (lam_haardt(j) == rf_wl%Val(ii)) THEN
             rf_incIm%Val(ii) = rf_incIm%Val(ii) + I_intergal(j)
             rf_incIp%Val(ii) = rf_incIp%Val(ii) + I_intergal(j)
          ELSE IF ((lam_haardt(j-1) < rf_wl%Val(ii)) .AND. (rf_wl%Val(ii) < lam_haardt(j))) THEN
             ! On interpole en lambda dans la table haardt.dat
             ! interpolation pour le premier redshift
             fact = (rf_wl%Val(ii) - lam_haardt(j-1)) / (lam_haardt(j) - lam_haardt(j-1))
             rf_incIm%Val(ii) = rf_incIm%Val(ii) + I_intergal(j-1) * (1.0_dp - fact) &
                                                 + fact * I_intergal(j)
             rf_incIp%Val(ii) = rf_incIp%Val(ii) + I_intergal(j-1) * (1.0_dp - fact) &
                                                 + fact * I_intergal(j)
          ELSE IF (rf_wl%Val(ii) > lam_haardt(nlign_haardt96)) THEN
             rf_incIm%Val(ii) = rf_incIm%Val(ii) + I_intergal(nlign_haardt96)
             rf_incIp%Val(ii) = rf_incIp%Val(ii) + I_intergal(nlign_haardt96)
             PRINT *,"WARNING : Not enough point in lambda in haardt96.dat"
             PRINT *,"          Lambda searched :", rf_wl%Val(ii)
          ENDIF
       ENDDO

       ! Remarque : Pas besoin de toucher a rf_incIm%Ord. Il a ete correctement construit lors
       !            du remplissage du champ de Draine.

       DEALLOCATE(tab_redshift)
       DEALLOCATE(lam_haardt)
       DEALLOCATE(I_haardt)
       DEALLOCATE(I_intergal)

    ENDIF

!   VERIFICATION DU CHAMP
!     OPEN (iwrtmp,file="toto.dat",status="unknown",action="WRITE")
!     DO ii = 0,nwlg
!        WRITE(iwrtmp,*) rf_wl%Val(ii), rf_incIm%Val(ii)
!     ENDDO
!     CLOSE(iwrtmp)
!     PRINT *,"Fin d'ecriture du champ initial dans toto.dat"
!     STOP

    CALL RF_FREE (rf_incI_c1)
    CALL RF_FREE (rf_incI_c2)
    CALL RF_FREE (rf_incI_c3)
    CALL RF_FREE (rf_incI_c4)
    IF (ALLOCATED(perp_RF_m)) THEN
       DEALLOCATE (perp_RF_m)
       DEALLOCATE (perp_RF_p)
    ENDIF

END SUBROUTINE RF_BOUND

END MODULE PXDR_TRANSFER
