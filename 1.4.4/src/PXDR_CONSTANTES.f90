
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

MODULE PXDR_CONSTANTES

  ! "kind PARAMETER"  for double precision
  INTEGER, PUBLIC, PARAMETER :: dp = SELECTED_REAL_KIND(P=15)
  INTEGER, PUBLIC, PARAMETER :: ilk = SELECTED_INT_KIND(10)

  ! usual numbers
  REAL (KIND=dp), PUBLIC, PARAMETER :: istiny  = 1.0e-60_dp
  REAL (KIND=dp), PUBLIC, PARAMETER :: hugest  = HUGE(0.0_dp)
  REAL (KIND=dp), PUBLIC, PARAMETER :: tiniest = TINY(0.0_dp)
  ! Use internal definitions if possible. Else, switch to proxy
! REAL (KIND=dp), PUBLIC, PARAMETER :: l_hu    = LOG(hugest)
! REAL (KIND=dp), PUBLIC, PARAMETER :: l_ty    = LOG(tiniest)
! REAL (KIND=dp), PUBLIC, PARAMETER :: l_10    = LOG(10.0_dp)
  REAL (KIND=dp), PUBLIC, PARAMETER :: l_hu    = 250.0_dp
  REAL (KIND=dp), PUBLIC, PARAMETER :: l_ty    = -250.0_dp
  REAL (KIND=dp), PUBLIC, PARAMETER :: l_10    = 2.30258509299405_dp

  ! Input-Output units:
  INTEGER, PUBLIC, PARAMETER        :: iread1 =  1
  INTEGER, PUBLIC, PARAMETER        :: iread2 =  2
  INTEGER, PUBLIC, PARAMETER        :: iread3 =  3
  INTEGER, PUBLIC, PARAMETER        :: iwrit1 = 11
  INTEGER, PUBLIC, PARAMETER        :: iwrit2 = 12
  INTEGER, PUBLIC, PARAMETER        :: iwrtmp = 13
  INTEGER, PUBLIC, PARAMETER        :: iscrn  =  6

  ! Flags
  INTEGER, PUBLIC, PARAMETER        :: F_OUTPUT = 2 ! 1: Do not write all line properties in output file
                                                    ! 2: Write all line properties in output files
  ! F_W_RF_ALB controls if the full radiation field and effective albedo files are written
  !        For full transfer this file can become quite large (up to 1 Go)
  !        This is a binary file. Cut or partial map can be extracted with the auxiliary tool
  !        "read_rf_alb.f90", found in src/OTHER_PROG
  !        F_W_RF_ALB = 1 (default) : Write full radiation field
  !        F_W_RF_ALB = 0           : skip writing
  INTEGER, PUBLIC                   :: F_W_RF_ALB = 1

  ! Physical constants (Partly from  rev. mod. phys. 59 , 1121 (1987) )
  ! Revised from physics.nist.gov/constants - 11 VIII 2011 (JLB)
  REAL (KIND=dp), PUBLIC, PARAMETER :: amu  = 1.660538921e-24_dp       ! Atomic mass unit (g)
  REAL (KIND=dp), PUBLIC, PARAMETER :: xmh  = 1.673532638e-24_dp       ! H atom mass (g)
  REAL (KIND=dp), PUBLIC, PARAMETER :: xmp  = 1.672621777e-24_dp       ! proton mass (g)
  REAL (KIND=dp), PUBLIC, PARAMETER :: xme  = 9.10938291e-28_dp        ! electron mass (g)
  REAL (KIND=dp), PUBLIC, PARAMETER :: xqe  = 4.80320450571347e-10_dp  ! electric charge (esu)
  REAL (KIND=dp), PUBLIC, PARAMETER :: xqe2 = 2.30707735237062e-19_dp  ! xqe * xqe
  REAL (KIND=dp), PUBLIC, PARAMETER :: xk   = 1.3806488e-16_dp         ! Boltzman constant k (erg deg-1)
  REAL (KIND=dp), PUBLIC, PARAMETER :: xhp  = 6.62606957e-27_dp        ! Planck constant h (erg s)
  REAL (KIND=dp), PUBLIC, PARAMETER :: xr   = 8.3144621e7_dp           ! Perfect gaz constant R (erg deg-1 mole-1)
  REAL (KIND=dp), PUBLIC, PARAMETER :: clum = 2.99792458e10_dp         ! Celerity of light c (cm s-1)
  REAL (KIND=dp), PUBLIC, PARAMETER :: xpi  = 3.1415926535897932384_dp ! Pi (!)
  REAL (KIND=dp), PUBLIC, PARAMETER :: sqpi = 1.77245385090551602_dp   ! Sqrt(pi)
  REAL (KIND=dp), PUBLIC, PARAMETER :: g_euler = 0.5772156649015328606065120900824024310422_dp
  REAL (KIND=dp), PUBLIC            :: stebol                          ! Stefan-Boltzmann constant (erg cm-2 s-1 K-4)
  REAL (KIND=dp), PUBLIC            :: hiop                            ! Ionisation potential of H (in erg)
                                                                       ! = 2 pi**2 e**4 m / h**2 (Lang p 244)
  ! Astrophysical constants
  REAL (KIND=dp), PUBLIC, PARAMETER :: tcmb0     = 2.732_dp            ! cosmological Black Body
  REAL (KIND=dp), PUBLIC, PARAMETER :: rsun      = 6.95508e10_dp       ! ApJ Let, 1998, 500L, 195
  REAL (KIND=dp), PUBLIC, PARAMETER :: V_band_wl = 5510.0_dp           ! V Photometric band (Angstrom)
  REAL (KIND=dp), PUBLIC, PARAMETER :: B_band_wl = 4450.0_dp           ! B Photometric band (Angstrom)
  REAL (KIND=dp), PUBLIC, PARAMETER :: FP_up_wl  = 3030.0_dp           ! Fitzpatrick & Massa fit upper limit

  ! Conversion factors
  REAL (KIND=dp), PUBLIC            :: tautav                          ! From Optical depth (tau) to Av (in mag)
  REAL (KIND=dp), PUBLIC            :: J_to_u                          ! From mean Intensity to energy density
                                                                       !      J_to_u = 4 pi / c
  REAL (KIND=dp), PUBLIC            :: cs2rate                         ! Effective collision strength to rate coefficient
                                                                       ! = SQRT(xme * 0.5_dp / (xpi * xk)) &
                                                                       ! * (xhp / xme)**2 * 0.5_dp / xpi
  REAL (KIND=dp), PUBLIC, PARAMETER :: e2smc2 = 2.8179403267e-13_dp    ! r0 = e2 / (m*c**2) (cm) - Lang (classical electron radius)
  REAL (KIND=dp), PUBLIC, PARAMETER :: mecspie2 = 37.6788493963118_dp  ! me * c / (pi * e**2)
  REAL (KIND=dp), PUBLIC, PARAMETER :: hcsurk  = 1.43877695998382_dp   ! From Wave number (cm-1) to Temperature (Kelvin)
  REAL (KIND=dp), PUBLIC, PARAMETER :: everg   = 1.602176565e-12_dp    ! From Electron-Volt to Erg
  REAL (KIND=dp), PUBLIC, PARAMETER :: evtoK   = 11604.5193028089_dp   ! eV to K
  REAL (KIND=dp), PUBLIC, PARAMETER :: calev   = 4.3363e-2_dp          ! From kilocal mole-1 to Ev
  REAL (KIND=dp), PUBLIC, PARAMETER :: pccm    = 3.086e18_dp           ! From parsec to cm
  REAL (KIND=dp), PUBLIC, PARAMETER :: aucm    = 14959787070000.0_dp   ! From Astronomical Unit to cm (Exact - Oct 2012)
  REAL (KIND=dp), PUBLIC, PARAMETER :: ryd2ang = 911.267050550915_dp   ! Rydberg to Angstrom conversion
  REAL (KIND=dp), PUBLIC, PARAMETER :: deksamu = 166289242.912603_dp   ! 2 * k / amu
  REAL (KIND=dp), PUBLIC, PARAMETER :: Myr     = 31556925216000.0_dp   ! 1 Myr in second (with 1 yr = 365.24219 days)

  ! Numerical iterations and precision related constants
  INTEGER, PUBLIC, PARAMETER        :: miter  = 50                 ! Max global iterations for a slab
  REAL (KIND=dp), PUBLIC, PARAMETER :: convli = 1.0e-3_dp          ! Relative error in global iterations

  ! Various initial or maximum dimensions
  INTEGER, PUBLIC, PARAMETER        :: nes    = 600                ! Max number of chemical species (220)
  INTEGER, PUBLIC, PARAMETER        :: np     = 602                ! Max number of chemical species + photons + CR (222)
  INTEGER, PUBLIC, PARAMETER        :: natom  = 21                 ! Number of chemical elements (atoms)
  INTEGER, PUBLIC, PARAMETER        :: nqm    = 8000               ! Max number of chemical reactions (8000)
  INTEGER, PUBLIC, PARAMETER        :: noptm  = 2000               ! Max number of optical depth points (500)

  ! H2 related dimensions
  INTEGER, PUBLIC, PARAMETER                                  :: nlevh2 = 318  ! Number of H2 rovibrational levels (X)
  INTEGER, PUBLIC, PARAMETER                                  :: njh2   = 29   ! Highest rotational level of H2 included
  INTEGER, PUBLIC, PARAMETER                                  :: nvxh2  = 14   ! Highest vibrational level (X)
  INTEGER, PUBLIC, PARAMETER                                  :: nvbh2  = 37   ! Highest vibrational level (B)
  INTEGER, PUBLIC, PARAMETER                                  :: nvch2  = 13   ! Highest vibrational level (C)
  INTEGER, PUBLIC, PARAMETER                                  :: nvbc   = 5    ! Highest vibrational level (B and C)
                                                                               ! for which pop is saved
  INTEGER, PUBLIC, PARAMETER                                  :: njbc   = 15   ! Highest rotational level (B and C)
                                                                               ! for which pop is saved

  ! H2 structure related quantities
  REAL (KIND=dp), PUBLIC                                      :: h2para        ! Total abundance of para-H2
  REAL (KIND=dp), PUBLIC                                      :: h2orth        ! Total abundance of ortho-H2
  INTEGER, PUBLIC, DIMENSION (nlevh2)                         :: njl_h2 = (/ &
        0,  1,  2,  3,  4,  5,  6,  7,  8,  0,  1,  2,  3,  9,  4, &
        5, 10,  6, 11,  7,  8,  0,  1, 12,  2,  3,  9,  4, 13,  5, &
       10,  6, 14, 11,  7,  8,  0,  1, 12,  2, 15,  3,  9,  4, 13, &
        5, 16, 10,  6, 14, 11,  7, 17,  8,  0,  1,  2, 12, 15,  3, &
        9,  4, 18, 13,  5, 10, 16,  6, 19, 14,  7, 11, 17,  0,  8, &
        1,  2, 12, 15,  3, 20,  9,  4, 18,  5, 13, 10, 16,  6, 21, &
        7, 14, 11, 19, 17,  0,  8,  1,  2, 12,  3, 15, 22,  9,  4, &
       20, 18,  5, 13, 10, 16,  6, 23, 21,  7, 11, 14, 19,  0, 17, &
        8,  1,  2, 12,  3, 15,  9,  4, 22, 24, 20,  5, 18, 13, 10, &
        6, 16,  7, 11, 23, 14, 21, 25, 19,  0,  1,  8, 17,  2,  3, &
       12, 15,  9,  4, 22,  5, 20, 24, 26, 18, 13, 10,  6, 16,  7, &
       11, 14, 21, 23,  0,  1, 19,  8, 25,  2, 17,  3, 27, 12,  4, &
        9, 15,  5, 22, 20, 13, 10, 24,  6, 18, 26, 16,  7, 28, 11, &
       14,  0,  1,  8,  2, 21, 19, 23,  3, 17, 12, 25,  4,  9, 15, &
        5, 27,  6, 10, 13, 29, 20, 22, 18, 24,  7, 16,  0, 11,  1, &
       26,  2, 14,  8,  3, 28,  4, 19, 12, 21,  9, 17, 23,  5, 15, &
       25,  6, 10, 13, 27,  7,  0,  1, 18, 20,  2, 11, 16, 22,  8, &
        3, 14, 29, 24,  4,  9,  5, 12, 26,  6, 19, 17, 15, 21, 10, &
        7,  0,  1, 13, 28, 23,  2,  3,  8, 11,  4, 25,  5, 16, 18, &
        9, 14, 20,  6, 12,  0,  1,  2,  7,  3, 22, 27, 10,  4, 15, &
       17, 24, 19, 29, 21, 26, 18, 23, 20, 28, 25, 22, 24, 27, 26, &
       29, 28, 29 /)

  INTEGER, PUBLIC, DIMENSION (nlevh2)                         :: nvl_h2 = (/ &
        0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  0,  1, &
        1,  0,  1,  0,  1,  1,  2,  2,  0,  2,  2,  1,  2,  0,  2, &
        1,  2,  0,  1,  2,  2,  3,  3,  1,  3,  0,  3,  2,  3,  1, &
        3,  0,  2,  3,  1,  2,  3,  0,  3,  4,  4,  4,  2,  1,  4, &
        3,  4,  0,  2,  4,  3,  1,  4,  0,  2,  4,  3,  1,  5,  4, &
        5,  5,  3,  2,  5,  0,  4,  5,  1,  5,  3,  4,  2,  5,  0, &
        5,  3,  4,  1,  2,  6,  5,  6,  6,  4,  6,  3,  0,  5,  6, &
        1,  2,  6,  4,  5,  3,  6,  0,  1,  6,  5,  4,  2,  7,  3, &
        6,  7,  7,  5,  7,  4,  6,  7,  1,  0,  2,  7,  3,  5,  6, &
        7,  4,  7,  6,  1,  5,  2,  0,  3,  8,  8,  7,  4,  8,  8, &
        6,  5,  7,  8,  2,  8,  3,  1,  0,  4,  6,  7,  8,  5,  8, &
        7,  6,  3,  2,  9,  9,  4,  8,  1,  9,  5,  9,  0,  7,  9, &
        8,  6,  9,  3,  4,  7,  8,  2,  9,  5,  1,  6,  9,  0,  8, &
        7, 10, 10,  9, 10,  4,  5,  3, 10,  6,  8,  2, 10,  9,  7, &
       10,  1, 10,  9,  8,  0,  5,  4,  6,  3, 10,  7, 11,  9, 11, &
        2, 11,  8, 10, 11,  1, 11,  6,  9,  5, 10,  7,  4, 11,  8, &
        3, 11, 10,  9,  2, 11, 12, 12,  7,  6, 12, 10,  8,  5, 11, &
       12,  9,  1,  4, 12, 11, 12, 10,  3, 12,  7,  8,  9,  6, 11, &
       12, 13, 13, 10,  2,  5, 13, 13, 12, 11, 13,  4, 13,  9,  8, &
       12, 10,  7, 13, 11, 14, 14, 14, 13, 14,  6,  3, 12, 14, 10, &
       9,  5,  8,  2,   7,  4,  9,  6,  8,  3,  5,  7,  6,  4,  5, &
       3,  4,  4 /)

  INTEGER, PUBLIC, DIMENSION (0:nvxh2,0:njh2)                 :: lev_h2
  DATA lev_h2 / &
    1, 10, 22, 37, 55, 74, 96, 119, 145, 170, 197, 223, 247, 272, 291, &
    2, 11, 23, 38, 56, 76, 98, 122, 146, 171, 198, 225, 248, 273, 292, &
    3, 12, 25, 40, 57, 77, 99, 123, 149, 175, 200, 227, 251, 277, 293, &
    4, 13, 26, 42, 60, 80, 101, 125, 150, 177, 204, 230, 256, 278, 295, &
    5, 15, 28, 44, 62, 83, 105, 128, 154, 180, 208, 232, 260, 281, 299, &
    6, 16, 30, 46, 65, 85, 108, 132, 156, 183, 211, 239, 262, 283, 0, &
    7, 18, 32, 49, 68, 89, 112, 136, 163, 189, 213, 242, 265, 289, 0, &
    8, 20, 35, 52, 71, 91, 115, 138, 165, 193, 221, 246, 271, 294, 0, &
    9, 21, 36, 54, 75, 97, 121, 147, 173, 199, 229, 255, 279, 0, 0, &
    14, 27, 43, 61, 82, 104, 127, 153, 181, 209, 236, 261, 286, 0, 0, &
    17, 31, 48, 66, 87, 110, 135, 162, 187, 214, 243, 270, 298, 0, 0, &
    19, 34, 51, 72, 93, 116, 139, 166, 195, 224, 252, 280, 0, 0, 0, &
    24, 39, 58, 78, 100, 124, 151, 179, 206, 234, 263, 290, 0, 0, 0, &
    29, 45, 64, 86, 109, 134, 161, 186, 215, 244, 274, 0, 0, 0, 0, &
    33, 50, 70, 92, 117, 141, 167, 196, 228, 257, 287, 0, 0, 0, 0, &
    41, 59, 79, 102, 126, 152, 182, 210, 240, 268, 300, 0, 0, 0, 0, &
    47, 67, 88, 111, 137, 164, 192, 222, 253, 284, 0, 0, 0, 0, 0, &
    53, 73, 95, 120, 148, 176, 205, 237, 267, 301, 0, 0, 0, 0, 0, &
    63, 84, 107, 133, 160, 190, 219, 249, 285, 307, 0, 0, 0, 0, 0, &
    69, 94, 118, 144, 172, 202, 233, 266, 303, 0, 0, 0, 0, 0, 0, &
    81, 106, 131, 157, 185, 217, 250, 288, 309, 0, 0, 0, 0, 0, 0, &
    90, 114, 142, 168, 201, 235, 269, 305, 0, 0, 0, 0, 0, 0, 0, &
    103, 129, 155, 184, 218, 254, 296, 312, 0, 0, 0, 0, 0, 0, 0, &
    113, 140, 169, 203, 238, 276, 308, 0, 0, 0, 0, 0, 0, 0, 0, &
    130, 158, 188, 220, 259, 302, 313, 0, 0, 0, 0, 0, 0, 0, 0, &
    143, 174, 207, 241, 282, 311, 0, 0, 0, 0, 0, 0, 0, 0, 0, &
    159, 191, 226, 264, 306, 315, 0, 0, 0, 0, 0, 0, 0, 0, 0, &
    178, 212, 245, 297, 314, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, &
    194, 231, 275, 310, 317, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, &
    216, 258, 304, 316, 318, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 /

  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvbh2,0:njh2+1,nlevh2) :: aeinb         ! Prob of emission B-X
  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvch2,0:njh2+1,nlevh2) :: aeinc         ! Prob of emission C-X
  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvbh2,0:njh2+1)        :: aeitb         ! Total prob of emission B-X to bound states
  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvch2,0:njh2+1,2)      :: aeitc         ! Total prob of emission B-X to bound states
  REAL (KIND=dp), PUBLIC, DIMENSION (1:2,0:nvbh2,0:njh2)      :: tmunh2bc      ! Inverse life time of H2 B and C states

  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvxh2,0:njh2)          :: pophm1        ! Prob to form H2(v,J) on grain (physisorption)
  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvxh2,0:njh2)          :: pophm2        ! Prob to form H2(v,J) on grain (Eley-Rideal)
  REAL (KIND=dp), PUBLIC, DIMENSION (nlevh2,nlevh2)           :: cashm         ! H2 cascade coeficients
  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvbh2,0:njh2+1,nlevh2) :: bhmbx         ! H2 B-X cascade coeficients
  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvch2,0:njh2+1,nlevh2) :: bhmcx         ! H2 C-X cascade coeficients
  REAL (KIND=dp), PUBLIC                                      :: emoyh2        ! Mean energy of H2 formed on grain
  REAL (KIND=dp), PUBLIC                                      :: eform_h2
  REAL (KIND=dp), PUBLIC                                      :: ef_er_h2      ! Energy available upon E-R formation

  REAL (KIND=dp), PUBLIC, ALLOCATABLE, DIMENSION (:,:)        :: evltot        ! H2 equilibrium
  REAL (KIND=dp), PUBLIC, ALLOCATABLE, DIMENSION (:,:)        :: evcolh        ! H2 collisional equilibrium
  REAL (KIND=dp), PUBLIC, ALLOCATABLE, DIMENSION (:,:)        :: evrdbc        ! H2 radiative equilibrium (B+C)

  ! HD related dimensions
  INTEGER, PUBLIC, PARAMETER                                   :: nlevhd = 182 ! Number of HD rovibrational levels (X)
  INTEGER, PUBLIC, PARAMETER                                   :: njthd  = 10  ! Highest rotational level of HD included
  INTEGER, PUBLIC, PARAMETER                                   :: nvxhd  = 17  ! Highest vibrational level (X)
  INTEGER, PUBLIC, PARAMETER                                   :: nvbhd  = 39  ! Highest vibrational level (B)
  INTEGER, PUBLIC, PARAMETER                                   :: nvchd  = 15  ! Highest vibrational level (C)

  ! HD structure related quantities
  INTEGER, PUBLIC, DIMENSION (nlevhd)                          :: njl_hd = (/ &
        0,  1,  2,  3,  4,  5,  6,  7,  8,  0,  1,  9,  2,  3,  4, &
       10,  5,  6,  7,  8,  0,  1,  9,  2,  3,  4, 10,  5,  6,  7, &
        8,  0,  1,  9,  2,  3,  4, 10,  5,  6,  7,  8,  0,  1,  9, &
        2,  3,  4, 10,  5,  6,  7,  8,  0,  1,  2,  9,  3,  4, 10, &
        5,  6,  7,  8,  0,  1,  2,  9,  3,  4, 10,  5,  6,  7,  8, &
        0,  1,  2,  9,  3,  4, 10,  5,  6,  7,  8,  0,  1,  2,  9, &
        3,  4, 10,  5,  6,  7,  8,  0,  1,  2,  9,  3,  4, 10,  5, &
        6,  7,  8,  0,  1,  2,  9,  3,  4,  5, 10,  6,  7,  8,  0, &
        1,  2,  3,  9,  4,  5, 10,  6,  7,  8,  0,  1,  2,  3,  9, &
        4,  5, 10,  6,  7,  8,  0,  1,  2,  3,  9,  4,  5, 10,  6, &
        7,  0,  8,  1,  2,  3,  4,  9,  5, 10,  6,  7,  0,  1,  2, &
        8,  3,  4,  9,  5,  6, 10,  7,  0,  1,  2,  3,  8,  4,  5, &
        0,  1 /)

  INTEGER, PUBLIC, DIMENSION (nlevhd)                          :: nvl_hd = (/ &
        0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  0,  1,  1,  1, &
        0,  1,  1,  1,  1,  2,  2,  1,  2,  2,  2,  1,  2,  2,  2, &
        2,  3,  3,  2,  3,  3,  3,  2,  3,  3,  3,  3,  4,  4,  3, &
        4,  4,  4,  3,  4,  4,  4,  4,  5,  5,  5,  4,  5,  5,  4, &
        5,  5,  5,  5,  6,  6,  6,  5,  6,  6,  5,  6,  6,  6,  6, &
        7,  7,  7,  6,  7,  7,  6,  7,  7,  7,  7,  8,  8,  8,  7, &
        8,  8,  7,  8,  8,  8,  8,  9,  9,  9,  8,  9,  9,  8,  9, &
        9,  9,  9, 10, 10, 10,  9, 10, 10, 10,  9, 10, 10, 10, 11, &
       11, 11, 11, 10, 11, 11, 10, 11, 11, 11, 12, 12, 12, 12, 11, &
       12, 12, 11, 12, 12, 12, 13, 13, 13, 13, 12, 13, 13, 12, 13, &
       13, 14, 13, 14, 14, 14, 14, 13, 14, 13, 14, 14, 15, 15, 15, &
       14, 15, 15, 14, 15, 15, 14, 15, 16, 16, 16, 16, 15, 16, 16, &
       17, 17 /)

  INTEGER, PUBLIC, DIMENSION (0:nvxhd,0:njthd)                 :: lev_hd
  DATA lev_hd / &
    1, 10, 21, 32, 43, 54, 65, 76, 87, 98, 109, 120, 131, 142, 152, 163, 174, 181, &
    2, 11, 22, 33, 44, 55, 66, 77, 88, 99, 110, 121, 132, 143, 154, 164, 175, 182, &
    3, 13, 24, 35, 46, 56, 67, 78, 89, 100, 111, 122, 133, 144, 155, 165, 176, 0, &
    4, 14, 25, 36, 47, 58, 69, 80, 91, 102, 113, 123, 134, 145, 156, 167, 177, 0, &
    5, 15, 26, 37, 48, 59, 70, 81, 92, 103, 114, 125, 136, 147, 157, 168, 179, 0, &
    6, 17, 28, 39, 50, 61, 72, 83, 94, 105, 115, 126, 137, 148, 159, 170, 180, 0, &
    7, 18, 29, 40, 51, 62, 73, 84, 95, 106, 117, 128, 139, 150, 161, 171, 0, 0, &
    8, 19, 30, 41, 52, 63, 74, 85, 96, 107, 118, 129, 140, 151, 162, 173, 0, 0, &
    9, 20, 31, 42, 53, 64, 75, 86, 97, 108, 119, 130, 141, 153, 166, 178, 0, 0, &
    12, 23, 34, 45, 57, 68, 79, 90, 101, 112, 124, 135, 146, 158, 169, 0, 0, 0, &
    16, 27, 38, 49, 60, 71, 82, 93, 104, 116, 127, 138, 149, 160, 172, 0, 0, 0 /

  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvbhd,0:njthd+1,nlevhd) :: ahdnb          ! Prob of emission B-X
  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvchd,0:njthd+1,nlevhd) :: ahdnc          ! Prob of emission C-X
  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvbhd,0:njthd+1)        :: ahdtb          ! Total prob of emission B-X to bound states
  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvchd,0:njthd+1,2)      :: ahdtc          ! Total prob of emission B-X to bound states

  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvxhd,0:njthd)          :: pophd          ! Probability to form HD(v,J) on grain
  REAL (KIND=dp), PUBLIC, DIMENSION (nlevhd,nlevhd)            :: cashd          ! HD cascade coeficients
  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvbhd,0:njthd+1,nlevhd) :: bhdbx          ! HD B-X cascade coeficients
  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvchd,0:njthd+1,nlevhd) :: bhdcmx         ! HD (C-)-X cascade coeficients
  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvchd,0:njthd+1,nlevhd) :: bhdcpx         ! HD (C+)-X cascade coeficients
  REAL (KIND=dp), PUBLIC                                       :: emoyhd         ! Mean energy of HD formed on grain
  REAL (KIND=dp), PUBLIC                                       :: eform_hd

  REAL (KIND=dp), PUBLIC, ALLOCATABLE, DIMENSION (:,:)         :: evrdhd         ! HD radiative equilibrium (B+C)

  ! H, D, CO & HCO+ related dimensions
  INTEGER, PUBLIC, PARAMETER                                   :: nmrh  = 199    ! Number of H Lyman lines included
  INTEGER, PUBLIC, PARAMETER                                   :: nmrd  = 20     ! Number of D Lyman lines included
  INTEGER, PUBLIC, PARAMETER                                   :: nmrco = 4000   ! Max num of CO electronic lines for dissociation

  ! CO A-X related quantities
  INTEGER, PUBLIC, ALLOCATABLE, DIMENSION (:,:)                :: lev_co         ! lev to quantum numbers (X)
  INTEGER, PUBLIC, ALLOCATABLE, DIMENSION (:)                  :: nvl_co         ! v to lev (X)
  INTEGER, PUBLIC, ALLOCATABLE, DIMENSION (:)                  :: njl_co         ! J to lev (X)
  INTEGER, PUBLIC                                              :: nl_CO_A        ! Number of levels in CO A electronic state
  INTEGER, PUBLIC, ALLOCATABLE, DIMENSION (:,:,:)              :: lev_CO_A       ! lev to quantum numbers
  INTEGER, PUBLIC, ALLOCATABLE, DIMENSION (:)                  :: v_CO_A         ! v to lev
  INTEGER, PUBLIC, ALLOCATABLE, DIMENSION (:)                  :: J_CO_A         ! J to lev
  INTEGER, PUBLIC, ALLOCATABLE, DIMENSION (:)                  :: p_CO_A         ! Parity to lev
  INTEGER, PUBLIC                                              :: J_CO_max       ! Highest J in A that can be reached from X
  INTEGER, PUBLIC, ALLOCATABLE, DIMENSION (:,:)                :: lm_CO_A        ! List of A levels reached from heach X level
  REAL (KIND=dp), PUBLIC, ALLOCATABLE, DIMENSION (:,:)         :: Aij_CO_A       ! Einstein transition probability
  REAL (KIND=dp), PUBLIC, ALLOCATABLE, DIMENSION (:,:)         :: lam_CO_A       ! Wavelength in Angstrom
  REAL (KIND=dp), PUBLIC, ALLOCATABLE, DIMENSION (:,:)         :: Cij_CO_X       ! Cascade coefficient from A to X
  REAL (KIND=dp), PUBLIC, ALLOCATABLE, DIMENSION (:,:)         :: UV_exc_CO      ! Excitation matrix in X via pumping

  INTEGER, PUBLIC, PARAMETER                                   :: npmg  = 12     ! Number of points in Gaussian integration over
                                                                                 ! The grain size distrib. (instable if nmpg > 12)

  ! Used in integration of L1 (line transfer)
  REAL (KIND=dp), PUBLIC, PARAMETER                            :: aa1_L1 = 3.7429035_dp
  REAL (KIND=dp), PUBLIC, PARAMETER                            :: aa2_L1 = 0.279151314393918_dp
  REAL (KIND=dp), PUBLIC, PARAMETER                            :: x_0_L1 = -4.20884012977226_dp

  ! Gauss-Hermitte integration data for 19 points.
  INTEGER, PUBLIC, PARAMETER                                   :: ngh   = 10     ! Number of points for Gauss-Hermitte integration

  REAL (KIND=dp), PUBLIC, DIMENSION(ngh)                       :: x_gh = (/ 0.0e0_dp &
                                             , 0.503520163423888209373811765050e0_dp &
                                             , 0.101036838713431135136859873726e1_dp &
                                             , 0.152417061939353303183354859367e1_dp &
                                             , 0.204923170985061937575050838669e1_dp &
                                             , 0.259113378979454256492128084112e1_dp &
                                             , 0.315784881834760228184318034120e1_dp &
                                             , 0.376218735196402009751489394104e1_dp &
                                             , 0.442853280660377943723498532226e1_dp &
                                             , 0.522027169053748216460967142500e1_dp /)

  REAL (KIND=dp), PUBLIC, DIMENSION(ngh)                       :: w_gh = (/ &
                                               0.502974888276186530840731361096e0_dp &
                                             , 0.391608988613030244504042313621e0_dp &
                                             , 0.183632701306997074156148485766e0_dp &
                                             , 0.508103869090520673569908110358e-1_dp &
                                             , 0.798886677772299020922211491861e-2_dp &
                                             , 0.670877521407181106194696282100e-3_dp &
                                             , 0.272091977631616257711941025214e-4_dp &
                                             , 0.448824314722312295179447915594e-6_dp &
                                             , 0.216305100986355475019693077221e-8_dp &
                                             , 0.132629709449851575185289154385e-11_dp /)

  REAL (KIND=dp), PUBLIC, DIMENSION(ngh)                       :: exp2gh         ! exp2gh = EXP(-x_gh*x_gh)

  ! Pumping data
  REAL (KIND=dp), PUBLIC, PARAMETER                            :: b_integ = 0.247574639011584_dp  ! Coef to aproximate L1 FUNCTION
  REAL (KIND=dp), PUBLIC, PARAMETER                            :: c_integ = 0.0489002060066637_dp ! Coef to aproximate K1 FUNCTION

  ! Input / output quantities
  CHARACTER (LEN=8), PUBLIC                                    :: data_dir   = '../data/'
  CHARACTER (LEN=50), PUBLIC                                   :: out_dir    = '../out/'
  CHARACTER (LEN=25), PUBLIC                                   :: dustem_src = '../DUSTEM_PDR/source_f90/'
  CHARACTER (LEN=22), PUBLIC                                   :: dustem_dat = '../DUSTEM_PDR/les_DAT/'
  CHARACTER (LEN=22), PUBLIC                                   :: dustem_res = '../DUSTEM_PDR/les_RES/'

  CHARACTER (LEN=255), PUBLIC                                  :: fichier
  CHARACTER (LEN=255), PUBLIC                                  :: filedef    ! Name of the .def file
  CHARACTER (LEN=255), PUBLIC                                  :: inpfile    ! Input file (Ex: ../data/pdr6.in)
  CHARACTER (LEN=255), PUBLIC                                  :: file_bin
  CHARACTER (LEN=31),  PUBLIC                                  :: lancer_dustem = '../DUSTEM_PDR/source_f90/dustem'

! Type definitions

! RF_TYPE is used for all wavelength dependant quantities.
!         indexes go from lower to upper (the user does have to care if lower = 0 or not)
!         Values are in member Val. They must be ordered in increasing order of WaveLength
!         Ord is used to retrieve the Wave Length value from object rf_wl
!         rf_wl%Ord(i) is the rank at which rf_wl%val(i) has been entered in rf_wl
!         A specific vector (wl_pointer) is used to retrieve i from a given iord
!         Thus rf_wl may been unevenly spaced, and values entered in ony order
TYPE, PUBLIC :: RF_TYPE !-----------------------------------------------------------
  INTEGER                                  :: lower             !  Lower index     !
  INTEGER                                  :: upper             !  Upper index     !
! REAL (KIND=dp), DIMENSION (:), POINTER   :: Val               !  Values....      !
  REAL (KIND=dp), DIMENSION (:), POINTER   :: Val => NULL()     !  Values....      !
  INTEGER, DIMENSION (:), POINTER          :: Ord               !  Order           !
END TYPE RF_TYPE !------------------------------------------------------------------

!! MGG 13 March 2008: We create a new Type variable, to describe dust emmissivities.

TYPE, PUBLIC :: RF_TYPE_DUSTEM !----------------------------------------------------
  INTEGER                                  :: lower             !  Lower index     !
  INTEGER                                  :: upper             !  Upper index     !
  REAL (KIND=dp), DIMENSION (:,:), POINTER :: Val               !  Values....      !
  INTEGER, DIMENSION (:), POINTER          :: Ord               !  Order           !
END TYPE RF_TYPE_DUSTEM !-----------------------------------------------------------

TYPE, PUBLIC :: PHOTO_ESP !-----------------------------------------------------------------------------
! used for all wavelength dependant quantities.                                                        !
! This is a table containing photodestruction reactions parameters on which we based new               !
! calculation of their corresponding photodestruction rates. This new calculations                     !
! integrating the FUV/UV radiation field at each point of the code used update                         !
! photodestruction cross sections listed in files 'data/section/*.dat'.                                !
! With the PHOTO_ESP type, we succeed to stock these sections and other useful                         !
! photo-reaction parameters. In this new TYPE, we also defined a variable as a RF_TYPE                 !
! variable.                                                                                            !
                                                                                                       !
  CHARACTER (LEN=15)                       :: nom                                                      !
  CHARACTER (LEN=10)                       :: r1, r2, r3        ! Reactants in the photo-reaction      !
  CHARACTER (LEN=10)                       :: p1, p2, p3, p4    ! Products from the photo-reaction     !
  REAL (KIND=dp)                           :: wl_cut_esp        ! Specie ionisation cut off            !
  TYPE (RF_TYPE)                           :: rf_esp            ! See above for the RF_TYPE definition !
END TYPE PHOTO_ESP !------------------------------------------------------------------------------------

TYPE, PUBLIC :: ATOM !----------------------------------------------------------------------------------
  CHARACTER(len=5)                         :: name              ! Name of atom                         !
  REAL(KIND=dp)                            :: abnorm            ! Normalized abundance: X/H            !
END TYPE ATOM !-----------------------------------------------------------------------------------------

TYPE, PUBLIC :: RXNS !----------------------------------------------------------------------------------
! Reaction Specifications                                                                              !
  INTEGER                                  :: io                ! Order (1, 2 or 3)                    !
  INTEGER                                  :: it                ! Type (variable itype)                !
  REAL (KIND=dp)                           :: k                 ! Rate                                 !
  INTEGER                                  :: r1                ! React 1 index                        !
  INTEGER                                  :: r2                ! React 2 index                        !
  INTEGER                                  :: r3                ! React 3 index                        !
  INTEGER                                  :: p1                ! Product 1 index                      !
  INTEGER                                  :: p2                ! Product 2 index                      !
  INTEGER                                  :: p3                ! Product 3 index                      !
  INTEGER                                  :: p4                ! Product 4 index                      !
END TYPE RXNS !-----------------------------------------------------------------------------------------

TYPE, PUBLIC :: SPDF !----------------------------------------------------------------------------------
! Species Destruction and Formation                                                                    !
  INTEGER                                  :: nbt               ! Number of form. and destr. terms     !
  TYPE (RXNS), DIMENSION (:), POINTER      :: fdt               ! Formation and destruction terms      !
END TYPE SPDF !-----------------------------------------------------------------------------------------

! Type used for debugging purpose only

INTEGER, PUBLIC, PARAMETER                 :: nld = 30

TYPE, PUBLIC :: CHERCHE !-------------------------------------------------------------------------------
  REAL (KIND=dp), DIMENSION (nld,nld)      :: qua                                                      !
  REAL (KIND=dp), DIMENSION (nld,nld)      :: col                                                      !
  REAL (KIND=dp), DIMENSION (nld,nld)      :: cas                                                      !
  REAL (KIND=dp), DIMENSION (nld)          :: des                                                      !
  REAL (KIND=dp), DIMENSION (nld)          :: cre                                                      !
  REAL (KIND=dp), DIMENSION (nld)          :: pop                                                      !
END TYPE CHERCHE!---------------------------------------------------------------------------------------

TYPE (CHERCHE), PUBLIC                     :: eq_spe

TYPE, PUBLIC :: SPECTRA
  !--- Chemical species ------------------------------------                                                                  !
  CHARACTER (LEN=8)                                :: nam    ! Species name (as used in chemistry file)                       !
  REAL (KIND=dp)                                   :: mom    ! Molecular mass                                                 !
  INTEGER                                          :: ind    ! Index in species list                                          !
  !--- Levels-----------------------------------------------                                                                  !
  INTEGER                                          :: nlv    ! Number of levels in data file                                  !
  INTEGER                                          :: use    ! Number of levels used in the simulation                        !
  INTEGER                                          :: usemin ! Minimum number of levels computed                              !
  INTEGER                                          :: nqn    ! Number of quantum number to define a level                     !
  CHARACTER(len=6),  DIMENSION(:),   POINTER       :: qnam   ! Name of the quantum numbers (1:nqn)                            !
  CHARACTER(len=4),  DIMENSION(:,:), POINTER       :: quant  ! Quantum numbers values                     (1:nlv,1:nqn)       !
  REAL (KIND=dp),    DIMENSION(:),   POINTER       :: gst    ! Statistical weight                         (1:nlv)             !
  REAL (KIND=dp),    DIMENSION(:),   POINTER       :: elk    ! Energy in Kelvin                           (1:nlv)             !
  !--- Transitions------------------------------------------                                                                  !
  INTEGER                                          :: ntr    ! Number of transitions in data file                             !
  CHARACTER(len=100),DIMENSION(:),   POINTER       :: inftr  ! Informations on the transition             (1:ntr)             !
  INTEGER,           DIMENSION(:),   POINTER       :: iup    ! Upper level                                (1:ntr)             !
  INTEGER,           DIMENSION(:),   POINTER       :: jlo    ! Lower level                                (1:ntr)             !
  REAL (KIND=dp),    DIMENSION(:),   POINTER       :: lac    ! Central wave length                        (1:ntr)             !
  REAL (KIND=dp),    DIMENSION(:),   POINTER       :: wlk    ! Wavelength (Kelvin)                        (1:ntr)             !
  REAL (KIND=dp),    DIMENSION(:),   POINTER       :: aij    ! Einstein emission coefficient              (1:ntr)             !
  !--- Abundance--------------------------------------------                                                                  !
  REAL (KIND=dp),    DIMENSION(:),   POINTER       :: abu    ! Abundance                                  (1:npo)             !
  REAL (KIND=dp),    DIMENSION(:,:), POINTER       :: xre_o  ! Relative abundance of level at prev iteration (1:nlv,0:npo)    !
  REAL (KIND=dp),    DIMENSION(:,:), POINTER       :: xre    ! Relative abundance of level                (1:nlv,0:npo)       !
  !--- Emission---------------------------------------------                                                                  !
  REAL (KIND=dp),    DIMENSION(:,:), POINTER       :: emi    ! Line local emissivity                      (1:ntr,0:npo)       !
  REAL (KIND=dp),    DIMENSION(:),   POINTER       :: ref    ! Cooling rate                               (0:npo)             !
  REAL (KIND=dp),    DIMENSION(:),   POINTER       :: vel    ! Thermal velocity (including turb)          (0:npo)             !
  !---  Other quantities------------------------------------                                                                  !
  INTEGER,           DIMENSION(:),   POINTER       :: hot    ! Flag: 1 if pumping efficient, 0 if not     (1:ntr)             !
  REAL (KIND=dp),    DIMENSION(:,:), POINTER       :: siD    ! Dust extinction cross sect. at wlk over ext at V (1:ntr,0:npo) !
  REAL (KIND=dp),    DIMENSION(:,:), POINTER       :: odl    ! Optical depth from tau_v = 0               (1:ntr,0:npo)       !
  REAL (KIND=dp),    DIMENSION(:,:), POINTER       :: odr    ! Optical depth from tau_v = tau_max         (1:ntr,0:npo)       !
  REAL (KIND=dp),    DIMENSION(:,:), POINTER       :: XXl    ! Integrated relative abundance (xre * abu / (v_T*gst)) (left)   !
  REAL (KIND=dp),    DIMENSION(:,:), POINTER       :: XXr    ! Integrated relative abundance (xre * abu / (v_T*gst)) (right)  !
  REAL (KIND=dp),    DIMENSION(:,:), POINTER       :: CoD    ! Weigthed Column Density                                        !
  REAL (KIND=dp),    DIMENSION(:),   POINTER       :: exl    ! external radiation (left)                  (1:ntr)             !
  REAL (KIND=dp),    DIMENSION(:),   POINTER       :: exr    ! external radiation (right)                 (1:ntr)             !
  REAL (KIND=dp),    DIMENSION(:,:), POINTER       :: dnu    ! Doppler width                              (1:ntr,0:npo)       !
  REAL (KIND=dp),    DIMENSION(:,:), POINTER       :: pum    ! Pumping radiation at wlk                   (1:ntr,0:npo)       !
  REAL (KIND=dp),    DIMENSION(:,:), POINTER       :: jiD    ! JinD - mean intensity from dust radiation  (1:ntr,0:npo)       !
  REAL (KIND=dp),    DIMENSION(:,:), POINTER       :: bel    ! Escape probability (left)                  (1:ntr,0:npo)       !
  REAL (KIND=dp),    DIMENSION(:,:), POINTER       :: ber    ! Escape probability (right)                 (1:ntr,0:npo)       !
  !--- Verification-----------------------------------------                                                                  !
  INTEGER                                          :: verif  ! Flag, If 1 produce an output file to check data                !
END TYPE SPECTRA

! Dust radiative properties type - JLB - 22 IV 2010
!      Each coefficient is a function of wavelength and position

TYPE, PUBLIC :: DU_DIST !-----------------------------------------------------------------------------------------------------!
  REAL (KIND=dp),    DIMENSION(:,:), ALLOCATABLE   :: abso   ! Absorption coefficient                    (0:nwlg,0:npo)       !
  REAL (KIND=dp),    DIMENSION(:,:), ALLOCATABLE   :: scat   ! Scatering coefficient                     (0:nwlg,0:npo)       !
  REAL (KIND=dp),    DIMENSION(:,:), ALLOCATABLE   :: emis   ! Emission coefficient                      (0:nwlg,0:npo)       !
  REAL (KIND=dp),    DIMENSION(:,:), ALLOCATABLE   :: gcos   ! Anisotropy factor (g = <cos theta>)       (0:nwlg,0:npo)       !
  REAL (KIND=dp),    DIMENSION(:,:), ALLOCATABLE   :: albe   ! Albedo                                    (0:nwlg,0:npo)       !
END TYPE DU_DIST !------------------------------------------------------------------------------------------------------------!

! Used to manipulate the exponential of the negative of large numbers (used in radiative transfer)
!      The number "arg" is split as : mag * grand + rm
!      Then: cmp = exp(-rm) and val = cmp * exp(-mag*grand)
 REAL    (KIND=dp),  PUBLIC                         :: grand = 100.0_dp
 INTEGER (KIND=ilk), PUBLIC, PARAMETER              :: mgr = 8
 REAL    (KIND=dp),  PUBLIC, DIMENSION (0:mgr)      :: expgr

! Constantes introduced by Franck for XML and FITS output

!   Created by Franck on 06/12/08.
!   Copyright 2008 LUTH - Observatoire de Paris. All rights reserved.
!
!=======================================================================================================================
! DECLARATION OF SOME COMMON PARAMETERS
! Note : Some of them should be determined during the execution
!=======================================================================================================================

INTEGER, PARAMETER, PUBLIC :: lenfilename = 85   ! Max size of the name of a file
INTEGER, PARAMETER, PUBLIC :: lenformat   = 16   ! Size string : format for .fits

INTEGER, PARAMETER, PUBLIC :: lenname     = 50   ! Size of a string : name
INTEGER, PARAMETER, PUBLIC :: lenID       = 70   ! Size of a string : ID
INTEGER, PARAMETER, PUBLIC :: lenIDLEV    = 15   ! Size of a string to code quantum numbers identifications
INTEGER, PARAMETER, PUBLIC :: lenmode     = 10   ! Size of a string : value
INTEGER, PARAMETER, PUBLIC :: lendescrip  = 400  ! Size of a string : DESCRIPTION
INTEGER, PARAMETER, PUBLIC :: lenstring   = 550  ! Max size of an XML line
INTEGER, PARAMETER, PUBLIC :: lendatID    = 3    !
INTEGER, PARAMETER, PUBLIC :: lendatatype = 16   ! Size string : format for .XML
INTEGER, PARAMETER, PUBLIC :: lenunit     = 30   ! Size of a string : unit
INTEGER, PARAMETER, PUBLIC :: lenucd      = 150  ! Size of a string : UCD
INTEGER, PARAMETER, PUBLIC :: lenutype    = 40   ! Size of a string : utype
INTEGER, PARAMETER, PUBLIC :: lenencoding = 7    ! Size of a string : encoding in a STREAM line
INTEGER, PARAMETER, PUBLIC :: lenpath     = 300  ! Size of a string : path towards a file
INTEGER, PARAMETER, PUBLIC :: lenextnum   = 10   ! Size of a string : integer corresponding to extnum
INTEGER, PUBLIC, PARAMETER :: lenstrint   = 12   ! Max length of a string from an integer casted to string by INT_STR

INTEGER, PARAMETER, PUBLIC :: lennamespecy = 19  ! Size of the string for the name of a chemical specy
                                                 ! ATTENTION: DANS modreadbin c'est 8 -> valeur du code PDR
                                                 ! MAIS pour les ID il faut monter plus haut car on doit rajouter
                                                 ! des caracteres pour differencier les especes sur les differentes
                                                 ! tailles de grains.

INTEGER, PARAMETER, PUBLIC :: naxmax = 5          ! Nombre max d'axes du code de simulation                                       !

INTEGER, PARAMETER, PUBLIC :: nparam_mod     = 30 ! nbre de parametres du modele                                                  !
INTEGER, PARAMETER, PUBLIC :: nparam_mod_r   = 66 ! nbre de parametres du modele qui sont des reels                               !
INTEGER, PARAMETER, PUBLIC :: nparam_mod_i   = 12 ! nbre de parametres du modele qui sont des entier                              !
INTEGER, PARAMETER, PUBLIC :: nparam_mod_s   =  7 ! nbre de parametres du modele qui sont des strings                             !
                                                                                                                                  !
INTEGER, PARAMETER, PUBLIC :: nconst_tab     = 15 ! nbre de constantes definissant des tailles de tableau que l'on veut dans output!
INTEGER, PARAMETER, PUBLIC :: nspec_ex       = 20 ! Nombres d'especes pour lesquelles on calcule l'excitation                     !
INTEGER, PARAMETER, PUBLIC :: nconst_tab_chi = 21 ! nbre de types de reactions                                                    !
INTEGER, PARAMETER, PUBLIC :: nreact_special =  4 ! nbre de reactions speciales (iadhgr, ifh2gr, iaddgr, ifhdgr)                  !
INTEGER, PARAMETER, PUBLIC :: nspeci_special = 40 ! nbre d'especes speciales (Ex: i_h, i_co, i_hgr, ...)                          !
INTEGER, PARAMETER, PUBLIC :: nprop_spe      =  6 ! nombre de categories pour les especes (neutre, cation, anion, sur grains)     !
INTEGER, PARAMETER, PUBLIC :: ncol_chim      = 13 ! 7 reactants plus produits + gamm, alpha, beta, de et itype                    !
INTEGER, PARAMETER, PUBLIC :: ncol_struct    =  5 ! Tgaz, nH, ionization, pressure, n                                             !
INTEGER, PARAMETER, PUBLIC :: ncol_heat      =  9 ! Heating processes                                                             !
INTEGER, PARAMETER, PUBLIC :: ncol_cool      = 20 ! Cooling processes                                                             !
INTEGER, PARAMETER, PUBLIC :: ncol_mixt      =  4 ! Processus mixtes : H2 et couplage gaz-grains: 2 cas pour chacun soit 4 col.   !
INTEGER, PARAMETER, PUBLIC :: ncol_sperat    =  9 ! Taux de reactions speciaux                                                    !

! USED BY PXDR_UTIL_XML AND PXDR_UTIL_FITS

TYPE colonne !---------------------------------------------------------------------                                               !
    character(len=lenname)    :: titre     ! titre de la colonne (plus utilise)   !                                               !
    character(len=lenID)      :: IDFD      ! ID de la colonne ()                  !                                               !
    character(len=lenformat)  :: form      ! format de la colonne (pour fits)     !  Exemple : 1J, 8A, ...                        !
    character(len=lendatatype):: datatyp   ! format pour xml                      !  Exemple : double, string                     !
    character(len=lenunit)    :: unite     ! unite des quantites dans la colonne  !                                               !
    character(len=lenucd)     :: ucd       ! ucd                                  !                                               !
    character(len=lenutype)   :: utype     ! utype                                !                                               !
    integer                   :: flag_desc ! 1 si description associee            !                                               !
    character(len=lendescrip) :: descrip   ! description                          !                                               !
END TYPE colonne !-----------------------------------------------------------------                                               !
TYPE(colonne),      PUBLIC, ALLOCATABLE :: descol(:)                                                                              !
REAL(KIND=DP),      PUBLIC, ALLOCATABLE :: rval(:)                                                                                !
REAL(KIND=DP),      PUBLIC, ALLOCATABLE :: rval2D(:,:)                                                                            !
CHARACTER(len=100), PUBLIC, ALLOCATABLE :: sval(:)                                                                                !
CHARACTER(len=100), PUBLIC, ALLOCATABLE :: sval2D(:,:)                                                                            !
INTEGER,            PUBLIC, ALLOCATABLE :: ival(:)                                                                                !
INTEGER,            PUBLIC, ALLOCATABLE :: ival2D(:,:)                                                                            !

END MODULE PXDR_CONSTANTES

