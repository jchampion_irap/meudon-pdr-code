
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

!%%%%%%%%%%%
PROGRAM PXDR
!%%%%%%%%%%%

  USE PXDR_CONSTANTES
  USE PXDR_CHEM_DATA
  USE PXDR_PROFIL
  USE PXDR_AUXILIAR
  USE PXDR_STR_DATA
  USE PXDR_RF_TOOLS
  USE PXDR_TRANSFER
  USE PXDR_INITIAL
  USE PXDR_INIFITS
  USE PXDR_OUTPUT
  USE PXDR_OUTFITS
  USE PXDR_FGK_TR
  USE PXDR_CHEMISTRY
  USE PXDR_BILTHERM
  USE PXDR_DUSTEM

  IMPLICIT NONE

  INTEGER                                    :: iargc, iopt, itenua
  INTEGER                                    :: i, ii, j, lev, nsplit
  REAL (KIND=dp)                             :: vartg, testv
  REAL (KIND=dp)                             :: xdens = 0.0_dp
  REAL (KIND=dp)                             :: xtemp = 0.0_dp
  REAL (KIND=dp)                             :: vart, varn, varh
  REAL (KIND=dp)                             :: varhd, varh2, varco, varc

!--------+---------+---------+---------+---------+---------+---------+-*-------+

  version_code = 'PDR_1.4.4_rev_870'

  PRINT *,'********************************************************'
  PRINT *,'*                                                      *'
  PRINT *,'*                *****    ****     *****               *'
  PRINT *,'*                *   *    *   *    *   *               *'
  PRINT *,'*                *****    *   *    *****               *'
  PRINT *,'*                *        *   *    * *                 *'
  PRINT *,'*                *        ***      *  *                *'
  PRINT *,'*                                                      *'
  PRINT *,'*          Version : ',TRIM(version_code),'            *'
  PRINT *,'********************************************************'

!------------------------------------------------------------------------------------
!-- PART 1 : INITIALISATION
!------------------------------------------------------------------------------------

  IF (iargc() .ne. 1) THEN
     inpfile="../data/pdr6.in"
  ELSE
     CALL getarg(1,inpfile)
  ENDIF

  CALL INIT1

  !-- Promoted from INIT1: Initialise radiation field and related quantities
  PRINT *,"--- Initialization of radiation field ---"
  CALL RF_INIT

  CALL COMP_DIST

  !-- Initialise a few extra quantities (d_cm requires a radiation field)
  CALL CALC_EXTRA

  WRITE(iscrn,*)
  WRITE(iscrn,*) "================================================"
  WRITE(iscrn,*) "              END OF INPUT FILE                 "
  WRITE(iscrn,*) "================================================"
  WRITE(iscrn,*)

!------------------------------------------------------------------------------------
!-- PART 2 : ITERATIONS
!--
!-- 1 - GLOBAL LOOP on the whole cloud (ifaf)
!-- 2 - LOOPS on each positions (iopt)
!-- 3 - LOCAL LOOPS for convergervence of physical processes (miter)
!------------------------------------------------------------------------------------

  !-- UV RADIATIVE TRANSFER -----------------------------------------
  WRITE (iscrn,*) " ================================================ "
  WRITE (iscrn,*) "           Solve Radiative Transfer               "
  WRITE (iscrn,*) " ================================================ "

  PRINT *, " Enter DUST_PROPERTIES - initial"
  CALL DUST_PROPERTIES
  IF (F_dustem == 1) THEN
     OPEN (iwrtmp, FILE="Cmp_Td.out", STATUS="unknown")
     DO iopt = 0, npo
        WRITE (iwrtmp,*) tau_o(iopt)*tautav, (tdust_o(i,iopt), i = 1, npg) &
                       , (tdust_dus_o(i,iopt), i = 1, npg)
     ENDDO
     CLOSE (iwrtmp)
  ENDIF

  i = SIZE(tau_o)
  IF (ALLOCATED(tmptau)) DEALLOCATE(tmptau)
  ALLOCATE(tmptau(0:i-1))
  tmptau = tau_o

  IF (isoneside == 1) THEN
     CALL ETIN901S (nwlg)
  ELSE
     CALL ETIN90 (nwlg)
  ENDIF

  ! FEED_LINES moved after grain initialisation is complete
  DO i = 1, nspl
     CALL FEED_LINES(spec_lin(i))
  ENDDO
  CALL CDT_WRAP

  PRINT *, "Global iterations"
  PRINT *, "   Final   : ", ifafm
  !-------------------------------------------------------------------------------
  !-- GLOBAL LOOP
  !-------------------------------------------------------------------------------
  DO ifaf = 1, ifafm

     ! This file is used for debug only
     ! fichier = TRIM(ADJUSTL(modele))//"_eq_"//TRIM(ADJUSTL(ii2c(ifaf)))//".dat"
     ! OPEN (56, file=fichier, status="unknown")

     iopt_o = 0
     tau(0) = 0.0_dp
     dtau(0) = 0.0_dp

     !-------------------------------------------------------------------------------
     !-- LOOP ON POSITION IN THE CLOUD (iopt)
     !-------------------------------------------------------------------------------
     DO iopt = 0, noptm
402     CONTINUE

        WRITE(iscrn,*)
        WRITE(iscrn,*) '*********************************************'
        WRITE(iscrn,'(5x,"Two sides iteration number",i2," of",i4)') ifaf, ifafm
        WRITE(iscrn,'(15x,"Slab number: ",i4)') iopt
        WRITE(iscrn,*) '*********************************************'

        temp = tgaz(iopt)
        av   = tau(iopt) * tautav

        ! Re-initialization of abundances
        ! cosmic rays, photons and so have an abundance of 1 (numerical trick)
        ab = 1.0_dp
        DO i = 1, n_var
           ab(i) = abnua(i,iopt)
        ENDDO

        !-----------------------------------------------------------------------
        !-- LOCAL LOOP FOR CONVERGENCE OF PHYSICAL PROCESSES (index : itenua)
        !-- Compute iteratively all physical quantities at a given point
        !-- 1 - Radiative transfer
        !-- 2 - Chemistry
        !-- 3 - Thermal balance
        !-- 4 - Various tests
        !-----------------------------------------------------------------------
        DO itenua = 1, miter

           WRITE(iscrn,*)
           WRITE(iscrn,*)'-------------------------------------------------'
           WRITE(iscrn,1030) ifaf,iopt, itenua, av, densh(iopt), temp

           ! Previous values are saved to tests
           IF (i_h  /= 0) oldhat = ab(i_h)
           IF (i_h2 /= 0) oldhmo = ab(i_h2)
           IF (i_hd /= 0) oldhd  = ab(i_hd)
           IF (i_co /= 0) oldco  = ab(i_co)
           IF (i_c  /= 0) oldc   = ab(i_c)

           !------------------------------------------------------------------------
           !--  1 - RADIATIVE TRANSFER (FGK approximation above jfgk for H2)
           !------------------------------------------------------------------------
           CALL FGKTR2 (iopt)

           ! absorption and dissociation of H2, CO and isotopes
           ! ionisation of C, S etc... by direct integration over the radiation field
           CALL FGKDAT (iopt)

           ! Write some photodissociation probabilities on the screen during execution
           WRITE(iscrn,*) 'Photodissociation probabilities [s-1] :'
           IF (i_hd /= 0) THEN
              IF (i_h2 /= 0) WRITE(iscrn,"(' P(H2): ',1p,e10.3)",ADVANCE="NO") pdiesp(kdeph0,iopt)
              IF (i_hd /= 0) WRITE(iscrn,"(' P(HD): ',1p,e10.3)",ADVANCE="NO") pdiesp(kdeph0+1,iopt)
              IF (i_co /= 0) WRITE(iscrn,"(' P(CO): ',1p,e10.3)",ADVANCE="NO") pdiesp(kdeph0+2,iopt)
           ELSE
              IF (i_h2 /= 0) WRITE(iscrn,"(' P(H2): ',1p,e10.3)",ADVANCE="NO") pdiesp(kdeph0,iopt)
              IF (i_co /= 0) WRITE(iscrn,"(' P(CO): ',1p,e10.3)",ADVANCE="NO") pdiesp(kdeph0+1,iopt)
           ENDIF
           WRITE(iscrn,*)

           !------------------------------------------------------------------------
           !--  2 - CHEMISTRY
           !------------------------------------------------------------------------

           ! Build the set of chemical equations
           CALL CHIMIJ (temp, iopt, itenua)

           ! Compute abelec at the end of CHIMIJ (19 XI 11)

           ! Solve the set of chemical equations (newton-raphson scheme)
           ! Inspired from (with home made tuning)
           ! numerical recipes, WH Press,BP Flannery,SA Teukolsky and WT Vetterling,
           ! Cambridge University Press, 1986.
           CALL MNEWT (iopt)

           ! Write some abundances on the screen during execution
           WRITE (iscrn,'("Abundances [cm-3] :")')
           DO i = 1, natom-1
              IF (icx(i,1) /= 0) THEN
                 WRITE (iscrn,"(2x,a8,':',1pe12.4,2x,a8,':',e12.4)") &
                        speci(icx(i,1)), ab(icx(i,1)), speci(icx(i,2)), ab(icx(i,2))
              ENDIF
           ENDDO

           IF (i_h    /= 0) WRITE(iscrn,'(" H   : ",1p,e12.3," cm-3")') ab(i_h)
           IF (i_hgrc /= 0) WRITE(iscrn,'(" H:: : ",1p,e12.3," cm-3")') ab(i_hgrc)
           IF (i_h2   /= 0) WRITE(iscrn,'(" H2  : ",1p,e12.3," cm-3")') ab(i_h2)
           IF (i_hd   /= 0) WRITE(iscrn,'(" HD  : ",1p,e12.3," cm-3")') ab(i_hd)
           IF (i_co   /= 0) WRITE(iscrn,'(" CO  : ",1p,e12.3," cm-3")') ab(i_co)
           IF (i_h2o  /= 0) WRITE(iscrn,'(" H2O : ",1p,e12.3," cm-3")') ab(i_h2o)

           WRITE (iscrn,'(" errx    : ", 1p,e12.2)') errx
           WRITE (iscrn,'(" mauvais : ", a8)') speci(mauvai)
           WRITE (iscrn,*) speci(nspec+1), ab(nspec+1) , speci(nspec+2), ab(nspec+2)
           WRITE (iscrn,*) " Grain charge:",  grcharge(iopt), ", Electron density:", abelec

           ! Save abundances for next iteration
           abnua(0,iopt) = 1.0_dp
           DO i = 1, n_var
              abnua(i,iopt) = ab(i)
           ENDDO

           ! Update abundances in line specific variable at first iteration
           IF (ifaf == 1) THEN
              DO i = 1, nspl
                 spec_lin(i)%abu = ab(spec_lin(i)%ind)
              ENDDO
              IF (i_c13o == 0 .AND. j_c13o /= 0 .AND. i_co /= 0) THEN
                 spec_lin(in_sp(j_c13o))%abu = ab(i_co) / r_c13
              ENDIF
              IF (i_co18 == 0 .AND. j_co18 /= 0 .AND. i_co /= 0) THEN
                 spec_lin(in_sp(j_co18))%abu = ab(i_co) / r_o18
              ENDIF
              IF (i_c13o18 == 0 .AND. j_c13o18 /= 0 .AND. i_co /= 0) THEN
                 spec_lin(in_sp(j_c13o18))%abu = ab(i_co) / (r_c13 * r_o18)
              ENDIF
           ENDIF

           !------------------------------------------------------------------------
           !--  3 - THERMAL BALANCE
           !--
           !--  Compute excitation of H2, CO, CS, C, C+, O, HCO+ and others...
           !--  Then heating and cooling rates.
           !--  Thermal balance only if asked for (FROID and CHAUD are used in BILTHM)
           !------------------------------------------------------------------------

           IF (ifeqth == 1 .AND. ifaf >= 2) THEN
              CALL BILTHM (iopt, itenua)
           ELSE
              CALL FROID (temp, iopt)
              CALL CHAUD (temp, iopt, itenua)
           ENDIF

           WRITE(iscrn,*) ' '
           WRITE(iscrn,"('Cooling and Heating rates:  ',1p,E12.6,3x, 1p,E12.6,&
                      &' [erg cm-3 s-1]')") reftot(iopt), chotot(iopt)
           !IF (i_c /= 0) THEN
           !   WRITE(iscrn,*) "C:", xrecat(1,iopt), xrecat(2,iopt), xrecat(3,iopt)
           !ENDIF
           !IF (i_h2o /= 0) THEN
           !   WRITE(iscrn,*) "H2O:", xreh2o(4,iopt) , refh2o(iopt)
           !ENDIF
           WRITE(iscrn,*)

           !------------------------------------------------------------------------
           !--  4 - TESTS
           !--    - Chemical solution accuracy
           !--    - relative abundance of H, H2, C, CO (and maybe some other)
           !--    - H2 population conservation
           !--      ===> Leave loop on itenua if ALL conditions are met
           !--    A Warning on screen gives the reason why another iteration is done
           !------------------------------------------------------------------------
           IF (itenua < 4) CYCLE   ! At least 4 iterations !

           WRITE(iscrn,*)
           WRITE(iscrn,*) '------------------------------------'
           WRITE(iscrn,*) '        convergence tests           '

           ! JLB 19 IV 2010 - try more severe conditions for thermal balance
           IF (ifeqth == 1) THEN
              vartg = ABS(tb(itenua) - tb(itenua-1))
              IF (      (ABS(bthrel) > (1.0_dp * convli)) &
                  .AND. (vartg > 0.02d0)                   &
                  .AND. (temp > 4.0_dp)) THEN
                 PRINT *, "   CASE 1: BT", bthrel, vartg
                 CYCLE
              ENDIF
           ENDIF

           IF ( (ifeqth == 1) .AND. (ABS(bthrel) > 1.0e-3_dp) .AND. (temp > 4.0_dp)) THEN
              PRINT *, "   CASE 2: bthrel", bthrel
              CYCLE
           ENDIF

           IF (errx > 1.0e-2_dp) THEN
              PRINT *, "   CASE 3: chemistry", errx
              CYCLE
           ENDIF

           testv = ABS(oldhat - abnua(i_h,iopt)) / abnua(i_h,iopt)
           !  JLB 29 IV 2008 : * 0.2
           !  JLB  4 IX 2008 : * 0.1
           testv = testv * 0.1_dp
           IF (testv > convli) THEN
              PRINT *, "   CASE 4: H", testv
              CYCLE
           ENDIF

           testv = ABS(oldhmo - abnua(i_h2,iopt)) / abnua(i_h2,iopt)
           IF (testv > convli) THEN
              PRINT *, "   CASE 5: H2", testv
              CYCLE
           ENDIF

           IF (i_hd /= 0) THEN
              testv = ABS(oldhd - abnua(i_hd,iopt)) / abnua(i_hd,iopt)
              IF (testv > convli) THEN
                 PRINT *, "   CASE 5b: HD", testv
                 CYCLE
              ENDIF
           ENDIF

           IF (i_co /= 0) THEN
              testv = ABS(oldco - abnua(i_co,iopt)) / abnua(i_co,iopt)
              IF (testv > convli) THEN
                 PRINT *, "   CASE 6: CO", testv
                 CYCLE
              ENDIF
           ENDIF

           IF (i_c /= 0) THEN
              testv = ABS(oldc - abnua(i_c,iopt)) / abnua(i_c,iopt)
              IF (testv > convli) THEN
                 PRINT *, "   CASE 7: C", testv
                 CYCLE
              ENDIF
           ENDIF

           EXIT

              !--------------------------------------------------------------------
        ENDDO !-- END OF LOOP ON LOCAL CONVERGENCE OF PHYSICAL PROCESSES
              !--------------------------------------------------------------------

        PRINT *, "  O/P:", h2para, h2orth, h2orth/h2para
        WRITE (iwrit2,"(8x,i4,2x,1pe17.10,2x,e17.10,3x,e13.6,6x, 0pf13.6,5x,i3)") &
                       iopt, tau(iopt), av, densh(iopt), temp, itenua

        !-- Save new temperature ---------------------------------------
        tgaz(iopt) = temp

        ! oct and dec 91: variable step size
        ! Dec 2006 : change slightly algo to get a smoother grid.

        IF (iopt == 0) THEN
           ! No comparison possible.
           ! If max iteration reached, redo the first point
           ! Else, go to next point.
           IF (itenua >= miter) THEN
              GOTO 402
           ENDIF

           ! FDENS CONTAINS various state laws and prescriptions
           ! controled by "ifisob" and "ifeqth"

           tau(1) = tau_o(1)
           dtau(1) = dtau_o(1)
           iopt_o = 1
           CALL FDENS(tau(1), iopt, xdens, xtemp)
           tgaz(1) = xtemp
           densh(1) = xdens

        ELSE
           ! Compute relative variation between two points (thevar)
           vart = ABS((tgaz(iopt) - tgaz(iopt-1)) / tgaz(iopt))
           varn = ABS((densh(iopt) - densh(iopt-1)) / densh(iopt))
           varh = ABS(abnua(i_h,iopt-1) - abnua(i_h,iopt)) / abnua(i_h,iopt)
           ! 29 IV 2008 - just for a test
           varh = 0.2_dp * varh
           varh2 = ABS(abnua(i_h2,iopt-1) - abnua(i_h2,iopt)) / abnua(i_h2,iopt)
           IF (i_hd /= 0) THEN
              varhd = ABS(abnua(i_hd,iopt-1) - abnua(i_hd,iopt)) / abnua(i_hd,iopt)
           ELSE
              varhd = 0.0_dp
           ENDIF
           IF (i_co /= 0) THEN
              varco = ABS(abnua(i_co,iopt-1) - abnua(i_co,iopt)) / abnua(i_co,iopt)
           ELSE
              varco = 0.0_dp
           ENDIF
           IF (i_c /= 0) THEN
              varc = ABS(abnua(i_c,iopt-1) - abnua(i_c,iopt)) / abnua(i_c,iopt)
           ELSE
              varc = 0.0_dp
           ENDIF
           thevar = vart + varn + varh + varhd + varh2 + varco + varc
           ! 6 II 2007
           IF (i_h2o /= 0 .AND. ALLOCATED(xreh2o)) THEN
              IF (ab(i_h2o) > densh(iopt) * 1.0e-20_dp) THEN
                 thevar = thevar + ABS(xreh2o(3,iopt-1) - xreh2o(3,iopt)) / xreh2o(3,iopt)
              ENDIF
           ENDIF

           testv = dtau(iopt) / MIN(tau(iopt),MAX(tau(1),taumax-tau(iopt)))

           IF ((thevar >= varmax .OR. itenua >= miter) &
           .AND. (testv >= dtaumin) &
           .AND. (ifisob /=2 .OR. ifaf /= 1 .OR. itenua > 2)) THEN
              ! If "thevar" is too large, or max number of iterations has been reached,
              ! the point is discarded. We need a new point, closer to the previous one.
              dtau(iopt) = dtau(iopt) * 0.5_dp
              tau(iopt) = tau(iopt-1) + dtau(iopt)
              CALL FDENS(tau(iopt), iopt, xdens, xtemp)
              tgaz(iopt) = xtemp
              densh(iopt) = xdens

              ! Reset abundances (use IF/THEN/ELSE to enforce HIP or LIP solutions (if they exist...)
              ! Rescaling only for full species. not for grain surface variables

              DO ii = 1, nspec
!                IF (tau(iopt) > 1.0e-1_dp) THEN
!                   abnua(ii,iopt) = abin(ii) * densh(iopt)
!                ELSE
                    abnua(ii,iopt) = abnua(ii,iopt-1) * densh(iopt) / densh(iopt-1)
!                ENDIF
              ENDDO
              GOTO 402
           ELSE
              ! If end of cloud, ...finished!
              IF (tau(iopt) >= taumax) GOTO 403

              ! Else, we accept the point and step to a further one.
              IF (tau_o(iopt_o) <= tau(iopt)) THEN
                 iopt_o = iopt_o + 1
              ENDIF
              nsplit = floor((tau_o(iopt_o) - tau(iopt))*400.0_dp/taumax)
              IF (nsplit < 2) THEN
                 dtau(iopt+1) = tau_o(iopt_o) - tau(iopt)
                 IF (dtau(iopt+1) /= dtau_o(iopt_o)) THEN
                    PRINT *, iopt, dtau(iopt+1), dtau_o(iopt_o)
                 ENDIF
                 tau(iopt+1) = tau_o(iopt_o)
              ELSE
                 dtau(iopt+1) = MIN((tau_o(iopt_o) - tau(iopt)) / nsplit, 0.2_dp*taulm/taumax)
                 tau(iopt+1) = tau(iopt) + dtau(iopt+1)
              ENDIF
              CALL FDENS(tau(iopt+1), iopt, xdens, xtemp)
              tgaz(iopt+1) = xtemp
              densh(iopt+1) = xdens
           ENDIF

        ENDIF ! END OF IF (iopt == 0)

        !----------------------------------------------------------------------------------
        ! For debug purpose only
        ! IF (MOD(iopt,10) == 0) THEN
        !    WRITE (56,'("#",i5,1p,4e18.8)') iopt, tau(iopt)*tautav, ab(i_co) &
        !                                  , ab(i_co)*SUM(eq_spe%des*eq_spe%pop), SUM(eq_spe%cre)
        !    DO ii = 1, nld
        !       WRITE (56,'(1p,3(30e15.7,"  "),3(e15.7,"  "))') (eq_spe%qua(ii,j), j=1,nld) &
        !                , (eq_spe%col(ii,j), j=1,nld), (eq_spe%cas(ii,j), j=1,nld) &
        !                , eq_spe%des(ii), eq_spe%cre(ii), eq_spe%pop(ii)
        !    ENDDO
        !    WRITE (56,'(" ")')
        ! ENDIF
        !----------------------------------------------------------------------------------

        !-----------------------------------------------------------------
        !-- Initial conditions at next point
        !-- - Radiation field
        !-- - Chemical abundances
        !-- - Excited levels
        !-----------------------------------------------------------------

        PRINT *,"Preparation of quantities for next point"
        !-- Grains temperature -------------------------------------------
        tdust(:,iopt+1) = tdust(:,iopt)

        !-- Abundances ---------------------------------------------------
        DO j = 1, nspec
           abnua(j,iopt+1) = abnua(j,iopt) * densh(iopt+1) / densh(iopt)
        ENDDO
        DO j = nspec+1, n_var
           abnua(j,iopt+1) = abnua(j,iopt)
        ENDDO

        !-- H2 levels ----------------------------------------------------
        DO lev = 1, l0h2
           xreh2(lev,iopt+1) = xreh2(lev,iopt)
        ENDDO

        !-- HD levels ----------------------------------------------------
        IF (i_hd /= 0) THEN
           DO lev = 1, l0hd
              xrehd(lev,iopt+1) = xrehd(lev,iopt)
           ENDDO
        ENDIF

        !-- CO (and isotopes) levels -------------------------------------
        IF (i_co /= 0) THEN
           DO j = 1, spec_lin(in_sp(i_co))%nlv
              xreco(j,iopt+1) = xreco(j,iopt)
           ENDDO
        ENDIF
        IF (i_c13o /= 0) THEN
           DO j = 1, spec_lin(in_sp(j_c13o))%nlv
              xrec13o(j,iopt+1) = xrec13o(j,iopt)
           ENDDO
        ENDIF
        IF (i_co18 /= 0) THEN
           DO j = 1, spec_lin(in_sp(j_co18))%nlv
              xreco18(j,iopt+1) = xreco18(j,iopt)
           ENDDO
        ENDIF
        IF (i_c13o18 /= 0) THEN
           DO j = 1, spec_lin(in_sp(j_c13o18))%nlv
              xrec13o18(j,iopt+1) = xrec13o18(j,iopt)
           ENDDO
        ENDIF

           !--------------------------------------------------------------------------------
     ENDDO !-- END OF LOOP ON CLOUD POSITION
           !--------------------------------------------------------------------------------
403  CONTINUE

     ! CLOSE (56)

     !---------------------------------------
     ! Now prepare quantities on the new grid
     !---------------------------------------

     !  Prepare sub-radiation field file for DUSTEM at next iteration
     ! JLB - 19 I 2012: use more points in DUSTEM
     IF (F_dustem == 1) THEN
        IF (MOD(iopt,nav_dec) == 0) THEN
           nav_dust = 1 + iopt / nav_dec
        ELSE
           nav_dust = 2 + iopt / nav_dec
        ENDIF
        IF (ALLOCATED(iav_dust)) THEN
           DEALLOCATE (iav_dust)
           DEALLOCATE (av_dust)
        ENDIF
        ALLOCATE (iav_dust(nav_dust))
        ALLOCATE (av_dust(nav_dust))
        DO i = 1, nav_dust-1
           iav_dust(i) = nav_dec * (i-1)
           av_dust(i) = tau(nav_dec * (i-1))
        ENDDO
        iav_dust(nav_dust) = iopt
        av_dust(nav_dust) = tau(iopt)
        IF (ALLOCATED(J_dust)) THEN
           DEALLOCATE(J_dust)
        ENDIF
        ALLOCATE (J_dust(nav_dust,0:nwlg))
        DO i = 1, nav_dust
           DO j = 0, npo
              IF (tau_o(j) > av_dust(i)) THEN
                 EXIT
              ENDIF
           ENDDO
           IF (j == npo+1) THEN
              j = npo
           ENDIF
           J_dust(i,:) = intJmp(j-1,:) + (intJmp(j,:) - intJmp(j-1,:)) &
                       * (av_dust(i) - tau_o(j-1)) / dtau_o(j)
        ENDDO
     ENDIF

     ! Set here the new size of computed quantities
     npo = MIN(noptm, iopt)

     PRINT *, " Enter DUST_PROPERTIES - running"
     CALL DUST_PROPERTIES

     ! Reallocate size dependant quantities - Part 1: Profil
     CALL REALL1

     CALL COMP_DIST

     ! Reallocate size dependant quantities - Part 2: Lines
     CALL REALL

     ! Modify populations to avoid oscillations.
     IF (ifaf > 5 .AND. MOD(ifaf,3) == 0) THEN
        DO ii = 1, nspl
           DO j = 1, spec_lin(ii)%nlv
              spec_lin(ii)%xre(j,:) = SQRT(spec_lin(ii)%xre(j,:)*spec_lin(ii)%xre_o(j,:))
           ENDDO
        ENDDO
     ENDIF

     !-- UV RADIATIVE TRANSFER -----------------------------------------
     WRITE (iscrn,*) " ================================================ "
     WRITE (iscrn,*) "           Solve Radiative Transfer               "
     WRITE (iscrn,*) " ================================================ "

     IF (F_dustem == 1) THEN
        OPEN (iwrtmp, FILE="Cmp_Td.out", STATUS="unknown")
        DO iopt = 0, npo
           WRITE (iwrtmp,*) tau_o(iopt)*tautav, (tdust_o(i,iopt), i = 1, npg) &
                          , (tdust_dus_o(i,iopt), i = 1, npg)
        ENDDO
        CLOSE (iwrtmp)
        ! Experimental 25 IV 2011
        tdust_o = tdust_dus_o
     ENDIF

     CALL CALC_EXTRA

     IF (isoneside == 1) THEN
        CALL ETIN901S (nwlg)
     ELSE
        CALL ETIN90 (nwlg)
     ENDIF

     ! FEED_LINES moved after grain initialisation is complete
     DO i = 1, nspl
        CALL FEED_LINES(spec_lin(i))
     ENDDO

     ! Write partial results, so we can check convergence.
     ! (binary file only -> use prep!)

     !-------------------------------------------------------------------------
     ! Compute extra quantities not used in the code but usefull in outputs
     !-------------------------------------------------------------------------
     CALL CALC_EXTRA

     PRINT *, "                                                "
     PRINT *, " -------------- WRITE RESULTS ------------------"
     CALL SORTIE1
     PRINT *,"WRITE FITS AND XML FILES"
     CALL OUTPUT_FITS

     !CALL FLUSH
     !FLUSH (iwrit1)
     WRITE(iscrn,*) " ================================================"
     WRITE(iscrn,*) "   END OF ITERATION: ", ifaf, "/", ifafm
     WRITE(iscrn,*) " ================================================"

        !----------------------------------------------------------------------------------
  ENDDO !-- END OF LOOP ON GLOBAL ITERATIONS
        !----------------------------------------------------------------------------------

  ! Human readable output file
  CALL SORTIE2                                          ! Write logs in the .def file
  WRITE(iwrit2,*) " "
  WRITE(iwrit2,'(I2)') itenua                           ! Write itenua
  WRITE(iwrit2,'("END_OF_MODEL")')                      ! "END_OF_MODEL" - Last line of .def file.
  CLOSE(iwrit2)                                         ! Close the .def file

!--------+---------+---------+---------+---------+---------+---------+-*-------+
!  formats:

1030 format (1x,'IFAF:',I3,2x,'SLAB:',i5,2x,'ITER:',i2,2x,'Av:', 1pe18.10,/, &
           ' --------------------------------------------------',/, &
           1x,'proton density [cm-3]:',1pe13.6,2x,'temperature [K]:',0pf10.3,/)
!---------------------------------------------------------------------

END PROGRAM PXDR
