
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.


!%%%%%%%%%%%
PROGRAM rf
!%%%%%%%%%%%

  IMPLICIT NONE
  INTEGER, PARAMETER                           :: dp = SELECTED_REAL_KIND(P=15)
  CHARACTER (len=255)                          :: fichier
  INTEGER                                      :: nwl, nav
  INTEGER                                      :: n, i, ii
  REAL (kind=dp), ALLOCATABLE, DIMENSION (:)   :: wl
  REAL (kind=dp), ALLOCATABLE, DIMENSION (:)   :: av
  REAL (kind=dp)                               :: av0, wl0
  REAL (kind=dp), ALLOCATABLE, DIMENSION (:,:) :: urf

  PRINT *, " file name?"
  READ *, fichier
  OPEN(44, FILE=fichier, STATUS="unknown", FORM = 'unformatted')
  PRINT *, fichier, " opened"

  READ (44) nwl
  PRINT *, nwl, " wavelength points"

  ALLOCATE (wl(nwl))
  READ (44) wl

  nav = 0
  DO
     READ (44, END=100) av0, wl
     nav = nav + 1
  END DO
100 CLOSE (44)

  PRINT *,nav," Av points"
  
  ALLOCATE (urf(nav,nwl))
  ALLOCATE (av(nav))

  OPEN(44, FILE=fichier, STATUS="unknown", FORM = 'unformatted')
  READ (44) nwl
  READ (44) wl
  DO n=1,nav
     READ (44, END=100) av(n), urf(n,:)
     !PRINT *,n, av(n)
  END DO

  CLOSE (44)

  PRINT *, " Which graph?"
  PRINT *, " 1 - Av"
  PRINT *, " 2 - WL"
  PRINT *, " 3 - 2D"
  PRINT *, " 4 - ISRF in Dustem format"
  READ *, ii

  SELECT CASE (ii)
  CASE (1)
     PRINT *, " Enter Av:"
     READ *, av0
     DO n=1,nav
        IF (av(n) > av0) EXIT
     END DO
     n = n - 1
     OPEN (10, FILE="rf_out.dat", STATUS="unknown")
     WRITE (10, '("# Av =",1pe16.8)') av(n)
     DO i=1,nwl
        WRITE (10,'(1P,2e16.7e3)') wl(i), urf(n,i)
     END DO
     CLOSE (10)
    
  CASE (2)
     PRINT *, " Enter Wave Length:"
     READ *, wl0
     DO n=1,nwl
        IF (wl(n) > wl0) EXIT
     END DO
     n = n - 1
     OPEN (10, FILE="rf_out.dat", STATUS="unknown")
     WRITE (10, '("# WL =",1pe16.8)') wl(n)
     DO i=1,nav
        WRITE (10,'(1P,2e16.7e3)') av(i), urf(i,n)
     END DO
     CLOSE (10)
    
  CASE (3)
     OPEN (10, FILE="rf_out.dat", STATUS="unknown")
     DO n=1,nwl
        DO i=1,nav
           WRITE (10,'(1P,3e16.7e3)') av(i), wl(n), urf(i,n)
        END DO
        WRITE (10, '(" ")')
     END DO
     CLOSE (10)

  CASE (4)
     PRINT *, " Enter Av:"
     READ *, av0
     DO n=1,nav
        IF (av(n) > av0) EXIT
     END DO
     n = n - 1
     OPEN (10, FILE="ISRF.dat", STATUS="unknown")
     WRITE (10, '("# Av =",1pe16.8)') av(n)
     DO i=1,nwl
        WRITE (10,'(1P,2e16.7e3)') wl(i)*1.0e-4_dp, urf(n,i)*1.0e-8_dp * wl(i)**2
     END DO
     CLOSE (10)
    
  END SELECT

  DEALLOCATE (wl)
  DEALLOCATE (av)
  DEALLOCATE (urf)

END PROGRAM rf

