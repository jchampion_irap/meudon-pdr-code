
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

MODULE PREP_INTEMI

  USE PXDR_CONSTANTES
  USE PXDR_PROFIL

  PUBLIC :: LDVInt, LDVIntMOD

CONTAINS

SUBROUTINE LDVInt (xmu, spelin, itr, tampon, flag_AllOutput)

  USE PXDR_CONSTANTES
  USE PXDR_PROFIL
  USE PXDR_STR_DATA
  USE PXDR_RF_TOOLS
  USE PREP_VAR

  IMPLICIT NONE

  REAL (KIND=dp),           INTENT (IN)               :: xmu             !  Line of sight cosine
  TYPE(SPECTRA),            INTENT (INOUT)            :: spelin          !  Line data
  INTEGER,                  INTENT (IN)               :: itr             !  Transition index
  CHARACTER (LEN=17),       INTENT (IN)               :: tampon          !  Line identity
  INTEGER,                  INTENT (IN)               :: flag_AllOutput  !  Flag

  REAL (KIND=dp)                                :: tbg = 2.73_dp   !  Cosmic Micro-Wave background
  Real (KIND=dp), DIMENSION (:), ALLOCATABLE    :: dtau_L          !  Integrand of line optical depth
  Real (KIND=dp), DIMENSION (:), ALLOCATABLE    :: dtau_D          !  Integrand of dust optical depth
  Real (KIND=dp), DIMENSION (:), ALLOCATABLE    :: tau_L           !  Line optical depth
  Real (KIND=dp), DIMENSION (:), ALLOCATABLE    :: tau_D           !  Dust optical depth
  Real (KIND=dp), DIMENSION (:), ALLOCATABLE    :: tau_T           !  Total optical depth (running variable)
  Real (KIND=dp), DIMENSION (:), ALLOCATABLE    :: tau_nu          !  Total optical depth through cloud
  Real (KIND=dp), DIMENSION (:), ALLOCATABLE    :: tau_nu_D, tau_nu_L

  REAL (KIND=dp)                                 :: nuij           !  Line frequency (s-1)
  REAL (KIND=dp)                                 :: eij            !  E_ij = (c**2 / 2h nu**3) * D_ij
  REAL (KIND=dp)                                 :: dij            !  D_ij = (h nu / 4 pi) g_i A_ij
  INTEGER                                        :: ju, jl

  INTEGER                                        :: jpm = 100      !  Number of points in line profile
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: spec_BG        ! Background contribution to spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: dspec_D        ! Integrand of dust contribution to spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: dspec_L        ! Integrand of line contribution to spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: spec_D         ! Dust contribution to spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: spec_L         ! Line contribution to spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: spec_DT        ! Dust contribution to spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: spec_LT        ! Line contribution to spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: spec_T         ! Total spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: ibg            ! Cosmic background
  REAL (KIND=dp)                                 :: emist          !  Total line emissivity (L + D)
  REAL (KIND=dp)                                 :: emisl          !  Total line emissivity (L alone)
  REAL (KIND=dp)                                 :: emisa          !  Total line emissivity (Apparent)

  INTEGER                                        :: i, j, j_wl
  REAL (KIND=dp)                                 :: nu, dnu, ddnu, yy
  REAL (KIND=dp)                                 :: fac_exp

  ! 22 I 2011 - JLB
  ! Find index of wavelength in grid
  DO j = 0, nwlg
     IF (ABS((rf_wl%Val(j) - spelin%lac(itr) *1.0e8_dp) / rf_wl%Val(j)) < 1.0e-8_dp) THEN
        j_wl = j
        EXIT
     ENDIF
  ENDDO
  ! End

  nuij = clum/spelin%lac(itr)
  dnu = MINVAL(spelin%dnu(itr,:)) / 20.0_dp
  ju = spelin%iup(itr)
  jl = spelin%jlo(itr)
  dij = (xhp * nuij / (4.0_dp * xpi)) * spelin%gst(ju) * spelin%aij(itr)
  eij = spelin%lac(itr)**2 * spelin%aij(itr) * spelin%gst(ju) / (8.0_dp*xpi)

  ALLOCATE (dtau_L(0:npo))
  ALLOCATE (dtau_D(0:npo))
  ALLOCATE (tau_L(0:npo))
  ALLOCATE (tau_D(0:npo))
  ALLOCATE (tau_T(0:npo))
  ALLOCATE (dspec_D(0:npo))
  ALLOCATE (dspec_L(0:npo))
  ALLOCATE (spec_D(0:npo))
  ALLOCATE (spec_L(0:npo))
  IF (.NOT. ALLOCATED(spec_T)) THEN
     ALLOCATE (spec_BG(-jpm:jpm))
     ALLOCATE (spec_T(-jpm:jpm))
     ALLOCATE (spec_LT(-jpm:jpm))
     ALLOCATE (spec_DT(-jpm:jpm))
     ALLOCATE (ibg(-jpm:jpm))
     ALLOCATE (tau_nu(-jpm:jpm))
     ALLOCATE (tau_nu_D(-jpm:jpm))
     ALLOCATE (tau_nu_L(-jpm:jpm))
  ENDIF
  tau_D   = 0.0_dp
  tau_L   = 0.0_dp
  spec_BG = 0.0_dp
  spec_D  = 0.0_dp
  spec_L  = 0.0_dp
  spec_T  = 0.0_dp

  !---  Compute optical depth ------------------------------------------------------------------------------
  ! integration is done from tau = 0 to tau_max, so the integrated quantity is tau_T(t) - tau_T(s_max)
  !     where t is the current position along the ray, and s_max is the total size of the cloud, measured
  !     from intrance at the "back" side (tau_max) to the front side of the cloud, towards the observer

  ! Dust optical depth (independant of profile):
  dtau_D = (du_prop%abso(j_wl,:) + du_prop%scat(j_wl,:)) * densh(0:npo) / xmu
  DO i = 1, npo
     tau_D(i) = tau_D(i-1) + (dtau_D(i) + dtau_D(i-1)) * (d_cm(i) - d_cm(i-1)) * 0.5_dp
  ENDDO

  !--- Scan line profile -----------------------------------------------------------------------------------
  DO j = -jpm, jpm
     ddnu = j * dnu
     nu = nuij + ddnu
     yy = ddnu / nuij

     ! Background
     fac_exp = xhp*nu/(xk*tbg)
     IF (fac_exp > LOG(HUGE(1.0_dp))*0.9_dp) THEN
        ibg(j) = 0.0_dp
     ELSE
        ibg(j) = 2.0_dp * xhp * nu**3 / (clum*clum)
        ibg(j) = ibg(j) / (EXP(xhp*nu/(xk*tbg)) - 1.0_dp)
     ENDIF

     ! Line optical depth
     dtau_L = (clum * eij / (SQRT(xpi) * nuij)) * spelin%abu &
            * (spelin%xre(jl,:) / spelin%gst(jl) - spelin%xre(ju,:) / spelin%gst(ju)) &
            * EXP(-(yy*clum/spelin%vel)**2) / (spelin%vel * xmu)
     DO i = 1, npo
        tau_L(i) = tau_L(i-1) + (dtau_L(i) + dtau_L(i-1)) * (d_cm(i) - d_cm(i-1)) * 0.5_dp
     ENDDO

     ! Total optical depth
     tau_T = tau_L + tau_D
     tau_nu(j) = tau_T(npo)
     tau_nu_D(j) = tau_D(npo)
     tau_nu_L(j) = tau_L(npo)

     ! Background contribution
     spec_BG(j) = ibg(j) * EXP(-tau_nu(j))

     ! Dust contribution
     dspec_D = (1.0e8_dp * spelin%lac(itr)**2 / clum) * du_prop%emis(j_wl,:) * densh(0:npo) * EXP(-tau_T) / xmu
     spec_D(npo) = 0.0_dp
     DO i = npo-1, 0, -1
        spec_D(i) = spec_D(i+1) + (dspec_D(i) + dspec_D(i+1)) * (d_cm(i+1) - d_cm(i)) * 0.5_dp
     ENDDO

     ! Line contribution
     ! One can check the influence of dust oppacity by using only tau_L in the exponential
     dspec_L = (clum * dij / (SQRT(xpi) * nuij)) * (spelin%abu * spelin%xre(ju,:) / spelin%gst(ju)) &
!            * EXP(-(yy*clum/spelin%vel)**2) * EXP(-tau_L) / (xmu *spelin%vel)
             * EXP(-(yy*clum/spelin%vel)**2) * EXP(-tau_T) / (xmu *spelin%vel)
     spec_L(npo) = 0.0_dp
     DO i = npo-1, 0, -1
        spec_L(i) = spec_L(i+1) + (dspec_L(i) + dspec_L(i+1)) * (d_cm(i+1) - d_cm(i)) * 0.5_dp
     ENDDO

     spec_LT(j) = spec_L(0)
     spec_DT(j) = spec_D(0)
     spec_T(j) = spec_BG(j) + spec_DT(j) + spec_LT(j)
  ENDDO

  ! Supress background
  spec_T = spec_T - ibg

  ! Output
  emist = 0.0_dp
  emisl = 0.0_dp
  emisa = 0.0_dp
  DO j = -jpm, jpm
     emist = emist + spec_T(j)
     emisl = emisl + spec_LT(j)
     emisa = emisa + (spec_T(j) - spec_DT(jpm))
  ENDDO
  emist = emist * dnu
  emisl = emisl * dnu
  emisa = emisa * dnu

  IF (flag_AllOutput == 1) THEN
     WRITE(*,'(A,3(2x,1p,E12.6),2x,"erg cm-2 s-1 str-1")') &
          TRIM(ADJUSTL(tampon)), emist, emisl, emisa

     WRITE (35, '(/,"# ",A," nu:",f11.2," GHz, lamda:",f11.4," micron, mu: ",0pf8.4)') &
           tampon, 1.0e-9_dp*nuij, 1.0e4_dp*spelin%lac(itr), xmu
     WRITE (35, '("#  I.T:",1pe12.5,", I.L:", e12.5,", I.ap:",e12.5," erg cm-2 s-1 str-1 ")') &
           emist, emisl, emisa
     WRITE (35, '("#  Int. Obs:",1pe12.5," K km s-1 ")') 0.5e-5_dp * emisl * spelin%lac(itr)**3 / xk
!    WRITE (35, '("# D.nu/nu   Tau_nu_D   Tau_nu_L  Spec.T    Spec.BG    spec.DT    spec.LT    ibg")')
     WRITE (35, '("# D.nu/nu   Tau_nu   Spec.T    Spec.BG    spec.DT    spec.LT    ibg")')
     DO j = -jpm, jpm
        ddnu = j * dnu
        yy = ddnu / nuij
!       WRITE (35, "(1p,20e15.6E3)") yy, tau_nu_D(j), tau_nu_L(j), spec_T(j), spec_BG(j), spec_DT(j), spec_LT(j), ibg(j)
        WRITE (35, "(1p,20e15.6E3)") yy, tau_nu(j), spec_T(j), spec_BG(j), spec_DT(j), spec_LT(j), ibg(j)
     ENDDO
  ENDIF

  DEALLOCATE (dtau_L, dtau_D)
  DEALLOCATE (tau_L, tau_D, tau_T)
  DEALLOCATE (tau_nu, tau_nu_D, tau_nu_L)
  DEALLOCATE (dspec_D)
  DEALLOCATE (dspec_L)
  DEALLOCATE (spec_BG, spec_D, spec_L)
  DEALLOCATE (spec_DT, spec_LT, spec_T)
  DEALLOCATE (ibg)

END SUBROUTINE LDVInt

SUBROUTINE LDVIntMOD (xmu, spelin, itr, tampon, flag_AllOutput)

  USE PXDR_CONSTANTES
  USE PXDR_PROFIL
  USE PXDR_STR_DATA
  USE PXDR_RF_TOOLS
  USE PREP_VAR

  IMPLICIT NONE

  REAL (KIND=dp),           INTENT (IN)               :: xmu             !  Line of sight cosine
  TYPE(SPECTRA),            INTENT (INOUT)            :: spelin          !  Line data
  INTEGER,                  INTENT (IN)               :: itr             !  Transition index
  CHARACTER (LEN=17),       INTENT (IN)               :: tampon          !  Line identity
  INTEGER,                  INTENT (IN)               :: flag_AllOutput  !  Flag

  REAL (KIND=dp)                                :: tbg = 2.73_dp   !  Cosmic Micro-Wave background
  Real (KIND=dp), DIMENSION (:), ALLOCATABLE    :: dtau_L          !  Integrand of line optical depth
  Real (KIND=dp), DIMENSION (:), ALLOCATABLE    :: dtau_D          !  Integrand of dust optical depth
  Real (KIND=dp), DIMENSION (:), ALLOCATABLE    :: tau_L           !  Line optical depth
  Real (KIND=dp), DIMENSION (:), ALLOCATABLE    :: tau_D           !  Dust optical depth
  Real (KIND=dp), DIMENSION (:), ALLOCATABLE    :: tau_T           !  Total optical depth (running variable)
  Real (KIND=dp), DIMENSION (:), ALLOCATABLE    :: tau_nu          !  Total optical depth through cloud
  Real (KIND=dp), DIMENSION (:), ALLOCATABLE    :: tau_nu_D, tau_nu_L

  REAL (KIND=dp)                                 :: nuij           !  Line frequency (s-1)
  REAL (KIND=dp)                                 :: eij            !  E_ij = (c**2 / 2h nu**3) * D_ij
  REAL (KIND=dp)                                 :: dij            !  D_ij = (h nu / 4 pi) g_i A_ij
  INTEGER                                        :: ju, jl

  INTEGER                                        :: jpm = 100      !  Number of points in line profile
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: spec_BG        ! Background contribution to spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: dspec_D        ! Integrand of dust contribution to spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: dspec_L        ! Integrand of line contribution to spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: spec_D         ! Dust contribution to spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: spec_L         ! Line contribution to spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: spec_DT        ! Dust contribution to spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: spec_LT        ! Line contribution to spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: spec_T         ! Total spectra
  REAL (KIND=dp), ALLOCATABLE, DIMENSION(:)      :: ibg            ! Cosmic background
  REAL (KIND=dp)                                 :: emist          !  Total line emissivity (L + D)
  REAL (KIND=dp)                                 :: emisl          !  Total line emissivity (L alone)
  REAL (KIND=dp)                                 :: emisa          !  Total line emissivity (Apparent)

  INTEGER                                        :: i, j, j_wl
  REAL (KIND=dp)                                 :: nu, dnu, ddnu, yy
  REAL (KIND=dp)                                 :: fac_exp

  ! 22 I 2011 - JLB
  ! Find index of wavelength in grid
  DO j = 0, nwlg
     IF (ABS((rf_wl%Val(j) - spelin%lac(itr) *1.0e8_dp) / rf_wl%Val(j)) < 1.0e-8_dp) THEN
        j_wl = j
        EXIT
     ENDIF
  ENDDO
  ! End

  nuij = clum/spelin%lac(itr)
  dnu = MINVAL(spelin%dnu(itr,:)) / 20.0_dp
  ju = spelin%iup(itr)
  jl = spelin%jlo(itr)
  dij = (xhp * nuij / (4.0_dp * xpi)) * spelin%gst(ju) * spelin%aij(itr)
  eij = spelin%lac(itr)**2 * spelin%aij(itr) * spelin%gst(ju) / (8.0_dp*xpi)

  ALLOCATE (dtau_L(0:npo))
  ALLOCATE (dtau_D(0:npo))
  ALLOCATE (tau_L(0:npo))
  ALLOCATE (tau_D(0:npo))
  ALLOCATE (tau_T(0:npo))
  ALLOCATE (dspec_D(0:npo))
  ALLOCATE (dspec_L(0:npo))
  ALLOCATE (spec_D(0:npo))
  ALLOCATE (spec_L(0:npo))
  IF (.NOT. ALLOCATED(spec_T)) THEN
     ALLOCATE (spec_BG(-jpm:jpm))
     ALLOCATE (spec_T(-jpm:jpm))
     ALLOCATE (spec_LT(-jpm:jpm))
     ALLOCATE (spec_DT(-jpm:jpm))
     ALLOCATE (ibg(-jpm:jpm))
     ALLOCATE (tau_nu(-jpm:jpm))
     ALLOCATE (tau_nu_D(-jpm:jpm))
     ALLOCATE (tau_nu_L(-jpm:jpm))
  ENDIF
  tau_D   = 0.0_dp
  tau_L   = 0.0_dp
  spec_BG = 0.0_dp
  spec_D  = 0.0_dp
  spec_L  = 0.0_dp
  spec_T  = 0.0_dp

  !---  Compute optical depth ------------------------------------------------------------------------------
  ! integration is done from tau = 0 to tau_max, so the integrated quantity is tau_T(t) - tau_T(s_max)
  !     where t is the current position along the ray, and s_max is the total size of the cloud, measured
  !     from intrance at the "back" side (tau_max) to the front side of the cloud, towards the observer

  ! Dust optical depth (independant of profile):
  dtau_D = (du_prop%abso(j_wl,:) + du_prop%scat(j_wl,:)) * densh(0:npo) / xmu
  DO i = 1, npo
     tau_D(i) = tau_D(i-1) + (dtau_D(i) + dtau_D(i-1)) * (d_cm(i) - d_cm(i-1)) * 0.5_dp
  ENDDO

  !--- Scan line profile -----------------------------------------------------------------------------------
  DO j = -jpm, jpm
     ddnu = j * dnu
     nu = nuij + ddnu
     yy = ddnu / nuij

     ! Background
     fac_exp = xhp*nu/(xk*tbg)
     IF (fac_exp > LOG(HUGE(1.0_dp))*0.9_dp) THEN
        ibg(j) = 0.0_dp
     ELSE
        ibg(j) = 2.0_dp * xhp * nu**3 / (clum*clum)
        ibg(j) = ibg(j) / (EXP(xhp*nu/(xk*tbg)) - 1.0_dp)
     ENDIF

     ! Line optical depth
     dtau_L = (clum * eij / (SQRT(xpi) * nuij)) * spelin%abu &
            * (spelin%xre(jl,:) / spelin%gst(jl) - spelin%xre(ju,:) / spelin%gst(ju)) &
            * EXP(-(yy*clum/spelin%vel)**2) / (spelin%vel * xmu)
     DO i = 1, npo
        tau_L(i) = tau_L(i-1) + (dtau_L(i) + dtau_L(i-1)) * (d_cm(i) - d_cm(i-1)) * 0.5_dp
     ENDDO

     ! Total optical depth
     tau_T = tau_L + tau_D
     tau_nu(j) = tau_T(npo)
     tau_nu_D(j) = tau_D(npo)
     tau_nu_L(j) = tau_L(npo)

     ! Background contribution
     spec_BG(j) = ibg(j) * EXP(-tau_nu(j))

     ! Dust contribution
     dspec_D = (1.0e8_dp * spelin%lac(itr)**2 / clum) * du_prop%emis(j_wl,:) * densh(0:npo) * EXP(-tau_T) / xmu
     spec_D(npo) = 0.0_dp
     DO i = npo-1, 0, -1
        spec_D(i) = spec_D(i+1) + (dspec_D(i) + dspec_D(i+1)) * (d_cm(i+1) - d_cm(i)) * 0.5_dp
     ENDDO

     ! Line contribution
     ! One can check the influence of dust oppacity by using only tau_L in the exponential
     dspec_L = (clum * dij / (SQRT(xpi) * nuij)) * (spelin%abu * spelin%xre(ju,:) / spelin%gst(ju)) &
!            * EXP(-(yy*clum/spelin%vel)**2) * EXP(-tau_L) / (xmu *spelin%vel)
             * EXP(-(yy*clum/spelin%vel)**2) * EXP(-tau_T) / (xmu *spelin%vel)
     spec_L(npo) = 0.0_dp
     DO i = npo-1, 0, -1
        spec_L(i) = spec_L(i+1) + (dspec_L(i) + dspec_L(i+1)) * (d_cm(i+1) - d_cm(i)) * 0.5_dp
     ENDDO

     spec_LT(j) = spec_L(0)
     spec_DT(j) = spec_D(0)
     spec_T(j) = spec_BG(j) + spec_DT(j) + spec_LT(j)
  ENDDO

  ! Supress background
  spec_T = spec_T - ibg

  ! Output
  emist = 0.0_dp
  emisl = 0.0_dp
  emisa = 0.0_dp
  DO j = -jpm, jpm
     emist = emist + spec_T(j)
     emisl = emisl + spec_LT(j)
     emisa = emisa + (spec_T(j) - spec_DT(jpm))
  ENDDO
  emist = emist * dnu
  emisl = emisl * dnu
  emisa = emisa * dnu

  IF (flag_AllOutput == 1) THEN
     WRITE(*,'(A,3(2x,1p,E12.6),2x,"erg cm-2 s-1 str-1")') &
          TRIM(ADJUSTL(tampon)), emist, emisl, emisa

     WRITE (35, '(/,"# ",A," nu:",f11.2," GHz, lamda:",f11.4," micron, mu: ",0pf8.4)') &
           tampon, 1.0e-9_dp*nuij, 1.0e4_dp*spelin%lac(itr), xmu
     IF (spelin%nqn == 2) THEN
        WRITE (43, '(A5,X,A7,X,A7,X,A3,X,A7,X,A7,X,A1,X,f11.2,X,0pf8.4,X,A1,X,1pe12.5,X,e12.5,X,e12.5)') &
              spelin%nam, &
              TRIM(ADJUSTL(spelin%qnam(1)))//'='//TRIM(ADJUSTL(spelin%quant(spelin%iup(itr),1))), &
              TRIM(ADJUSTL(spelin%qnam(2)))//'='//TRIM(ADJUSTL(spelin%quant(spelin%iup(itr),2))), &
              '-->', &
              TRIM(ADJUSTL(spelin%qnam(1)))//'='//TRIM(ADJUSTL(spelin%quant(spelin%jlo(itr),1))), &
              TRIM(ADJUSTL(spelin%qnam(2)))//'='//TRIM(ADJUSTL(spelin%quant(spelin%jlo(itr),2))), &
              '|',1.0e-9_dp*nuij,xmu,'|',1.0e-3_dp*emist,1.0e-3_dp*emisl,1.0e-3_dp*emisa
     ELSEIF (spelin%nqn == 1) THEN
        WRITE (43, '(A5,X,A7,X,A7,X,A3,X,A7,X,A7,X,A1,X,f11.2,X,0pf8.4,X,A1,X,1pe12.5,X,e12.5,X,e12.5)') &
              spelin%nam, &
              TRIM(ADJUSTL(spelin%qnam(1)))//'='//TRIM(ADJUSTL(spelin%quant(spelin%iup(itr),1))),' ', &
              '-->', &
              TRIM(ADJUSTL(spelin%qnam(1)))//'='//TRIM(ADJUSTL(spelin%quant(spelin%jlo(itr),1))),' ', &
              '|',1.0e-9_dp*nuij,xmu,'|',1.0e-3_dp*emist,1.0e-3_dp*emisl,1.0e-3_dp*emisa
     ELSE
        WRITE (43, '(A5,X,A7,X,A7,X,A3,X,A7,X,A7,X,A1,X,f11.2,X,0pf8.4,X,A1,X,1pe12.5,X,e12.5,X,e12.5)') &
              spelin%nam, &
              TRIM(ADJUSTL(spelin%qnam(1)))//'='//TRIM(ADJUSTL(spelin%quant(spelin%iup(itr),1))), &
              TRIM(ADJUSTL(spelin%qnam(2)))//'='//TRIM(ADJUSTL(spelin%quant(spelin%iup(itr),2))), &
              '-->', &
              TRIM(ADJUSTL(spelin%qnam(1)))//'='//TRIM(ADJUSTL(spelin%quant(spelin%jlo(itr),1))), &
              TRIM(ADJUSTL(spelin%qnam(2)))//'='//TRIM(ADJUSTL(spelin%quant(spelin%jlo(itr),2))), &
              '|',1.0e-9_dp*nuij,xmu,'|',1.0e-3_dp*emist,1.0e-3_dp*emisl,1.0e-3_dp*emisa
     ENDIF
     WRITE (35, '("#  I.T:",1pe12.5,", I.L:", e12.5,", I.ap:",e12.5," erg cm-2 s-1 str-1 ")') &
           emist, emisl, emisa
     WRITE (35, '("#  Int. Obs:",1pe12.5," K km s-1 ")') 0.5e-5_dp * emisl * spelin%lac(itr)**3 / xk
!    WRITE (35, '("# D.nu/nu   Tau_nu_D   Tau_nu_L  Spec.T    Spec.BG    spec.DT    spec.LT    ibg")')
     WRITE (35, '("# D.nu/nu   Tau_nu   Spec.T    Spec.BG    spec.DT    spec.LT    ibg")')
     DO j = -jpm, jpm
        ddnu = j * dnu
        yy = ddnu / nuij
!       WRITE (35, "(1p,20e15.6E3)") yy, tau_nu_D(j), tau_nu_L(j), spec_T(j), spec_BG(j), spec_DT(j), spec_LT(j), ibg(j)
        WRITE (35, "(1p,20e15.6E3)") yy, tau_nu(j), spec_T(j), spec_BG(j), spec_DT(j), spec_LT(j), ibg(j)
     ENDDO
  ENDIF

  DEALLOCATE (dtau_L, dtau_D)
  DEALLOCATE (tau_L, tau_D, tau_T)
  DEALLOCATE (tau_nu, tau_nu_D, tau_nu_L)
  DEALLOCATE (dspec_D)
  DEALLOCATE (dspec_L)
  DEALLOCATE (spec_BG, spec_D, spec_L)
  DEALLOCATE (spec_DT, spec_LT, spec_T)
  DEALLOCATE (ibg)

END SUBROUTINE LDVIntMOD

END MODULE PREP_INTEMI
