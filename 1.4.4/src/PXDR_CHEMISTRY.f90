
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

! JLB - 22 May 2008

MODULE PXDR_CHEMISTRY

  PRIVATE

  PUBLIC :: CHIMIJ, MNEWT, FBTH

CONTAINS

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE CHIMIJ (ttry, iopt, itn)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  USE PXDR_CONSTANTES
  USE PXDR_CHEM_DATA
  USE PXDR_PROFIL
  USE PXDR_AUXILIAR
  USE PXDR_STR_DATA
  USE PXDR_RF_TOOLS

!--------1---------2---------3---------4---------5---------6---------7--
!   Chemical equation source terms computation
!---------------------------------------------------------------------

  IMPLICIT NONE

  REAL (KIND=dp), INTENT (IN)        :: ttry
  INTEGER, INTENT (IN)               :: iopt, itn

  INTEGER, DIMENSION(0:nes)          :: indy
  REAL (KIND=dp), DIMENSION(0:natom) :: ratiab
  REAL (KIND=dp)                     :: ratlim = 1.0_dp
  REAL (KIND=dp)                     :: avr
  REAL (KIND=dp)                     :: rate, rate1, rate2
  REAL (KIND=dp)                     :: nu0, nu0_1, nu0_2
  REAL (KIND=dp)                     :: fr_free, tgr
  REAL (KIND=dp)                     :: bilth = 0.0_dp
  REAL (KIND=dp)                     :: xldb_1, xldb_2
  REAL (KIND=dp)                     :: tm1_1, tm1_2
  REAL (KIND=dp)                     :: stickH, frach2
  REAL (KIND=dp)                     :: r_ad_i, r_rej
  REAL (KIND=dp)                     :: endo_rxn, rat_l
  INTEGER                            :: ir1, ir2, ir3, iv1, iv2, ivp, iprod
  INTEGER                            :: ideb, ifin
  INTEGER                            :: ik, kkk, lev

  INTEGER                            :: i, j, k2, l, mm
  INTEGER                            :: ici1, ici2, icc
  REAL (KIND=dp)                     :: summ, dum, expon
  REAL (KIND=dp)                     :: top1, top2
  REAL (KIND=dp)                     :: stick_ER

  ! xkneut is the neutralisation rate of ions on grains
  ! from Bakes & Tielens, Ap. J., 427, 822, (1994)

  ! Look for most abundant species
  ! elements conservation is done (for each atom) on the
  ! most abundance species. In case of conflict (e.g. CO)
  ! on the second most abundant

  ! LCON(K2): index of species used for conservation of element K2.
  DO k2 = 1, natom-1
     top1 = 0.0_dp
     top2 = 0.0_dp
     icx(k2,1) = 0
     icx(k2,2) = 0

     DO l = 1, nspec-1
        dum = ael(l,k2)
        IF (dum == 0.0_dp) CYCLE
        IF (lcon(l) == k2) THEN
           lcon(l) = 0
        ENDIF
        IF (ab(l)*dum > top1) THEN
           top2 = top1
           top1 = ab(l) * dum
           icx(k2,2) = icx(k2,1)
           icx(k2,1) = l
        ELSE IF (ab(l)*dum >top2) THEN
           top2 = ab(l) * dum
           icx(k2,2) = l
        ENDIF
     ENDDO
     IF (top1 /= 0.0_dp) THEN
        ratiab(k2) = top2 / top1
     ENDIF
  ENDDO

  DO k2 = 1, natom-1
     ici1 = icx(k2,1)
     ici2 = icx(k2,2)
     IF (ici1 == 0) CYCLE

     IF (ratiab(k2) > ratlim) THEN
        IF (ael(ici1,natom) == 1 .AND. ael(ici2,natom) == 0) THEN
           j = ici1
           ici1 = ici2
           ici2 = j
        ENDIF
     ENDIF

     icc = 0
     DO i = 1, k2-1
        IF (lcon(ici1) == i) icc = i
     ENDDO

     IF (icc == 0) THEN
        lcon(ici1) = k2
     ELSE
        IF (ratiab(k2) > ratiab(icc)) THEN
           lcon(ici2) = k2
        ELSE
           lcon(ici1) = k2
           lcon(icx(icc,2)) = icc
        ENDIF
     ENDIF
  ENDDO

  indy = 0

  ! Extinction from the "other" side
  avr = Avmax - av

  ! Fraction of H2 (for secondary photons flux)
  frach2 = ab(i_h2) / (ab(i_h) + ab(i_h2))

! chemical reactions are reordered:
! (0) formation of H2 and HD on grains [Old Style] (nh2for)
! (1) cosmic ray processes (ncrion)
! (2) secondary photon processes (npsion)
! (3) radiative association  (nasrad)
! (4) gas phase reactions (nrest)
! (6) Endothermal reactions with H2 (nh2endo) (NEW!)
! (5) photoreactions avec fit exterieur (nphot_f)
! (7) photoreactions avec integration directe (nphot_i)
! (8) 3 body reactions (n3body)
! (101...) exceptions (ncpart)

! (11) reactions on grain surface (ngsurf)
! (12) photoreactions on grain surface (ngphot)
! (13) adsorption on to grains (ngads)
! (14) neutralization (ngneut)
! (15) explosive desorption from grains (nggdes)
! (16) cosmic ray induced desorption from grains (ncrdes)
! (17) photodesorption from grains (nphdes)
! (18) evaporation from grains (ngevap)
! (19) chemisorption on grains (ngchs)
! (20) Eley-Rideal process (ngelri)

!-------------------------------------------------------------------------
!  Special cases       : NSPEC + 1 : ELECTRON
!                        NSPEC + 2 : PHOTON
!                        NSPEC + 3 : CRP
!                        NSPEC + 4 : GRAIN

!       -------------------
!-- 1 - Gas phase reactions
!       -------------------

!-------------------------------------------------------------------------
! (0) formation of H2 and HD on grains [Old Style] (nh2for)  (cm3 s-1)
!-------------------------------------------------------------------------

  DO i = 1, nh2for
     ir1 = react(i,1)
     ir2 = react(i,2)
     iprod = react(i,4)

     ! H2 formation - OLD STYLE : gamm  = rate in cm+3 s-1 at 100 K for a standard metallicity
     ! Typically, gamm = 3E-17 cm+3 s-1
     ! We scale gamma by SQRT(T) and the metallicity (we use the mass of grains / gas for that)
     ! The scaling (100.0 or 300.0 K) and the exponent (0.0 or 0.5) are set in the chemistry file
     rate = gamm(i) * densh(iopt) * (ttry / bet(i))**alpha(i) &
          * (g_ratio / 0.01_dp) / (istiny + ab(i_h))

     kkk = indy(iprod) + 1
     indy(iprod) = kkk
     xmatk(iprod)%fdt(kkk)%k = rate

     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate

     kkk = indy(ir2) + 1
     indy(ir2) = kkk
     xmatk(ir2)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (1) cosmic ray processes (ncrion)   (s-1)
!-------------------------------------------------------------------------
  ideb = nh2for + 1
  ifin = nh2for + ncrion
  DO i = ideb, ifin
     rate = gamm(i) * zeta

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (2) secondary photon processes (npsion)   (s-1)
!-------------------------------------------------------------------------

  ideb = ifin + 1
  ifin = ifin + npsion
  DO i = ideb, ifin
     rate = gamm(i) * frach2 * zeta * (ttry/300.0_dp)**alpha(i)

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (3) radiative association  (nasrad)   (cm3 s-1)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + nasrad
  DO i = ideb, ifin
     rate = gamm(i) * (ttry/300.0_dp)**alpha(i) * EXP(-bet(i)/ttry)

     iprod = react(i,4)
     kkk = indy(iprod) + 1
     indy(iprod) = kkk
     xmatk(iprod)%fdt(kkk)%k = rate

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate

     ir2 = react(i,2)
     kkk = indy(ir2) + 1
     indy(ir2) = kkk
     xmatk(ir2)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (4) gas phase reactions (nrest)   (cm3 s-1)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + nrest
  DO i = ideb, ifin
     IF (bet(i) < 0.0_dp) THEN
        expon = EXP(-bet(i)/MAX(ttry,200.0_dp))
     ELSE
        expon = EXP(-bet(i)/ttry)
     ENDIF
     rate = gamm(i) * (ttry/300.0_dp)**alpha(i) * expon

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate

     ir2 = react(i,2)
     kkk = indy(ir2) + 1
     indy(ir2) = kkk
     xmatk(ir2)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (6) Endothermal reactions with H2 (nh2endo)   (cm3 s-1)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + nh2endo
  DO i = ideb, ifin
     rate = 0.0_dp
     IF (itype(i) == 6) THEN
        DO lev = 1, l0h2
           endo_rxn = MAX(bet(i) - spec_lin(in_sp(i_h2))%elk(lev), 0.0_dp)
           rat_l = gamm(i) * (ttry/300.0_dp)**alpha(i) * EXP(-endo_rxn/ttry)
           rate = rate + rat_l * xreh2(lev,iopt)
        ENDDO
     ELSE IF (itype(i) == 61) THEN ! Formation of CH+ following Agundez et al. ApJ (2010)
        ! If used in the chemistry, gamm, alp, bet in .chi file are not used.
        DO lev = 1, 8
           endo_rxn = MAX(4827.0e0_dp-spec_lin(in_sp(i_h2))%elk(lev),0.0e0_dp)
           rat_l = 1.58e-10_dp * EXP(-endo_rxn/ttry)
           rate  = rate + rat_l * xreh2(lev,iopt)
        ENDDO
        DO lev = 9, l0h2
           rate = rate + 1.6e-9_dp * xreh2(lev,iopt)
        ENDDO
     ENDIF

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate

     ir2 = react(i,2)
     kkk = indy(ir2) + 1
     indy(ir2) = kkk
     xmatk(ir2)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (5) photoreactions (external fit) (nphot_f)    (s-1)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + nphot_f
  DO i = ideb, ifin
     ir1 = react(i,1)
     ! The effective radm (or radp) coefficient is computed in PXDR_TRANSFER.f90
     rate1 = gamm(i) * EXP(-bet(i) * av) * radm
     rate2 = gamm(i) * EXP(-bet(i) * avr) * radp
     rate = rate1 + rate2

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (7) photoreactions avec integration directe (nphot_i)    (s-1)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + nphot_i
  DO i = ideb, ifin
     rate = pdiesp(i-ideb+1,iopt)

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (8) 3 body reactions (n3body)
!-------------------------------------------------------------------------

  ideb = ifin + 1
  ifin = ifin + n3body
  DO i = ideb, ifin
     ir1 = react(i,1)
     ir2 = react(i,2)
     ir3 = react(i,3)
     expon = EXP(-bet(i)/ttry)
     rate = gamm(i) * (ttry/300.0_dp)**alpha(i) * expon

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate
     kkk = indy(ir2) + 1
     indy(ir2) = kkk
     xmatk(ir2)%fdt(kkk)%k = - rate
     kkk = indy(ir3) + 1
     indy(ir3) = kkk
     xmatk(ir3)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (101...) exceptions (ncpart)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + ncpart
  DO i = ideb, ifin
     ir1 = react(i,1)
     ir2 = react(i,2)

     ! 101: N+ + H2:
     IF (ir1 == i_h2 .AND. ir2 == i_np) THEN
        rate = gamm(i) * (h2para * EXP(-bet(i) / ttry) + h2orth) * ((ttry / 300.0_dp)**alpha(i))

     ! 102: H3+ + HD:
     ELSE IF (ir1 == i_h2 .AND. ir2 == i_h2dp .AND. ttry>= 20.0_dp)   THEN
        rate = gamm(i) / 10**(62.481 *ttry**(-0.94871_dp))
     ELSE IF (ir1 == i_h2 .AND. ir2 == i_h2dp .AND. ttry< 20.0_dp) THEN
        rate = gamm(i) / (6.3407d35 * ttry **(-25.203_dp))

     ! 103: O+ + H:
     ELSE IF (ir1 == i_h .AND. ir2 == i_op) THEN
        rate = (gamm(i) + alpha(i) * ttry) * EXP(-bet(i) / ttry)

     ! 104: H+ + O:
     ELSE IF (ir1 == i_o .AND. ir2 == i_hp) THEN
        rate = (gamm(i) + alpha(i) * ttry) * EXP(-bet(i) / ttry)

     ! 105: reaction EXP(-T/E)  (formation de H- : Black)
     ELSE IF (itype(i) == 105) THEN
        rate = gamm(i) * (ttry/300.0_dp)**alpha(i) * EXP(-ttry/bet(i))

     ! 106: cl + h2 -> hcl + h :
     ELSE IF (itype(i) == 106 .AND. ttry< 354.0_dp) THEN
        rate = 2.52e-11_dp * EXP(-2214.0_dp / ttry)
     ELSE IF (itype(i) == 106 .AND. ttry>= 354.0_dp)   THEN
        rate = 2.86e-11_dp * (ttry/300.0_dp)**1.72_dp * EXP(-1544.0_dp / ttry)

     ! 107: hcl + h  -> cl + h2 :
     ELSE IF (itype(i) == 107 .AND. ttry< 354.0_dp) THEN
        rate = 1.49e-11_dp * EXP(-1763.0_dp / ttry)
     ELSE IF (itype(i) == 107 .AND. ttry>= 354.0_dp)   THEN
        rate = 1.69e-12_dp * (ttry/300.0_dp)**1.72_dp * EXP(-1093.0_dp / ttry)

     ! 108: f + h2 -> hf + h :
     ELSE IF (itype(i) == 108) THEN
     ! The rate below can become negative for large values of T.
     ! rate = 8.8909e-13_dp + 4.6259e-14_dp * ttry + 3.2187e-16_dp * ttry**2 &
     !      - 4.1299e-19_dp * ttry**3
     ! Expression from Neufeld 2009
       rate = 1.0e-10_dp * (exp(-450.0_dp/ttry) + 0.078_dp * exp(-80.0_dp/ttry) &
            + 0.01555_dp * exp(-10.0_dp/ttry))

     ELSE
        rate = 0.0_dp
     ENDIF

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate

     kkk = indy(ir2) + 1
     indy(ir2) = kkk
     xmatk(ir2)%fdt(kkk)%k = - rate
  ENDDO

!       ---------------------
!-- 2 - Reactions with grains
!       ---------------------

!-------------------------------------------------------------------------
! (11) reactions on grain surface (ngsurf)
!-------------------------------------------------------------------------
! 10 Feb 2008 - Those reaction are built for only one product
!               This could be changed later

  ideb = ifin + 1
  ifin = ifin + ngsurf
  DO i = ideb, ifin
     ir1 = react(i,1)
     ir2 = react(i,2)

     IF (itype(i) == 11) THEN

        nu0_2 = SQRT(2.0_dp*xk*t_evap(ir2)/(xmh*mol(ir2))) / (xpi * dsite)
        fr_free = MIN(dsite**2 / (s_gr_t * densh(iopt)), 1.0_dp / ab(ir1))

        rate = 0.0_dp
        iprod = react(i,4)
        DO mm = 1, npg
           iv2 = nspec + (ir2 - nsp_n1 - 1) * npg + mm

           tgr = tdust(mm,iopt)
           xldb_2 =  2.0_dp * xpi * SQRT(2.0_dp * xmh * mol(ir2) * xk * (t_diff(ir2) - tgr)) / xhp
           tm1_2 = nu0_2 * (EXP(-t_diff(ir2) / tgr) + 1.0_dp / (1.0_dp + t_diff(ir2)**2 &
                 * SINH(xldb_2 * dsite)**2 / (4.0_dp * tgr * (t_diff(ir2) - tgr))))

           rate = gamm(i) * tm1_2 * fr_free
           rate = rate * EXP(-bet(i)/tgr)

           kkk = indy(iprod) + 1
           indy(iprod) = kkk
           xmatk(iprod)%fdt(kkk)%k = rate * agrnrm * wwmg(mm) * densh(iopt)

           kkk = indy(ir1) + 1
           indy(ir1) = kkk
           xmatk(ir1)%fdt(kkk)%k = - rate * agrnrm * wwmg(mm) * densh(iopt)

           kkk = indy(iv2) + 1
           indy(iv2) = kkk
           xmatk(iv2)%fdt(kkk)%k = - rate
        ENDDO

     ELSE IF (itype(i) == 111) THEN

        nu0_1 = SQRT(2.0_dp*xk*t_evap(ir1)/(xmh*mol(ir1))) / (xpi * dsite)
        nu0_2 = SQRT(2.0_dp*xk*t_evap(ir2)/(xmh*mol(ir2))) / (xpi * dsite)

        iprod = react(i,4)
        DO mm = 1, npg
           iv1 = nspec + (ir1 - nsp_n1 - 1) * npg + mm
           iv2 = nspec + (ir2 - nsp_n1 - 1) * npg + mm

           tgr = tdust(mm,iopt)
           xldb_1 =  2.0_dp * xpi * SQRT(2.0_dp * xmh * mol(ir1) * xk * (t_diff(ir1) - tgr)) / xhp
           xldb_2 =  2.0_dp * xpi * SQRT(2.0_dp * xmh * mol(ir2) * xk * (t_diff(ir2) - tgr)) / xhp

           tm1_1 = nu0_1 * (EXP(-t_diff(ir1) / tgr) + 1.0_dp / (1.0_dp + t_diff(ir1)**2 &
                 * SINH(xldb_1 * dsite)**2 / (4.0_dp * tgr * (t_diff(ir1) - tgr))))
           tm1_2 = nu0_2 * (EXP(-t_diff(ir2) / tgr) + 1.0_dp / (1.0_dp + t_diff(ir2)**2 &
                 * SINH(xldb_2 * dsite)**2 / (4.0_dp * tgr * (t_diff(ir2) - tgr))))

           ! This should be 1 / (8 * pi) (see notes)
           rate = gamm(i) * (tm1_1 + tm1_2) * dsite**2 / (8.0_dp * xpi * adust(mm)**2)
           rate = rate * EXP(-bet(i)/tgr)

           kkk = indy(iprod) + 1
           indy(iprod) = kkk
           xmatk(iprod)%fdt(kkk)%k = rate * agrnrm * wwmg(mm) * densh(iopt)

           kkk = indy(iv1) + 1
           indy(iv1) = kkk
           xmatk(iv1)%fdt(kkk)%k = - rate

           kkk = indy(iv2) + 1
           indy(iv2) = kkk
           xmatk(iv2)%fdt(kkk)%k = - rate
        ENDDO
     ENDIF
  ENDDO

!-------------------------------------------------------------------------
! (12) photoreactions on grain surface (ngphot)
!-------------------------------------------------------------------------
!  11 Feb 2008: not revised yet. Do not use

  ideb = ifin + 1
  ifin = ifin + ngphot
  DO i = ideb, ifin
     rate1 = gamm(i) * EXP(-bet(i)*av) * radm
     rate2 = gamm(i) * EXP(-bet(i)*avr) * radp
     rate = rate1 + rate2

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (13) adsorption on to grains (ngads)
!-------------------------------------------------------------------------

! Old formulation
! IF (i_hgr /= 0) THEN
!    CALL STICKING(iopt, stickH)
!    gamm(iadhgr) = stickH
! ENDIF
! IF (i_d /= 0) THEN
!    gamm(iaddgr) = stickH
! ENDIF

  ideb = ifin + 1
  ifin = ifin + ngads
  DO i = ideb, ifin
     ir1 = react(i,1)
     ! Same sticking probability for all species (not only for H and D)
     CALL STICKING(iopt, stickH)
     gamm(i) = stickH

     IF (itype(i) == 13) THEN

        rate = gamm(i) * vmaxw * SQRT(ttry / mol(ir1)) * s_gr_t * densh(iopt) / 4.0_dp

        iprod = react(i,4)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate

        kkk = indy(ir1) + 1
        indy(ir1) = kkk
        xmatk(ir1)%fdt(kkk)%k = - rate

     ELSE IF (itype(i) == 113) THEN

        rate = gamm(i) * vmaxw * SQRT(ttry / mol(ir1)) * s_gr_t * densh(iopt) / 4.0_dp

        kkk = indy(ir1) + 1
        indy(ir1) = kkk
        xmatk(ir1)%fdt(kkk)%k = - rate

        r_rej  = gamm(i) * vmaxw * SQRT(ttry / mol(ir1)) * dsite*dsite / 4.0_dp
        r_ad_i = gamm(i) * vmaxw * SQRT(ttry / mol(ir1)) * xpi

        iprod = react(i,4)
        DO mm = 1, npg
           ivp = nspec + (iprod - nsp_n1 - 1) * npg + mm
           kkk = indy(ivp) + 1
           indy(ivp) = kkk
           xmatk(ivp)%fdt(kkk)%k = r_ad_i * adust(mm)**2
           kkk = indy(ivp) + 1
           indy(ivp) = kkk
           xmatk(ivp)%fdt(kkk)%k = - r_rej

           kkk = indy(ir1) + 1
           indy(ir1) = kkk
           xmatk(ir1)%fdt(kkk)%k = r_rej * agrnrm * densh(iopt) * wwmg(mm)
        ENDDO
     ENDIF
  ENDDO

!-------------------------------------------------------------------------
! (14) neutralization (ngneut)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + ngneut

  IF (ngneut > 0) THEN
     DO i = ideb, ifin
        ir1 = react(i,1)
        rxigr(ir1,iopt) = 0.0_dp
     ENDDO
     relgr(:,iopt) = 0.0_dp
     grcharge(iopt) = 0.0_dp

     DO mm = 1, npg
        CALL FBTH(ttry, bilth, iopt, itn, mm)
        j = 0
        DO i = ideb, ifin
           ir1 = react(i,1)
           j = j + 1
           rxigr(ir1,iopt) = rxigr(ir1,iopt) + agrnrm * xkneut(j) * wwmg(mm)
        ENDDO
        grcharge(iopt) = grcharge(iopt) + agrnrm * zmoy(mm,iopt) * wwmg(mm)
        relgr(1,iopt) = relgr(1,iopt) + agrnrm * xkphel(1) * wwmg(mm)
        relgr(2,iopt) = relgr(2,iopt) + agrnrm * xkphel(2) * wwmg(mm)
     ENDDO
     grcharge(iopt) = grcharge(iopt) * densh(iopt)
!    PRINT *, " Grain charge:",  grcharge(iopt), ", Electron density:", abelec
  ENDIF

  DO i = ideb, ifin
     ir1 = react(i,1)
     rate = rxigr(ir1,iopt)

     iprod = react(i,4)
     kkk = indy(iprod) + 1
     indy(iprod) = kkk
     xmatk(iprod)%fdt(kkk)%k = rate

     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (15) explosive desorption from grains (nggdes)
!    ref: Shalabiea & Greenberg 1994
!-------------------------------------------------------------------------
!  11 Feb 2008: not revised yet. Do not use

  ideb = ifin + 1
  ifin = ifin + nggdes
  DO i = ideb, ifin
     ir1 = react(i,1)
     rate1 = gamm(i) * rdeso * densh(iopt)
     rate2 = gamm(i) * 3.0d4 * zeta * (signgr / 7.0e-21_dp)
     rate = rate1 + rate2

     IF (itype(i) == 15) THEN
        iprod = react(i,4)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate

        kkk = indy(ir1) + 1
        indy(ir1) = kkk
        xmatk(ir1)%fdt(kkk)%k = - rate
     ENDIF
  ENDDO

!-------------------------------------------------------------------------
! (16) cosmic ray induced desorption from grains (ncrdes)
!      (sans les photons secondaires)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + ncrdes
  DO i = ideb, ifin
     rate = gamm(i) * zeta * dsite**2 / 4.0_dp
     ir1 = react(i,1)
     iprod = react(i,4)

     IF (itype(i) == 16) THEN

        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate

        kkk = indy(ir1) + 1
        indy(ir1) = kkk
        xmatk(ir1)%fdt(kkk)%k = - rate

     ELSE IF (itype(i) == 116) THEN

        DO mm = 1, npg
           ivp = nspec + (ir1 - nsp_n1 - 1) * npg + mm
           kkk = indy(ivp) + 1
           indy(ivp) = kkk
           xmatk(ivp)%fdt(kkk)%k = - rate

           kkk = indy(iprod) + 1
           indy(iprod) = kkk
           xmatk(iprod)%fdt(kkk)%k = rate * agrnrm * densh(iopt) * wwmg(mm)
        ENDDO
     ENDIF
  ENDDO

!-------------------------------------------------------------------------
! (17) photodesorption from grains (nphdes)
!-------------------------------------------------------------------------
!  flux de photons secondaires due aux rayons cosmiques:
!     1E3 cm-2 s-1 (Roberge, in Dalgarno's book)
!     fphsec = 1.0e3_dp * (zeta / 1.0e-17_dp) = 1.0e3_dp * fmrc

!  flux de Mathis et al de 911 a 2400 A (avec F(l) = 5e-17 erg cm-3 A-1)
!  flupho = 1e8 photon cm-2 s-1      (Tielens & Hagen, 1982, A&A 114, 245)
!     flupho = 1.0d8
!     flupho = flupho * RAD * EXP(-2.0_dp * AV)

  ideb = ifin + 1
  ifin = ifin + nphdes
  DO i = ideb, ifin
     rate = gamm(i) * (fphsec * frach2 + fluph(iopt)) * dsite**2 / 4.0_dp
     ir1 = react(i,1)

     IF (itype(i) == 17) THEN

        kkk = indy(ir1) + 1
        indy(ir1) = kkk
        xmatk(ir1)%fdt(kkk)%k = - rate

        ik = react(i,0)
        DO j = 4, ik+1
           iprod = react(i,j)
           kkk = indy(iprod) + 1
           indy(iprod) = kkk
           xmatk(iprod)%fdt(kkk)%k = rate
        ENDDO

     ELSE IF (itype(i) == 117) THEN

        DO mm = 1, npg
           ivp = nspec + (ir1 - nsp_n1 - 1) * npg + mm
           kkk = indy(ivp) + 1
           indy(ivp) = kkk
           xmatk(ivp)%fdt(kkk)%k = - rate

           ik = react(i,0)
           DO j = 4, ik+1
              iprod = react(i,j)
              kkk = indy(iprod) + 1
              indy(iprod) = kkk
              xmatk(iprod)%fdt(kkk)%k = rate * agrnrm * densh(iopt) * wwmg(mm)
           ENDDO
        ENDDO
     ENDIF
  ENDDO

!-------------------------------------------------------------------------
! (18) evaporation from grains (ngevap)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + ngevap
  DO i = ideb, ifin
     ir1 = react(i,1)
     iprod = react(i,4)
     nu0 = SQRT(2.0_dp*xk*t_evap(ir1)/(xmh*mol(ir1))) / (xpi * dsite)

     IF (itype(i) == 18) THEN

        summ = 0.0_dp
        DO mm = 1, npg
!          tgr = MIN(tdust(mm,iopt), 25.0_dp)
           tgr = tdust(mm,iopt)
           summ = summ + wwmg(mm) * adust(mm)**2 * EXP(-t_evap(ir1) / tgr)
        ENDDO
!       Alternative expression (Evelyne) => no divide by zero for rmin = rmax
        rate = nu0 * summ * 4.0_dp * xpi * agrnrm / s_gr_t

        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate

        kkk = indy(ir1) + 1
        indy(ir1) = kkk
        xmatk(ir1)%fdt(kkk)%k = - rate

     ELSE IF (itype(i) == 118) THEN

        DO mm = 1, npg
           ! Remove limitation in tgr. This was used when only LH was
           ! implemented
           ! tgr = MIN(tdust(mm,iopt), 25.0_dp)
           tgr = tdust(mm,iopt)
           rate = nu0 * EXP(-t_evap(ir1) / tgr)
           ivp = nspec + (ir1 - nsp_n1 - 1) * npg + mm
           kkk = indy(ivp) + 1
           indy(ivp) = kkk
           xmatk(ivp)%fdt(kkk)%k = - rate

           kkk = indy(iprod) + 1
           indy(iprod) = kkk
           xmatk(iprod)%fdt(kkk)%k = rate * agrnrm * densh(iopt) * wwmg(mm)
        ENDDO
     ENDIF
  ENDDO

!-------------------------------------------------------------------------
! (19) Chemisorption on grains (ngchs)
!-------------------------------------------------------------------------

  ideb = ifin + 1
  ifin = ifin + ngchs

  DO i = ideb, ifin
     ! stick_ER = MIN(1.0_dp, SQRT(300.0_dp/ttry))
     ! stick_ER = 1.0_dp / (1.0_dp + 1.0e-4_dp * ttry**1.5_dp)
     stick_ER = 1.0_dp / (1.0_dp + (ttry/T2_stick_ER)**ex_stick_ER)
     ir1 = react(i,1)

     expon = EXP(-bet(i)/ttry)
     rate = stick_ER * gamm(i) * vmaxw * SQRT(ttry / mol(ir1)) * s_gr_t * densh(iopt) * expon / 4.0_dp
     r_rej = stick_ER * gamm(i) * vmaxw * SQRT(ttry / mol(ir1)) * dsite**2 *expon / 4.0_dp

     iprod = react(i,4)
     kkk = indy(iprod) + 1
     indy(iprod) = kkk
     xmatk(iprod)%fdt(kkk)%k = rate

     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate

     ! Rejection
     kkk = indy(iprod) + 1
     indy(iprod) = kkk
     xmatk(iprod)%fdt(kkk)%k = - r_rej
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = r_rej

     ! That value is known analytically if there are only 2 reactions
     ab(i_hgrc) =  (stick_ER * expon / (1.0_dp + stick_ER * expon)) * s_gr_t * densh(iopt) / dsite**2
  ENDDO

!-------------------------------------------------------------------------
! (20) Eley-Rideal process (ngelri)   (cm3 s-1)
!-------------------------------------------------------------------------
! Note : the total number of sites is: s_gr_t * densh(iopt) / dsite**2

  ideb = ifin + 1
  ifin = ifin + ngelri
  DO i = ideb, ifin

     rate = gamm(i) * vmaxw * SQRT(ttry / mol(ir1)) * dsite**2 / 4.0_dp

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate

     ir2 = react(i,2)
     kkk = indy(ir2) + 1
     indy(ir2) = kkk
     xmatk(ir2)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------

  ! Compute abundance of electrons ABELEC from charge conservation
  ! JLB - 15 X 2010: electron abundance must take the grain charge into account
  abelec = grcharge(iopt)
  DO i = nsp_ne+1, nspec-1
     abelec = abelec + ab(i) * ael(i,natom)
  ENDDO
  ab(nspec) = abelec

END SUBROUTINE CHIMIJ

!%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE MNEWT (iopt)
!%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA

!  See numerical recipe p 272.

   IMPLICIT NONE

   INTEGER, INTENT (IN)                         :: iopt

!  Numerical PARAMETERs (unused outside MNEWT, used to be defined as PARAMETER)

   INTEGER         :: ntr1 = 10         ! Max number of global iteration in N-R algorithm
                                        !     1 global iter = change conservation equation
   INTEGER         :: ntr2 = 3          ! Max number of iteration with the same
                                        !     conservation equation
   REAL (KIND=dp)  :: tolx = 1.0e-6_dp  ! Total relative error for chemistry

   REAL (KIND=dp), ALLOCATABLE, DIMENSION (:,:) :: am, bbmn
   REAL (KIND=dp), DIMENSION (nes)              :: abold, abt
   REAL (KIND=dp), DIMENSION(0:natom)           :: ratiab
   REAL (KIND=dp)                               :: ratlim = 1.0_dp
   INTEGER, DIMENSION (nes)                     :: indx
   REAL (KIND=dp), DIMENSION (nqm)              :: ab1, ab2, ab3
   INTEGER, DIMENSION (nqm)                     :: j1, j2, j3
   INTEGER                                      :: i, ii, j, k, l, k1, k2
   INTEGER                                      :: kspec, nrxi
   REAL (KIND=dp)                               :: damp, duma, errc
   REAL (KIND=dp)                               :: bad, dum, tt1, top1, top2, ddumm
   REAL (KIND=dp)                               :: fabric, destru, des_f, toto
   INTEGER                                      :: nrhs = 1
   INTEGER                                      :: info, ici1, ici2, jjj, icc

   ALLOCATE (am(n_var,n_var), bbmn(n_var,1))

   abold(1:n_var) = ab(1:n_var)

   abt(:) = 0.0_dp
   DO i = 1, n_var
      duma = densh(iopt)
      DO j = 1, natom-1
         kspec = ael(i,j)
         IF (kspec == 0) THEN
            CYCLE
         ELSE
            duma = MIN(duma,densh(iopt)*abtot(j)/kspec)
         ENDIF
      ENDDO
      abt(i) = duma
   ENDDO

   DO k1 = 1, ntr1

   damp = 0.7_dp

   DO k2 = 1, ntr2

      ! usrfun computes the Jacobian and the function to nullify
      CALL USRFUN (iopt, am, bbmn)

      abold(1:n_var) = ab(1:n_var)

      ! Switch to LAPACK
      CALL DGESV (n_var, nrhs, am, n_var, indx, bbmn, n_var, info)

      ! We limit variation to gas density, but ALL variables must be rescaled.
      dum = SUM(ABS(bbmn(1:nspec,1)))
      IF (dum > densh(iopt)) THEN
         bbmn(1:n_var,1) = bbmn(1:n_var,1) * densh(iopt) / dum
      ENDIF

      errx = 0.0_dp

      bad = 0.0_dp
      mauvai = 0
      DO i = 1, nspec
         IF (ab(i) > MAX(istiny,-bbmn(i,1)) .OR. bbmn(i,1) > 0.0_dp) THEN
            IF (ab(i) > 1.0e45_dp*istiny) THEN
               tt1 = ABS(bbmn(i,1)) / (abold(i) + istiny)
               errx = errx + tt1
               IF (bad < tt1) THEN
                  bad = tt1
                  mauvai = i
               ENDIF
            ENDIF
            IF (lcon(i) == 0) THEN
               ab(i) = ab(i) + bbmn(i,1) * damp
            ELSE
               ab(i) = ab(i) + bbmn(i,1)
            ENDIF
            IF (ab(i) > abt(i)) THEN
               ab(i) = (abt(i) + abold(i)) * 0.5_dp
            ELSE IF (ab(i) < 0.0_dp) THEN
               ab(i) = abold(i) / 10.0_dp
            ENDIF
         ELSE
            ab(i) = istiny
         ENDIF
      ENDDO
      DO i = nspec+1, n_var
         tt1 = ABS(bbmn(i,1)) / (abold(i) + istiny)
         errx = errx + tt1
         ab(i) = ab(i) + bbmn(i,1)
         IF (ab(i) < 0.0_dp) THEN
            ab(i) = abold(i) / 10.0_dp
         ENDIF
      ENDDO

      IF (MOD(k2,2) == 1) THEN
         DO i = 1, n_var
            ab(i) = (ab(i) + abold(i)) * 0.5_dp
         ENDDO
      ENDIF
      ! *** end patch ***

      ! JLB - 15 X 2010 : add grain charge to electronic balance
      abelec = SUM(ab(nsp_ne+1:nsp_ne+nsp_ip+nsp_im) &
             * ael(nsp_ne+1:nsp_ne+nsp_ip+nsp_im,natom)) &
             + grcharge(iopt)
      errx = errx / damp
      errc = 0.0_dp
      DO i = 1, n_var
         IF (i > nsp_n1 .AND. i < nspec) THEN
            CYCLE
         ENDIF
         fabric = 0.0_dp
         destru = 0.0_dp
         des_f = 0.0_dp
         nrxi = xmatk(i)%nbt
         DO k = 1, nrxi
            j1(k) = xmatk(i)%fdt(k)%r1
            j2(k) = xmatk(i)%fdt(k)%r2
            j3(k) = xmatk(i)%fdt(k)%r3
            IF (j1(k) > n_var) THEN
               ab1(k) = 1.0_dp
            ELSE
               ab1(k) = ab(j1(k))
            ENDIF
            IF (j2(k) > n_var) THEN
               ab2(k) = 1.0_dp
            ELSE
               ab2(k) = ab(j2(k))
            ENDIF
            IF (j3(k) > n_var) THEN
               ab3(k) = 1.0_dp
            ELSE
               ab3(k) = ab(j3(k))
            ENDIF
            IF (xmatk(i)%fdt(k)%k > 0.0_dp) THEN
               fabric = fabric + xmatk(i)%fdt(k)%k * ab1(k) * ab2(k) * ab3(k)
            ELSE
               toto = xmatk(i)%fdt(k)%k * ab1(k) * ab2(k) * ab3(k)
               destru = destru + toto
               des_f = des_f + toto / ab(i)
            ENDIF
         ENDDO
         IF (ab(i) > 1.0e-20_dp) THEN
            errc = errc + ABS(fabric+destru)/ABS(fabric-destru)
         ELSE
            ab(i) = - fabric / des_f
            IF (ab(i) > abt(i)) THEN
               ab(i) = (abt(i) + abold(i)) * 0.5_dp
            ENDIF
         ENDIF
      ENDDO

      IF (errx <= tolx .AND. MOD(k2,2) == 0 .AND.  errc < 1.0e-3_dp) THEN
         CALL VERUSF (iopt, bbmn)
         DEALLOCATE (am, bbmn)
         RETURN
      ENDIF
   ENDDO

   ! Modify conservation equation in case of bad convergence
   DO i = 1, natom-1
      top1 = 0.0_dp
      top2 = 0.0_dp
      icx(i,1) = 0
      icx(i,2) = 0

      DO l = 1, nspec-1
         dum = ael(l,i)
         IF (dum == 0.0_dp) CYCLE
         IF (lcon(l) == i) THEN
            lcon(l) = 0
         ENDIF
         ddumm = ab(l) * dum
         IF (ddumm > top1) THEN
            top2 = top1
            top1 = ddumm
            icx(i,2) = icx(i,1)
            icx(i,1) = l
         ELSE IF (ddumm > top2) THEN
            top2 = ddumm
            icx(i,2) = l
         ENDIF
      ENDDO
      IF (top1 /= 0.0_dp) THEN
         ratiab(i) = top2 / top1
      ENDIF
   ENDDO

   DO i = 1, natom-1
      ici1 = icx(i,1)
      ici2 = icx(i,2)
      IF (ici1 == 0) CYCLE

      ! Patch recombination
      IF (ratiab(i) > ratlim) THEN
         IF (ael(ici1,natom) == 1 .AND. ael(ici2,natom) == 0) THEN
            jjj = ici1
            ici1 = ici2
            ici2 = jjj
         ENDIF
      ENDIF

      icc = 0
      DO ii = 1, i-1
         IF (lcon(ici1) == ii) icc = ii
      ENDDO

      IF (icc == 0) THEN
         lcon(ici1) = i
      ELSE
         IF (ratiab(i) > ratiab(icc)) THEN
            lcon(ici2) = i
         ELSE
            lcon(ici1) = i
            lcon(icx(icc,2)) = icc
         ENDIF
      ENDIF
   ENDDO

   ENDDO

   k2 = k2 - 1
!  ratlim = 0.35_dp
   ratlim = 0.20_dp

   DEALLOCATE (am, bbmn)

END SUBROUTINE MNEWT

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE USRFUN (iopt, am, bbmn)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA

!  Computes the FUNCTION (-beta) to nullify and its derivatives
!  (alpha) at point ab, using conservation equations

   IMPLICIT NONE

   INTEGER, INTENT (IN)                          :: iopt

   REAL (KIND=dp), INTENT (OUT), DIMENSION (:,:) :: am
   REAL (KIND=dp), INTENT (OUT), DIMENSION (:,:) :: bbmn

   REAL (KIND=dp), DIMENSION (nqm)               :: ab1, ab2, ab3
   INTEGER, DIMENSION (nqm)                      :: j1, j2, j3
   REAL (KIND=dp), DIMENSION (0:nes)             :: dumb

   INTEGER                                       :: i, j, k, nrxi, kspec
   INTEGER                                       :: jatom, mm, ivp
   REAL (KIND=dp)                                :: duma

   ! Chemical equations include only polynomials in the abundances
   ! If lcon(i) = 0 : this is an ordinary equation
   !              d[Xi] / dt = 0
   ! If lcon(i) = j /= 0 : this is a conservation equation of atom j
   ! Monolayer on grain species and electrons are done separately
   am = 0.0_dp
   bbmn = 0.0_dp
   DO i = 1, nsp_n1
      nrxi = xmatk(i)%nbt
      jatom = lcon(i)

      IF (jatom == 0) THEN

         DO k = 1, nrxi
            j1(k) = xmatk(i)%fdt(k)%r1
            j2(k) = xmatk(i)%fdt(k)%r2
            j3(k) = xmatk(i)%fdt(k)%r3
            IF (j1(k) > n_var) THEN
               ab1(k) = 1.0_dp
            ELSE
               ab1(k) = ab(j1(k))
            ENDIF
            IF (j2(k) > n_var) THEN
               ab2(k) = 1.0_dp
            ELSE
               ab2(k) = ab(j2(k))
            ENDIF
            IF (j3(k) > n_var) THEN
               ab3(k) = 1.0_dp
            ELSE
               ab3(k) = ab(j3(k))
            ENDIF
         ENDDO

         ! Computes residuals
         duma = SUM(xmatk(i)%fdt(1:nrxi)%k * ab1(1:nrxi) * ab2(1:nrxi) * ab3(1:nrxi))
         bbmn(i,1) = - duma

         ! Computes partial derivatives
         dumb(1:n_var) = 0.0_dp

         DO k = 1, nrxi
            dumb(j1(k)) = dumb(j1(k)) + xmatk(i)%fdt(k)%k * ab2(k) * ab3(k)
            dumb(j2(k)) = dumb(j2(k)) + xmatk(i)%fdt(k)%k * ab1(k) * ab3(k)
            dumb(j3(k)) = dumb(j3(k)) + xmatk(i)%fdt(k)%k * ab1(k) * ab2(k)
         ENDDO

         am(i,1:n_var) = dumb(1:n_var)

      ELSE

         duma = abtot(jatom) * densh(iopt)
         DO j = 1, nspec-1
            kspec = ael(j,jatom)
            duma = duma - kspec * ab(j)
            am(i,j) = kspec
         ENDDO

         bbmn(i,1) = duma
         am(i,nspec) = 0.0_dp
      ENDIF
   ENDDO

   ! Here we compute the abundance of species on grains that require
   !      detailed size by size evaluation
   DO i = nsp_n1+1, nsp_n1+nsp_g1
      duma = ab(i)
      DO mm = 1, npg
         ivp = nspec + (i - nsp_n1 - 1) * npg + mm
         duma = duma - agrnrm * densh(iopt) * wwmg(mm) * ab(ivp)
         am(i,ivp) = agrnrm * densh(iopt) * wwmg(mm)
      ENDDO
      am(i,i) = - 1.0_dp
      bbmn(i,1) = duma
   ENDDO

   ! Here we have the equation for electrons
   duma = ab(nspec)
   DO j = 1, nspec-1
      kspec = ael(j,natom)
      duma = duma - kspec * ab(j)
      am(nspec,j) = kspec
   ENDDO

   bbmn(nspec,1) = duma
   am(nspec,nspec) = - 1.0_dp

   ! Here we have the equations for the variables on grains
   DO i = nspec+1, n_var
      nrxi = xmatk(i)%nbt

      DO k = 1, nrxi
         j1(k) = xmatk(i)%fdt(k)%r1
         j2(k) = xmatk(i)%fdt(k)%r2
         j3(k) = xmatk(i)%fdt(k)%r3
         IF (j1(k) > n_var) THEN
            ab1(k) = 1.0_dp
         ELSE
            ab1(k) = ab(j1(k))
         ENDIF
         IF (j2(k) > n_var) THEN
            ab2(k) = 1.0_dp
         ELSE
            ab2(k) = ab(j2(k))
         ENDIF
         IF (j3(k) > n_var) THEN
            ab3(k) = 1.0_dp
         ELSE
            ab3(k) = ab(j3(k))
         ENDIF
      ENDDO

      duma = SUM(xmatk(i)%fdt(1:nrxi)%k * ab1(1:nrxi) * ab2(1:nrxi) * ab3(1:nrxi))
      bbmn(i,1) = - duma

      dumb(1:n_var) = 0.0_dp
      DO k = 1, nrxi
         dumb(j1(k)) = dumb(j1(k)) + xmatk(i)%fdt(k)%k * ab2(k) * ab3(k)
         dumb(j2(k)) = dumb(j2(k)) + xmatk(i)%fdt(k)%k * ab1(k) * ab3(k)
         dumb(j3(k)) = dumb(j3(k)) + xmatk(i)%fdt(k)%k * ab1(k) * ab2(k)
      ENDDO

      am(i,1:n_var) = dumb(1:n_var)
   ENDDO

END SUBROUTINE USRFUN

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE VERUSF (iopt, bbmn)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA

!  Verify chemical balance, using result of MNEWT
!  WITHOUT using conservation equations

   IMPLICIT NONE

   INTEGER, INTENT (IN)                             :: iopt
   REAL (KIND=dp), INTENT (OUT), DIMENSION(:,:)     :: bbmn

   REAL (KIND=dp), DIMENSION (nqm)                  :: ab1, ab2, ab3
   INTEGER, DIMENSION (nqm)                         :: j1, j2, j3
   INTEGER                                          :: i, j, k, mm
   INTEGER                                          :: ivp, nrxi
   INTEGER                                          :: kspec
   REAL (KIND=dp)                                   :: duma

   bbmn = 0.0_dp
   DO i = 1, nsp_n1
      nrxi = xmatk(i)%nbt
      DO k = 1, nrxi
         j1(k) = xmatk(i)%fdt(k)%r1
         j2(k) = xmatk(i)%fdt(k)%r2
         j3(k) = xmatk(i)%fdt(k)%r3
         IF (j1(k) > n_var) THEN
            ab1(k) = 1.0_dp
         ELSE
            ab1(k) = ab(j1(k))
         ENDIF
         IF (j2(k) > n_var) THEN
            ab2(k) = 1.0_dp
         ELSE
            ab2(k) = ab(j2(k))
         ENDIF
         IF (j3(k) > n_var) THEN
            ab3(k) = 1.0_dp
         ELSE
            ab3(k) = ab(j3(k))
         ENDIF
      ENDDO

      ! Computes residuals
      duma = SUM(xmatk(i)%fdt(1:nrxi)%k * ab1(1:nrxi) * ab2(1:nrxi) * ab3(1:nrxi))

      bbmn(i,1) = duma
      IF (ABS(duma) > 1.0e-5_dp) THEN
         PRINT *, i, speci(i), duma
      ENDIF
   ENDDO

   ! Here we compute the abundance of species on grains that require
   !      detailed size by size evaluation
   DO i = nsp_n1+1, nspec-1
      duma = ab(i)
      DO mm = 1, npg
         ivp = nspec + (i - nsp_n1 - 1) * npg + mm
         duma = duma - agrnrm * densh(iopt) * ab(ivp) * wwmg(mm)
      ENDDO
      bbmn(i,1) = duma
      IF (ABS(duma) > 1.0e-5_dp) THEN
         PRINT *, i, speci(i), duma
      ENDIF
   ENDDO

   ! Here we have the equation for electrons
   ! Note: this is just a conservation equation. Not a formation/destruction balance
   duma = ab(nspec)
   DO j = 1, nspec-1
      kspec = ael(j,natom)
      duma = duma - kspec * ab(j)
   ENDDO
   IF (ABS(duma) > 1.0e-5_dp) THEN
      PRINT *, nspec, speci(nspec), duma
   ENDIF

   bbmn(i,1) = duma

   ! Here we have the equations for the variables on grains
   DO i = nspec+1, n_var
      nrxi = xmatk(i)%nbt

      DO k = 1, nrxi
         j1(k) = xmatk(i)%fdt(k)%r1
         j2(k) = xmatk(i)%fdt(k)%r2
         j3(k) = xmatk(i)%fdt(k)%r3
         IF (j1(k) > n_var) THEN
            ab1(k) = 1.0_dp
         ELSE
            ab1(k) = ab(j1(k))
         ENDIF
         IF (j2(k) > n_var) THEN
            ab2(k) = 1.0_dp
         ELSE
            ab2(k) = ab(j2(k))
         ENDIF
         IF (j3(k) > n_var) THEN
            ab3(k) = 1.0_dp
         ELSE
            ab3(k) = ab(j3(k))
         ENDIF
      ENDDO

      duma = SUM(xmatk(i)%fdt(1:nrxi)%k * ab1(1:nrxi) * ab2(1:nrxi) * ab3(1:nrxi))
      bbmn(i,1) = - duma
      IF (ABS(duma) > 1.0e-5_dp) THEN
         PRINT *, i, speci(i), duma
      ENDIF
   ENDDO

END SUBROUTINE VERUSF

!----------------------------------------------------------------------

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE FBTH(temper, bilth, iopt, itn, mm)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PXDR_RF_TOOLS

   IMPLICIT NONE

!  Photoelectric heating and grain charge balance as a function of size
!  Modified from Bakes & Tielens, Ap J 427, 822 (1994)

  REAL (KIND=dp), INTENT (IN)                 :: temper
  INTEGER, INTENT (IN)                        :: iopt, itn
  INTEGER, INTENT (IN)                        :: mm
  REAL (KIND=dp), INTENT (OUT)                :: bilth

  ! Charges...
  INTEGER                                     :: izmin, izmax
  INTEGER                                     :: kzmin, kzmax
  REAL (KIND=dp)                              :: zmax
  REAL (KIND=dp)                              :: zzmoy
  INTEGER, DIMENSION(:), ALLOCATABLE          :: jzmin
  INTEGER, DIMENSION(:), ALLOCATABLE          :: jzmax

  ! Integrals of Q * U * (1 or l or l2)  over lambda
  REAL (KIND=dp)                              :: s_QU, s_QUl, s_QUl2

!  Gas grain interactions are:
!     xjpe : photoemission rate
!     xje  : electrons accretion rate
!     xji  : individual ions accretion rate
!     xjit : total ions accretion rate
!--------------------------------------------------------

  REAL (KIND=dp), DIMENSION (-izm0:izp0)      :: xjpe
  REAL (KIND=dp), DIMENSION (-izm0:izp0)      :: xje
  REAL (KIND=dp), DIMENSION (-izm0:izp0)      :: xjit

  ! ionisation fraction
  REAL (KIND=dp), DIMENSION (-izm0:izp0)      :: frac

  ! Heating and Cooling
  REAL (KIND=dp), DIMENSION (-izm0:izp0)      :: heat
  REAL (KIND=dp), DIMENSION (-izm0:izp0)      :: cool

  ! stic : electrons sticking coefficient
  REAL (KIND=dp)                              :: stic = 1.0_dp

  ! x_nc : Number of carbon atoms in the grain
  ! Dummy variables
  REAL (KIND=dp)                              :: tauph, cte, x_nc_t
  REAL (KIND=dp)                              :: x_ip, thet, xjtm, xjtp, xjt0
  REAL (KIND=dp)                              :: xltm, xltp, xlt0, ziz
  REAL (KIND=dp)                              :: sup, titi, toto, tata, xsum, tier, tempo
  REAL (KIND=dp)                              :: cte_H, cte_J, cte_IP

  REAL (KIND=dp)                              :: wl_cut_IP, IP_min
  INTEGER                                     :: I_cut_IP

  ! Dummy Indices
  INTEGER                                     :: i, j, iz, ndeb, nfin, ir1, izf, izd

  SAVE jzmin, jzmax

!------------------------------------------------------------------------

  tier = 1.0_dp / 3.0_dp
  IF ( .NOT. ALLOCATED(jzmin)) THEN
     ALLOCATE (jzmin(npg), jzmax(npg))
     jzmin = 0
     jzmax = 0
  ENDIF

  IP_min = xhp * clum * 1.0e8_dp / (rf_wl%Val(nwlg) * everg)

  ! Reduced PARAMETER (from Eq 9). With qi = +/- 1
  tauph = adust(mm) * xk * temper / xqe2

  ! Eq 9 (first part)
  cte = SQRT(8.0_dp * xk * temper * xpi) * adust(mm)**2
  x_nc_t = x_nc(mm)**tier

  ! Maximum Charges (Eq 26 et 27):
  IF (iopt == 0 .AND. itn == 1) THEN
     ! Charges can not be very negative at the edge...
     !      zmin = 0.4_dp * x_nc_t - 0.5_dp
     !      zmin =  MAX(-zmin,-90.0_dp*tauph)
     !      izmin = zmin
     izmin = -1

     zmax = (hiop/everg - 4.4_dp) * x_nc_t / 11.1_dp - 0.5_dp
     izmax = FLOOR(zmax)

     ! We limit the maximum possible extension
     izmin = MAX(izmin,-izm0)
     izmax = MIN(izmax,izp0)

     IF (iopt /= 0 .AND. jzmin(mm) < jzmax(mm)) THEN
        izmin = jzmin(mm) - 5
        izmax = jzmax(mm) + 5
     ELSE IF (iopt /= 0 .AND. mm > 1) THEN
        izmin = MIN(izmax-5,MAX(izmin,jzmin(mm-1)/2))
        izmax = MAX(izmin+5,MIN(izmax,jzmax(mm-1)+10))
     ENDIF

     IF (izmin >= izmax) THEN
        izmin = izmin - 2
        izmax = izmax + 2
     ENDIF

  ELSE

     izmin = jzmin(mm) - 2
     izmax = jzmax(mm) + 2

  ENDIF

  ! For neutralisation reactions
  ndeb = nh2for + ncrion + npsion + nasrad + nrest + nh2endo + nphot_f &
       + nphot_i + n3body + ncpart + ngsurf + ngphot + ngads + 1
  nfin = ndeb + ngneut - 1

  ! Loop over grain charge:
  ! Three parts: negatives, neutrals, positives
  cte_J = x_nc(mm) * 0.14_dp * bt_fy(mm) * 1.0e-8_dp / xhp
  cte_H = x_nc(mm) * 0.14_dp * bt_fy(mm) * 0.5_dp * clum

  izf = MIN(izmax,-1)

  DO iz = izmin, izf
     ! Ionisation potentiel (en eV) (Eq 25)
     ! 9 III 2012 - JLB - Change IP threshold of negative ions to 0.6 eV
     ! x_ip = MAX(IP_min,MIN(4.4_dp + (iz + 0.5_dp) * 11.1_dp / x_nc_t, hiop/everg))
     x_ip = MAX(IP_min,MIN(0.6_dp + (iz + 0.5_dp) * 11.1_dp / x_nc_t, hiop/everg))
     cte_IP = x_ip * everg * 1.0e-8_dp / (xhp * clum)
     wl_cut_IP = 1.0_dp / cte_IP
     CALL RF_LOCATE (rf_wl, wl_cut_IP, I_cut_IP)

     s_QU   = si_QU(mm,I_cut_IP)
     s_QUl  = si_QUl(mm,I_cut_IP)
     s_QUl2 = si_QUl2(mm,I_cut_IP)

     ! Ejection of one photo-electron (Eq 20):
     xjpe(iz) = cte_J * (s_QUl - cte_IP * s_QUl2)

     ! photo-electric heating ( Eq 30):
     heat(iz) = cte_H * (s_QU - 2.0_dp * cte_IP * s_QUl + cte_IP*cte_IP * s_QUl2)

     ! Collisions with gas phase particules. Eq 13:
     ziz = -iz
     thet = ziz / (1.0_dp + 1.0_dp/SQRT(ziz))

     ! Eq 11:
     xjtm = (1.0_dp - iz / tauph) * (1.0_dp + SQRT(2.0_dp/(tauph - 2.0_dp * iz)))

     ! Eq 12:
     xjtp = EXP(-thet / tauph) * (1.0_dp + 1.0_dp / SQRT(4.0_dp * tauph - 3.0_dp * iz))**2

     ! Eq 33:
     xltm = (2.0_dp - iz / tauph) * (1.0_dp + SQRT(1.0_dp/(tauph - iz)))

     ! Eq 34:
     xltp = EXP(-thet / tauph) * (2.0_dp - iz / tauph) &
          * (1.0_dp + 1.0_dp / SQRT(3.0_dp /(2.0_dp*tauph) - 3.0_dp * iz))

     ! Eq 9 (continued) et 31:
     xje(iz) = cte * stic * ab(nspec) * xjtp / SQRT(xme)
     titi = 0.0_dp
     toto = cte * xk * temper * ab(nspec) * xltp / SQRT(xme)
     tata = toto

     DO i = ndeb, nfin
        ir1 = react(i,1)
        xji(ir1,iz) = cte * gamm(i) * ab(ir1) * xjtm / SQRT(xmh*mol(ir1))
        titi = titi + xji(ir1,iz)
        tata = tata + cte * gamm(i) * xk * temper * ab(ir1) * xltm / SQRT(xmh*mol(ir1))
     ENDDO
     xjit(iz) = titi

     ! If positive ions do not stick on grains, they do not contribute to cooling
     ! Include only electrons contribution
!    cool(iz) = tata
     cool(iz) = toto
  ENDDO

  IF (izmin <= 0 .AND. izmax >= 0) THEN

     iz = 0

     x_ip = MAX(IP_min,MIN(4.4_dp + (iz + 0.5_dp) * 11.1_dp / x_nc_t, hiop/everg))
     cte_IP = x_ip * everg * 1.0e-8_dp / (xhp * clum)
     wl_cut_IP = 1.0_dp / cte_IP
     CALL RF_LOCATE (rf_wl, wl_cut_IP, I_cut_IP)

     s_QU   = si_QU(mm,I_cut_IP)
     s_QUl  = si_QUl(mm,I_cut_IP)
     s_QUl2 = si_QUl2(mm,I_cut_IP)

     xjpe(0) = cte_J * (s_QUl - cte_IP * s_QUl2)
     heat(0) = cte_H * (s_QU - 2.0_dp * cte_IP * s_QUl + cte_IP*cte_IP * s_QUl2)

     xjt0 = 1.0_dp + SQRT(xpi / (2.0_dp * tauph))

     xlt0 = 2.0_dp + 3.0_dp * SQRT(xpi/(2.0_dp*tauph)) /2.0_dp

     xje(0) = cte * ab(nspec) * xjt0 / SQRT(xme)
     titi = 0.0_dp
     toto = cte * xk * temper * ab(nspec) * xlt0 / SQRT(xme)
     tata = toto

     DO i = ndeb, nfin
        ir1 = react(i,1)
        xji(ir1,0) = cte * gamm(i) * ab(ir1) * xjt0 / SQRT(xmh*mol(ir1))
        titi = titi+ xji(ir1,0)
        tata = tata + cte * gamm(i)* xk * temper * xlt0 * ab(ir1) / SQRT(xmh*mol(ir1))
     ENDDO
     xjit(0) = titi
     ! If positive ions do not stick on grains, they do not contribute to cooling
!    cool(0) = tata
     cool(0) = toto

  ENDIF

  izd = max (izmin,1)

  DO iz = izd, izmax
     ! Ionisation potentiel (in eV) (Eq 25)
     x_ip = MAX(IP_min,MIN(4.4_dp + (iz + 0.5_dp) * 11.1_dp / x_nc_t, hiop/everg))
     cte_IP = x_ip * everg * 1.0e-8_dp / (xhp * clum)
     wl_cut_IP = 1.0_dp / cte_IP
     CALL RF_LOCATE (rf_wl, wl_cut_IP, I_cut_IP)

     s_QU   = si_QU(mm,I_cut_IP)
     s_QUl  = si_QUl(mm,I_cut_IP)
     s_QUl2 = si_QUl2(mm,I_cut_IP)

     xjpe(iz) = cte_J * (s_QUl - cte_IP * s_QUl2)
     heat(iz) = cte_H * (s_QU - 2.0_dp * cte_IP * s_QUl + cte_IP*cte_IP * s_QUl2)

     ! Eq 13:
     ziz = iz
     thet = ziz / (1.0_dp + 1.0_dp/SQRT(ziz))

     ! Eq 11:
     xjtm = (1.0_dp + iz / tauph) * (1.0_dp + SQRT(2.0_dp/(tauph + 2.0_dp * iz)))

     ! Eq 12:
     xjtp = EXP(-thet / tauph) * (1.0_dp + 1.0_dp / SQRT(4.0_dp * tauph + 3.0_dp * iz))**2

     ! Eq 33:
     xltm = (2.0_dp + iz / tauph) * (1.0_dp + SQRT(1.0_dp/(tauph + iz)))

     ! Eq 34:
     xltp = EXP(-thet / tauph) * (2.0_dp + iz / tauph) &
          * (1.0_dp + 1.0_dp / SQRT(3.0_dp /(2.0_dp*tauph) + 3.0_dp * iz))

     ! Eq 9 (continued) et 31:
     xje(iz) = cte * ab(nspec) * xjtm / SQRT(xme)
     titi = 0.0_dp
     toto = cte * xk * temper * ab(nspec) * xltm / SQRT(xme)
     tata = toto

     DO i = ndeb, nfin
        ir1 = react(i,1)
        xji(ir1,iz) = cte * gamm(i) * ab(ir1) * xjtp / SQRT(xmh*mol(ir1))
        titi = titi + xji(ir1,iz)
        tata = tata + cte * gamm(i) * xk * temper * ab(ir1) * xltp / SQRT(xmh*mol(ir1))
     ENDDO
     xjit(iz) = titi
     ! If positive ions do not stick on grains, they do not contribute to cooling
!    cool(iz) = tata
     cool(iz) = toto
  ENDDO

  ! Ionisation fraction :
  sup = 0.0_dp

  IF (izmin >= 0) THEN
     frac(izmin) = 0.0_dp

     toto = 0.0_dp
     DO iz = izmin+1, izmax
        tempo = (xjpe(iz-1) + xjit(iz-1)) / xje(iz)
        IF (tempo > 1.0e-40_dp) THEN
           toto = toto + LOG(tempo)
        ELSE
           toto = toto - 92.1_dp
        ENDIF
        frac(iz) = toto
        sup = MAX(sup,toto)
     ENDDO

  ELSE IF (izmax <= 0) THEN
     frac(izmax) = 0.0_dp

     toto = 0.0_dp
     DO iz = izf, izmin, -1
        tempo = xje(iz+1) / (xjpe(iz) + xjit(iz))
        IF (tempo > 1.0e-40_dp) THEN
           toto = toto + LOG(tempo)
        ELSE
           toto = toto - 92.1_dp
        ENDIF
        frac(iz) = toto
        sup = MAX(sup,toto)
     ENDDO

  ELSE
     frac(0) = 0.0_dp

     ! Eq 6:
     toto = 0.0_dp
     DO iz = izd, izmax
        tempo = (xjpe(iz-1) + xjit(iz-1)) / xje(iz)
        IF (tempo > 1.0e-40_dp) THEN
           toto = toto + LOG(tempo)
        ELSE
           toto = toto - 92.1_dp
        ENDIF
        frac(iz) = toto
        sup = MAX(sup,toto)
     ENDDO

     ! Eq 7:
     toto = 0.0_dp
     DO iz = izf, izmin, -1
        tempo = xje(iz+1) / (xjpe(iz) + xjit(iz))
        IF (tempo > 1.0e-40_dp) THEN
           toto = toto + LOG(tempo)
        ELSE
           toto = toto - 92.1_dp
        ENDIF
        frac(iz) = toto
        sup = MAX(sup,toto)
     ENDDO
  ENDIF

  ! Eq 8 et 3 (part of it):
  bilth = 0.0_dp
  xsum = 0.0_dp
  IF (ngneut > 0) THEN
     xkneut(1:ngneut) = 0.0_dp
  ENDIF
  DO iz = izmin, izmax
     frac(iz) = EXP(frac(iz) - sup)
     xsum = xsum + frac(iz)
  ENDDO

  IF (xsum == 0.0_dp) THEN
     PRINT *, ' Pb dans FBTH, xsum = 0.0'
  ENDIF

  kzmin = izmax
  kzmax = izmin
  zzmoy = 0
  xkphel(:) = 0.0_dp
  DO iz = izmin, izmax
     frac(iz) = frac(iz) / xsum
     IF (frac(iz) > 1.0e-5_dp) THEN
        kzmin = MIN(kzmin,iz)
        kzmax = MAX(kzmax,iz)
     ENDIF
     zzmoy = zzmoy + iz * frac(iz)
     bilth = bilth + (heat(iz) - cool(iz)) * frac(iz)
     xkphel(1) = xkphel(1) + xjpe(iz) * frac(iz)
     xkphel(2) = xkphel(2) + xje(iz) * frac(iz)
  ENDDO
  jzmin(mm) = kzmin
  jzmax(mm) = kzmax
  zmoy(mm,iopt) = zzmoy
  xkphel(:) = xkphel(:) * densh(iopt)

  j = 0
  DO i = ndeb, nfin
     ir1 = react(i,1)
     j = j + 1
     DO iz = izmin, izmax
        xkneut(j) = xkneut(j) + xji(ir1,iz) * frac(iz)
     ENDDO
     ! 4 I 2006 : Bug (ER + FLP) : divide by ab(ir1)
     IF (ab(ir1) /= 0.0_dp) THEN
        xkneut(j) = xkneut(j) * densh(iopt) / ab(ir1)
     ENDIF
  ENDDO

END SUBROUTINE FBTH

END MODULE PXDR_CHEMISTRY
