!
!	PXDR_INIFITS.f90
!	PDR_1.4_svn
!
!	Created by Franck on 18/07/09.
!	Copyright 2009 LUTH - Observatoire de Paris. All rights reserved.
!

!***********************************************************************
! MODULE PXDR_INIFITS
!***********************************************************************
MODULE PXDR_INIFITS
USE PXDR_CONSTANTES
IMPLICIT NONE

REAL(KIND=dp), PRIVATE, ALLOCATABLE:: array1D(:)
REAL(KIND=dp), PRIVATE, ALLOCATABLE:: array2D(:,:)


PRIVATE
PUBLIC :: RESTARTFITS

CONTAINS
!=======================================================================
!== SUBROUTINE RESTARTFITS
!==
!== Reread some values of a FITS file from a previous model to
!== restart from this model.
!==
!== 1 - Prepare FITS files (Open, initialize status, get units)
!== 2 - Read interesting values
!== 3 - Replace initialization values done before in the code by the
!==     new values (Ex: tau_o grid)
!=======================================================================
SUBROUTINE RESTARTFITS(filefits)
USE PXDR_CONSTANTES
USE PXDR_CHEM_DATA
USE PXDR_PROFIL
USE PXDR_AUXILIAR
USE PXDR_STR_DATA
USE PXDR_RF_TOOLS
USE PXDR_INITIAL
IMPLICIT NONE

CHARACTER(LEN=lenfilename), INTENT (INOUT) :: filefits

CHARACTER(LEN=lenfilename) :: dfilefits
INTEGER :: stat       ! error code of cfitsio library
INTEGER :: unitfits   ! logical unit of fits file
LOGICAL :: anynull
INTEGER :: readwrite
INTEGER :: blocksize

CHARACTER(len=lenID) :: DU_NAME
CHARACTER(len=lenID) :: FD_NAME
CHARACTER(len=lennamespecy) :: espece
CHARACTER(len=lennamespecy) :: especeID
CHARACTER(len=128)          :: fichin

REAL (KIND=dp), ALLOCATABLE :: xre(:)


INTEGER :: i, j, k
     !==================================================================
     !== BEGIN RESTARTFITS
     !==================================================================

     WRITE(iscrn,*) "                             "
     WRITE(iscrn,*) "-----------------------------"
     WRITE(iscrn,*) "          RESTART            "
     WRITE(iscrn,*) TRIM(filefits)
     WRITE(iscrn,*) "-----------------------------"

     dfilefits = TRIM(out_dir)//filefits
     PRINT *,"RESTART from : ",TRIM(dfilefits)

     !==================================================================
     !== INITIALIZATION & OPEN FITS FILE
     !==================================================================
     !- Initialize error code ------------------------------------------
     stat = 0 ! error code mandatory to set to 0
     anynull = .false.
     !-- Get an unused Logical Unit Number to use to open the FITS file.
     CALL ftgiou(unitfits,stat)
     PRINT *,stat
     !-- Open the FITS file previously created by WRITEIMAGE
     readwrite = 0
     CALL ftopen(unitfits,dfilefits,readwrite,blocksize,stat)
     Print *,stat

     !==================================================================
     !== GET DATA
     !== Donne as standard RESTART from binary file
     !==================================================================

     !-- Size of some arrays -------------------------------------------
     npo   = GETFITS_INT(unitfits,"DU_ARRAYSIZE","FD_ARRAYSIZE_NPO")
     nspec = GETFITS_INT(unitfits,"DU_ARRAYSIZE","FD_ARRAYSIZE_NSPEC")
     n_var = GETFITS_INT(unitfits,"DU_ARRAYSIZE","FD_ARRAYSIZE_NVAR")
     ! ATTENTION TOUS LES FITS N'ONT PAS NSPL POUR L'INSTANT
     nspl  = GETFITS_INT(unitfits,"DU_ARRAYSIZE","FD_ARRAYSIZE_NSPL")

     F_ISRF   = GETFITS_INT(unitfits,"DU_MODPARAM_I","FD_FISRF")

     !------------------------------------------------------------------
     !-- Optical depth
     !------------------------------------------------------------------
     CALL GETFITS_DBL1D(unitfits,"DU_OPTDEPTH","FD_OPTDEPTH")
     tau(:) = array1D(:)
     !-- Update initialization value of tau --
     DEALLOCATE(tau_o); ALLOCATE(tau_o(0:npo))
     tau_o(0:npo) = tau(0:npo)

     !------------------------------------------------------------------
     !-- Gas temperature
     !------------------------------------------------------------------
     CALL GETFITS_DBL1D(unitfits,"DU_STRUCTURE","FD_GASTEMPERATURE")
     tgaz(:) = array1D(:)
     !-- Update initialization value of tgaz --
     DEALLOCATE(tgaz_o); ALLOCATE(tgaz_o(0:npo+1))
     tgaz_o(0:npo) = tgaz(0:npo)
     tgaz_o(npo+1) = tgaz(npo)

     !------------------------------------------------------------------
     !-- Proton density
     !------------------------------------------------------------------
     CALL GETFITS_DBL1D(unitfits,"DU_STRUCTURE","FD_DENSITY")
     densh(:) = array1D(:)
     !-- Update initialization value of densh--
     DEALLOCATE(dens_o); ALLOCATE(dens_o(0:npo))
     dens_o(0:npo) = densh(0:npo)

     !------------------------------------------------------------------
     !-- Abundances
     !------------------------------------------------------------------
     abnua(:,:) = 0.0_dp ! Initialization
     !-- Abundances of standard species
     CALL GETFITS_DBL2D(unitfits,"DU_ABUNDANCES")
     DO i = 1, nspec
        abnua(i,0:npo) = array2D(:,i) ! array2D index goes from 1 to npo+1
     ENDDO
     !-- Moments
     CALL GETFITS_DBL2D(unitfits,"DU_MOMPROFILES")
     DO i = nspec+1, n_var
        abnua(i,0:npo) = array2D(:,i-nspec)
     ENDDO
     !-- Pseudo abundances of PHOTONS, CRP, PHOSEC, GRAIN, 2H
     abnua(n_var+1:n_var+5,0:npo) = 1.0_dp
     !-- Update initialization value of abnua --
     DEALLOCATE(abnua_o); ALLOCATE(abnua_o(0:n_var,0:npo))
     abnua_o(0:n_var,0:npo) = abnua(0:n_var,0:npo)

     !-- Initialization value of H abundance --
     PRINT *,"Update initialization values of H abundance"
     IF (ALLOCATED(abh_o) ) DEALLOCATE(abh_o)
     ALLOCATE(abh_o(0:npo))
     abh_o = abnua(i_h,0:npo)

     !-- Initialization value of H2 abundance --
     PRINT *,"Update initialization values of H2 abundance"
     IF (ALLOCATED(abh2_o)) DEALLOCATE(abh2_o)
     ALLOCATE(abh2_o(0:npo))
     abh2_o = abnua(i_h2,0:npo)

     !------------------------------------------------------------------
     !-- Grains temperature
     !------------------------------------------------------------------
     !-- Dust temperatures are written in FITS file in a single column
     !-- (k,1) with k from 0 to (npo+1)*npg
     CALL GETFITS_DBL2D(unitfits,"DU_GRTEMP")
     k = 0
     DO i = 1, npg
        DO j = 0, npo
           k = k + 1
           tdust(i,j) = array2D(k,1)
        ENDDO
     ENDDO
    !-- Update initialization value of tdust --------------------------
     DEALLOCATE(tdust_o); ALLOCATE(tdust_o(npg,0:npo))
     tdust_o(:,0:npo) = tdust(:,0:npo)

     !------------------------------------------------------------------
     !-- Grains charge -- IS IT REALLY REQUIRED ?
     !------------------------------------------------------------------
     CALL GETFITS_DBL2D(unitfits,"DU_GRCHARGE")
     k = 0
     DO i = 1, npg
        DO j = 0, npo
           k = k + 1
           zmoy(i,j) = array2D(k,1)
        ENDDO
     ENDDO

     !------------------------------------------------------------------
     !-- LEVELS & LINES
     !------------------------------------------------------------------

     IF (ALLOCATED(xre)) DEALLOCATE(xre)
     ALLOCATE(xre(0:npo))
     DO k = 1, nspl

        espece = TRIM(spec_lin(k)%nam) ! PAS BIEN CAR UTILISE UN NOM INITIALISE AVANT ET PAS FORCEMENT DANS LE FITS
        CALL SPEMAJ(espece,especeID)

        !-- Get %ind ---------------------------------------------------
        ! NOTE : PAS TRES CORRECT. LE FAIT QUE L'ON REPRENNE LE IND
        ! SUPPOSE QU'ON A LA MEME CLASSEMENT DES ESPECES. PAR CONSEQUENT
        ! IL N'Y A PAS A SUPPOSER QUE LA LISTE DES NSPL ESPECES PEUT CHANGER
        ! ET DONC PAS BESOIN A S'EMBETTER A RELIRE DU FITS DES QUANTITES COMME LEUR NOM
        DU_NAME   = "DU_LEVELINFO_"//TRIM(ADJUSTL(especeID))
        FD_NAME   = "FD_IND_"//TRIM(ADJUSTL(especeID))
        spec_lin(k)%ind = GETFITS_INT(unitfits,DU_NAME,FD_NAME)

        !-- Get %mom ---------------------------------------------------
        DU_NAME   = "DU_LEVELINFO_"//TRIM(ADJUSTL(especeID))
        FD_NAME   = "FD_MASS_"//TRIM(ADJUSTL(especeID))
        spec_lin(k)%mom = GETFITS_DBL(unitfits,DU_NAME,FD_NAME)

        !-- Get the number of levels used ------------------------------
        DU_NAME   = "DU_LEVELINFO_"//TRIM(ADJUSTL(especeID))
        FD_NAME   = "FD_NUSE_"//TRIM(ADJUSTL(especeID))
        spec_lin(k)%use = GETFITS_INT(unitfits,DU_NAME,FD_NAME)

        !-- Reread data levels -----------------------------------------
        !-- READ_DATALEVEL fills     : %nlv, %nqn, %gst, %elk, %qnam, %quant
        !--                ALLOCATES :             %gst, %elk, %qnam, %quant
        DEALLOCATE(spec_lin(k)%gst)
        DEALLOCATE(spec_lin(k)%elk)
        DEALLOCATE(spec_lin(k)%qnam)
        DEALLOCATE(spec_lin(k)%quant)
        fichin = TRIM(data_dir)//"Levels/level_"//TRIM(spec_lin(k)%nam)//".dat"
        CALL READ_DATALEVEL(fichin, spec_lin(k),spec_lin(k)%use)

        !-- Feel lines properties --------------------------------------
        !-- READ_DATALINE fills     : %ntr, %iup, %jlo, %wlk, %aij, %inftr
        !--               ALLOCATES :       %iup, %jlo, %wlk, %aij, %inftr
        DEALLOCATE(spec_lin(k)%iup)
        DEALLOCATE(spec_lin(k)%jlo)
        DEALLOCATE(spec_lin(k)%wlk)
        DEALLOCATE(spec_lin(k)%aij)
        DEALLOCATE(spec_lin(k)%inftr)
        fichin = TRIM(data_dir)//"Lines/line_"//TRIM(spec_lin(k)%nam)//".dat"
        CALL READ_DATALINE(fichin,spec_lin(k))

        !-- Get %usemin ------------------------------------------------
        ! A FAIRE ECRIRE DANS LE FITS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        !-- ALLOCATION OF SPELIN ELEMENTS ------------------------------!
        !-- ALLOC_SPECTRA Allocates : %abu, %ref, %vel, %xre, %XXl, %CoD!
        !--                     %hot, %lac, %exl, %exr, %siD, %odl, %odr!
        !--                     %dnu, %emi, %pum, %jiD, %bel, %ber      !
        !--               Requires  : npo, %use, %ntr                   !
        DEALLOCATE(spec_lin(k)%abu)                                     !
        DEALLOCATE(spec_lin(k)%ref)                                     !
        DEALLOCATE(spec_lin(k)%vel)                                     !
        DEALLOCATE(spec_lin(k)%xre)                                     !
        DEALLOCATE(spec_lin(k)%XXl)                                     !
        DEALLOCATE(spec_lin(k)%XXr)                                     !
        DEALLOCATE(spec_lin(k)%CoD)                                     !
        DEALLOCATE(spec_lin(k)%hot)                                     !
        DEALLOCATE(spec_lin(k)%lac)                                     !
        DEALLOCATE(spec_lin(k)%exl)                                     !
        DEALLOCATE(spec_lin(k)%exr)                                     !
        DEALLOCATE(spec_lin(k)%siD)                                     !
        DEALLOCATE(spec_lin(k)%odl)                                     !
        DEALLOCATE(spec_lin(k)%odr)                                     !
        DEALLOCATE(spec_lin(k)%dnu)                                     !
        DEALLOCATE(spec_lin(k)%emi)                                     !
        DEALLOCATE(spec_lin(k)%pum)                                     !
        DEALLOCATE(spec_lin(k)%jiD)                                     !
        DEALLOCATE(spec_lin(k)%bel)                                     !
        DEALLOCATE(spec_lin(k)%ber)                                     !
        CALL ALLOC_SPECTRA(spec_lin(k),spec_lin(k)%use,spec_lin(k)%ntr) !
        !----------------------------------------------------------------

        !-- Fill %abu (required in BILTHM) -----------------------------
        spec_lin(k)%abu(:) = abnua(spec_lin(k)%ind,0:npo)
        IF (spec_lin(k)%ind == j_c13o .AND. i_c13o == 0) THEN
            spec_lin(in_sp(j_c13o))%abu = abnua(i_co,0:npo) / r_c13
        ENDIF
        IF (spec_lin(k)%ind == j_co18 .AND. i_co18 == 0) THEN
            spec_lin(in_sp(j_co18))%abu = abnua(i_co,0:npo) / r_o18
        ENDIF
        IF (spec_lin(k)%ind == j_c13o18 .AND. i_c13o18 == 0) THEN
            spec_lin(in_sp(j_c13o18))%abu = abnua(i_co,0:npo) / (r_c13 * r_o18)
        ENDIF

        !-- Fill %lac --------------------------------------------------
        spec_lin(k)%lac(:) = hcsurk / spec_lin(k)%wlk(:)

        !-- Fill %vel --------------------------------------------------
        !-- This quantity is optionnal in the FITS file, so we recompute it
        vturb = GETFITS_DBL(unitfits,"DU_MODPARAM_R","FD_VTURB")
        spec_lin(k)%vel(0:npo) = SQRT(vturb * vturb + deksamu * tgaz_o(0:npo) &
                                                     / spec_lin(k)%mom)

        !-- Get %xre - level excitations -------------------------------
        PRINT *,"LEVEL EXCITATION"
        DU_NAME = "DU_EX_"//TRIM(ADJUSTL(especeID))
        CALL GETFITS_DBL2D(unitfits,DU_NAME)
        DO i = 1, spec_lin(k)%use
           !PRINT *,"Level : ",i
           spec_lin(k)%xre(i,0:npo) = array2D(:,i) ! array2D index goes from 1 to npo+1
           xre(0:npo) = spec_lin(k)%xre(i,0:npo)
           CALL AB_REL_REV(spec_lin(k)%ind,xre,i) ! Fill all the xreXX(1:%use,0:npo) arrays as xreh2
           ! Note : spec_lin(k)%ind is the i_XX value of the special species; ex i_co
           !        These values are not read from the FITS file even if they are are written.
           !        We assume the same chemistry file is used for restart and so these numbers will be the sames.
        ENDDO

        !-- FEED LINES -------------------------------------------------
        !-- FEED LINES fills : %XXr, %CoD, %siD, %exl, %exr, %dnu, %odl, %odr,
        !--                    %pum, %hot, %JiD, %bel, %ber, %emi
        !--            requires : all the other spelin%
        !--                       and dens_o, tau_o, tdust_o
        CALL FEED_LINES(spec_lin(k))

     ENDDO ! End of DO on species with excitation computed (1-nspl)
     DEALLOCATE(xre)

     !==================================================================
     !== Close FITS file
     !==================================================================
     CALL ftclos(unitfits, stat)
     CALL ftfiou(unitfits, stat)

     PRINT *,"Restart data are now in memory"

     !==================================================================
     !== Update initialization values
     !==================================================================
     PRINT *,"Update initialization values"

     !-- Initialization values of levels populations --------------------
     CALL CDT_WRAP

     IF (i_co .NE. 0) THEN
        DEALLOCATE(eqrco); ALLOCATE(eqrco(spec_lin(in_sp(i_co))%use))
        ! For CO isotopes if they are not in the chemistry i_c13o = i_co18 = i_c13o18 =0
        ! But since they are required in level populations for thermal balance
        ! in INIT_LINES, j_c13o, j_co18 and j_c13o18 have been set
        DEALLOCATE(eqrc13o); ALLOCATE(eqrc13o(spec_lin(in_sp(j_c13o))%use))
        DEALLOCATE(eqrco18); ALLOCATE(eqrco18(spec_lin(in_sp(j_co18))%use))
        DEALLOCATE(eqrc13o18); ALLOCATE(eqrc13o18(spec_lin(in_sp(j_c13o18))%use))
     ENDIF

END SUBROUTINE RESTARTFITS

!=======================================================================
!== SUBROUTINE GETFITS_DBL1D
!=======================================================================
SUBROUTINE GETFITS_DBL1D(unitfits, DU_NAME, FD_NAME)
USE PXDR_CONSTANTES
IMPLICIT NONE

INTEGER,          INTENT(IN) :: unitfits
CHARACTER(len=*), INTENT(IN) :: DU_NAME
CHARACTER(len=*), INTENT(IN) :: FD_NAME

INTEGER           :: stat       ! error code of cfitsio library
INTEGER           :: colnum
INTEGER           :: ncols
INTEGER           :: nrows
CHARACTER(len=10) :: tscal
CHARACTER(len=10) :: tzero
CHARACTER(len=10) :: ttype
CHARACTER(len=10) :: tunit
CHARACTER(len=10) :: tnull
CHARACTER(len=10) :: tdisp
CHARACTER(len=3)  :: datatype
INTEGER           :: repeat
LOGICAL           :: anynull

     !-- Initialize stat (mandatory) -----------------------------------
     stat = 0

     !==================================================================
     !== READ DATA
     !==================================================================

     PRINT *,"SEARCH : ",TRIM(DU_NAME)," ",TRIM(FD_NAME)

     !-- Move to the Data Unit -----------------------------------------
     CALL ftmnhd(unitfits, -1,TRIM(DU_NAME), 0, stat)
     !PRINT *,"Move to Data Unit : ",TRIM(DU_NAME)," - status : ",stat

     !-- Get number of lines of the Data Unit --------------------------
     CALL ftgnrw(unitfits, nrows, stat)
     !PRINT *,"Number of lines in DU : ",nrows," - status : ",stat
     IF (ALLOCATED(array1D)) DEALLOCATE(array1D)
     ALLOCATE(array1D(nrows))

     !-- Get number of columns -----------------------------------------
     CALL ftgncl(unitfits, ncols, stat)
     !PRINT *,"Number of columns : ",ncols," - status : ",stat

     !-- Get index number of the searched column -----------------------
     CALL ftgcno(unitfits,.true.,TRIM(FD_NAME),colnum,stat)
     !PRINT *,"Index of column :",colnum," - status : ",stat

     !-- Get the datatype of the column --------------------------------
     CALL ftgbcl(unitfits,colnum,ttype,tunit,datatype,repeat,tscal,tzero,tnull,tdisp,stat)
     !PRINT *,"Datatype of column : ",datatype," - status",stat

     !-- READ DATA ----------------------------------------------------
     IF (trim(datatype) == 'D') THEN
        ! pour recuperer la colonne : les 1,1 disent qu'on prend les elements a partir du
        ! 1er element de la 1ere ligne
        ! nrows = nb de lignes dans le tableau
        ! 0. = la valeur qu'on met dans le tableau si un element est non defini dans le fits
        CALL ftgcvd(unitfits, colnum, 1, 1, nrows, 0., array1D, anynull, stat)
     ENDIF

     !PRINT *,"Read values - status : ",stat

END SUBROUTINE GETFITS_DBL1D

!=======================================================================
!== SUBROUTINE GETFITS_DBL2D
!=======================================================================
SUBROUTINE GETFITS_DBL2D(unitfits, DU_NAME)
IMPLICIT NONE

INTEGER,          INTENT(IN) :: unitfits
CHARACTER(len=*), INTENT(IN) :: DU_NAME

INTEGER :: stat       ! error code of cfitsio library
INTEGER :: colnum
INTEGER :: ncols
INTEGER :: nrows
LOGICAL           :: anynull

     !-- Initialize stat (mandatory) -----------------------------------
     stat = 0

     !==================================================================
     !== READ DATA
     !==================================================================

     PRINT *,"SEARCH : ",TRIM(DU_NAME)

     !-- Move to the Data Unit -----------------------------------------
     CALL ftmnhd(unitfits, -1,TRIM(DU_NAME), 0, stat)
     !PRINT *,"Move to Data Unit : ",TRIM(DU_NAME)," - status : ",stat

     !-- Get number of lines of the Data Unit --------------------------
     CALL ftgnrw(unitfits, nrows, stat)
     !PRINT *,"Number of lines in DU : ",nrows," - status : ",stat

     !-- Get number of columns -----------------------------------------
     CALL ftgncl(unitfits, ncols, stat)
     !PRINT *,"Number of columns : ",ncols," - status : ",stat

     IF (ALLOCATED(array2D)) DEALLOCATE(array2D)
     ALLOCATE(array2D(nrows,ncols))

     !-- READ DATA ----------------------------------------------------
     DO colnum = 1, ncols
        CALL ftgcvd(unitfits, colnum, 1, 1, nrows, 0., array2D(:,colnum), anynull, stat)
     ENDDO
     !PRINT *,"Read values - status : ",stat

END SUBROUTINE GETFITS_DBL2D

!=======================================================================
!== FUNCTION GETFITS_INT
!=======================================================================
FUNCTION GETFITS_INT(unitfits, DU_NAME, FD_NAME)
USE PXDR_CONSTANTES
IMPLICIT NONE

INTEGER,          INTENT(IN) :: unitfits
CHARACTER(len=*), INTENT(IN) :: DU_NAME
CHARACTER(len=*), INTENT(IN) :: FD_NAME
INTEGER                      :: GETFITS_INT


INTEGER :: stat       ! error code of cfitsio library
INTEGER :: colnum
INTEGER :: ncols
INTEGER :: nrows
CHARACTER(len=10) :: tscal
CHARACTER(len=10) :: tzero
CHARACTER(len=10) :: ttype
CHARACTER(len=10) :: tunit
CHARACTER(len=10) :: tnull
CHARACTER(len=10) :: tdisp
CHARACTER(len=3)  :: datatype
INTEGER           :: repeat
LOGICAL           :: anynull

INTEGER     :: value

     !-- Initialize stat (mandatory) -----------------------------------
     stat = 0

     !==================================================================
     !== READ DATA
     !==================================================================

     !-- Move to the Data Unit -----------------------------------------
     CALL ftmnhd(unitfits, -1,TRIM(DU_NAME), 0, stat)
     PRINT *,"Move to DU : ",TRIM(DU_NAME)," - status : ",stat

     !-- Get number of lines of the Data Unit --------------------------
     CALL ftgnrw(unitfits, nrows, stat)
     PRINT *,"Number of lines in DU : ",nrows," - status : ", stat

     !-- Get number of columns -----------------------------------------
     CALL ftgncl(unitfits, ncols, stat)
     PRINT *,"Number of columns in DU : ",ncols, " - status : ",stat

     !-- Get index number of the searched column -----------------------
     CALL ftgcno(unitfits,.true.,TRIM(FD_NAME),colnum,stat)
     PRINT *,'Column index for : ',TRIM(FD_NAME)," ",colnum," - status : ",stat

     !-- Get the datatype of the column --------------------------------
     CALL ftgbcl(unitfits,colnum,ttype,tunit,datatype,repeat,tscal,tzero,tnull,tdisp,stat)
     PRINT *,'Datatype : ',datatype," ",stat

     !-- READ DATA -----------------------------------------------------
     IF (trim(datatype) == 'J') THEN
        ! pour recuperer la colonne : les 1,1 disent qu'on prend les elements a partir du
        ! 1er element de la 1ere ligne
        ! nrows = nb de lignes dans le tableau
        ! 0. = la valeur qu'on met dans le tableau si un element est non defini dans le fits
        CALL ftgcvj(unitfits, colnum, 1, 1, nrows, 0., value, anynull, stat)
     ENDIF

     PRINT *,"VALUE = ",value
     GETFITS_INT = value

END FUNCTION GETFITS_INT

!=======================================================================
!== FUNCTION GETFITS_INT
!=======================================================================
FUNCTION GETFITS_DBL(unitfits, DU_NAME, FD_NAME)
USE PXDR_CONSTANTES
IMPLICIT NONE

INTEGER,          INTENT(IN) :: unitfits
CHARACTER(len=*), INTENT(IN) :: DU_NAME
CHARACTER(len=*), INTENT(IN) :: FD_NAME
REAL(KIND=dp)                :: GETFITS_DBL


INTEGER :: stat       ! error code of cfitsio library
INTEGER :: colnum
INTEGER :: ncols
INTEGER :: nrows
CHARACTER(len=10) :: tscal
CHARACTER(len=10) :: tzero
CHARACTER(len=10) :: ttype
CHARACTER(len=10) :: tunit
CHARACTER(len=10) :: tnull
CHARACTER(len=10) :: tdisp
CHARACTER(len=3)  :: datatype
INTEGER           :: repeat
LOGICAL           :: anynull

REAL(KIND=DP)     :: value

     !-- Initialize stat (mandatory) -----------------------------------
     stat = 0

     !==================================================================
     !== READ DATA
     !==================================================================

     !-- Move to the Data Unit -----------------------------------------
     CALL ftmnhd(unitfits, -1,TRIM(DU_NAME), 0, stat)
     PRINT *,"Move to DU : ",TRIM(DU_NAME)," - status : ",stat

     !-- Get number of lines of the Data Unit --------------------------
     CALL ftgnrw(unitfits, nrows, stat)
     PRINT *,"Number of lines in DU : ",nrows," - status : ", stat

     !-- Get number of columns -----------------------------------------
     CALL ftgncl(unitfits, ncols, stat)
     PRINT *,"Number of columns in DU : ",ncols, " - status : ",stat

     !-- Get index number of the searched column -----------------------
     CALL ftgcno(unitfits,.true.,TRIM(FD_NAME),colnum,stat)
     PRINT *,'Column index for : ',TRIM(FD_NAME)," ",colnum," - status : ",stat

     !-- Get the datatype of the column --------------------------------
     CALL ftgbcl(unitfits,colnum,ttype,tunit,datatype,repeat,tscal,tzero,tnull,tdisp,stat)
     PRINT *,'Datatype : ',datatype," ",stat

     !-- READ DATA -----------------------------------------------------
     IF (trim(datatype) == 'D') THEN
        ! pour recuperer la colonne : les 1,1 disent qu'on prend les elements a partir du
        ! 1er element de la 1ere ligne
        ! nrows = nb de lignes dans le tableau
        ! 0. = la valeur qu'on met dans le tableau si un element est non defini dans le fits
        CALL ftgcvd(unitfits, colnum, 1, 1, nrows, 0., value, anynull, stat)
     ENDIF

     PRINT *,"VALUE = ",value
     GETFITS_DBL = value

END FUNCTION GETFITS_DBL


END MODULE PXDR_INIFITS
