! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

!JLB - 22 mai 2008

MODULE PXDR_OUTPUT

  PRIVATE

  PUBLIC :: SORTIE1, SORTIE2

CONTAINS

!=================
SUBROUTINE SORTIE1
!=================

  USE PXDR_CONSTANTES
  USE PXDR_CHEM_DATA
  USE PXDR_PROFIL
  USE PXDR_AUXILIAR
  USE PXDR_STR_DATA
  USE PXDR_RF_TOOLS
  USE PXDR_TRANSFER
  USE PXDR_INITIAL

  IMPLICIT NONE

  CHARACTER(LEN=70)   :: fichic

  INTEGER :: i, j, k
  INTEGER :: iv, ij
  INTEGER :: iopt

   !-------------------------------------------------------------------------
   ! Open output file
   !-------------------------------------------------------------------------
   fichic = TRIM(file_bin)//TRIM(ADJUSTL(ii2c(ifaf)))
   PRINT *,"Write outputs in : ",fichic
   version_output = "26VIII08_PXDR"

   OPEN (iwrit1, file = fichic, status = 'unknown', form = 'unformatted')

   WRITE (iwrit1) version_output
   WRITE (iwrit1) version_code

   WRITE (iwrit1) natom
   WRITE (iwrit1) npo, nspec, n_var
   WRITE (iwrit1) cdunit, rrv
   WRITE (iwrit1) l0h2, l0hd

   WRITE (iwrit1) nvbc, njbc
   WRITE (iwrit1) i_h, i_d, i_hgr, i_dgr, i_hgrc, i_h2, i_hd, i_h2gr, i_hdgr, i_he, i_c, i_n, i_o, i_s, &
                  i_si, i_cp, i_np, i_op, i_sp, i_sip, i_h2o, i_h2ogr, i_oh, i_co, i_c13o, i_co18, &
                  i_c13o18, i_cogr, i_hp, i_h2dp, i_h3p, i_fep, i_c2, i_cs, i_c13, i_o18, i_hcop, &
                  i_chp, i_hcn, i_o2

   WRITE (iwrit1) nspl
   WRITE (iwrit1) in_sp
   WRITE (iwrit1) j_c13o, j_co18, j_c13o18
   DO i = 1, nspl
      CALL WRIT_LIN(spec_lin(i))
   ENDDO

   WRITE (iwrit1) (speci(i), i=0,n_var+5)

   ! GRAIN PROPERTIES
   WRITE (iwrit1) npg
   WRITE (iwrit1) ((zmoy(i,j), i=1,npg), j=0,npo)
   WRITE (iwrit1) (relgr(1:2,j), j=0,npo)
   WRITE (iwrit1) (adust(i), i=1,npg)
   WRITE (iwrit1) (wwmg(i), i=1,npg)
   WRITE (iwrit1) (surfgr(i), i=1,npg)
   WRITE (iwrit1) (tau(i), densh(i), tgaz(i), (tdust(j,i), j=1,npg), i=0,npo)

   !--------------------------------------
   !  HEATING AND COOLING RATES
   !--------------------------------------
   WRITE (iwrit1) (reffep(i), i=0,npo)
   WRITE (iwrit1) (chophg(i), choh2g(i), chohdg(i), chopht(i), chorc(i), chophs(i), i=0,npo)
   WRITE (iwrit1) (choeqg(i), chochi(i), chotur(i), reftot(i), chotot(i), i=0,npo)

   !---------------------------------------
   ! Formation and destruction of H2 and HD
   !---------------------------------------
   WRITE (iwrit1) (timh21(i), timh22(i), timh23(i), timh24(i), timh25(i), i=0,npo)
   IF (i_hd /= 0) THEN
      WRITE (iwrit1) (timhd1(i), timhd2(i), timhd3(i), timhd4(i), i=0,npo)
   ENDIF

   WRITE (iwrit1) (fluph(i), int_rad(i), i=0,npo)

   !--------------------------------------------------------
   ! Abondances of all species:
   !--------------------------------------------------------
   WRITE (iwrit1) ((abnua(j,i), j=0,n_var), i=0,npo)
   WRITE (iwrit1) ((codesp(j,i), j=0,nspec), i=0,npo)

   !---------------------------------------------------------
   ! Model parameters
   !---------------------------------------------------------
   WRITE (iwrit1) Avmax, nh_init, tg_init, d_sour, fmrc, presse
   WRITE (iwrit1) vturb, F_dustem
   WRITE (iwrit1) neqt, F_ISRF, radm, radp, radm_ini, radp_ini
   WRITE (iwrit1) isoneside
   WRITE (iwrit1) modele, chimie, fprofil, srcpp, los_ext
   WRITE (iwrit1) ifafm, ifeqth, ifisob, itrfer, lfgkh2, ichh2
   WRITE (iwrit1) iforh2, ifaf

   !---------------------------------------------------------
   ! Chemical network
   !---------------------------------------------------------
   WRITE (iwrit1) ((rxigr(i,j), i=0,nspec), j=0,npo)
   WRITE (iwrit1) (t_evap(i), i=0,nspec)
   WRITE (iwrit1) (t_diff(i), i=0,nspec)
   WRITE (iwrit1) T2_stick_ER, ex_stick_ER
   WRITE (iwrit1) (xmatk(i)%nbt, i=1,n_var)
   WRITE (iwrit1) ((xmatk(i)%fdt(k), k=1,xmatk(i)%nbt), i=1,n_var)

   WRITE (iwrit1) ((react(j,i), i=0,7), j=1,neqt)
   WRITE (iwrit1) (gamm(i), i=1,neqt)
   WRITE (iwrit1) (alpha(i), i=1,neqt)
   WRITE (iwrit1) (bet(i), i=1,neqt)
   WRITE (iwrit1) (de(i), i=1,neqt)
   WRITE (iwrit1) (itype(i), i=1,neqt)
   WRITE (iwrit1) nh2for, ncrion, npsion, nasrad, nrest
   WRITE (iwrit1) nh2endo, nphot_f, nphot_i, n3body, ncpart, ngsurf, ngphot, i_mandat
   WRITE (iwrit1) ngads, ngneut, nggdes, ncrdes, nphdes, ngevap, ngchs, ngelri
   WRITE (iwrit1) nsp_ne, nsp_ip, nsp_im, nsp_cs, nsp_gm, nsp_g1
   WRITE (iwrit1) ((pdiesp(k,i), k=1,nphot_i), i=0,npo)
   WRITE (iwrit1) (phdest(k)%r1, k=1,nphot_i)

   !----------------------------------------------------------
   ! Model parameters 2
   !----------------------------------------------------------
   WRITE (iwrit1) g_ratio, rhogr, alpgr, rgrmin, rgrmax, zeta
   WRITE (iwrit1) s_gr_t
   WRITE (iwrit1) xngr, agrnrm, signgr
   WRITE (iwrit1) vmaxw, fphsec, sngclb, rdeso
   WRITE (iwrit1) dsite
   WRITE (iwrit1) istic
   WRITE (iwrit1) iadhgr, ifh2gr, iaddgr, ifhdgr

   !----------------------------------------------------------
   ! Properties of species
   !----------------------------------------------------------
   WRITE (iwrit1) (mol(i), i=0,n_var+5)
   WRITE (iwrit1) ((ael(i,j), i=0,n_var+5), j=1,natom)
   WRITE (iwrit1) (enth(i), i=0,n_var+5)
   WRITE (iwrit1) (abin(i), i=0,n_var+5)
   WRITE (iwrit1) (l_atom(i), i=1,natom-1)

   !----------------------------------------------------------
   !  Excitation of levels B and C of H2
   !----------------------------------------------------------
   WRITE(iwrit1) (((h2bnua(iv,ij,iopt), iv=0,nvbc), ij=0,njbc), iopt=0,npo)
   WRITE(iwrit1) (((h2cnua(iv,ij,iopt), iv=0,nvbc), ij=0,njbc), iopt=0,npo)

   !----------------------------------------------------------
   ! Fitzpatrick and Massa extinction curve parameters
   !----------------------------------------------------------
   WRITE(iwrit1) cc1, cc2, cc3, cc4, xgam, y0

   !----------------------------------------------------------
   ! Informations on H2 and HD formation
   !----------------------------------------------------------
   WRITE (iwrit1) eform_h2, emoyh2
   WRITE (iwrit1) eform_hd, emoyhd

   !----------------------------------------------------------
   ! Informations on incident radiation field
   !----------------------------------------------------------
   WRITE (iwrit1) wl_6eV, wl_hab
   WRITE (iwrit1) int_rad0, flu_rad0

   ! One side cloud
   IF (isoneside == 1) THEN
      WRITE (iwrit1) int_rad1, flu_rad1
   ELSE
      WRITE (iwrit1) int_rad1, flu_rad1
      WRITE (iwrit1) int_rad2, flu_rad2
   ENDIF

   WRITE (iwrit1) G0m, G0p

   !-------------------------------------------------------
   ! Wavelength grid
   !-------------------------------------------------------
   WRITE(iwrit1) nwlg
   WRITE(iwrit1) rf_wl%Val(0:nwlg) ! tableau des longeurs d'onde
   WRITE(iwrit1) (du_prop%abso(:,iopt), iopt=0,npo)
   WRITE(iwrit1) (du_prop%scat(:,iopt), iopt=0,npo)
   WRITE(iwrit1) (du_prop%emis(:,iopt), iopt=0,npo)
   WRITE(iwrit1) (du_prop%gcos(:,iopt), iopt=0,npo)
   WRITE(iwrit1) (du_prop%albe(:,iopt), iopt=0,npo)

   !--------------------------------------------------------
   ! OPTIONAL QUANTITIES --- USED FOR SIMDB
   !--------------------------------------------------------
   ! H2 molecular fraction
   WRITE (iwrit1) (molfrac_ab(iopt),iopt=0,npo)
   WRITE (iwrit1) (molfrac_cd(iopt),iopt=0,npo)
   ! H2 Excitation temperature
   WRITE (iwrit1) (T01_ab(iopt),iopt=0,npo)
   WRITE (iwrit1) (T01_cd(iopt),iopt=0,npo)
   ! Ionization degree, gas pressure, total density, distance
   WRITE (iwrit1) (ionidegree(iopt),iopt=0,npo)
   WRITE (iwrit1) (gaspressure(iopt),iopt=0,npo)
   WRITE (iwrit1) (totdensity(iopt),iopt=0,npo)
   WRITE (iwrit1) (d_cm(iopt),iopt=0,npo)
   WRITE (iwrit1) (dd_cm(iopt),iopt=0,npo)
   WRITE (iwrit1) (visualext(iopt),iopt=0,npo)
   WRITE (iwrit1) (protoncd(iopt),iopt=0,npo)

   CLOSE (iwrit1)

   PRINT *,"END OF SIMULATION"
   PRINT *,"H  column density :", codesp(i_h, npo)
   PRINT *,"H2 column density :", codesp(i_h2,npo)
   PRINT *,"Molecular fraction:", molfrac_cd(npo)
   PRINT *,"T01 - H2          :", T01_cd(npo)

END SUBROUTINE SORTIE1

!===============================================================================
! SUBROUTINE : WRIT_LIN
!===============================================================================
SUBROUTINE WRIT_LIN (spelin)

  USE PXDR_CONSTANTES
  USE PXDR_PROFIL
  USE PXDR_STR_DATA

  IMPLICIT NONE

  TYPE (SPECTRA), INTENT (IN) :: spelin
  INTEGER                     :: i, n
  INTEGER                     :: nqn

   PRINT *, "Write results for     : ", spelin%nam
   PRINT *, "Number of levels used : ", spelin%use

!--- On n'ecrit les resultats que pour les niveaux <= spelin%use
!--- On n'ecrit les transitions que pour les transitions avec
!    iup <= spelin%use et jlo <= spelin%use

   nqn = spelin%nqn

   !--- Species
   WRITE (iwrit1) spelin%nam        !  Species fortran name
   WRITE (iwrit1) spelin%mom        !  Molecular mass
   WRITE (iwrit1) spelin%ind        !  Index in species list

   !--- Properties
   WRITE (iwrit1) spelin%nlv        !  Number of levels in data files
   WRITE (iwrit1) spelin%use        !  Number of levels used in the model
   WRITE (iwrit1) spelin%nqn        !  Number of quantum numbers to define a level
   WRITE (iwrit1) spelin%ntr        !  Number of transitions for this species

   !--- Levels properties ----------------------------------------------------------------------------------
   WRITE (iwrit1) (spelin%qnam(n), n=1,nqn)              ! Names of the quantum levels (Ex : v, J, ...)
   DO i = 1, spelin%use
      WRITE (iwrit1) (spelin%quant(i,n), n=1,nqn)        !  Values of the quantum numbers for each levels
   ENDDO
   WRITE (iwrit1) (spelin%gst(i),     i=1, spelin%use)   !  Degenerecy of the level
   WRITE (iwrit1) (spelin%elk(i),     i=1, spelin%use)   !  Energy of levels in Kelvin

   !--- Lines properties ----------------------------------------------------------------------------------
   DO i = 1, spelin%ntr
      WRITE (iwrit1) spelin%inftr(i)                     ! Information on transition
   ENDDO
   DO i = 1, spelin%ntr !--- Index of higher level
      WRITE (iwrit1) spelin%iup(i)                       ! Index of upper level of the transition
   ENDDO
   DO i = 1, spelin%ntr !--- Index of lower level
      WRITE (iwrit1) spelin%jlo(i)                       ! Index of lower level of the transition
   ENDDO
   DO i = 1, spelin%ntr !--- Central wavelength
      WRITE (iwrit1) spelin%lac(i)                       ! Central wavelength
   ENDDO
   DO i = 1, spelin%ntr !--- Central wavelength
      WRITE (iwrit1) spelin%wlk(i)                       ! Central wavelength in K
   ENDDO
   DO i = 1, spelin%ntr !--- Einstein Coeff.
      WRITE (iwrit1) spelin%aij(i)                       ! Aij
   ENDDO
   DO i = 1, spelin%ntr !--- Emission
      WRITE (iwrit1) (spelin%emi(i,n), n=0,npo)          ! Emissivities at each position
   ENDDO

   !--- Abundances --------------------------------------------------------------------------------------
   WRITE (iwrit1) (spelin%abu(n), n=0,npo)               !  Abundance of species in cm-3
   DO i = 1, spelin%use
      WRITE (iwrit1) (spelin%xre(i,n), n=0,npo)          ! Normalized level populations
   ENDDO

   !--- Cooling -----------------------------------------------------------------------------------------
   WRITE (iwrit1) (spelin%ref(n), n=0,npo)

   !--- Velocity
   WRITE (iwrit1) (spelin%vel(n), n=0,npo)

   !--- Others
   DO i = 1, spelin%ntr
      WRITE (iwrit1) spelin%hot(i)
      WRITE (iwrit1) (spelin%siD(i,n), n=0,npo)
      WRITE (iwrit1) (spelin%odl(i,n), n=0,npo)
      WRITE (iwrit1) (spelin%odr(i,n), n=0,npo)
   ENDDO
   DO i = 1, spelin%use
      WRITE (iwrit1) (spelin%XXl(i,n), n=0,npo)
      WRITE (iwrit1) (spelin%XXr(i,n), n=0,npo)
      WRITE (iwrit1) (spelin%CoD(i,n), n=0,npo)
   ENDDO
   DO i = 1, spelin%ntr
      WRITE (iwrit1) spelin%exl(i)
      WRITE (iwrit1) spelin%exr(i)
      WRITE (iwrit1) (spelin%dnu(i,n), n=0,npo)
      WRITE (iwrit1) (spelin%pum(i,n), n=0,npo)
      WRITE (iwrit1) (spelin%jiD(i,n), n=0,npo)
      WRITE (iwrit1) (spelin%bel(i,n), n=0,npo)
      WRITE (iwrit1) (spelin%ber(i,n), n=0,npo)
   ENDDO

END SUBROUTINE WRIT_LIN

!%%%%%%%%%%%%%%%%%%
SUBROUTINE SORTIE2
!%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PXDR_RF_TOOLS

    IMPLICIT NONE

    REAL (KIND=dp) :: tion(0:noptm)
    INTEGER        :: i

!--------+---------+---------+---------+---------+---------+---------+-*-------+

   WRITE (iwrit2,2010)

   DO i = 0, npo
      tion(i) = abnua(nspec,i) / densh(i)
      WRITE (iwrit2,2020) tau(i), densh(i), tgaz(i), tdust(1,i), tion(i)
   ENDDO

!  Heating and cooling rates
!  -------------------------

   WRITE (iwrit2,6000)

   WRITE (iwrit2,3029) speci(i_h2), speci(i_co)
   DO i = 0, npo
      WRITE (iwrit2,3021, ADVANCE="NO") tau(i), rfjh2t(i)
      IF (i_co /= 0) THEN
         WRITE (iwrit2,3021) spec_lin(in_sp(i_co))%ref(i)
      ELSE
         WRITE (iwrit2,'("-")')
      ENDIF
   ENDDO

   IF (i_c /= 0 .AND. i_o /= 0 .AND. i_cp /= 0) THEN
      WRITE (iwrit2,3019) speci(i_c), speci(i_o), speci(i_cp)
      DO i = 0, npo
         WRITE (iwrit2,3021) tau(i), spec_lin(in_sp(i_c))%ref(i), spec_lin(in_sp(i_o))%ref(i) &
                           , spec_lin(in_sp(i_cp))%ref(i)
      ENDDO
   ENDIF

   WRITE (iwrit2,6001)
   DO i = 0, npo
      WRITE (iwrit2,3021) tau(i), chophg(i), choh2g(i), chohdg(i), chopht(i), chorc(i), chophs(i)
   ENDDO
   WRITE (iwrit2,6002)
   DO i = 0, npo
      WRITE (iwrit2,3021) tau(i), choeqg(i), chochi(i), chotur(i), reftot(i), chotot(i)
   ENDDO

! 1-Formation and destruction of H2, HD et CO

   WRITE (iwrit2,3008)

   WRITE (iwrit2,3014)
   DO i = 0, npo
      WRITE (iwrit2,3020) tau(i), timh21(i), timh22(i), timh23(i), timh24(i) &
                        , timhd1(i), timhd2(i), timhd3(i), timhd4(i)
   ENDDO

! 4-Abondances of all species:

   WRITE (iwrit2,3015) speci(i_h), speci(i_h2), speci(i_d), speci(i_hd), speci(i_cp) &
                     , speci(i_c), speci(i_co), speci(i_o), speci(i_hp), speci(i_cs)
   DO i = 0, npo
      WRITE (iwrit2,3020) tau(i), abnua(i_h,i), abnua(i_h2,i),  abnua(i_d, i) &
                  , abnua(i_hd,i), abnua(i_cp,i), abnua(i_c,i), abnua(i_co,i) &
                  , abnua(i_o,i), abnua(i_hp, i), abnua(i_cs,i)
   ENDDO

!--------+---------+---------+---------+---------+---------+---------+-*-------+

!  formats:

 2010  FORMAT (1x,//,5x,'tau',8x,'density',4x,'temp_gas',2x, &
              'temp_dust',2x,'ionisation',/,18x,'cm-3',8x,'K',9x,'K')
 2020  FORMAT (2x,1pe10.3,3x,e9.2,0p,2f10.2,5x,1pe12.4)
 3008  FORMAT(//,10x,'Formation and destruction of H2 et HD:',/, &
                  20x,'in cm-3 s-1')
 3014  FORMAT(//,30x,'H2',30x,'HD',/1x,' tau:      fab chi   des chi    grains   photons', &
             '   fab chi   des chi   photons')
 3015  FORMAT (/,1x,' tau:     ',10(2x,a8))
 3019  FORMAT(//,3x,'tau:   ',7(3x,a8),/)
 3029  FORMAT(//,3x,'tau:   ',9(3x,a8),/)
 3020  FORMAT(1x,1pe9.2,12(2x,e9.2))
 3021  FORMAT(1x,1pe9.2,8(1x,e10.3))
 6000  FORMAT (//,10x,'Cooling and heating rates:',//,10x,'1- Cooling')
 6001  FORMAT (//,10x,'2- Heating',//,1x,'tau:  ',5x,'pel_Gr      H2_gr    HD_gr    phot_mol', &
               '    RC_mol    phs_mol')
 6002  FORMAT (//,1x,'tau:  ',4x,'eq_gaz_gr   eq_chim    cho_turb   ref_tot   cho_tot')

!---------------------------------------------------------------------

END SUBROUTINE SORTIE2

END MODULE PXDR_OUTPUT
