
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

! JLB - 22 May 2008

MODULE PXDR_FGK_TR

  USE PXDR_CONSTANTES

  PRIVATE

  PUBLIC :: FGKTR2, FGKDAT

CONTAINS

!%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE FGKTR2 (iopt)
!%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PXDR_RF_TOOLS

!---------------------------------------------------------------------
!       Computes line absorption using FGK approximation
!       Radiation on both size of the cloud
!       The radiation field is defined in stv1y.f90 and transfer is solved in PXDR_TRANSFER.f90
!---------------------------------------------------------------------

    IMPLICIT NONE

    INTEGER, INTENT (IN)                      :: iopt

!  Used for interpolation

    REAL (KIND=dp), DIMENSION (nlevh2)        :: cdh2f
    REAL (KIND=dp), DIMENSION (nlevhd)        :: cdhdf
    REAL (KIND=dp), DIMENSION (nlevh2)        :: cdh2r
    REAL (KIND=dp), DIMENSION (nlevhd)        :: cdhdr
    REAL (KIND=dp), DIMENSION(:), ALLOCATABLE :: cdc0f
    REAL (KIND=dp), DIMENSION(:), ALLOCATABLE :: cdc1f
    REAL (KIND=dp), DIMENSION(:), ALLOCATABLE :: cdc2f
    REAL (KIND=dp), DIMENSION(:), ALLOCATABLE :: cdc3f
    REAL (KIND=dp), DIMENSION(:), ALLOCATABLE :: cdc0r
    REAL (KIND=dp), DIMENSION(:), ALLOCATABLE :: cdc1r
    REAL (KIND=dp), DIMENSION(:), ALLOCATABLE :: cdc2r
    REAL (KIND=dp), DIMENSION(:), ALLOCATABLE :: cdc3r
    REAL (KIND=dp), DIMENSION (0:noptm)       :: cdtau

    REAL (KIND=dp)                            :: ress
    REAL (KIND=dp)                            :: ajrf
    REAL (KIND=dp)                            :: vt2, tata
    REAL (KIND=dp)                            :: nu0, nu

    REAL (KIND=dp), DIMENSION(:), ALLOCATABLE :: xwl, yvoi, xsum
    REAL (KIND=dp), DIMENSION(0:nwlg)         :: intJ1, intJ2
    REAL (KIND=dp), DIMENSION(0:nwlg)         :: bbsp1, bbsp2

    TYPE (RF_TYPE)  :: rf_tmp

    REAL (KIND=dp)                            :: ff1 = 3.02_dp
    REAL (KIND=dp)                            :: ff2 = 1.0e3_dp
    REAL (KIND=dp)                            :: ff3 = 6.4e-2_dp
    INTEGER                                   :: i, j, iwlg, mm, lev
    INTEGER                                   :: njl, nvl, nbil, krai, njj
    INTEGER                                   :: ilmin, ilmax, ikh2, ikhd, jwl
    REAL (KIND=dp)                            :: cdinf, aux1, aux2
    REAL (KIND=dp)                            :: bdop, ct1, rapwl
    REAL (KIND=dp)                            :: xlam0, gama, betas, betas0
    REAL (KIND=dp)                            :: r, t1, opt0, taudf, u1f
    REAL (KIND=dp)                            :: taudfr, u1fr, ajrfr
    REAL (KIND=dp)                            :: avoi, wlmin, wlmax

    IF (i_co /= 0) THEN
       ALLOCATE (cdc0f(spec_lin(in_sp(i_co))%use))
       ALLOCATE (cdc0r(spec_lin(in_sp(i_co))%use))
    ENDIF
    IF (j_c13o /= 0) THEN
       ALLOCATE (cdc1f(spec_lin(in_sp(j_c13o))%use))
       ALLOCATE (cdc1r(spec_lin(in_sp(j_c13o))%use))
    ENDIF
    IF (j_co18 /= 0) THEN
       ALLOCATE (cdc2f(spec_lin(in_sp(j_co18))%use))
       ALLOCATE (cdc2r(spec_lin(in_sp(j_co18))%use))
    ENDIF
    IF (j_c13o18 /= 0) THEN
       ALLOCATE (cdc3f(spec_lin(in_sp(j_c13o18))%use))
       ALLOCATE (cdc3r(spec_lin(in_sp(j_c13o18))%use))
    ENDIF

    ! Interpolation location in previous grid
    DO j = 1, npo
       IF (tau(iopt) <= tau_o(j)) THEN
          icd_o = j-1
          EXIT
       ENDIF
    ENDDO

    raptau = (tau(iopt) - tau_o(icd_o)) / dtau_o(icd_o+1)

!  Computes radiation field attenuation between the edge of the
!  cloud and present position.
!  See Flannery, Roberge, Rybicki, Ap J, 236, 598 (1980)
!   intJm and abpspp computed in stv2z.f90

   vt2 = vturb * vturb

   intJ1 = 0.0_dp
   intJ2 = 0.0_dp
   bbsp1 = 0.0_dp
   bbsp2 = 0.0_dp
   DO iwlg = 0, nwlg
      intJ1(iwlg) = INTERPCD(raptau, intJm(0:npo,iwlg), icd_o, npo)
      intJ2(iwlg) = INTERPCD(raptau, intJp(0:npo,iwlg), icd_o, npo)
      rf_u%Val(iwlg) = INTERPCD(raptau, intJmp(0:npo,iwlg), icd_o, npo) * J_to_u
      bbsp1(iwlg) = INTERPCD(raptau, bbspm(0:npo,iwlg), icd_o, npo)
      bbsp2(iwlg) = INTERPCD(raptau, bbspp(0:npo,iwlg), icd_o, npo)
   ENDDO

   CALL RF_CREATE(rf_tmp, rf_wl%lower, rf_wl%upper)
   rf_tmp%Ord = rf_u%Ord
   DO mm = 1, npg
      rf_tmp%Val = rf_u%Val
      CALL SECT_PRI(sigabs(mm), rf_tmp, wl_pointer, si_, nwlg)
      si_QU(mm,:) = si_
      rf_tmp%Val = rf_tmp%Val * rf_wl%Val
      CALL SECT_PRI(sigabs(mm), rf_tmp, wl_pointer, si_, nwlg)
      si_QUl(mm,:) = si_
      rf_tmp%Val = rf_tmp%Val * rf_wl%Val
      CALL SECT_PRI(sigabs(mm), rf_tmp, wl_pointer, si_, nwlg)
      si_QUl2(mm,:) = si_
   ENDDO
   CALL RF_FREE (rf_tmp)

!     *********************************************************************
!               ct1= pi e^2/mc = 2.654057e-2_dp
!                  =(section d absorption totale integree sur les frequences)
!                    en cm^2
!               vturb donne en cm s-1
!               bdop:  DIMENSION vitesse cm s-1 a
!      *****************************************************************

   ! CDUNIT Gas to dust ratio : NH = CDUNIT * E(B-V)
   ! RRR Extinction to color index ratio : Av = RRR * E(B-V)
   ct1 = xpi * xqe2 / xme / clum

   ! Computes Doppler width weighted H2 column density for each level
   ! If "one side", THEN the true optical depth towards "dark" side is high
   IF (isoneside == 0) THEN
      cdinf = 1.0_dp
   ELSE
      cdinf = 10.0_dp
   ENDIF

   ! Ratio of turbulent Doppler width to total Doppler width
   ! This ratio does not depend on the line because lambda disapears
   DO lev = 1, l0h2
      cdtau(0:npo) = cdth2(lev,0:npo)
      ress = INTERPCD(raptau, cdtau(0:npo), icd_o, npo)
      cdh2f(lev) = ress
      cdh2r(lev) = MAX(0.0_dp,cdinf * cdtau(npo) - ress)
   ENDDO

   ! Computes Doppler width weighted HD column density for each level
   IF (i_hd /= 0) THEN
      DO lev = 1, l0hd
         cdtau(0:npo) = cdthd(lev,0:npo)
         ress = INTERPCD(raptau, cdtau(0:npo), icd_o, npo)
         cdhdf(lev) = ress
         cdhdr(lev) = MAX(0.0_dp,cdinf * cdtau(npo) - ress)
      ENDDO
   ENDIF

   ! Computes Doppler width weighted CO column density for each level
   IF (i_co /= 0) THEN
      DO j = 1, spec_lin(in_sp(i_co))%use
         cdtau(0:npo) = cdtco(j,:)
         ress = INTERPCD(raptau, cdtau, icd_o, npo)
         cdc0f(j) = ress
         cdc0r(j) = MAX(0.0_dp,cdinf * cdtau(npo) - ress)
      ENDDO
   ENDIF

   ! Computes Doppler width weighted 13CO column density for each level
   IF (i_c13 /= 0) THEN
      DO j = 1, spec_lin(in_sp(j_c13o))%use
         cdtau(0:npo) = cdtc13o(j,:)
         ress = INTERPCD(raptau, cdtau, icd_o, npo)
         cdc1f(j) = ress
         cdc1r(j) = MAX(0.0_dp,cdinf * cdtau(npo) - ress)
      ENDDO
   ENDIF

   ! Computes Doppler width weighted C18O column density for each level
   IF (i_o18 /= 0) THEN
      DO j = 1, spec_lin(in_sp(j_co18))%use
         cdtau(0:npo) = cdtco18(j,:)
         ress = INTERPCD(raptau, cdtau, icd_o, npo)
         cdc2f(j) = ress
         cdc2r(j) = MAX(0.0_dp,cdinf * cdtau(npo) - ress)
      ENDDO
   ENDIF

   ! Computes Doppler width weighted 13C18O column density for each level
   IF (i_c13o18 /= 0) THEN
      DO j = 1, spec_lin(in_sp(j_c13o18))%use
         cdtau(0:npo) = cdtc13o18(j,:)
         ress = INTERPCD(raptau, cdtau, icd_o, npo)
         cdc3f(j) = ress
         cdc3r(j) = MAX(0.0_dp,cdinf * cdtau(npo) - ress)
      ENDDO
   ENDIF

   IF (.NOT. ALLOCATED(fgkrf)) THEN
     ALLOCATE (fgkrf(mfgk), fgkrfr(mfgk))
   ENDIF

   fgkrf = 1.0_dp
   fgkrfr = 1.0_dp

!  Reminder:
!     irah2(bc) 2:       lower level v
!               3:       lower level J
!               4:       upper level v
!               5:       delta J (Jup = Jlo + delj)
!     prah2(bc) 1:       Oscillator strength
!               2:       Wavelength (in A)
!               3:       Inverse radiative lifetime of upper level
!               4:       Upper level dissociation probability

   ! Computes H2 line profiles
   bdop = 1.0e8_dp * SQRT(vt2 + deksamu * tgaz(iopt) / spec_lin(in_sp(i_h2))%mom)

   DO i = 1, nrh2bt
      njl = irah2b(i,2)
      nvl = irah2b(i,1)
      lev = lev_h2(nvl,njl)
      xlam0 = prah2b(i,2)
      gama = prah2b(i,3) / (4.0_dp * xpi)
      betas = bdop / xlam0

      IF (nvl > 0 .OR. njl >= lfgkh2) THEN
         betas0 = 1.0e8_dp * vturb / xlam0
         r     = gama / (sqpi * betas)
         t1    = ff1 * (ff2 * r) ** (-ff3)
         opt0  = ct1 * prah2b(i,1) / (sqpi * betas0)
         taudf = opt0 * cdh2f(lev)
         u1f   = SQRT(taudf * r) / t1
         ajrf  = (r / t1) / SQRT(u1f * u1f + xpi / 4.0_dp)
         taudfr = opt0 * cdh2r(lev)
         u1fr   = SQRT(taudfr * r) / t1
         ajrfr  = (r / t1) / SQRT(u1fr * u1fr + xpi / 4.0_dp)

         fgkrf(i) = MIN(AJDF(taudf) + ajrf, 1.0_dp)
         fgkrfr(i) = MIN(AJDF(taudfr) + ajrfr, 1.0_dp)
      ELSE
         nu0 = 1.0e8_dp * clum / xlam0
         avoi = prah2b(i,3) / (4.0_dp * xpi * betas)
         wlmin = 1.0e8_dp * clum / (nu0 + wlrwid * betas)
         wlmax = 1.0e8_dp * clum / (nu0 - wlrwid * betas)
         CALL RF_LOCATE (rf_wl, wlmin, ilmin)
         CALL RF_LOCATE (rf_wl, wlmax, ilmax)
         nbil = ilmax-ilmin
         ALLOCATE (xwl(0:nbil), yvoi(0:nbil), xsum(0:nbil))
         DO j = 0, nbil
            nu = 1.0e8_dp * clum / rf_wl%Val(j+ilmin)
            xwl(j) = (nu0 - nu) / betas
         ENDDO

         CALL HUMLIK (nbil+1, xwl, avoi, yvoi)

         xsum = yvoi * bbsp1(ilmin:ilmax)
         CALL SIMPINT(xwl, xsum, nbil, tata)
         fgkrf(i) = tata / SQRT(xpi)

         xsum = yvoi * bbsp2(ilmin:ilmax)
         CALL SIMPINT(xwl, xsum, nbil, tata)
         fgkrfr(i) = tata / SQRT(xpi)

         DEALLOCATE (xwl, yvoi, xsum)
      ENDIF
   ENDDO
   krai = nrh2bt

   DO i = 1, nrh2ct
      njl = irah2c(i,2)
      nvl = irah2c(i,1)
      lev = lev_h2(nvl,njl)
      xlam0 = prah2c(i,2)
      gama = prah2c(i,3) / (4.0_dp * xpi)
      betas = bdop / xlam0
      ikh2 = krai + i

      IF (nvl > 0 .OR. njl >= lfgkh2) THEN
         betas0 = 1.0e8_dp * vturb / xlam0
         r     = gama / (sqpi * betas)
         t1    = ff1 * (ff2 * r) ** (-ff3)
         opt0  = ct1 * prah2c(i,1) / (sqpi * betas0)
         taudf = opt0 * cdh2f(lev)
         u1f   = SQRT(taudf * r) / t1
         ajrf  = (r / t1) / SQRT(u1f * u1f + xpi / 4.0_dp)
         taudfr = opt0 * cdh2r(lev)
         u1fr   = SQRT(taudfr * r) / t1
         ajrfr  = (r / t1) / SQRT(u1fr * u1fr + xpi / 4.0_dp)

         fgkrf(ikh2) = MIN(AJDF(taudf) + ajrf, 1.0_dp)
         fgkrfr(ikh2) = MIN(AJDF(taudfr) + ajrfr, 1.0_dp)
      ELSE
         nu0 = 1.0e8_dp * clum / xlam0
         avoi = prah2c(i,3) / (4.0_dp * xpi * betas)
         wlmin = 1.0e8_dp * clum / (nu0 + wlrwid * betas)
         wlmax = 1.0e8_dp * clum / (nu0 - wlrwid * betas)
         CALL RF_LOCATE (rf_wl, wlmin, ilmin)
         CALL RF_LOCATE (rf_wl, wlmax, ilmax)
         nbil = ilmax-ilmin
         ALLOCATE (xwl(0:nbil), yvoi(0:nbil), xsum(0:nbil))
         DO j = 0, nbil
            nu = 1.0e8_dp * clum / rf_wl%Val(j+ilmin)
            xwl(j) = (nu0 - nu) / betas
         ENDDO

         CALL HUMLIK (nbil+1, xwl, avoi, yvoi)

         xsum = yvoi * bbsp1(ilmin:ilmax)
         CALL SIMPINT(xwl, xsum, nbil, tata)
         fgkrf(ikh2) = tata / SQRT(xpi)

         xsum = yvoi * bbsp2(ilmin:ilmax)
         CALL SIMPINT(xwl, xsum, nbil, tata)
         fgkrfr(ikh2) = tata / SQRT(xpi)

         DEALLOCATE (xwl, yvoi, xsum)
      ENDIF
   ENDDO
   krai = krai + nrh2ct

   ! Computes HD line profiles
   IF (i_hd /= 0) THEN

      bdop = 1.0e8_dp * SQRT(vt2 + deksamu * tgaz(iopt) / spec_lin(in_sp(i_hd))%mom)

      DO i = 1, nrhdbt
         njl = irahdb(i,2)
         nvl = irahdb(i,1)
         lev = lev_hd(nvl,njl)
         xlam0 = prahdb(i,2)
         gama = prahdb(i,3) / (4.0_dp * xpi)
         betas = bdop / xlam0
         betas0 = 1.0e8_dp * vturb / xlam0
         r     = gama / (sqpi * betas)
         t1    = ff1 * (ff2 * r) ** (-ff3)
         opt0  = ct1 * prahdb(i,1) / (sqpi * betas0)
         taudf = opt0 * cdhdf(lev)
         u1f   = SQRT(taudf * r) / t1
         ajrf  = (r / t1) / SQRT(u1f * u1f + xpi / 4.0_dp)
         taudfr = opt0 * cdhdr(lev)
         u1fr   = SQRT(taudfr * r) / t1
         ajrfr  = (r / t1) / SQRT(u1fr * u1fr + xpi / 4.0_dp)

         ikhd = krai + i
         fgkrf(ikhd) = MIN(AJDF(taudf) + ajrf, 1.0_dp)
         fgkrfr(ikhd) = MIN(AJDF(taudfr) + ajrfr, 1.0_dp)
      ENDDO
      krai = krai + nrhdbt

      DO i = 1, nrhdct
         njl = irahdc(i,2)
         nvl = irahdc(i,1)
         lev = lev_hd(nvl,njl)
         xlam0 = prahdc(i,2)
         gama = prahdc(i,3) / (4.0_dp * xpi)
         betas = bdop / xlam0
         betas0 = 1.0e8_dp * vturb / xlam0
         r     = gama / (sqpi * betas)
         t1    = ff1 * (ff2 * r) ** (-ff3)
         opt0  = ct1 * prahdc(i,1) / (sqpi * betas0)
         taudf = opt0 * cdhdf(lev)
         u1f   = SQRT(taudf * r) / t1
         ajrf  = (r / t1) / SQRT(u1f * u1f + xpi / 4.0_dp)
         taudfr = opt0 * cdhdr(lev)
         u1fr   = SQRT(taudfr * r) / t1
         ajrfr  = (r / t1) / SQRT(u1fr * u1fr + xpi / 4.0_dp)

         ikhd = krai + i
         fgkrf(ikhd) = MIN(AJDF(taudf) + ajrf, 1.0_dp)
         fgkrfr(ikhd) = MIN(AJDF(taudfr) + ajrfr, 1.0_dp)
      ENDDO
      krai = krai + nrhdct
   ENDIF

   ! Computes CO line profiles
   IF (i_co /= 0) THEN
      bdop  = 1.0e8_dp * SQRT(vt2 + deksamu * tgaz(iopt) / spec_lin(in_sp(i_co))%mom)

      DO i = 1, nrco
         njj   = irco(i) + 1
         IF (njj > spec_lin(in_sp(i_co))%use) CYCLE
         gama  = prco(i,3) / (4.0_dp * xpi)
         xlam0 = prco(i,2)

         betas = bdop / xlam0
         betas0 = 1.0e8_dp * vturb / xlam0
         r     = gama / (sqpi * betas)
         t1    = ff1 * (ff2*r) ** (-ff3)
         opt0  = ct1 * prco(i,1) / (sqpi * betas0)
         taudf = opt0 * cdc0f(njj)
         u1f   = SQRT(taudf * r) / t1
         ajrf  = (r / t1) / SQRT(u1f * u1f + xpi / 4.0_dp)
         taudfr = opt0 * cdc0r(njj)
         u1fr   = SQRT(taudfr * r) / t1
         ajrfr  = (r / t1) / SQRT(u1fr * u1fr + xpi / 4.0_dp)

         fgkrf(krai+i) = MIN(AJDF(taudf) + ajrf, 1.0_dp)
         fgkrfr(krai+i) = MIN(AJDF(taudfr) + ajrfr, 1.0_dp)
      ENDDO
      krai = krai + nrco
   ENDIF

   ! Computes 13CO line profiles
   IF (i_c13o /= 0) THEN
      bdop = 1.0e8_dp * SQRT(vt2 + deksamu * tgaz(iopt) / spec_lin(in_sp(j_c13o))%mom)

      DO i = 1, nrc13o
         njj   = irc13o(i) + 1
         IF (njj > spec_lin(in_sp(j_c13o))%use) CYCLE
         gama  = prc13o(i,3) / (4.0_dp * xpi)
         xlam0  = prc13o(i,2)

         betas  = bdop / xlam0
         betas0 = 1.0e8_dp * vturb / xlam0
         r      = gama / (sqpi*betas)
         t1     = ff1 * (ff2*r) ** (-ff3)
         opt0   = ct1 * prc13o(i,1) / (sqpi * betas0)
         taudf  = opt0 * cdc1f(njj)
         u1f    = SQRT(taudf * r) / t1
         ajrf   = (r / t1) / SQRT(u1f * u1f + xpi / 4.0_dp)
         taudfr = opt0 * cdc1r(njj)
         u1fr   = SQRT(taudfr * r) / t1
         ajrfr  = (r / t1) / SQRT(u1fr * u1fr + xpi / 4.0_dp)

         fgkrf(krai+i) = MIN(AJDF(taudf) + ajrf, 1.0_dp)
         fgkrfr(krai+i) = MIN(AJDF(taudfr) + ajrfr, 1.0_dp)
      ENDDO
      krai = krai + nrc13o
   ENDIF

   ! Computes C18O line profiles
   IF (i_co18 /= 0) THEN
      bdop = 1.0e8_dp * SQRT(vt2 + deksamu * tgaz(iopt) / spec_lin(in_sp(j_co18))%mom)

      DO i = 1, nrco18
         njj   = irco18(i) + 1
         IF (njj > spec_lin(in_sp(j_co18))%use) CYCLE
         gama  = prco18(i,3) / (4.0_dp * xpi)
         xlam0  = prco18(i,2)

         betas  = bdop / xlam0
         betas0 = 1.0e8_dp * vturb / xlam0
         r      = gama / (sqpi*betas)
         t1     = ff1 * (ff2*r) ** (-ff3)
         opt0   = ct1 * prco18(i,1) / (sqpi * betas0)
         taudf  = opt0 * cdc2f(njj)
         u1f    = SQRT(taudf * r) / t1
         ajrf   = (r / t1) / SQRT(u1f * u1f + xpi / 4.0_dp)
         taudfr = opt0 * cdc2r(njj)
         u1fr   = SQRT(taudfr * r) / t1
         ajrfr  = (r / t1) / SQRT(u1fr * u1fr + xpi / 4.0_dp)

         fgkrf(krai+i) = MIN(AJDF(taudf) + ajrf, 1.0_dp)
         fgkrfr(krai+i) = MIN(AJDF(taudfr) + ajrfr, 1.0_dp)
      ENDDO
      krai = krai + nrco18
   ENDIF

   ! Computes 13C18O line profiles
   IF (i_c13o18 /= 0) THEN
      bdop = 1.0e8_dp * SQRT(vt2 + deksamu * tgaz(iopt) / spec_lin(in_sp(j_c13o18))%mom)

      DO i = 1, nrc13o18
         njj   = irc13o18(i) + 1
         IF (njj > spec_lin(in_sp(j_c13o18))%use) CYCLE
         gama  = prc13o18(i,3) / (4.0_dp * xpi)
         xlam0  = prc13o18(i,2)

         betas  = bdop / xlam0
         betas0 = 1.0e8_dp * vturb / xlam0
         r      = gama / (sqpi*betas)
         t1     = ff1 * (ff2*r) ** (-ff3)
         opt0   = ct1 * prc13o18(i,1) / (sqpi * betas0)
         taudf  = opt0 * cdc3f(njj)
         u1f    = SQRT(taudf * r) / t1
         ajrf   = (r / t1) / SQRT(u1f * u1f + xpi / 4.0_dp)
         taudfr = opt0 * cdc3r(njj)
         u1fr   = SQRT(taudfr * r) / t1
         ajrfr  = (r / t1) / SQRT(u1fr * u1fr + xpi / 4.0_dp)

         fgkrf(krai+i) = MIN(AJDF(taudf) + ajrf, 1.0_dp)
         fgkrfr(krai+i) = MIN(AJDF(taudfr) + ajrfr, 1.0_dp)
      ENDDO
   ENDIF

   ! Computes radiation field in slab
   DO iwlg = 1, nrh2bt
      nvl = irah2b(iwlg,1)
      njl = irah2b(iwlg,2)
      IF (nvl > 0 .OR. njl >= lfgkh2) THEN
         xlam0 = fgkwl(iwlg)
         CALL RF_LOCATE(rf_wl, xlam0, jwl)
         rapwl = (xlam0 - rf_wl%Val(jwl)) / (rf_wl%Val(jwl+1) - rf_wl%Val(jwl))
         aux1 = intJ1(jwl) + (intJ1(jwl+1) - intJ1(jwl)) * rapwl
         aux2 = intJ2(jwl) + (intJ2(jwl+1) - intJ2(jwl)) * rapwl
         fgkgrd(iwlg) = aux1 * fgkrf(iwlg) + aux2 * fgkrfr(iwlg)
         fgkgrd(iwlg) = fgkgrd(iwlg) * J_to_u
      ENDIF
   ENDDO
   DO iwlg = nrh2bt+1, nrh2bt+nrh2ct
      nvl = irah2c(iwlg-nrh2bt,1)
      njl = irah2c(iwlg-nrh2bt,2)
      IF (nvl > 0 .OR. njl >= lfgkh2) THEN
         xlam0 = fgkwl(iwlg)
         CALL RF_LOCATE(rf_wl, xlam0, jwl)
         rapwl = (xlam0 - rf_wl%Val(jwl)) / (rf_wl%Val(jwl+1) - rf_wl%Val(jwl))
         aux1 = intJ1(jwl) + (intJ1(jwl+1) - intJ1(jwl)) * rapwl
         aux2 = intJ2(jwl) + (intJ2(jwl+1) - intJ2(jwl)) * rapwl
         fgkgrd(iwlg) = aux1 * fgkrf(iwlg) + aux2 * fgkrfr(iwlg)
         fgkgrd(iwlg) = fgkgrd(iwlg) * J_to_u
      ENDIF
   ENDDO
   DO iwlg = nrh2bt+nrh2ct+1, mfgk
      xlam0 = fgkwl(iwlg)
      CALL RF_LOCATE(rf_wl, xlam0, jwl)
      rapwl = (xlam0 - rf_wl%Val(jwl)) / (rf_wl%Val(jwl+1) - rf_wl%Val(jwl))
      aux1 = intJ1(jwl) + (intJ1(jwl+1) - intJ1(jwl)) * rapwl
      aux2 = intJ2(jwl) + (intJ2(jwl+1) - intJ2(jwl)) * rapwl
      fgkgrd(iwlg) = aux1 * fgkrf(iwlg) + aux2 * fgkrfr(iwlg)
      fgkgrd(iwlg) = fgkgrd(iwlg) * J_to_u
   ENDDO

   IF (i_co /= 0) THEN
      DEALLOCATE (cdc0f)
      DEALLOCATE (cdc0r)
   ENDIF
   IF (j_c13o /= 0) THEN
      DEALLOCATE (cdc1f)
      DEALLOCATE (cdc1r)
   ENDIF
   IF (j_co18 /= 0) THEN
      DEALLOCATE (cdc2f)
      DEALLOCATE (cdc2r)
   ENDIF
   IF (j_c13o18 /= 0) THEN
      DEALLOCATE (cdc3f)
      DEALLOCATE (cdc3r)
   ENDIF

END SUBROUTINE FGKTR2

!%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE FGKDAT (iopt)
!%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PXDR_RF_TOOLS
   USE PXDR_PROFIL

!  Prepares data for H2, HD and CO detailed balance
!  Computes H2, HD and CO photodissociation rates
!  Computes C and S ionisation rates

    IMPLICIT NONE

    INTEGER, INTENT (IN)                      :: iopt

    TYPE (RF_TYPE)                            :: rf_tmp, rf_tmp1

    REAL (KIND=dp), DIMENSION(:), ALLOCATABLE :: xwl, yvoi, xsum
    REAL (KIND=dp)                            :: nu0, nu
    REAL (KIND=dp)                            :: coefb, vt2
    INTEGER                                   :: j, k, ik, nbil, krai, levl
    INTEGER                                   :: nvl, njl, lev, kdeph
    INTEGER                                   :: nvu, nju, ndj, i2, ilmax, ilmin
    INTEGER                                   :: ivu, iju, ikh2, ikhd, nj2
    REAL (KIND=dp)                            :: dumh2, dumhd, dumy
    REAL (KIND=dp)                            :: dumco
    REAL (KIND=dp)                            :: bdop, xlam0, chrai, betas, avoi
    REAL (KIND=dp)                            :: wlmin, wlmax, tata
    REAL (KIND=dp)                            :: probdi, beinst, absorb
    REAL (KIND=dp)                            :: disici

    SAVE

    ! Numerical coeficient : pi e**2 / (me * h * c**2) * 1.0e-16 (for Angstrom)
    coefb = 1.0e-16_dp * xpi * xqe2 / (xme * xhp * clum*clum)
    vt2 = vturb * vturb

    ! Compute energy density from Lyman cut-off up to Habing cut-off (2400 Angstrom)
    ! JLB - 7 I 2013: full energy density is not used. Using Habing cut-off gives G0
    CALL RF_CREATE(rf_tmp, 1, 2)
    CALL RF_LOCATE(rf_wl, wlth0, i2)
    rf_tmp%Val(1) = 1.0_dp
    rf_tmp%Ord(1) = rf_wl%Ord(i2)
    CALL RF_LOCATE(rf_wl, wl_hab, i2)
    rf_tmp%Val(2) = 1.0_dp
    rf_tmp%Ord(2) = rf_wl%Ord(i2)
    int_rad(iopt) = SECT_INT(rf_tmp, rf_u, wl_pointer, wl_hab)
    CALL RF_FREE (rf_tmp)

    ! Compute photon flux (used in chemistry)
    ! 5 VIII 09 - Photon flux is used only for desorption from grains.
    !           - Threshold set at 2000 A rather arbitrarily (JLB)
    CALL RF_LOCATE(rf_wl, 2000.0_dp, i2)
    CALL RF_CREATE(rf_tmp1, rf_wl%lower, rf_wl%upper)
    rf_tmp1%Val(rf_wl%lower:i2) = rf_u%Val(rf_wl%lower:i2) * rf_wl%Val(rf_wl%lower:i2)
    rf_tmp1%Val(i2+1:rf_wl%upper) = 0.0_dp
    fluph(iopt) = RF_INTEGRATION (rf_wl,rf_tmp1) * 1.0e-8_dp / xhp
    CALL RF_FREE (rf_tmp1)

    ! calcul du taux de photoionisation de toutes les especes
    ! a partir des sections de photodestruction contenues dans
    ! les fichiers data/section/*.dat : Mars 2006
    CALL RF_CREATE (rf_tmp1, 0, nwlg)
    rf_tmp1%Val = rf_wl%Val * rf_u%Val
    DO k = 1, nphot_i - i_mandat
       pdiesp(k,iopt) = SECT_INT (phdest(k)%rf_esp, rf_tmp1, wl_pointer, phdest(k)%wl_cut_esp) &
                      / (1.0e8_dp * xhp)
    ENDDO
    CALL RF_FREE(rf_tmp1)

    ! End modif - 14 Jan au 12 Fev 2002
    ! Radiative pumping
    !    H2
    evrdbc(1:l0h2,1:l0h2) = 0.0_dp
    IF (i_hd /= 0) THEN
       evrdhd(1:l0hd,1:l0hd) = 0.0_dp
    ENDIF

    IF (i_co /= 0) THEN
       eqrco = 0.0_dp
    ENDIF
    IF (i_c13o /= 0) THEN
       eqrc13o = 0.0_dp
    ENDIF
    IF (i_co18 /= 0) THEN
       eqrco18 = 0.0_dp
    ENDIF
    IF (i_c13o18 /= 0) THEN
       eqrc13o18 = 0.0_dp
    ENDIF

    dumh2 = 0.0_dp
    dumhd = 0.0_dp
    dumy = 0.0_dp
    pdh2vJ(:,iopt) = 0.0_dp

    !-----------------------------
    !  H2 dissociation probability
    !-----------------------------
    bdop = 1.0e8_dp * SQRT(vt2 + deksamu * tgaz(iopt) / spec_lin(in_sp(i_h2))%mom)

    DO ik = 1, nrh2bt
       nvl = irah2b(ik,1)
       njl = irah2b(ik,2)
       lev = lev_h2(nvl,njl)
       xlam0 = prah2b(ik,2)

       IF (nvl > 0 .OR. njl >= lfgkh2) THEN
          chrai = fgkgrd(ik)
       ELSE
          nu0 = 1.0e8_dp * clum / xlam0
          betas = bdop / xlam0
          avoi = prah2b(ik,3) / (4.0_dp * xpi * betas)
          wlmin = 1.0e8_dp * clum / (nu0 + wlrwid * betas)
          wlmax = 1.0e8_dp * clum / (nu0 - wlrwid * betas)
          CALL RF_LOCATE (rf_wl, wlmin, ilmin)
          CALL RF_LOCATE (rf_wl, wlmax, ilmax)
          nbil = ilmax-ilmin
          ALLOCATE (xwl(0:nbil), yvoi(0:nbil), xsum(0:nbil))
          DO j = 0, nbil
             nu = 1.0e8_dp * clum / rf_wl%Val(j+ilmin)
             xwl(j) = (nu0 - nu) / betas
          ENDDO

          CALL HUMLIK (nbil+1, xwl, avoi, yvoi)

          xsum = yvoi * rf_u%Val(ilmin:ilmax) * 1.0e8 * clum / (betas * rf_wl%Val(ilmin:ilmax)**2)
          CALL SIMPINT(rf_wl%Val(ilmin:ilmax), xsum, nbil, tata)
          chrai = tata / SQRT(xpi)
          DEALLOCATE (xwl, yvoi, xsum)
       ENDIF

       nvu = irah2b(ik,3)
       ndj = irah2b(ik,4)
       nju = njl + ndj

       probdi = prah2b(ik,4)
       beinst = coefb * prah2b(ik,1) * (xlam0**3)
       absorb = beinst * chrai
       disici = absorb * probdi
       dumh2 = dumh2 + disici * xreh2(lev,iopt)
       pdh2vJ(lev,iopt) = pdh2vJ(lev,iopt) + disici * xreh2(lev,iopt)

       ! calcul explicite des populations de la bande B
       IF ((nvu <= nvbc) .AND. (nju <= njbc)) THEN
          h2bnua(nvu,nju,iopt) = h2bnua(nvu,nju,iopt) + absorb * xreh2(lev,iopt)
       ENDIF

       ! Radiative equilibrium matrix
       ! 5 IX 08 - JLB - Suppress dissociation from equilibrium matrix (now taken care of in chemistry)
       evrdbc(lev,lev) = evrdbc(lev,lev) + absorb * (1.0_dp - probdi)

       IF (MOD(njl,2) == 0) THEN
          evrdbc(levpc,lev) = evrdbc(levpc,lev) - absorb * (1.0_dp - probdi) * bhmbx(nvu,nju,levpc)
       ELSE
          evrdbc(levic,lev) = evrdbc(levic,lev) - absorb * (1.0_dp - probdi) * bhmbx(nvu,nju,levic)
       ENDIF
    ENDDO

    ! End of upper levels population computation
    DO ivu = 0, nvbc
       DO iju = 0, njbc
          h2bnua(ivu,iju,iopt) = h2bnua(ivu,iju,iopt) / tmunh2bc(1,ivu,iju)
       ENDDO
    ENDDO
    krai = nrh2bt

    DO ik = 1, nrh2ct
       ikh2 = krai + ik
       nvl = irah2c(ik,1)
       njl = irah2c(ik,2)
       lev = lev_h2(nvl,njl)
       xlam0 = prah2c(ik,2)

       IF (nvl > 0 .OR. njl >= lfgkh2) THEN
          chrai = fgkgrd(ikh2)
       ELSE
          nu0 = 1.0e8_dp * clum / xlam0
          betas = bdop / xlam0
          avoi = prah2c(ik,3) / (4.0_dp * xpi * betas)
          wlmin = 1.0e8_dp * clum / (nu0 + wlrwid * betas)
          wlmax = 1.0e8_dp * clum / (nu0 - wlrwid * betas)
          CALL RF_LOCATE (rf_wl, wlmin, ilmin)
          CALL RF_LOCATE (rf_wl, wlmax, ilmax)
          nbil = ilmax-ilmin
          ALLOCATE (xwl(0:nbil), yvoi(0:nbil), xsum(0:nbil))
          DO j = 0, nbil
             nu = 1.0e8_dp * clum / rf_wl%Val(j+ilmin)
             xwl(j) = (nu0 - nu) / betas
          ENDDO

          CALL HUMLIK (nbil+1, xwl, avoi, yvoi)

          xsum = yvoi * rf_u%Val(ilmin:ilmax) * 1.0e8 * clum / (betas * rf_wl%Val(ilmin:ilmax)**2)
          CALL SIMPINT(rf_wl%Val(ilmin:ilmax), xsum, nbil, tata)
          chrai = tata / SQRT(xpi)
          DEALLOCATE (xwl, yvoi, xsum)
       ENDIF

       nvu = irah2c(ik,3)
       ndj = irah2c(ik,4)
       nju = njl + ndj

       probdi = prah2c(ik,4)
       beinst = coefb * prah2c(ik,1) * (xlam0**3)
       absorb = beinst * chrai
       disici = absorb * probdi
       dumh2 = dumh2 + disici * xreh2(lev,iopt)
       pdh2vJ(lev,iopt) = pdh2vJ(lev,iopt) + disici * xreh2(lev,iopt)

       ! calcul explicite des populations de la bande C
       IF ((nvu <= nvbc) .AND. (nju <= njbc)) THEN
          h2cnua(nvu,nju,iopt) = h2cnua(nvu,nju,iopt) + absorb * xreh2(lev,iopt)
       ENDIF

       ! Construction de la matrice d'equilibre radiatif
       evrdbc(lev,lev) = evrdbc(lev,lev) + absorb * (1.0_dp - probdi)

       IF (MOD(njl,2) == 0) THEN
          evrdbc(levpc,lev) = evrdbc(levpc,lev) - absorb * (1.0_dp - probdi) * bhmcx(nvu,nju,levpc)
       ELSE
          evrdbc(levic,lev) = evrdbc(levic,lev) - absorb * (1.0_dp - probdi) * bhmcx(nvu,nju,levic)
       ENDIF
    ENDDO

    ! Fin du calcul des populations des niveaux hauts
    DO ivu = 0, nvbc
       DO iju = 0, njbc
          IF (tmunh2bc(2,ivu,iju) > 1.0e-30_dp) THEN
             h2cnua(ivu,iju,iopt) = h2cnua(ivu,iju,iopt) / tmunh2bc(2,ivu,iju)
          ELSE
             h2cnua(ivu,iju,iopt) = -1.0_dp
          ENDIF
       ENDDO
    ENDDO
    kdeph = kdeph0
    pdiesp(kdeph,iopt) = dumh2
    krai = krai + nrh2ct

!----------------------------------------------
!  HD dissociation probability
!-----------------------------------------------

    IF (i_hd /= 0) THEN
       DO ik = 1, nrhdbt
          ikhd = krai + ik
          nvl = irahdb(ik,1)
          njl = irahdb(ik,2)
          lev = lev_hd(nvl,njl)
          xlam0 = prahdb(ik,2)
          chrai = fgkgrd(ikhd)
          IF (lev > l0hd .OR. chrai <= 0.0_dp) CYCLE

          nvu = irahdb(ik,3)
          ndj = irahdb(ik,4)
          nju = njl + ndj

          probdi = prahdb(ik,4)
          beinst = coefb * prahdb(ik,1) * (xlam0**3)
          absorb = beinst * chrai
          disici = absorb * probdi
          dumhd = dumhd + disici * xrehd(lev,iopt)

          evrdhd(lev,lev) = evrdhd(lev,lev) + absorb

          DO levl = 1, l0hd
             nj2 = njl_hd(levl)
             dumy = absorb * (1.0_dp - probdi) * bhdbx(nvu,nju,levl)
             evrdhd(levl,lev) = evrdhd(levl,lev) - dumy
          ENDDO
       ENDDO
       krai = krai + nrhdbt

       DO ik = 1, nrhdct
          ikhd = krai + ik
          nvl = irahdc(ik,1)
          njl = irahdc(ik,2)
          lev = lev_hd(nvl,njl)
          xlam0 = prahdc(ik,2)
          chrai = fgkgrd(ikhd)
          IF (lev > l0hd .OR. chrai <= 0.0_dp) CYCLE

          nvu = irahdc(ik,3)
          ndj = irahdc(ik,4)
          nju = njl + ndj

          probdi = prahdc(ik,4)
          beinst = coefb * prahdc(ik,1) * (xlam0**3)
          absorb = beinst * chrai
          disici = absorb * probdi
          dumhd = dumhd + disici * xrehd(lev,iopt)

          evrdhd(lev,lev) = evrdhd(lev,lev) + absorb

          DO levl = 1, l0hd
             nj2 = njl_hd(levl)
             IF (ndj == 0) THEN
                dumy = absorb * (1.0_dp - probdi) * bhdcmx(nvu,nju,levl)
                evrdhd(levl,lev) = evrdhd(levl,lev) - dumy
             ELSE
                dumy = absorb * (1.0_dp - probdi) * bhdcpx(nvu,nju,levl)
                evrdhd(levl,lev) = evrdhd(levl,lev) - dumy
             ENDIF
          ENDDO
       ENDDO
       kdeph = kdeph + 1
       pdiesp(kdeph,iopt) = dumhd
       krai = krai + nrhdct
    ENDIF

!------------------------------------------------------
!  CO dissociation probability
!------------------------------------------------------

!  coeff num: coefb = (pi*e**2)/(m*h*c**2)=1.336d14
!     * 1.0e-16_dp (pour tenir compte des longueurs d onde en a).

    IF (i_co /= 0) THEN
       dumco = 0.0_dp
       DO ik = 1, nrco
          njl = irco(ik)
          IF ((njl+1) > spec_lin(in_sp(i_co))%use) CYCLE
          probdi = prco(ik,4)
          xlam0 = prco(ik,2)
          beinst = coefb * prco(ik,1) * (xlam0**3)
          chrai = fgkgrd(krai+ik)
          absorb = beinst * chrai
          dumco = dumco + absorb * probdi * xreco(njl+1,iopt)
          eqrco(njl+1) = eqrco(njl+1) + absorb * probdi
       ENDDO
       kdeph = kdeph + 1
       pdiesp(kdeph,iopt) = dumco
       krai = krai + nrco
    ENDIF

!  13CO dissociation probability
!  ----------------------------

    IF (i_c13o /= 0) THEN
       dumco = 0.0_dp
       DO ik = 1, nrc13o
          njl = irc13o(ik)
          IF ((njl+1) > spec_lin(in_sp(j_c13o))%use) CYCLE
          probdi = prc13o(ik,4)
          xlam0 = prc13o(ik,2)
          beinst = coefb * prc13o(ik,1) * (xlam0**3)
          chrai = fgkgrd(krai+ik)
          absorb = beinst * chrai
          dumco = dumco + absorb * probdi * xrec13o(njl+1,iopt)
          eqrc13o(njl+1) = eqrc13o(njl+1) + absorb * probdi
       ENDDO
       kdeph = kdeph + 1
       pdiesp(kdeph,iopt) = dumco
       krai = krai + nrc13o
    ENDIF

!  C18O dissociation probability
!  ---------------------------------

    IF (i_co18 /= 0) THEN
       dumco = 0.0_dp
       DO ik = 1, nrco18
          njl = irco18(ik)
          IF ((njl+1) > spec_lin(in_sp(j_co18))%use) CYCLE
          probdi = prco18(ik,4)
          xlam0 = prco18(ik,2)
          beinst = coefb * prco18(ik,1) * (xlam0**3)
          chrai = fgkgrd(krai+ik)
          absorb = beinst * chrai
          dumco = dumco + absorb * probdi * xreco18(njl+1,iopt)
          eqrco18(njl+1) = eqrco18(njl+1) + absorb * probdi
       ENDDO
       kdeph = kdeph + 1
       pdiesp(kdeph,iopt) = dumco
       krai = krai + nrco18
    ENDIF

!  13C18O dissociation probability
!  ---------------------------------

    IF (i_c13o18 /= 0) THEN
       dumco = 0.0_dp
       DO ik = 1, nrc13o18
          njl = irc13o18(ik)
          IF ((njl+1) > spec_lin(in_sp(j_c13o18))%use) CYCLE
          probdi = prc13o18(ik,4)
          xlam0 = prc13o18(ik,2)
          beinst = coefb * prc13o18(ik,1) * (xlam0**3)
          chrai = fgkgrd(krai+ik)
          absorb = beinst * chrai
          dumco = dumco + absorb * probdi * xrec13o18(njl+1,iopt)
          eqrc13o18(njl+1) = eqrc13o18(njl+1) + absorb * probdi
       ENDDO
       kdeph = kdeph + 1
       pdiesp(kdeph,iopt) = dumco
    ENDIF

END SUBROUTINE FGKDAT

!--------------------------------------------------------------------------

!  Interpolation FUNCTION, utilisee pour determinee la colonne densitee
!           entre le point courant et le centre du nuage

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REAL (KIND=dp) FUNCTION INTERPCD(rapp, cd_o, i, npo)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES

   IMPLICIT NONE

   INTEGER, INTENT (IN)                           :: i, npo
   REAL (KIND=dp), INTENT (IN)                    :: rapp
   REAL (KIND=dp), DIMENSION (0:npo), INTENT (IN) :: cd_o

   REAL (KIND=dp) cd_c

   cd_c = cd_o(i) + (cd_o(i+1) - cd_o(i)) * rapp
   INTERPCD = cd_c

END FUNCTION INTERPCD

!--------------------------------------------------------------------------

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REAL (KIND=dp) FUNCTION AJDF(taudf)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   REAL (KIND=dp), INTENT (IN) ::  taudf

   IF (taudf < 2.0_dp) THEN
      AJDF = EXP(-2.0_dp * taudf / 3.0_dp)
   ELSE IF (taudf < 10.0_dp) THEN
      AJDF = 0.638_dp * taudf**(-1.25_dp)
   ELSE IF (taudf < 1.0d2) THEN
      AJDF = 0.505_dp * taudf**(-1.15_dp)
   ELSE
      AJDF = 0.344_dp * taudf**(-1.0667_dp)
   ENDIF

END FUNCTION AJDF

END MODULE PXDR_FGK_TR
