#============================================================================#
# This makefile will produce the 2 codes :                                   #
# 1 - PDR  : the PDR code                                                    #
#            Use : ./PDR ../data/NAME_OF_INPUT_FILE                          #
#            by default ../data/pdr6.in is used                              #
#                                                                            #
# 2 - PREP : A post-processing code to analyse the PDR results stored in     #
#            binary files                                                    #
#                                                                            #
# Compilation of DUSTEM has to be done separately in the DUSTEM directory    #
#============================================================================#

#============================================================================#
#                               CONFIGURATION                                #
#                                                                            #
#     1 - Set the name of your Fortran 90 compiler - variable FC             #
#     2 - Set the compilation options (variable FFLAGS)                      #
#     3 - If necessary set the PATH to your libraries LAPACK and BLAS        #
#         Variable : LAPACK_PATH - add -L$(LAPACK_PATH) to compilation line  #
#     4 - If necessary set the PATH to your cfitsio library                  #
#         Variable : CFITSIO_PATH - add -L$(CFITSIO_PATH)                    #
#     5 - Provide the names of your LAPACK, BLAS and other fortran libraries #
#         Variable : LIBS1                                                   #
#     6 - Provide the name of you cfitsio library                            #
#         Variable : LIBS2                                                   #
#                                                                            #
#     Opt. : On Apple computers, VecLib can be used instead of LAPACK - BLAS #
#            To do it, do not provide lapack and blas in LIBS                #
#            Instead, select the veclib line at the end of the compilation   #
#                                                                            #
#============================================================================#

#============================================================================#
#                            USING VecLib on a Mac                           #
#============================================================================#
#                                                                            #
# To use the optimized Lapack on a Mac do not specify LAPACK_PATH but a the  #
# end of the Makefile replace the line :                                     #
#  $(FC) $(FFLAGS) PXDR.o $(OBJstv) -llapack -lblas -L$(LAPACK_PATH) -o pxdr
#    by :                                                                    #
#  $(FC) PXDR.o $(OBJstv) -Wl,-framework -Wl,vecLib -o pxdr
#                                                                            #
# and the line :                                                             #
#  $(FC) $(FFLAGS) PREP.o $(OBJprep) $(OBJstv) -llapack -lblas -L$(LAPACK_PATH) -o prep
#    by :                                                                    #
#  $(FC) PREP.o $(OBJprep) $(OBJstv) -Wl,-framework -Wl,vecLib -o prep
#                                                                            #
#============================================================================#

#============================================================================#
#                                IFORT (Intel)                               #
#============================================================================#
#  FC = ifort

#- Version 12.0
#  FFLAGS = -g -O2 -parallel -mkl -xSSSE3 -axSSSE3 -m64
#  FFLAGS = -O2 -parallel
#  FFLAGS = -g3 -O0 -mkl -check all -warn all -traceback -debug all -ftrapuv -fpe0 -diag-enable warn -fp-stack-check -mp1 -stand f03

#  LIBS1 = -lpthread
#  LIBS2 = -lcfitsio

#============================================================================#
#                                  GFORTRAN                                  #
#============================================================================#
  FC = gfortran

# Options and Path
# FFLAGS = -g3 -O0 -fno-second-underscore -Wall -Wextra -fcheck=all -fimplicit-none -std=f2003 -pedantic -ffpe-trap=zero,overflow -fbacktrace -gdwarf-2 -fall-intrinsics -Wno-unused-function
  FFLAGS = -g -Ofast -fno-second-underscore -falign-loops=16 -msse2 -ffast-math -fassociative-math -fno-ipa-cp-clone

  LIBS1 = -llapack -lblas
  LIBS2 = -lcfitsio
#  LAPACK_PATH = /usr/local/lib/
#  BLAS_PATH = /usr/local/lib/libblas.a
#  CFITSIO_PATH = /usr/local/lib/libcfitsio.a
#  LIBS1 = -llapack -lblas -L$(LAPACK_PATH) -L$(BLAS_PATH)
#  LIBS2 = -lcfitsio -L$(CFITSIO_PATH)


#============================================================================#
#                                    G95                                     #
#============================================================================#
# FC = g95

# Options and Path
# FFLAGS = -O2 -fno-second-underscore
# FFLAGS = -g3 -fno-second-underscore -Wall -Wextra -fbounds-check -ftrace=full -Wno=112
# FFLAGS = -g3 -m64 -gdwarf-2 -fno-second-underscore -Wall -Wextra -fbounds-check -ftrace=full -Wno=112,136,161,102,165 -finteger=99999999 -flogical=none -freal=nan -fpointer=invalid

# LIBS1 = -llapack -lblas
# LIBS2 = -lcfitsio

#============================================================================#
#                       Add your compiler here                               #
#============================================================================#

.MAKEOPTS: -k -s

.SUFFIXES: .f90

.f90.o:
	$(FC) $(FFLAGS) -c $*.f90

OBJstv = PXDR_CONSTANTES.o \
         PXDR_AUXILIAR.o \
         PXDR_UTIL_XML.o \
         PXDR_UTIL_FITS.o \
         PXDR_CHEM_DATA.o \
         PXDR_PROFIL.o \
         PXDR_DRAINE_DUST.o \
         PXDR_STR_DATA.o \
         PXDR_RF_TOOLS.o \
         PXDR_ISOTCO.o \
         PXDR_READCHIM.o \
         PXDR_DUSTEM.o \
         PXDR_TRANSFER.o \
         PXDR_INITIAL.o \
         PXDR_INIFITS.o \
         PXDR_OUTPUT.o \
         PXDR_OUTFITS.o \
         PXDR_FGK_TR.o \
         PXDR_CHEMISTRY.o \
         PXDR_EXCITH2.o \
         PXDR_EXCITHD.o \
         PXDR_COLSPE.o \
         PXDR_BILTHERM.o

OBJprep = PREP_VAR.o \
          PREP_INTEMI.o \
          PREP_LECTUR.o \
          PREP_ABONDA.o \
          PREP_EXCIT.o \
          PREP_COLDEN.o \
          PREP_EMISSI.o \
          PREP_CHOFRO.o \
          PREP_AUTRE.o \
          PREP_ANALYS.o \
          PREP_SPHERE.o

ALL: PDR PREP PREPMOD
	echo "Compilation finished"

PDR: $(OBJstv) PXDR.o
	$(FC) $(FFLAGS) PXDR.o $(OBJstv) $(LIBS1) $(LIBS2) -o PDR
#	$(FC) $(FFLAGS) PXDR.o $(OBJstv) -Wl,-framework -Wl,vecLib $(LIBS1) $(LIBS2) -o PDR

PREP: $(OBJprep) $(OBJstv) PREP.o
	$(FC) $(FFLAGS) PREP.o $(OBJprep) $(OBJstv) $(LIBS1) $(LIBS2) -o PREP
#	$(FC) $(FFLAGS) PREP.o $(OBJprep) $(OBJstv) -Wl,-framework -Wl,vecLib $(LIBS1) $(LIBS2) -o PREP

PREPMOD: $(OBJprep) $(OBJstv) PREPMOD.o
	$(FC) $(FFLAGS) PREPMOD.o $(OBJprep) $(OBJstv) $(LIBS1) $(LIBS2) -o PREPMOD

clean:
	rm -f -r *.o *.s PDR PREP *mod *.dSYM

