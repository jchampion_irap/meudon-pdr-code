! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

MODULE PXDR_OUTFITS

USE PXDR_CONSTANTES
USE PXDR_CHEM_DATA
USE PXDR_PROFIL
USE PXDR_AUXILIAR
USE PXDR_STR_DATA
USE PXDR_RF_TOOLS
USE PXDR_TRANSFER
USE PXDR_INITIAL
USE PXDR_UTIL_XML
USE PXDR_UTIL_FITS

IMPLICIT NONE

CHARACTER(LEN=lenfilename) :: namefits        ! Name of FITS file without directory,   Ex : toto.fits
CHARACTER(LEN=lenfilename) :: filefits        ! Name of FITS file with directory path, Ex : ../out/toto.fits
CHARACTER(LEN=lenfilename) :: fileuv          ! Name of the .rf file
CHARACTER(LEN=lenfilename) :: fileuvfits      ! nom du fichier .fits pour le spectre .uv
CHARACTER(LEN=lenfilename) :: filexml         ! nom du fichier .xml
INTEGER, PARAMETER         :: unitxml = 23    ! Logical number of XML file

TYPE(DUXML)                :: blocDUxml       ! Metadata used in XML and FITS headers
CHARACTER(LEN=lenpath)     :: href            ! href value in VO-TABLE

INTEGER                    :: naxe            ! Number of axes to which a quantity is related
TYPE(DUAXE)                :: descaxe(naxmax) ! Description of axes

INTEGER                    :: numhdu          ! Index of current DataUnit in FITS file
CHARACTER(LEN=lenID)       :: extname         ! ID of DataUnit / Group
CHARACTER(LEN=lenname)     :: name            ! Human readable name of DataUnit / Group
INTEGER                    :: nlgn            ! Number of lines in DataUnit
INTEGER                    :: ncol            ! Number of columns in DataUnit
CHARACTER(LEN=lentw)       :: type_write      ! Flag : determine the kind of writting in FITS file
                                              ! Routine to write in FITS depends on ??? (integer, real, ...)
                                              ! Special calls are done for table with mixt categories
INTEGER                    :: ncool           ! Number of coolants in the model
INTEGER                    :: nmom            ! Number of moments used in the simulation
CHARACTER(LEN=lennamespecy), ALLOCATABLE :: species(:)
CHARACTER(LEN=lennamespecy), ALLOCATABLE :: moments(:)
REAL(KIND=dp),               ALLOCATABLE :: massmol(:)
REAL(KIND=dp),               ALLOCATABLE :: enthalp(:)
REAL(KIND=dp),               ALLOCATABLE :: abinitial(:)
REAL(KIND=dp),               ALLOCATABLE :: temp_evap(:)
REAL(KIND=dp),               ALLOCATABLE :: temp_diff(:)
CHARACTER(LEN=lennamespecy) :: namemom
CHARACTER(LEN=lennamespecy) :: IDmom
REAL(KIND=dp), ALLOCATABLE  :: momprofiles(:,:) ! Profils des moments

!*******************************************************************************************************
!                                             CONTAINS
!*******************************************************************************************************
CONTAINS

!********************************************************************************************************
! SUBROUTINES
! - OUTPUT_FITS : Main subroutine to write outputs in VO-TABLE + FITS file
!
! - CRE_CONTAIN
! - CRE_AXE
!********************************************************************************************************

!!!======================================================================================================
!!! SUBROUTINE OUTPUT_FITS
!!!======================================================================================================
!!! Write outputs in VO-TABLE (for metadata) and FITS
!!! The VO-Table provide informations on quantities in output (name, description, unit, ucd, ...)
!!! The Fits file contains data
!!! VO-Table points towards the fits file thanks to the href reference
!!! Each quantity is identified by a unique ID. This ID is the same in the VO-Table and the FITS file
!!! and it is the way relationships are done between VO-TABLE and FITS.
!!!------------------------------------------------------------------------------------------------------
!!! Structure of the VO-TABLE
!!!------------------------------------------------------------------------------------------------------
!!! RESOURCE 1 : Containers informations
!!!              This resource is interpreted by GUI PDR_Analyzer to build the tree/group
!!! RESOURCE 2 : Axes informations
!!!              Describe the possible axes (spatial : Av, NH, ...; Wavelength, Grain size)
!!!              It is used to build relationships between a quantity and an axe
!!!              Example : Temperature can be a function of Av
!!! RESOURCE 3 : Data description
!!!              This resource is divided in several Tables to organize data informations
!!!-----------------------------------------------------------------------------------------------------
!!! Structure of FITS file
!!!-----------------------------------------------------------------------------------------------------
!!! Data in the fits file are organized in several DataUnits (DU) and Fields.
!!! ID are commonly build using this structure, for example : DU_DUMMY.FD_DUMDUM
!!! means DataUnit DUMMY, Field DUMDUM
!!!-----------------------------------------------------------------------------------------------------
!!! NOTE
!!!-----------------------------------------------------------------------------------------------------
!!! 1) FITS is limited to 999 FIELDS and so some quantities are difficult to write in a generic way.
!!!    As a consequence, only the first 999 line intensities of H2 are in the FITS file.
!!! 2) CFITSIO produces "segmentation fault" if DU is too big.
!!!    Some table have been cut in the FITS output for this reason
!!!=====================================================================================================
SUBROUTINE OUTPUT_FITS

     IMPLICIT NONE
     INTEGER                     :: i, j, k ! compteurs
     INTEGER                     :: icol
     INTEGER                     :: nqn
     CHARACTER(LEN=lennamespecy) :: espece
     CHARACTER(LEN=lennamespecy) :: especeID
     CHARACTER(LEN=12)           :: s_i ! chaine de caractere de transformation int -> string
     CHARACTER(LEN=lenIDLEV)     :: IDLEV ! ID of a quantum level
     INTEGER                     :: numH2  ! numero de taille de grains
     INTEGER                     :: num    ! numero
     CHARACTER(LEN=12)           :: snum   ! chaine de caractere correspondant a num
     CHARACTER(LEN=lennamespecy) :: espece_pre
     INTEGER                     :: npixlam ! Number of points in wavelength

     !-------------------------------------------------------------------------------------------------
     ! INITIALIZATION
     !-------------------------------------------------------------------------------------------------

     !-- Name of output files -------------------------------------------------------------------------
     namefits = TRIM(ADJUSTL(modele))//"_"//TRIM(ADJUSTL(ii2c(ifaf)))//".fits"
     filefits = TRIM(out_dir)//TRIM(ADJUSTL(modele))//"_"//TRIM(ADJUSTL(ii2c(ifaf)))//".fits"
     filexml  = TRIM(out_dir)//TRIM(ADJUSTL(modele))//"_"//TRIM(ADJUSTL(ii2c(ifaf)))//".xml"
     PRINT *," "
     PRINT *,"===================================="
     PRINT *,"     WRITE DATA and METADATA"
     PRINT *,"FITS : ",TRIM(ADJUSTL(filefits))
     PRINT *,"XML  : ",TRIM(ADJUSTL(filexml))
     PRINT *,"===================================="
     PRINT *," "

     !--- Preparation of the STREAM for the VO-TABLE -------------------------------------------------
     href = "file//./"//namefits ! name of fits files in the VO-TABLE
     href = TRIM(ADJUSTL(href))
     blocDUxml%flag_stream = 1
     blocDUxml%extnum      = 0    ! It is not used in our case
     blocDUxml%encoding    = ""
     blocDUxml%href        = href

     !-- Initialization of descaxe -------------------------------------------------------------------
     descaxe(:)%name = ""
     descaxe(:)%ID   = ""

     !-- Get some useful data ------------------------------------------------------------------------
     ncool = nspl - 1 + 1 ! Number of coolants is the number of species for which excitation is computed
                          ! (nspl), excepted H2 that is a mixed process (-1) plus Fe
     npixlam = rf_wl%upper-rf_wl%lower+1 ! Number of pixels in wavelength table

     nmom = n_var - nspec             ! Number of moments
     ALLOCATE(species(0:nspec+5))     ! Name of species in chemical network
     ALLOCATE(moments(1:nmom))        ! List of moments used in the simulation
     ALLOCATE(massmol(0:nspec+5))     ! Molar mass of species in the chemical network
     ALLOCATE(abinitial(0:nspec+5))   ! Abondances initiales
     ALLOCATE(enthalp(0:nspec+5))     ! Formation enthalpy of species in chemistry
     ALLOCATE(temp_evap(0:nspec+5))   ! Evaporation threshold from grains
     ALLOCATE(temp_diff(0:nspec+5))   ! Diffusion threshold from grains
     CALL PREPAR_CHEMFITS

     !------------------------------------------------------------------------------------------------
     !-- WRITE BEGINNING OF VO-TABLE
     !------------------------------------------------------------------------------------------------
     OPEN(unitxml,FILE=filexml,STATUS="unknown",ACTION="write")
     WRITE(unitxml,"(A22)") '<?xml version="1.0" ?>'
     WRITE(unitxml,"(A9)") '<VOTABLE>'

     !------------------------------------------------------------------------------------------------
     !-- WRITE CONTAINERS INFORMATIONS
     !-- Containers are the groups in the GUI PDR_Analyzer
     !-- To add or remove groups, edit container.dat file
     !------------------------------------------------------------------------------------------------
     PRINT *,"Creation of containers in xml file"
     CALL CRE_CONTAIN(unitxml)

     !------------------------------------------------------------------------------------------------
     !-- WRITE AXES INFORMATIONS
     !------------------------------------------------------------------------------------------------
     PRINT *,"XML : Creation of axes informations"
     CALL CRE_AXE(unitxml)

     !------------------------------------------------------------------------------------------------
     !-- WRITE RESOURCE WITH METADATA
     !------------------------------------------------------------------------------------------------
     WRITE(unitxml,"(A46)") '<RESOURCE name="DataUnit"   ID="RES_DATAUNIT">'

     !------------------------------------------------------------------------------------------------
     !-- INITIALIZATION OF FITS FILE
     !-- Fits files always begin by an image, eventually a dummy one
     !-- We write this dummy image
     !-- Dummy image is the first DU
     !------------------------------------------------------------------------------------------------

     statusfits = 0
     ! Destruction of FITS file if it exists
     call deletefile(filefits)

     !--- Get a Unit number --------------------------------------------
     statusfits = 0
     call ftgiou(unitfits,statusfits)
     call printerrfits(statusfits)

     !--- Open FITS FILE AND INITIALIZE IT by an image -----------------
     PRINT *,"Initialization of FITS file"
     CALL INIIMAGE(filefits)
     ! FITS file is now open and ready to be filled.
     numhdu = 1

     !------------------------------------------------------------------------------------------------
     !==@ 2nd HDU : tableau des entiers definissant les tailles de tableau
     !==@           nlgn = nconst_tab    ncol = 3
     !==@           Format col : STRING  STRING  INTEGER
     !------------------------------------------------------------------------------------------------
     numhdu  = numhdu+1                            ! Increase the index of Data Unit to write in a new one
     !PRINT *,"Ecriture du tableau des entiers"
     nlgn = 1                                      ! Number of lines in the table
     ncol = nconst_tab                             ! Number of columns in the table 2 colonne pour sval et une pour ival
     ALLOCATE(descol(ncol))                        ! Allocate descol : structure to describe columns
     ALLOCATE(ival2D(nlgn,ncol))                   ! Allocate ival2D : array transmited to WRITE_HDU containing data of the table
     CALL CRE_DATA_SIZETAB                         ! Fill ival2D and descol (title and ID of columns, units, descriptions, ...)

     name        = "Arrays sizes"                  ! Name of the table (human readable)
     extname     = "DU_ARRAYSIZE"                  ! ID of the table (computer understandable) - Must be unique

     type_write  = "int2D"                         ! Type of the table - here a 2 dimensions integer table
     CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)  ! Write data unit in FITS file

     !--- Creation du XML --------------           ! Prepare writting of XML
     blocDUxml%nameDU    = name                    ! Human readable name of the table
     blocDUxml%IDDU      = extname                 ! ID of the table - Must be unique
     blocDUxml%CNTIDDU   = "CNT_INFODEV_ARRSIZ"    ! ID of the container in the PDRAnalyser tree where data should be presented
     blocDUxml%modeDU    = "DEV"                   ! Mode Developer - Data will only appear in DEV mode of the PDRAnalyser
     blocDUxml%flag_desc = 1                       ! Flag to tell if there is a description to the table
     blocDUxml%descripDU = "Arrays sizes"          ! Description that will appear in a bubble in the PDRAnalyser
     blocDUxml%nrows     = nlgn                    ! Number of lines
     blocDUxml%ncol      = ncol                    ! Number of columns
     naxe = 0                                      ! These data are not associated to any axes
     CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)  ! Write XML

     DEALLOCATE(descol)
     DEALLOCATE(ival2D)

     !-----------------------------------------------------------------------------------------------
     !-- DU 003 MODEL PARAMETERS : REAL
     !--        nlgn = 1     ncol = nparam_mod_r
     !--        Format DOUBLE
     !----------------------------------------------------------------------------
    numhdu  = numhdu+1
    !PRINT *, "Ecriture des parametres du modele"
    nlgn = 1
    ncol = nparam_mod_r
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,nparam_mod_r))

    CALL CRE_DATA_PARAMOD_R

    name        = "Model parameters"    ! Human readable name for DataUnit
    extname     = "DU_MODPARAM_R"       ! ID of DataUnit

    !- Write FITS -------------------
    type_write  = "double2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !- WRITE VO-TABLE --------------
    blocDUxml%nameDU    = name               ! Human readable name for Quantity Group
    blocDUxml%IDDU      = extname            ! ID
    blocDUxml%CNTIDDU   = "CNT_MODPARAM"     ! ID of related container in the GUI PDR_Analyzer
    blocDUxml%modeDU    = "DEV"              ! Mode : Developper (?)
    blocDUxml%flag_desc = 1                  ! Flag description set to "yes"
    blocDUxml%descripDU = "Model parameters" ! Description
    blocDUxml%nrows     = nlgn               ! Number of lines
    blocDUxml%ncol      = ncol               ! Number of columns
    naxe                = 0                  ! Number of related axes
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------
    !-- DU 004 MODEL PARAMETERS : INTEGERS
    !--        nlgn = 1     ncol = nparam_mod_i
    !--        Format INTEGER
    !----------------------------------------------------------------------------
    numhdu  = numhdu+1
    !PRINT *, "Ecriture des parametres du modele"
    nlgn = 1
    ncol = nparam_mod_i
    ALLOCATE(descol(ncol))
    ALLOCATE(ival2D(nlgn,ncol))

    CALL CRE_DATA_PARAMOD_I

    name        = "Model parameters"
    extname     = "DU_MODPARAM_I"

    type_write  = "int2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_MODPARAM"
    blocDUxml%modeDU   = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Model parameters"
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(ival2D)

    !----------------------------------------------------------------------------
    !-- DU 005 MODEL PARAMETERS : STRING
    !--        nlgn = 1     ncol = nparam_mod_s
    !--        Format STRING
    !----------------------------------------------------------------------------
    numhdu  = numhdu+1
    !PRINT *, "Ecriture des parametres du modele"
    nlgn = 1
    ncol = nparam_mod_s
    ALLOCATE(descol(ncol))
    ALLOCATE(sval2D(nlgn,nparam_mod_s))

    CALL CRE_DATA_PARAMOD_S

    name        = "Model parameters"
    extname     = "DU_MODPARAM_S"

    type_write  = "string2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_MODPARAM"
    blocDUxml%modeDU   = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Model parameters"
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(sval2D)

    !----------------------------------------------------------------------------
    !-- DU 006 LIST OF CHEMICAL SPECIES
    !--        nlgn = nspec+5+1  ncol = 7
    !--        FORMAT : STRING  STRING DOUBLE DOUBLE DOUBLE DOUBLE DOUBLE
    !--        Creation des donnees pour .... ne comportant pas les moments
    !--        Specy " " is also stored
    !----------------------------------------------------------------------------
    numhdu = numhdu+1
    name    = "Species and molecular mass"
    extname = "DU_SPECIES"
    !PRINT *,"Ecriture de la liste des especes"
    ! ATTENTION : C'est le tableau species(0:nspec+5) et non speci(0:n_var+5)
    !             que l'on manipule
    nlgn = nspec+1+5   ! tableau speci va de 0 a np
                       ! (mais il n'est rempli que de 0 a n_var+5)
                       ! nspec     = ELECTRON
                       ! nspec + 1 = PHOTON, n_var+2 = CRP, n_var+3 = SECPHOTO
                       ! n_var + 4 = GRAIN,  n_var+5 = 2H
    ncol = 7           ! 1) nom de l'espece, 2) ID de l'espece, 3) masse molaire,
                       ! 4) enthalpie de formation, 5) abondance initiale,
                       ! 6) temperature de diffusion, 7) temperature d'evaporation
    ALLOCATE(descol(ncol))
    ALLOCATE(sval2D(nlgn,2))
    ALLOCATE(rval2D(nlgn,5))
    CALL CRE_DATA_SPECIES(nlgn)

    type_write = "str2dbln"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol,type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "CNT_SPECIES"
    blocDUxml%modeDU    = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "List of chemical species in the simulation"
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(sval2D)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------
    !-- DU 006b LIST OF MOMENTS
    !--
    !----------------------------------------------------------------------------
    ! ATTENTION : C'est un bout du tableau speci stocke dans le tableau moments
    nlgn = nmom
    ncol = 2           ! 1) nom du moment, 2) ID du moment
    IF (nlgn .GT. 0) THEN
    numhdu = numhdu+1
    name    = "Moments used in the model"
    extname = "DU_NAMEMOMENTS"
    ALLOCATE(descol(ncol))
    ALLOCATE(sval2D(nlgn,ncol))
    CALL CRE_DATA_NAMEMOMENTS(nlgn)

    type_write = "string2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol,type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "CNT_CHEMISTRY"
    blocDUxml%modeDU    = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "List of chemical species in the simulation"
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(sval2D)
    ENDIF

    !----------------------------------------------------------------------------
    !-- DU 006c LIST OF CHEMICAL SPECIES UP TO N_VAR
    !--        nlgn = n_var+5+1  ncol = 7
    !--        FORMAT : STRING STRING DOUBLE DOUBLE DOUBLE DOUBLE DOUBLE
    !--        Creation des donnees pour .... ne comportant pas les moments
    !--        Specy " " is also stored
    !----------------------------------------------------------------------------
    numhdu = numhdu+1
    name    = "Species and molecular mass"
    extname = "DU_SPECI_NUM"
    !PRINT *,"Ecriture de la liste des especes"
    ! ATTENTION : C'est le tableau species(0:nspec+5) et non speci(0:n_var+5)
    !             que l'on manipule
    nlgn = n_var+1+5   ! tableau speci va de 0 a np
                       ! (mais il n'est rempli que de 0 a n_var+5)
                       ! nspec     = ELECTRON
                       ! nspec + 1 = PHOTON, n_var+2 = CRP, n_var+3 = SECPHOTO
                       ! n_var + 4 = GRAIN,  n_var+5 = 2H
    ncol = 6           ! 1) nom de l'espece, 2) ID de l'espece, 3) masse molaire,
                       ! 4) enthalpie de formation, 5) temperature de diffusion
                       ! 6) temperature d'evaporation
    ALLOCATE(descol(ncol))
    ALLOCATE(sval2D(nlgn,2))
    ALLOCATE(rval2D(nlgn,5))
    CALL CRE_DATA_SPECI_NUM(nlgn)

    type_write = "str2dbln"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol,type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "CNT_SPECI_NUM"
    blocDUxml%modeDU    = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "List of variables to solve chemistry"
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(sval2D)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------
    !-- DU 007 ELEMENTAL COMPOSITION OF SPECIES
    !--        nlgn = nspec+5+1  ncol = 2
    !--        FORMAT : STRING  DOUBLE
    !----------------------------------------------------------------------------
    numhdu = numhdu+1
    name    = "Elementaty composition"
    extname = "DU_ELEMENTCOMP"
    !PRINT *,"Ecriture de la composition des especes"
    nlgn = nspec+5+1 ! tableau speci va de 0 a np
                     ! et on utilise les cases de 0 a nspec
                     ! Donc on s'arrete avant les electrons
    ncol = natom     ! H, C, N, O, He, D, C, N, O, Li, F, Na, Mg, Al, Si, P, S, Cl, K, Fe, charge
    ALLOCATE(descol(ncol))
    ALLOCATE(ival2D(nlgn,ncol))
    CALL CRE_DATA_ELEMENTCOMP

    type_write = "int2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_SPECIES"
    blocDUxml%modeDU   = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Elementary composition of species"
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(ival2D)

    !----------------------------------------------------------------------------
    !-- DU 008 INDEX OF SPECIAL SPECIES
    !--        nlgn = 1  ncol = nspeci_special
    !--        FORMAT : INTEGER
    !----------------------------------------------------------------------------
    !PRINT *,"Write numbers of special species"
    numhdu = numhdu + 1
    name    = "Numbers of special species"
    extname = "DU_NSPESPE"
    nlgn    = 1
    ncol    = nspeci_special
    ALLOCATE(descol(ncol))
    ALLOCATE(ival2D(nlgn,ncol))
    CALL CRE_DATA_SPESPE

    type_write = "int2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_INFODEV_NSPESPE"
    blocDUxml%modeDU   = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Numbers associated to special species in the code"
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(ival2D)

    !----------------------------------------------------------------------------
    !-- DU 008b NUMBER OF SPECIES BY CATEGORY
    !--        nlgn = 1  ncol = nspeci_kind
    !--        FORMAT : INTEGER
    !----------------------------------------------------------------------------
    ! PRINT *,"Write number of species by category"
    numhdu  = numhdu + 1
    name    = "Numbers of species by categories"
    extname = "DU_NUMPROPSPE"
    nlgn    = 1
    ncol    = nprop_spe
    ALLOCATE(descol(ncol))
    ALLOCATE(ival2D(nlgn,ncol))
    CALL CRE_DATA_PROPSPE

    type_write = "int2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_CHEMISTRY_PROPLISTSPE"
    blocDUxml%modeDU   = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Numbers of species by categories : neutral, cations, anions, &
                           &on grains with multi-layers, on grains with mono-layer"
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(ival2D)

    !----------------------------------------------------------------------------
    !-- DU 009 CLOUD GEOMETRY
    !--        nlgn = npo+1   ncol = 4
    !--        FORMAT : DOUBLE
    !--        tau, av, distance, NH
    !----------------------------------------------------------------------------
    numhdu = numhdu + 1
    nlgn   = npo + 1
    ncol   = 4
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    descol(:)%form    = "1D"
    descol(:)%datatyp = "double"

    descol(1)%titre   = "Optical depth"
    descol(1)%IDFD    = "FD_OPTDEPTH"
    descol(1)%unite   = "mag"
    descol(1)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/OpticalDepthInTheVisible"
    descol(1)%utype   = ""
    descol(1)%flag_desc = 0

    descol(2)%titre   = "AV"
    descol(2)%IDFD    = "FD_AV"
    descol(2)%unite   = "mag"
    descol(2)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/VisualExtinction"
    descol(2)%utype   = ""
    descol(2)%flag_desc = 1
    descol(2)%descrip = "<![CDATA[Visual extinction (AV)<br/>]]>"

    descol(3)%titre   = "Distance"
    descol(3)%IDFD    = "FD_DISTANCE"
    descol(3)%unite   = "cm"
    descol(3)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/Distance"
    descol(3)%utype   = ""
    descol(3)%flag_desc = 1
    descol(3)%descrip = "<![CDATA[Distance <br/>]]>"

    descol(4)%titre   = "NH"
    descol(4)%IDFD    = "FD_NH"
    descol(4)%unite   = "cm-2"
    descol(4)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/ColumnDensity"
    descol(4)%utype   = ""
    descol(4)%flag_desc = 1
    descol(4)%descrip = "<![CDATA[Proton column density<br/> &
                      & NH = N(H) + N(H+) + 2N(H2)]]>"

    DO i = 0, nlgn-1
        rval2D(i+1,1) = tau(i)
        rval2D(i+1,2) = visualext(i)
        rval2D(i+1,3) = d_cm(i)
        rval2D(i+1,4) = protoncd(i)
    ENDDO
    name              = "Geometry"
    extname           = "DU_OPTDEPTH"

    type_write = "double2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_GEOMETRY"
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Various distances"
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------
    !-- DU 010 CLOUD STRUCTURE : T, nH, ne, ...
    !--        nlgn = npo+1    ncol = ncol_struct
    !--        FORMAT : DOUBLE
    !--        tgaz, densh, ionidegree, gaspressure, totdensity
    !----------------------------------------------------------------------------
    !PRINT *,"Structure"
    numhdu = numhdu+1
    nlgn   = npo + 1        ! de 0 a npo
    ncol   = ncol_struct
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))

    CALL CRE_DATA_STRUC
    name        = "Structure"
    extname     = "DU_STRUCTURE"

    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_STRUCTURE"
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------
    !-- 011 OTHERS : Molecular Fraction, T01, ...
    !--     nlgn = npo+1    ncol = 4
    !--     FORMAT : DOUBLE
    !----------------------------------------------------------------------------
    !PRINT *,"Others"
    numhdu = numhdu+1
    nlgn   = npo + 1        ! de 0 a npo
    ncol   = 4
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    rval2D(:,:) = 0.0_dp

    name    = "Others"
    extname = "DU_OTHER"
    descol(:)%form    = "1D"
    descol(:)%datatyp = "double"

    descol(1)%titre = "f(H2)_ab"
    descol(1)%IDFD  = "FD_FH2_AB"
    descol(1)%unite   = "no unit"
    descol(1)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/MolecularFraction"
    descol(1)%utype   = ""
    descol(1)%flag_desc = 1
    descol(1)%descrip = "<![CDATA[H2 molecular fraction calculated from abundances<br/>&
                    &f(H2) = 2n(H2)/[n(H) + 2n(H2)]]]>"

    descol(2)%titre = "f(H2)_cd"
    descol(2)%IDFD  = "FD_FH2_CD"
    descol(2)%unite   = "no unit"
    descol(2)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/MolecularFraction"
    descol(2)%utype   = ""
    descol(2)%flag_desc = 1
    descol(2)%descrip = "<![CDATA[H2 molecular fraction calculated from column densities<br/>&
                    &f(H2) = 2N(H2)/[N(H) + 2N(H2)]]]>"

    descol(3)%titre = "T01(H2)_ab"
    descol(3)%IDFD  = "FD_T01_AB"
    descol(3)%unite   = "Kelvin"
    descol(3)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/Temperature"
    descol(3)%utype   = ""
    descol(3)%flag_desc = 1
    descol(3)%descrip = "<![CDATA[H2 excitation temperature T01 calculated from abundances<br/>&
                    &T01 = -170.5 / ln(n(H2,J=1)/(9n(H2,J=0))]]>"

    descol(4)%titre = "T01(H2)_cd"
    descol(4)%IDFD  = "FD_T01_CD"
    descol(4)%unite   = "Kelvin"
    descol(4)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/Temperature"
    descol(4)%utype   = ""
    descol(4)%flag_desc = 1
    descol(4)%descrip = "<![CDATA[H2 excitation temperature T01 calculated from column densities<br/>&
                    &T01 = -170.5 / ln(N(H2,J=1)/(9N(H2,J=0))]]>"

    DO i = 0, nlgn-1
        rval2D(i+1,1) = molfrac_ab(i)
        rval2D(i+1,2) = molfrac_cd(i)
        rval2D(i+1,3) = T01_ab(i)
        rval2D(i+1,4) = T01_cd(i)
    ENDDO

    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_OTHER" ! On met les quantities derivees dans OTHER
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Derived quantities"
    naxe = 1
    descaxe(1)%name    = '"Optical depth"'
    descaxe(1)%ID      = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !---------------------------------------------------------------------------
    !-- DU 012 HEATING RATES
    !--    nlgn = npo+1    ncol = ncol_heat
    !--    FORMAT : DOUBLE
    !---------------------------------------------------------------------------
    !PRINT *,"Heating"
    numhdu = numhdu+1
    nlgn   = npo + 1                   ! de 0 a npo
    ncol   = ncol_heat
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    CALL CRE_DATA_HEAT
    name    = "Heating"
    extname = "DU_HEATING"

    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_HEATCOOL_HEAT"
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Heating rates in erg cm-3 s-1"
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !---------------------------------------------------------------------------
    !-- DU 013 COOLING RATES
    !--        nlgn = npo+1   ncol = nspl
    !--        FORMAT : DOUBLE
    !---------------------------------------------------------------------------
    ! PRINT *,"Cooling"
    numhdu = numhdu+1
    nlgn   = npo + 1
    ncol   = 1 + ncool ! total cooling rates plus individual ones
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    name        = "Cooling"
    extname     = "DU_COOLING"

    CALL CRE_DATA_COOL

    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "CNT_HEATCOOL_COOL"
    blocDUxml%modeDU    = "USER"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Cooling rates in erg cm-3 s-1"
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !---------------------------------------------------------------------------
    !-- DU 014 MIXT PROCESSES
    !--        nlgn = npo+1    ncol = ncol_mixt
    !--        FORMAT : DOUBLE
    !---------------------------------------------------------------------------
    !PRINT *,"Mixte"
    numhdu = numhdu+1
    nlgn   = npo + 1                   ! de 0 a npo
    ncol   = ncol_mixt
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    CALL CRE_DATA_MIXT
    name        = "Mixte"
    extname     = "DU_MIXT"

    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_HEATCOOL_MIXT"
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "<![CDATA[Heating or Cooling rates in erg cm-3 s-1. <br/> &
                          &<i>These processes can heat or cool the gas.</i>]]>"
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !---------------------------------------------------------------------------------
    !-- DU 015 ABUNDANCES
    !--        nlgn = npo+1    ncol = nspec+1
    !--        FORMAT : DOUBLE
    !---------------------------------------------------------------------------
    !PRINT *,"Ecriture des abondances"
    numhdu = numhdu+1
    nlgn = npo+1       ! de 0 a npo
    ncol = nspec       ! nbre d'especes chimiques dans la chimie (sans 0 -NOTHING et jusqu'a nspec ELECTR)
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    rval2D = 0.0_dp ! initialisation
    CALL CRE_DATA_ABOND(nlgn,ncol)
    name        = "Abundances"
    extname     = "DU_ABUNDANCES"

    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_ABUNDANCES"
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Abundances [cm-3]"
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !---------------------------------------------------------------------------
    !-- DU 016 COLUMN DENSITIES
    !--        nlgn = npo+1    ncol = nspec+1
    !--        FORMAT DOUBLE
    !---------------------------------------------------------------------------
    !PRINT *,"Ecriture des colonnes densites"
    numhdu = numhdu+1
    nlgn = npo+1   ! de 0 a npo
    ncol = nspec   ! nbre d'especes chimiques dans la chimie (From H to nspec -ELECTR-)
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    rval2D = 0.0_dp ! initialisation
    CALL CRE_DATA_COLDENS(nlgn,ncol)
    name        = "Column densities"
    extname     = "DU_COLDENS"

    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "CNT_COLDENS"
    blocDUxml%modeDU    = "USER"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Column density [cm-2]"
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !---------------------------------------------------------------------------------
    !-- DU 015b PROFILS DES MOMENTS
    !--        nlgn = npo+1    ncol = nmom
    !--        FORMAT : DOUBLE
    !---------------------------------------------------------------------------
    !PRINT *,"Ecriture des profils des moments"
    nlgn   = npo+1     ! de 0 a npo
    ncol   = nmom      ! nbre d'especes chimiques dans la chimie (0 -NOTHING- to nspec -ELECTR-)
    IF (ncol .GT. 0) THEN
    numhdu = numhdu+1
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    rval2D = 0.0_dp ! initialisation
    CALL CRE_DATA_MOMPROFILES(nlgn)
    name        = "Moments profiles"
    extname     = "DU_MOMPROFILES"

    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "CNT_MOMENTS"
    blocDUxml%modeDU    = "USER"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Moments [number on a grain]"
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)
    ENDIF

    !---------------------------------------------------------------------------
    !-- DU 017 INFORMATION ON DETAILED SPECIES
    !--        nlgn = 1    ncol = 7 + nqn
    !--        FORMAT SPECIAL
    !--        Ecriture dans un seul DU de:
    !--        Nom_ESPECE   MASS   INDEX NLV   NLV_USE   NQN   NTR   NOMS_DES_NOMBRES_QUANTIQUES
    !--        STRING       DLB    INT   INT   INT       INT   INT   NQN * STRING
    !--        ATTENTION : Toute modif dans cette routine doit etre suivie d'une modif dans modfits.
    !---------------------------------------------------------------------------
    DO k = 1, nspl
       numhdu = numhdu + 1

       espece = TRIM(spec_lin(k)%nam)
       CALL SPEMAJ(espece,especeID)
       nqn  = spec_lin(k)%nqn

       name = TRIM(ADJUSTL(espece))//" properties"
       extname = "DU_LEVELINFO_"//TRIM(ADJUSTL(especeID))

       nlgn = 1        ! We put eveything on one line
       ncol = 7 + nqn  ! name, mass, ind, nlv, nlv_use, nqn, ntr, names of quantum numbers

       ALLOCATE(descol(ncol))        ! NCOL = 1 string + 4 entier + NQN strings
       ALLOCATE(sval2D(nlgn,1+nqn))  ! nom de l'espece + nom des nbres quantiques
       ALLOCATE(rval2D(nlgn,1))      ! Mass de l'espece
       ALLOCATE(ival2D(nlgn,5))      ! 5 pour IND, NLV, NLV_USE, NQN, NTR

       !--- Nom de l'espece ---
       descol(1)%titre     = "Name"
       descol(1)%form      = "8A"
       descol(1)%unite     = "no unit"
       descol(1)%IDFD      = "FD_NAME_"//TRIM(ADJUSTL(especeID))
       descol(1)%datatyp   = "string"
       descol(1)%ucd       = ""
       descol(1)%utype     = ""
       descol(1)%flag_desc = 1
       descol(1)%descrip   = "<![CDATA[Name of specy]]>"
       sval2D(nlgn,1)      = espece

       !--- Masse de l'espece ---
       descol(2)%titre     = "Mass"
       descol(2)%form      = "1D"
       descol(2)%unite     = "atomic mass"
       descol(2)%IDFD      = "FD_MASS_"//TRIM(ADJUSTL(especeID))
       descol(2)%datatyp   = "double"
       descol(2)%ucd       = "http://purl.org/astronomy/vocab/PhysicalQuantities/Mass"
       descol(2)%utype     = ""
       descol(2)%flag_desc = 1
       descol(2)%descrip   = "<![CDATA[Mass in atomic mass]]>"
       rval2D(nlgn,1)      = spec_lin(k)%mom

       !--- FACTORISATION : 2:index 3:nlv 4:nlv_use 5:nqn 6:ntr
       descol(3:7)%form    = "1J"
       descol(3:7)%datatyp = "integer"
       descol(3:7)%unite   = "no unit"
       descol(3:7)%ucd     = ""
       descol(3:7)%utype   = ""

       descol(3)%titre     = "Index of specy"
       descol(3)%IDFD      = "FD_IND_"//TRIM(ADJUSTL(especeID))
       descol(3)%flag_desc = 1
       descol(3)%descrip   = "<![CDATA[Index of specy]]>"
       ival2D(nlgn,1)      = spec_lin(k)%ind

       descol(4)%titre     = "Number of levels"
       descol(4)%IDFD      = "FD_NLEV_"//TRIM(ADJUSTL(especeID))
       descol(4)%flag_desc = 1
       descol(4)%descrip   = "<![CDATA[Number of levels]]>"
       ival2D(nlgn,2)      = spec_lin(k)%nlv

       descol(5)%titre     = "Number of levels used"
       descol(5)%IDFD      = "FD_NUSE_"//TRIM(ADJUSTL(especeID))
       descol(5)%flag_desc = 1
       descol(5)%descrip   = "<![CDATA[Number of levels used &
                             &in the model]]>"
       ival2D(nlgn,3)      = spec_lin(k)%use

       descol(6)%titre     = "Number of quantum numbers"
       descol(6)%IDFD      = "FD_NQN_"//TRIM(ADJUSTL(especeID))
       descol(6)%flag_desc = 1
       descol(6)%descrip   = "<![CDATA[Number of quantum &
                             &numbers to define a level]]>"
       ival2D(nlgn,4)      = spec_lin(k)%nqn ! ATTENTION : SI NQN n'est plus dans la case 4 de ival2D
                                             ! Il faut aussi modifier la routine WRITE_LEVELINFO
                                             ! La boucle sur l'ecriture des noms des nombres quantiques
                                             ! utilise la valeur de nqn stockee dans ival2D

       descol(7)%titre     = "Number of transitions"
       descol(7)%IDFD      = "FD_NTR_"//TRIM(ADJUSTL(especeID))
       descol(7)%flag_desc = 1
       descol(7)%descrip   = "<![CDATA[Number of transitions]]>"
       ival2D(nlgn,5)      = MIN(spec_lin(k)%ntr,999) !ATTENTION!!!!

       ! Colonnes de strings pour les noms des nombres quantiques - Commence a la 7eme colonne
       j = 1
       DO i = 8, 8+(nqn-1)
          CALL INT_STR(j,s_i)
          descol(i)%titre     = "Quant. Numb. name"
          descol(i)%form      = "8A"
          descol(i)%datatyp   = "string"
          descol(i)%unite     = "no unit"
          descol(i)%ucd       = ""
          descol(i)%utype     = ""
          descol(i)%IDFD      = "FD_QNAM_"//TRIM(ADJUSTL(especeID))//"_"//TRIM(ADJUSTL(s_i))
          descol(i)%flag_desc = 1
          descol(i)%descrip   = "<![CDATA[Name of quantum number]]>"
          sval2D(nlgn,j+1)    = spec_lin(k)%qnam(j) ! sval commence a 2 puisque sval2D(...,1) est le nom de l'espece
                                                    ! spec_lin(k)%qnam commence a 1
          j = j + 1
       ENDDO

       type_write = "level_info"
       CALL WRITE_HDU(numhdu,extname,nlgn,ncol,type_write)

       blocDUxml%nameDU    = name
       blocDUxml%IDDU      = extname
       blocDUxml%nrows     = nlgn
       blocDUxml%ncol      = ncol
       blocDUxml%CNTIDDU   = "" ! ATTENTION CAS PARTICULIER PAS DANS l'ARBRE
       blocDUxml%modeDU    = "DEV"
       blocDUxml%flag_desc = 1
       blocDUxml%descripDU = "Info on levels and molecules"
       naxe = 0
       !descaxe(1)%name = '""'
       !descaxe(1)%ID   = '""'
       CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

       DEALLOCATE(descol)
       DEALLOCATE(sval2D)
       DEALLOCATE(rval2D)
       DEALLOCATE(ival2D)
    ENDDO

    !---------------------------------------------------------------------------
    !-- DU 018 INFORMATION ON LEVELS
    !--        nlgn = 1    ncol = 4 + nqn
    !--        FORMAT SPECIAL
    !--        Le fichier fits contient :
    !--        numero_du_niveau  Energy  g   Valeur_q1 Valeur_q2 ... ID_LEVEL
    !---------------------------------------------------------------------------
    DO k = 1, nspl

        numhdu = numhdu + 1

        espece = TRIM(spec_lin(k)%nam)
        CALL SPEMAJ(espece,especeID)
        nqn  = spec_lin(k)%nqn

        name = TRIM(ADJUSTL(espece))//" levels properties"
        extname = "DU_LEVELPROP_"//TRIM(ADJUSTL(especeID))

        nlgn = spec_lin(k)%use ! number of levels used in the simulation
        ncol = nqn + 4         ! nqn nbres quantiques + numero des niveaux + Energy  + degenerescy + ID du niveau

        ALLOCATE(descol(ncol))
        ALLOCATE(ival2D(nlgn,1))        ! Numero des niveaux
        ALLOCATE(rval2D(nlgn,2))        ! Energies des niveaux et degenerescence
        ALLOCATE(sval2D(nlgn,nqn+1))    ! Nombres quantiques en string + ID des niveaux

        ! Numero de ligne
        descol(1)%titre     = "ID"
        descol(1)%form      = "1J"
        descol(1)%unite     = "no unit"
        descol(1)%IDFD      = "FD_LEVEL_INDEX_"//TRIM(ADJUSTL(especeID))
        descol(1)%datatyp   = "integer"
        descol(1)%ucd       = ""
        descol(1)%utype     = ""
        descol(1)%flag_desc = 1
        descol(1)%descrip   = "<![CDATA[Index of levels &
                              & used in the code]]>"

        ! Energy
        descol(2)%titre     = "Energy"
        descol(2)%form      = "1D"
        descol(2)%unite     = "K"
        descol(2)%IDFD      = "FD_LEVEL_ENERGY_"//TRIM(ADJUSTL(especeID))
        descol(2)%datatyp   = "double"
        descol(2)%ucd       = "http://purl.org/astronomy/vocab/PhysicalQuantities/Energy"
        descol(2)%utype     = ""
        descol(2)%flag_desc = 1
        descol(2)%descrip   = "<![CDATA[Energy of levels &
                               &]]>"

        ! Degenerecy
        descol(3)%titre     = "g"
        descol(3)%form      = "1D"
        descol(3)%unite     = "no unit"
        descol(3)%IDFD      = "FD_LEVEL_DEGEN_"//TRIM(ADJUSTL(especeID))
        descol(3)%datatyp   = "double"
        descol(3)%ucd       = "phys.atmol.sWeight"
        descol(3)%utype     = ""
        descol(3)%flag_desc = 1
        descol(3)%descrip   = "<![CDATA[Statistical weight]]>"

        ! Nombres quantiques
        icol = 3
        IF (nqn .NE. 0) THEN
           DO i = 1, nqn
              descol(icol+i)%titre       = TRIM(ADJUSTL(spec_lin(k)%qnam(i)))
              descol(icol+i)%form        = "60A"
              descol(icol+i)%unite       = "no unit"
              descol(icol+i)%IDFD        = "FD_LEVEL_"//TRIM(ADJUSTL(especeID))//"_"&
                                           //TRIM(ADJUSTL(spec_lin(k)%qnam(i)))
              descol(icol+i)%datatyp     = "string"
              descol(icol+i)%ucd         = ""
              descol(icol+i)%utype       = ""
              descol(icol+i)%flag_desc   = 1
              descol(icol+i)%descrip = "<![CDATA[Quantum number]]>"
           ENDDO
        ENDIF

        ! ID des niveaux
        icol = 3 + nqn + 1
        descol(icol)%titre       = "Level ID"
        descol(icol)%form        = "50A"
        descol(icol)%unite       = "no unit"
        descol(icol)%IDFD        = "FD_LEVEL_ID_"//TRIM(ADJUSTL(especeID))
        descol(icol)%datatyp     = "string"
        descol(icol)%ucd         = ""
        descol(icol)%utype       = ""
        descol(icol)%flag_desc   = 1
        descol(icol)%descrip     = "<![CDATA[Level ID]]>"

        ! Fill with data
        DO i = 1, spec_lin(k)%use
           ival2D(i,1) = i                     ! Numero de ligne
           rval2D(i,1) = spec_lin(k)%elk(i)    ! Energy Kelvin
           rval2D(i,2) = spec_lin(k)%gst(i)    ! Degenerescy
           IF (nqn .NE. 0) THEN
              DO j = 1, nqn
                 sval2D(i,j) = spec_lin(k)%quant(i,j)
              ENDDO
           ENDIF
           CALL IDLEVEL(spec_lin(k),i,IDLEV)
           sval2D(i,nqn+1) = TRIM(ADJUSTL(IDLEV))
        ENDDO

        type_write = "level_data"
        ! NOTE : In the FITS routine, spec_lin%nqn will be deduced from ncol.
        !        It will assume nqn = ncol - 4
        CALL WRITE_HDU(numhdu,extname,nlgn,ncol,type_write)

        blocDUxml%nameDU    = name
        blocDUxml%IDDU      = extname
        blocDUxml%nrows     = nlgn
        blocDUxml%ncol      = ncol
        blocDUxml%CNTIDDU   = "" ! ATTENTION CAS PARTICULIER PAS DANS l'ARBRE
        blocDUxml%modeDU    = "DEV"
        blocDUxml%flag_desc = 1
        blocDUxml%descripDU = "Levels"
        naxe = 0
        !descaxe(1)%name = '""'
        !descaxe(1)%ID   = '""'
        CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

       DEALLOCATE(descol)
       DEALLOCATE(ival2D)
       DEALLOCATE(rval2D)
       DEALLOCATE(sval2D)

    ENDDO

    !---------------------------------------------------------------------------
    !-- DU 019 LEVELS EXCITATIONS
    !--        nlgn = npo+1    ncol = spec_lin(k)%use
    !--        FORMAT DOUBLE
    !---------------------------------------------------------------------------
    DO k = 1, nspl
       numhdu = numhdu + 1

       espece = TRIM(spec_lin(k)%nam)
       CALL SPEMAJ(espece,especeID)

       name = TRIM(ADJUSTL(espece))//" levels excitation"
       extname = "DU_EX_"//TRIM(ADJUSTL(especeID))

       nlgn = npo+1
       ncol = spec_lin(k)%use         ! nqn nbres quantiques + numero des niveaux + Energy  + degenerescy + ID du niveau

       ALLOCATE(descol(ncol))
       ALLOCATE(rval2D(nlgn,ncol))
       rval2D(:,:) = 0.0_dp
       DO i = 1, ncol
          CALL IDLEVEL(spec_lin(k),i,IDLEV) ! Creation d'un ID a partir du numero du niveau i
          descol(i)%titre = TRIM(ADJUSTL(especeID))//"_LEV"//TRIM(ADJUSTL(IDLEV))
          descol(i)%IDFD  = "FD_EX_"//TRIM(ADJUSTL(especeID))//"_LEV"//TRIM(ADJUSTL(IDLEV))
       ENDDO

       descol(:)%form  = "1D"
       descol(:)%unite = "no unit"
       descol(:)%datatyp = "double"
       descol(:)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/LevelPopulation"
       descol(:)%utype   = ""
       descol(:)%flag_desc = 0
       DO i = 0, nlgn-1
          DO j = 1, ncol
             rval2D(i+1,j) = spec_lin(k)%xre(j,i)
          ENDDO
       ENDDO

       type_write = "double2D"
       CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

       !--- Creation du XML --------------
       blocDUxml%nameDU   = name
       blocDUxml%IDDU     = extname
       blocDUxml%nrows     = nlgn
       blocDUxml%ncol      = ncol
       blocDUxml%CNTIDDU  = "" ! ATTENTION CAS PARTICULIER
       blocDUxml%modeDU   = "USER"
       blocDUxml%flag_desc = 1
       blocDUxml%descripDU = "Normalized populations of "//TRIM(ADJUSTL(espece))//" levels"
       naxe = 1
       descaxe(1)%name = '"Optical depth"'
       descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
       CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

       DEALLOCATE(descol)
       DEALLOCATE(rval2D)

    ENDDO

    !---------------------------------------------------------------------------
    !-- DU 020 LEVELS COLUMN DENSITIES
    !--        nlgn = npo+1    ncol = spec_lin(k)%use
    !--        FORMAT DOUBLE
    !---------------------------------------------------------------------------
    DO k = 1, nspl
       numhdu = numhdu + 1

       espece = TRIM(spec_lin(k)%nam)
       CALL SPEMAJ(espece,especeID)

       name = TRIM(ADJUSTL(espece))//" levels excitation"
       extname = "DU_CDEX_"//TRIM(ADJUSTL(especeID))

       nlgn = npo+1
       ncol = spec_lin(k)%use         ! nqn nbres quantiques + numero des niveaux + Energy  + degenerescy + ID du niveau

       ALLOCATE(descol(ncol))
       ALLOCATE(rval2D(nlgn,ncol))
       rval2D(:,:) = 0.0_dp
       DO i = 1, ncol
          CALL IDLEVEL(spec_lin(k),i,IDLEV) ! Creation d'un ID a partir du numero du niveau i
          descol(i)%titre = TRIM(ADJUSTL(especeID))//"_LEV"//TRIM(ADJUSTL(IDLEV))
          descol(i)%IDFD  = "FD_CDEX_"//TRIM(ADJUSTL(especeID))//"_LEV"//TRIM(ADJUSTL(IDLEV))
       ENDDO

       descol(:)%form      = "1D"
       descol(:)%unite     = "cm-2"
       descol(:)%datatyp   = "double"
       descol(:)%ucd       = ""
       descol(:)%utype     = ""
       descol(:)%flag_desc = 0
       DO i = 0, nlgn-1
          DO j = 1, ncol
             rval2D(i+1,j) = spec_lin(k)%CoD(j,i)
          ENDDO
       ENDDO

       type_write = "double2D"
       CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

       !--- Creation du XML --------------
       blocDUxml%nameDU    = name
       blocDUxml%IDDU      = extname
       blocDUxml%nrows     = nlgn
       blocDUxml%ncol      = ncol
       blocDUxml%CNTIDDU   = "" ! ATTENTION CAS PARTICULIER
       blocDUxml%modeDU    = "USER"
       blocDUxml%flag_desc = 1
       blocDUxml%descripDU = "Column densities of "//TRIM(ADJUSTL(espece))//" levels"
       naxe = 1
       descaxe(1)%name = '"Optical depth"'
       descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
       CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

       DEALLOCATE(descol)
       DEALLOCATE(rval2D)

    ENDDO

    !---------------------------------------------------------------------------
    !-- DU 021 TRANSITIONS PROPERTIES
    !--        Write several DU
    !--        DU 01 : LINE PROPERTIES
    !--        DU 02 : LOCAL EMISSIVITIES - spelin%emi
    !--        DU 03 : ASSOCIATED QUANTITIES - spelin%siD, spelin%exl and spelin%exr
    !--        DU 04 : OPTICAL DEPTH LEFT
    !--        DU 05 : OPTICAL DEPTH RIGHT
    !--        DU 06 : ESCAPE PROBABILITY
    !--        DU 07 : PUMPING RATE
    !--        DU 08 : DOPPLER WIDTH
    !--        DU 09 : JiD
    !--        DU 10 : INTEGRATED RELATIVE ABUNDANCE - spelin%XXl
    !--        DU 11 : THERMAL VELOCITY - spelin%vel
    !---------------------------------------------------------------------------
    DO k = 1, nspl
       IF (spec_lin(k)%ntr .GT. 0 ) THEN
          CALL WRITE_DATA_SPELIN(filefits,unitxml,numhdu,spec_lin(k))
       ENDIF
    ENDDO

    !---------------------------------------------------------------------------
    !-- DU 021 DUST PROPERTIES
    !---------------------------------------------------------------------------
    !----------------------------------------------------------------------------------
    !--- du_prop%abso - Dust properties
    !----------------------------------------------------------------------------------
    numhdu  = numhdu+1
    nlgn    = nwlg+1
    ncol    = npo+1
    name = "Dust absorption factor"
    extname = "DU_DUPROP_ABSO"
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))

    DO i = 1, ncol
       descol(i)%titre     = name
       CALL INT_STR(i-1, s_i)
       descol(i)%IDFD      = "FD_DUPROP_ABSO_"//TRIM(ADJUSTL(s_i))
       descol(i)%form      = "1D"
       descol(i)%unite     = ""
       descol(i)%datatyp   = "double"
       descol(i)%ucd       = ""
       descol(i)%utype     = ""
       descol(i)%flag_desc = 0
    ENDDO
    DO i = 0, nlgn-1
       DO j = 0, ncol-1
          rval2D(i+1, j+1) = du_prop%abso(i,j) ! (ligne = nwlg, colonne = npo)
       ENDDO
    ENDDO
    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "" ! ATTENTION CAS PARTICULIER
    blocDUxml%modeDU    = "DEV"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------------
    !--- du_prop%scat - Dust properties
    !----------------------------------------------------------------------------------
    numhdu  = numhdu+1
    nlgn    = nwlg+1
    ncol    = npo+1
    name = "Dust absorption factor"
    extname = "DU_DUPROP_SCAT"
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))

    DO i = 1, ncol
       descol(i)%titre     = name
       CALL INT_STR(i-1, s_i)
       descol(i)%IDFD      = "FD_DUPROP_SCAT_"//TRIM(ADJUSTL(s_i))
       descol(i)%form      = "1D"
       descol(i)%unite     = ""
       descol(i)%datatyp   = "double"
       descol(i)%ucd       = ""
       descol(i)%utype     = ""
       descol(i)%flag_desc = 0
    ENDDO
    DO i = 0, nlgn-1
       DO j = 0, ncol-1
          rval2D(i+1, j+1) = du_prop%scat(i,j) ! (ligne = nwlg, colonne = npo)
       ENDDO
    ENDDO
    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "" ! ATTENTION CAS PARTICULIER
    blocDUxml%modeDU    = "DEV"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------------
    !--- du_prop%emis - Dust properties
    !----------------------------------------------------------------------------------
    numhdu  = numhdu+1
    nlgn    = nwlg+1
    ncol    = npo+1
    name = "Dust absorption factor"
    extname = "DU_DUPROP_EMIS"
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))

    DO i = 1, ncol
       descol(i)%titre     = name
       CALL INT_STR(i-1, s_i)
       descol(i)%IDFD      = "FD_DUPROP_EMIS_"//TRIM(ADJUSTL(s_i))
       descol(i)%form      = "1D"
       descol(i)%unite     = ""
       descol(i)%datatyp   = "double"
       descol(i)%ucd       = ""
       descol(i)%utype     = ""
       descol(i)%flag_desc = 0
    ENDDO
    DO i = 0, nlgn-1
       DO j = 0, ncol-1
          rval2D(i+1, j+1) = du_prop%emis(i,j) ! (ligne = nwlg, colonne = npo)
       ENDDO
    ENDDO
    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "" ! ATTENTION CAS PARTICULIER
    blocDUxml%modeDU    = "DEV"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------------
    !--- du_prop%gcos - Dust properties
    !----------------------------------------------------------------------------------
    numhdu  = numhdu+1
    nlgn    = nwlg+1
    ncol    = npo+1
    name = "Dust absorption factor"
    extname = "DU_DUPROP_GCOS"
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))

    DO i = 1, ncol
       descol(i)%titre     = name
       CALL INT_STR(i-1, s_i)
       descol(i)%IDFD      = "FD_DUPROP_GCOS_"//TRIM(ADJUSTL(s_i))
       descol(i)%form      = "1D"
       descol(i)%unite     = ""
       descol(i)%datatyp   = "double"
       descol(i)%ucd       = ""
       descol(i)%utype     = ""
       descol(i)%flag_desc = 0
    ENDDO
    DO i = 0, nlgn-1
       DO j = 0, ncol-1
          rval2D(i+1, j+1) = du_prop%gcos(i,j) ! (ligne = nwlg, colonne = npo)
       ENDDO
    ENDDO
    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "" ! ATTENTION CAS PARTICULIER
    blocDUxml%modeDU    = "DEV"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------------
    !--- du_prop%albe - Dust properties
    !----------------------------------------------------------------------------------
    numhdu  = numhdu+1
    nlgn    = nwlg+1
    ncol    = npo+1
    name = "Dust absorption factor"
    extname = "DU_DUPROP_ALBE"
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))

    DO i = 1, ncol
       descol(i)%titre     = name
       CALL INT_STR(i-1, s_i)
       descol(i)%IDFD      = "FD_DUPROP_ALBE_"//TRIM(ADJUSTL(s_i))
       descol(i)%form      = "1D"
       descol(i)%unite     = ""
       descol(i)%datatyp   = "double"
       descol(i)%ucd       = ""
       descol(i)%utype     = ""
       descol(i)%flag_desc = 0
    ENDDO
    DO i = 0, nlgn-1
       DO j = 0, ncol-1
          rval2D(i+1, j+1) = du_prop%albe(i,j) ! (ligne = nwlg, colonne = npo)
       ENDDO
    ENDDO
    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "" ! ATTENTION CAS PARTICULIER
    blocDUxml%modeDU    = "DEV"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !---------------------------------------------------------------------------
    !-- DU 022 CHEMICAL NETWORK PROPERTIES
    !--        nlgn = nconst_tab_chi    ncol = 2
    !--        FORMAT : STRING INTEGER
    !---------------------------------------------------------------------------
    numhdu  = numhdu+1
    !PRINT *,"Ecriture des proprietes de la chimie - HDU :",numhdu
    nlgn = 1
    ncol = nconst_tab_chi
    ALLOCATE(descol(ncol))
    ALLOCATE(ival2D(nlgn,ncol)) ! seconde colonne  : entiers
    CALL CRE_DATA_CHIM
    name    = "Chemistry properties"
    extname = "DU_PROPCHEM"

    type_write = "int2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "CNT_CHEMISTRY_PROP"
    blocDUxml%modeDU    = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Properties of chemistry : Number of chemical &
                          &reactions of each type"
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(ival2D)

    !---------------------------------------------------------------------
    !-- DU 023 INDEXES OF SPECIAL REACTIONS
    !--        nlgn = nreact_special    ncol = 2
    !--        FORTMAT : STRING INTEGER
    !---------------------------------------------------------------------
    numhdu=numhdu+1
    !PRINT *,"Ecriture des numeros de reactions speciales - HDU :",numhdu
    nlgn = 1
    ncol = nreact_special
    ! 1 colonne pour sval et une pour ival
    ALLOCATE(descol(ncol))
    ALLOCATE(ival2D(nlgn,ncol)) ! seconde colonne  : entiers
    CALL CRE_DATA_INDREAC

    name        = "Index of special reactions"
    extname     = "DU_NUMSPEREACT"

    type_write = "int2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_CHEMISTRY_NSPEREACT"
    blocDUxml%modeDU   = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Numbers identifying special reactions"
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(ival2D)

    !-----------------------------------------------------------------------
    !-- DU 024 NUMBER OF REACTIONS FOR EACH SPECIES (VARIABLES)
    !--        nlgn = n_var, ncol = 1
    !--        Format DOUBLE
    !-----------------------------------------------------------------------
    numhdu  = numhdu+1
    name    = "Number of reactions for species"
    extname = "DU_CHEM_NREACT"
    !PRINT *,"Ecriture des nombres de reactions par especes"
    ncol = 1
    nlgn = n_var
    ALLOCATE(descol(ncol))
    ALLOCATE(ival2D(nlgn,ncol))
    descol(1)%titre   = "Number of reactions"
    descol(1)%IDFD    = "FD_CHEM_NREACT"
    descol(1)%form    = "1J"
    descol(1)%unite   = "no unit"
    descol(1)%datatyp = "integer"
    descol(1)%ucd     = ""
    descol(1)%utype   = ""
    descol(1)%flag_desc = 0
    DO j = 1, nlgn
         ival2D(j,1) = xmatk(j)%nbt
    ENDDO

    type_write = "int2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "CNT_CHEMISTRY_NREACT"
    blocDUxml%modeDU    = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Number of reactions for each species"
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(ival2D)

    !-----------------------------------------------------------------------
    !-- DU 024b REACTIONS FOR EACH VARIABLES
    !--         nlgn = xmatk(isp)%nbt    ncol = ???
    !-- Write xmatk(isp) in n_var Data Units
    !-----------------------------------------------------------------------
    DO i = 1, n_var ! On ne met pas la case 0 correspondant a l'espece RIEN
       espece = speci(i) ! species contains all species and moments and special
       CALL SPEMAJ(espece,especeID)
       numhdu  = numhdu+1
       name    = "Reactions for "//espece
       extname = "DU_REACT_SPE_"//especeID
       !PRINT *,"WRITE DU : ",TRIM(extname)
       ncol    = 9 ! r1, r2, r3, p1, p2, p3, p4, io, it - We do not write k here
       nlgn    = xmatk(i)%nbt
       ALLOCATE(descol(ncol))
       ALLOCATE(ival2D(nlgn,ncol))
       descol(1)%titre   = "React. 1"
       descol(1)%IDFD    = "FD_R1"
       descol(1)%form    = "1J"
       descol(1)%unite   = "no unit"
       descol(1)%datatyp = "integer"
       descol(1)%ucd     = ""
       descol(1)%utype   = ""
       descol(1)%flag_desc = 0

       descol(2)%titre   = "React. 2"
       descol(2)%IDFD    = "FD_R2"
       descol(2)%form    = "1J"
       descol(2)%unite   = "no unit"
       descol(2)%datatyp = "integer"
       descol(2)%ucd     = ""
       descol(2)%utype   = ""
       descol(2)%flag_desc = 0

       descol(3)%titre   = "React. 3"
       descol(3)%IDFD    = "FD_R3"
       descol(3)%form    = "1J"
       descol(3)%unite   = "no unit"
       descol(3)%datatyp = "integer"
       descol(3)%ucd     = ""
       descol(3)%utype   = ""
       descol(3)%flag_desc = 0

       descol(4)%titre   = "Product 1"
       descol(4)%IDFD    = "FD_P1"
       descol(4)%form    = "1J"
       descol(4)%unite   = "no unit"
       descol(4)%datatyp = "integer"
       descol(4)%ucd     = ""
       descol(4)%utype   = ""
       descol(4)%flag_desc = 0

       descol(5)%titre   = "Product 2"
       descol(5)%IDFD    = "FD_P2"
       descol(5)%form    = "1J"
       descol(5)%unite   = "no unit"
       descol(5)%datatyp = "integer"
       descol(5)%ucd     = ""
       descol(5)%utype   = ""
       descol(5)%flag_desc = 0

       descol(6)%titre   = "Product 3"
       descol(6)%IDFD    = "FD_P3"
       descol(6)%form    = "1J"
       descol(6)%unite   = "no unit"
       descol(6)%datatyp = "integer"
       descol(6)%ucd     = ""
       descol(6)%utype   = ""
       descol(6)%flag_desc = 0

       descol(7)%titre   = "Product 4"
       descol(7)%IDFD    = "FD_P4"
       descol(7)%form    = "1J"
       descol(7)%unite   = "no unit"
       descol(7)%datatyp = "integer"
       descol(7)%ucd     = ""
       descol(7)%utype   = ""
       descol(7)%flag_desc = 0

       descol(8)%titre   = "Order of reaction"
       descol(8)%IDFD    = "FD_IO"
       descol(8)%form    = "1J"
       descol(8)%unite   = "no unit"
       descol(8)%datatyp = "integer"
       descol(8)%ucd     = ""
       descol(8)%utype   = ""
       descol(8)%flag_desc = 0

       descol(9)%titre   = "Type of reaction"
       descol(9)%IDFD    = "FD_IT"
       descol(9)%form    = "1J"
       descol(9)%unite   = "no unit"
       descol(9)%datatyp = "integer"
       descol(9)%ucd     = ""
       descol(9)%utype   = ""
       descol(9)%flag_desc = 0

       DO k = 1, xmatk(i)%nbt
          ival2D(k,1) = xmatk(i)%fdt(k)%r1
          ival2D(k,2) = xmatk(i)%fdt(k)%r2
          ival2D(k,3) = xmatk(i)%fdt(k)%r3
          ival2D(k,4) = xmatk(i)%fdt(k)%p1
          ival2D(k,5) = xmatk(i)%fdt(k)%p2
          ival2D(k,6) = xmatk(i)%fdt(k)%p3
          ival2D(k,7) = xmatk(i)%fdt(k)%p4
          ival2D(k,8) = xmatk(i)%fdt(k)%io
          ival2D(k,9) = xmatk(i)%fdt(k)%it
       ENDDO
       type_write = "int2D"
       CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

       !--- Creation du XML --------------
       blocDUxml%nameDU    = name
       blocDUxml%IDDU      = extname
       blocDUxml%nrows     = nlgn
       blocDUxml%ncol      = ncol
       blocDUxml%CNTIDDU   = "" ! Special n'apparait pas dans l'arbre
       blocDUxml%modeDU    = "DEV"
       blocDUxml%flag_desc = 1
       blocDUxml%descripDU = "Reactions for each species in chemistry"
       naxe = 0
       CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

       DEALLOCATE(descol)
       DEALLOCATE(ival2D)

    ENDDO

    !-----------------------------------------------------------------------
    !-- DU 025 RECOMBINATION RATES OF IONS ON GRAINS : rxigr
    !--         nlgn = npo+1    ncol = nspec+1
    !--         Format DOUBLE
    !-----------------------------------------------------------------------
    numhdu  = numhdu+1
    name    = "Recombination rates on grains"
    extname = "DU_GR_RECOMB"
    !PRINT *,"Ecriture des taux de recombinaison sur les grains"
    ncol = nspec + 1
    nlgn = npo + 1
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    numH2 = 0
    DO i = 0, nspec
       espece = speci(i)
       CALL SPEMAJ(espece,especeID)
       descol(i+1)%titre   = espece
       descol(i+1)%IDFD    = "FD_GR_RECOMB_SPE_"//especeID
       descol(i+1)%form    = "1D"
       descol(i+1)%unite   = "cm-3 s-1"
       descol(i+1)%datatyp = "double"
       descol(i+1)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/RecombinationRateOnGrains"
       descol(i+1)%utype   = ""
       descol(i+1)%flag_desc = 0
    ENDDO
    DO i = 0, nspec
       DO j = 0, npo
          rval2D(j+1,i+1) = rxigr(i,j)
       ENDDO
    ENDDO

    type_write = "double2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "CNT_CHEMISTRY_RECOMBGR"
    blocDUxml%modeDU    = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Recombination rates of ions on grains versus position"
    naxe = 1
    descaxe(1)%name     = '"Optical depth"'
    descaxe(1)%ID       = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !---------------------------------------------------------------------
    !-- DU 026 CHEMICAL NETWORK
    !--        nlgn = neqt    ncol = ncol_chim
    !--        FORMAT I I I I I I I I D D D D I
    !---------------------------------------------------------------------
    numhdu  = numhdu+1
    name    = "Chemical reactions"
    extname = "DU_REACTIONS"
    !PRINT *,"Ecriture des reactions chimiques - HDU :",numhdu
    nlgn = neqt
    ncol = ncol_chim ! produits de 0 a 7 plus gamm, beta, alpha, de, itype
    ALLOCATE(descol(ncol))
    ALLOCATE(ival2D(nlgn,8))
    ALLOCATE(rval2D(nlgn,4))
    ALLOCATE(ival(nlgn))
    descol(1)%titre   = "Num. Product";   descol(1)%IDFD  = "FD_REACT_NPROD"
    descol(2)%titre   = "Reac 1"    ;     descol(2)%IDFD  = "FD_REACT_R1"
    descol(3)%titre   = "Reac 2"    ;     descol(3)%IDFD  = "FD_REACT_R2"
    descol(4)%titre   = "Reac 3"    ;     descol(4)%IDFD  = "FD_REACT_R3"
    descol(5)%titre   = "Prod 1"    ;     descol(5)%IDFD  = "FD_REACT_P1"
    descol(6)%titre   = "Prod 2"    ;     descol(6)%IDFD  = "FD_REACT_P2"
    descol(7)%titre   = "Prod 3"    ;     descol(7)%IDFD  = "FD_REACT_P3"
    descol(8)%titre   = "Prod 4"    ;     descol(8)%IDFD  = "FD_REACT_P4"
    descol(9)%titre   = "gamma"     ;     descol(9)%IDFD  = "FD_REACT_GAMMA"
    descol(10)%titre  = "alpha"     ;     descol(10)%IDFD = "FD_REACT_ALPHA"
    descol(11)%titre  = "beta"      ;     descol(11)%IDFD = "FD_REACT_BETA"
    descol(12)%titre  = "Delta H"   ;     descol(12)%IDFD = "FD_REACT_DE"
    descol(13)%titre  = "Type"      ;     descol(13)%IDFD = "FD_REACT_TYPE"
    descol(1:8)%form  = "1J" ! nom des reactifs
    descol(1:8)%datatyp  = "integer"
    descol(9:12)%form    = "1D" ! gamm, alpha, beta, de
    descol(9:12)%datatyp = "double"
    descol(13)%form      = "1J" ! itype
    descol(13)%datatyp   = "integer"
    descol(:)%unite      = "no unit"
    descol(:)%ucd   = ""
    descol(:)%utype = ""
    descol(:)%flag_desc = 0
    DO i = 1, neqt
       DO j = 0, 7
          ival2D(i,j+1) = react(i,j)
       ENDDO
       rval2D(i,1) = gamm(i)
       rval2D(i,2) = alpha(i)
       rval2D(i,3) = bet(i)
       rval2D(i,4) = de(i)
       ival(i)     = itype(i)
    ENDDO

    type_write = "chim"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_CHEMISTRY_REACT"
    blocDUxml%modeDU   = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "<![CDATA[Chemical reactions <br/>  &
                          &Species are identified by a number]]>"
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(ival2D)
    DEALLOCATE(rval2D)
    DEALLOCATE(ival)

    !-----------------------------------------------------------------------
    !-- DU 027 LIST OF SPECIES WITH DIRECT PHOT-REACTIONS
    !--        nlgn = nphot_i    ncol = 1
    !--        FORMAT : STRING
    !-----------------------------------------------------------------------
    numhdu  = numhdu + 1
    name    = "Species direct integration"
    extname = "DU_SPEDIRECTINT"
    !PRINT *, "Liste des especes avec integration directe"
    !PRINT *, "Num. HDU : ",numhdu

    ncol = 1
    nlgn = nphot_i
    ALLOCATE(descol(ncol))
    ALLOCATE(sval(nlgn))
    descol(1)%titre = "Espece"
    descol(1)%IDFD  = "FD_SPEDIRECTINT_SPE"
    descol(1)%form  = "8A"
    descol(1)%unite = "no unit"
    descol(1)%datatyp = "string"
    descol(1)%ucd = ""
    descol(1)%utype = ""
    descol(1)%flag_desc = 0
    DO i = 1, nlgn
       sval(i) = phdest(i)%r1
    ENDDO

    type_write = "string"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_CHEMISTRY_SPEDIRINT"
    blocDUxml%modeDU   = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "<![CDATA[List of chemical species <br/> &
                          & with photo-destruction directly computed &
                          & with cross sections]]>"
    naxe = 0
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(sval)

    !-----------------------------------------------------------------------
    !-- DU 028 PHOTO-REACTION RATES
    !--        nlgn = npo+1    ncol = nphot_i
    !--        FORMAT : DOUBLE
    !-----------------------------------------------------------------------
    numhdu  = numhdu + 1
    name    = "Photodestruction probabilities"
    extname = "DU_RATE_PHOTODES"
    !PRINT *,"Photodissociation rates"
    ncol = nphot_i
    nlgn = npo+1
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    espece_pre = ""
    DO i = 1, ncol
       espece = TRIM(ADJUSTL(phdest(i)%r1))
       CALL SPEMAJ(espece,especeID)
       ! On ajoute un numero a la fin du nom de l'espece pour differencier
       ! les voies de photodissociation lorsqu'il y en a plusieurs
       IF (espece .NE. espece_pre) THEN
          num = 1
       ELSE IF (espece .EQ. espece_pre) THEN
          num = num+1
       ENDIF
       CALL INT_STR(num,snum)
       espece_pre = especeID
       especeID = TRIM(ADJUSTL(espece))//"_"//TRIM(ADJUSTL(snum))
       descol(i)%titre = "Photodestruction probability "//&
                         TRIM(ADJUSTL(especeID))
       descol(i)%IDFD  = "FD_RATE_PHOTODES_SPE_"//especeID
       descol(i)%form  = "1D"
       descol(i)%unite = "s-1"
       descol(i)%datatyp = "double"
       descol(i)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/PhotodestructionRate"
       descol(i)%utype   = ""
       descol(i)%flag_desc = 0
       DO j = 0, npo
          rval2D(j+1,i) = pdiesp(i,j)
       ENDDO
    ENDDO

    type_write = "double2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "CNT_CHEMISTRY_PHOTODISS"
    blocDUxml%modeDU    = "USER"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "<![CDATA[<b>Probability of photodestruction (s-1)</b> &
                          &for species with computation of photodestruction by &
                          &direct integration of photodestruction cross sections]]>"
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !-----------------------------------------------------------------------
    !-- DU 029 RATES OF SPECIAL REACTIONS (H2 formation, destruction, ...)
    !--        nlgn = npo+1    ncol = ncol_sperat
    !--        Format lgn : DOUBLE
    !-----------------------------------------------------------------------
    numhdu = numhdu + 1
    name = "Special reaction rates"
    extname = "DU_RATESPEREACT"
    !PRINT *,"Formation and destruction"
    ncol = ncol_sperat
    nlgn = npo+1
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    CALL CRE_DATA_SPERATES

    type_write = "double2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_CHEMISTRY_FORMDEST"
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Rates of special reactions"
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !-----------------------------------------------------------------------
    !-- DU 030 OTHER : FLUPH
    !--        nlgn = npo+1    ncol = 1
    !--        Format lgn : DOUBLE
    !-----------------------------------------------------------------------
    numhdu = numhdu + 1
    name    = "Other: fluph"
    extname = "DU_OTHER_FLUPH"
    !PRINT *,"Other: fluph"
    ncol = 1
    nlgn = npo+1
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))

    descol(:)%form = "1D"
    descol(:)%datatyp = "double"

    descol(1)%titre   = "fluph"
    descol(1)%unite   = "???"
    descol(1)%IDFD    = "FD_OTHER_FLUPH"
    descol(1)%ucd     = ""
    descol(1)%utype   = ""
    descol(1)%flag_desc = 0
    rval2D(1:npo+1,1)       = fluph(0:npo)

    type_write = "double2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_CHEMISTRY_OTHER"
    blocDUxml%modeDU   = "DEV"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !-----------------------------------------------------------------------
    !-- DU 031 GRAINS RADIUS AND SURFACES
    !--       nlgn = npg   ncol = 2
    !--       FORMAT : DOUBLE
    !------------------------------------------------------------------------
    numhdu  = numhdu+1
    name    = "Radius and surface"
    extname = "DU_GRRADIUS" ! LIEN AXE
    !PRINT *,"Ecriture du rayon et de la surface des grains"
    ncol = 2
    nlgn = npg
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    descol(1)%titre = "Radius"
    descol(1)%form  = "1D"
    descol(1)%unite = "cm"
    descol(1)%IDFD  = "FD_GRRADIUS"
    descol(1)%datatyp = "double"
    descol(1)%ucd   = "http://purl.org/astronomy/vocab/PhysicalQuantities/GrainRadius"
    descol(1)%utype = ""
    descol(1)%flag_desc = 0
    descol(2)%titre = "Surface"
    descol(2)%form  = "1D"
    descol(2)%unite = "cm-2"
    descol(2)%IDFD  = "FD_GRSURFACE"
    descol(2)%datatyp = "double"
    descol(2)%ucd   = "http://purl.org/astronomy/vocab/PhysicalQuantities/GrainSurface"
    descol(2)%utype = ""
    descol(2)%flag_desc = 0
    rval2D(1:npg,1) = adust(1:npg)
    rval2D(1:npg,2) = surfgr(1:npg)

    type_write = "double2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "CNT_GRAINS_RADIUS"
    blocDUxml%modeDU    = "USER"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Radius (cm) and surface (cm2) of grains per bin size of grains"
    naxe = 1
    descaxe(1)%name = '"Grain radius"'
    descaxe(1)%ID   = '"DU_GRRADIUS.FD_GRRADIUS"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !-----------------------------------------------------------------------
    !-- DU 032 GRAINS TEMPERATURE
    !--        nlgn = (npo+1)*npg  ncol = 1
    !--        FORMAT : DOUBLE
    !-----------------------------------------------------------------------
    ! ATTENTION : On met tout sur une colonne pour faciliter les graphs 3D
    !             Il serait plus logique de ranger sur plusieurs colonnes
    !             et c'est ensuite le client qui fera la re-organisation
    numhdu  = numhdu+1
    name    = "Dust temperature"
    extname = "DU_GRTEMP"
    !PRINT *,"Ecriture de la temperature des grains"
    ncol = 1
    nlgn = (npo + 1) * npg
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))

    descol(1)%titre     = name
    descol(1)%IDFD      = "FD_GRTEMP"
    descol(1)%form      = "1D"
    descol(1)%unite     = "K"
    descol(1)%datatyp   = "double"
    descol(1)%ucd       = "http://purl.org/astronomy/vocab/PhysicalQuantities/TemperatureOfGrain"
    descol(1)%utype     = ""
    descol(1)%flag_desc = 0
    k = 0
    DO i = 1, npg
       DO j = 0, npo
          k = k + 1
          rval2D(k,1) = tdust(i,j)
       ENDDO
    ENDDO

    type_write = "double2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_GRAINS_TEMP"
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Temperature of grains by size of grains"
    naxe = 2
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    descaxe(2)%name = '"Grain radius"'
    descaxe(2)%ID   = '"DU_GRRADIUS.FD_GRRADIUS"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !-----------------------------------------------------------------------
    !-- DU 033 GRAINS CHARGE - zmoy
    !--        nlgn = (npo+1) * npg    ncol = 1
    !--        FORMAT : DOUBLE
    !-----------------------------------------------------------------------
    numhdu  = numhdu+1
    name    = "Dust charge"
    extname = "DU_GRCHARGE"
    !PRINT *,"Ecriture de la charge des grains"
    ncol = 1
    nlgn = (npo + 1) * npg
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    descol(1)%titre = "Dust charge"
    descol(1)%IDFD  = "FD_GRCHARGE"
    descol(1)%form  = "1D"
    descol(1)%unite = "no unit"
    descol(1)%datatyp = "double"
    descol(1)%ucd = "http://purl.org/astronomy/vocab/PhysicalQuantities/ChargeOfGrain"
    descol(1)%utype = ""
    descol(1)%flag_desc = 0

    k = 0
    DO i = 1, npg
       DO j = 0, npo
          k = k + 1
          rval2D(k,1) = zmoy(i,j)
       ENDDO
    ENDDO

    type_write = "double2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_GRAINS_CHARGE"
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Charge of grains by size of grains"
    naxe = 2
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    descaxe(2)%name = '"Grain radius"'
    descaxe(2)%ID   = '"DU_GRRADIUS.FD_GRRADIUS"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !-----------------------------------------------------------------------
    !-- DU 034 GAUSS POINTS - wwmg
    !--        nlgn = npg    ncol = 1
    !--        FORMAT : DOUBLE
    !-----------------------------------------------------------------------
    numhdu  = numhdu+1
    name    = "Gauss points"
    extname  = "DU_GAUSSPT"
    !PRINT *,"Ecriture des points de gauss HDU "
    ncol = 1
    nlgn = npg
    ALLOCATE(descol(ncol))
    ALLOCATE(rval(nlgn))
    descol(1)%titre = "Gauss point"
    descol(1)%IDFD = "FD_GAUSSPT"
    descol(1)%form  = "1D"
    descol(1)%unite = "no unit"
    descol(1)%datatyp = "double"
    descol(1)%ucd = ""
    descol(1)%utype = ""
    descol(1)%flag_desc = 0
    rval(:) = wwmg(:)

    type_write = "double"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_GRAINS_GAUSSPT"
    blocDUxml%modeDU   = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Gauss points used gor gaussian integration"
    naxe = 1
    descaxe(1)%name = '"Grain radius"'
    descaxe(1)%ID   = '"DU_GRRADIUS.FD_GRRADIUS"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval)

    !-----------------------------------------------------------------------
    !-- DU 035 : ENERGY DENSITY
    !--          nlgn = npo+1    ncol = 1
    !--          FORMAT DOUBLE
    !-----------------------------------------------------------------------
    numhdu  = numhdu + 1
    name    = "Energy density"
    extname = "DU_RADFIELD"
    !PRINT *,"Energy density"
    ncol = 1
    nlgn = npo+1
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))

    descol(:)%form = "1D"
    descol(:)%datatyp = "double"

    !--- Colonne 1 : int_rad ---------
    descol(1)%titre     = "Energy density"
    descol(1)%IDFD      = "FD_ENER_DENSITY"
    descol(1)%unite     = "???"
    descol(1)%ucd       = "http://purl.org/astronomy/vocab/PhysicalQuantities/DensityOfEnergy"
    descol(1)%utype     = ""
    descol(1)%flag_desc = 0
    rval2D(1:npo+1,1)   = int_rad(0:npo)

    type_write = "double2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "CNT_RADFIELD"
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    PRINT *,"================================================"
    PRINT *,"               WRITE SPECTRA                    "
    PRINT *,"================================================"

    !-----------------------------------------------------------------------
    !-- DU 036 WAVELENGTH TABLE
    !--        nlgn = rf_wl%upper-rf_wl%lower+1    ncol = 1
    !--        FORMAT : DOUBLE
    !-----------------------------------------------------------------------
    numhdu  = numhdu + 1
    name    = "Wavelength"
    extname = "DU_WAVELENGTH"
    ncol = 1
    nlgn = rf_wl%upper-rf_wl%lower+1
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    descol(1)%titre     = "Wavelength"
    descol(1)%IDFD      = "FD_WAVELENGTH"
    descol(1)%form      = "1D"
    descol(1)%unite     = "Angstroms"
    descol(1)%datatyp   = "double"
    descol(1)%ucd       = "http://purl.org/astronomy/vocab/PhysicalQuantities/Wavelength"
    descol(1)%utype     = ""
    descol(1)%flag_desc = 0

    DO i = 1, nlgn
       rval2D(i,1) = rf_wl%Val(rf_wl%lower+i-1) ! rf_wl%Val va de 0 to nwlg
    ENDDO

    type_write = "double2D"
    CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows    = nlgn
    blocDUxml%ncol     = ncol
    blocDUxml%CNTIDDU  = "CNT_SPECTRA"
    blocDUxml%modeDU   = "DEV"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 1
    descaxe(1)%name = '"Wavelength"'
    descaxe(1)%ID   = '"DU_WAVELENGTH.FD_WAVELENGTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------
    !-- DU 037 INCIDENT SPECTRA
    !----------------------------------------------------------------------------
    CALL WRITE_SPECTRE_INC(npixlam,filefits,numhdu,unitxml)

    !----------------------------------------------------------------------------
    !-- DU 038 ABSORPTION SPECTRA
    !--        Write the absorption spectra on different angles
    !----------------------------------------------------------------------------
    CALL WRITE_SPECTRE_ABS(npixlam,filefits,numhdu,unitxml)

    !----------------------------------------------------------------------------
    !-- DU 039 SPECTRA FOR EACH ANGLES
    !--        Write the incident and emergent spectra for each angles
    !----------------------------------------------------------------------------
    CALL WRITE_SPECTRA_ANG(npixlam,filefits,numhdu,unitxml)

    !----------------------------------------------------------------------------
    !-- END OF RESSOURCE WITH METADATA OF ALL MAIN QUANTITIES in output
    !----------------------------------------------------------------------------
    WRITE(unitxml,"(A11)") '</RESOURCE>'

     !--- fermeture du FITS --------------------------------------------------
     call ftclos(unitfits, statusfits)
     call printerrfits(statusfits)
     !-- Free unit number ----------------------------------------------------
     call ftfiou(unitfits, statusfits)
     call printerrfits(statusfits)

    !---------------------------------------------------------------------------
    !-- TABLE : DENSITE D'ENERGIE EN FONCTION DE LA POSITION ET DE LAMBDA
    !---------------------------------------------------------------------------
!    ! Open ressource for energy densities
!    WRITE(unitxml,*) '<RESOURCE name="DataUnit"   ID="RES_DENSENER">'
!    ! Data for energy density are stored in a separate file
!    ! Creation du nom de fichier .fits de sortie

!    fileuv = TRIM(out_dir)//TRIM(modele)//'.rf'
!    fileuvfits = TRIM(out_dir)//TRIM(ADJUSTL(modele))//"_"//TRIM(ADJUSTL(ii2c(ifaf)))//"_rf.fits"
!    CALL WRITE_RADFIELD(fileuv,fileuvfits,unitxml)

!    ! Fin de la ressources sur les densites d'energies
!    WRITE(unitxml,*) ' </RESOURCE>'

    !---------------------------------------------------------------------------------
    ! CLOSE VO-TABLE
    !---------------------------------------------------------------------------------
    WRITE(unitxml,*) ' </VOTABLE>'
    CLOSE(unitxml) ! CLOSE VO-TABLE

    !---------------------------------------------------------------------------------
    ! DEALLOCATION
    !---------------------------------------------------------------------------------
     DEALLOCATE(species)     ! Name of species in chemical network
     DEALLOCATE(moments)     ! List of moments used in the simulation
     DEALLOCATE(massmol)     ! Molar mass of species in the chemical network
     DEALLOCATE(abinitial)   ! Abondances initiales
     DEALLOCATE(enthalp)     ! Formation enthalpy of species in chemistry
     DEALLOCATE(temp_evap)   ! Evaporation threshold from grains
     DEALLOCATE(temp_diff)   ! Diffusion threshold from grains

END SUBROUTINE OUTPUT_FITS

!!!************************************************************************************
!!!                     SUBROUTINES APPELLEES PAR OUTPUT_FITS
!!!************************************************************************************

!!!====================================================================================
!!! SUBROUTINE PREPAR_CHEMFITS
!!!====================================================================================
SUBROUTINE PREPAR_CHEMFITS
IMPLICIT NONE

INTEGER :: i, j

!--------------------------------------------------------------------------------------
! Build species table
!--------------------------------------------------------------------------------------
species(0:nspec)   = speci(0:nspec)
massmol(0:nspec)   = mol(0:nspec)
abinitial(0:nspec) = abin(0:nspec)
enthalp(0:nspec)   = enth(0:nspec)
temp_evap(0:nspec) = t_evap(0:nspec)
temp_diff(0:nspec) = t_diff(0:nspec)
j = nspec + 1
DO i = n_var+1, n_var+5
   species(j)   = speci(i)
   massmol(j)   = mol(i)
   abinitial(j) = abin(i)
   enthalp(j)   = enth(i)
   temp_evap(j) = t_evap(i)
   temp_diff(j) = t_diff(i)
   j = j + 1
ENDDO

!---------------------------------------------------------------------------------------
! Build moments tables
!---------------------------------------------------------------------------------------
moments(:) = speci(nspec+1:n_var)

END SUBROUTINE PREPAR_CHEMFITS

!!!====================================================================================
!!! SUBROUTINE CRE_CONTAIN
!!!====================================================================================
!!! Write in a VO-TABLE RESSOURCE the list of containers
!!! Containers are the groups in the GUI PDR_Analyzer
!!! To remove/add groups edit the containers.dat
!!! Each Group as an ID.
!!! To add a new physicial quantity to a group, write the quantity in a TABLE of the VO-TABLE
!!! and in the FITS File and provide the container ID as a PARAM of the VO-TABLE
!!!====================================================================================
SUBROUTINE CRE_CONTAIN(unit)

IMPLICIT NONE
INTEGER, INTENT(IN)        :: unit  ! Logical number of XML file

INTEGER, PARAMETER         :: nlignmax = 100000
CHARACTER(LEN=lenname)     :: name
CHARACTER(LEN=lenID)       :: ID

CHARACTER(LEN=lenstring)   :: ligne
TYPE(LINE_TEMPLATE)        :: xmlline
CHARACTER                  :: caract
INTEGER                    :: posTABLE
INTEGER                    :: posPARAM
INTEGER                    :: ios
INTEGER                    :: decal
INTEGER                    :: len_ligne
INTEGER                    :: i, j, k

   !<RESOURCE  ...............  />
   xmlline%ttype = 1 ! RESOURCE
   xmlline%decal = 3 ! Decalage des lignes
   name(:) = " "
   ID(:)   = " "
   name = '"containers"'
   ID   = '"RES_CONTAINERS"'
   CALL INIT_xmlline(xmlline)
   CALL WRITE_nameID(unit,xmlline,name,ID)

    !--- LECTURE DU FICHIER DES CONTAINERS ---
    !--- et ECRITURE DANS LE FICHIER XML   ---
    OPEN(70,FILE="../data/containers.dat",STATUS="old",ACTION="read")
    DO i = 1, nlignmax
       ios = 0
       j   = 0
       ligne = ""
       DO WHILE (ios .EQ. 0)
          READ(70,"(1A)",ADVANCE="NO",IOSTAT=ios,ERR=171,END=170) caract
          IF (ios .EQ. 0) THEN
             j = j + 1
             ligne(j:j) = caract
          ENDIF
       ENDDO
171    continue
       ! Recherche type de ligne qu'on a lu
       ligne = TRIM(ADJUSTL(ligne))
       posTABLE = INDEX(ligne,"<TABLE")
       IF (posTABLE .GT. 0) THEN
          decal = 3
       ENDIF
       posTABLE = INDEX(ligne,"</TABLE")
       IF (posTABLE .GT. 0) THEN
          decal = 3
       ENDIF
       posPARAM = INDEX(ligne,"<PARAM")
       IF (posPARAM .GT. 0) THEN
          decal = 9
       ENDIF
       ! Ecriture dans le fichier xml
       len_ligne = LEN_TRIM(ADJUSTL(ligne))
       IF (len_ligne .GE. 1) THEN
         DO k = 1, decal
            WRITE(unit,"(1A)",ADVANCE="NO") " "
         ENDDO
         DO k = 1, len_ligne
            WRITE(unit,"(1A)", ADVANCE="NO") ligne(k:k)
         ENDDO
         WRITE(unit,*) ! go to next line
       ENDIF
    ENDDO
170 continue ! END OF FILE
    CLOSE(70)

   !--- Fin de RESSOURCE
   CALL ENDRESOURCE(unit,3)

END SUBROUTINE CRE_CONTAIN

!!!====================================================================================
!!! SUBROUTINE CRE_AXE
!!!====================================================================================
!!! Write in a VO-TABLE RESSOURCE the list of possible axes
!!! When a physical quantity is described in the VO-TABLE, one can provide related axes
!!! providing the axis ID as PARAM
!!! Several axis can be related to a quantity this way.
!!! These relations in the VO-TABLE are usefull so that the GUI PDR_Analyzer can extract
!!! physical quantities with the values of the related axes.
!!!
!!! Example :
!!!<RESOURCE name="axes" ID="RES_AXE">
!!!      <TABLE name="Position axes">
!!!         <FIELD name="Optical depth"     ID="DU_OPTDEPTH.FD_OPTDEPTH" />
!!!         <FIELD name="Av"                ID="DU_OPTDEPTH.FD_AV" />
!!!         <FIELD name="Distance"          ID="DU_OPTDEPTH.FD_DISTANCE" />
!!!         <FIELD name="H2 column density" ID="DU_COLDENS.FD_CD_H2" />
!!!      </TABLE>
!!!      <TABLE name="Grain">
!!!         <FIELD name="Grain radius"      ID="DU_GRRADIUS.FD_GRRADIUS" />
!!!      </TABLE>
!!!      <TABLE name="Wavelength">
!!!         <FIELD name="Wavelength"        ID="RES_SPECTRA.DU_SPECTRUM_UV.FD_WAVELENGTH" />
!!!      </TABLE>
!!!</RESOURCE>
!!!====================================================================================
SUBROUTINE CRE_AXE(unit)

USE PXDR_CONSTANTES

IMPLICIT NONE

INTEGER, INTENT(IN)        :: unit
CHARACTER(LEN=lenname)     :: name
CHARACTER(LEN=lenID)       :: ID
CHARACTER(LEN=lenstrint)   :: snrows
TYPE(FIELD)                :: axefield
TYPE(DUXML)                :: blocDUxml
TYPE(LINE_TEMPLATE)        :: xmlline

   !==============================================================================
   ! RESSOURCE AXE
   !==============================================================================
   !<RESOURCE  ...............  />
   xmlline%ttype = 1 ! RESOURCE
   xmlline%decal = 3 ! Decalage des lignes
   name(:) = " "
   ID(:)   = " "
   name = '"axes"'
   ID   = '"RES_AXIS"'
   CALL INIT_xmlline(xmlline)
   CALL WRITE_nameID(unit,xmlline,name,ID)

   !------------------------------------------------------------------------------
   ! TABLE DES AXES SPATIAUX
   !------------------------------------------------------------------------------

   xmlline%ttype = 2 ! TABLE WITH NO END : <TABLE .............. >
   xmlline%decal = 6 ! Decalage des lignes
   name(:) = " "
   ID(:)   = " "
   name = '"Spatial axes"'
   ID   = '"LIST_AXES_POS"'
   snrows = '"0"'
   CALL INIT_xmlline(xmlline)
   CALL WRITE_TABLE(unit,xmlline,name,ID,snrows)

   ! FIELD - Optical depth
   axefield%nameFD    = "Optical depth"
   axefield%IDFD      = "FD_OPTDEPTH"
   axefield%datatypFD = "double"
   axefield%unitFD    = "mag"
   axefield%ucdFD     = "http://purl.org/astronomy/vocab/PhysicalQuantities/OpticalDepthInTheVisible"
   axefield%utypeFD   = ""
   blocDUxml%IDDU     = "DU_OPTDEPTH"
   CALL INIT_xmlline(xmlline)
   CALL WRITE_DUFIELD(unit,axefield,blocDUxml)

   ! FIELD - Av
   axefield%nameFD    = "Av"
   axefield%IDFD      = "FD_AV"
   axefield%datatypFD = "double"
   axefield%unitFD    = "mag"
   axefield%ucdFD     = "http://purl.org/astronomy/vocab/PhysicalQuantities/VisualExtinction"
   axefield%utypeFD   = ""
   blocDUxml%IDDU     = "DU_OPTDEPTH"
   CALL INIT_xmlline(xmlline)
   CALL WRITE_DUFIELD(unit,axefield,blocDUxml)

   ! FIELD - Distance
   axefield%nameFD    = "Distance"
   axefield%IDFD      = "FD_DISTANCE"
   axefield%datatypFD = "double"
   axefield%unitFD    = "cm"
   axefield%ucdFD     = "http://purl.org/astronomy/vocab/PhysicalQuantities/Distance"
   axefield%utypeFD   = ""
   blocDUxml%IDDU     = "DU_OPTDEPTH"
   CALL INIT_xmlline(xmlline)
   CALL WRITE_DUFIELD(unit,axefield,blocDUxml)

   ! FIELD - N(H2)
   axefield%nameFD    = "H2 column density"
   axefield%IDFD      = "FD_CD_H2"
   axefield%datatypFD = "double"
   axefield%unitFD    = "cm-2"
   axefield%ucdFD     = "http://purl.org/astronomy/vocab/PhysicalQuantities/ColumnDensity"
   axefield%utypeFD   = ""
   blocDUxml%IDDU     = "DU_COLDENS"
   CALL INIT_xmlline(xmlline)
   CALL WRITE_DUFIELD(unit,axefield,blocDUxml)

   CALL ENDTABLE(unit,6)

   !------------------------------------------------------------------------------
   ! TABLE DES TAILLE DES GRAINS
   !------------------------------------------------------------------------------

   xmlline%ttype = 2 ! TABLE WITH NO END : <TABLE .............. >
   xmlline%decal = 6 ! Decalage des lignes
   name(:) = " "
   ID(:)   = " "
   name = '"Grain radius"'
   ID   = '"LIST_AXES_GRAIN"'
   snrows = '"0"'
   CALL INIT_xmlline(xmlline)
   CALL WRITE_TABLE(unit,xmlline,name,ID,snrows)

   ! FIELD - Grain radius
   axefield%nameFD    = "Grain radius"
   axefield%IDFD      = "FD_GRRADIUS"
   axefield%datatypFD = "double"
   axefield%unitFD    = "cm"
   axefield%ucdFD     = "http://purl.org/astronomy/vocab/PhysicalQuantities/GrainRadius"
   axefield%utypeFD   = ""
   blocDUxml%IDDU     = "DU_GRRADIUS"
   CALL INIT_xmlline(xmlline)
   CALL WRITE_DUFIELD(unit,axefield,blocDUxml)

   CALL ENDTABLE(unit,6)

   !------------------------------------------------------------------------------
   ! TABLE DES LONGUEURS D'ONDE
   !------------------------------------------------------------------------------

   xmlline%ttype = 2 ! TABLE WITH NO END : <TABLE .............. >
   xmlline%decal = 6 ! Decalage des lignes
   name(:) = " "
   ID(:)   = " "
   name = '"Wavelength"'
   ID   = '"LIST_AXES_WAVELENGTH"'
   snrows = '"0"'
   CALL INIT_xmlline(xmlline)
   CALL WRITE_TABLE(unit,xmlline,name,ID,snrows)

   ! FIELD - Wavelength
   axefield%nameFD    = "Wavelength"
   axefield%IDFD      = "FD_WAVELENGTH"
   axefield%datatypFD = "double"
   axefield%unitFD    = "Angstrom"
   axefield%ucdFD     = "http://purl.org/astronomy/vocab/PhysicalQuantities/Wavelength"
   axefield%utypeFD   = ""
   blocDUxml%IDDU     = "DU_WAVELENGTH"
   CALL INIT_xmlline(xmlline)
   CALL WRITE_DUFIELD(unit,axefield,blocDUxml)
   CALL ENDTABLE(unit,6)

   !------------------------------------------------------------------------------
   ! END RESOURCE
   !------------------------------------------------------------------------------
   CALL ENDRESOURCE(unit,3)

END SUBROUTINE CRE_AXE

!!!==========================================================================
!!! SUBROUTINE CRE_DATA_SIZETAB : tableau des entiers de tailles de tableaux
!!!==========================================================================
SUBROUTINE CRE_DATA_SIZETAB

IMPLICIT NONE
INTEGER             :: i

i = 1
descol(i)%titre   = "Max. num of points in spatial grid"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_NOPTM"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Max. number of points in spatial grid]]>"
ival2D(1,i)       = noptm

i = i + 1
descol(i)%titre   = "Num. of points in spatial grid"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_NPO"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of points in spatial grid]]>"
ival2D(1,i)       = npo

i = i + 1
descol(i)%titre   = "Max num of species"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_NP"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Max number of species]]>"
ival2D(1,i)       = np

i = i + 1
descol(i)%titre   = "Number of species"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_NSPEC"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of species in chemistry]]>"
ival2D(1,i)       = nspec

i = i + 1
descol(i)%titre   = "Number of variables for chemistry"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_NVAR"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of variables in Newton-Raphson to solve chemistry]]>"
ival2D(1,i)       = n_var

i = i + 1
descol(i)%titre   = "Number of atoms plus charge"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_NATOM"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of atoms plus charge]]>"
ival2D(1,i)       = natom

i = i + 1
descol(i)%titre   = "Num. of chemical reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_NEQT"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Num. of chemical reactions]]>"
ival2D(1,i)       = neqt

i = i + 1
descol(i)%titre   = "Max. numbers of points in Gauss distribution"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_NPMG"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Max. number of points in Gauss distribution]]>"
ival2D(1,i)       = npmg

i = i + 1
descol(i)%titre   = "Num. of points in Gauss distribution"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_NPG"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Num. of points in Gauss distribution]]>"
ival2D(1,i)       = npg

i = i + 1
descol(i)%titre   = "Number of H2 levels"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_L0H2"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of H2 ro-vibrationnal levels]]>"
ival2D(1,i)       = l0h2

i = i + 1
descol(i)%titre   = "Number of HD levels"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_L0HD"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of HD ro-vibrationnal levels]]>"
ival2D(1,i)       = l0hd

i = i + 1
descol(i)%titre   = "Number of species with direct integration"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_NPHOTI"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of species with direct integration for photo-reactions]]>"
ival2D(1,i)       = nphot_i

i = i + 1
descol(i)%titre   = "Max v of H2 in B and C states"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_NVBC"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Maximum vibrationnel level of H2 in levels B and C]]>"
ival2D(1,i)       = nvbc

i = i + 1
descol(i)%titre   = "Max J of H2 in B and C states"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_NJBC"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Maximum rotationnal level of H2 in levels B and C]]>"
ival2D(1,i)       = njbc

i = i + 1
descol(i)%titre   = "Number of species with detailed balance"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ARRAYSIZE_NSPL"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of species with detailed balance]]>"
ival2D(1,i)       = nspl

END SUBROUTINE CRE_DATA_SIZETAB

!!!===========================================================================================
!!! SUBROUTINE CRE_DATA_PARAMOD_R
!!! Constructs data concerning model parameters that are real
!!!-------------------------------------------------------------------------------------------
!!! NOTE : Because cfitsio librairies are not the same to write real, integer and strings
!!!        it is more simple to split model parameters in 3 DataUnits
!!!===========================================================================================
SUBROUTINE CRE_DATA_PARAMOD_R
!-- The number of parameters is definied by nparam_mod_r.
!-- If a parameter is added or removed, nparam_mod_r has to be changed

IMPLICIT NONE
INTEGER :: i

descol(:)%flag_desc=0

i = 1
descol(i)%titre   = "ISRF factor (Obs. side) - edge of the cloud"
descol(i)%IDFD    = "FD_RADM"
descol(i)%form    = "1D"
descol(i)%datatyp = "double"
descol(i)%unite   = "no unit"
descol(i)%ucd     = "http://purl.org/astronomy/vocab/PDRParameters/Scaling_Factor_to_Interstellar_Standard_Radiation_Field"
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Scaling factor to Draine radiation field </b> &
                    &on the observer side of the cloud. <br/>&
                    &Different from the value in the parameter &
                    &file because it takes into account : <br/> &
                    &- the presence of the cloud <br/>&
                    &- backscattering by dust<br/>&
                    &- the influence of the backside of the cloud<br/>&
                    &Fortran variable : radm]]>"
rval2D(1,i) = radm

i = i + 1
descol(i)%titre   = "ISRF factor (back side) - edge of the cloud"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_RADP"
descol(i)%datatyp = "double"
descol(i)%ucd     = "http://purl.org/astronomy/vocab/PDRParameters/Scaling_Factor_to_Interstellar_Standard_Radiation_Field"
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Scaling factor to Draine radiation field </b>&
                    &on the backside of the cloud. <br/>&
                    &Different from the value in the parameter &
                    &file because it takes into account : <br/>&
                    &- the presence of the cloud<br/>&
                    &- backscattering by dust<br/>&
                    &- the influence of the observer side of the cloud.]]>"
rval2D(1,i) = radp

i = i + 1
descol(i)%titre = "C = NH / E(B-V)"
descol(i)%form  = "1D"
descol(i)%unite = "cm-2 mag-1"
descol(i)%IDFD    = "FD_CD"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[ <b>N(H) / E(B-V)</b> <br/> &
                    & Ratio of proton column density on reddening. <br/> &
                    & Typical value for the Galaxy is 5.8E21 <br/> &
                    & <i>Reference : Bohlin et al., ApJ, 224, 132, (1978).</i> ]]>"
rval2D(1,i) = cdunit

i = i + 1
descol(i)%titre = "Rv"
descol(i)%form  = "1D"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_RV"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[ <b>Rv = Av / E(B-V)</b> <br/> &
                    & Total to selective extinction ratio.]]>"
rval2D(1,i) = rrv

i = i + 1
descol(i)%titre = "Extinction curve - coeff. 1"
descol(i)%form  = "1D"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_CC1"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[ First parameter in Fitzpatrick  &
                    &expression of the <b>extinction curve</b>. <br/> &
                    &<i>Reference: Fitzpatrick et Massa, &
                    &ApJ Supp. 72, 163, 1990.</i><br/>&
                    &Fortran variable : cc1]]>"
rval2D(1,i) = cc1

i = i + 1
descol(i)%titre = "Extinction curve - coeff. 2"
descol(i)%form  = "1D"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_CC2"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[ Second parameter in Fitzpatrick &
                    & expression of the <b>extinction curve</b>. <br/> &
                    & <i>Reference: Fitzpatrick et Massa, &
                    & ApJ Supp. 72, 163, 1990.</i><br/>&
                    &Fortran variable : cc2]]>"
rval2D(1,i) = cc2

i = i + 1
descol(i)%titre   = "Extinction curve - coeff. 3"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_CC3"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[ Third parameter in Fitzpatrick &
                    & expression of the <b>extinction curve</b>.<br/>&
                    & <i> Reference: Fitzpatrick et Massa, &
                    & ApJ Supp. 72, 163, 1990.</i><br/>&
                    & Fortran variable : cc3]]>"
rval2D(1,i) = cc3

i = i + 1
descol(i)%titre = "Extinction curve - coeff. 4"
descol(i)%form  = "1D"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_CC4"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[ Fourth parameter in Fitzpatrick &
                    &expression of the <b>extinction curve</b>.<br/>&
                    &<i>Reference: Fitzpatrick et Massa, &
                    &ApJ Supp. 72, 163, 1990.</i><br/>&
                    &Fortran variable : cc4]]>"
rval2D(1,i) = cc4

i = i + 1
descol(i)%titre = "Extinction curve - coeff. gamma"
descol(i)%form  = "1D"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_XGAM"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[ Gamma in Fitzpatrick expression of the &
                    &<b>extinction curve</b>.<br/>&
                    &<i>Reference: Fitzpatrick et Massa, &
                    &ApJ Supp. 72, 163, 1990.</i><br/>&
                    &Fortran variable : xgam]]>"
rval2D(1,i) = xgam

i = i + 1
descol(i)%titre = "Extinction curve - coeff. x0"
descol(i)%form  = "1D"
descol(i)%unite = "micrometer-1 "
descol(i)%IDFD    = "FD_Y0"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[ x0 parameter in Fitzpatrick expression of the &
                     &<b>extinction curve</b>. <br/>&
                     &<i>Reference: Fitzpatrick et Massa, &
                     &ApJ Supp. 72, 163, 1990.</i><br/>&
                     &Fortran variable : x0]]>"
rval2D(1,i) = y0

i = i + 1
descol(i)%titre = "Mass grain / Mass of gas"
descol(i)%form  = "1D"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_GRATIO"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[ <b>m_grain / m_gas</b> <br/> &
                     & Dust on grain mass ratio.<br/> &
                     & <i>Typical value: 0.01</i>]]>"
rval2D(1,i) = g_ratio

i = i + 1
descol(i)%titre = "Grains volumic mass"
descol(i)%form  = "1D"
descol(i)%unite = "g cm-3"
descol(i)%IDFD    = "FD_RHOGR"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Grain volumic mass</b><br/>&
                     &<i>Unit: g cm-3</i><br/>&
                     &<i>Typical Value: 3 g cm-3</i><br/>]]>"
rval2D(1,i) = rhogr

i = i + 1
descol(i)%titre   = "Index of grains size distribution"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ALPGR"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Index of grain size distribution</b><br/>&
                     &<i>Typical value: 3.5 </i><br/>&
                     &<i>Reference: Mathis, Rumpl and Nordsieck, &
                     &1977, ApJ, 217, 425.</i>]]>"
rval2D(1,i) = alpgr

i = i + 1
descol(i)%titre = "Grains minimal radius"
descol(i)%form  = "1D"
descol(i)%unite = "cm"
descol(i)%IDFD    = "FD_RMIN"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Minimum radius of grains</b><br/>&
                     &<i>Unit: cm</i>]]>"
rval2D(1,i) = rgrmin

i = i + 1
descol(i)%titre = "Grains maximum radius"
descol(i)%form  = "1D"
descol(i)%unite = "cm"
descol(i)%IDFD    = "FD_RMAX"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Maximum radius of grains</b><br/>&
                     &<i>Unit: cm</i>]]>"
rval2D(1,i) = rgrmax

i = i + 1
descol(i)%titre = "Flux of cosmic rays"
descol(i)%form  = "1D"
descol(i)%unite = "s-1 per H"
descol(i)%IDFD    = "FD_ZETA"
descol(i)%datatyp = "double"
descol(i)%ucd     = "http://purl.org/astronomy/vocab/PDRParameters/Flux_Of_Cosmic_Rays"
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Cosmic ionization rate</b>&
                     &<i>Unit: ionization s-1 per H</i><br/>&
                     &Affects: <br/> &
                     &- chemical rates<br/>&
                     &- heating rates in cloud center<br/>&
                     &- flux of secondary photons<br/>&
                     &<i>Typical value: a few 1E-17</i>]]>"
rval2D(1,i) = zeta

i = i + 1
descol(i)%titre = "Flux of secondary photons"
descol(i)%form  = "1D"
descol(i)%unite = "s-1"
descol(i)%IDFD    = "FD_FPHSEC"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Flux of secondary photons</b><br/>&
                     &=1000 * zeta / 1E-17<br/> &
                     &(depends on the cosmic ionization rate)<br/>&
                     &<i>Unit: s-1</i><br/>&
                     &Fortran variable : fphsec]]>"
rval2D(1,i) = fphsec

i = i + 1
descol(i)%titre = "Micro-turbulent velocity"
descol(i)%form  = "1D"
descol(i)%unite = "cm s-1"
descol(i)%IDFD    = "FD_VTURB"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Turbulent velocity</b><br/>&
                     &Affects Doppler broadening in &
                     &radiative transfer<br/>&
                     &<i>Unit: cm s-1</i><br/>&
                     &<i>Typical values: 2-5E5 cm s-1</i>]]>"
rval2D(1,i) = vturb

i = i + 1
descol(i)%titre = "vmaxw" ! c'est racine de 8h/pi mH
descol(i)%form  = "1D"    ! CE N'EST PAS UN PARAMETER
descol(i)%unite = " "
descol(i)%IDFD    = "FD_VMAXW"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[SQRT(8*pi / mH)]]>"
rval2D(1,i) = vmaxw

i = i + 1
descol(i)%titre = "Normalization factor of grains size distribution"
descol(i)%form  = "1D"
descol(i)%unite = ""
descol(i)%IDFD    = "FD_AGRNRM"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b><n_gr * s_gr> / nH</b><br/>&
                     &Normalization factor for grains distribution<br/>&
                     &Fortran variable : agrnrm]]>"
rval2D(1,i) = agrnrm

i = i + 1
descol(i)%titre = "Mean grains cross section"
descol(i)%form  = "1D"
descol(i)%unite = "cm2"
descol(i)%IDFD    = "FD_SIGNGR"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b><n_gr * s_gr> / nH</b><br/>&
                     &Mean value of the grain cross section&
                     &after integration on the grain size &
                     &distribution<br/>&
                     &<i>Unit: cm2</i><br/>&
                     &Fortran variable : signgr]]>"
rval2D(1,i) = signgr

i = i + 1
descol(i)%titre = "Mean number of adsorption sites per grain"
descol(i)%form  = "1D"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_BSITGR"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Mean number of adsorption &
                     &sites per grain</b><br/>&
                     &after integration on the grain size &
                     &distribution<br/>&
                     &<i>Unit: no unit</i>"
i = i + 1
descol(i)%titre = "sngclb"
descol(i)%form  = "1D"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_SNGCLB"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Coulomb interaction</b><br/>&
                     &= 2 e^2 / (3k) * f2/f3 <br/>&
                     &in equation (A6) by <br/>&
                     &<i>Le Bourlot et al., A&A, 302, &
                     &870, (1995)</i>]]>"
rval2D(1,i) = sngclb

i = i + 1
descol(i)%titre = "rdeso"
descol(i)%form  = "1D"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_RDESO"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Desorption velocity</b> &
                     &per grain-grain collision <br/>&
                     &See equation (B6) by<br/>&
                     &<i>Le Bourlot et al., A&A, 302, &
                     &870, (1995)</i>]]>"
rval2D(1,i) = rdeso

i = i + 1
descol(i)%titre = "Distance between adsorption sites"
descol(i)%form  = "1D"
descol(i)%unite = "cm"
descol(i)%IDFD    = "FD_DSITE"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Distance between &
                     &adsorption sites</b><br/>&
                     &<i>Unit: cm</i><br/> &
                     &Fortran variable : dsite]]>"
rval2D(1,i)         = dsite

i = i + 1
descol(i)%titre = "Av of the cloud"
descol(i)%form  = "1D"
descol(i)%unite = "mag"
descol(i)%IDFD    = "FD_AVMAX"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip   = "<![CDATA[<b>Size of the cloud</b> <br/>&
                       &in visual extinction given as &
                       &input parameter of the model <br/>&
                       &Fortran variable : Avmax]]>"
rval2D(1,i)         = Avmax

i = i + 1
descol(i)%titre = "Proton density (initial)"
descol(i)%form  = "1D"
descol(i)%unite = "cm-3"
descol(i)%IDFD    = "FD_NHINIT"
descol(i)%datatyp = "double"
descol(i)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/Proton_Density"
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Initial proton density</b> &
                     &given as input parameters.<br/>&
                     &<i>Unit: cm-3</i><br/>&
                     &Note: If a density profil has been &
                     &used this parameter has no importance.]]>"
rval2D(1,i)         = nh_init

i = i + 1
descol(i)%titre = "Gas temperature (initial)"
descol(i)%form  = "1D"
descol(i)%unite = "K"
descol(i)%IDFD    = "FD_TINIT"
descol(i)%datatyp = "double"
descol(i)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/TemperatureOfGas"
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Initial temperature</b> &
                     &given in input parameters.<br/>&
                     &<i>Unit: K</i><br/>&
                     &Note: if thermal balance has been solved &
                     &or if a temperature profil has been used, &
                     &this parameter has no importance]]>"
rval2D(1,i)         = tg_init

i = i + 1
descol(i)%titre = "Distance of the point UV source"
descol(i)%form  = "1D"
descol(i)%unite = "pc"
descol(i)%IDFD    = "FD_DSOURCE"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Distance of the external &
                     &source</b><br/>&
                     &<i>Unit: pc</i><br/>&
                     &- if  0.0: no star illuminate the cloud<br/>&
                     &- if >0.0: a star illuminate the cloud on &
                     &the backside<br/>&
                     &- if <0.0: a star illuminate the cloud on &
                     &the observer side<br/>&
                     &Fortran variable : d_sour]]>"
rval2D(1,i)         = d_sour

i = i + 1
descol(i)%titre = "Flux of cosmic rays"
descol(i)%form  = "1D"
descol(i)%unite = "1E-17 s-1"
descol(i)%IDFD    = "FD_FMRC"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Flux of cosmic rays </b>&
                     & as given in the input parameters<br/>&
                     &<i>Unit: 1E-17 s-1 per H</i><br/>&
                     &Affects: <br/> &
                     &- chemical rates<br/>&
                     &- heating rates in cloud center<br/>&
                     &- flux of secondary photons<br/>&
                     &<i>Typical value: a few 1E-17</i>&
                     &Fortran variable : fmrc]]>"
rval2D(1,i)         = fmrc

i = i + 1
descol(i)%titre      = "Gas pressure"
descol(i)%form       = "1D"
descol(i)%unite      = "cm-3 K"
descol(i)%IDFD       = "FD_PRESSE"
descol(i)%datatyp    = "double"
descol(i)%ucd        = "phys.pressure"
descol(i)%utype      = ""
descol(i)%flag_desc = 1
descol(i)%descrip    = "<![CDATA[<b>Gas pressure</b> &
                        &given as input parameters<br/>&
                        &<i>Unit: cm-3 K</i><br/>&
                        &Note: Has been used only if model &
                        &is isochore <br/>&
                        &Fortran variable : presse]]>"
rval2D(1,i)          = presse

i = i + 1
descol(i)%titre = "Total grain surface"
descol(i)%form  = "1D"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_SURF_GR_TOT"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Total grain surface</b><br/>&
                     &Fortran variable : s_gr_t]]>"
rval2D(1,i)         = s_gr_t

i = i + 1
descol(i)%titre = "Relative abundance of grains"
descol(i)%form  = "1D"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_GRAINRELABUND"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Relative abundance of grains</b><br/>&
                     &<i>xngr = n(gr) / nH</i><br/>&
                     &with : n(gr) number of grains per cubic cm<br/>, &
                     &and nH, the density of protons in cm-3<br/>&
                     &Fortran variable : xngr]]>"
rval2D(1,i)         = xngr

i = i + 1
descol(i)%titre = "ISRF scaling factor (Obs. side) - initial"
descol(i)%form  = "1D"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_RADM_INI"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Scaling factor to ISRF </b><br/>&
                     &on the observer's side of the cloud &
                     &in the input parameters.<br/> &
                     &This corresponds to the radiation field&
                     &seen from a position far from the cloud.]]> "
rval2D(1,i)         = radm_ini

i = i + 1
descol(i)%titre = "ISRF scaling factor (Back side) - initial"
descol(i)%form  = "1D"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_RADP_INI"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Scaling factor to ISRF </b><br/>&
                     &on the back side of the cloud &
                     &in the input parameters.<br/> &
                     &This corresponds to the radiation field &
                     &seen from a position far from the cloud.]]>"
rval2D(1,i)         = radp_ini

i = i + 1
descol(i)%titre = "wl_6eV"
descol(i)%form  = "1D"
descol(i)%unite = "Ang"
descol(i)%IDFD    = "FD_WL6EV"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Wavelength in Ang &
                     &corresponding to <b>6 eV</b>]]>"
rval2D(1,i)       = wl_6eV

i = i + 1
descol(i)%titre = "wl_hab"
descol(i)%form  = "1D"
descol(i)%unite = "Ang"
descol(i)%IDFD    = "FD_WLHAB"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Wavelength corresponding &
                     & to <b>Habing cutoff</b><br/>&
                     &<i>Unit: Ang</i>&
                     &<i>See: Habing, Bull. Astr. Inst. &
                     &Netherlands, 19, 421, (1968)</i>]]>"
rval2D(1,i)       = wl_hab

i = i + 1
descol(i)%titre   = "int_rad0"
descol(i)%form    = "1D"
descol(i)%unite   = "erg cm-3"
descol(i)%IDFD    = "FD_INTRAD0"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[???]]>"
rval2D(1,i)         = int_rad0

i = i + 1
descol(i)%titre = "flu_rad0"
descol(i)%form  = "1D"
descol(i)%unite = "phot cm-2 s-1"
descol(i)%IDFD    = "FD_FLURAD0"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[???]]>"
rval2D(1,i)         = flu_rad0

i = i + 1
descol(i)%titre   = "int_rad1"
descol(i)%form    = "1D"
descol(i)%unite   = "erg cm-3"
descol(i)%IDFD    = "FD_INTRAD1"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Density of energy of the &
                     &incident radiation field</b><br/> &
                     &at the edge of the observer side &
                     &of the cloud<br/>&
                     &This value is obtained by the integration &
                     &of the radiation field <br/>&
                     &from the Lyman limit to the Habing cutoff<br/>&
                     &<i>Unit: erg cm-3</i><br/>&
                     &<i>See: Habing, Bull. Astr. Inst. &
                     &Netherlands, 19, 421, (1968)</i>]]>"
rval2D(1,i)         = int_rad1

i = i + 1
descol(i)%titre = "flu_rad1"
descol(i)%form  = "1D"
descol(i)%unite = "phot cm-2 s-1"
descol(i)%IDFD    = "FD_FLURAD1"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Flux of the &
                     &incident radiation field</b><br/> &
                     &at the edge of the observer side &
                     &of the cloud<br/>&
                     &This value is obtained by the integration &
                     &of the radiation field <br/>&
                     &from the Lyman limit to the Habing cutoff<br/>&
                     &<i>Unit: photon cm-2 s-1</i><br/>&
                     &<i>See: Habing, Bull. Astr. Inst. &
                     &Netherlands, 19, 421, (1968)</i>]]>"
rval2D(1,i)         = flu_rad1

i = i + 1
descol(i)%titre   = "int_rad2"
descol(i)%form    = "1D"
descol(i)%unite   = "erg cm-3"
descol(i)%IDFD    = "FD_INTRAD2"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Density of energy of the &
                     &incident radiation field</b><br/> &
                     &at the edge of backside &
                     &of the cloud<br/>&
                     &This value is obtained by the integration &
                     &of the radiation field <br/>&
                     &from the Lyman limit to 6eV<br/>&
                     &<i>Unit: erg cm-3</i>]]>"
rval2D(1,i)         = int_rad2

i = i + 1
descol(i)%titre = "flu_rad2"
descol(i)%form  = "1D"
descol(i)%unite = "phot cm-2 s-1"
descol(i)%IDFD    = "FD_FLURAD2"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Flux of the &
                     &incident radiation field</b><br/> &
                     &at the edge of the backside &
                     &of the cloud<br/>&
                     &This value is obtained by the integration &
                     &of the radiation field <br/>&
                     &from the Lyman limit to the Habing cutoff<br/>&
                     &<i>Unit: photon cm-2 s-1</i><br/>&
                     &<i>See: Habing, Bull. Astr. Inst. &
                     &Netherlands, 19, 421, (1968)</i>]]>>"
rval2D(1,i)         = flu_rad2

i = i + 1
descol(i)%titre = "ISRF scaling factor (Obs. side) in Habing units"
descol(i)%form  = "1D"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_G0M"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Scaling factor to Habing &
                     &radiation field</b><br/> at the observer &
                     &side of the cloud.]]>"
rval2D(1,i)         = G0m

i = i + 1
descol(i)%titre = "ISRF scaling factor (back side) in Habing units"
descol(i)%form  = "1D"
descol(i)%unite = "phot cm-2 s-1"
descol(i)%IDFD    = "FD_G0p"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Scaling factor to Habing &
                     &radiation field</b><br/> at the backside &
                     &of the cloud.]]>"
rval2D(1,i)         = G0p

i = i + 1
descol(i)%titre = "T_CMB"
descol(i)%form  = "1D"
descol(i)%unite = "K"
descol(i)%IDFD    = "FD_TCMB"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Temperature of the CMB</b> &
                     &in Kelvin. </b>&
                     &Fortran variable : tcmb]]>"
rval2D(1,i)         = tcmb

!- elementary abundances
i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(1)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_H"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase H/H]]>"
rval2D(1,i)         = l_atom(1)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(2)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_C"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase C/H]]>"
rval2D(1,i)         = l_atom(2)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(3)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_N"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase N/H]]>"
rval2D(1,i)         = l_atom(3)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(4)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_O"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase O/H]]>"
rval2D(1,i)         = l_atom(4)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(5)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_He"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase He/H]]>"
rval2D(1,i)         = l_atom(5)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(6)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_D"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase D/H]]>"
rval2D(1,i)         = l_atom(6)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(7)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_C13"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase C13/H]]>"
rval2D(1,i)         = l_atom(7)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(8)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_N15"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase N15/H]]>"
rval2D(1,i)         = l_atom(8)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(9)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_O18"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase O18/H]]>"
rval2D(1,i)         = l_atom(9)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(10)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_PAH"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase PAH/H]]>"
rval2D(1,i)         = l_atom(10)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(11)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_F"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase F/H]]>"
rval2D(1,i)         = l_atom(11)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(12)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_Na"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase Na/H]]>"
rval2D(1,i)         = l_atom(12)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(13)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_Mg"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase Mg/H]]>"
rval2D(1,i)         = l_atom(13)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(14)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_Al"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase Al/H]]>"
rval2D(1,i)         = l_atom(14)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(15)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_Si"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase Si/H]]>"
rval2D(1,i)         = l_atom(15)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(16)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_P"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase P/H]]>"
rval2D(1,i)         = l_atom(16)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(17)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_S"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase S/H]]>"
rval2D(1,i)         = l_atom(17)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(18)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_Cl"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase Cl/H]]>"
rval2D(1,i)         = l_atom(18)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(19)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_Ca"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase Ca/H]]>"
rval2D(1,i)         = l_atom(19)%abnorm

i = i + 1
descol(i)%titre   = TRIM(ADJUSTL(l_atom(20)%name))//" / H"
descol(i)%form    = "1D"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_METAL_Fe"
descol(i)%datatyp = "double"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Elementary abundance in gas phase Fe/H]]>"
rval2D(1,i)         = l_atom(20)%abnorm

END SUBROUTINE CRE_DATA_PARAMOD_R

!!!===========================================================================================
!!! SUBROUTINE CRE_DATA_PARAMOD_I : Tableau des parametres du modele : INTEGER
!!!===========================================================================================
!!!  1 - F_ISRF
!!!  2 - iforh2
!!!  3 - ichh2
!!!  4 - istic
!!!  5 - isoneside
!!!  6 - ifeqth
!!!  7 - ifisob
!!!  8 - itrfer
!!!  9 - lfgkh2
!!! 10 - ifafm
!!! 11 - ifaf
!!! 12 - F_dustem
!============================================================================================
SUBROUTINE CRE_DATA_PARAMOD_I

IMPLICIT NONE
INTEGER :: i

! Number of parameters is defined in nparam_mod_i

descol(:)%flag_desc=0

i = 1
descol(i)%titre = "ISRF function"
descol(i)%form  = "1J"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_FISRF"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Flag to select a model &
                     &for the ISRF function<br/>&
                     &1 - Mathis et al. (corrected by Jacques Le Bourlot)<br/>&
                     &2 - Draine (1978) <br/>&
                     &Fortran variable : F_ISRF]]>"
ival2D(1,i) = F_ISRF

i = i + 1
descol(i)%titre   = "H2 excitation model after formation on grains"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_IFORH2"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Flag to determine model &
                     &for H2 excitation &
                     &after formation on dust. &
                     &1 - Folows a Boltzman distribution &
                     &with Eint = 1.478 eV<br/>&
                     &2 - Boltzman distribution with &
                     &Tex = 1/3 of formation energy<br/>&
                     &3 - H2 formed in v=14, J=0 and 1<br/>&
                     &4 - H2 formed in v=6 with T=65 K &
                     &for rotational levels<br/>&
                     &<i>See 6.1.2 in Le Petit et al., ApJ, &
                     &164, 506 (2006)</i>&
                     &Fortran variable : iforh2]]>"
ival2D(1,i) = iforh2

i = i + 1
descol(i)%titre = "Model for H + H2 collision rates"
descol(i)%form  = "1J"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_ICHH2"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Flag to determine <b>model for &
                    &H-H2 collisions</b><br/>&
                    &0 - Martin and Mandy prescription<br/>&
                    &1 - Flower et al. without reactive &
                    &collisions<br/>&
                    &2 - Flower et al. with reactive collisions &
                    &from Schofield<br/>&
                    &3 - Flower et al. with reactive collisions &
                    &from Schofield <br/>&
                    &    plus empiric laws for &
                    &non-referenced transitions&
                    &Fortran variable : ichh2]]>"
ival2D(1,i) = ichh2

i = i + 1
descol(i)%titre = "Model for atoms sticking on dust"
descol(i)%form  = "1J"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_ISTIC"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Flag to determine <b>model &
                    &for H sticking</b> on grains<br/>&
                    &1 - Sticking factor corresponds to gamma &
                    & in chemistry file for H + grain -> H<br/>&
                    &2 - Prescription by Sternberg et Dalgarno<br/>&
                    &3 - Prescription by Aderson et Vanier<br/>&
                    &4 - Our prescription: <br/>&
                    &    s = SQRT(10/Tgas) for T > 10 K<br/>&
                    &    s = 1             for T < 10 K&
                    &Fortran variable : istic]]>"
ival2D(1,i) = istic

i = i + 1
descol(i)%titre = "Flag : 1 or 2 side(s) model"
descol(i)%form  = "1J"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_ISONESIDE"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Flag: <br/>&
                    &0 - two-side model <br/>&
                    &1 - semi-infinite cloud<br/>&
                    &Determined by the value of radp &
                    &in input parameters&
                    &Fortran variable : isoneside]]>"
ival2D(1,i) = isoneside

i = i + 1
descol(i)%titre = "Flag : Thermal balance"
descol(i)%form  = "1J"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_IFEQTH"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Flag: <b>thermal balance</b><br/>&
                    &1 - thermal balance solved <br/>&
                    &0 - isothermal model<br/>&
                    &Given in input parameters&
                    &Fortran variable : ifeqth]]>"
ival2D(1,i) = ifeqth

i = i + 1
descol(i)%titre   = "Flag: State equation"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_IFISOB"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip  = "<![CDATA[Flag: <b>state equation</b><br/>&
                     &0 - Constant density model<br/>&
                     &1 - Isobaric model - Pressure (nH*T) is &
                     &constant<br/>&
                     &2 - User specific density profile<br/>&
                     &Negative value: Force the density profile &
                     &to be symetrical&
                     &Fortran variable : ifisob]]>"
ival2D(1,i) = ifisob

i = i + 1
descol(i)%titre = "Flag: Algorithm for line self-shielding"
descol(i)%form  = "1J"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_ITFER"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Flag: <b>UV radiative transfer method</b><br/>&
                    & 0 - FGK approximation for H2 self-shielding<br/> &
                    & 2 - Exact computation of H2 self-shielding for &
                    &lines with J below lfgkh2<br/>&
                    &     FGK approximation for lines from J=>lfgkh2&
                    &<i>Note: It is pointless to have 1 here. &
                    &It gives an accuracy between option 0 and 2</i><br/>&
                    &Fortran variable : itrfer]]>"
ival2D(1,i) = itrfer

i = i + 1
descol(i)%titre   = "J limit for H2 exact self-shielding"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_JFGKH2"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[J below wich <b>exact UV radiative &
                    &transfer</b> is done <br/>&
                    &in H2 lines if itrfer > 0&
                    &Fortran variable : lfgkh2]]>"
ival2D(1,i) = lfgkh2

i = i + 1
descol(i)%titre = "Number of global iterations"
descol(i)%form  = "1J"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_IFAFM"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Number of iterations</b> &
                    &requested in input parameters <br/> &
                    &Fortran variable : ifafm]]>"
ival2D(1,i) = ifafm

i = i + 1
descol(i)%titre = "Index of present global iteration"
descol(i)%form  = "1J"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_IFAF"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Index of iteration of the actual result<br/>&
                     &Fortran variable : ifaf]]>"
ival2D(1,i) = ifaf

i = i + 1
descol(i)%titre   = "Flag : Use of DUSTEM"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_FDUSTEM"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Flag : <b>DUSTEM</b><br/>&
                    & 1 - DUSTEM used <br/> &
                    & 0 - DUSTEM not used]]>"
ival2D(1,i) = F_dustem

END SUBROUTINE CRE_DATA_PARAMOD_I

!!!===========================================================================================
!!! SUBROUTINE CRE_DATA_PARAMOD_S : Tableau des parametres du modele : INTEGER
!!!===========================================================================================
!!! 1 - Model name
!!! 2 - Chemistry file name
!!! 3 - nH/T profile file name
!!!   - ISRF name
!!! 4 - External source name
!!! 5 - Line of sight name
!!! 6 - Code version
!=============================================================================================
SUBROUTINE CRE_DATA_PARAMOD_S

IMPLICIT NONE
INTEGER :: i

descol(:)%flag_desc=0

i = 1
descol(i)%titre   = "Model name"
descol(i)%form    = "50A"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_MODNAME"
descol(i)%datatyp = "string"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Name of the model]]>"
sval2D(1,i) = modele

i = i + 1
descol(i)%titre   = "Chemistry file name"
descol(i)%form    = "50A"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_CHEMNAME"
descol(i)%datatyp = "string"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Chemistry file name]]>"
sval2D(1,i) = chimie

i = i + 1
descol(i)%titre = "Density-Temperature profile file name"
descol(i)%form  = "50A"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_PROFNAME"
descol(i)%datatyp = "string"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Density profile file name]]>"
sval2D(1,i) = fprofil

i = i + 1
descol(i)%titre   = "ISRF model"
descol(i)%form    = "50A"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_ISRF"
descol(i)%datatyp = "string"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>ISRF model</b> used in the model<br/>]]>"
sval2D(1,i) = ISRF_name

i = i + 1
descol(i)%titre = "External source name"
descol(i)%form  = "50A"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_SRCNAME"
descol(i)%datatyp = "string"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>External source name</b><br/>]]>"
sval2D(1,i) = srcpp

i = i + 1
descol(i)%titre = "Line of sight dust extinction curve"
descol(i)%form  = "50A"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_LOSNAME"
descol(i)%datatyp = "string"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[<b>Line of sight</b><br/>&
                    &Determines the dust extinction curve]]>"
sval2D(1,i) = los_ext

i = i + 1
descol(i)%titre = "Code version"
descol(i)%form  = "50A"
descol(i)%unite = "no unit"
descol(i)%IDFD    = "FD_VERSIONCODE"
descol(i)%datatyp = "string"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Version of the Meudon PDR code]]>"
sval2D(1,i) = version_code

END SUBROUTINE CRE_DATA_PARAMOD_S

!!!===========================================================================================
!!! SUBROUTINE CRE_DATA_SPECIES
!!!
!!! ATTENTION : In PDR_1.4 the array speci contains species and moments :
!!!             0               - NOTHING
!!!             1:nspec         - Species in gas phase from H to ELECTR
!!!             nspec+1:n_var   - Moments
!!!             n_var+1:n_var+5 - PHOTON, CRP, PHOSEC, GRAIN, 2H
!!! If we want to get a Chemistry Analyser nearly similar the best thing to do, would be to
!!! build another array with speci from 0 to n_var+5
!!! NOT DONE YET
!!!===========================================================================================
SUBROUTINE CRE_DATA_SPECIES(nlgn)

    IMPLICIT NONE

    INTEGER, INTENT(IN)         :: nlgn
    INTEGER                     :: i
    CHARACTER(LEN=lennamespecy) :: espece
    CHARACTER(LEN=lennamespecy) :: especeID

    INTEGER                        :: numH2  ! numero de taille de grains

    !PRINT *,"Meta-donnees pour Species"
    ! Name of the chemical species
    descol(1)%titre   = "Species"
    descol(1)%form    = "8A"
    descol(1)%unite   = "no unit"
    descol(1)%IDFD    = "FD_SPECIES"
    descol(1)%datatyp = "string"
    descol(1)%ucd     = ""
    descol(1)%utype   = ""
    descol(1)%flag_desc = 1
    descol(1)%descrip = "<![CDATA[<b>Name of species</b>]]>"

    ! ID the chemical species
    descol(2)%titre   = "ID"
    descol(2)%form    = "8A"
    descol(2)%unite   = "no unit"
    descol(2)%IDFD    = "FD_IDSPECIES"
    descol(2)%datatyp = "string"
    descol(2)%ucd     = ""
    descol(2)%utype   = ""
    descol(2)%flag_desc = 1
    descol(2)%descrip = "<![CDATA[<b>ID of species</b>]]>"

    ! molar mass
    descol(3)%titre   = "Molar mass"
    descol(3)%form    = "1D"
    descol(3)%unite   = "atomic mass"
    descol(3)%IDFD    = "FD_MOLARMASS"
    descol(3)%datatyp = "double"
    descol(3)%ucd     = ""
    descol(3)%utype   = ""
    descol(3)%flag_desc = 1
    descol(3)%descrip = "<![CDATA[<b>Molar mass</b><br/>&
                        &<i>unit: atomic mass</i><br/>&
                        &<i>1 amu = 1.66053873e-24 g</i>]]>"

    ! formation enthalpy
    descol(4)%titre   = "Formation enthalpy"
    descol(4)%form    = "1D"
    descol(4)%unite   = "kcal mol-1"
    descol(4)%IDFD    = "FD_FORMENTHALPY"
    descol(4)%datatyp = "double"
    descol(4)%ucd     = ""
    descol(4)%utype   = ""
    descol(4)%flag_desc = 1
    descol(4)%descrip = "<![CDATA[<b>Formation enthalpy</b><br/>&
                        &<i>unit: kcal/mol</i><br/>&
                        &<i>1 eV = 23.06036 kcal/mol</i>]]>"

    ! Initial abundance
    descol(5)%titre   = "Initial abundance"
    descol(5)%form    = "1D"
    descol(5)%unite   = "cm-3"
    descol(5)%IDFD    = "FD_ABINI"
    descol(5)%datatyp = "double"
    descol(5)%ucd     = ""
    descol(5)%utype   = ""
    descol(5)%flag_desc = 1
    descol(5)%descrip = "<![CDATA[<b>Initial abundance</b> in &
                        &chemistry file<br/>&
                        &<i>unit: cm-3</i><br/>&
                        &<i>Note: can affect outputs because &
                        &of bistability</i>]]>"

    ! Diffusion temperature
    descol(6)%titre   = "Diffusion temperature"
    descol(6)%form    = "1D"
    descol(6)%unite   = "K"
    descol(6)%IDFD    = "FD_TDIFF"
    descol(6)%datatyp = "double"
    descol(6)%ucd     = ""
    descol(6)%utype   = ""
    descol(6)%flag_desc = 1
    descol(6)%descrip = "<![CDATA[<b>Diffusion temperature</b> on &
                        &grains<br/>&
                        &<i>unit: K</i>]]>"

    ! Evaporation temperature
    descol(7)%titre   = "Evaporation temperature"
    descol(7)%form    = "1D"
    descol(7)%unite   = "K"
    descol(7)%IDFD    = "FD_TEVAP"
    descol(7)%datatyp = "double"
    descol(7)%ucd     = ""
    descol(7)%utype   = ""
    descol(7)%flag_desc = 1
    descol(7)%descrip = "<![CDATA[<b>Evaporation temperature</b> from&
                        &grains<br/>&
                        &<i>unit: K</i>]]>"

    !--- PREMIERE ESPECE ---------------------------
    ! speci va de 0 a nspec+5, mol de 0 a nspec+5
    ! on cree un pseudo mol(0) = 0.0
    sval2D(1,1) = species(0)
    sval2D(1,2) = ""
    rval2D(1,1) = 0.0_dp ! = massmol(0)
    rval2D(1,2) = 0.0_dp ! = enthalp(0)
    rval2D(1,3) = 0.0_dp ! = abinitial(0)
    rval2D(1,4) = 0.0_dp ! = temp_diff(0)
    rval2D(1,5) = 0.0_dp ! = temp_evap(0)
    !--- AUTRES ESPECES ---------------------------
    numH2 = 0
    DO i = 1, nlgn-1
       espece = species(i)
       CALL SPEMAJ(espece,especeID)
       sval2D(i+1,1) = espece
       sval2D(i+1,2) = especeID
       rval2D(i+1,1) = massmol(i)
       rval2D(i+1,2) = enthalp(i)
       rval2D(i+1,3) = abin(i)
       rval2D(i+1,4) = temp_diff(i)
       rval2D(i+1,5) = temp_evap(i)
    ENDDO

END SUBROUTINE CRE_DATA_SPECIES

!!!===========================================================================================
!!! SUBROUTINE CRE_DATA_SPECI_NUM
!!!
!!! ATTENTION : In PDR_1.4 the array speci contains species and moments :
!!!             0               - NOTHING
!!!             1:nspec         - Species in gas phase from H to ELECTR
!!!             nspec+1:n_var   - Moments
!!!             n_var+1:n_var+5 - PHOTON, CRP, PHOSEC, GRAIN, 2H
!!! Here we store all data from 0 to n_var+5. They will be used in chemistry analyser
!!!===========================================================================================
SUBROUTINE CRE_DATA_SPECI_NUM(nlgn)

    IMPLICIT NONE

    INTEGER, INTENT(IN)         :: nlgn
    INTEGER                     :: i
    CHARACTER(LEN=lennamespecy) :: espece
    CHARACTER(LEN=lennamespecy) :: especeID

    INTEGER                        :: numH2  ! numero de taille de grains

    ! Name of the chemical species
    i = 1
    descol(i)%titre   = "Species"
    descol(i)%form    = "8A"
    descol(i)%unite   = "no unit"
    descol(i)%IDFD    = "FD_SPECI_NUM"
    descol(i)%datatyp = "string"
    descol(i)%ucd     = ""
    descol(i)%utype   = ""
    descol(i)%flag_desc = 1
    descol(i)%descrip = "<![CDATA[<b>Name of species</b>]]>"

    ! ID the chemical species
    i = i + 1
    descol(i)%titre   = "ID"
    descol(i)%form    = "8A"
    descol(i)%unite   = "no unit"
    descol(i)%IDFD    = "FD_IDSPECI_NUM"
    descol(i)%datatyp = "string"
    descol(i)%ucd     = ""
    descol(i)%utype   = ""
    descol(i)%flag_desc = 1
    descol(i)%descrip = "<![CDATA[<b>ID of species</b>]]>"

    ! molar mass
    i = i + 1
    descol(i)%titre   = "Molar mass"
    descol(i)%form    = "1D"
    descol(i)%unite   = "atomic mass"
    descol(i)%IDFD    = "FD_MOLARMASS_NUM"
    descol(i)%datatyp = "double"
    descol(i)%ucd     = ""
    descol(i)%utype   = ""
    descol(i)%flag_desc = 1
    descol(i)%descrip = "<![CDATA[<b>Molar mass</b><br/>&
                        &<i>unit: atomic mass</i><br/>&
                        &<i>1 amu = 1.66053873e-24 g</i>]]>"

    ! formation enthalpy
    i = i + 1
    descol(i)%titre   = "Formation enthalpy"
    descol(i)%form    = "1D"
    descol(i)%unite   = "kcal mol-1"
    descol(i)%IDFD    = "FD_FORMENTHALPY_NUM"
    descol(i)%datatyp = "double"
    descol(i)%ucd     = ""
    descol(i)%utype   = ""
    descol(i)%flag_desc = 1
    descol(i)%descrip = "<![CDATA[<b>Formation enthalpy</b><br/>&
                        &<i>unit: kcal/mol</i><br/>&
                        &<i>1 eV = 23.06036 kcal/mol</i>]]>"

    ! Diffusion temperature
    i = i + 1
    descol(i)%titre   = "Diffusion temperature"
    descol(i)%form    = "1D"
    descol(i)%unite   = "K"
    descol(i)%IDFD    = "FD_TDIFF_NUM"
    descol(i)%datatyp = "double"
    descol(i)%ucd     = ""
    descol(i)%utype   = ""
    descol(i)%flag_desc = 1
    descol(i)%descrip = "<![CDATA[<b>Diffusion temperature</b> on &
                        &grains<br/>&
                        &<i>unit: K</i>]]>"

    ! Evaporation temperature
    i = i + 1
    descol(i)%titre   = "Evaporation temperature"
    descol(i)%form    = "1D"
    descol(i)%unite   = "K"
    descol(i)%IDFD    = "FD_TEVAP_NUM"
    descol(i)%datatyp = "double"
    descol(i)%ucd     = ""
    descol(i)%utype   = ""
    descol(i)%flag_desc = 1
    descol(i)%descrip = "<![CDATA[<b>Evaporation temperature</b> from&
                        &grains<br/>&
                        &<i>unit: K</i>]]>"

    !--- PREMIERE ESPECE ---------------------------
    ! speci va de 0 a nspec+5, mol de 0 a nspec+5
    ! on cree un pseudo mol(0) = 0.0
    sval2D(1,1) = speci(0)
    sval2D(1,2) = ""
    rval2D(1,1) = 0.0_dp ! = massmol(0)
    rval2D(1,2) = 0.0_dp ! = enthalp(0)
    rval2D(1,3) = 0.0_dp ! = abinitial(0)
    rval2D(1,4) = 0.0_dp ! = temp_diff(0)
    rval2D(1,5) = 0.0_dp ! = temp_evap(0)
    !--- AUTRES ESPECES ---------------------------
    numH2 = 0
    DO i = 1, nlgn-1
       espece = speci(i)
       CALL SPEMAJ(espece,especeID)
       sval2D(i+1,1) = espece
       sval2D(i+1,2) = especeID

       rval2D(i+1,1) = mol(i)
       rval2D(i+1,2) = enth(i)
       rval2D(i+1,3) = t_diff(i)
       rval2D(i+1,4) = t_evap(i)
    ENDDO

END SUBROUTINE CRE_DATA_SPECI_NUM

!!!==========================================================================
!!! SUBROUTINE CRE_DATA_NAMEMOMENTS
!!!==========================================================================
SUBROUTINE CRE_DATA_NAMEMOMENTS(nlgn)
USE PXDR_AUXILIAR
IMPLICIT NONE

INTEGER, INTENT(IN) :: nlgn
INTEGER             :: i

    ! Name of the moment
    descol(1)%titre   = "Moments"
    descol(1)%form    = "10A"
    descol(1)%unite   = "no unit"
    descol(1)%IDFD    = "FD_NAMEMOMENT"
    descol(1)%datatyp = "string"
    descol(1)%ucd     = ""
    descol(1)%utype   = ""
    descol(1)%flag_desc = 1
    descol(1)%descrip = "<![CDATA[<b>Name of moments</b>]]>"

    ! ID of the moment
    descol(2)%titre   = "ID moments"
    descol(2)%form    = "10A"
    descol(2)%unite   = "no unit"
    descol(2)%IDFD    = "FD_IDMOMENT"
    descol(2)%datatyp = "string"
    descol(2)%ucd     = ""
    descol(2)%utype   = ""
    descol(2)%flag_desc = 1
    descol(2)%descrip = "<![CDATA[<b>ID of moments</b>]]>"

    DO i = 1, nlgn
       CALL MAKE_NAMEMOMENTS(moments(i),namemom,IDmom)
       sval2D(i,1) = TRIM(ADJUSTL(namemom))
       sval2D(i,2) = TRIM(ADJUSTL(IDmom))
    ENDDO

END SUBROUTINE CRE_DATA_NAMEMOMENTS

!!!==========================================================================
!!! SUBROUTINE CRE_DATA_ELEMENTCOMP
!!!==========================================================================
SUBROUTINE CRE_DATA_ELEMENTCOMP
IMPLICIT NONE

INTEGER :: i, j

descol(:)%form    = "1J"
descol(:)%unite   = "no unit"
descol(:)%datatyp = "integer"
descol(:)%ucd     = ""
descol(:)%utype   = ""
descol(:)%flag_desc = 0

descol(1)%titre   = "H atom"
descol(1)%IDFD    = "FD_EL_H"

descol(2)%titre   = "C atom"
descol(2)%IDFD    = "FD_EL_C"

descol(3)%titre   = "N atom"
descol(3)%IDFD    = "FD_EL_N"

descol(4)%titre   = "O atom"
descol(4)%IDFD    = "FD_EL_O"

descol(5)%titre   = "He atom"
descol(5)%IDFD    = "FD_EL_He"

descol(6)%titre   = "D atom"
descol(6)%IDFD    = "FD_EL_D"

descol(7)%titre   = "C13 atom"
descol(7)%IDFD    = "FD_EL_C13"

descol(8)%titre   = "N15 atom"
descol(8)%IDFD    = "FD_EL_N15"

descol(9)%titre   = "O18 atom"
descol(9)%IDFD    = "FD_EL_O18"

descol(10)%titre   = "Li atom"
descol(10)%IDFD    = "FD_EL_Li"

descol(11)%titre   = "F atom"
descol(11)%IDFD    = "FD_EL_F"

descol(12)%titre   = "Na atom"
descol(12)%IDFD    = "FD_EL_Na"

descol(13)%titre   = "Mg atom"
descol(13)%IDFD    = "FD_EL_Mg"

descol(14)%titre   = "Al atom"
descol(14)%IDFD    = "FD_EL_Al"

descol(15)%titre   = "Si atom"
descol(15)%IDFD    = "FD_EL_Si"

descol(16)%titre   = "P atom"
descol(16)%IDFD    = "FD_EL_P"

descol(17)%titre   = "S atom"
descol(17)%IDFD    = "FD_EL_S"

descol(18)%titre   = "Cl atom"
descol(18)%IDFD    = "FD_EL_Cl"

descol(19)%titre   = "K atom"
descol(19)%IDFD    = "FD_EL_K"

descol(20)%titre   = "Fe atom"
descol(20)%IDFD    = "FD_EL_Fe"

descol(21)%titre   = "Charge"
descol(21)%IDFD    = "FD_EL_CHARGE"

DO i = 0, nspec+5
   DO j = 1, 21
      ival2D(i+1,j) = ael(i,j)
   ENDDO
ENDDO

END SUBROUTINE CRE_DATA_ELEMENTCOMP

!!!==========================================================================
!!! SUBROUTINE CRE_DATA_SPESPE
!!! Store indexes of special species
!!!==========================================================================
SUBROUTINE CRE_DATA_SPESPE

IMPLICIT NONE
INTEGER :: i

descol(:)%form    = "1J"
descol(:)%unite   = "no unit"
descol(:)%datatyp = "integer"
descol(:)%ucd     = ""
descol(:)%utype   = ""
descol(:)%flag_desc = 0

i = 1
descol(i)%titre   = "H_Index"
descol(i)%IDFD    = "FD_NUM_H"
descol(i)%datatyp = "integer"
ival2D(1,i) = i_h

i = i + 1
descol(i)%titre   = "D_Index"
descol(i)%IDFD    = "FD_NUM_D"
ival2D(1,i) = i_d

i = i + 1
descol(i)%titre   = "H:_Index"
descol(i)%IDFD    = "FD_NUM_HA"
descol(i)%datatyp = "integer"
ival2D(1,i) = i_hgr

i = i + 1
descol(i)%titre   = "D:_Index"
descol(i)%IDFD    = "FD_NUM_DA"
ival2D(1,i) = i_dgr

i = i + 1
descol(i)%titre   = "H::_Index"
descol(i)%IDFD    = "FD_NUM_HAC"
descol(i)%datatyp = "integer"
ival2D(1,i) = i_hgrc

i = i + 1
descol(i)%titre   = "H2_Index"
descol(i)%IDFD    = "FD_NUM_H2"
ival2D(1,i) = i_h2

i = i + 1
descol(i)%titre   = "HD_Index"
descol(i)%IDFD    = "FD_NUM_HD"
ival2D(1,i) = i_hd

i = i + 1
descol(i)%titre   = "H2:_Index"
descol(i)%IDFD    = "FD_NUM_H2A"
ival2D(1,i) = i_h2gr

i = i + 1
descol(i)%titre   = "HD:_Index"
descol(i)%IDFD    = "FD_NUM_HDA"
ival2D(1,i) = i_hdgr

i = i + 1
descol(i)%titre   = "He_Index"
descol(i)%IDFD    = "FD_NUM_He"
ival2D(1,i) = i_he

i = i + 1
descol(i)%titre   = "C_Index"
descol(i)%IDFD    = "FD_NUM_C"
ival2D(1,i) = i_c

i = i + 1
descol(i)%titre   = "N_Index"
descol(i)%IDFD    = "FD_NUM_N"
ival2D(1,i) = i_n

i = i + 1
descol(i)%titre   = "O_Index"
descol(i)%IDFD    = "FD_NUM_O"
ival2D(1,i) = i_o

i = i + 1
descol(i)%titre   = "S_Index"
descol(i)%IDFD    = "FD_NUM_S"
ival2D(1,i) = i_s

i = i + 1
descol(i)%titre   = "Si_Index"
descol(i)%IDFD    = "FD_NUM_Si"
ival2D(1,i) = i_si

i = i + 1
descol(i)%titre   = "C+_Index"
descol(i)%IDFD    = "FD_NUM_Cp"
ival2D(1,i) = i_cp

i = i + 1
descol(i)%titre   = "N+_Index"
descol(i)%IDFD    = "FD_NUM_Np"
ival2D(1,i) = i_np

i = i + 1
descol(i)%titre   = "O+_Index"
descol(i)%IDFD    = "FD_NUM_Op"
ival2D(1,i) = i_op

i = i + 1
descol(i)%titre   = "S+_Index"
descol(i)%IDFD    = "FD_NUM_Sp"
ival2D(1,i) = i_sp

i = i + 1
descol(i)%titre   = "Si+_Index"
descol(i)%IDFD    = "FD_NUM_Sip"
ival2D(1,i) = i_sip

i = i + 1
descol(i)%titre   = "H2O_Index"
descol(i)%IDFD    = "FD_NUM_H2O"
ival2D(1,i) = i_h2o

i = i + 1
descol(i)%titre   = "H2O:_Index"
descol(i)%IDFD    = "FD_NUM_H2OA"
ival2D(1,i) = i_h2ogr

i = i + 1
descol(i)%titre   = "OH_Index"
descol(i)%IDFD    = "FD_NUM_OH"
ival2D(1,i) = i_oh

i = i + 1
descol(i)%titre   = "CO_Index"
descol(i)%IDFD    = "FD_NUM_CO"
ival2D(1,i) = i_co

i = i + 1
descol(i)%titre   = "C*O_Index"
descol(i)%IDFD    = "FD_NUM_C13O"
ival2D(1,i) = i_c13o

i = i + 1
descol(i)%titre   = "CO*_Index"
descol(i)%IDFD    = "FD_NUM_CO18"
ival2D(1,i) = i_co18

i = i + 1
descol(i)%titre   = "C*O*_Index"
descol(i)%IDFD    = "FD_NUM_C13O18"
ival2D(1,i) = i_c13o18

i = i + 1
descol(i)%titre   = "CO:_Index"
descol(i)%IDFD    = "FD_NUM_COA"
ival2D(1,i) = i_cogr

i = i + 1
descol(i)%titre   = "H+_Index"
descol(i)%IDFD    = "FD_NUM_Hp"
ival2D(1,i) = i_hp

i = i + 1
descol(i)%titre   = "H2D+_Index"
descol(i)%IDFD    = "FD_NUM_H2Dp"
ival2D(1,i) = i_h2dp

i = i + 1
descol(i)%titre   = "H3+_Index"
descol(i)%IDFD    = "FD_NUM_H3p"
ival2D(1,i) = i_h3p

i = i + 1
descol(i)%titre   = "Fe+_Index"
descol(i)%IDFD    = "FD_NUM_Fep"
ival2D(1,i) = i_fep

i = i + 1
descol(i)%titre   = "C2_Index"
descol(i)%IDFD    = "FD_NUM_C2"
ival2D(1,i) = i_c2

i = i + 1
descol(i)%titre   = "CS_Index"
descol(i)%IDFD    = "FD_NUM_CS"
ival2D(1,i) = i_cs

i = i + 1
descol(i)%titre   = "C#13_Index"
descol(i)%IDFD    = "FD_NUM_C_13"
ival2D(1,i) = i_c13

i = i + 1
descol(i)%titre   = "O#18_Index"
descol(i)%IDFD    = "FD_NUM_O_18"
ival2D(1,i) = i_o18

i = i + 1
descol(i)%titre   = "HCO+_Index"
descol(i)%IDFD    = "FD_NUM_HCOp"
ival2D(1,i) = i_hcop

i = i + 1
descol(i)%titre   = "CH+_Index"
descol(i)%IDFD    = "FD_NUM_CHp"
ival2D(1,i) = i_chp

i = i + 1
descol(i)%titre   = "HCN_Index"
descol(i)%IDFD    = "FD_NUM_HCN"
ival2D(1,i) = i_hcn

i = i + 1
descol(i)%titre   = "O2_Index"
descol(i)%IDFD    = "FD_NUM_O2"
ival2D(1,i) = i_o2

END SUBROUTINE CRE_DATA_SPESPE

!!!==========================================================================
!!! SUBROUTINE CRE_DATA_PROPSPE
!!! Store indexes of special species
!!!==========================================================================
SUBROUTINE CRE_DATA_PROPSPE

IMPLICIT NONE
INTEGER :: i

descol(:)%form    = "1J"
descol(:)%unite   = "no unit"
descol(:)%datatyp = "integer"
descol(:)%ucd     = ""
descol(:)%utype   = ""
descol(:)%flag_desc = 0

i = 1
descol(i)%titre   = "Num_neutral"
descol(i)%IDFD    = "FD_NUMPROPSPE_NEUTRAL"
ival2D(1,i) = nsp_ne

i = i + 1
descol(i)%titre   = "Num_cations"
descol(i)%IDFD    = "FD_NUMPROPSPE_CATION"
ival2D(1,i) = nsp_ip

i = i + 1
descol(i)%titre   = "Num_anions"
descol(i)%IDFD    = "FD_NUMPROPSPE_ANION"
ival2D(1,i) = nsp_im

i = i + 1
descol(i)%titre   = "Num_chemsorb"
descol(i)%IDFD    = "FD_NUMPROPSPE_CHEMSORB"
ival2D(1,i) = nsp_cs

i = i + 1
descol(i)%titre   = "Num_multi-layer"
descol(i)%IDFD    = "FD_NUMPROPSPE_ADSMULTI"
ival2D(1,i) = nsp_gm

i = i + 1
descol(i)%titre   = "Num_mono-layer"
descol(i)%IDFD    = "FD_NUMPROPSPE_ADSMONO"
ival2D(1,i) = nsp_g1

END SUBROUTINE CRE_DATA_PROPSPE

!!!==============================================================================
!!! SUBROUTINE CRE_DATA_STRUC
!!! Store data to characterise the structure of the cloud
!!! Tgas, nH, ne/nH, Pressure, n(H) + n(H2) + n(He)
!!!==============================================================================
SUBROUTINE CRE_DATA_STRUC

IMPLICIT NONE

!--- Colonne 1 : Tgas ----------
descol(1)%titre   = "Temperature"
descol(1)%unite   = "K"
descol(1)%form    = "1D"
descol(1)%IDFD    = "FD_GASTEMPERATURE"
descol(1)%datatyp = "double"
descol(1)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/TemperatureOfGas"
descol(1)%utype   = ""
descol(1)%flag_desc = 1
descol(1)%descrip = "<![CDATA[Gas temperature<br/>&
                    &<i>Unit: K</i>]]>"
rval2D(1:npo+1,1)       = tgaz(0:npo)

!--- Colonne 2: proton density------
descol(2)%titre   = "Proton density"
descol(2)%unite   = "cm-3"
descol(2)%form    = "1D"
descol(2)%IDFD    = "FD_DENSITY"
descol(2)%datatyp = "double"
descol(2)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/Proton_Density"
descol(2)%utype   = ""
descol(2)%flag_desc = 1
descol(2)%descrip = "<![CDATA[Proton density<br/>&
                    &nH = n(H) + 2n(H2) + n(H+)<br/>&
                    &<i>Unit: cm-3]]>"
rval2D(1:npo+1,2)       = densh(0:npo)

!--- Colonne 3: Ionization degree ------
descol(3)%titre   = "Ionization degree"
descol(3)%IDFD    = "FD_IONIDEGREE"
descol(3)%form    = "1D"
descol(3)%unite   = ""
descol(3)%datatyp = "double"
descol(3)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/IonizationDegree"
descol(3)%utype   = ""
descol(3)%flag_desc = 1
descol(3)%descrip = "<![CDATA[Ionization degree, n(e-) / nH ]]>"
rval2D(1:npo+1,3) = ionidegree(0:npo)

!--- Colonne 4: Gas pressure ------
descol(4)%titre   = "Gas pressure"
descol(4)%IDFD    = "FD_GASPRESSURE"
descol(4)%form    = "1D"
descol(4)%unite   = "K cm-3"
descol(4)%datatyp = "double"
descol(4)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/ThermalPressure"
descol(4)%utype   = ""
descol(4)%flag_desc = 1
descol(4)%descrip = "<![CDATA[Gas pressure <br/>&
                  &P = [n(H) + n(H2) + n(He)] * T ]]>"
rval2D(1:npo+1,4) = gaspressure(0:npo)

!--- Colonne 5: Total Density ------
descol(5)%titre   = "Total density"
descol(5)%IDFD    = "FD_TOTDENSITY"
descol(5)%form    = "1D"
descol(5)%unite   = "cm-3"
descol(5)%datatyp = "double"
descol(5)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/Density"
descol(5)%utype   = ""
descol(5)%flag_desc = 1
descol(5)%descrip = "<![CDATA[Total density <br/>&
                  &n = n(H) + n(H2) + n(He) ]]>"
rval2D(1:npo+1,5) = totdensity(0:npo)

END SUBROUTINE CRE_DATA_STRUC

!!!==============================================================================
!!! SUBROUTINE CRE_DATA_HEAT
!!! Store data concerning heating rates
!==============================================================================
SUBROUTINE CRE_DATA_HEAT
IMPLICIT NONE

descol(:)%form = "1D" ! format double precision

!--- Colonne 1 : chotot ----------
descol(1)%titre   = "Total heating"
descol(1)%unite   = "erg cm-3 s-1"
descol(1)%form    = "1D"
descol(1)%IDFD    = "FD_TOTALHEAT"
descol(1)%datatyp = "double"
descol(1)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/HeatingRate"
descol(1)%utype   = ""
descol(1)%flag_desc = 1
descol(1)%descrip = "<![CDATA[<b>Total heating rate</b>]]>"
rval2D(1:npo+1,1)       = chotot(0:npo)

!--- Colonne 2 : Photoelectriq ---
descol(2)%titre   = "Photoelectric heating"
descol(2)%unite   = "erg cm-3 s-1"
descol(2)%form    = "1D"
descol(2)%IDFD    = "FD_PHOTELECHEAT"
descol(2)%datatyp = "double"
descol(2)%ucd     = ""
descol(2)%utype   = ""
descol(2)%flag_desc = 1
descol(2)%descrip = "<![CDATA[<b>Heating rate by photoelectric effect on dust</b>]]>"
rval2D(1:npo+1,2)       = chophg(0:npo)

!--- Colonne 3 : choh2g ----------
descol(3)%titre   = "H2 on grains heating"
descol(3)%unite   = "erg cm-3 s-1"
descol(3)%form    = "1D"
descol(3)%IDFD    = "FD_H2GRAINHEAT"
descol(3)%datatyp = "double"
descol(3)%ucd     = ""
descol(3)%utype   = ""
descol(3)%flag_desc = 1
descol(3)%descrip = "<![CDATA[<b>Heating rate by H2 formation on dust</b>]]>"
rval2D(1:npo+1,3)       = choh2g(0:npo)

!--- Colonne 4 : chohdg ---------
descol(4)%titre   = "HD on grains heating"
descol(4)%unite   = "erg cm-3 s-1"
descol(4)%form    = "1D"
descol(4)%IDFD    = "FD_HDGRAINHEAT"
descol(4)%datatyp = "double"
descol(4)%ucd     = ""
descol(4)%utype   = ""
descol(4)%flag_desc = 1
descol(4)%descrip = "<![CDATA[<b>Heating rate by HD formation on dust</b>]]>"
rval2D(1:npo+1,4)       = chohdg(0:npo)

!--- Colonne 5 : chopht ----------
descol(5)%titre   = "Heating by photoprocesses"
descol(5)%unite   = "erg cm-3 s-1"
descol(5)%form    = "1D"
descol(5)%IDFD    = "FD_PHOTHEAT"
descol(5)%datatyp = "double"
descol(5)%ucd     = ""
descol(5)%utype   = ""
descol(5)%flag_desc = 0
descol(5)%descrip = "<![CDATA[<b>Heating rate by photo-processes</b>]]>"
rval2D(1:npo+1,5) = chopht(0:npo)

!--- Colonne 6 : chorc ----------
descol(6)%titre   = "Cosmic rays heating"
descol(6)%unite   = "erg cm-3 s-1"
descol(6)%form    = "1D"
descol(6)%IDFD    = "FD_COSMRAYHEAT"
descol(6)%datatyp = "double"
descol(6)%ucd     = ""
descol(6)%utype   = ""
descol(6)%flag_desc = 1
descol(6)%descrip = "<![CDATA[<b>Heating rate by cosmic rays ionizations</b>]]>"
rval2D(1:npo+1,6) = chorc(0:npo)

!--- Colonne 7 : chophs ---------
descol(7)%titre   = "Secondary photons heating"
descol(7)%unite   = "erg cm-3 s-1"
descol(7)%form    = "1D"
descol(7)%IDFD    = "FD_SECPHOTHEAT"
descol(7)%datatyp = "double"
descol(7)%ucd     = ""
descol(7)%utype   = ""
descol(7)%flag_desc = 1
descol(7)%descrip = "<![CDATA[<b>Heating rate by secondary photons ionizations</b>]]>"
rval2D(1:npo+1,7) = chophs(0:npo)

!--- Colonne 8 : chochi ----------
descol(8)%titre   = "Chemistry heating"
descol(8)%unite   = "erg cm-3 s-1"
descol(8)%form    = "1D"
descol(8)%IDFD    = "FD_CHEMHEAT"
descol(8)%datatyp = "double"
descol(8)%ucd     = ""
descol(8)%utype   = ""
descol(8)%flag_desc = 1
descol(8)%descrip = "<![CDATA[<b>Heating rate by chemical reactions</b>]]>"
rval2D(1:npo+1,8) = chochi(0:npo)

!--- Colonne 9 : chotur ---------
descol(9)%titre   = "Turbulence heating"
descol(9)%unite   = "erg cm-3 s-1"
descol(9)%form    = "1D"
descol(9)%IDFD    = "FD_TURBUHEAT"
descol(9)%datatyp = "double"
descol(9)%ucd     = ""
descol(9)%utype   = ""
descol(9)%flag_desc = 1
descol(9)%descrip = "<![CDATA[<b>Heating rate by turbulence</b>]]>"
rval2D(1:npo+1,9) = chotur(0:npo)

END SUBROUTINE CRE_DATA_HEAT

!!!=========================================================================
!!! SUBROUTINE CRE_DATA_COOL
!!!=========================================================================
SUBROUTINE CRE_DATA_COOL
IMPLICIT NONE

INTEGER :: i, j
CHARACTER(LEN=lennamespecy)    :: espece   ! name of the coolant species
CHARACTER(LEN=lennamespecy)    :: especeID ! ID of the coolant

descol(:)%unite   = "erg cm-3 s-1"
descol(:)%form    = "1D"
descol(:)%datatyp = "double"
descol(:)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/CoolingRate"
descol(:)%flag_desc = 1

!--- Colonne 1 : reftot ----------
i=1
descol(i)%titre   = "Total cooling"
descol(i)%IDFD    = "FD_TOTALCOOL"
descol(i)%utype   = ""
descol(i)%descrip = "<![CDATA[<b>Total cooling rate</b>]]>"
rval2D(1:npo+1,i)       = reftot(0:npo)

!--- Cooling by Fe+
i = i + 1
descol(i)%titre   = "Fe+ cooling"
descol(i)%IDFD    = "FD_FepCOOL"
descol(i)%utype   = ""
descol(i)%descrip = "<![CDATA[<b>Cooling rate by Fe+</b>]]>"
rval2D(1:npo+1,i) = reffep(0:npo)

!--- Specy by specy excepted H2 that is a mixed a process
DO j = 1, nspl
   IF (TRIM(spec_lin(j)%nam) == "h2") CYCLE
   espece = TRIM(spec_lin(j)%nam)
   !PRINT *,j, nspl, "COOLING : ",espece
   CALL SPEMAJ(espece,especeID)
   i = i + 1
   descol(i)%titre = TRIM(espece)//" cooling"
   descol(i)%IDFD    = "FD_"//TRIM(especeID)//"COOL"
   descol(i)%utype   = ""
   descol(i)%descrip = "<![CDATA[<b>Cooling rate by "//TRIM(espece)//"lines</b>]]>"
   rval2D(1:npo+1,i) = spec_lin(j)%ref(0:npo)
ENDDO

END SUBROUTINE CRE_DATA_COOL

!!!==============================================================================
!!! SUBROUTINE CRE_DATA_MIXT
!!! CREATE DATA FOR MIXTE MECHANISMS
!!!==============================================================================
SUBROUTINE CRE_DATA_MIXT

IMPLICIT NONE

!--- Colonne 1 : H2 heating ----------
descol(1)%titre   = "H2 cascades heating"
descol(1)%unite   = "erg cm-3 s-1"
descol(1)%form    = "1D"
descol(1)%IDFD    = "FD_MIXT_H2_HEAT"
descol(1)%datatyp = "double"
descol(1)%ucd     = ""
descol(1)%utype   = ""
descol(1)%flag_desc = 1
descol(1)%descrip = "<![CDATA[<b>Heating rate by H2 formation</b>]]>"
rval2D(1:npo+1,1) = MAX(0.0_dp,-spec_lin(in_sp(i_h2))%ref(0:npo))

!--- Colonne 2 : H2 cooling ----------
descol(2)%titre   = "H2 cascades cooling"
descol(2)%unite   = "erg cm-3 s-1"
descol(2)%form    = "1D"
descol(2)%IDFD    = "FD_MIXT_H2_COOL"
descol(2)%datatyp = "double"
descol(2)%ucd     = ""
descol(2)%utype   = ""
descol(2)%flag_desc = 1
descol(2)%descrip = "<![CDATA[<b>Cooling rate by H2 formation</b>]]>"
rval2D(1:npo+1,2) = MAX(0.0_dp,spec_lin(in_sp(i_h2))%ref(0:npo))

!--- Colonne 3: gaz-grain heating ----------
descol(3)%titre   = "Gas-grains collisions heating"
descol(3)%unite   = "erg cm-3 s-1"
descol(3)%form    = "1D"
descol(3)%IDFD    = "FD_MIXT_GASGRAIN_HEAT"
descol(3)%datatyp = "double"
descol(3)%ucd     = ""
descol(3)%utype   = ""
descol(3)%flag_desc = 1
descol(3)%descrip = "<![CDATA[<b>Heating rate by gas-grains collisions</b>]]>"
rval2D(1:npo+1,3) = MAX(0.0_dp,choeqg(0:npo))

!--- Colonne 4: gaz-grain cooling ----------
descol(4)%titre   = "Gas-grains collisions cooling"
descol(4)%unite   = "erg cm-3 s-1"
descol(4)%form    = "1D"
descol(4)%IDFD    = "FD_MIXT_GASGRAIN_COOL"
descol(4)%datatyp = "double"
descol(4)%ucd     = ""
descol(4)%utype   = ""
descol(4)%flag_desc = 1
descol(4)%descrip = "<![CDATA[<b>Cooling rate by gas-grains collisions</b>]]>"
rval2D(1:npo+1,4)       = MAX(0.0_dp,-choeqg(0:npo))

END SUBROUTINE CRE_DATA_MIXT

!!!============================================================================
!!! SUBROUTINE CRE_DATA_ABOND : Abondances
!!!
!!! ATTENTION : ABNUA contains heterogeneous data that are
!!!             0                      : pseudo abundance of specy NOTHING
!!!             1 to nspec             : abundances in cm-3
!!!             nspec+1 to n_var       : moment (number of atom on a grain)
!!!             n_var + 1 to n_var + 5 : pseudo abundances of PHOTONS, CRP, PHOSEC, GRAIN, 2H
!!!
!!! HERE we only write abundances from 0 to nspec
!!!============================================================================
SUBROUTINE CRE_DATA_ABOND(nlgn, ncol)

IMPLICIT NONE
INTEGER,       INTENT(IN)  :: nlgn
INTEGER,       INTENT(IN)  :: ncol
INTEGER                    :: i, j
CHARACTER(LEN=lennamespecy):: espece
CHARACTER(LEN=lennamespecy):: especeID
INTEGER                    :: ngr

descol(:)%form="1D"  ! Format double precision

ngr = 0 ! compteur pour le numero de bin des grains
        ! sert a differencier les differents H2:
DO i = 1, ncol
   espece = species(i)
   CALL SPEMAJ(espece,especeID)
   descol(i)%titre = "AB_"//TRIM(espece)
   descol(i)%IDFD  = "FD_AB_"//especeID
ENDDO
descol(:)%form        = "1D"
descol(:)%unite       = "cm-3"
descol(:)%datatyp     = "double"
descol(:)%ucd         = "http://purl.org/astronomy/vocab/PhysicalQuantities/Abundance"
descol(:)%utype       = ""
descol(:)%flag_desc    = 0
DO i = 0, nlgn-1    ! boucle sur npo
   DO j = 1, ncol ! boucle sur nspec
      rval2D(i+1,j) = abnua(j,i)
   ENDDO
ENDDO

END SUBROUTINE CRE_DATA_ABOND

!!!============================================================================
!!! SUBROUTINE CRE_DATA_COLDENS : Column densities
!!!============================================================================
SUBROUTINE CRE_DATA_COLDENS(nlgn, ncol)

IMPLICIT NONE
INTEGER,       INTENT(IN)  :: nlgn
INTEGER,       INTENT(IN)  :: ncol
INTEGER                    :: i, j
CHARACTER(LEN=lennamespecy):: espece
CHARACTER(LEN=lennamespecy):: especeID
INTEGER                    :: ngr

descol(:)%form="1D"  ! Format double precision

 ! passage en majuscule
ngr = 0 ! compteur pour le numero de bin des grains
        ! sert a differencier les differents H2:
DO i = 1, nspec
   espece = species(i)
   CALL SPEMAJ(espece,especeID)
   descol(i)%titre = "CD_"//TRIM(espece)
   descol(i)%IDFD  = "FD_CD_"//especeID
ENDDO
descol(:)%form        = "1D"
descol(:)%unite       = "cm-2"
descol(:)%datatyp     = "double"
descol(:)%ucd         = "http://purl.org/astronomy/vocab/PhysicalQuantities/ColumnDensity"
descol(:)%utype       = ""
descol(:)%flag_desc   = 0
DO i = 0, nlgn-1    ! boucle sur npo
   DO j = 1, ncol ! boucle sur nspec
      rval2D(i+1,j) = codesp(j,i)
   ENDDO
ENDDO

END SUBROUTINE CRE_DATA_COLDENS

!!!============================================================================
!!! SUBROUTINE CRE_DATA_MOMPROFILES : Profiles of moments as a function of position
!!!============================================================================
SUBROUTINE CRE_DATA_MOMPROFILES(nlgn)
IMPLICIT NONE

INTEGER,        INTENT(IN)  :: nlgn

CHARACTER(LEN=lennamespecy) :: espece
CHARACTER(LEN=lennamespecy) :: namemom
CHARACTER(LEN=lennamespecy) :: idmom

INTEGER :: i, j, k

descol(:)%form="1D"  ! Format double precision

k = 0
DO i = nspec+1, n_var
   k = k + 1
   espece = ""
   espece = speci(i)
   CALL MAKE_NAMEMOMENTS(espece,namemom, idmom)
   descol(k)%titre = TRIM(namemom)
   descol(k)%IDFD  = "FD_MOMPROFILES_"//TRIM(idmom)
ENDDO
descol(:)%form        = "1D"
descol(:)%unite       = "partic. on a grain"
descol(:)%datatyp     = "double"
descol(:)%ucd         = ""
descol(:)%utype       = ""
descol(:)%flag_desc   = 0

DO i = 0, nlgn-1    ! boucle sur npo
   k = 0
   DO j = nspec+1, n_var ! boucle sur nspec
      k = k + 1
      rval2D(i+1,k) = abnua(j,i)
   ENDDO
ENDDO

END SUBROUTINE CRE_DATA_MOMPROFILES

!!!========================================================================
!!! SUBROUTINE IDLEVEL
!!! Creation d'un ID de niveau avec les nombres quantiques
!!!
!!! Exemple : Creer un identifiant de la forme _v12_J20 pour un niveau donner
!!!           Input - spelin, numero du niveau
!!!=========================================================================
SUBROUTINE IDLEVEL(spelin,nlev,IDLEV)

IMPLICIT NONE

TYPE(SPECTRA), INTENT(IN)            :: spelin
INTEGER,       INTENT(IN)            :: nlev   ! Numero du niveau
CHARACTER(LEN=lenIDLEV), INTENT(OUT) :: IDLEV

CHARACTER(LEN=lenIDLEV), ALLOCATABLE :: tabID(:)
CHARACTER(LEN=lenIDLEV)              :: chaine_tempo
CHARACTER(LEN=lenIDLEV)              :: chaine_actu
INTEGER :: nstr  ! Nbre pair de nombres quantiques (= nqn si il est pair ou nqn + 1)
INTEGER :: i

  ! Pour faire la concatenation finale facilement on se ramene a un nombre pair de
  ! nbres quantiques - eventuellement le dernier est vide
  IF (MOD(spelin%nqn,2) .EQ. 1) THEN
     nstr = spelin%nqn + 1 ! Si ntr
     ALLOCATE(tabID(spelin%nqn+1))
     tabID(spelin%nqn+1) = ""
  ELSE
     nstr = spelin%nqn
     ALLOCATE(tabID(spelin%nqn))
  ENDIF

  ! Concatenation des couples : nom + valeur dans les cases de tabID
  DO i = 1, spelin%nqn
     tabID(i) = "_"//TRIM(ADJUSTL(spelin%qnam(i)))//TRIM(ADJUSTL(spelin%quant(nlev,i)))
  ENDDO

  ! On met le tout ensemble - C'est ici que c'est plus pratique d'avoir un nombre pair
  ! de chaines a traiter
  IDLEV = ""
  DO i = 1, nstr, 2
     chaine_tempo = TRIM(ADJUSTL(IDLEV))
     chaine_actu  = TRIM(ADJUSTL(tabID(i)))//TRIM(ADJUSTL(tabID(i+1)))
     IDLEV = TRIM(ADJUSTL(chaine_tempo))//TRIM(ADJUSTL(chaine_actu))
  ENDDO

  DEALLOCATE(tabID)

END SUBROUTINE IDLEVEL

!!!======================================================================================
!!! SUBROUTINE WRITE_DATA_SPELIN
!!! WRITE PROPERTIES OF LINES :
!!! -- DU 01 : LINE PROPERTIES
!!! -- DU 02 : LOCAL EMISSIVITIES - spelin%emi
!!! -- DU 03 : ASSOCIATED QUANTITIES - spelin%siD, spelin%exl and spelin%exr
!!! -- DU 04 : OPTICAL DEPTH LEFT
!!! -- DU 05 : OPTICAL DEPTH RIGHT
!!! -- DU 06 : ESCAPE PROBABILITY
!!! -- DU 07 : PUMPING RATE
!!! -- DU 08 : DOPPLER WIDTH
!!! -- DU 09 : JiD
!!! -- DU 10 : INTEGRATED RELATIVE ABUNDANCE - spelin%XXl
!!! -- DU 11 : THERMAL VELOCITY - spelin%vel
!!!======================================================================================
SUBROUTINE WRITE_DATA_SPELIN(filefits,unitxml,numhdu,spelin)
USE PXDR_CONSTANTES
IMPLICIT NONE

CHARACTER(LEN=lenfilename), INTENT(IN)  :: filefits  ! nom du fichier .fits
INTEGER, INTENT(IN)                     :: unitxml
INTEGER, INTENT(INOUT)                  :: numhdu
TYPE(SPECTRA), INTENT(IN)               :: spelin

TYPE(DUXML)                    :: blocDUxml
CHARACTER(LEN=lenpath)         :: href
INTEGER                        :: ncol
INTEGER                        :: nlgn
CHARACTER(LEN=lenname)         :: name
CHARACTER(LEN=lenID)           :: extname
CHARACTER(LEN=lennamespecy)    :: espece
CHARACTER(LEN=lennamespecy)    :: especeID
CHARACTER(LEN=lenIDLEV)        :: IDLEV
CHARACTER(LEN=lenIDLEV)        :: IDLEVup
CHARACTER(LEN=lenIDLEV)        :: IDLEVlo
INTEGER                        :: naxe ! Nombre d'axes auquelles les quantites sont associees
CHARACTER(LEN=lentw)           :: type_write
TYPE(DUAXE)                    :: descaxe(naxmax) ! Description des axes

INTEGER :: i, j

    !--- Preparation of the STREAM for VO-TABLE
    href = "file///./fits/"//filefits
    href = TRIM(ADJUSTL(href))
    blocDUxml%flag_stream = 1
    blocDUxml%extnum      = 0    ! It is not used in our case
    blocDUxml%encoding    = ""
    blocDUxml%href        = href

    espece = TRIM(spelin%nam)
    CALL SPEMAJ(espece,especeID)

    !-----------------------------------------------------------------------------------
    !--- Properties of the transitions
    !-----------------------------------------------------------------------------------
    numhdu = numhdu + 1
    nlgn = spelin%ntr
    IF (nlgn .GT. 999) nlgn = 999
    ncol = 8 ! num, nup, nlo, lam_central, waveK, Aij, information, ID
             ! 3 entiers (ival2D) + 3 doubles (rval2D) + 2 string (sval2D)

    name = TRIM(ADJUSTL(espece))//" lines properties"
    extname = "DU_LINEPROP_"//TRIM(ADJUSTL(especeID))
    ALLOCATE(descol(ncol))
    ALLOCATE(ival2D(nlgn,3))
    ALLOCATE(rval2D(nlgn,3))
    ALLOCATE(sval2D(nlgn,2))

    descol(1)%titre   = "num"
    descol(1)%form    = "1J"
    descol(1)%unite   = "no unit"
    descol(1)%IDFD    = "FD_TRANS_INDEX"
    descol(1)%datatyp = "integer"
    descol(1)%ucd     = ""
    descol(1)%utype   = ""
    descol(1)%flag_desc = 1
    descol(1)%descrip = "<![CDATA[Index of the transitions &
                        & used in the model]]>"

    descol(2)%titre   = "Lev up"
    descol(2)%form    = "1J"
    descol(2)%unite   = "no unit"
    descol(2)%IDFD    = "FD_TRANS_IUP"
    descol(2)%datatyp = "integer"
    descol(2)%ucd     = ""
    descol(2)%utype   = ""
    descol(2)%flag_desc = 1
    descol(2)%descrip = "<![CDATA[Index of the upper &
                        & level]]>"

    descol(3)%titre   = "Lev low"
    descol(3)%form    = "1J"
    descol(3)%unite   = "no unit"
    descol(3)%IDFD    = "FD_TRANS_ILO"
    descol(3)%datatyp = "integer"
    descol(3)%ucd     = ""
    descol(3)%utype   = ""
    descol(3)%flag_desc = 1
    descol(3)%descrip = "<![CDATA[Index of the lower &
                        & level]]>"

    descol(4)%titre   = "Wavelength"
    descol(4)%form    = "1D"
    descol(4)%unite   = "cm"
    descol(4)%IDFD    = "FD_TRANS_LAC"
    descol(4)%datatyp = "double"
    descol(4)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/Wavelength"
    descol(4)%utype   = "ldm:Line.wavelength"
    descol(4)%flag_desc = 1
    descol(4)%descrip = "<![CDATA[Central wavelength &
                        & ]]>"

    descol(5)%titre   = "Wavelength"
    descol(5)%form    = "1D"
    descol(5)%unite   = "K"
    descol(5)%IDFD    = "FD_TRANS_WLK"
    descol(5)%datatyp = "double"
    descol(5)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/Wavelength"
    descol(5)%utype   = ""
    descol(5)%flag_desc = 1
    descol(5)%descrip = "<![CDATA[Central wavelength &
                        & in Kelvin]]>"

    descol(6)%titre   = "Aij"
    descol(6)%form    = "1D"
    descol(6)%unite   = "s-1"
    descol(6)%IDFD    = "FD_TRANS_EINSTEIN_A"
    descol(6)%datatyp = "double"
    descol(6)%ucd     = "phys.atmol.transProb"
    descol(6)%utype   = "ldm:Line.einsteinA"
    descol(6)%flag_desc = 1
    descol(6)%descrip = "<![CDATA[Emission probability  &
                        & ]]>"

    descol(7)%titre   = "Information"
    descol(7)%form    = "100A"
    descol(7)%unite   = "no unit"
    descol(7)%IDFD    = "FD_TRANS_INFO"
    descol(7)%datatyp = "string"
    descol(7)%ucd     = ""
    descol(7)%utype   = ""
    descol(7)%flag_desc = 1
    descol(7)%descrip = "<![CDATA[Information on transition &
                        & ]]>"

    descol(8)%titre   = "ID"
    descol(8)%form    = "50A"
    descol(8)%unite   = "no unit"
    descol(8)%IDFD    = "FD_TRANS_ID"
    descol(8)%datatyp = "string"
    descol(8)%ucd     = ""
    descol(8)%utype   = ""
    descol(8)%flag_desc = 1
    descol(8)%descrip = "<![CDATA[Line ID]]>"

    ival2D(:,:) = 0 ! stupid value
    rval2D(:,:) = 0.0_dp
    DO j = 1, nlgn
      ival2D(j,1) = j ! numero de la transition
      ival2D(j,2) = spelin%iup(j) ! index of upper level
      ival2D(j,3) = spelin%jlo(j) ! index of lower level
      rval2D(j,1) = spelin%lac(j) ! wavelength in cm
      rval2D(j,2) = spelin%wlk(j) ! wavelength in K
      rval2D(j,3) = spelin%aij(j) ! Einstein coefficient Aij (s-1)
      sval2D(j,1) = spelin%inftr(j)
      CALL IDLEVEL(spelin,spelin%iup(j), IDLEVUP)
      CALL IDLEVEL(spelin,spelin%jlo(j), IDLEVLO)
      sval2D(j,2) = TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
    ENDDO

    type_write = "line_data"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol,type_write)
    blocDUxml%nameDU    = name
    blocDUxml%IDDU      = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU   = "" ! ATTENTION CAS PARTICULIER PAS DANS l'ARBRE
    blocDUxml%modeDU    = "DEV"
    blocDUxml%flag_desc = 1
    blocDUxml%descripDU = "Lines"
    naxe = 0
    !descaxe(1)%name = '""'
    !descaxe(1)%ID   = '""'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)
    DEALLOCATE(ival2D)
    DEALLOCATE(sval2D)

    !----------------------------------------------------------------------------------
    !--- Optical depth towards left - Array 0:npo
    !----------------------------------------------------------------------------------
    numhdu  = numhdu+1
    nlgn    = npo+1
    ncol    = spelin%ntr
    IF (ncol .GT. 999) ncol = 999 ! ATTENTION IL SEMBLE QUE LE FITS NE SUPPORTE PAS PLUS
                                  ! DE 999 COLONNES. OR POUR H2 ON EN A 4668 ET PLUS POUR HD

    name = TRIM(ADJUSTL(espece))//" optical depth towards observer"
    extname = "DU_OPTDEPL_"//TRIM(ADJUSTL(especeID))
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    DO i = 1, ncol
        CALL IDLEVEL(spelin,spelin%iup(i),IDLEVup)
        CALL IDLEVEL(spelin,spelin%jlo(i),IDLEVlo)
        descol(i)%titre = "Opt. depth left tr:"//TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
        descol(i)%IDFD  = "FD_OPTDEPL_"//TRIM(ADJUSTL(especeID))//"_TR" &
                          //TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
    ENDDO
    descol(:)%form  = "1D"
    descol(:)%unite = "no unit"
    descol(:)%datatyp = "double"
    descol(:)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/OpticalDepthInTheVisible"
    descol(:)%utype   = ""
    descol(:)%flag_desc = 0
    DO i = 0, nlgn-1
       DO j = 1, ncol
          rval2D(i+1,j) = spelin%odl(j,i)
       ENDDO
    ENDDO
    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "" ! ATTENTION CAS PARTICULIER
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------------
    !--- Optical depth towards right - Array 0:np
    !----------------------------------------------------------------------------------
    numhdu  = numhdu+1
    nlgn    = npo+1
    ncol    = spelin%ntr
    IF (ncol .GT. 999) ncol = 999 ! ATTENTION IL SEMBLE QUE LE FITS NE SUPPORTE PAS PLUS
                                  ! DE 999 COLONNES. OR POUR H2 ON EN A 4668 ET PLUS POUR HD

    name = TRIM(ADJUSTL(espece))//" optical depth towards backside"
    extname = "DU_OPTDEPR_"//TRIM(ADJUSTL(especeID))
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    DO i = 1, ncol
        CALL IDLEVEL(spelin,spelin%iup(i),IDLEVup)
        CALL IDLEVEL(spelin,spelin%jlo(i),IDLEVlo)
        descol(i)%titre = "Opt. depth right tr:"//TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
        descol(i)%IDFD  = "FD_OPTDEPR_"//TRIM(ADJUSTL(especeID))//"_TR" &
                          //TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
    ENDDO
    descol(:)%form  = "1D"
    descol(:)%unite = "no unit"
    descol(:)%datatyp = "double"
    descol(:)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/OpticalDepthInTheVisible"
    descol(:)%utype   = ""
    descol(:)%flag_desc = 0
    DO i = 0, nlgn-1
       DO j = 1, ncol
          rval2D(i+1,j) = spelin%odr(j,i)
       ENDDO
    ENDDO
    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "" ! ATTENTION CAS PARTICULIER
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------------
    !--- Escape probability (left)
    !----------------------------------------------------------------------------------
    numhdu  = numhdu+1
    nlgn    = npo+1
    ncol    = spelin%ntr
    IF (ncol .GT. 999) ncol = 999 ! ATTENTION IL SEMBLE QUE LE FITS NE SUPPORTE PAS PLUS
                                  ! DE 999 COLONNES. OR POUR H2 ON EN A 4668 ET PLUS POUR HD

    name = TRIM(ADJUSTL(espece))//" escape probability (left)"
    extname = "DU_ESCPROBL_"//TRIM(ADJUSTL(especeID))
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    DO i = 1, ncol
        CALL IDLEVEL(spelin,spelin%iup(i),IDLEVup)
        CALL IDLEVEL(spelin,spelin%jlo(i),IDLEVlo)
        descol(i)%titre = "Esc. prob. L tr:"//TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
        descol(i)%IDFD  = "FD_ESCPROBL_"//TRIM(ADJUSTL(especeID))//"_TR" &
                        //TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
    ENDDO
    descol(:)%form  = "1D"
    descol(:)%unite = "no unit"
    descol(:)%datatyp = "double"
    descol(:)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/EscapeProbability"
    descol(:)%utype   = ""
    descol(:)%flag_desc = 0
    DO i = 0, nlgn-1
       DO j = 1, ncol
          rval2D(i+1,j) = spelin%bel(j,i)
       ENDDO
    ENDDO
    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "" ! ATTENTION CAS PARTICULIER
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------------
    !--- Escape probability (right)
    !----------------------------------------------------------------------------------
    numhdu  = numhdu+1
    nlgn    = npo+1
    ncol    = spelin%ntr
    IF (ncol .GT. 999) ncol = 999 ! ATTENTION IL SEMBLE QUE LE FITS NE SUPPORTE PAS PLUS
                                  ! DE 999 COLONNES. OR POUR H2 ON EN A 4668 ET PLUS POUR HD

    name = TRIM(ADJUSTL(espece))//" escape probability (right)"
    extname = "DU_ESCPROBR_"//TRIM(ADJUSTL(especeID))
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    DO i = 1, ncol
        CALL IDLEVEL(spelin,spelin%iup(i),IDLEVup)
        CALL IDLEVEL(spelin,spelin%jlo(i),IDLEVlo)
        descol(i)%titre = "Esc. prob. R tr:"//TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
        descol(i)%IDFD  = "FD_ESCPROBR_"//TRIM(ADJUSTL(especeID))//"_TR" &
                        //TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
    ENDDO
    descol(:)%form  = "1D"
    descol(:)%unite = "no unit"
    descol(:)%datatyp = "double"
    descol(:)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/EscapeProbability"
    descol(:)%utype   = ""
    descol(:)%flag_desc = 0
    DO i = 0, nlgn-1
       DO j = 1, ncol
          rval2D(i+1,j) = spelin%ber(j,i)
       ENDDO
    ENDDO
    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows     = nlgn
    blocDUxml%ncol      = ncol
    blocDUxml%CNTIDDU  = "" ! ATTENTION CAS PARTICULIER
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------------
    !--- Dust extinction properties (0:npo)
    !--- Variable fortran : spelin%siD(ntr,npo+1)
    !----------------------------------------------------------------------------------
    numhdu  = numhdu+1
    nlgn    = npo+1
    ncol    = spelin%ntr
    IF (ncol .GT. 999) ncol = 999 ! ATTENTION IL SEMBLE QUE LE FITS NE SUPPORTE PAS PLUS
                                  ! DE 999 COLONNES. OR POUR H2 ON EN A 4668 ET PLUS POUR HD

    name = TRIM(ADJUSTL(espece))//" dust extinction"
    extname = "DU_DUST_EX_"//TRIM(ADJUSTL(especeID))
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    DO i = 1, ncol
        CALL IDLEVEL(spelin,spelin%iup(i),IDLEVup)
        CALL IDLEVEL(spelin,spelin%jlo(i),IDLEVlo)
        descol(i)%titre = "Dust extinct.:"//TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
        descol(i)%IDFD  = "FD_DUST_EX_"//TRIM(ADJUSTL(especeID))//"_TR" &
                        //TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
    ENDDO
    descol(:)%form  = "1D"
    descol(:)%unite = "cm**2"
    descol(:)%datatyp = "double"
    descol(:)%ucd     = ""
    descol(:)%utype   = ""
    descol(:)%flag_desc = 0
    DO i = 0, nlgn-1
       DO j = 1, ncol
          rval2D(i+1,j) = spelin%siD(j,i)
       ENDDO
    ENDDO
    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows    = nlgn
    blocDUxml%ncol     = ncol
    blocDUxml%CNTIDDU  = "" ! ATTENTION CAS PARTICULIER
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    !----------------------------------------------------------------------------------
    !--- Pumping radiation
    !----------------------------------------------------------------------------------
    numhdu  = numhdu+1
    nlgn    = npo+1
    ncol    = spelin%ntr
    IF (ncol .GT. 999) ncol = 999 ! ATTENTION IL SEMBLE QUE LE FITS NE SUPPORTE PAS PLUS
                                  ! DE 999 COLONNES. OR POUR H2 ON EN A 4668 ET PLUS POUR HD

    name = TRIM(ADJUSTL(espece))//" pumping radiation in lines"
    extname = "DU_LINEPUMP_"//TRIM(ADJUSTL(especeID))
    !PRINT *,"HDU :", numhdu, " ",extname
    ALLOCATE(descol(ncol))
    ALLOCATE(rval2D(nlgn,ncol))
    DO i = 1, ncol
        CALL IDLEVEL(spelin,spelin%iup(i),IDLEVup)
        CALL IDLEVEL(spelin,spelin%jlo(i),IDLEVlo)
        descol(i)%titre = "Pumping tr:"//TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
        descol(i)%IDFD  = "FD_LINEPUMP_"//TRIM(ADJUSTL(especeID))//"_TR" &
                          //TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
    ENDDO
    descol(:)%form  = "1D"
    descol(:)%unite = ""
    descol(:)%datatyp = "double"
    descol(:)%ucd     = ""
    descol(:)%utype   = ""
    descol(:)%flag_desc = 0
    DO i = 0, nlgn-1
       DO j = 1, ncol
          rval2D(i+1,j) = spelin%pum(j,i)
       ENDDO
    ENDDO
    type_write = "double2D"
    CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

    !--- Creation du XML --------------
    blocDUxml%nameDU   = name
    blocDUxml%IDDU     = extname
    blocDUxml%nrows    = nlgn
    blocDUxml%ncol     = ncol
    blocDUxml%CNTIDDU  = "" ! ATTENTION CAS PARTICULIER
    blocDUxml%modeDU   = "USER"
    blocDUxml%flag_desc = 0
    blocDUxml%descripDU = ""
    naxe = 1
    descaxe(1)%name = '"Optical depth"'
    descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
    CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

    DEALLOCATE(descol)
    DEALLOCATE(rval2D)

    IF (F_OUTPUT .EQ. 2) THEN

       !----------------------------------------------------------------------------------
       !--- Local emissivities : spelin%emi
       !----------------------------------------------------------------------------------
       numhdu = numhdu+1
       nlgn = npo+1
       ncol = spelin%ntr ! one column per transition
       IF (ncol .GT. 999) ncol = 999 ! ATTENTION IL SEMBLE QUE LE FITS NE SUPPORTE PAS PLUS
                                     ! DE 999 COLONNES. OR POUR H2 ON EN A 4668 ET PLUS POUR HD

       name = TRIM(ADJUSTL(espece))//" local emissivities"
       extname = "DU_EMILOC_"//TRIM(ADJUSTL(especeID))
       !PRINT *,"HDU :", numhdu, " ",extname
       ALLOCATE(descol(ncol))
       ALLOCATE(rval2D(nlgn,ncol))
       rval2D(:,:) = 0.0_dp
       DO i = 1, ncol
          CALL IDLEVEL(spelin,spelin%iup(i),IDLEVup)
          CALL IDLEVEL(spelin,spelin%jlo(i),IDLEVlo)
          descol(i)%titre = "Transition"//TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
          descol(i)%IDFD  = "FD_EMILOC_"//TRIM(ADJUSTL(especeID))//"_TR" &
                          //TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
       ENDDO
       descol(:)%form    = "1D"
       descol(:)%unite   = "erg cm-3 s-1"
       descol(:)%datatyp = "double"
       descol(:)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/Emissivity"
       descol(:)%utype   = ""
       descol(:)%flag_desc = 0
       DO i = 0, nlgn-1
          DO j = 1, ncol
             rval2D(i+1,j) = spelin%emi(j,i)
          ENDDO
       ENDDO
       type_write = "double2D"
       CALL WRITE_HDU(numhdu,extname,nlgn,ncol,type_write)

       blocDUxml%nameDU    = name
       blocDUxml%IDDU      = extname
       blocDUxml%nrows     = nlgn
       blocDUxml%ncol      = ncol
       blocDUxml%CNTIDDU   = "" ! ATTENTION CAS PARTICULIER PAS DANS l'ARBRE
       blocDUxml%modeDU    = "USER"
       blocDUxml%flag_desc = 0
       blocDUxml%descripDU = ""
       naxe = 1
       descaxe(1)%name = '"Optical depth"'
       descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
       CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

       DEALLOCATE(descol)
       DEALLOCATE(rval2D)

       !-----------------------------------------------------------------------------------
       !--- External radiation field at line wavelength
       !--- Fortran variable : spelin%exl(ntr) and spelin%exr(ntr)
       !-----------------------------------------------------------------------------------
       numhdu = numhdu + 1
       nlgn = spelin%ntr
       IF (nlgn .GT. 999) nlgn = 999
       ncol = 2 ! exl and exr
                ! 2 doubles => rval2D

       name = TRIM(ADJUSTL(espece))//" Line quantities"
       extname = "DU_LINEQUANT_"//TRIM(ADJUSTL(especeID))
       ALLOCATE(descol(ncol))
       ALLOCATE(rval2D(nlgn,ncol))

       descol(1)%titre   = "Ext. radiation field at line wav. left"
       descol(1)%form    = "1D"
       descol(1)%unite   = ""
       descol(1)%IDFD    = "FD_EXTRADL"
       descol(1)%datatyp = "double"
       descol(1)%ucd     = ""
       descol(1)%utype   = ""
       descol(1)%flag_desc = 1
       descol(1)%descrip = "<![CDATA[External radiation field at &
                           &line center form obs. side]]>"

       descol(2)%titre   = "Ext. radiation field at line wav. right"
       descol(2)%form    = "1D"
       descol(2)%unite   = ""
       descol(2)%IDFD    = "FD_EXTRADR"
       descol(2)%datatyp = "double"
       descol(2)%ucd     = ""
       descol(2)%utype   = ""
       descol(2)%flag_desc = 1
       descol(2)%descrip = "<![CDATA[External radiation field at &
                           &line center from back side]]>"

       rval2D(:,:) = 0.0_dp
       DO j = 1, nlgn
          rval2D(j,1) = spelin%exl(j) ! Ext radiation field at line center from obs. side
          rval2D(j,2) = spelin%exr(j) ! Ext radiation field at line center from back side
       ENDDO

       type_write = "double2D"
       CALL WRITE_HDU(numhdu,extname,nlgn,ncol,type_write)
       blocDUxml%nameDU    = name
       blocDUxml%IDDU      = extname
       blocDUxml%nrows     = nlgn
       blocDUxml%ncol      = ncol
       blocDUxml%CNTIDDU   = "" ! ATTENTION CAS PARTICULIER PAS DANS l'ARBRE
       blocDUxml%modeDU    = "USER"
       blocDUxml%flag_desc = 0
       blocDUxml%descripDU = ""
       naxe = 0
       CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

       DEALLOCATE(descol)
       DEALLOCATE(rval2D)

       !----------------------------------------------------------------------------------
       !--- Doppler width
       !--- Fortran variable : spelin%dnu
       !----------------------------------------------------------------------------------
       numhdu  = numhdu+1
       nlgn    = npo+1
       ncol    = spelin%ntr
       IF (ncol .GT. 999) ncol = 999 ! ATTENTION IL SEMBLE QUE LE FITS NE SUPPORTE PAS PLUS
                                     ! DE 999 COLONNES. OR POUR H2 ON EN A 4668 ET PLUS POUR HD

       name = TRIM(ADJUSTL(espece))//" pumping radiation in lines"
       extname = "DU_LINEDOP_"//TRIM(ADJUSTL(especeID))
       ALLOCATE(descol(ncol))
       ALLOCATE(rval2D(nlgn,ncol))
       DO i = 1, ncol
          CALL IDLEVEL(spelin,spelin%iup(i),IDLEVup)
          CALL IDLEVEL(spelin,spelin%jlo(i),IDLEVlo)
          descol(i)%titre = "Doppler width tr:"//TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
          descol(i)%IDFD  = "FD_LINEDOP_"//TRIM(ADJUSTL(especeID))//"_TR" &
                            //TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
       ENDDO
       descol(:)%form    = "1D"
       descol(:)%unite   = ""
       descol(:)%datatyp = "double"
       descol(:)%ucd     = "http://purl.org/astronomy/vocab/PhysicalQuantities/DopplerWidth"
       descol(:)%utype   = ""
       descol(:)%flag_desc = 0
       DO i = 0, nlgn-1
          DO j = 1, ncol
             rval2D(i+1,j) = spelin%dnu(j,i)
          ENDDO
       ENDDO
       type_write = "double2D"
       CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

       !--- Creation du XML --------------
       blocDUxml%nameDU    = name
       blocDUxml%IDDU      = extname
       blocDUxml%nrows     = nlgn
       blocDUxml%ncol      = ncol
       blocDUxml%CNTIDDU   = "" ! ATTENTION CAS PARTICULIER
       blocDUxml%modeDU    = "USER"
       blocDUxml%flag_desc = 0
       blocDUxml%descripDU = ""
       naxe = 1
       descaxe(1)%name = '"Optical depth"'
       descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
       CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

       DEALLOCATE(descol)
       DEALLOCATE(rval2D)

       !----------------------------------------------------------------------------------
       !--- Fortran variable : spelin%jiD
       !----------------------------------------------------------------------------------
       numhdu  = numhdu+1
       nlgn    = npo+1
       ncol    = spelin%ntr
       IF (ncol .GT. 999) ncol = 999 ! ATTENTION IL SEMBLE QUE LE FITS NE SUPPORTE PAS PLUS
                                     ! DE 999 COLONNES. OR POUR H2 ON EN A 4668 ET PLUS POUR HD

       name = TRIM(ADJUSTL(espece))//" mean intens. from dust radiation"
       extname = "DU_LINEJID_"//TRIM(ADJUSTL(especeID))
       ALLOCATE(descol(ncol))
       ALLOCATE(rval2D(nlgn,ncol))
       DO i = 1, ncol
          CALL IDLEVEL(spelin,spelin%iup(i),IDLEVup)
          CALL IDLEVEL(spelin,spelin%jlo(i),IDLEVlo)
          descol(i)%titre = "Jid tr:"//TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
          descol(i)%IDFD  = "FD_LINEJID_"//TRIM(ADJUSTL(especeID))//"_TR" &
                            //TRIM(ADJUSTL(IDLEVup))//TRIM(ADJUSTL(IDLEVlo))
       ENDDO
       descol(:)%form  = "1D"
       descol(:)%unite = ""
       descol(:)%datatyp = "double"
       descol(:)%ucd     = ""
       descol(:)%utype   = ""
       descol(:)%flag_desc = 0
       DO i = 0, nlgn-1
          DO j = 1, ncol
             rval2D(i+1,j) = spelin%jiD(j,i)
          ENDDO
       ENDDO
       type_write = "double2D"
       CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

       !--- Creation du XML --------------
       blocDUxml%nameDU    = name
       blocDUxml%IDDU      = extname
       blocDUxml%nrows     = nlgn
       blocDUxml%ncol      = ncol
       blocDUxml%CNTIDDU   = "" ! ATTENTION CAS PARTICULIER
       blocDUxml%modeDU    = "USER"
       blocDUxml%flag_desc = 0
       blocDUxml%descripDU = ""
       naxe = 1
       descaxe(1)%name = '"Optical depth"'
       descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
       CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

       DEALLOCATE(descol)
       DEALLOCATE(rval2D)

       !----------------------------------------------------------------------------------
       !--- XXL - Integrated relative abundance (left)
       !----------------------------------------------------------------------------------
       numhdu  = numhdu+1
       nlgn    = npo+1
       ncol    = spelin%use
       IF (ncol .GT. 999) ncol = 999 ! ATTENTION IL SEMBLE QUE LE FITS NE SUPPORTE PAS PLUS
                                     ! DE 999 COLONNES. OR POUR H2 ON EN A 4668 ET PLUS POUR HD

       name = TRIM(ADJUSTL(espece))//" integrated relative abundance"
       extname = "DU_XXL_"//TRIM(ADJUSTL(especeID))
       ALLOCATE(descol(ncol))
       ALLOCATE(rval2D(nlgn,ncol))
       DO i = 1, ncol
          CALL IDLEVEL(spelin,i,IDLEV)
          descol(i)%titre = "XXl LEV"//TRIM(ADJUSTL(IDLEV))
          descol(i)%IDFD  = "FD_XXL_"//TRIM(ADJUSTL(especeID))//"_LEV" &
                            //TRIM(ADJUSTL(IDLEV))
       ENDDO
       descol(:)%form  = "1D"
       descol(:)%unite = ""
       descol(:)%datatyp = "double"
       descol(:)%ucd     = ""
       descol(:)%utype   = ""
       descol(:)%flag_desc = 0
       DO i = 0, nlgn-1
          DO j = 1, ncol
             rval2D(i+1,j) = spelin%XXl(j,i)
          ENDDO
       ENDDO
       type_write = "double2D"
       CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

       !--- Creation du XML --------------
       blocDUxml%nameDU   = name
       blocDUxml%IDDU     = extname
       blocDUxml%nrows     = nlgn
       blocDUxml%ncol      = ncol
       blocDUxml%CNTIDDU  = "" ! ATTENTION CAS PARTICULIER
       blocDUxml%modeDU   = "USER"
       blocDUxml%flag_desc = 0
       blocDUxml%descripDU = ""
       naxe = 1
       descaxe(1)%name = '"Optical depth"'
       descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
       CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

       DEALLOCATE(descol)
       DEALLOCATE(rval2D)

       !----------------------------------------------------------------------------------
       !--- XXR - Integrated relative abundance (right)
       !----------------------------------------------------------------------------------
       numhdu  = numhdu+1
       nlgn    = npo+1
       ncol    = spelin%use
       IF (ncol .GT. 999) ncol = 999 ! ATTENTION IL SEMBLE QUE LE FITS NE SUPPORTE PAS PLUS
                                     ! DE 999 COLONNES. OR POUR H2 ON EN A 4668 ET PLUS POUR HD

       name = TRIM(ADJUSTL(espece))//" integrated relative abundance"
       extname = "DU_XXR_"//TRIM(ADJUSTL(especeID))
       ALLOCATE(descol(ncol))
       ALLOCATE(rval2D(nlgn,ncol))
       DO i = 1, ncol
          CALL IDLEVEL(spelin,i,IDLEV)
          descol(i)%titre = "XX LEV"//TRIM(ADJUSTL(IDLEV))
          descol(i)%IDFD  = "FD_XXR_"//TRIM(ADJUSTL(especeID))//"_LEV" &
                            //TRIM(ADJUSTL(IDLEV))
       ENDDO
       descol(:)%form  = "1D"
       descol(:)%unite = ""
       descol(:)%datatyp = "double"
       descol(:)%ucd     = ""
       descol(:)%utype   = ""
       descol(:)%flag_desc = 0
       DO i = 0, nlgn-1
          DO j = 1, ncol
             rval2D(i+1,j) = spelin%XXr(j,i)
          ENDDO
       ENDDO
       type_write = "double2D"
       CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

       !--- Creation du XML --------------
       blocDUxml%nameDU   = name
       blocDUxml%IDDU     = extname
       blocDUxml%nrows     = nlgn
       blocDUxml%ncol      = ncol
       blocDUxml%CNTIDDU  = "" ! ATTENTION CAS PARTICULIER
       blocDUxml%modeDU   = "USER"
       blocDUxml%flag_desc = 0
       blocDUxml%descripDU = ""
       naxe = 1
       descaxe(1)%name = '"Optical depth"'
       descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
       CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

       DEALLOCATE(descol)
       DEALLOCATE(rval2D)

       !----------------------------------------------------------------------------------
       !--- vel - Thermal velocity (including turbulence)
       !----------------------------------------------------------------------------------
       numhdu  = numhdu+1
       nlgn    = npo+1
       ncol    = 1
       name = TRIM(ADJUSTL(espece))//" Thermal velocity field"
       extname = "DU_SPECVEL_"//TRIM(ADJUSTL(especeID))
       ALLOCATE(descol(1))
       ALLOCATE(rval(nlgn))
       descol(1)%titre     = name
       descol(1)%IDFD      = "FD_SPECVEL_"//TRIM(ADJUSTL(especeID))
       descol(1)%form      = "1D"
       descol(1)%unite     = ""
       descol(1)%datatyp   = "double"
       descol(1)%ucd       = ""
       descol(1)%utype     = ""
       descol(1)%flag_desc = 0
       DO i = 0, nlgn-1
          rval(i+1) = spelin%vel(i)
       ENDDO
       type_write = "double"
       CALL WRITE_HDU(numhdu,extname,nlgn,ncol, type_write)

       !--- Creation du XML --------------
       blocDUxml%nameDU    = name
       blocDUxml%IDDU      = extname
       blocDUxml%nrows     = nlgn
       blocDUxml%ncol      = ncol
       blocDUxml%CNTIDDU   = "" ! ATTENTION CAS PARTICULIER
       blocDUxml%modeDU    = "USER"
       blocDUxml%flag_desc = 0
       blocDUxml%descripDU = ""
       naxe = 1
       descaxe(1)%name = '"Optical depth"'
       descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
       CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

       DEALLOCATE(descol)
       DEALLOCATE(rval)

    ENDIF ! End of the IF on the optional outputs controled by F_OUTPUT

END SUBROUTINE WRITE_DATA_SPELIN

!!!============================================================================
!!! SUBROUTINE CRE_DATA_CHIM : tableau des prop de la chimie
!!!============================================================================
SUBROUTINE CRE_DATA_CHIM

IMPLICIT NONE
INTEGER             :: i

i = 1
descol(i)%titre   = "Numb. of H2/HD form/dest reactions on grains"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NH2FOR"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of H2 and HD formation and &
                            & destruction reactions on grains]]>"
ival2D(1,i)       = nh2for

i = i + 1
descol(i)%titre   = "Numb. of cosmic rays reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NCRION"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of cosmic rays reactions]]>"
ival2D(1,i)       = ncrion

i = i + 1
descol(i)%titre   = "Numb. of reactions with secondary photons"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NPSION"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of reactions with secondary photons]]>"
ival2D(1,i)       = npsion

i = i + 1
descol(i)%titre   = "Numb. of radiative association reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NASRAD"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of radiative association reactions]]>"
ival2D(1,i)       = nasrad

i = i + 1
descol(i)%titre   = "Numb. of gas phase reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NREST"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of gas phase reactions]]>"
ival2D(1,i)       = nrest

i = i + 1
descol(i)%titre   = "Numb. of H2 endothermal reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NH2ENDO"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of H2 endothermal reactions]]>"
ival2D(1,i)       = nh2endo

i = i + 1
descol(i)%titre   = "Numb. of photo-reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NPHOTF"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of photo-reactions with analytic expression]]>"
ival2D(1,i)       = nphot_f

i = i + 1
descol(i)%titre   = "Numb. of direct photo-reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NPHOTI"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of photo-reactions with direct integration]]>"
ival2D(1,i)       = nphot_i

i = i + 1
descol(i)%titre   = "Numb. of 3 bodies reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_N3BODY"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of 3 bodies reactions]]>"
ival2D(1,i)       = n3body

i = i + 1
descol(i)%titre   = "Numb. of particular reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NCPART"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of particular reactions]]>"
ival2D(1,i)       = ncpart

i = i + 1
descol(i)%titre   = "Numb. of grain surface reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NGSURF"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of grain surface reactions]]>"
ival2D(1,i)       = ngsurf

i = i + 1
descol(i)%titre   = "Numb. of photo-reations on grains surface"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NGPHOT"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of photo-reactions on grain surface]]>"
ival2D(1,i)       = ngphot

i = i + 1
descol(i)%titre   = "Numb. of grains adsorption reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NGADS"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of grain adsorption reactions]]>"
ival2D(1,i)       = ngads

i = i + 1
descol(i)%titre   = "Numb. of neutralization reactions on grains"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NGNEUT"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of neutralization reactions on grains]]>"
ival2D(1,i)       = ngneut

i = i + 1
descol(i)%titre   = "Numb. of explosive desorption reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NGGDES"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of explosive desorption reactions]]>"
ival2D(1,i)       = nggdes

i = i + 1
descol(i)%titre   = "Numb. of cosmic rays desorption"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NCRDES"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of cosmic rays desorptions]]>"
ival2D(1,i)       = ncrdes

i = i + 1
descol(i)%titre   = "Numb. of photo-desorption reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NPHDES"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of photo-desorption reactions]]>"
ival2D(1,i)       = nphdes

i = i + 1
descol(i)%titre   = "Numb. of grain evaporation reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NGEVAP"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of grain evaporation reactions]]>"
ival2D(1,i)       = ngevap

i = i + 1
descol(i)%titre   = "Numb. of chemisorption reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NGCHS"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of chemisorption reactions]]>"
ival2D(1,i)       = ngchs

i = i + 1
descol(i)%titre   = "Numb. of Eley-Rideal reactions"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_NGELRI"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of Eley-Rideal reactions]]>"
ival2D(1,i)       = ngelri

i = i + 1
descol(i)%titre   = "Numb. of mandatory direction integration"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_PROPCHEM_IMANDAT"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Number of mandatory photo-reactions &
                            & with direct integration]]>"
ival2D(1,i)       = i_mandat

END SUBROUTINE CRE_DATA_CHIM

!!!============================================================================
!!! SUBROUTINE CRE_DATA_INDREACT : tableau des numeros de reactions speciales
!!!============================================================================
SUBROUTINE CRE_DATA_INDREAC

IMPLICIT NONE
INTEGER :: i

i = 1
descol(i)%titre   = "Ind. of H adsorption reaction"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_NUMSPEREAC_IADHGR"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Index of the H adsorption reaction]]>"
ival2D(1,i)       = iadhgr

i = i + 1
descol(i)%titre   = "Ind. of H2 formation reaction on grains"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_NUMSPEREAC_IFH2GR"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Ind. of the H2 formation reaction on grains]]>"
ival2D(1,i)       = ifh2gr

i = i + 1
descol(i)%titre   = "Ind. of D adsorption reaction"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_NUMSPEREAC_IADDGR"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Index of the D adsorption reaction]]>"
ival2D(1,i)       = iaddgr

i = i + 1
descol(i)%titre   = "Ind. of HD formation reaction on grains"
descol(i)%form    = "1J"
descol(i)%unite   = "no unit"
descol(i)%IDFD    = "FD_NUMSPEREAC_IFHDGR"
descol(i)%datatyp = "integer"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 1
descol(i)%descrip = "<![CDATA[Ind. of the HD formation reaction on grains]]>"
ival2D(1,i)       = ifhdgr

END SUBROUTINE CRE_DATA_INDREAC

!!!==================================================================================
!!! SUBROUTINE CRE_DATA_SPERATES
!!! SPECIAL RATES
!!!==================================================================================
SUBROUTINE CRE_DATA_SPERATES

IMPLICIT NONE
INTEGER :: i

descol(:)%form = "1D" ! format double precision
descol(:)%datatyp = "double"

i = 1
!--- Colonne 1 : timh21 ---------
descol(i)%titre   = "H2 formation rate by chemistry"
descol(i)%unite   = "cm-3 s-1"
descol(i)%IDFD    = "FD_SPERATES_H2_F_CHEM"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 0
rval2D(1:npo+1,i) = timh21(0:npo)

i = i + 1
!--- Colonne 2 : timh22 ----------
descol(i)%titre   = "H2 destruction rate by chemistry"
descol(i)%unite   = "cm-3 s-1"
descol(i)%IDFD    = "FD_SPERATES_H2_D_CHEM"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 0
rval2D(1:npo+1,i) = timh22(0:npo)

i = i + 1
!--- Colonne 3 : timh23 ----------
descol(i)%titre   = "H2 formation rate on grains"
descol(i)%unite   = "cm-3 s-1"
descol(i)%IDFD    = "FD_SPERATES_H2_F_GRAIN"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 0
rval2D(1:npo+1,i) = timh23(0:npo)

i = i + 1
!--- Colonne 4 : timh24 ---------
descol(i)%titre   = "H2 destruction rate by photons"
descol(i)%unite   = "cm-3 s-1"
descol(i)%IDFD    = "FD_SPERATES_H2_D_PHOT"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 0
rval2D(1:npo+1,i) = timh24(0:npo)

i = i + 1
!--- Colonne 5 : timh25 ---------
descol(i)%titre   = "H2 formation rate by Eley-Rideal"
descol(i)%unite   = "cm-3 s-1"
descol(i)%IDFD    = "FD_SPERATES_H2_F_ELRI"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 0
rval2D(1:npo+1,i) = timh25(0:npo)

i = i + 1
!--- Colonne 8 : timhd1 ---------
descol(i)%titre   = "HD formation rate by chemistry"
descol(i)%unite   = "cm-3 s-1"
descol(i)%IDFD    = "FD_SPERATES_HD_F_CHEM"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 0
rval2D(1:npo+1,i) = timhd1(0:npo)

i = i + 1
!--- Colonne 9 : timhd2 ----------
descol(i)%titre   = "HD destruction rate by chemistry"
descol(i)%unite   = "cm-3 s-1"
descol(i)%IDFD    = "FD_SPERATES_HD_D_CHEM"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 0
rval2D(1:npo+1,i) = timhd2(0:npo)

i = i + 1
!--- Colonne 10 : timhd3 ----------
descol(i)%titre   = "HD formation rate on grains"
descol(i)%unite   = "cm-3 s-1"
descol(i)%IDFD    = "FD_SPERATES_HD_F_GRAIN"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 0
rval2D(1:npo+1,i) = timhd3(0:npo)

i = i + 1
!--- Colonne 11 : timhd4 ---------
descol(i)%titre   = "HD destruction rate by photons"
descol(i)%unite   = "cm-3 s-1"
descol(i)%IDFD    = "FD_SPERATES_HD_D_PHOT"
descol(i)%ucd     = ""
descol(i)%utype   = ""
descol(i)%flag_desc = 0
rval2D(1:npo+1,i) = timhd4(0:npo)

END SUBROUTINE CRE_DATA_SPERATES

!!!==============================================================================
!!! WRITE_SPECTRE_INC
!!! Write in FITS file the incoming spectra (ISRF and star from Obs. side and back side)
!!! * Quantities : Specific intensities in erg cm-2 s-1 strd-1 Ang-1
!!! * ISRF Obs. side is stored in rf_incIm%Val
!!! * Star Obs. side is stored in fluxm
!!! * ISRF back side is stored in rf_incIp%Val
!!! * Star Obs. side is stored in fluxp
!!!
!!! Only quantities effectively used in the model are written
!!!==============================================================================
SUBROUTINE WRITE_SPECTRE_INC(npixlam, filefits, numhdu, unitxml)

USE PXDR_RF_TOOLS
USE PXDR_AUXILIAR

IMPLICIT NONE
INTEGER,                    INTENT(IN)    :: npixlam ! number of pixels in wavelength
CHARACTER(LEN=lenfilename), INTENT(IN)    :: filefits
INTEGER,                    INTENT(INOUT) :: numhdu  ! numero du HDU actuel (avant incrementation)
INTEGER,                    INTENT(IN)    :: unitxml

INTEGER                :: i

CHARACTER(LEN=lenID)   :: extname
CHARACTER(LEN=lenname) :: name
INTEGER                :: nlgn
INTEGER                :: ncol
INTEGER                :: icol

TYPE(DUXML)            :: blocDUxml
CHARACTER(LEN=lenpath) :: href
INTEGER                :: naxe ! Nombre d'axes auquelles les quantites sont associees
TYPE(DUAXE)            :: descaxe(naxmax) ! Description des axes
CHARACTER(LEN=lentw)   :: type_write

     !------------------------------------------------------------------------
     ! WRITE DATA IN FITS FILE
     !------------------------------------------------------------------------
     numhdu = numhdu + 1
     nlgn   = npixlam      ! Lambda on the lines

     ! Determine the number of column to write
     ncol = 0
     IF (radm_ini    .GT.  1.0E-11_dp) ncol = ncol + 1  ! ISRF on observer side
     IF (radp_ini    .GT.  1.0E-11_dp) ncol = ncol + 1  ! ISRF on back side
     IF (d_sour      .LT. -1.0E-11_dp) ncol = ncol + 1  ! STAR on observer side
     IF (d_sour      .GT.  1.0E-11_dp) ncol = ncol + 1  ! STAR on back side

     ALLOCATE(descol(ncol))
     ALLOCATE(rval2D(nlgn,ncol))

     name    = "Incident radiation field"
     extname = "DU_SPECTRA_RFINC"

     !--- Give info on columns ---------------------------
     descol(:)%form      = "1D"
     descol(:)%datatyp   = "double"
     descol(:)%flag_desc = 1

     icol = 0
     !--- ISRF Observer side -----------------------------
     IF (radm_ini .GT. 1.0E-11_dp) THEN
        icol = icol + 1
        descol(icol)%titre   = "Incoming Isot. Rad. Field on observ. side"
        descol(icol)%unite   = "erg cm-2 s-1 str-1 Ang-1"
        descol(icol)%IDFD    = "FD_RFISOT_OBS"
        descol(icol)%ucd     = ""
        descol(icol)%utype   = ""
        descol(icol)%descrip = "<![CDATA[Spectrum (Specific intensity) of &
                               &the incident isotropic radiation field on &
                               &the observer side of the cloud<br/>&
                               &[erg cm-2 s-1 str-1 Ang-1]]]>"
        DO i = 1, nlgn
           rval2D(i,icol) = rf_incIm%val(i-1)
        ENDDO
     ENDIF

     !--- Star Observer side ----------------------------
     IF (d_sour .LT. -1E-11_dp) THEN
        icol = icol + 1
        descol(icol)%titre  = "Incoming Star Rad. Field on observ. side"
        descol(icol)%unite  = "erg cm-2 s-1 str-1 Ang-1"
        descol(icol)%IDFD   = "FD_RFSTAR_OBS"
        descol(icol)%ucd    = ""
        descol(icol)%utype  = ""
        descol(icol)%descrip = "<![CDATA[Spectrum (specific intensity)of the &
                               &incident radiation field of the star on the &
                               &observer side of the cloud. <br/>This radiation field&
                               &is uni-directional on the perpendicular to &
                               &the surface of the cloud.<br/>&
                               &[erg cm-2 s-1 str-1 Ang-1]]]>"
        DO i = 1, nlgn
           rval2D(i,icol) = fluxm(i-1)
        ENDDO
     ENDIF

     !--- ISRF Back side --------------------------------
     IF (radp_ini .GT. 1.0E-11_dp) THEN
        icol = icol + 1
        descol(icol)%titre   = "Incoming Isot. Rad. Field on back side"
        descol(icol)%unite   = "erg cm-2 s-1 str-1 Ang-1"
        descol(icol)%IDFD    = "FD_RFISOT_BS"
        descol(icol)%ucd     = ""
        descol(icol)%utype   = ""
        descol(icol)%descrip = "<![CDATA[Spectrum (Specific intensity) of &
                               &the incident isotropic radiation field on &
                               &the back side of the cloud<br/>&
                               &[erg cm-2 s-1 str-1 Ang-1]]]>"
        DO i = 1, nlgn
           rval2D(i,icol) = rf_incIp%val(i-1)
        ENDDO
     ENDIF

     !--- Star Back side ----------------------------
     IF (d_sour .GT. 1E-11_dp) THEN
        icol = icol + 1
        descol(icol)%titre  = "Star spectrum on back side"
        descol(icol)%unite  = "erg cm-2 s-1 str-1 Ang-1"
        descol(icol)%IDFD   = "FD_RFSTAR_BS"
        descol(icol)%ucd    = ""
        descol(icol)%utype  = ""
        descol(icol)%descrip = "<![CDATA[Spectrum (specific intensity)of the &
                               &incident radiation field of the star on the &
                               &observer side of the cloud. <br/>This &
                               &radiation field&
                               &is uni-directional on the perpendicular to &
                               &the surface of the cloud.<br/>&
                               &[erg cm-2 s-1 str-1 Ang-1]]]>"
        DO i = 1, nlgn
           rval2D(i,icol) = fluxp(i-1)
        ENDDO
     ENDIF

     type_write="double2D"
     CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

     !-------------------------------------------------------
     !--- Creation du XML
     !-------------------------------------------------------
     !--- Preparation of the STREAM for VO-TABLE
     href = "file///./fits/"//filefits
     href = TRIM(ADJUSTL(href))
     blocDUxml%flag_stream = 1
     blocDUxml%extnum      = 0    ! It is not used in our case
     blocDUxml%encoding    = ""
     blocDUxml%href        = href

     blocDUxml%nameDU      = name
     blocDUxml%IDDU        = extname
     blocDUxml%nrows       = nlgn
     blocDUxml%ncol        = ncol
     blocDUxml%CNTIDDU     = "CNT_SPECTRA"
     blocDUxml%modeDU      = "USER"
     blocDUxml%flag_desc   = 0
     blocDUxml%descripDU   = ""
     naxe = 1 ! 1 axe : lambda
     descaxe(1)%name = '"Wavelength"'
     descaxe(1)%ID   = '"DU_WAVELENGTH.FD_WAVELENGTH"'
     CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

     deallocate(descol)
     deallocate(rval2D)

END SUBROUTINE WRITE_SPECTRE_INC

!!!==============================================================================
!!! SUBROUTINE WRITE_SPECTRE_ABS
!!! Write the spectrum of the absorption factor
!!! This spectrum is also written in the .uv ASCII file
!!!==============================================================================
SUBROUTINE WRITE_SPECTRE_ABS(npixlam, filefits, numhdu, unitxml)
USE PXDR_RF_TOOLS
USE PXDR_AUXILIAR
IMPLICIT NONE

INTEGER,                    INTENT(IN) :: npixlam
CHARACTER(LEN=lenfilename), INTENT(IN) :: filefits
INTEGER, INTENT(INOUT)                 :: numhdu ! numero avec incrementation
INTEGER, INTENT(IN)                    :: unitxml

INTEGER                                :: ncol
INTEGER                                :: i

CHARACTER(LEN=lenID)      :: extname
CHARACTER(LEN=lenname)    :: name
INTEGER                   :: nlgn

TYPE(DUXML)               :: blocDUxml
CHARACTER(LEN=lenpath)    :: href
INTEGER                   :: naxe            ! Nombre d'axes auquelles les quantites sont associees
TYPE(DUAXE)               :: descaxe(naxmax) ! Description des axes
CHARACTER(LEN=lentw)      :: type_write

     numhdu = numhdu + 1
     nlgn   = npixlam
     ncol   = 1          ! absorption factor

     ALLOCATE (descol(ncol))
     ALLOCATE (rval2D(nlgn,ncol))

     name    = "Absorption factor spectrum"
     extname = "DU_SPECTRA_ABSFACT"
     descol(1)%form    = "1D"
     descol(1)%unite   = "no_unit"
     descol(1)%datatyp = "double"
     descol(1)%ucd     = ""
     descol(1)%utype   = ""
     descol(1)%titre   = "Absorption factor spectrum"
     descol(1)%IDFD    = "FD_ABSFACT"
     descol(1)%flag_desc = 1
     descol(1)%descrip = "<![CDATA[Absorption factor as a function of &
                         &wavelength. <br/>&
                         &[no unit]]]>"

     DO i = 1, nlgn
        rval2D(i,1) = abs_tot(i-1)
     ENDDO

     type_write="double2D"
     CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

     !-------------------------------------------------------
     !--- Creation du XML --------------
     !-------------------------------------------------------
     !--- Preparation of the STREAM for VO-TABLE
     href = "file///./fits/"//filefits
     href = TRIM(ADJUSTL(href))
     blocDUxml%flag_stream = 1
     blocDUxml%extnum      = 0    ! It is not used in our case
     blocDUxml%encoding    = ""
     blocDUxml%href        = href

     blocDUxml%nameDU   = name
     blocDUxml%IDDU     = extname
     blocDUxml%nrows    = nlgn
     blocDUxml%ncol     = ncol
     blocDUxml%CNTIDDU  = "CNT_SPECTRA"
     blocDUxml%modeDU   = "USER"
     blocDUxml%flag_desc = 0
     blocDUxml%descripDU = ""
     naxe = 1
     descaxe(1)%name = '"Wavelength"'
     descaxe(1)%ID   = '"DU_WAVELENGTH.FD_WAVELENGTH"'
     CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

     deallocate(descol)
     deallocate(rval2D)

END SUBROUTINE WRITE_SPECTRE_ABS

!!!==============================================================================
!!! SUBROUTINE WRITE_SPECTRE_ANG
!!! Write the incident and emergent spectra for each angle of the Legendre decomposition
!!! These spectra are specific intensities in erg cm-2 s-1 str-1 Ang-1
!!! * Obs. side : I_esc_0
!!! * Back side : I_esc_max
!!! With a decomposition on 15 angles this gives 60 spectra
!!! ID in the XML and FITS
!!! - DU_SPECTRA_ANG_INCI_OS and FD_SPECTRA_INCI_OS_index-of-the-angle (index < 0)
!!! - DU_SPECTRA_ANG_EMER_OS and FD_SPECTRA_EMER_OS_index-of-the-angle (index > 0)
!!! - DU_SPECTRA_ANG_INCI_BS and FD_SPECTRA_INCI_BS_index-of-the-angle (index > 0)
!!! - DU_SPECTRA_ANG_EMER_BS and FD_SPECTRA_EMER_BS_index-of-the-angle (index < 0)
!!!==============================================================================
SUBROUTINE WRITE_SPECTRA_ANG(npixlam, filefits, numhdu, unitxml)
USE PXDR_RF_TOOLS
IMPLICIT NONE

INTEGER,                    INTENT(IN) :: npixlam
CHARACTER(LEN=lenfilename), INTENT(IN) :: filefits
INTEGER, INTENT(INOUT)                 :: numhdu ! numero avec incrementation
INTEGER, INTENT(IN)                    :: unitxml

INTEGER                                :: ncol
INTEGER                                :: i, j, k !

CHARACTER(LEN=lenID)      :: extname
CHARACTER(LEN=lenname)    :: name
INTEGER                   :: nlgn

TYPE(DUXML)               :: blocDUxml
CHARACTER(LEN=lenpath)    :: href
INTEGER                   :: naxe            ! Nombre d'axes auquelles les quantites sont associees
TYPE(DUAXE)               :: descaxe(naxmax) ! Description des axes
CHARACTER(LEN=lentw)      :: type_write

REAL(KIND=dp)             :: angle_deg       ! Angle in degrees
CHARACTER(LEN=lenstrint)  :: s_i
CHARACTER(LEN=5)          :: strdble         ! Angle stored as a string

     !-----------------------------------------------------------------------
     !=== Observer side - Incident spectra
     !-----------------------------------------------------------------------
     numhdu = numhdu + 1
     nlgn   = npixlam
     ncol   = nmui ! number of angles

     ALLOCATE (descol(ncol))
     ALLOCATE (rval2D(nlgn,ncol))

     name    = "Incident spectra observer side - angles"
     extname = "DU_SPECTRA_ANG_INCI_OS"
     descol(:)%form    = "1D"
     descol(:)%unite   = "erg cm-2 s-1 str-1 Ang-1"
     descol(:)%datatyp = "double"
     descol(:)%ucd     = ""
     descol(:)%utype   = ""
     descol(:)%flag_desc = 1

     j = 0
     DO i = -1, -nmui, -1 ! angle
        j = j + 1
        angle_deg = ACOS(xmui_M(-i)) * 180.0_dp / xpi
        ! We create a string from the integer i to define the ID
        CALL INT_STR(i,s_i)
        WRITE(strdble,"(F5.2)") angle_deg
        descol(j)%titre   = "Incident spec. intensity - Obs. side:&
                            & "//TRIM(ADJUSTL(strdble))//" deg"
        descol(j)%IDFD    = "FD_SPECTRA_INCI_OS_"//TRIM(ADJUSTL(s_i))
        descol(j)%descrip = "<![CDATA[Spectrum of the incident &
                            &specific intensity on the observer &
                            &side of the cloud seen on an angle of " &
                          //TRIM(ADJUSTL(strdble))//" degrees<br/>&
                               &[erg cm-2 s-1 str-1 Ang-1]]]>"
        DO k = 1, nlgn ! wavelength
           rval2D(k,j) = I_esc_0(k-1,i) ! with i < 0
        ENDDO
     ENDDO

     type_write="double2D"
     CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

     !-------------------------------------------------------
     !--- Creation du XML --------------
     !-------------------------------------------------------
     !--- Preparation of the STREAM for VO-TABLE
     href = "file///./fits/"//filefits
     href = TRIM(ADJUSTL(href))
     blocDUxml%flag_stream = 1
     blocDUxml%extnum      = 0    ! It is not used in our case
     blocDUxml%encoding    = ""
     blocDUxml%href        = href

     blocDUxml%nameDU   = name
     blocDUxml%IDDU     = extname
     blocDUxml%nrows    = nlgn
     blocDUxml%ncol     = ncol
     blocDUxml%CNTIDDU  = "CNT_SPECTRA"
     blocDUxml%modeDU   = "USER"
     blocDUxml%flag_desc = 0
     blocDUxml%descripDU = ""
     naxe = 1
     descaxe(1)%name = '"Wavelength"'
     descaxe(1)%ID   = '"DU_WAVELENGTH.FD_WAVELENGTH"'
     CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

     deallocate(descol)
     deallocate(rval2D)

     !-----------------------------------------------------------------------
     !=== Observer side - Emergent spectra
     !-----------------------------------------------------------------------
     numhdu = numhdu + 1
     nlgn   = npixlam
     ncol   = nmui ! number of angles

     ALLOCATE (descol(ncol))
     ALLOCATE (rval2D(nlgn,ncol))

     name    = "Emergent spectra observer side - angles"
     extname = "DU_SPECTRA_ANG_EMER_OS"
     descol(:)%form    = "1D"
     descol(:)%unite   = "erg cm-2 s-1 str-1 Ang-1"
     descol(:)%datatyp = "double"
     descol(:)%ucd     = ""
     descol(:)%utype   = ""
     descol(:)%flag_desc = 1

     j = 0
     DO i = 1, nmui ! angle
        j = j + 1
        angle_deg = ACOS(xmui_M(i)) * 180.0_dp / xpi
        ! We create a string from the integer i to define the ID
        CALL INT_STR(i,s_i)
        WRITE(strdble,"(F5.2)") angle_deg
        descol(j)%titre   = "Emergent spec. intensity - Obs. side:&
                            & "//TRIM(ADJUSTL(strdble))//" deg"
        descol(j)%IDFD    = "FD_SPECTRA_EMER_OS_"//TRIM(ADJUSTL(s_i))
        descol(j)%descrip = "<![CDATA[Spectrum of the emergent &
                            &specific intensity on the observer &
                            &side of the cloud seen on an angle of " &
                          //TRIM(ADJUSTL(strdble))//" degrees<br/>&
                               &[erg cm-2 s-1 str-1 Ang-1]]]>"
        DO k = 1, nlgn ! wavelength
           rval2D(k,j) = I_esc_0(k-1,i) ! with i > 0
        ENDDO
     ENDDO

     type_write="double2D"
     CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

     !-------------------------------------------------------
     !--- Creation du XML --------------
     !-------------------------------------------------------
     !--- Preparation of the STREAM for VO-TABLE
     href = "file///./fits/"//filefits
     href = TRIM(ADJUSTL(href))
     blocDUxml%flag_stream = 1
     blocDUxml%extnum      = 0    ! It is not used in our case
     blocDUxml%encoding    = ""
     blocDUxml%href        = href

     blocDUxml%nameDU   = name
     blocDUxml%IDDU     = extname
     blocDUxml%nrows    = nlgn
     blocDUxml%ncol     = ncol
     blocDUxml%CNTIDDU  = "CNT_SPECTRA"
     blocDUxml%modeDU   = "USER"
     blocDUxml%flag_desc = 0
     blocDUxml%descripDU = ""
     naxe = 1
     descaxe(1)%name = '"Wavelength"'
     descaxe(1)%ID   = '"DU_WAVELENGTH.FD_WAVELENGTH"'
     CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

     deallocate(descol)
     deallocate(rval2D)

     IF (isoneside .NE. 1) THEN

     !-----------------------------------------------------------------------
     !=== Back side - Incident spectra
     !-----------------------------------------------------------------------
     numhdu = numhdu + 1
     nlgn   = npixlam
     ncol   = nmui ! number of angles

     ALLOCATE (descol(ncol))
     ALLOCATE (rval2D(nlgn,ncol))

     name    = "Incident spectra back side - angles"
     extname = "DU_SPECTRA_ANG_INCI_BS"
     descol(:)%form    = "1D"
     descol(:)%unite   = "erg cm-2 s-1 str-1 Ang-1"
     descol(:)%datatyp = "double"
     descol(:)%ucd     = ""
     descol(:)%utype   = ""
     descol(:)%flag_desc = 1

     j = 0
     DO i = 1, nmui ! angle
        j = j + 1
        angle_deg = ACOS(xmui_M(i)) * 180.0_dp / xpi
        ! We create a string from the integer i to define the ID
        CALL INT_STR(i,s_i)
        WRITE(strdble,"(F5.2)") angle_deg
        descol(j)%titre   = "Incident spec. intensity - Obs. side:&
                            & "//TRIM(ADJUSTL(strdble))//" deg"
        descol(j)%IDFD    = "FD_SPECTRA_INCI_BS_"//TRIM(ADJUSTL(s_i))
        descol(j)%descrip = "<![CDATA[Spectrum of the incident &
                            &specific intensity on the back side &
                            &of the cloud seen on an angle of " &
                          //TRIM(ADJUSTL(strdble))//" degrees<br/>&
                               &[erg cm-2 s-1 str-1 Ang-1]]]>"
        DO k = 1, nlgn ! wavelength
           rval2D(k,j) = I_esc_max(k-1,i) ! with i > 0
        ENDDO
     ENDDO

     type_write="double2D"
     CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

     !-------------------------------------------------------
     !--- Creation du XML --------------
     !-------------------------------------------------------
     !--- Preparation of the STREAM for VO-TABLE
     href = "file///./fits/"//filefits
     href = TRIM(ADJUSTL(href))
     blocDUxml%flag_stream = 1
     blocDUxml%extnum      = 0    ! It is not used in our case
     blocDUxml%encoding    = ""
     blocDUxml%href        = href

     blocDUxml%nameDU   = name
     blocDUxml%IDDU     = extname
     blocDUxml%nrows    = nlgn
     blocDUxml%ncol     = ncol
     blocDUxml%CNTIDDU  = "CNT_SPECTRA"
     blocDUxml%modeDU   = "USER"
     blocDUxml%flag_desc = 0
     blocDUxml%descripDU = ""
     naxe = 1
     descaxe(1)%name = '"Wavelength"'
     descaxe(1)%ID   = '"DU_WAVELENGTH.FD_WAVELENGTH"'
     CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

     deallocate(descol)
     deallocate(rval2D)

     !-----------------------------------------------------------------------
     !=== Back side - Emergent spectra
     !-----------------------------------------------------------------------

     numhdu = numhdu + 1
     nlgn   = npixlam
     ncol   = nmui ! number of angles

     ALLOCATE (descol(ncol))
     ALLOCATE (rval2D(nlgn,ncol))

     name    = "Emergent spectra back side - angles"
     extname = "DU_SPECTRA_ANG_EMER_BS"
     descol(:)%form    = "1D"
     descol(:)%unite   = "erg cm-2 s-1 str-1 Ang-1"
     descol(:)%datatyp = "double"
     descol(:)%ucd     = ""
     descol(:)%utype   = ""
     descol(:)%flag_desc = 1

     j = 0
     DO i = -1, -nmui, -1 ! angle
        j = j + 1
        angle_deg = ACOS(xmui_M(-i)) * 180.0_dp / xpi
        ! We create a string from the integer i to define the ID
        CALL INT_STR(i,s_i)
        WRITE(strdble,"(F5.2)") angle_deg
        descol(j)%titre   = "Emergent spec. intensity - Back side:&
                            & "//TRIM(ADJUSTL(strdble))//" deg"
        descol(j)%IDFD    = "FD_SPECTRA_EMER_BS_"//TRIM(ADJUSTL(s_i))
        descol(j)%descrip = "<![CDATA[Spectrum of the emergent &
                            &specific intensity on the back side &
                            &of the cloud seen on an angle of " &
                          //TRIM(ADJUSTL(strdble))//" degrees<br/>&
                               &[erg cm-2 s-1 str-1 Ang-1]]]>"
        DO k = 1, nlgn ! wavelength
           rval2D(k,j) = I_esc_max(k-1,i) ! with i < 0
        ENDDO
     ENDDO

     type_write="double2D"
     CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

     !-------------------------------------------------------
     !--- Creation du XML --------------
     !-------------------------------------------------------
     !--- Preparation of the STREAM for VO-TABLE
     href = "file///./fits/"//filefits
     href = TRIM(ADJUSTL(href))
     blocDUxml%flag_stream = 1
     blocDUxml%extnum      = 0    ! It is not used in our case
     blocDUxml%encoding    = ""
     blocDUxml%href        = href

     blocDUxml%nameDU   = name
     blocDUxml%IDDU     = extname
     blocDUxml%nrows    = nlgn
     blocDUxml%ncol     = ncol
     blocDUxml%CNTIDDU  = "CNT_SPECTRA"
     blocDUxml%modeDU   = "USER"
     blocDUxml%flag_desc = 0
     blocDUxml%descripDU = ""
     naxe = 1
     descaxe(1)%name = '"Wavelength"'
     descaxe(1)%ID   = '"DU_WAVELENGTH.FD_WAVELENGTH"'
     CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

     deallocate(descol)
     deallocate(rval2D)

     ENDIF ! TEST IF ON ISONESIDE

END SUBROUTINE WRITE_SPECTRA_ANG

!!!==============================================================================
!!! SUBROUTINE WRITE_RADFIELD : Fichiers .rf
!!!
!!! 1 - Read the .rf file
!!  2 - Construct the corresponding .fits file.
!!!==============================================================================
SUBROUTINE WRITE_RADFIELD(fichradfield, specfitsname, unitxml)
IMPLICIT NONE
CHARACTER(LEN=lenfilename), INTENT(IN) :: fichradfield
CHARACTER(LEN=lenfilename), INTENT(IN) :: specfitsname
INTEGER,                    INTENT(IN) :: unitxml

INTEGER       :: npixlam                 ! Number of points in lambda
INTEGER       :: nAV                     ! Number of points in Av
REAL(KIND=DP) :: avdum, wldum            ! Values of Av. Not used since they are
                                         ! Known from binary file
REAL(KIND=DP), ALLOCATABLE :: lamrf(:)   ! Wavelength in .rf file
REAL(KIND=DP), ALLOCATABLE :: AV(:)      ! Av
REAL(KIND=DP), ALLOCATABLE :: urf(:,:)   ! Energy density

INTEGER       :: i, j, k

CHARACTER(LEN=lenID)      :: extname
CHARACTER(LEN=lenname)    :: name
INTEGER                   :: numhdu
INTEGER                   :: nlgn
INTEGER                   :: ncol
CHARACTER(LEN=lentw)      :: type_write

TYPE(DUXML)               :: blocDUxml
CHARACTER(LEN=lenpath)    :: href
INTEGER                   :: naxe ! Nombre d'axes auquelles les quantites sont associees
TYPE(DUAXE)               :: descaxe(naxmax) ! Description des axes

    !-------------------------------------------------------------------------
    ! READ DATA IN .rf FILE
    !-------------------------------------------------------------------------
    !--- Ouverture du fichier .rf ----------------------------------------
    PRINT *,"Lecture de :",fichradfield
    OPEN(44,FILE=fichradfield,STATUS="old",FORM="unformatted")
    !--- Lecture du nombre de pixels en lambda
    READ(44) npixlam                              ! number of pixels in lambda
    PRINT *,"Number of pixels :",npixlam

    ALLOCATE(lamrf(npixlam))
    !--- Read table of wavelength
    READ(44) lamrf                                ! wavelength in the table
    PRINT *,"Lam min (Ang): ",lamrf(1)
    PRINT *,"Lam max (Ang): ",lamrf(npixlam-1)

    !--- Get number of position in Av
    nAV = 0
    DO
      READ(44,END=144) avdum,wldum
      nAV = nAV + 1
    ENDDO
144 CONTINUE
    PRINT *,"Number of points in Av :",nAV
    ALLOCATE(urf(nAV,npixlam))
    ALLOCATE(av(nAV))

    !--- Rewind
    REWIND(44)
    READ(44) npixlam
    READ(44) lamrf

    !--- Read data
    DO i = 1, nAV
       READ(44) AV(i), urf(i,:)
    ENDDO
    CLOSE(44)

    !------------------------------------------------------------------------
    ! PREPARATION OF XML INFORMATIONS
    !------------------------------------------------------------------------
     href = "file///./fits/"//specfitsname
     href = TRIM(ADJUSTL(href))
     blocDUxml%flag_stream = 1
     blocDUxml%extnum      = 0    ! It is not used in our case
     blocDUxml%encoding    = ""
     blocDUxml%href        = href

     !------------------------------------------------------------------------
     ! INITIALISATION OF FITS FILE
     !------------------------------------------------------------------------
     !PRINT *,"Initialisation de l'image bidon"
     call iniimage(specfitsname)
     !PRINT *,"Fin de l'initialisation de l'image bidon "

     !------------------------------------------------------------------------
     ! WRITE DATA FOR SPECTRA IN FITS FILE
     !------------------------------------------------------------------------
     numhdu = 2
     nlgn   = nAV * npixlam ! We put everythig in one column, with lambda vary first
     ! LIMIT DANS LA FITS A 1567314 lignes
     IF (nlgn .GT. 1000000) THEN
        PRINT *,"ATTENTION PROBLEME DE TAILLE DE FICHIERS"
        nlgn = 1000000
     ENDIF
     ncol   = 1          ! On empile tous les flux dans une colonne
     allocate(descol(ncol))
     allocate(rval2D(nlgn,ncol))

     name    = "Energy density"
     extname = "DU_RADFIELD_ENERDENS"

     !--- Give info on columns
     descol(1)%titre   = "Energy density"
     descol(1)%IDFD    = "FD_RADFIELD_ENERDENS"
     descol(1)%form    = "1D"
     descol(1)%unite   = "erg cm-3"
     descol(1)%datatyp = "double"
     descol(1)%ucd     = ""
     descol(1)%utype   = ""

     k = 0
     DO j = 1, npixlam         ! pixels in lambda
         DO i = 1, nAV      ! Each column correspond to one position
            k = k + 1
            ! LIMIT DANS LA FITS A 1567314 lignes
            !IF (k .LE. 1567314) THEN
            IF (k .LE. 1000000) THEN
            ! rval2D(k,1) = urf(i,j)
            ENDIF
         ENDDO
     ENDDO

     !PRINT *,"Ecriture dans le fichier fits"
     type_write = "double2D"
     CALL WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

     !-------------------------------------------------------
     !--- Creation du XML --------------
     !-------------------------------------------------------
     PRINT *,"Ecriture des informations dans XML"
     blocDUxml%nameDU   = name
     blocDUxml%IDDU     = extname
     blocDUxml%nrows    = nlgn
     blocDUxml%ncol     = ncol
     blocDUxml%CNTIDDU  = "CNT_SPECTRA"
     blocDUxml%modeDU   = "USER"
     blocDUxml%flag_desc = 0
     blocDUxml%descripDU = ""
     naxe = 2 ! 2 axes Av et lambda
     descaxe(1)%name = '"Optical depth"'
     descaxe(1)%ID   = '"DU_OPTDEPTH.FD_OPTDEPTH"'
     descaxe(2)%name = '"Wavelength"'
     descaxe(2)%ID   = '"DU_WAVELENGTH.FD_WAVELENGTH"'
     CALL WRITE_DUXML(unitxml,blocDUxml,ncol,naxe,descaxe,0)

     deallocate(descol)
     deallocate(rval2D)

     DEALLOCATE(lamrf)
     DEALLOCATE(urf)
     DEALLOCATE(av)

END SUBROUTINE WRITE_RADFIELD

END MODULE PXDR_OUTFITS
