
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

! JLB - 24 May 2008

MODULE PREP_CHOFRO

  PRIVATE

  PUBLIC :: CHOFRO

CONTAINS

!%%%%%%%%%%%%%%%%
SUBROUTINE CHOFRO
!%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_STR_DATA
   USE PREP_VAR

   IMPLICIT NONE

   INTEGER :: ii2, ii3

!--------+---------+---------+---------+---------+---------+---------+-*-------+

   PRINT *
   ii2 = 0
   DO WHILE (ii2 /= -1)
      PRINT *, 'Heating and Cooling processes'
      PRINT *, '   1: Pure Heating'
      PRINT *, '   2: Pure Cooloing'
      PRINT *, '   3: Mixt Processes mixtes (H2, gaz-grains, ...)'
      PRINT *, '   -1: End'
      READ *, ii2
      IF (irec == 1) THEN
         WRITE (8,*) ii2
      ENDIF

      IF (ii2 == -1) THEN
         EXIT
      ENDIF

      IF (ii2 == 1) THEN

         ii3 = 0
         DO WHILE (ii3 /= -1)
            PRINT *, '     1: (Unused),          2: Photons on grains'
            PRINT *, '     3: H2 on grains,      4: Photons on gas'
            PRINT *, '     5: Cosmic Rays,       6: Secondary Photons'
            PRINT *, '     7: Chemical Equil.,,  8: Turbulent Heating'
            PRINT *, '     9: Total Heating,    -1: End'
            READ *, ii3
            IF (irec == 1) THEN
               WRITE (8,*) ii3
            ENDIF

            IF (ii3 == -1) THEN
               CYCLE
            ENDIF

            IF (ii3 == 1) THEN
               PRINT *, "Unused !"
            ELSE IF (ii3 == 2) THEN
               iimax = iimax + 1
               y(iimax,0:npo) = chophg(0:npo)
               texty(iimax) = 'Cho.Ph.Gr     '
            ELSE IF (ii3 == 3) THEN
               iimax = iimax + 1
               y(iimax,0:npo) = choh2g(0:npo)
               texty(iimax) = 'Cho.H2.Gr     '
            ELSE IF (ii3 == 4) THEN
               iimax = iimax + 1
               y(iimax,0:npo) = chopht(0:npo)
               texty(iimax) = 'Cho.photons   '
            ELSE IF (ii3 == 5) THEN
               iimax = iimax + 1
               y(iimax,0:npo) = chorc(0:npo)
               texty(iimax) = 'Cho.RC        '
            ELSE IF (ii3 == 6) THEN
               iimax = iimax + 1
               y(iimax,0:npo) = chophs(0:npo)
               texty(iimax) = 'Cho.ph.sec    '
            ELSE IF (ii3 == 7) THEN
               iimax = iimax + 1
               y(iimax,0:npo) = chochi(0:npo)
               texty(iimax) = 'Cho.eq.chi    '
            ELSE IF (ii3 == 8) THEN
               iimax = iimax + 1
               y(iimax,0:npo) = chotur(0:npo)
               texty(iimax) = 'Cho.turb      '
            ELSE IF (ii3 == 9) THEN
               iimax = iimax + 1
               y(iimax,0:npo) = chotot(0:npo)
               texty(iimax) = 'Cho.total     '
            ELSE
               PRINT *, '     WARNING: Illegal value:', ii3
            ENDIF
         ENDDO

      ELSE IF (ii2 == 2) THEN

         ii3 = 0
         DO WHILE (ii3 /= -1)
            PRINT *, '  1: C I,   2: N I,    3: O I,   4: S I,   5: Si I'
            PRINT *, '  6: C II,  7: N II,   8: O II,  9: S II, 10: Si II'
            PRINT *, ' 11: CO,   12: 13CO,  13: C18O, 14: 13C18O'
            PRINT *, ' 15: OH,   16: H2O,   17: O2,   18: CS,   19: HCN'
            PRINT *, ' 20: HD,   21: H3+,   22: HCO+, 23: CH+,  24: Fe+'
            PRINT *, ' 25: Total Cooling'
            PRINT *, ' -1: End'
            READ *, ii3
            IF (irec == 1) THEN
               WRITE (8,*) ii3
            ENDIF

            IF (ii3 < 1 .OR. ii3 > 25) THEN
               EXIT
            ENDIF

            IF (ii3 == 1) THEN
               IF (i_c /= 0) THEN
                  IF (in_sp(i_c) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_c))%ref(0:npo)
                     texty(iimax) = 'Ref.C.tot     '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 2) THEN
               IF (i_n /= 0) THEN
                  IF (in_sp(i_n) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_n))%ref(0:npo)
                     texty(iimax) = 'Ref.N.tot     '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 3) THEN
               IF (i_o /= 0) THEN
                  IF (in_sp(i_o) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_o))%ref(0:npo)
                     texty(iimax) = 'Ref.O.tot     '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 4) THEN
               IF (i_s /= 0) THEN
                  IF (in_sp(i_s) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_s))%ref(0:npo)
                     texty(iimax) = 'Ref.S.tot     '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 5) THEN
               IF (i_si /= 0) THEN
                  IF (in_sp(i_si) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_si))%ref(0:npo)
                     texty(iimax) = 'Ref.Si.tot    '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 6) THEN
               IF (i_cp /= 0) THEN
                  IF (in_sp(i_cp) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_cp))%ref(0:npo)
                     texty(iimax) = 'Ref.C+.tot    '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 7) THEN
               IF (i_np /= 0) THEN
                  IF (in_sp(i_np) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_np))%ref(0:npo)
                     texty(iimax) = 'Ref.N+.tot    '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 8) THEN
               IF (i_op /= 0) THEN
                  IF (in_sp(i_op) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_op))%ref(0:npo)
                     texty(iimax) = 'Ref.O+.tot    '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 9) THEN
               IF (i_sp /= 0) THEN
                  IF (in_sp(i_sp) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_sp))%ref(0:npo)
                     texty(iimax) = 'Ref.S+.tot    '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 10) THEN
               IF (i_sip /= 0) THEN
                  IF (in_sp(i_sip) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_sip))%ref(0:npo)
                     texty(iimax) = 'Ref.Si+.tot   '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 11) THEN
               IF (i_co /= 0) THEN
                  IF (in_sp(i_co) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_co))%ref(0:npo)
                     texty(iimax) = 'Ref.CO.tot    '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 12) THEN
               IF (j_c13o /= 0) THEN
                  IF (in_sp(j_c13o) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(j_c13o))%ref(0:npo)
                     texty(iimax) = 'Ref.C*O.tot   '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 13) THEN
               IF (j_co18 /= 0) THEN
                  IF (in_sp(j_co18) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(j_co18))%ref(0:npo)
                     texty(iimax) = 'Ref.CO*.tot   '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 14) THEN
               IF (j_c13o18 /= 0) THEN
                  IF (in_sp(j_c13o18) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(j_c13o18))%ref(0:npo)
                     texty(iimax) = 'Rf.C*O*.tot   '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 15) THEN
               IF (i_oh /= 0) THEN
                  IF (in_sp(i_oh) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_oh))%ref(0:npo)
                     texty(iimax) = 'Ref.OH.tot   '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 16) THEN
               IF (i_h2o /= 0) THEN
                  IF (in_sp(i_h2o) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_h2o))%ref(0:npo)
                     texty(iimax) = 'Ref.H2O.tot   '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 17) THEN
               IF (i_o2 /= 0) THEN
                  IF (in_sp(i_o2) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_o2))%ref(0:npo)
                     texty(iimax) = 'Ref.O2.tot  '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 18) THEN
               IF (i_cs /= 0) THEN
                  IF (in_sp(i_cs) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_cs))%ref(0:npo)
                     texty(iimax) = 'Ref.CS.tot    '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 19) THEN
               IF (i_hcn /= 0) THEN
                  IF (in_sp(i_hcn) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_hcn))%ref(0:npo)
                     texty(iimax) = 'Ref.HCN.tot  '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 20) THEN
               IF (i_hd /= 0) THEN
                  IF (in_sp(i_hd) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_hd))%ref(0:npo)
                     texty(iimax) = 'Ref.HD.tot  '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 21) THEN
               IF (i_h3p /= 0) THEN
                  IF (in_sp(i_h3p) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_h3p))%ref(0:npo)
                     texty(iimax) = 'Ref.H3+.tot   '
                 ENDIF
               ENDIF
            ELSE IF (ii3 == 22) THEN
               IF (i_hcop /= 0) THEN
                  IF (in_sp(i_hcop) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_hcop))%ref(0:npo)
                     texty(iimax) = 'Ref.HCO+.tot  '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 23) THEN
               IF (i_chp /= 0) THEN
                  IF (in_sp(i_chp) /= 0) THEN
                     iimax = iimax + 1
                     y(iimax,0:npo) = spec_lin(in_sp(i_chp))%ref(0:npo)
                     texty(iimax) = 'Ref.CH+.tot  '
                  ENDIF
               ENDIF
            ELSE IF (ii3 == 24) THEN
               iimax = iimax + 1
               y(iimax,0:npo) = reffep(0:npo)
               texty(iimax) = 'Ref.Fe+.tot   '

            ELSE IF (ii3 == 25) THEN
               iimax = iimax + 1
               y(iimax,0:npo) = reftot(0:npo)
               texty(iimax) = 'Ref.total     '
            ELSE
                PRINT *, '     ****  WARNING: valeur impossible!  ****'
                CYCLE
            ENDIF
         ENDDO

      ELSE IF (ii2 == 3) THEN

         ii3 = 0
         DO WHILE (ii3 /= -1)
         PRINT *, '     1: H2,    2: gaz-grains coupling'
         PRINT *, '     -1: End'
         READ *, ii3
         IF (irec == 1) THEN
            WRITE (8,*) ii3
         ENDIF

         IF (ii3 == -1) THEN
            CYCLE
         ENDIF

         IF (ii3 == 1) THEN
            iimax = iimax + 1
            y(iimax,0:npo) = MAX(0.0_dp,-spec_lin(in_sp(i_h2))%ref(0:npo))
            texty(iimax) = 'Cho.casc.H2   '

            iimax = iimax + 1
            y(iimax,0:npo) = MAX(0.0_dp,spec_lin(in_sp(i_h2))%ref(0:npo))
            texty(iimax) = 'Ref.casc.H2   '
         ELSE IF (ii3 == 2) THEN
            iimax = iimax + 1
            y(iimax,0:npo) = MAX(0.0_dp,choeqg(0:npo))
            texty(iimax) = 'Cho.gas.gr    '

            iimax = iimax + 1
            y(iimax,0:npo) = MAX(0.0_dp,-choeqg(0:npo))
            texty(iimax) = 'Ref.gas.gr    '
         ELSE
           PRINT *, '   WARNING: Illegal value'
           CYCLE
         ENDIF
         ENDDO

      ELSE
          PRINT *, '   WARNING: Illegal value'
          CYCLE
      ENDIF
   ENDDO

END SUBROUTINE CHOFRO

END MODULE PREP_CHOFRO
