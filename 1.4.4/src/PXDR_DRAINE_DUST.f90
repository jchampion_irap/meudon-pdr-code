
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

MODULE PXDR_DRAINE_DUST

USE PXDR_CONSTANTES

IMPLICIT NONE

   ! Draine's files give log grids for Q_abs, Q_scat, g. Change the following PARAMETERs if you change file.
   INTEGER, PARAMETER, PUBLIC                            :: nptmax=85            ! Number of radius pts max
   INTEGER, PARAMETER, PUBLIC                            :: npwmax=241           ! Number of wlgth pts max

   ! Grains data base. Read in READ_DRAINE_FILES
   INTEGER, PUBLIC                                       :: np_r                 ! Number of radius pts READ
   INTEGER, PUBLIC                                       :: np_w                 ! Number of wlgth pts READ

   REAL (KIND=dp), PUBLIC                                :: r_min, r_max
   REAL (KIND=dp), PUBLIC, DIMENSION (nptmax)            :: radgr
   REAL (KIND=dp), PUBLIC, DIMENSION (npwmax+1)          :: wavgrd
   INTEGER, PUBLIC                                       :: Jwlmin_gr, jwlmax_gr ! range of wl used in wavgrd

   ! Optical properties
   REAL (KIND=dp), PUBLIC, DIMENSION (nptmax,npwmax+1)   :: Qabs_Gra, Qsca_Gra, g_Gra
   REAL (KIND=dp), PUBLIC, DIMENSION (nptmax,npwmax+1)   :: Qabs_Sil, Qsca_Sil, g_Sil
   REAL (KIND=dp), PUBLIC, DIMENSION (:,:), ALLOCATABLE  :: Qabs_Dr, Qsca_Dr, g_Dr

CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     READING DATA FILES GIVEN BY DRAINE FOR GRAPHITE AND SILICATES
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE READ_DRAINE_FILES
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  USE PXDR_CONSTANTES

  IMPLICIT NONE

  INTEGER                   :: i, j
  REAL (KIND=dp)            :: toto


!---------------------------------------------------------------------
!             READING  GRAPHITE and SILICATE IN FILE:  gr_GS_85
!---------------------------------------------------------------------

  WRITE (iscrn,*) "    * Read grains data from Draine's datafiles "//TRIM(fichier)

  fichier = TRIM(data_dir)//'Grains/gr_GS_85.dat'
  OPEN (iwrtmp, file=fichier, status='old')

  READ (iwrtmp,'(3/,i4,2e10.3)') np_r, r_min, r_max
  READ (iwrtmp,*)                np_w

  IF (np_r /= nptmax) THEN
     PRINT *, " !!! WRONG number of radii !!!"
     STOP
  ENDIF
  IF (np_w /= npwmax) THEN
     PRINT *, " !!! WRONG number of wavelengths !!!"
     STOP
  ENDIF

  DO i = 1, np_r
     READ (iwrtmp,*) radgr(i)
     READ (iwrtmp,*)
     DO j = np_w, 1, -1
        READ (iwrtmp,*) wavgrd(j), Qabs_Gra(i,j), Qsca_Gra(i,j), g_Gra(i,j) &
                 , toto, Qabs_Sil(i,j), Qsca_Sil(i,j), g_Sil(i,j)
        IF (toto /= wavgrd(j)) THEN
           PRINT *, " pb lecture!"
        ENDIF
     ENDDO
  ENDDO
  CLOSE(iwrtmp)

  radgr(:) = radgr(:) * 1.0e-4_dp                               ! Convert from micrometer to cm
  wavgrd(:) = wavgrd(:) * 1.0e4_dp                              ! Convert from micrometer to Angstrom

END SUBROUTINE READ_DRAINE_FILES

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
REAL(KIND=dp)  FUNCTION Q_inter(Qx,x,a,i0)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  IMPLICIT NONE

  REAL(KIND=dp), DIMENSION(:), INTENT (IN)               :: Qx
  REAL(KIND=dp), DIMENSION(:), INTENT (IN)               :: x
  REAL(KIND=dp), INTENT (IN)                             :: a
  INTEGER, INTENT (IN)                                   :: i0

! Bug - found by Marcellino - 28 II 2012
! Q_inter = Qx(i0) + (Qx(i0+1)-Qx(i0)) * ((x(i0+1)-a) / (x(i0+1)-x(i0)))
  Q_inter = Qx(i0) + (Qx(i0+1)-Qx(i0)) * ((a-x(i0)) / (x(i0+1)-x(i0)))

END FUNCTION Q_inter

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE MIXGRSI (wlmin, wlmax)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR

   IMPLICIT NONE

   REAL (KIND=dp), INTENT (IN)        :: wlmin, wlmax
   INTEGER                            :: jmin, jmax
   REAL (KIND=dp)                     :: Qg1, Qg2, Qs1, Qs2
   INTEGER                            :: i0, i, j, mm
   REAL (KIND=dp)                     :: pente

   WRITE (iscrn,*) "    * Mix grains properties"
   ! Find range in wavelength grid
   jmin = 0
   jmax = 0
   DO i = 1, np_w-1
      IF (wavgrd(i) <= wlmin .AND. wavgrd(i+1) > wlmin) THEN
         jmin = i
      ENDIF
      IF (wavgrd(i) <= wlmax .AND. wavgrd(i+1) > wlmax) THEN
         jmax = i
         EXIT
      ENDIF
   ENDDO
   IF (jmax == 0) THEN
      jmax = np_w
      wavgrd(np_w+1) = wlmax
      pente = LOG(Qabs_Gra(1,np_w)/Qabs_gra(1,np_w-1)) / LOG(wavgrd(np_w)/wavgrd(np_w-1))
      Qabs_Gra(:,np_w+1) = Qabs_Gra(:,np_w) * (wlmax/wavgrd(np_w))**pente
      pente = LOG(Qsca_Gra(1,np_w)/Qsca_Gra(1,np_w-1)) / LOG(wavgrd(np_w)/wavgrd(np_w-1))
      Qsca_Gra(:,np_w+1) = Qsca_Gra(:,np_w) * (wlmax/wavgrd(np_w))**pente
      g_Gra(:,np_w+1) = g_Gra(:,np_w)
      pente = LOG(Qabs_Sil(1,np_w)/Qabs_Sil(1,np_w-1)) / LOG(wavgrd(np_w)/wavgrd(np_w-1))
      Qabs_Sil(:,np_w+1) = Qabs_Sil(:,np_w) * (wlmax/wavgrd(np_w))**pente
      pente = LOG(Qsca_Sil(1,np_w)/Qsca_Sil(1,np_w-1)) / LOG(wavgrd(np_w)/wavgrd(np_w-1))
      Qsca_Sil(:,np_w+1) = Qsca_Sil(:,np_w) * (wlmax/wavgrd(np_w))**pente
      g_Sil(:,np_w+1) = g_Sil(:,np_w)
   ENDIF

   ALLOCATE (Qabs_Dr(jmax-jmin+2,npg))
   ALLOCATE (Qsca_Dr(jmax-jmin+2,npg))
   ALLOCATE (g_Dr   (jmax-jmin+2,npg))
   Qabs_Dr = 0.0_dp
   Qsca_Dr = 0.0_dp
   g_Dr    = 0.0_dp

   DO mm = 1, npg
      IF (adust(mm) < radgr(1) .OR. adust(mm) > radgr(np_r)) THEN
         PRINT *, " Radius out of range:", radgr(1), " -", radgr(np_r)
         STOP
      ENDIF

      i0 = 1
      DO
         IF (radgr(i0+1) > adust(mm) .AND. radgr(i0) <= adust(mm)) THEN
            EXIT
         ENDIF
         i0 = i0 + 1
      ENDDO

      ! Absorption
      Qg1 = Q_inter(Qabs_Gra(:,jmin),radgr,adust(mm),i0)
      Qg2 = Q_inter(Qabs_Gra(:,jmin+1),radgr,adust(mm),i0)
      Qs1 = Q_inter(Qabs_Sil(:,jmin),radgr,adust(mm),i0)
      Qs2 = Q_inter(Qabs_Sil(:,jmin+1),radgr,adust(mm),i0)
      Qabs_Dr(1,mm) = (1.0_dp-amix) * (Qg1 + (Qg2-Qg1) * (wlmin-wavgrd(jmin)) / (wavgrd(jmin+1)-wavgrd(jmin))) &
                 +         amix  * (Qs1 + (Qs2-Qs1) * (wlmin-wavgrd(jmin)) / (wavgrd(jmin+1)-wavgrd(jmin)))

      DO j = jmin+1, jmax
         Qg1 = Q_inter(Qabs_Gra(:,j),radgr,adust(mm),i0)
         Qs1 = Q_inter(Qabs_Sil(:,j),radgr,adust(mm),i0)
         Qabs_Dr(j-jmin+1,mm) = (1.0_dp-amix) * Qg1 + amix * Qs1
      ENDDO

      Qg1 = Q_inter(Qabs_Gra(:,jmax),radgr,adust(mm),i0)
      Qg2 = Q_inter(Qabs_Gra(:,jmax+1),radgr,adust(mm),i0)
      Qs1 = Q_inter(Qabs_Sil(:,jmax),radgr,adust(mm),i0)
      Qs2 = Q_inter(Qabs_Sil(:,jmax+1),radgr,adust(mm),i0)
      Qabs_Dr(jmax-jmin+2,mm) = (1.0_dp-amix) * (Qg1 + (Qg2-Qg1) * (wlmax-wavgrd(jmax)) / (wavgrd(jmax+1)-wavgrd(jmax))) &
                           +         amix  * (Qs1 + (Qs2-Qs1) * (wlmax-wavgrd(jmax)) / (wavgrd(jmax+1)-wavgrd(jmax)))

      ! Scattering
      Qg1 = Q_inter(Qsca_Gra(:,jmin),radgr,adust(mm),i0)
      Qg2 = Q_inter(Qsca_Gra(:,jmin+1),radgr,adust(mm),i0)
      Qs1 = Q_inter(Qsca_Sil(:,jmin),radgr,adust(mm),i0)
      Qs2 = Q_inter(Qsca_Sil(:,jmin+1),radgr,adust(mm),i0)
      Qsca_Dr(1,mm) = (1.0_dp-amix) * (Qg1 + (Qg2-Qg1) * (wlmin-wavgrd(jmin)) / (wavgrd(jmin+1)-wavgrd(jmin))) &
                 +         amix  * (Qs1 + (Qs2-Qs1) * (wlmin-wavgrd(jmin)) / (wavgrd(jmin+1)-wavgrd(jmin)))

      DO j = jmin+1, jmax
         Qg1 = Q_inter(Qsca_Gra(:,j),radgr,adust(mm),i0)
         Qs1 = Q_inter(Qsca_Sil(:,j),radgr,adust(mm),i0)
         Qsca_Dr(j-jmin+1,mm) = (1.0_dp-amix) * Qg1 + amix * Qs1
      ENDDO

      Qg1 = Q_inter(Qsca_Gra(:,jmax),radgr,adust(mm),i0)
      Qg2 = Q_inter(Qsca_Gra(:,jmax+1),radgr,adust(mm),i0)
      Qs1 = Q_inter(Qsca_Sil(:,jmax),radgr,adust(mm),i0)
      Qs2 = Q_inter(Qsca_Sil(:,jmax+1),radgr,adust(mm),i0)
      Qsca_Dr(jmax-jmin+2,mm) = (1.0_dp-amix) * (Qg1 + (Qg2-Qg1) * (wlmax-wavgrd(jmax)) / (wavgrd(jmax+1)-wavgrd(jmax))) &
                           +         amix  * (Qs1 + (Qs2-Qs1) * (wlmax-wavgrd(jmax)) / (wavgrd(jmax+1)-wavgrd(jmax)))

      ! Anisotropy factor
      Qg1 = Q_inter(g_Gra(:,jmin),radgr,adust(mm),i0)
      Qg2 = Q_inter(g_Gra(:,jmin+1),radgr,adust(mm),i0)
      Qs1 = Q_inter(g_Sil(:,jmin),radgr,adust(mm),i0)
      Qs2 = Q_inter(g_Sil(:,jmin+1),radgr,adust(mm),i0)
      g_Dr(1,mm) = (1.0_dp-amix) * (Qg1 + (Qg2-Qg1) * (wlmin-wavgrd(jmin)) / (wavgrd(jmin+1)-wavgrd(jmin))) &
                 +         amix  * (Qs1 + (Qs2-Qs1) * (wlmin-wavgrd(jmin)) / (wavgrd(jmin+1)-wavgrd(jmin)))

      DO j = jmin+1, jmax
         Qg1 = Q_inter(g_Gra(:,j),radgr,adust(mm),i0)
         Qs1 = Q_inter(g_Sil(:,j),radgr,adust(mm),i0)
         g_Dr(j-jmin+1,mm) = (1.0_dp-amix) * Qg1 + amix * Qs1
      ENDDO

      Qg1 = Q_inter(g_Gra(:,jmax),radgr,adust(mm),i0)
      Qg2 = Q_inter(g_Gra(:,jmax+1),radgr,adust(mm),i0)
      Qs1 = Q_inter(g_Sil(:,jmax),radgr,adust(mm),i0)
      Qs2 = Q_inter(g_Sil(:,jmax+1),radgr,adust(mm),i0)
      g_Dr(jmax-jmin+2,mm) = (1.0_dp-amix) * (Qg1 + (Qg2-Qg1) * (wlmax-wavgrd(jmax)) / (wavgrd(jmax+1)-wavgrd(jmax))) &
                           +         amix  * (Qs1 + (Qs2-Qs1) * (wlmax-wavgrd(jmax)) / (wavgrd(jmax+1)-wavgrd(jmax)))

   ENDDO

   jwlmin_gr = jmin
   jwlmax_gr = jmax

END SUBROUTINE MIXGRSI

END MODULE PXDR_DRAINE_DUST

