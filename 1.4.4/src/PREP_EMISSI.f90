!
! PREP_EMISSI.f90 code modified by Jason Champion (IRAP)
!
!--------+---------+---------+---------+---------+---------+---------+---------+
!
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

! JLB - 24 May 2008

MODULE PREP_EMISSI

  PRIVATE

  PUBLIC :: EMISSI, VIEW_LINES, EMIINT, EMISSI_WITH_FILE, EMIINT_WITH_FILE

CONTAINS

!%%%%%%%%%%%%%%%%
SUBROUTINE EMISSI
!%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PREP_VAR
   USE PREP_INTEMI

   IMPLICIT NONE

   CHARACTER (LEN=8) :: yspec
   INTEGER           :: ii, ju, jl, jj
   INTEGER           :: iloc, i, ind_sp

!--------+---------+---------+---------+---------+---------+---------+-*-------+

   PRINT *, '   Specify which problem:'
   PRINT *, '   0: Local emissivities'
   PRINT *, '   1: Line intensities'
   READ *, iloc
   IF (irec == 1) THEN
      WRITE (8,*) iloc
   ENDIF

   IF (iloc == 1) THEN
      PRINT *, ' Compute integrated emissivities'
      CALL EMIINT
   ELSE IF (iloc == 0) THEN
      PRINT *, ' Compute local emissivities'
      PRINT *, " "
      PRINT *, " Possible species:"
      i = 1
      DO WHILE (i <= nspl)
         IF (MOD(i,3) /= 0) THEN
            WRITE (*,'(a10)',ADVANCE="NO") spec_lin(i)%nam
         ELSE
            WRITE (*,'(a10)') spec_lin(i)%nam
         ENDIF
         i = i + 1
      ENDDO
      WRITE (*,*)

      DO
         PRINT *
         PRINT *, ' Which species?  (end: -1)'
         READ (*,'(a8)') yspec
         yspec = TRIM(ADJUSTL(yspec))
         IF (irec == 1) THEN
            WRITE (8,'(a8)') yspec
         ENDIF
         IF (yspec == "-1") THEN
            PRINT *, "  End lines"
            EXIT
         ENDIF
         DO i = 1, nspec
            IF (yspec /= TRIM(ADJUSTL(speci(i)))) THEN
               CYCLE
            ELSE
               EXIT
            ENDIF
         ENDDO
         IF (i > nspec) THEN
            SELECT CASE (yspec)
            CASE ("c*o")
               i = nspec + 1
            CASE ("co*")
               i = nspec + 2
            CASE ("c*o*")
               i = nspec + 3
            CASE DEFAULT
               i = 0
            END SELECT
         ENDIF
         IF (i == 0 .AND. yspec /= '-1') THEN
            PRINT *, "  WARNING:", yspec, " is not in chemistry!"
            CYCLE
         ELSE IF (in_sp(i) == 0) THEN
            PRINT *, "  WARNING: excitation of ", yspec &
                   , " was not computed"
         ELSE
            ind_sp = in_sp(i)
         ENDIF

         ii = 999
         PRINT *, ' Emission lines of: ', yspec
         CALL VIEW_LINES(spec_lin(ind_sp), irec)
         DO WHILE (ii /= 0)
            PRINT *,"Enter line number (0 end)"
            READ *, ii
            IF (ii > spec_lin(ind_sp)%ntr) THEN
               PRINT *, ii, ' > ', spec_lin(ind_sp)%ntr, ' ! - END'
               ii = 0
               EXIT
            ENDIF
            IF (irec == 1) THEN
               WRITE (8,*) ii
            ENDIF

            IF (ii /= 0) THEN
               PRINT *, 'Collect line number:', ii
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%emi(ii,0:npo)
               texty(iimax) = 'EmL.'//TRIM(spec_lin(ind_sp)%nam)//'.'//TRIM(ADJUSTL(ii2c(ii)))
            ELSE
               PRINT *, 'End lines of: ', yspec
               EXIT
            ENDIF

            PRINT *, "Full line properties? (1: yes, 0: no)"
            READ *, jj
            IF (irec == 1) THEN
               WRITE (8,*) jj
            ENDIF
            IF (jj == 1) THEN
               ju = spec_lin(ind_sp)%iup(ii)
               jl = spec_lin(ind_sp)%jlo(ii)

               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%abu(0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.abun.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%vel(0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.vel.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%xre(ju,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.xreu.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%xre(jl,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.xrel.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%XXl(ju,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.XXlu.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%XXl(jl,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.XXlr.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%XXr(ju,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.XXru.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%XXr(jl,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.XXrl.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%odl(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.odl.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%odr(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.odr.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%dnu(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.dnu.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%emi(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.emi.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%pum(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.pum.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%jiD(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.jiD.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%bel(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.bel.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%ber(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.ber.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%exl(ii)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.exl.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%exr(ii)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.exr.'//TRIM(ADJUSTL(ii2c(ii)))
            ENDIF

         ENDDO
      ENDDO

   ENDIF

END SUBROUTINE EMISSI

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE EMISSI_WITH_FILE(iloc, fichout, xmu, yspectab, nsp, nlines, linesnum)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PREP_VAR
   USE PREP_INTEMI

   IMPLICIT NONE

   CHARACTER (LEN=8) :: yspec
   INTEGER           :: ii, ju, jl, jj
   INTEGER           :: i, ind_sp
   INTEGER, INTENT(IN) :: iloc
   CHARACTER(LEN=50), INTENT(INOUT) :: fichout
   REAL(KIND=dp), INTENT(INOUT) :: xmu
   CHARACTER(LEN=8), INTENT(IN) :: yspectab(10)
   INTEGER, INTENT(IN) :: nsp, nlines(10), linesnum(50)

!--------+---------+---------+---------+---------+---------+---------+-*-------+

   IF (iloc == 1) THEN
      PRINT *
      PRINT *, ' Compute integrated emissivities'
      CALL EMIINT_WITH_FILE(fichout, xmu, yspectab, nsp, nlines, linesnum)
   ELSE IF (iloc == 0) THEN
      PRINT *, ' Compute local emissivities'
      PRINT *, " "
      PRINT *, " Possible species:"
      i = 1
      DO WHILE (i <= nspl)
         IF (MOD(i,3) /= 0) THEN
            WRITE (*,'(a10)',ADVANCE="NO") spec_lin(i)%nam
         ELSE
            WRITE (*,'(a10)') spec_lin(i)%nam
         ENDIF
         i = i + 1
      ENDDO
      WRITE (*,*)

      DO
         PRINT *
         PRINT *, ' Which species?  (end: -1)'
         READ (*,'(a8)') yspec
         yspec = TRIM(ADJUSTL(yspec))
         IF (irec == 1) THEN
            WRITE (8,'(a8)') yspec
         ENDIF
         IF (yspec == "-1") THEN
            PRINT *, "  End lines"
            EXIT
         ENDIF
         DO i = 1, nspec
            IF (yspec /= TRIM(ADJUSTL(speci(i)))) THEN
               CYCLE
            ELSE
               EXIT
            ENDIF
         ENDDO
         IF (i > nspec) THEN
            SELECT CASE (yspec)
            CASE ("c*o")
               i = nspec + 1
            CASE ("co*")
               i = nspec + 2
            CASE ("c*o*")
               i = nspec + 3
            CASE DEFAULT
               i = 0
            END SELECT
         ENDIF
         IF (i == 0 .AND. yspec /= '-1') THEN
            PRINT *, "  WARNING:", yspec, " is not in chemistry!"
            CYCLE
         ELSE IF (in_sp(i) == 0) THEN
            PRINT *, "  WARNING: excitation of ", yspec &
                   , " was not computed"
         ELSE
            ind_sp = in_sp(i)
         ENDIF

         ii = 999
         PRINT *, ' Emission lines of: ', yspec
         CALL VIEW_LINES(spec_lin(ind_sp), irec)
         DO WHILE (ii /= 0)
            PRINT *,"Enter line number (0 end)"
            READ *, ii
            IF (ii > spec_lin(ind_sp)%ntr) THEN
               PRINT *, ii, ' > ', spec_lin(ind_sp)%ntr, ' ! - END'
               ii = 0
               EXIT
            ENDIF
            IF (irec == 1) THEN
               WRITE (8,*) ii
            ENDIF

            IF (ii /= 0) THEN
               PRINT *, 'Collect line number:', ii
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%emi(ii,0:npo)
               texty(iimax) = 'EmL.'//TRIM(spec_lin(ind_sp)%nam)//'.'//TRIM(ADJUSTL(ii2c(ii)))
            ELSE
               PRINT *, 'End lines of: ', yspec
               EXIT
            ENDIF

            PRINT *, "Full line properties? (1: yes, 0: no)"
            READ *, jj
            IF (irec == 1) THEN
               WRITE (8,*) jj
            ENDIF
            IF (jj == 1) THEN
               ju = spec_lin(ind_sp)%iup(ii)
               jl = spec_lin(ind_sp)%jlo(ii)

               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%abu(0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.abun.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%vel(0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.vel.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%xre(ju,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.xreu.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%xre(jl,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.xrel.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%XXl(ju,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.XXlu.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%XXl(jl,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.XXlr.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%XXr(ju,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.XXru.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%XXr(jl,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.XXrl.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%odl(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.odl.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%odr(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.odr.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%dnu(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.dnu.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%emi(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.emi.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%pum(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.pum.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%jiD(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.jiD.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%bel(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.bel.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%ber(ii,0:npo)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.ber.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%exl(ii)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.exl.'//TRIM(ADJUSTL(ii2c(ii)))
               iimax = iimax + 1
               y(iimax,0:npo) = spec_lin(ind_sp)%exr(ii)
               texty(iimax) = TRIM(spec_lin(ind_sp)%nam)//'.exr.'//TRIM(ADJUSTL(ii2c(ii)))
            ENDIF

         ENDDO
      ENDDO

   ENDIF

END SUBROUTINE EMISSI_WITH_FILE

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE VIEW_LINES(spelin, irec)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_STR_DATA

   IMPLICIT NONE
   TYPE(spectra), INTENT (IN) :: spelin
   INTEGER, INTENT (IN)       :: irec
   INTEGER                    :: ntr, nqn, ntrp
   INTEGER                    :: i, j
   CHARACTER (LEN=40)         :: frmt
   CHARACTER (LEN=1)          :: ld='(', rd=')'

   ntr = spelin%ntr
   nqn = spelin%nqn
   PRINT *, " List length ? (Max:", ntr, ")"
   READ *, ntrp
   IF (irec == 1) THEN
      WRITE (8,*) ntrp
   ENDIF

   PRINT *," num  - ",(spelin%qnam(j),j=1,nqn)   &
          ,"-> ",(spelin%qnam(j),j=1,nqn)

   DO i = 1, MIN(ntrp,spelin%ntr)
      WRITE (frmt,'(a1,"i4,5x,a",i2,a1)') ld, LEN_TRIM(spelin%inftr(i)), rd
      WRITE (*,frmt) i, spelin%inftr(i)
   ENDDO

END SUBROUTINE VIEW_LINES

!%%%%%%%%%%%%%%%%
SUBROUTINE EMIINT
!%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PREP_VAR
   USE PREP_INTEMI

   IMPLICIT NONE

   CHARACTER (LEN=8)        :: yspec
   CHARACTER (LEN=17)       :: tampon
   CHARACTER (LEN=50)       :: fichout
   REAL (KIND=dp)           :: xmu
   INTEGER                  :: i, ii
   INTEGER                  :: iupl, jlol
   INTEGER                  :: ind_sp
   INTEGER                  :: flag_AllOutputs  ! Flag to call LDVInt - 1 is used in prep to get lines properties

!--------+---------+---------+---------+---------+---------+---------+-*-------+

   PRINT *
   PRINT *, ' Give lines file name:'
   READ (*,'(a50)') fichout
   fichout = TRIM(ADJUSTL(fichout))
   IF (irec == 1) THEN
      WRITE (8,'(a50)') fichout
   ENDIF

   fichier = TRIM(out_dir)//fichout
   PRINT *," Lines written in:", fichier
   OPEN (35, file = fichier, status = 'new')
   OPEN (43, file = TRIM(ADJUSTL(fichier))//"_light", access = 'append')

   PRINT *
   PRINT *, ' Give angle of line of sight (radian) [wrt normal]'
   READ *, xmu
   IF (irec == 1) THEN
      WRITE (8,*) xmu
   ENDIF
   xmu = cos(xmu)
   WRITE (35,'("# mu:",1pe14.6)') xmu

   PRINT *, " "
   PRINT *, " Possible species:"
   i = 1
   DO WHILE (i <= nspl)
      IF (MOD(i,3) /= 0) THEN
         WRITE (*,'(a10)',ADVANCE="NO") spec_lin(i)%nam
      ELSE
         WRITE (*,'(a10)') spec_lin(i)%nam
      ENDIF
      i = i + 1
   ENDDO
   WRITE (*,*)

   DO
      PRINT *
      PRINT *, ' Which species?  (end: -1)'
      READ (*,'(a8)') yspec
      yspec = TRIM(ADJUSTL(yspec))
      IF (irec == 1) THEN
         WRITE (8,'(a8)') yspec
      ENDIF
      IF (yspec == '-1') THEN
         PRINT *, "End lines"
         EXIT
      ENDIF

      ind_sp = 0
      DO i = 1, nspl
        IF ( TRIM(ADJUSTL(yspec)) == TRIM(ADJUSTL(spec_lin(i)%nam))) THEN
            ind_sp = i
            EXIT
         ENDIF
      ENDDO
      print *, ind_sp, yspec, spec_lin(ind_sp)%nam
      IF (ind_sp == 0) THEN
         PRINT *, "  WARNING: excitation of ", yspec, " was not computed"
         CYCLE
      ENDIF

      ii = 999
      PRINT *, ' Emission lines of: ', yspec
      CALL VIEW_LINES(spec_lin(ind_sp), irec)
      DO WHILE (ii /= 0)
         PRINT *, 'Transition?  (0: end)'
         READ *, ii
         IF (ii > spec_lin(ind_sp)%ntr) THEN
            ii = 0
         ENDIF
         IF (irec == 1) THEN
            WRITE (8,*) ii
         ENDIF

         IF (ii /= 0) THEN
            iupl = spec_lin(ind_sp)%iup(ii)
            jlol = spec_lin(ind_sp)%jlo(ii)
            tampon = ""
            tampon = 'Emis.'//TRIM(spec_lin(ind_sp)%nam)//'.'
            tampon = TRIM(ADJUSTL(tampon))//TRIM(ADJUSTL(ii2c(iupl)))//'-'//TRIM(ADJUSTL(ii2c(jlol)))
            flag_AllOutputs = 1
            CALL LDVInt (xmu, spec_lin(ind_sp), ii, tampon, flag_AllOutputs)
         ENDIF
      ENDDO
   ENDDO

   CLOSE (35)

END SUBROUTINE EMIINT

!%%%%%%%%%%%%%%%%
SUBROUTINE EMIINT_WITH_FILE(fichout, xmu, yspectab, nsp, nlines, linesnum)
!%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PREP_VAR
   USE PREP_INTEMI

   IMPLICIT NONE

   CHARACTER (LEN=8)        :: yspec
   CHARACTER (LEN=17)       :: tampon
   !CHARACTER (LEN=50)       :: fichout
   !REAL (KIND=dp)           :: xmu
   INTEGER                  :: i, ii
   INTEGER                  :: iupl, jlol
   INTEGER                  :: ind_sp
   INTEGER                  :: flag_AllOutputs  ! Flag to call LDVInt - 1 is used in prep to get lines properties
   CHARACTER(LEN=50), INTENT(INOUT) :: fichout
   REAL(KIND=dp), INTENT(INOUT) :: xmu
   CHARACTER(LEN=8), INTENT(IN) :: yspectab(10)
   INTEGER, INTENT(IN) :: nsp, nlines(10), linesnum(50)
   INTEGER :: iline, isp, it

!--------+---------+---------+---------+---------+---------+---------+-*-------+

   fichout = TRIM(ADJUSTL(fichout))
   fichier = TRIM(out_dir)//fichout
   PRINT *
   PRINT *," Lines written in:", TRIM(ADJUSTL(fichier)),"and",TRIM(ADJUSTL(fichier))//"_light"
   OPEN (35, file = fichier, status = 'replace')
   OPEN (43, file = TRIM(ADJUSTL(fichier))//"_light", access = 'append')

   xmu = cos(xmu)
   WRITE (35,'("# mu:",1pe14.6)') xmu

   iline = 0
   DO isp=1,nsp
      yspec = yspectab(isp)
      yspec = TRIM(ADJUSTL(yspec))

      ind_sp = 0
      DO i = 1, nspl
        IF ( TRIM(ADJUSTL(yspec)) == TRIM(ADJUSTL(spec_lin(i)%nam))) THEN
            ind_sp = i
            EXIT
         ENDIF
      ENDDO
      !print *, ind_sp, yspec, spec_lin(ind_sp)%nam
      IF (ind_sp == 0) THEN
         PRINT *, "  WARNING: excitation of ", yspec, " was not computed"
         CYCLE
      ENDIF

      PRINT *
      PRINT *, ' Emission lines of: ', yspec
      !CALL VIEW_LINES(spec_lin(ind_sp), irec)
      DO it=1,nlines(isp)
         iline = iline + 1
         ii = linesnum(iline)
         IF (ii > spec_lin(ind_sp)%ntr) THEN
            ii = 0
         ENDIF
         IF (irec == 1) THEN
            WRITE (8,*) ii
         ENDIF

         IF (ii /= 0) THEN
            iupl = spec_lin(ind_sp)%iup(ii)
            jlol = spec_lin(ind_sp)%jlo(ii)
            tampon = ""
            tampon = 'Emis.'//TRIM(spec_lin(ind_sp)%nam)//'.'
            tampon = TRIM(ADJUSTL(tampon))//TRIM(ADJUSTL(ii2c(iupl)))//'-'//TRIM(ADJUSTL(ii2c(jlol)))
            flag_AllOutputs = 1
            CALL LDVIntMOD (xmu, spec_lin(ind_sp), ii, tampon, flag_AllOutputs)
         ENDIF
      ENDDO
   ENDDO

   CLOSE (35)
   CLOSE (43)

END SUBROUTINE EMIINT_WITH_FILE

END MODULE PREP_EMISSI
