! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

! JLB - 21 May 2008

MODULE PXDR_READCHIM

   CHARACTER (LEN=15), PUBLIC, DIMENSION(:), ALLOCATABLE :: csect_files          ! List of reactions
   CHARACTER (LEN=15)                                    :: un_nom, de_nom
   CHARACTER (LEN=15), DIMENSION(:), ALLOCATABLE         :: les_esp              ! List of species
   PRIVATE

   PUBLIC :: LECTUR

CONTAINS

!!!==============================================================================
!!! SUBROUTINE LECTUR
!!!
!!! Read chemical species, initial abundances and chemical reactions
!!!==============================================================================

!%%%%%%%%%%%%%%%%
SUBROUTINE LECTUR
!%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PXDR_RF_TOOLS
   USE PXDR_ISOTCO

   IMPLICIT NONE

   CHARACTER (LEN=8)                  :: ref
   CHARACTER (LEN=8)                  :: r1, r2, p1, p2, p3, p4, pp
   INTEGER                            :: j1, j2, j3, j4

   REAL (KIND=dp)                     :: msp(natom+6)

   REAL (KIND=dp)                     :: sreact
   REAL (KIND=dp)                     :: creact
   REAL (KIND=dp)                     :: sprod
   REAL (KIND=dp)                     :: cprod
   INTEGER, DIMENSION(2,21)           :: bel
   INTEGER, DIMENSION(:), ALLOCATABLE :: indy
   INTEGER                            :: idiff = 0

   INTEGER                            :: i, ii, j, jj, k, ik, kj, l, mm
   INTEGER                            :: ir1, ir2, ir3, iv1, iv2, kkk, kjj
   INTEGER                            :: iprod
   INTEGER                            :: ifin, ideb, nreact, ireact
   REAL (KIND=dp)                     :: xnip0, xnim0
   REAL (KIND=dp)                     :: dreact, ddprod
   INTEGER                            :: ivp
   INTEGER                            :: keep_react ! Flag, if 0 a fitted photo-reaction read in .chi is removed
                                                    ! and will be treated by direct integration of cross sections

!              set up molecular weight of elements in amu

    DATA msp /   1.00_dp, 12.0_dp, 14.0_dp, 16.0_dp,  4.0_dp,  2.0_dp, 13.0_dp, 15.0_dp, 18.0_dp &
             , 666.00_dp, 19.0_dp, 23.0_dp, 24.3_dp, 27.0_dp, 28.1_dp, 31.0_dp, 32.0_dp, 35.5_dp &
             ,  40.08_dp, 55.8_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp,  0.0_dp /

!*********************************************************************

!              Relationship between index and element

!        1  :  h
!        2  :  c
!        3  :  n
!        4  :  o
!        5  :  he
!        6  :  d
!        7  :  c13 (=c*)
!        8  :  n15 (=n*)
!        9  :  o18 (=o*)
!       10  :  li or PAH
!       11  :  f
!       12  :  na
!       13  :  mg
!       14  :  al
!       15  :  si
!       16  :  p
!       17  :  s
!       18  :  cl
!       19  :  k ou ca
!       20  :  fe
!       21  :  charge

   WRITE (iscrn,*)
   WRITE (iscrn,*) "--- Read chemistry data file : ", chimie

   ael(0,:)  = 0
   mol(0)    = 0.0_dp
   enth(0)   = 0.0_dp
   charge(0) = 0.0_dp
   abin(:)   = 1.0e-20_dp
   t_diff(:) = 0.0e+0_dp
   t_evap(:) = 0.0e+0_dp

!   READ species file and set up conservation array
!  Species must be ordered as:
!      1 - Gaz neutral
!      2 - Gaz positive ions
!      3 - Gaz negative ions
!      4 - Chemisorbed (with Langmuir rejection)
!      5 - Grain multi-layers
!      6 - Grain mono-layer (i.e. with Langmuir rejection)

   i = 0
   n_var  = 0
   nsp_ne = 0
   nsp_ip = 0
   nsp_im = 0
   nsp_cs = 0
   nsp_gm = 0
   nsp_g1 = 0
   speci(0) = "        "
   l_atom(:)%abnorm = 0.0_dp

   !=====================================================================
   !=== READ CHEMISTRY FILE
   !=====================================================================
   fichier = TRIM(data_dir)//'Chimie/'//chimie
   WRITE (iscrn,*) "    * Read the list of chemical species"
   OPEN (iread1, file = fichier, status = 'old')
   READ (iread1,"(/)")

   !-----------------------------------------------------------------
   ! Get the list of chemical species
   !-----------------------------------------------------------------
   i = 0
   DO
      i = i + 1
      READ (iread1,'(i3,2x,a8,1x,2i2,3i1,1x,4i1,1x,5i1,1x,6i1,1x,i2,1x,d10.3,f9.3,1x,a16)') &
           l, speci(i), (ael(i,j), j=1,21), abin(i), enth(i)

      IF (l == 888) THEN ! In chemistry file "888" indicates the end
         EXIT            ! of the list of species
      ENDIF

      ! Compute elementary abundances
      DO j = 1, natom-1
         l_atom(j)%abnorm = l_atom(j)%abnorm + ael(i,j) * abin(i)
      ENDDO

      ! Identify index of all species which may be needed elsewhere
      IF (speci(i) == 'h'    .OR. speci(i) == 'H'   )    i_h = i
      IF (speci(i) == 'd'    .OR. speci(i) == 'D'   )    i_d = i
      IF (speci(i) == 'h:'   .OR. speci(i) == 'H:'  )  i_hgr = i
      IF (speci(i) == 'd:'   .OR. speci(i) == 'D:'  )  i_dgr = i
      IF (speci(i) == 'h::'  .OR. speci(i) == 'H::' ) i_hgrc = i
      IF (speci(i) == 'h2'   .OR. speci(i) == 'H2'  )   i_h2 = i
      IF (speci(i) == 'hd'   .OR. speci(i) == 'HD'  )   i_hd = i
      IF (speci(i) == 'h2:'  .OR. speci(i) == 'H2:' ) i_h2gr = i
      IF (speci(i) == 'hd:'  .OR. speci(i) == 'HD:' ) i_hdgr = i
      IF (speci(i) == 'he'   .OR. speci(i) == 'HE'  )   i_he = i
      IF (speci(i) == 'c'    .OR. speci(i) == 'C'   )    i_c = i
      IF (speci(i) == 'n'    .OR. speci(i) == 'N'   )    i_n = i
      IF (speci(i) == 'o'    .OR. speci(i) == 'O'   )    i_o = i
      IF (speci(i) == 'o2'   .OR. speci(i) == 'O2'  )   i_o2 = i
      IF (speci(i) == 's'    .OR. speci(i) == 'S'   )    i_s = i
      IF (speci(i) == 'si'   .OR. speci(i) == 'Si'  )   i_si = i
      IF (speci(i) == 'c*'   .OR. speci(i) == 'C*'  )  i_c13 = i
      IF (speci(i) == 'o*'   .OR. speci(i) == 'O*'  )  i_o18 = i
      IF (speci(i) == 'h2o'  .OR. speci(i) == 'H2O' )  i_h2o = i
      IF (speci(i) == 'h2o:' .OR. speci(i) == 'H2O:') i_h2ogr = i
      IF (speci(i) == 'oh'   .OR. speci(i) == 'OH'  )   i_oh = i
      IF (speci(i) == 'c2'   .OR. speci(i) == 'C2'  )   i_c2 = i
      IF (speci(i) == 'co'   .OR. speci(i) == 'CO'  )   i_co = i
      IF (speci(i) == 'c*o'  .OR. speci(i) == 'C*O' ) i_c13o = i
      IF (speci(i) == 'co*'  .OR. speci(i) == 'CO*' ) i_co18 = i
      IF (speci(i) == 'c*o*' .OR. speci(i) == 'C*O*') i_c13o18 = i
      IF (speci(i) == 'co:'  .OR. speci(i) == 'CO:' ) i_cogr = i
      IF (speci(i) == 'cs'   .OR. speci(i) == 'CS'  )   i_cs = i
      IF (speci(i) == 'hcn'  .OR. speci(i) == 'HCN' )  i_hcn = i
      IF (speci(i) == 'h+'   .OR. speci(i) == 'H+'  )   i_hp = i
      IF (speci(i) == 'h3+'  .OR. speci(i) == 'H3+' )  i_h3p = i
      IF (speci(i) == 'h2d+' .OR. speci(i) == 'H2D+') i_h2dp = i
      IF (speci(i) == 'c+'   .OR. speci(i) == 'C+'  )   i_cp = i
      IF (speci(i) == 'c*+'  .OR. speci(i) == 'C*+' ) i_c13p = i
      IF (speci(i) == 'n+'   .OR. speci(i) == 'N+'  )   i_np = i
      IF (speci(i) == 'o+'   .OR. speci(i) == 'O+'  )   i_op = i
      IF (speci(i) == 's+'   .OR. speci(i) == 'S+'  )   i_sp = i
      IF (speci(i) == 'si+'  .OR. speci(i) == 'SI+' )  i_sip = i
      IF (speci(i) == 'fe+'  .OR. speci(i) == 'FE+' )  i_fep = i
      IF (speci(i) == 'hco+' .OR. speci(i) == 'HCO+') i_hcop = i
      IF (speci(i) == 'ch+'  .OR. speci(i) == 'CH+')  i_chp  = i

      !- Neutral species
      IF (ael(i,natom) == 0 .AND. INDEX(speci(i),":") == 0) THEN
         nsp_ne = nsp_ne + 1
         mol(i) = SUM(ael(i,1:natom-1)*msp(1:natom-1))
         charge(i) = ael(i,natom)
      !- Positive ions
      ELSE IF (ael(i,natom) > 0 .AND. INDEX(speci(i),":") == 0) THEN
         nsp_ip = nsp_ip + 1
         mol(i) = SUM(ael(i,1:natom-1)*msp(1:natom-1))
         charge(i) = ael(i,natom)
      !- Negative ions
      ELSE IF (ael(i,natom) < 0 .AND. INDEX(speci(i),":") == 0) THEN
         nsp_im = nsp_im + 1
         mol(i) = SUM(ael(i,1:natom-1)*msp(1:natom-1))
         charge(i) = ael(i,natom)
      !- H chemisorbed on grains
      ELSE IF (i == i_hgrc) THEN
         nsp_cs = nsp_cs + 1
         mol(i) = SUM(ael(i,1:natom-1)*msp(1:natom-1))
         charge(i) = ael(i,natom)
         t_diff(i) = 5000.0_dp           ! Arbitrary high value => strong binding
         t_evap(i) = 5000.0_dp           ! Arbitrary high value => strong binding
         T2_stick_ER = 464.0_dp          ! Value such that sticking is 1.0 / 1.1 for T_gas = 100 K
         ex_stick_ER = 1.5_dp

      !- Adsorbed species excepted H, D, H2 and HD
      ELSE IF (INDEX(speci(i),":") /= 0 .AND. &
               i /= i_hgr .AND. i /= i_dgr .AND. &
               i /= i_h2gr .AND. i /= i_hdgr .AND. i /= i_hgrc) THEN
         nsp_gm = nsp_gm + 1
         mol(i) = SUM(ael(i,1:natom-1)*msp(1:natom-1))
         charge(i) = ael(i,natom)
      ! From McCoustra et al (MNRAS 354, 1133, 2004) as computed by Evelyne:
      ! Do not uncomment co2gr and ch3ohgr yet !
         IF (i == i_h2ogr) THEN
            t_evap(i) = 5050.0_dp
         ELSE IF (i == i_cogr) THEN
            t_evap(i) = 1510.0_dp
!        ELSE IF (i == i_co2gr) THEN
!           t_evap(i) = 2500.0_dp
!        ELSE IF (i == i_ch3ohgr) THEN
!           t_evap(i) = 3710.0_dp
         ELSE
            t_evap(i) = 3500.0_dp          ! Rough estimate
         ENDIF
         PRINT *, "  WARNING: t_diff and t_evap not set !"
      !- Adsorbed H
      ELSE IF (i == i_hgr) THEN
         nsp_g1 = nsp_g1 + 1
         n_var = n_var + npg
         mol(i) = SUM(ael(i,1:natom-1)*msp(1:natom-1))
         charge(i) = ael(i,natom)
!  See also table 4 of Chang et al. A&A 469, 273 (2007)
!        t_diff(i) = 406.0_dp            ! Amorphous silicate (Perets et al., 2007)
!        t_evap(i) = 510.0_dp            ! Amorphous silicate (Perets et al., 2007)
         t_diff(i) = 510.606_dp          ! Amorphous carbon
         t_evap(i) = 657.985_dp          ! Amorphous carbon
!        t_diff(i) = 100.0_dp            ! Hasegawa et al. 1992
!        t_evap(i) = 350.0_dp            ! Hasegawa et al. 1992
      !- Adsorbed D
      ELSE IF (i == i_dgr) THEN
         nsp_g1 = nsp_g1 + 1
         n_var = n_var + npg
         mol(i) = SUM(ael(i,1:natom-1)*msp(1:natom-1))
         charge(i) = ael(i,natom)
         t_diff(i) = 510.6_dp           ! Amorphous carbon (LePetit09)
         t_evap(i) = 716.0_dp           ! Amorphous carbon (LePetit09)
      !- Adsorbed H2
      ELSE IF (i == i_h2gr) THEN
         nsp_g1 = nsp_g1 + 1
         n_var = n_var + npg
         mol(i) = SUM(ael(i,1:natom-1)*msp(1:natom-1))
         charge(i) = ael(i,natom)
         t_diff(i) = 320.0_dp            ! Rough estimation
         t_evap(i) = 450.0_dp            ! Rough estimation
      !- Adsorbed HD
      ELSE IF (i == i_hdgr) THEN
         nsp_g1 = nsp_g1 + 1
         n_var = n_var + npg
         mol(i) = SUM(ael(i,1:natom-1)*msp(1:natom-1))
         charge(i) = ael(i,natom)
         t_diff(i) = 320.0_dp            ! Rough estimation, same as H2
         t_evap(i) = 450.0_dp            ! Rough estimation
      ENDIF

         !--------------------------------------------------------------------
   ENDDO ! END OF IMPLICIT LOOP ON SPECIES READING
         !--------------------------------------------------------------------

   ! Number of species before monolayer on grains
   nsp_n1 = nsp_ne + nsp_ip + nsp_im + nsp_cs + nsp_gm
   ! Add 1 for electrons.
   nspec  = nsp_n1 + nsp_g1 + 1
   speci(nspec)  = sp(21)
   mol(nspec)    = msp(21)
   enth(nspec)   = 0.0_dp
   charge(nspec) = -1.0_dp
   ael(nspec,21) = -1
   n_var = n_var + nspec

   ! Take care of variables for grain surface
   DO i = nsp_n1+1, nsp_n1+nsp_g1
      DO mm = 1, npg
         ivp = nspec + (i - nsp_n1 - 1) * npg + mm
!        speci(ivp) = "mom_X   "
         speci(ivp) = "mo"//TRIM(speci(i))//TRIM(ADJUSTL(ii2c(mm)))
      ENDDO
   ENDDO
   ALLOCATE (xmatk(n_var))
   xmatk(1:n_var)%nbt = 0

   ! Add photons (sp(22)), crp (sp(23)), phosec (sp(24))
   !     grains (sp(25)), 2h (sp(26))
   DO i = 1, 5
      ii = i + n_var
      jj = i + natom
      speci(ii) = sp(jj)
      mol(ii) = msp(jj)
   ENDDO
   enth(n_var+1)   =  13.0_dp / calev ! Photons
   enth(n_var+2)   =  20.0_dp / calev ! Cosmic
   enth(n_var+3)   =  13.0_dp / calev ! Secondary photons
   enth(n_var+4)   =   0.0_dp         ! Grains
   enth(n_var+5)   =   0.0_dp         ! 2H
   charge(n_var+1) =   0.0_dp         ! Photons
   charge(n_var+2) =   0.0_dp         ! Cosmic
   charge(n_var+3) =   0.0_dp         ! Secondary photons
   charge(n_var+4) =   0.0_dp         ! Grains
   charge(n_var+5) =   0.0_dp         ! 2H
   ! ael was initialized to 0. Here we only replace the incorrect values
   ael(nspec+5,1)  = 2                ! 2H
   mol(n_var+5)    = 2.0_dp * msp(1)  ! 2H

   ! Inital abondance of electrons.
   xnip0 = SUM(abin(nsp_ne+1:nsp_ne+nsp_ip))
   xnim0 = SUM(abin(nsp_ne+nsp_ip+1:nsp_ne+nsp_ip+nsp_im))
   IF (xnip0 < xnim0) THEN
      WRITE (iscrn,'(" Wrong number of positive charges!")')
      WRITE (iscrn,'(5x,1p,"ions + :",e9.2," ions - :",e9.2)') xnip0, xnim0
      STOP
   ENDIF
   abin(nspec) = xnip0 - xnim0
   abelec = abin(nspec)
   lcon(nspec) = natom

   ! Total abondances of each atom
   abtot = 0.0_dp
   DO i = 1, nspec-1
      DO l = 1, natom-1
         abtot(l) = abtot(l) + ael(i,l) * abin(i)
      ENDDO
   ENDDO

   abin(nspec+1:n_var) = 1.0e-20_dp

   WRITE (iwrit2,'(//,3x,"Included species :", &
                 & /,23x,"weight   ab_init   enthalpy",/)')
   DO l = 1, n_var+5
      WRITE (iwrit2,'(5x,i3,5x,a8,1x,f5.1,2x,1pe12.4,2x,0pf8.3)') &
             l, speci(l), mol(l), abin(l), enth(l)
   ENDDO

   WRITE (iscrn,*) " "
   WRITE (iscrn,*) "    ============= CHEMICAL SPECIES ====================="
   WRITE (iscrn,*) "    * Size of 'speci' array                           : ", n_var + 5
   WRITE (iscrn,*) "    * Number of variables for chemical network        : ", n_var
   WRITE (iscrn,*) "    * Total number of species, with electrons         : ", nspec
   WRITE (iscrn,*) "    ----------------------------------------------------"
   WRITE (iscrn,*) "    * Number of neutral species                       : ", nsp_ne
   WRITE (iscrn,*) "    * Number of cations                               : ", nsp_ip
   WRITE (iscrn,*) "    * Number of anions                                : ", nsp_im
   WRITE (iscrn,*) "    * Number of chemisorbed species                   : ", nsp_cs
   WRITE (iscrn,*) "    * Number of species in grains mantles (any depth) : ", nsp_gm
   WRITE (iscrn,*) "    * Number of species in grains mantles (one layer) : ", nsp_g1
   WRITE (iscrn,*) "    ===================================================="

   WRITE (iwrit2,'(//,3x,i3," neutral species", &
                 & /,3x,i3," positively ionised species", &
                 & /,3x,i3," negatively ionised species", &
                 & /,3x,i3," chemisorbed species on grains", &
                 & /,3x,i3," species in grain mantles (any depth)", &
                 & /,3x,i3," species in grain mantles (one layer)", &
                 & /,3x,i3," chemical species (with electrons)",/)') &
                   nsp_ne, nsp_ip, nsp_im, nsp_cs, nsp_gm, nsp_g1, nspec

   !-------------------------------------------------------------------
   ! PHOTO-REACTIONS - Get the list of photo-reactions with direct integration
   !-------------------------------------------------------------------
   ! Look for species with direct integration for photo-reactions
   CALL POST_LECTUR

   !-------------------------------------------------------------------
   ! Read chemical reactions with reordering
   !-------------------------------------------------------------------
   WRITE (iscrn,*)
   WRITE (iscrn,*) "    * Read the chemical network"

   WRITE (iwrit2,'(3x,"Chemical reactions",/, &
                 & 63x,"gamma",3x,"alpha",4x,"beta",3x,"deltae",/)')
   i = nphot_i + 1

   DO
      react(i,0:7) = 999

      !- READ A CHEMICAL REACTION ----------------------------------------
      READ (iread1,"(A5)",ADVANCE="NO")  ref
           IF (ref == "99999") EXIT              ! end of chemistry
      READ (iread1,"(1x,A8)",ADVANCE="NO") r1
           IF (r1 == "        ") THEN            ! commentary line in chemistry
              READ (iread1,*)                    ! go to begining of next line
              CYCLE                              ! THEN READ next line
           ENDIF
      ! Read the rest of the reaction
      READ (iread1,'(4a8,a7,d8.2,1x,f5.2,1x,f8.1,i4)') &
           r2, p1, p2, p3, p4, gamm(i), alpha(i), bet(i), itype(i)

      j1 = 9999
      j2 = 9999
      j3 = 9999
      j4 = 9999
      ! Sort products
      DO j = 1, n_var+5
         IF (TRIM(ADJUSTL(p1)) == speci(j)) THEN
            j1 = j
         ENDIF
         IF (TRIM(ADJUSTL(p2)) == speci(j)) THEN
            j2 = j
         ENDIF
         IF (TRIM(ADJUSTL(p3)) == speci(j)) THEN
            j3 = j
         ENDIF
         IF (TRIM(ADJUSTL(p4)) == speci(j)) THEN
            j4 = j
         ENDIF
      ENDDO
      IF (j2 < j1) THEN
         pp = p1
         p1 = p2
         p2 = pp
      ENDIF
      IF (j3 < j1) THEN
         pp = p1
         p1 = p3
         p3 = pp
      ENDIF
      IF (j4 < j1) THEN
         pp = p1
         p1 = p4
         p4 = pp
      ENDIF
      IF (j3 < j2) THEN
         pp = p2
         p2 = p3
         p3 = pp
      ENDIF
      IF (j4 < j2) THEN
         pp = p4
         p4 = p2
         p4 = pp
      ENDIF
      IF (j4 < j3) THEN
         pp = p3
         p3 = p4
         p4 = pp
      ENDIF
      ! itype = 5 : photodestruction
      ! Check if the photo-destruction reactions has to be computed with
      ! direct integration. If yes, the reaction in the chemistry is not read
      IF (itype(i) == 5) THEN
         keep_react = 1  ! Flag, if 0 the reaction read in the .chi file will be not be used
         DO k = 1, nphot_i
            IF (        ( TRIM(ADJUSTL(r1)) == TRIM(ADJUSTL(phdest(k)%r1)) ) &
                 .AND.  ( TRIM(ADJUSTL(p1)) == TRIM(ADJUSTL(phdest(k)%p1)) ) &
                 .AND.  ( TRIM(ADJUSTL(p2)) == TRIM(ADJUSTL(phdest(k)%p2)) ) &
                 .AND.  ( TRIM(ADJUSTL(p3)) == TRIM(ADJUSTL(phdest(k)%p3)) ) &
                 .AND.  ( TRIM(ADJUSTL(p4)) == TRIM(ADJUSTL(phdest(k)%p4)) ) &
               ) THEN
               keep_react = 0
            ENDIF
         ENDDO
         IF (keep_react == 0) THEN
            WRITE(*,1070) r1, r2, p1, p2, p3, p4
            CYCLE
         ENDIF
      ENDIF
1070  FORMAT("       Remove : ",2(A8,1x)," -> ",4(A8,1x), &
             " (treated by integration of rad. cross sections)")

      ! Used for Pepe's chemistry to avoid duplicate reactions (provided by Evelyne)
      IF (ref == 'deja ')  THEN
         CYCLE
      ENDIF
!     IF (bet(i) < 0.0_dp)  THEN
!        bet(i) = 0.0_dp
!     ENDIF

      ! WARNING: speci begins at 0 !!!
      DO j = 0, n_var+5
         IF (r1 == speci(j)) THEN
            react(i,1) = j
            EXIT
         ENDIF
      ENDDO
      DO j = 0, n_var+4
         IF (r2 == speci(j)) THEN
            react(i,2) = j
            react(i,3) = 0
            EXIT
         ENDIF
      ENDDO
      IF (r2 == speci(n_var+5)) THEN
         react(i,2) = i_h
         react(i,3) = i_h
      ENDIF
      DO j = 0, n_var+5
         IF (p1 == speci(j)) THEN
            react(i,4) = j
            EXIT
         ENDIF
      ENDDO
      DO j = 0, n_var+5
         IF (p2 == speci(j)) THEN
            react(i,5) = j
            EXIT
         ENDIF
      ENDDO
      DO j = 0, n_var+5
         IF (p3 == speci(j)) THEN
            react(i,6) = j
            EXIT
         ENDIF
      ENDDO
      DO j = 0, n_var+5
         IF (p4 == speci(j)) THEN
            react(i,7) = j
            EXIT
         ENDIF
      ENDDO

      IF (r2 == sp(natom+6))  react(i,2) = 0
      IF (p2 == sp(natom+6))  react(i,5) = 0
      IF (p3 == sp(natom+6))  react(i,6) = 0
      IF (p4 == sp(natom+6))  react(i,7) = 0

      ! Look for incomplete reactions
      DO j = 1, 7
         IF (react(i,j) == 999)  THEN
            react(i,j) = 0
            WRITE (iwrit2,'(5x,"WARNING, error in reactions, line:",i4,2x,7a8)') &
                   i, (speci(react(i,jj)), jj=1,j)
            WRITE (iscrn,'(5x,"WARNING, error in reactions, line:",i4,2x,7a8)') &
                   i, (speci(react(i,jj)), jj=1,j)
            CYCLE
         ENDIF
      ENDDO

      IF (react(i,5) == 0) THEN
         react(i,0) = 3
      ELSE IF (react(i,6) == 0) THEN
         react(i,0) = 4
      ELSE IF (react(i,7) == 0) THEN
         react(i,0) = 5
      ELSE
         react(i,0) = 6
      ENDIF

      ! Compute sum of mass of products of each reaction
      ! and net energy balance (in eV)
      ! convention: positive ==> exothermique
      !             negative ==> endothermique
      !             ******** ==> donnees insuffisantes

      bel = 0
      DO j = 1, 20
         bel(1,j) = ael(react(i,1),j) + ael(react(i,2),j) + ael(react(i,3),j)
         bel(2,j) = ael(react(i,4),j) + ael(react(i,5),j) + ael(react(i,6),j) + ael(react(i,7),j)
         IF (bel(1,j) /= bel(2,j)) THEN
            PRINT *, speci(react(i,1)), speci(react(i,2)), speci(react(i,3)), speci(react(i,4)) &
                   , speci(react(i,5)), speci(react(i,6)), speci(react(i,7))
!           STOP
         ENDIF
      ENDDO

      !--- Check wrong reactions --------------------------------------
      sreact = 0.0_dp
      creact = 0.0_dp
      sprod  = 0.0_dp
      cprod  = 0.0_dp
      sreact = sreact + mol(react(i,1)) + mol(react(i,2)) + mol(react(i,3))
      sprod  = sprod  + mol(react(i,4)) + mol(react(i,5)) + mol(react(i,6)) + mol(react(i,7))
      creact = creact + charge(react(i,1)) + charge(react(i,2)) + charge(react(i,3))
      cprod  = cprod  + charge(react(i,4)) + charge(react(i,5)) + charge(react(i,6)) + charge(react(i,7))

      IF (react(i,5) /= n_var+1) THEN
         dreact = enth(react(i,1)) + enth(react(i,2)) + enth(react(i,3))
         ddprod = 0.0_dp
         kj = react(i,0)
         DO j = 4, kj+1
            iprod = react(i,j)
            IF (iprod /= n_var+1) THEN
               ddprod = ddprod + enth(iprod)
            ENDIF
         ENDDO
         de(i) = dreact - ddprod
         de(i) = de(i) * calev
      ELSE
         de(i) = 0.0_dp
      ENDIF
      bilanm(i) = sreact - sprod
      bilanc(i) = creact - cprod
      IF (ABS(bilanm(i)) > 0.0_dp) THEN
         PRINT *, i, (speci(react(i,j)),j=1,7)
         PRINT *, i, "Mass - React:", sreact, " Prod:", sprod, "Diff:", bilanm(i)
         PRINT *,"Problem in mass conservation"
         PRINT *,"Reactant : ", speci(react(i,1)), "Mass :", mol(react(i,1))
         PRINT *,"Reactant : ", speci(react(i,2)), "Mass :", mol(react(i,2))
         PRINT *,"Reactant : ", speci(react(i,3)), "Mass :", mol(react(i,3))
         PRINT *,"Product  : ", speci(react(i,4)), "Mass :", mol(react(i,4))
         PRINT *,"Product  : ", speci(react(i,5)), "Mass :", mol(react(i,5))
         PRINT *,"Product  : ", speci(react(i,6)), "Mass :", mol(react(i,6))
         PRINT *,"Product  : ", speci(react(i,7)), "Mass :", mol(react(i,7))
         STOP
      ENDIF
      IF (      (speci(react(i,1)) /= "grain ") .AND. (speci(react(i,2)) /= "grain ") &
          .AND. (speci(react(i,3)) /= "grain ") ) THEN
         IF (ABS(bilanc(i)) > 0.0_dp) THEN
            PRINT *, i, (speci(react(i,j)),j=1,7)
            PRINT *, i, "Charge - React:", creact, " Prod:", cprod, " Diff:", bilanc(i)
            PRINT *,"Problem in charge conservation"
            PRINT *,"Reactant : ", speci(react(i,1)), "Mass :", charge(react(i,1))
            PRINT *,"Reactant : ", speci(react(i,2)), "Mass :", charge(react(i,2))
            PRINT *,"Reactant : ", speci(react(i,3)), "Mass :", charge(react(i,3))
            PRINT *,"Product  : ", speci(react(i,4)), "Mass :", charge(react(i,4))
            PRINT *,"Product  : ", speci(react(i,5)), "Mass :", charge(react(i,5))
            PRINT *,"Product  : ", speci(react(i,6)), "Mass :", charge(react(i,6))
            PRINT *,"Product  : ", speci(react(i,7)), "Mass :", charge(react(i,7))
            STOP
         ENDIF
      ENDIF

      i = i + 1

         !------------------------------------------------------------------
   ENDDO ! END OF IMPLICIT LOOP ON CHEMICAL REACTIONS
         !------------------------------------------------------------------

   neqt = i - 1

   nreact = neqt
   WRITE (iscrn,*)
   WRITE (iscrn,*) "    * Total number of chemical reactions : ", nreact

! *************************************************************************
!                 Build here isotopic reactions
! *************************************************************************

!   convention :    In the species list, each isotpe must follow
!   ----------      immediately the main species
!                   If the specy CONTAINS C and O, O* must follow C*
!                   ex :    hco2
!                           hc*o2
!                           hco*o
!                           hc*o*o

! *************************************************************************

!  Are there isotopes in the species list (C* THEN O*) ?

!-------------------------------------------------------------------------
!  Test variable: i_c13 > 0  ==>  There is 13C in the chemistry
!                            ==>  We need C*O excitation

!                 i_c13 = 0  ==>  No 13C

!                 i_o18 > 0  ==>  There is 18O in the chemistry
!                            ==>  We need CO* (and C*O*) excitation

!                 i_o18 = 0  ==>  No 18O
!-------------------------------------------------------------------------

   ! Are there isotopes within neutral species (C* and O*) ?
   ! NO ==> Reaction list is CLOSEd (I.e. go to 400)
   IF ((i_c13 /= 0) .OR. (i_o18 /= 0)) THEN
      IF (i_c13 > 0) THEN
         WRITE (iscrn,*) "    * Build 13C chemical network"
         CALL ISOTC (nreact, idiff)
      ENDIF
      IF (i_o18 > 0) THEN
         WRITE (iscrn,*) "    * Build 18O chemical network"
         CALL ISOTO (nreact,idiff)
      ENDIF

!----------------------------------------------------------------------------

!         neqt   = total number of reactions without isotopes (up to now)
!         nreact = Isotopic reaction number
!         idiff  = Number of the last differential reaction

!----------------------------------------------------------------------------
!  End of isotopic chemistry construction
!----------------------------------------------------------------------------

      WRITE (iwrit2,'(//,10x,"Isotopic reactions",5x,"i_c13 =",i2, &
                   &" i_o18 =",i2,/,10x,19("-"),/)')  i_c13, i_o18

      DO ii = neqt+1, nreact

         !-------------------------------------------------------------------
         ! Compute mass and energy balance for each reaction
         !-------------------------------------------------------------------
         dreact = 0.0_dp
         ddprod = 0.0_dp
         DO jj = 1, 3
            ireact = react(ii,jj)
            dreact = dreact + enth(ireact)
         ENDDO
         kjj = react(ii,0) + 1
         DO jj = 4, kjj
            iprod = react(ii,jj)
            IF (iprod /= n_var+1) THEN
               ddprod = ddprod + enth(iprod)
            ENDIF
         ENDDO
         de(ii) = dreact - ddprod
         de(ii) = de(ii) * calev

         !-------------------------------------------------------------------
         ! Write isotopic reactions
         !-------------------------------------------------------------------
         r1 = speci (react(ii,1))
         r2 = speci (react(ii,2))
         p1 = speci (react(ii,4))
         p2 = speci (react(ii,5))
         p3 = speci (react(ii,6))
         p4 = speci (react(ii,7))
         ref = 'isotp   '
         !-------------------------------------------------------------------

      ENDDO

   ! Total number of reactions (normal + isotopic)
   ELSE
      WRITE (iscrn,*) "    * No 13C or 18O chemical reactions to add"
   ENDIF

   neqt = nreact
   WRITE (iwrit2,'(/,3x,i4," chemical reactions (with photo-reactions)",/)') neqt

   ! Reorder reactants in increasing species number
   ! which leads to a triangular matrix
   DO i = 1, neqt
      IF (react(i,1) > react(i,2) .AND.  react(i,2) /= 0)  THEN
         l = react(i,1)
         react(i,1) = react(i,2)
         react(i,2) = l
      ENDIF
   ENDDO

   CLOSE(iread1)

   CALL ORDON

   WRITE (iwrit2,'(/,3x,i4," chemical reactions (with photo-reactions)",//)') neqt
   WRITE (iscrn,'("     * Total number of chemical reactions : ",I5)') neqt

   ! Count number of destruction and creation term for each variable

   ! itype : 0
   DO i = 1, nh2for
      ir1 = react(i,1)
      ir2 = react(i,2)
      iprod = react(i,4)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      xmatk(ir2)%nbt = xmatk(ir2)%nbt + 1
      xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
   ENDDO

   ! itype : 1
   ideb = nh2for + 1
   ifin = nh2for + ncrion
   DO i = ideb, ifin
      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
      ENDDO
      ir1 = react(i,1)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
   ENDDO

   ! itype : 2
   ideb = ifin + 1
   ifin = ifin + npsion
   DO i = ideb, ifin
      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
      ENDDO
      ir1 = react(i,1)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
   ENDDO

   ! itype : 3
   ideb = ifin + 1
   ifin = ifin + nasrad
   DO i = ideb, ifin
      iprod = react(i,4)
      xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
      ir1 = react(i,1)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      ir2 = react(i,2)
      xmatk(ir2)%nbt = xmatk(ir2)%nbt + 1
   ENDDO

   ! itype : 4
   ideb = ifin + 1
   ifin = ifin + nrest
   DO i = ideb, ifin
      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
      ENDDO
      ir1 = react(i,1)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      ir2 = react(i,2)
      xmatk(ir2)%nbt = xmatk(ir2)%nbt + 1
   ENDDO

   ! itype : 6 (Warning: DO not interchange 6 and 5 !)
   ideb = ifin + 1
   ifin = ifin + nh2endo
   DO i = ideb, ifin
      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
      ENDDO
      ir1 = react(i,1)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      ir2 = react(i,2)
      xmatk(ir2)%nbt = xmatk(ir2)%nbt + 1
   ENDDO

   ! itype : 5 (Warning: DO not interchange 6 and 5 !)
   ideb = ifin + 1
   ifin = ifin + nphot_f
   DO i = ideb, ifin
      ir1 = react(i,1)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
      ENDDO
   ENDDO

   ! itype : 7
   ideb = ifin + 1
   ifin = ifin + nphot_i
   DO i = ideb, ifin
      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
      ENDDO
      ir1 = react(i,1)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
   ENDDO

   ! itype : 8
   ideb = ifin + 1
   ifin = ifin + n3body
   DO i = ideb, ifin
      ir1 = react(i,1)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      ir2 = react(i,2)
      xmatk(ir2)%nbt = xmatk(ir2)%nbt + 1
      ir3 = react(i,3)
      xmatk(ir3)%nbt = xmatk(ir3)%nbt + 1
      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
      ENDDO
   ENDDO

   ! itype : 101...
   ideb = ifin + 1
   ifin = ifin + ncpart
   DO i = ideb, ifin
      ir1 = react(i,1)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      ir2 = react(i,2)
      xmatk(ir2)%nbt = xmatk(ir2)%nbt + 1
      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
      ENDDO
   ENDDO

   ! itype : 11 and 111
   ideb = ifin + 1
   ifin = ifin + ngsurf
   DO i = ideb, ifin
      ir1 = react(i,1)
      ir2 = react(i,2)

      IF (itype(i) == 11) THEN
         iprod = react(i,4)
         DO mm = 1, npg
            iv2 = nspec + (ir2 - nsp_n1 - 1) * npg + mm
            xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
            xmatk(iv2)%nbt = xmatk(iv2)%nbt + 1
            xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
         ENDDO
      ELSE IF (itype(i) == 111) THEN
         iprod = react(i,4)
         DO mm = 1, npg
            iv1 = nspec + (ir1 - nsp_n1 - 1) * npg + mm
            iv2 = nspec + (ir2 - nsp_n1 - 1) * npg + mm
            xmatk(iv1)%nbt = xmatk(iv1)%nbt + 1
            xmatk(iv2)%nbt = xmatk(iv2)%nbt + 1
            xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
         ENDDO
      ENDIF
   ENDDO

   ! itype : 12 (not to use yet)
   ideb = ifin + 1
   ifin = ifin + ngphot
   DO i = ideb, ifin
      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
      ENDDO
      ir1 = react(i,1)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
   ENDDO

   ! itype : 13
   ideb = ifin + 1
   ifin = ifin + ngads
   DO i = ideb, ifin
      ir1 = react(i,1)
      iprod = react(i,4)
      IF (itype(i) == 13) THEN
         xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
         xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      ELSE IF (itype(i) == 113) THEN
         xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
         DO mm = 1, npg
            ivp = nspec + (iprod - nsp_n1 - 1) * npg + mm
            xmatk(ivp)%nbt = xmatk(ivp)%nbt + 1
            xmatk(ivp)%nbt = xmatk(ivp)%nbt + 1
            xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
         ENDDO
      ENDIF
   ENDDO

   ! itype : 14
   ideb = ifin + 1
   ifin = ifin + ngneut
   DO i = ideb, ifin
      ir1 = react(i,1)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      iprod = react(i,4)
      xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
   ENDDO

   ! itype : 15 (Do not use yet)
   ideb = ifin + 1
   ifin = ifin + nggdes
   DO i = ideb, ifin
      ir1 = react(i,1)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      iprod = react(i,4)
      xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
   ENDDO

   ! itype : 16 and 116
   ideb = ifin + 1
   ifin = ifin + ncrdes
   DO i = ideb, ifin
      ir1 = react(i,1)
      iprod = react(i,4)
      IF (itype(i) == 16) THEN
         xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
         xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      ELSE IF (itype(i) == 116) THEN
         DO mm = 1, npg
            ivp = nspec + (ir1 - nsp_n1 - 1) * npg + mm
            xmatk(ivp)%nbt = xmatk(ivp)%nbt + 1
            xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
         ENDDO
      ENDIF
   ENDDO

   ! itype : 17 and 117
   ideb = ifin + 1
   ifin = ifin + nphdes
   DO i = ideb, ifin
      ir1 = react(i,1)
      ik = react(i,0)
      IF (itype(i) == 17) THEN
         DO j = 4, ik+1
            iprod = react(i,j)
            xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
         ENDDO
         xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      ELSE IF (itype(i) == 117) THEN
         DO mm = 1, npg
            ivp = nspec + (ir1 - nsp_n1 - 1) * npg + mm
            xmatk(ivp)%nbt = xmatk(ivp)%nbt + 1
            DO j = 4, ik+1
               iprod = react(i,j)
               xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
            ENDDO
         ENDDO
      ENDIF
   ENDDO

   ! itype : 18
   ideb = ifin + 1
   ifin = ifin + ngevap
   DO i = ideb, ifin
      ir1 = react(i,1)
      iprod = react(i,4)
      IF (itype(i) == 18) THEN
         xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
         xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      ELSE IF (itype(i) == 118) THEN
         DO mm = 1, npg
            ivp = nspec + (ir1 - nsp_n1 - 1) * npg + mm
            xmatk(ivp)%nbt = xmatk(ivp)%nbt + 1
            xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
         ENDDO
      ENDIF
   ENDDO

   ! itype : 19
   ideb = ifin + 1
   ifin = ifin + ngchs
   DO i = ideb, ifin
      ik = react(i,0)
      ir1 = react(i,1)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      iprod = react(i,4)
      xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
      ! Rejection
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
   ENDDO

   ! itype : 20
   ideb = ifin + 1
   ifin = ifin + ngelri
   DO i = ideb, ifin
      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         xmatk(iprod)%nbt = xmatk(iprod)%nbt + 1
      ENDDO
      ir1 = react(i,1)
      xmatk(ir1)%nbt = xmatk(ir1)%nbt + 1
      ir2 = react(i,2)
      xmatk(ir2)%nbt = xmatk(ir2)%nbt + 1
   ENDDO

   WRITE (iscrn,*)
   WRITE (iscrn,*) "    * Number of chemical species             : ", nspec
   WRITE (iscrn,*) "    * Number of variables (chemistry solver) : ", n_var
   DO i = 1, n_var
      ALLOCATE (xmatk(i)%fdt(1:xmatk(i)%nbt))
      xmatk(i)%fdt(:)%io = 0
   ENDDO

   ! Fill rxns parameters (all but rate)
   ! reactant number 3 is arbitrarily set to nspec + 1
   ! (whose abundance is 1) IF it is not H

   ALLOCATE (indy(n_var))
   indy = 0

   !-------------------------------------------------------------------------
   !(0) formation of H2 and HD on grains [Old Style] (nh2for)  (cm3 s-1)
   !-------------------------------------------------------------------------

   DO i = 1, nh2for
      ir1 = react(i,1)
      ir2 = react(i,2)
      iprod = react(i,4)

      kkk = indy(iprod) + 1
      indy(iprod) = kkk
      xmatk(iprod)%fdt(kkk)%io = 2
      xmatk(iprod)%fdt(kkk)%it = itype(i)
      xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
      xmatk(iprod)%fdt(kkk)%r1 = ir1
      xmatk(iprod)%fdt(kkk)%r2 = ir2
      xmatk(iprod)%fdt(kkk)%r3 = 0
      xmatk(iprod)%fdt(kkk)%p1 = iprod
      xmatk(iprod)%fdt(kkk)%p2 = 0
      xmatk(iprod)%fdt(kkk)%p3 = 0
      xmatk(iprod)%fdt(kkk)%p4 = 0

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 2
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = ir2
      xmatk(ir1)%fdt(kkk)%r3 = 0
      xmatk(ir1)%fdt(kkk)%p1 = iprod
      xmatk(ir1)%fdt(kkk)%p2 = 0
      xmatk(ir1)%fdt(kkk)%p3 = 0
      xmatk(ir1)%fdt(kkk)%p4 = 0

      kkk = indy(ir2) + 1
      indy(ir2) = kkk
      xmatk(ir2)%fdt(kkk)%io = 2
      xmatk(ir2)%fdt(kkk)%it = itype(i)
      xmatk(ir2)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir2)%fdt(kkk)%r1 = ir1
      xmatk(ir2)%fdt(kkk)%r2 = ir2
      xmatk(ir2)%fdt(kkk)%r3 = 0
      xmatk(ir2)%fdt(kkk)%p1 = iprod
      xmatk(ir2)%fdt(kkk)%p2 = 0
      xmatk(ir2)%fdt(kkk)%p3 = 0
      xmatk(ir2)%fdt(kkk)%p4 = 0
   ENDDO

   !-------------------------------------------------------------------------
   ! (1) cosmic ray processes (ncrion)   (s-1)
   !-------------------------------------------------------------------------
   ideb = nh2for + 1
   ifin = nh2for + ncrion
   DO i = ideb, ifin
      ir1 = react(i,1)

      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         kkk = indy(iprod) + 1
         indy(iprod) = kkk
         xmatk(iprod)%fdt(kkk)%io = 1
         xmatk(iprod)%fdt(kkk)%it = itype(i)
         xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
         xmatk(iprod)%fdt(kkk)%r1 = ir1
         xmatk(iprod)%fdt(kkk)%r2 = n_var + 2
         xmatk(iprod)%fdt(kkk)%r3 = 0
         xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
         xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
         xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
         xmatk(iprod)%fdt(kkk)%p4 = react(i,7)
      ENDDO

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 1
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = n_var + 2
      xmatk(ir1)%fdt(kkk)%r3 = 0
      xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir1)%fdt(kkk)%p4 = react(i,7)
   ENDDO

   !-------------------------------------------------------------------------
   ! (2) secondary photon processes (npsion)   (s-1)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + npsion
   DO i = ideb, ifin
      ir1 = react(i,1)

      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         kkk = indy(iprod) + 1
         indy(iprod) = kkk
         xmatk(iprod)%fdt(kkk)%io = 1
         xmatk(iprod)%fdt(kkk)%it = itype(i)
         xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
         xmatk(iprod)%fdt(kkk)%r1 = ir1
         xmatk(iprod)%fdt(kkk)%r2 = n_var + 3
         xmatk(iprod)%fdt(kkk)%r3 = 0
         xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
         xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
         xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
         xmatk(iprod)%fdt(kkk)%p4 = react(i,7)
      ENDDO

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 1
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = n_var + 3
      xmatk(ir1)%fdt(kkk)%r3 = 0
      xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir1)%fdt(kkk)%p4 = react(i,7)
     ENDDO

   !-------------------------------------------------------------------------
   ! (3) radiative association  (nasrad)   (cm3 s-1)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + nasrad
   DO i = ideb, ifin
      ir1 = react(i,1)
      ir2 = react(i,2)
      iprod = react(i,4)

      kkk = indy(iprod) + 1
      indy(iprod) = kkk
      xmatk(iprod)%fdt(kkk)%io = 2
      xmatk(iprod)%fdt(kkk)%it = itype(i)
      xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
      xmatk(iprod)%fdt(kkk)%r1 = ir1
      xmatk(iprod)%fdt(kkk)%r2 = ir2
      xmatk(iprod)%fdt(kkk)%r3 = 0
      xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
      xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
      xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
      xmatk(iprod)%fdt(kkk)%p4 = react(i,7)

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 2
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = ir2
      xmatk(ir1)%fdt(kkk)%r3 = 0
      xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir1)%fdt(kkk)%p4 = react(i,7)

      kkk = indy(ir2) + 1
      indy(ir2) = kkk
      xmatk(ir2)%fdt(kkk)%io = 2
      xmatk(ir2)%fdt(kkk)%it = itype(i)
      xmatk(ir2)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir2)%fdt(kkk)%r1 = ir1
      xmatk(ir2)%fdt(kkk)%r2 = ir2
      xmatk(ir2)%fdt(kkk)%r3 = 0
      xmatk(ir2)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir2)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir2)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir2)%fdt(kkk)%p4 = react(i,7)
   ENDDO

   !-------------------------------------------------------------------------
   ! (4) gas phase reactions (nrest)   (cm3 s-1)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + nrest
   DO i = ideb, ifin
      ir1 = react(i,1)
      ir2 = react(i,2)

      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         kkk = indy(iprod) + 1
         indy(iprod) = kkk
         xmatk(iprod)%fdt(kkk)%io = 2
         xmatk(iprod)%fdt(kkk)%it = itype(i)
         xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
         xmatk(iprod)%fdt(kkk)%r1 = ir1
         xmatk(iprod)%fdt(kkk)%r2 = ir2
         xmatk(iprod)%fdt(kkk)%r3 = 0
         xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
         xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
         xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
         xmatk(iprod)%fdt(kkk)%p4 = react(i,7)
      ENDDO

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 2
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = ir2
      xmatk(ir1)%fdt(kkk)%r3 = 0
      xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir1)%fdt(kkk)%p4 = react(i,7)

      kkk = indy(ir2) + 1
      indy(ir2) = kkk
      xmatk(ir2)%fdt(kkk)%io = 2
      xmatk(ir2)%fdt(kkk)%it = itype(i)
      xmatk(ir2)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir2)%fdt(kkk)%r1 = ir1
      xmatk(ir2)%fdt(kkk)%r2 = ir2
      xmatk(ir2)%fdt(kkk)%r3 = 0
      xmatk(ir2)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir2)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir2)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir2)%fdt(kkk)%p4 = react(i,7)
   ENDDO

   !-------------------------------------------------------------------------
   ! (6) Endothermal reactions with H2 (nh2endo)   (cm3 s-1)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + nh2endo
   DO i = ideb, ifin
      ir1 = react(i,1)
      ir2 = react(i,2)

      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         kkk = indy(iprod) + 1
         indy(iprod) = kkk
         xmatk(iprod)%fdt(kkk)%io = 2
         xmatk(iprod)%fdt(kkk)%it = itype(i)
         xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
         xmatk(iprod)%fdt(kkk)%r1 = ir1
         xmatk(iprod)%fdt(kkk)%r2 = ir2
         xmatk(iprod)%fdt(kkk)%r3 = 0
         xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
         xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
         xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
         xmatk(iprod)%fdt(kkk)%p4 = react(i,7)
      ENDDO

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 2
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = ir2
      xmatk(ir1)%fdt(kkk)%r3 = 0
      xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir1)%fdt(kkk)%p4 = react(i,7)

      kkk = indy(ir2) + 1
      indy(ir2) = kkk
      xmatk(ir2)%fdt(kkk)%io = 2
      xmatk(ir2)%fdt(kkk)%it = itype(i)
      xmatk(ir2)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir2)%fdt(kkk)%r1 = ir1
      xmatk(ir2)%fdt(kkk)%r2 = ir2
      xmatk(ir2)%fdt(kkk)%r3 = 0
      xmatk(ir2)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir2)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir2)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir2)%fdt(kkk)%p4 = react(i,7)
   ENDDO

   !-------------------------------------------------------------------------
   ! (5) photoreactions (external fit) (nphot_f)    (s-1)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + nphot_f
   DO i = ideb, ifin
      ir1 = react(i,1)

      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         kkk = indy(iprod) + 1
         indy(iprod) = kkk
         xmatk(iprod)%fdt(kkk)%io = 1
         xmatk(iprod)%fdt(kkk)%it = itype(i)
         xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
         xmatk(iprod)%fdt(kkk)%r1 = ir1
         xmatk(iprod)%fdt(kkk)%r2 = n_var + 1
         xmatk(iprod)%fdt(kkk)%r3 = 0
         xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
         xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
         xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
         xmatk(iprod)%fdt(kkk)%p4 = react(i,7)
      ENDDO

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 1
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = n_var + 1
      xmatk(ir1)%fdt(kkk)%r3 = 0
      xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir1)%fdt(kkk)%p4 = react(i,7)
   ENDDO

   !-------------------------------------------------------------------------
   ! (7) photoreactions avec integration directe (nphot_i)    (s-1)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + nphot_i
   DO i = ideb, ifin
      ir1 = react(i,1)

      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         kkk = indy(iprod) + 1
         indy(iprod) = kkk
         xmatk(iprod)%fdt(kkk)%io = 1
         xmatk(iprod)%fdt(kkk)%it = itype(i)
         xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
         xmatk(iprod)%fdt(kkk)%r1 = ir1
         xmatk(iprod)%fdt(kkk)%r2 = n_var + 1
         xmatk(iprod)%fdt(kkk)%r3 = 0
         xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
         xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
         xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
         xmatk(iprod)%fdt(kkk)%p4 = react(i,7)
      ENDDO

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 1
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = n_var + 1
      xmatk(ir1)%fdt(kkk)%r3 = 0
      xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir1)%fdt(kkk)%p4 = react(i,7)
   ENDDO

   !-------------------------------------------------------------------------
   ! (8) 3 body reactions (n3body)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + n3body
   DO i = ideb, ifin
      ir1 = react(i,1)
      ir2 = react(i,2)
      ir3 = react(i,3)

      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         kkk = indy(iprod) + 1
         indy(iprod) = kkk
         xmatk(iprod)%fdt(kkk)%io = 3
         xmatk(iprod)%fdt(kkk)%it = itype(i)
         xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
         xmatk(iprod)%fdt(kkk)%r1 = ir1
         xmatk(iprod)%fdt(kkk)%r2 = ir2
         xmatk(iprod)%fdt(kkk)%r3 = ir3
         xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
         xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
         xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
         xmatk(iprod)%fdt(kkk)%p4 = react(i,7)
      ENDDO

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 3
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = ir2
      xmatk(ir1)%fdt(kkk)%r3 = ir3
      xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir1)%fdt(kkk)%p4 = react(i,7)
      kkk = indy(ir2) + 1
      indy(ir2) = kkk
      xmatk(ir2)%fdt(kkk)%io = 3
      xmatk(ir2)%fdt(kkk)%it = itype(i)
      xmatk(ir2)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir2)%fdt(kkk)%r1 = ir1
      xmatk(ir2)%fdt(kkk)%r2 = ir2
      xmatk(ir2)%fdt(kkk)%r3 = ir3
      xmatk(ir2)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir2)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir2)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir2)%fdt(kkk)%p4 = react(i,7)
      kkk = indy(ir3) + 1
      indy(ir3) = kkk
      xmatk(ir3)%fdt(kkk)%io = 3
      xmatk(ir3)%fdt(kkk)%it = itype(i)
      xmatk(ir3)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir3)%fdt(kkk)%r1 = ir1
      xmatk(ir3)%fdt(kkk)%r2 = ir2
      xmatk(ir3)%fdt(kkk)%r3 = ir3
      xmatk(ir3)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir3)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir3)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir3)%fdt(kkk)%p4 = react(i,7)
   ENDDO

   !-------------------------------------------------------------------------
   ! (101...) exceptions (ncpart)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + ncpart
   DO i = ideb, ifin
      ir1 = react(i,1)
      ir2 = react(i,2)

      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         kkk = indy(iprod) + 1
         indy(iprod) = kkk
         xmatk(iprod)%fdt(kkk)%io = 3
         xmatk(iprod)%fdt(kkk)%it = itype(i)
         xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
         xmatk(iprod)%fdt(kkk)%r1 = ir1
         xmatk(iprod)%fdt(kkk)%r2 = ir2
         xmatk(iprod)%fdt(kkk)%r3 = 0
         xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
         xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
         xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
         xmatk(iprod)%fdt(kkk)%p4 = react(i,7)
      ENDDO

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 2
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = ir2
      xmatk(ir1)%fdt(kkk)%r3 = 0
      xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir1)%fdt(kkk)%p4 = react(i,7)

      kkk = indy(ir2) + 1
      indy(ir2) = kkk
      xmatk(ir2)%fdt(kkk)%io = 2
      xmatk(ir2)%fdt(kkk)%it = itype(i)
      xmatk(ir2)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir2)%fdt(kkk)%r1 = ir1
      xmatk(ir2)%fdt(kkk)%r2 = ir2
      xmatk(ir2)%fdt(kkk)%r3 = 0
      xmatk(ir2)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir2)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir2)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir2)%fdt(kkk)%p4 = react(i,7)
   ENDDO

   !-------------------------------------------------------------------------
   ! (11) reactions on grain surface (ngsurf)
   !-------------------------------------------------------------------------
   ! 10 Feb 2008 - Those reaction are built for only one product
   !               This could be changed later

   ideb = ifin + 1
   ifin = ifin + ngsurf
   DO i = ideb, ifin
      ir1 = react(i,1)
      ir2 = react(i,2)

      IF (itype(i) == 11) THEN
         iprod = react(i,4)
         DO mm = 1, npg
            iv2 = nspec + (ir2 - nsp_n1 - 1) * npg + mm

            kkk = indy(iprod) + 1
            indy(iprod) = kkk
            xmatk(iprod)%fdt(kkk)%io = 2
            xmatk(iprod)%fdt(kkk)%it = itype(i)
            xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
            xmatk(iprod)%fdt(kkk)%r1 = ir1
            xmatk(iprod)%fdt(kkk)%r2 = iv2
            xmatk(iprod)%fdt(kkk)%r3 = 0
            xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
            xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
            xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
            xmatk(iprod)%fdt(kkk)%p4 = react(i,7)

            kkk = indy(ir1) + 1
            indy(ir1) = kkk
            xmatk(ir1)%fdt(kkk)%io = 2
            xmatk(ir1)%fdt(kkk)%it = itype(i)
            xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
            xmatk(ir1)%fdt(kkk)%r1 = ir1
            xmatk(ir1)%fdt(kkk)%r2 = iv2
            xmatk(ir1)%fdt(kkk)%r3 = 0
            xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
            xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
            xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
            xmatk(ir1)%fdt(kkk)%p4 = react(i,7)

            kkk = indy(iv2) + 1
            indy(iv2) = kkk
            xmatk(iv2)%fdt(kkk)%io = 2
            xmatk(iv2)%fdt(kkk)%it = itype(i)
            xmatk(iv2)%fdt(kkk)%k  = 0.0_dp
            xmatk(iv2)%fdt(kkk)%r1 = ir1
            xmatk(iv2)%fdt(kkk)%r2 = iv2
            xmatk(iv2)%fdt(kkk)%r3 = 0
            xmatk(iv2)%fdt(kkk)%p1 = react(i,4)
            xmatk(iv2)%fdt(kkk)%p2 = react(i,5)
            xmatk(iv2)%fdt(kkk)%p3 = react(i,6)
            xmatk(iv2)%fdt(kkk)%p4 = react(i,7)
         ENDDO
      ELSE IF (itype(i) == 111) THEN
         iprod = react(i,4)
         DO mm = 1, npg
            iv1 = nspec + (ir1 - nsp_n1 - 1) * npg + mm
            iv2 = nspec + (ir2 - nsp_n1 - 1) * npg + mm

            kkk = indy(iprod) + 1
            indy(iprod) = kkk
            xmatk(iprod)%fdt(kkk)%io = 2
            xmatk(iprod)%fdt(kkk)%it = itype(i)
            xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
            xmatk(iprod)%fdt(kkk)%r1 = iv1
            xmatk(iprod)%fdt(kkk)%r2 = iv2
            xmatk(iprod)%fdt(kkk)%r3 = 0
            xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
            xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
            xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
            xmatk(iprod)%fdt(kkk)%p4 = react(i,7)

            kkk = indy(iv1) + 1
            indy(iv1) = kkk
            xmatk(iv1)%fdt(kkk)%io = 2
            xmatk(iv1)%fdt(kkk)%it = itype(i)
            xmatk(iv1)%fdt(kkk)%k  = 0.0_dp
            xmatk(iv1)%fdt(kkk)%r1 = iv1
            xmatk(iv1)%fdt(kkk)%r2 = iv2
            xmatk(iv1)%fdt(kkk)%r3 = 0
            xmatk(iv1)%fdt(kkk)%p1 = react(i,4)
            xmatk(iv1)%fdt(kkk)%p2 = react(i,5)
            xmatk(iv1)%fdt(kkk)%p3 = react(i,6)
            xmatk(iv1)%fdt(kkk)%p4 = react(i,7)

            kkk = indy(iv2) + 1
            indy(iv2) = kkk
            xmatk(iv2)%fdt(kkk)%io = 2
            xmatk(iv2)%fdt(kkk)%it = itype(i)
            xmatk(iv2)%fdt(kkk)%k  = 0.0_dp
            xmatk(iv2)%fdt(kkk)%r1 = iv1
            xmatk(iv2)%fdt(kkk)%r2 = iv2
            xmatk(iv2)%fdt(kkk)%r3 = 0
            xmatk(iv2)%fdt(kkk)%p1 = react(i,4)
            xmatk(iv2)%fdt(kkk)%p2 = react(i,5)
            xmatk(iv2)%fdt(kkk)%p3 = react(i,6)
            xmatk(iv2)%fdt(kkk)%p4 = react(i,7)
         ENDDO
      ENDIF
   ENDDO

   !-------------------------------------------------------------------------
   ! (12) photoreactions on grain surface (ngphot)
   !-------------------------------------------------------------------------
   !  11 Feb 2008: not revised yet. Do not use

   ideb = ifin + 1
   ifin = ifin + ngphot
   DO i = ideb, ifin
      ir1 = react(i,1)

      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         kkk = indy(iprod) + 1
         indy(iprod) = kkk
         xmatk(iprod)%fdt(kkk)%io = 1
         xmatk(iprod)%fdt(kkk)%it = itype(i)
         xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
         xmatk(iprod)%fdt(kkk)%r1 = ir1
         xmatk(iprod)%fdt(kkk)%r2 = n_var + 1
         xmatk(iprod)%fdt(kkk)%r3 = 0
         xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
         xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
         xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
         xmatk(iprod)%fdt(kkk)%p4 = react(i,7)
      ENDDO

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 1
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = n_var + 1
      xmatk(ir1)%fdt(kkk)%r3 = 0
      xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir1)%fdt(kkk)%p4 = react(i,7)
   ENDDO

   !-------------------------------------------------------------------------
   ! (13) adsorption on to grains (ngads)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + ngads
   DO i = ideb, ifin
      ir1 = react(i,1)
      IF (itype(i) == 13) THEN

         iprod = react(i,4)
         kkk = indy(iprod) + 1
         indy(iprod) = kkk
         xmatk(iprod)%fdt(kkk)%io = 1
         xmatk(iprod)%fdt(kkk)%it = itype(i)
         xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
         xmatk(iprod)%fdt(kkk)%r1 = ir1
         xmatk(iprod)%fdt(kkk)%r2 = n_var + 4
         xmatk(iprod)%fdt(kkk)%r3 = 0
         xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
         xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
         xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
         xmatk(iprod)%fdt(kkk)%p4 = react(i,7)

         kkk = indy(ir1) + 1
         indy(ir1) = kkk
         xmatk(ir1)%fdt(kkk)%io = 1
         xmatk(ir1)%fdt(kkk)%it = itype(i)
         xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
         xmatk(ir1)%fdt(kkk)%r1 = ir1
         xmatk(ir1)%fdt(kkk)%r2 = n_var + 4
         xmatk(ir1)%fdt(kkk)%r3 = 0
         xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
         xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
         xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
         xmatk(ir1)%fdt(kkk)%p4 = react(i,7)
      ELSE IF (itype(i) == 113) THEN
         kkk = indy(ir1) + 1
         indy(ir1) = kkk
         xmatk(ir1)%fdt(kkk)%io = 1
         xmatk(ir1)%fdt(kkk)%it = itype(i)
         xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
         xmatk(ir1)%fdt(kkk)%r1 = ir1
         xmatk(ir1)%fdt(kkk)%r2 = n_var + 4
         xmatk(ir1)%fdt(kkk)%r3 = 0
         xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
         xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
         xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
         xmatk(ir1)%fdt(kkk)%p4 = react(i,7)

         iprod = react(i,4)
         DO mm = 1, npg
            ivp = nspec + (iprod - nsp_n1 - 1) * npg + mm

            kkk = indy(ivp) + 1
            indy(ivp) = kkk
            xmatk(ivp)%fdt(kkk)%io = 1
            xmatk(ivp)%fdt(kkk)%it = itype(i)
            xmatk(ivp)%fdt(kkk)%k  = 0.0_dp
            xmatk(ivp)%fdt(kkk)%r1 = ir1
            xmatk(ivp)%fdt(kkk)%r2 = n_var + 4
            xmatk(ivp)%fdt(kkk)%r3 = 0
            xmatk(ivp)%fdt(kkk)%p1 = react(i,4)
            xmatk(ivp)%fdt(kkk)%p2 = react(i,5)
            xmatk(ivp)%fdt(kkk)%p3 = react(i,6)
            xmatk(ivp)%fdt(kkk)%p4 = react(i,7)
            kkk = indy(ivp) + 1
            indy(ivp) = kkk
            xmatk(ivp)%fdt(kkk)%io = 2
            xmatk(ivp)%fdt(kkk)%it = itype(i)
            xmatk(ivp)%fdt(kkk)%k  = 0.0_dp
            xmatk(ivp)%fdt(kkk)%r1 = ir1
            xmatk(ivp)%fdt(kkk)%r2 = ivp
            xmatk(ivp)%fdt(kkk)%r3 = 0
            xmatk(ivp)%fdt(kkk)%p1 = react(i,4)
            xmatk(ivp)%fdt(kkk)%p2 = react(i,5)
            xmatk(ivp)%fdt(kkk)%p3 = react(i,6)
            xmatk(ivp)%fdt(kkk)%p4 = react(i,7)

            kkk = indy(ir1) + 1
            indy(ir1) = kkk
            xmatk(ir1)%fdt(kkk)%io = 2
            xmatk(ir1)%fdt(kkk)%it = itype(i)
            xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
            xmatk(ir1)%fdt(kkk)%r1 = ir1
            xmatk(ir1)%fdt(kkk)%r2 = ivp
            xmatk(ir1)%fdt(kkk)%r3 = 0
            xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
            xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
            xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
            xmatk(ir1)%fdt(kkk)%p4 = react(i,7)
         ENDDO
      ENDIF
   ENDDO

   !-------------------------------------------------------------------------
   ! (14) neutralization (ngneut)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + ngneut
   DO i = ideb, ifin
      ir1 = react(i,1)

      iprod = react(i,4)
      kkk = indy(iprod) + 1
      indy(iprod) = kkk
      xmatk(iprod)%fdt(kkk)%io = 1
      xmatk(iprod)%fdt(kkk)%it = itype(i)
      xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
      xmatk(iprod)%fdt(kkk)%r1 = ir1
      xmatk(iprod)%fdt(kkk)%r2 = n_var + 4
      xmatk(iprod)%fdt(kkk)%r3 = 0
      xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
      xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
      xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
      xmatk(iprod)%fdt(kkk)%p4 = react(i,7)

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 1
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = n_var + 4
      xmatk(ir1)%fdt(kkk)%r3 = 0
      xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir1)%fdt(kkk)%p4 = react(i,7)
   ENDDO

   !-------------------------------------------------------------------------
   ! (15) explosive desorption from grains (nggdes)
   !    ref: Shalabiea & Greenberg 1994
   !-------------------------------------------------------------------------
   !  11 Feb 2008: not revised yet. Do not use
   ideb = ifin + 1
   ifin = ifin + nggdes
   DO i = ideb, ifin
      ir1 = react(i,1)
      IF (itype(i) == 15) THEN
         iprod = react(i,4)
         kkk = indy(iprod) + 1
         indy(iprod) = kkk
         xmatk(iprod)%fdt(kkk)%io = 1
         xmatk(iprod)%fdt(kkk)%it = itype(i)
         xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
         xmatk(iprod)%fdt(kkk)%r1 = ir1
         xmatk(iprod)%fdt(kkk)%r2 = n_var + 4
         xmatk(iprod)%fdt(kkk)%r3 = 0
         xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
         xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
         xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
         xmatk(iprod)%fdt(kkk)%p4 = react(i,7)

         kkk = indy(ir1) + 1
         indy(ir1) = kkk
         xmatk(ir1)%fdt(kkk)%io = 1
         xmatk(ir1)%fdt(kkk)%it = itype(i)
         xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
         xmatk(ir1)%fdt(kkk)%r1 = ir1
         xmatk(ir1)%fdt(kkk)%r2 = n_var + 4
         xmatk(ir1)%fdt(kkk)%r3 = 0
         xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
         xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
         xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
         xmatk(ir1)%fdt(kkk)%p4 = react(i,7)
      ENDIF
   ENDDO

   !-------------------------------------------------------------------------
   ! (16) cosmic ray induced desorption from grains (ncrdes)
   !      (sans les photons secondaires)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + ncrdes
   DO i = ideb, ifin
      ir1 = react(i,1)
      iprod = react(i,4)

      IF (itype(i) == 16) THEN
         kkk = indy(iprod) + 1
         indy(iprod) = kkk
         xmatk(iprod)%fdt(kkk)%io = 1
         xmatk(iprod)%fdt(kkk)%it = itype(i)
         xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
         xmatk(iprod)%fdt(kkk)%r1 = ir1
         xmatk(iprod)%fdt(kkk)%r2 = n_var + 2
         xmatk(iprod)%fdt(kkk)%r3 = 0
         xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
         xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
         xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
         xmatk(iprod)%fdt(kkk)%p4 = react(i,7)

         kkk = indy(ir1) + 1
         indy(ir1) = kkk
         xmatk(ir1)%fdt(kkk)%io = 1
         xmatk(ir1)%fdt(kkk)%it = itype(i)
         xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
         xmatk(ir1)%fdt(kkk)%r1 = ir1
         xmatk(ir1)%fdt(kkk)%r2 = n_var + 2
         xmatk(ir1)%fdt(kkk)%r3 = 0
         xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
         xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
         xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
         xmatk(ir1)%fdt(kkk)%p4 = react(i,7)
      ELSE IF (itype(i) == 116) THEN
         DO mm = 1, npg
            ivp = nspec + (ir1 - nsp_n1 - 1) * npg + mm
            kkk = indy(ivp) + 1
            indy(ivp) = kkk
            xmatk(ivp)%fdt(kkk)%io = 1
            xmatk(ivp)%fdt(kkk)%it = itype(i)
            xmatk(ivp)%fdt(kkk)%k  = 0.0_dp
            xmatk(ivp)%fdt(kkk)%r1 = ivp
            xmatk(ivp)%fdt(kkk)%r2 = n_var + 2
            xmatk(ivp)%fdt(kkk)%r3 = 0
            xmatk(ivp)%fdt(kkk)%p1 = react(i,4)
            xmatk(ivp)%fdt(kkk)%p2 = react(i,5)
            xmatk(ivp)%fdt(kkk)%p3 = react(i,6)
            xmatk(ivp)%fdt(kkk)%p4 = react(i,7)

            kkk = indy(iprod) + 1
            indy(iprod) = kkk
            xmatk(iprod)%fdt(kkk)%io = 1
            xmatk(iprod)%fdt(kkk)%it = itype(i)
            xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
            xmatk(iprod)%fdt(kkk)%r1 = ivp
            xmatk(iprod)%fdt(kkk)%r2 = n_var + 2
            xmatk(iprod)%fdt(kkk)%r3 = 0
            xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
            xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
            xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
            xmatk(iprod)%fdt(kkk)%p4 = react(i,7)
         ENDDO
      ENDIF
   ENDDO

   !-------------------------------------------------------------------------
   ! (17) photodesorption from grains (nphdes)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + nphdes
   DO i = ideb, ifin
      ir1 = react(i,1)

      IF (itype(i) == 17) THEN
         kkk = indy(ir1) + 1
         indy(ir1) = kkk
         xmatk(ir1)%fdt(kkk)%io = 1
         xmatk(ir1)%fdt(kkk)%it = itype(i)
         xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
         xmatk(ir1)%fdt(kkk)%r1 = ir1
         xmatk(ir1)%fdt(kkk)%r2 = n_var + 1
         xmatk(ir1)%fdt(kkk)%r3 = 0
         xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
         xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
         xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
         xmatk(ir1)%fdt(kkk)%p4 = react(i,7)

         ik = react(i,0)
         DO j = 4, ik+1
            iprod = react(i,j)
            kkk = indy(iprod) + 1
            indy(iprod) = kkk
            xmatk(iprod)%fdt(kkk)%io = 1
            xmatk(iprod)%fdt(kkk)%it = itype(i)
            xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
            xmatk(iprod)%fdt(kkk)%r1 = ir1
            xmatk(iprod)%fdt(kkk)%r2 = n_var + 1
            xmatk(iprod)%fdt(kkk)%r3 = 0
            xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
            xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
            xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
            xmatk(iprod)%fdt(kkk)%p4 = react(i,7)
         ENDDO
      ELSE IF (itype(i) == 117) THEN
         DO mm = 1, npg
            ivp = nspec + (ir1 - nsp_n1 - 1) * npg + mm
            kkk = indy(ivp) + 1
            indy(ivp) = kkk
            xmatk(ivp)%fdt(kkk)%io = 1
            xmatk(ivp)%fdt(kkk)%it = itype(i)
            xmatk(ivp)%fdt(kkk)%k  = 0.0_dp
            xmatk(ivp)%fdt(kkk)%r1 = ivp
            xmatk(ivp)%fdt(kkk)%r2 = n_var + 1
            xmatk(ivp)%fdt(kkk)%r3 = 0
            xmatk(ivp)%fdt(kkk)%p1 = react(i,4)
            xmatk(ivp)%fdt(kkk)%p2 = react(i,5)
            xmatk(ivp)%fdt(kkk)%p3 = react(i,6)
            xmatk(ivp)%fdt(kkk)%p4 = react(i,7)

            ik = react(i,0)
            DO j = 4, ik+1
               iprod = react(i,j)
               kkk = indy(iprod) + 1
               indy(iprod) = kkk
               xmatk(iprod)%fdt(kkk)%io = 1
               xmatk(iprod)%fdt(kkk)%it = itype(i)
               xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
               xmatk(iprod)%fdt(kkk)%r1 = ivp
               xmatk(iprod)%fdt(kkk)%r2 = n_var + 1
               xmatk(iprod)%fdt(kkk)%r3 = 0
               xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
               xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
               xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
               xmatk(iprod)%fdt(kkk)%p4 = react(i,7)
            ENDDO
         ENDDO
      ENDIF
   ENDDO

   !-------------------------------------------------------------------------
   ! (18) evaporation from grains (ngevap)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + ngevap
   DO i = ideb, ifin
      ir1 = react(i,1)
      iprod = react(i,4)

      IF (itype(i) == 18) THEN
         kkk = indy(iprod) + 1
         indy(iprod) = kkk
         xmatk(iprod)%fdt(kkk)%io = 1
         xmatk(iprod)%fdt(kkk)%it = itype(i)
         xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
         xmatk(iprod)%fdt(kkk)%r1 = ir1
         xmatk(iprod)%fdt(kkk)%r2 = n_var + 4
         xmatk(iprod)%fdt(kkk)%r3 = 0
         xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
         xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
         xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
         xmatk(iprod)%fdt(kkk)%p4 = react(i,7)

         kkk = indy(ir1) + 1
         indy(ir1) = kkk
         xmatk(ir1)%fdt(kkk)%io = 1
         xmatk(ir1)%fdt(kkk)%it = itype(i)
         xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
         xmatk(ir1)%fdt(kkk)%r1 = ir1
         xmatk(ir1)%fdt(kkk)%r2 = n_var + 4
         xmatk(ir1)%fdt(kkk)%r3 = 0
         xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
         xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
         xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
         xmatk(ir1)%fdt(kkk)%p4 = react(i,7)
      ELSE IF (itype(i) == 118) THEN
         DO mm = 1, npg
            ivp = nspec + (ir1 - nsp_n1 - 1) * npg + mm
            kkk = indy(ivp) + 1
            indy(ivp) = kkk
            xmatk(ivp)%fdt(kkk)%io = 1
            xmatk(ivp)%fdt(kkk)%it = itype(i)
            xmatk(ivp)%fdt(kkk)%k  = 0.0_dp
            xmatk(ivp)%fdt(kkk)%r1 = ivp
            xmatk(ivp)%fdt(kkk)%r2 = n_var + 4
            xmatk(ivp)%fdt(kkk)%r3 = 0
            xmatk(ivp)%fdt(kkk)%p1 = react(i,4)
            xmatk(ivp)%fdt(kkk)%p2 = react(i,5)
            xmatk(ivp)%fdt(kkk)%p3 = react(i,6)
            xmatk(ivp)%fdt(kkk)%p4 = react(i,7)

            kkk = indy(iprod) + 1
            indy(iprod) = kkk
            xmatk(iprod)%fdt(kkk)%io = 1
            xmatk(iprod)%fdt(kkk)%it = itype(i)
            xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
            xmatk(iprod)%fdt(kkk)%r1 = ivp
            xmatk(iprod)%fdt(kkk)%r2 = n_var + 4
            xmatk(iprod)%fdt(kkk)%r3 = 0
            xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
            xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
            xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
            xmatk(iprod)%fdt(kkk)%p4 = react(i,7)
         ENDDO
      ENDIF
   ENDDO

   !-------------------------------------------------------------------------
   ! (19) Chemisorption on grains (ngchs)   (cm3 s-1)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + ngchs
   DO i = ideb, ifin
      ir1 = react(i,1)
      ir2 = react(i,2)

      iprod = react(i,4)
      kkk = indy(iprod) + 1
      indy(iprod) = kkk
      xmatk(iprod)%fdt(kkk)%io = 1
      xmatk(iprod)%fdt(kkk)%it = itype(i)
      xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
      xmatk(iprod)%fdt(kkk)%r1 = ir1
      xmatk(iprod)%fdt(kkk)%r2 = ir2
      xmatk(iprod)%fdt(kkk)%r3 = 0
      xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
      xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
      xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
      xmatk(iprod)%fdt(kkk)%p4 = react(i,7)

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 1
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = ir2
      xmatk(ir1)%fdt(kkk)%r3 = 0
      xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir1)%fdt(kkk)%p4 = react(i,7)

      ! Rejection
      kkk = indy(iprod) + 1
      indy(iprod) = kkk
      xmatk(iprod)%fdt(kkk)%io = 2
      xmatk(iprod)%fdt(kkk)%it = itype(i)
      xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
      xmatk(iprod)%fdt(kkk)%r1 = ir1
      xmatk(iprod)%fdt(kkk)%r2 = iprod
      xmatk(iprod)%fdt(kkk)%r3 = 0
      xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
      xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
      xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
      xmatk(iprod)%fdt(kkk)%p4 = react(i,7)

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 2
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = iprod
      xmatk(ir1)%fdt(kkk)%r3 = 0
      xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir1)%fdt(kkk)%p4 = react(i,7)

   ENDDO

   !-------------------------------------------------------------------------
   ! (20) Eley-Rideal process (ngelri)   (cm3 s-1)
   !-------------------------------------------------------------------------
   ideb = ifin + 1
   ifin = ifin + ngelri
   DO i = ideb, ifin
      ir1 = react(i,1)
      ir2 = react(i,2)

      ik = react(i,0)
      DO j = 4, ik+1
         iprod = react(i,j)
         kkk = indy(iprod) + 1
         indy(iprod) = kkk
         xmatk(iprod)%fdt(kkk)%io = 2
         xmatk(iprod)%fdt(kkk)%it = itype(i)
         xmatk(iprod)%fdt(kkk)%k  = 0.0_dp
         xmatk(iprod)%fdt(kkk)%r1 = ir1
         xmatk(iprod)%fdt(kkk)%r2 = ir2
         xmatk(iprod)%fdt(kkk)%r3 = 0
         xmatk(iprod)%fdt(kkk)%p1 = react(i,4)
         xmatk(iprod)%fdt(kkk)%p2 = react(i,5)
         xmatk(iprod)%fdt(kkk)%p3 = react(i,6)
         xmatk(iprod)%fdt(kkk)%p4 = react(i,7)
      ENDDO

      kkk = indy(ir1) + 1
      indy(ir1) = kkk
      xmatk(ir1)%fdt(kkk)%io = 2
      xmatk(ir1)%fdt(kkk)%it = itype(i)
      xmatk(ir1)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir1)%fdt(kkk)%r1 = ir1
      xmatk(ir1)%fdt(kkk)%r2 = ir2
      xmatk(ir1)%fdt(kkk)%r3 = 0
      xmatk(ir1)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir1)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir1)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir1)%fdt(kkk)%p4 = react(i,7)

      kkk = indy(ir2) + 1
      indy(ir2) = kkk
      xmatk(ir2)%fdt(kkk)%io = 2
      xmatk(ir2)%fdt(kkk)%it = itype(i)
      xmatk(ir2)%fdt(kkk)%k  = 0.0_dp
      xmatk(ir2)%fdt(kkk)%r1 = ir1
      xmatk(ir2)%fdt(kkk)%r2 = ir2
      xmatk(ir2)%fdt(kkk)%r3 = 0
      xmatk(ir2)%fdt(kkk)%p1 = react(i,4)
      xmatk(ir2)%fdt(kkk)%p2 = react(i,5)
      xmatk(ir2)%fdt(kkk)%p3 = react(i,6)
      xmatk(ir2)%fdt(kkk)%p4 = react(i,7)
   ENDDO

   ! Identify some hard to find reactions
   ! CO formation by C + OH (used for formation - excitation) - JLB 10 VI 2011
   k_fco_c_oh = 0
   t_fco_c_oh = 0.0_dp
   ! CO formation by HCO+ + e- (used for formation - excitation) - JLB 2 II 2012
   k_fco_hcop = 0
   t_fco_hcop = 0.0_dp
   DO k = 1, xmatk(i_co)%nbt
     IF (xmatk(i_co)%fdt(k)%r1 == i_c .AND. xmatk(i_co)%fdt(k)%r2 == i_oh) THEN
        k_fco_c_oh = k
        t_fco_c_oh = (enth(i_c) + enth(i_oh) - enth(i_co) - enth(i_h)) * calev * everg / xk
        print *, " C + OH -> CO + H", k_fco_c_oh, t_fco_c_oh
!       EXIT
     ENDIF
     IF (xmatk(i_co)%fdt(k)%r1 == i_hcop .AND. xmatk(i_co)%fdt(k)%r2 == nspec .AND. &
         xmatk(i_co)%fdt(k)%p1 == i_h .AND. xmatk(i_co)%fdt(k)%p2 == i_co) THEN
        k_fco_hcop = k
        t_fco_hcop = (enth(i_hcop) + enth(nspec) - enth(i_co) - enth(i_h)) * calev * everg / xk
        print *, " HCO+ + e- -> CO + H", k_fco_hcop, t_fco_hcop
!       EXIT
     ENDIF
   ENDDO
   ! Reset to 0 to supress that scecific excitation
   ! k_fco_c_oh = 0
   ! k_fco_hcop = 0

   WRITE (iscrn,*) "    * End of chemistry reading"

END SUBROUTINE LECTUR

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!%%%%%%%%%%%%%%%
SUBROUTINE ORDON
!%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA

   IMPLICIT NONE

   CHARACTER (LEN=8)               :: r1, r2, r3, p1, p2, p3, p4

   REAL (KIND=dp), DIMENSION (nqm) :: dee
   INTEGER, DIMENSION (nqm,0:7)    :: lreact
   INTEGER, DIMENSION (nqm)        :: ntype
   REAL (KIND=dp), DIMENSION (nqm) :: gammm
   REAL (KIND=dp), DIMENSION (nqm) :: palph
   REAL (KIND=dp), DIMENSION (nqm) :: bett

   INTEGER           :: k, j, ity
   INTEGER           :: numero, ideb, ifin

! identify and re-order the chemical reactions:

!  WARNING : Type 6 reactions are treated BEFORE type 5
!            (this is because they result from a splitting of type 4)

! (0) formation of h2 and hd on grains [old style] (nh2for)
! (1) cosmic ray processes (ncrion)
! (2) secondary photon processes (npsion)
! (3) radiative association  (nasrad)
! (4) gas phase reactions (nrest)
! (6) Endothermal reactions with H2 (nh2endo) (NEW!)
! (5) photoreactions (external fit) (nphot_f)
! (7) photoreactions (direct integration) (nphot_i)
! (8) 3 body reactions
! (101...) exceptions (ncpart)

! (11) reactions on grain surface (ngsurf)
! (12) photoreactions on grain surface (ngphot)
! (13) adsorption on to grains (ngads)
! (14) neutralization (ngneut)
! (15) explosive desorption from grains (nggdes)
! (16) cosmic ray induced desorption from grains (ncrdes)
! (17) photodesorption from grains (nphdes)
! (18) evaporation from grains (ngevap)
! (19) chemisorption on grains (ngchs)
! (20) Eley-Rideal process (ngelri)

! (111) to (118) same processes taking grain size distribution
!                into account

! -------------------------------------------------------
! the DO loops are over all considered chemical reactions:
! -------------------------------------------------------

   nh2for  = 0
   ncrion  = 0
   npsion  = 0
   nasrad  = 0
   nrest   = 0
   nh2endo = 0
   nphot_f = 0
   nphot_i = 0
   n3body  = 0
   ncpart  = 0
   ngsurf  = 0
   ngphot  = 0
   ngads   = 0
   ngneut  = 0
   nggdes  = 0
   ncrdes  = 0
   nphdes  = 0
   ngevap  = 0
   ngchs   = 0
   ngelri  = 0

   numero  = 0

   ! (0) formation of h2 and hd on grains [old style] (nh2for)
   !     This special type is reserved for h + h -> h2
   !     It can be used to force the H2 formation rate,
   !     typically for example to get R = 3E-17 cm+3 s-1
   !     In the chemistry file, R has to be provided in
   !     the gamm column
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 0) THEN
         nh2for = nh2for + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         IF (bet(k) == 0.0_dp) THEN
            PRINT *, "  WARNING! Error in chemistry file, reaction type 0"
            PRINT *, "           (This is obsolete H2 formation rate)"
            PRINT *, "  beta is used as: (T/beta)**alpha"
            PRINT *, "  So, it should not be 0.0. Set to 100.0 K"
            bett(numero) = 100.0_dp
         ELSE
            bett(numero) = bet(k)
         ENDIF
         dee(numero) = de(k)
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
         ifh2gr = numero
      ENDIF
   ENDDO

   ! (1) cosmic ray processes (ncrion)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 1) THEN
         ncrion = ncrion + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         ! Heating by cosmic rays ionisation is done in stv5c.f
         dee(numero) = 0.0_dp
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   ! (2) secondary photon processes (npsion)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 2) THEN
         npsion = npsion + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = de(k)
         IF (ael(react(k,1),10) == 1) THEN
            dee(numero) = 0.0_dp
         ELSE
            dee(numero) = de(k)
         ENDIF
         ntype(numero) = itype(k)
         DO j = 0, 7
           lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   ! (3) radiative association  (nasrad)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 3) THEN
         nasrad = nasrad + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) =  bet(k)
         ! All the energy is taken by the photon
         dee(numero) = 0.0_dp
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
        ENDDO
      ENDIF
   ENDDO

   ! (4) gas phase reactions (nrest)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 4) THEN
         nrest = nrest + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = de(k)
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
       ENDIF
   ENDDO

   ! (6) Endothermal reactions with H2 (nh2endo)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 6) THEN
         nh2endo = nh2endo + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = de(k)
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
      ELSE IF (ity == 61) THEN ! Formation of CH+ following Agundez et al. ApJ (2010)
         nh2endo = nh2endo + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = de(k)
         ntype(numero) = itype(k)
         DO j = 0,7
           lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   ! (5) photoreactions (nphot_f)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 5) THEN
         nphot_f = nphot_f + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         ! Evelyne dixit
         dee(numero) = de(k) * 0.1_dp
         ! Special case: for PAH heating is already taken into account
         IF (ael(react(k,1),10) == 1) THEN
            dee(numero) = 0.0_dp
         ENDIF
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   ! (7) photoreactions (direct integration) (nphot_i)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 7) THEN
         nphot_i = nphot_i + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         ! Evelyne dixit
         dee(numero) = de(k) * 0.1_dp
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   ! (8) 3 body reactions
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 8) THEN
         n3body = n3body + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         ! Which enthapy for 3 body?
         dee(numero) = de(k)
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   ! (101...) exceptions (ncpart)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity > 100 .AND.  ity < 110) THEN
         ncpart = ncpart + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = de(k)
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   ! (11) reactions on grain surface (ngsurf)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 11 .OR. ity == 111) THEN
         ngsurf = ngsurf + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = 0.0_dp
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF

      IF (react(k,1) == i_hgr .AND.  react(k,2) == i_hgr) THEN
         dee(numero) = de(k)
         ifh2gr = numero
      ENDIF

      IF (react(k,1) == i_hgr .AND.  react(k,2) == i_dgr .OR. &
          react(k,1) == i_dgr .AND.  react(k,2) == i_hgr) THEN
          dee(numero) = de(k)
         ifhdgr = numero
      ENDIF
   ENDDO

   ! (12) photoreactions on grain surface (ngphot)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 12 .OR. ity == 112) THEN
         ngphot = ngphot + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = 0.0_dp
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   ! (13) adsorption on to grains (ngads)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 13 .OR.  ity == 113 .OR. ity == 123) THEN
         ngads = ngads + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = 0.0_dp
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO

         ! Sticking coef de H:
         IF (react(k,1) == i_h) THEN
            iadhgr = numero
         ENDIF

         ! Sticking coef de D:
         IF (react(k,1) == i_d) THEN
            iaddgr = numero
         ENDIF
      ENDIF
   ENDDO

   ! (14) neutralization (ngneut)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 14 .OR. ity == 114) THEN
         ngneut = ngneut + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = 0.0_dp
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   ! (15) explosive desorption from grains (nggdes)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 15 .OR. ity == 115) THEN
         nggdes = nggdes + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = 0.0_dp
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   ! (16) direct cosmic ray induced desorption from grains (ncrdes)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 16 .OR. ity == 116) THEN
         ncrdes = ncrdes + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = 0.0_dp
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   ! (17) photodesorption from grains (UV + PHOSEC) (nphdes)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 17 .OR. ity == 117) THEN
         nphdes = nphdes + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = 0.0_dp
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   ! (18) evaporation from grains (ngevap)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 18 .OR. ity == 118) THEN
         ngevap = ngevap + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = 0.0_dp
         ntype(numero) = itype(k)
         DO j = 0, 7
           lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   ! (19) chemisorption on to grains (ngchs)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 19) THEN
         ngchs = ngchs + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = 0.0_dp
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   ! (20) Eley-Rideal process (ngelri)
   DO k = 1, neqt
      ity = itype(k)
      IF (ity == 20) THEN
         ngelri = ngelri + 1
         numero = numero + 1
         gammm(numero) = gamm(k)
         palph(numero) = alpha(k)
         bett(numero) = bet(k)
         dee(numero) = 0.0_dp
         ntype(numero) = itype(k)
         DO j = 0, 7
            lreact(numero,j) = react(k,j)
         ENDDO
      ENDIF
   ENDDO

   DO k = 1,neqt
      gamm(k) = gammm(k)
      alpha(k) = palph(k)
      bet(k) = bett(k)
      de(k) = dee(k)
      itype(k) = ntype(k)
      DO j = 0, 7
         react(k,j) = lreact(k,j)
      ENDDO
      r1 = sp(natom+6)
      r2 = sp(natom+6)
      r3 = sp(natom+6)
      p1 = sp(natom+6)
      p2 = sp(natom+6)
      p3 = sp(natom+6)
      p4 = sp(natom+6)
      DO j = 1, n_var+6
         IF(react(k,1) == j) r1=speci(j)
         IF(react(k,2) == j) r2=speci(j)
         IF(react(k,3) == j) r3=speci(j)
         IF(react(k,4) == j) p1=speci(j)
         IF(react(k,5) == j) p2=speci(j)
         IF(react(k,6) == j) p3=speci(j)
         IF(react(k,7) == j) p4=speci(j)
      ENDDO
      IF (itype(k) /= 7) THEN
         WRITE (iwrit2,'(2x,"     ",1x,a8,"+ ",a8,"+ ",a8,"= ",3(a8,1x),a8, &
                      & 1pe9.2,2x,0pf6.2,2x,f8.1,2x,f9.3,3x,i4,i5)') &
                        r1, r2, r3, p1, p2, p3, p4, gamm(k), alpha(k), bet(k) &
                      , de(k), itype(k), k
      ELSE
         WRITE (iwrit2,'(2x,"     ",1x,a8,"+ ",a8,"+ ",a8,"= ",3(a8,1x),a8, &
                      & a29,f9.3,3x,i4,i5)') &
                       r1, r2, r3, p1, p2, p3, p4, "          Direct integration" &
                     , de(k), itype(k), k
      ENDIF
   ENDDO

   ! Check for duplicate reactions - JLB 4 II 2010
   ! 19 V 2011 - Test if we can check all reactions in one pass

   ideb = 1
   ifin = neqt
   CALL DUPLI (ideb, ifin)

END SUBROUTINE ORDON

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE DUPLI (ideb, ifin)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA

   IMPLICIT NONE

   INTEGER, INTENT (IN) :: ideb, ifin
   INTEGER              :: i, j, i_pb

   i_pb = 0
   DO i=ideb,ifin-1
      DO j=i+1,ifin
         IF ( (react(i,1) == react(j,1)) .AND. &
              (react(i,2) == react(j,2)) .AND. &
              (react(i,3) == react(j,3)) .AND. &
              (react(i,4) == react(j,4)) .AND. &
              (react(i,5) == react(j,5)) .AND. &
              (react(i,6) == react(j,6)) .AND. &
              (react(i,7) == react(j,7)) ) THEN
            IF (react(i,1) == i_c .AND. react(i,2) ==  i_c13) THEN   ! Patch for the symetrical reaction c + c* -> ...
               CYCLE
            ENDIF
            PRINT *, " Identical reactions:", i, j, " Type:", itype(i)
            PRINT *, speci(react(i,1)), speci(react(i,2)), speci(react(i,3)), " -> ", &
                     speci(react(i,4)), speci(react(i,5)), speci(react(i,6)), speci(react(i,7))
            i_pb = 1
         ENDIF
      ENDDO
   ENDDO

   IF (i_pb == 1) THEN
      PRINT *, " Please verify and correct chemistry file!"
      STOP
   ENDIF

END SUBROUTINE DUPLI

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

!*********************
SUBROUTINE POST_LECTUR
!*********************

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PXDR_RF_TOOLS

   !=========================================================================================
   ! DECLARATIONS
   !=========================================================================================
   IMPLICIT NONE

   ! Variables internes
   INTEGER                                           :: i, j, k, ind
   INTEGER                                           :: garder, fact
   INTEGER                                           :: long
   INTEGER                                           :: l1, l2
   CHARACTER (LEN = 100)                             :: s_react   ! String containing a chemical reaction
                                                                  ! read in a cross section data file
   CHARACTER (LEN = 100)                             :: s_react_r ! Sub-string of s_react containing
                                                                  ! the reactants part of the chemical reaction
   CHARACTER (LEN = 100)                             :: s_react_p ! Sub-string of s_react containing
                                                                  ! the products part of the chemical reaction
   CHARACTER (LEN=10), DIMENSION(:,:), ALLOCATABLE   :: esp_pres
   INTEGER, DIMENSION(:), ALLOCATABLE                :: num_react
   INTEGER                                           :: f_phodest ! Flag in data/photodestruction.flag
                                                                  ! 1 - take into account sections
                                                                  ! 0 - sections not taken into account
   CHARACTER (LEN=20)                                :: phodest_file ! Name of photodestruction
                                                                     ! file without .dat extension

   ! Variables declarees implicitement, utilisees dans stv1e.f: (test) pour le calcul de la
   ! somme des masses des produits et du bilan net d'energie

   INTEGER                                           :: kj, iprod, k0
   INTEGER                                           :: nbre_react_dep, nb_exclu
   REAL (KIND=dp)                                    :: sreact, creact, sprod, cprod
   REAL (KIND=dp)                                    :: dreact, dprodu

   ! Variable used to sort products of photo-reactions
   INTEGER                                           :: j1, j2, j3, j4  ! used in sort species
   CHARACTER(LEN=10)                                 :: name_specy  ! variable used to store temporarily the name of a species

   !==========================================================================================
   ! Recherche des reactions de photodestruction a calculer avec les sections
   !==========================================================================================

      ! File data/photodest.flag provides Flags to determine if computation of photodestruction
      ! by direct integration of cross-sections has to be done.
      ! If a line begins by "1", then the cross sections in data/sections/. are used

      ! 1- Read photodest.flag to count how many direct integrations are wished
      !    Some of these reactions will not be used if the specy is not present in the list
      !    of chemical species used for this model.
      !    These species will be removed later

      WRITE (iscrn,*)
      WRITE (iscrn,*) "    * Get the list of photo-reactions with rates computed "
      WRITE (iscrn,*) "      by direct integration of radiative cross sections"

      fichier = TRIM(ADJUSTL(data_dir))//"photodest.flag"
      OPEN (iwrtmp, FILE=fichier, STATUS="old", ACTION="read")
      nbre_react_dep = 0
      READ (iwrtmp,*) ! commentaire
      DO k = 1, 10000
         READ (iwrtmp,'(I1)',END=110) f_phodest
         IF (f_phodest == 1) nbre_react_dep = nbre_react_dep + 1
      ENDDO
110   CONTINUE

      ! 2 - Get the names of the cross section files (without the .dat extension)
      !     These filenames are stored in the array csect_file
      ALLOCATE(csect_files(nbre_react_dep))
      REWIND(iwrtmp)
      i = 0
      READ (iwrtmp,*) ! commentaire
      DO k = 1, 10000
         READ (iwrtmp,'(I1,3x,A20)',END=111) f_phodest, phodest_file
         IF (f_phodest == 1) THEN
            i = i + 1
            csect_files(i) = TRIM(ADJUSTL(phodest_file))
         ENDIF
      ENDDO
111   CONTINUE
      CLOSE(iwrtmp)

      ! 3 - Get the number of corresponding species (ns)
      !     Some species can appear several times if there are several photo-destruction paths
      !     We count them once
      de_nom = ""
      ns = 0
      DO k = 1, nbre_react_dep
         ic = INDEX(csect_files(k), "_")
         un_nom = csect_files(k)(1:ic-1)
         IF (un_nom == de_nom) THEN ! Check if the specy is already in the list
            CYCLE
         ELSE                       ! The specy has not been counted yet
            ns = ns + 1
            de_nom = un_nom
         ENDIF
      ENDDO

      ! ns correspond au nombre d'especes intervenant dans les reactions de itype=7 (= especes presentes
      ! dans "../data/liste.dat")
      ! nbre_react_dep correspond au nombre de reactions de itype=7 utilisees (= nombre de lignes du fichier
      ! "../data/liste.dat")
      ALLOCATE(les_esp(ns))

      ! 4 - Get the names of the species
      !     Now that the dynamical allocation is done, we get the names
      !     - Species names   are stored in the array : les_esp
      !     - Data file names are stored in the array : csect_files
      de_nom = ""
      ns = 0
      DO k = 1, nbre_react_dep
         ic = INDEX(csect_files(k), "_")
         un_nom = csect_files(k)(1:ic-1) ! Name of the specy extracted from the data file name
         IF (un_nom == de_nom) THEN      ! We already have stored this name
            CYCLE
         ELSE
            ns = ns + 1
            de_nom = un_nom
            les_esp(ns) = un_nom  ! Store the names of the species
         ENDIF
      ENDDO

      ! 5 - Get the corresponding chemical reactions and determine if the reaction has to be kept
      !     We keep a reaction only if all reactants and products are in the list of chemical species
      !     for this model

      ! 2) Table of data filling up. This table (photodest) is a  PHOTO_ESP TYPE variable

      ALLOCATE(num_react(nbre_react_dep))
      ALLOCATE(esp_pres(nbre_react_dep,7))
      nb_exclu = 0

      DO ind = 1, nbre_react_dep
         num_react(ind) = 0
      ENDDO

      DO k = 1, nbre_react_dep ! Nber of reactions for which direct integration is wished
         ! Get the chemical reaction. The reaction is written in the data file.
         fichier = TRIM(data_dir)//'Sections/'//TRIM(csect_files(k))//".dat"
         OPEN (iread2, file = fichier, status = 'old')
         READ (iread2,*)             ! Comment line
         READ (iread2,'(a)') s_react ! Reaction (line begin with a # and has the syntax : # c3 + photon = c2 + c)

         ! Parse the string "chaine" to extract the chemical reaction
         s_react = TRIM(ADJUSTL(s_react))
         long = LEN(s_react)
         i = INDEX(s_react,"#")
         j = INDEX(s_react,"=")
         s_react_r = TRIM(ADJUSTL(s_react(i+1:j-1)))  ! sub-string containing the reactants part of the reaction
         s_react_p = TRIM(ADJUSTL(s_react(j+1:long))) ! sub-string containing the products part of the reaction

         DO i = 1, 7 ! Loop on the elements of a reaction (in the PDR code : 3 reactants + 4 products)
            esp_pres(k,i) = ' '
         ENDDO

         ! Parse the reactants part of the chemical reaction to get the list of reactants
         long = LEN_TRIM(s_react_r)
         l1 = INDEX(s_react_r," + ") ! Reactants are separated by a "+"
         esp_pres(k,1) = TRIM(ADJUSTL(s_react_r(1:l1-1)))     ! First reactant
         esp_pres(k,2) = TRIM(ADJUSTL(s_react_r(l1+2:long)))  ! Second reactant (photon)

         DO i = 4, 7
            long = LEN_TRIM(s_react_p)
            IF (long == 0) THEN
               EXIT
            ENDIF
            l2 = INDEX(s_react_p," + ") ! Products are separated by a "+" character
            IF (l2 /= 0) THEN
               esp_pres(k,i) = TRIM(ADJUSTL(s_react_p(1:l2-1)))
            ELSE
               esp_pres(k,i) = TRIM(ADJUSTL(s_react_p(1:long)))
            ENDIF
            IF (l2 /= 0) THEN
               s_react_p = TRIM(ADJUSTL(s_react_p(l2+3:long)))
            ELSE
               s_react_p = ''
            ENDIF
         ENDDO

         ! SORT PRODUCTS OF THE REACTION
         ! This part is mandatory. This sorting is also done for reactions read in the .chi file.
         ! After these 2 sortings, the code will check if some photo-reactions from the .chi file
         ! have to be removed. Since products will be in the same order, it will be easy to
         ! compare reactions.
         j1 = 9999
         j2 = 9999
         j3 = 9999
         j4 = 9999
         ! Sort products
         DO j = 1, n_var+5
            IF (TRIM(ADJUSTL(esp_pres(k,4))) == speci(j)) j1 = j
            IF (TRIM(ADJUSTL(esp_pres(k,5))) == speci(j)) j2 = j
            IF (TRIM(ADJUSTL(esp_pres(k,6))) == speci(j)) j3 = j
            IF (TRIM(ADJUSTL(esp_pres(k,7))) == speci(j)) j4 = j
         ENDDO
         IF (j2 < j1) THEN
            name_specy    = esp_pres(k,4)
            esp_pres(k,4) = esp_pres(k,5)
            esp_pres(k,5) = name_specy
         ENDIF
         IF (j3 < j1) THEN
            name_specy    = esp_pres(k,4)
            esp_pres(k,4) = esp_pres(k,6)
            esp_pres(k,6) = name_specy
         ENDIF
         IF (j4 < j1) THEN
            name_specy    = esp_pres(k,4)
            esp_pres(k,4) = esp_pres(k,7)
            esp_pres(k,7) = name_specy
         ENDIF
         IF (j3 < j2) THEN
            name_specy    = esp_pres(k,5)
            esp_pres(k,5) = esp_pres(k,6)
            esp_pres(k,6) = name_specy
         ENDIF
         IF (j4 < j2) THEN
            name_specy    = esp_pres(k,7)
            esp_pres(k,7) = esp_pres(k,5)
            esp_pres(k,7) = name_specy
         ENDIF
         IF (j4 < j3) THEN
            name_specy    = esp_pres(k,6)
            esp_pres(k,6) = esp_pres(k,7)
            esp_pres(k,7) = name_specy
         ENDIF

         ! Check if we keep the reaction or not.
         ! We only keep reactions for which all the reactants and products are present in the
         ! chemistry file used for the model
         ! The list of species in the chemistry file is stored in the array speci(n_var+1)
         ! The list of species appearing in the direct photo-destruction reaction is stored in esp_pres(nbre_react_dep,1:7)
         ! RQ: speci(j)=' ' existe sinon 'garder' serait = a 0

         garder = 1
         DO i = 1, 7
            fact = 0
            DO j = 0, n_var+1
               IF (esp_pres(k,i) == speci(j))THEN
                  fact = 1
               ENDIF
            ENDDO
            garder = garder * fact
         ENDDO

         ! nb_exclu correspond au nombre d'especes non repertoriees par la chimie (speci(n_var+1))
         ! et qui pourtant interviennent dans les reactions de itype=7 (cf esp_pres(nbre_react_dep,1:7))

         IF (garder == 0) THEN
            num_react(k) = 1
            nb_exclu = nb_exclu + 1
         ENDIF
         CLOSE(iread2)
      ENDDO

      ! nphot_i correspond au nombre de reactions de itype=7 conservees dans tout le reste du programme
      nphot_i = nbre_react_dep - nb_exclu
      i = 0
      ! Reorganisation du tableau csect_files(nbre_react_dep), qui devient maintenant csect_files(nphot_i)
      ! et du tableau esp_pres(nbre_react_dep,1:7), qui devient maintenant esp_pres(nphot_i,1:7)
      DO k = 1, nbre_react_dep
         IF (num_react(k) == 0) THEN
            i = i + 1
            csect_files(i) = csect_files(k)
            DO j = 1, 7
               esp_pres(i,j) = esp_pres(k,j)
            ENDDO
         ELSE
            CYCLE
         ENDIF
      ENDDO

      !- 6 - Add mandatory direct photo-reactions (Photodissociation of H2, HD, CO, isotopes)

      ! Count of mandatory reactions
      i_mandat = 1                ! H2
      IF (i_co /= 0)     i_mandat = i_mandat + 1  ! CO
      IF (i_hd /= 0)     i_mandat = i_mandat + 1  ! HD
      IF (i_c13o /= 0)   i_mandat = i_mandat + 1  ! 13CO
      IF (i_co18 /= 0)   i_mandat = i_mandat + 1  ! C18O
      IF (i_c13o18 /= 0) i_mandat = i_mandat + 1  ! 13C18O

      WRITE (iscrn,*) "    - Number of mandatory photo-reactions       : ", i_mandat
      WRITE (iscrn,*) "    - Other photo-reactions                     : ", nphot_i
      WRITE (iscrn,*) "    - Number of species in these other reactions: ", ns

      !- 7 - Add other required quantities for the i_mandat + nphot_i direct photo-reactions
      !  phdest(:)  ! TYPE containing a photo-reaction (defined by %nom, %r1, %r2, %r3, %p1, %p2, %p3, %p4, %wl_cut_esp, %rf_esp)
      !             ! Cross sections and wavelengths are stored in TYPE %rf_esp
      !  itype(:)   ! Index type of the direct photo-reactions (itype = 7)
      !  de(:)      ! Enthalpy of the reaction
      ! reactions ayant itype=7
      ! ==> chgt variable "nphot_i":

      kdeph0  = nphot_i + 1
      nphot_i = nphot_i + i_mandat ! nphot_i corresponds now to the total number of photo-reactions with direct integration

      !--- Allocation of phdest ---
      IF (ALLOCATED(phdest)) THEN
         DO k = 1, nphot_i
            IF (ASSOCIATED(phdest(k)%rf_esp%Val)) THEN
               CALL RF_FREE (phdest(k)%rf_esp)
            ENDIF
         ENDDO
         DEALLOCATE (phdest)
      ENDIF
      ALLOCATE (phdest(nphot_i))
      DO k = 1, nphot_i
         NULLIFY (phdest(k)%rf_esp%Val, phdest(k)%rf_esp%Ord)
      ENDDO
      ALLOCATE(pdiesp(nphot_i,0:noptm))

      DO k = 1, nphot_i-i_mandat       ! Fill data for non mandatory photo-reactions
         itype(k) = 7
         phdest(k)%nom = csect_files(k)
         phdest(k)%r1 = esp_pres(k,1)  ! Reactant 1
         phdest(k)%r2 = esp_pres(k,2)  ! Reactant 2
         phdest(k)%r3 = esp_pres(k,3)  ! Reactant 3
         phdest(k)%p1 = esp_pres(k,4)  ! Product 1
         phdest(k)%p2 = esp_pres(k,5)  ! Product 2
         phdest(k)%p3 = esp_pres(k,6)  ! Product 3
         phdest(k)%p4 = esp_pres(k,7)  ! Product 4

         ! Begin to fill "react". The array "react contains the chemical reactions, with the species identified by the index
         ! Reactants
         DO j = 0, n_var+1  ! Loop on the index of chemical species (with 0 = nothing, and n_var+1 = photon)
            IF (esp_pres(k,1) == speci(j)) THEN
               react(k,1) = j     ! Index of the first reactant of the k_th chemical reaction
               EXIT
            ENDIF
         ENDDO
         react(k,2) = n_var + 1   ! n_var + 1 = photon
         react(k,3) = 0           ! Photo-reactions : there is no 3rd reactant
         k0 = 2
         ! Products
         DO i = 4, 7
            DO j = 0, n_var+1
               IF (esp_pres(k,i) == speci(j)) THEN
                  react(k,i) = j
                  IF (j /= 0) THEN
                     k0 = k0 + 1
                  ENDIF
                  EXIT
               ENDIF
            ENDDO
         ENDDO
         react(k,0) = k0

         ! Compute the enthalpy of the reactions
         IF (react(k,5) /= n_var+1) THEN
            dreact = enth(react(k,1)) + enth(react(k,2)) &
                   + enth(react(k,3))
            dprodu = 0.0_dp
            kj = react(k,0)
            DO j = 4, kj+1
               iprod = react(k,j)
               IF (iprod /= n_var+1) THEN
                  dprodu = dprodu + enth(iprod)
               ENDIF
            ENDDO
            de(k) = dreact - dprodu
            de(k) = de(k) * calev
         ELSE
            de(k) = 0.0_dp
         ENDIF

      ENDDO

      ! Mandatory reactions
      ! H2 + photon -> H + H
      IF (i_h2 /= 0) THEN
         react(k,0) = 4
         react(k,1) = i_h2
         react(k,2) = n_var + 1
         react(k,3) = 0
         react(k,4) = i_h
         react(k,5) = i_h
         react(k,6) = 0
         react(k,7) = 0
         itype(k) = 7
         de(k) = 1.0_dp ! For H2 and HD, the typical energy released in the gas
                        ! is 1 eV for each dissociation (Evelyne)
         phdest(k)%r1 = 'h2      '
         phdest(k)%r2 = 'photon  '
         phdest(k)%r3 = '        '
         phdest(k)%p1 = 'h       '
         phdest(k)%p2 = 'h       '
         phdest(k)%p3 = '        '
         phdest(k)%p4 = '        '
      ENDIF

      ! HD + photon -> H + D
      IF (i_hd /= 0) THEN
         k = k + 1
         react(k,0) = 4
         react(k,1) = i_hd
         react(k,2) = n_var + 1
         react(k,3) = 0
         react(k,4) = i_h
         react(k,5) = i_d
         react(k,6) = 0
         react(k,7) = 0
         itype(k) = 7
         de(k) = 1.0_dp
         phdest(k)%r1 = 'hd      '
         phdest(k)%r2 = 'photon  '
         phdest(k)%r3 = '        '
         phdest(k)%p1 = 'h       '
         phdest(k)%p2 = 'd       '
         phdest(k)%p3 = '        '
         phdest(k)%p4 = '        '
      ENDIF

   !  Use same variation enthalpy (de) for all isotopes (since this is an approximation anyway)
      dreact = enth(i_co) + enth(n_var+1)
      dprodu = enth(i_c) + enth(i_o)

      ! CO + photon -> C + O
      IF (i_co /= 0) THEN
         k = k + 1
         react(k,0) = 4
         react(k,1) = i_co
         react(k,2) = n_var + 1
         react(k,3) = 0
         react(k,4) = i_c
         react(k,5) = i_o
         react(k,6) = 0
         react(k,7) = 0
         itype(k) = 7
         de(k) = (dreact - dprodu) * calev
         phdest(k)%r1 = 'co      '
         phdest(k)%r2 = 'photon  '
         phdest(k)%r3 = '        '
         phdest(k)%p1 = 'c       '
         phdest(k)%p2 = 'o       '
         phdest(k)%p3 = '        '
         phdest(k)%p4 = '        '
      ENDIF

      ! C*O + photon -> C* + O
      IF (i_c13o /= 0) THEN
         k = k + 1
         react(k,0) = 4
         react(k,1) = i_c13o
         react(k,2) = n_var + 1
         react(k,3) = 0
         react(k,4) = i_c13
         react(k,5) = i_o
         react(k,6) = 0
         react(k,7) = 0
         itype(k) = 7
         de(k) = (dreact - dprodu) * calev
         phdest(k)%r1 = 'c*o     '
         phdest(k)%r2 = 'photon  '
         phdest(k)%r3 = '        '
         phdest(k)%p1 = 'c*      '
         phdest(k)%p2 = 'o       '
         phdest(k)%p3 = '        '
         phdest(k)%p4 = '        '
      ENDIF

      ! CO* + photon -> C + O*
      IF (i_co18 /= 0) THEN
         k = k + 1
         react(k,0) = 4
         react(k,1) = i_co18
         react(k,2) = n_var + 1
         react(k,3) = 0
         react(k,4) = i_c
         react(k,5) = i_o18
         react(k,6) = 0
         react(k,7) = 0
         itype(k) = 7
         de(k) = (dreact - dprodu) * calev
         phdest(k)%r1 = 'co*     '
         phdest(k)%r2 = 'photon  '
         phdest(k)%r3 = '        '
         phdest(k)%p1 = 'c       '
         phdest(k)%p2 = 'o*      '
         phdest(k)%p3 = '        '
         phdest(k)%p4 = '        '
      ENDIF

      ! C*O* + photon -> C* + O*
      IF (i_c13o18 /= 0) THEN
         k = k + 1
         react(k,0) = 4
         react(k,1) = i_c13o18
         react(k,2) = n_var + 1
         react(k,3) = 0
         react(k,4) = i_c13
         react(k,5) = i_o18
         react(k,6) = 0
         react(k,7) = 0
         itype(k) = 7
         de(k) = (dreact - dprodu) * calev
         phdest(k)%r1 = 'c*o*    '
         phdest(k)%r2 = 'photon  '
         phdest(k)%r3 = '        '
         phdest(k)%p1 = 'c*      '
         phdest(k)%p2 = 'o*      '
         phdest(k)%p3 = '        '
         phdest(k)%p4 = '        '
      ENDIF

      DO k = 1, nphot_i
         WRITE (iscrn,"('        ',A8)", ADVANCE="NO") phdest(k)%r1
         WRITE (iscrn,"(' + ',A8)",    ADVANCE="NO")   phdest(k)%r2
         WRITE (iscrn,"(' -> ',A8)",   ADVANCE="NO")   phdest(k)%p1
         WRITE (iscrn,"(' + ',A8)",    ADVANCE="NO")   phdest(k)%p2
         WRITE (iscrn,"(' + ',A8)",    ADVANCE="NO")   phdest(k)%p3
         WRITE (iscrn,"(' + ',A8)")                    phdest(k)%p4
      ENDDO

      !----calcul de la somme des masses des produits de chaque reaction
      !    et du bilan net d energie de chaque reaction (en ev)
      !    convention: positif ==> exothermique
      !                negatif ==> endothermique
      !                ******* ==> donnees insuffisantes

      DO k = 1, nphot_i
         sreact = 0.0_dp
         creact = 0.0_dp
         sprod  = 0.0_dp
         cprod  = 0.0_dp

         sreact = sreact + mol(react(k,1)) + mol(react(k,2)) &
                         + mol(react(k,3))
         creact = creact + charge(react(k,1)) + charge(react(k,2)) &
                         + charge(react(k,3))
         sprod = sprod   + mol(react(k,4)) + mol(react(k,5)) &
                         + mol(react(k,6)) + mol(react(k,7))
         cprod = cprod   + charge(react(k,4)) + charge(react(k,5)) &
                         + charge(react(k,6)) + charge(react(k,7))

         bilanm(k) = sreact - sprod
         bilanc(k) = creact - cprod
         IF (ABS(bilanm(k)) > 0.0_dp) THEN
            PRINT *, k, (speci(react(k,j)),j=1,7)
            PRINT *, k, "Mass - React:", sreact, &
                 " Prod:", sprod, "Diff:", bilanm(k)
            PRINT *,"Problem in mass conservation"
            STOP
         ENDIF
         IF ((speci(react(k,1)) /= "grain ") &
              .AND. (speci(react(k,2)) /= "grain ") &
              .AND. (speci(react(k,3)) /= "grain ") ) THEN
            IF (ABS(bilanc(k)) > 0.0_dp) THEN
               PRINT *, k, (speci(react(k,j)),j=1,7)
               PRINT *, k, "Charge - React:", creact, &
                           " Prod:", cprod, &
                           " Diff:", bilanc(k)
               PRINT *,"Problem in charge conservation"
            ENDIF
         ENDIF
      ENDDO

      DEALLOCATE (num_react)
      DEALLOCATE (esp_pres)

END SUBROUTINE POST_LECTUR

END MODULE PXDR_READCHIM
