
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

! JLB 21 - May 2008

MODULE PXDR_ISOTCO

PRIVATE

PUBLIC :: ISOTC, ISOTO

CONTAINS

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE ISOTC (nreact,idiff)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  USE PXDR_CONSTANTES
  USE PXDR_CHEM_DATA
  USE PXDR_AUXILIAR
  USE PXDR_STR_DATA

! *************************************************************************
!          CONSTRUCTION DE LA CHIMIE ISOTOPEE DU CARBONE
! *************************************************************************
!
!         NEQT   = NOMBRE TOTAL DE REACTIONS SANS ISOTOPE
!         NREACT = NUMERO DE LA REACTION ISOTOPEE
!         IDIFF  = NUMERO DE LA DERNIERE REACTION DIFFERENTIELLE
!         JJ     = NUMERO DE LA REACTION CORRESPONDANTE (SS ISOTOPE)
!
! *************************************************************************
!
!   CONVENTION :    DANS LA LISTE DES ESPECES, CHAQUE ESPECE ISOTOPEE DOIT
!   ----------      SUIVRE IMMEDIATEMENT l ESPECE MERE
!                   DANS LE CAS OU l ESPECE CONTIENT DE l OXYGENE ET DU
!                   CARBONE (C OU C*) ==> l ESPECE ISOTOPEE: O* SUIT C*
!                   EX :    HCO2
!                           HC*O2
!                           HCO*O
!                           HC*O*O
!
! *************************************************************************

  IMPLICIT NONE

  INTEGER, INTENT (INOUT) :: nreact
  INTEGER, INTENT (OUT)   :: idiff
  INTEGER                 :: ir0, ir1, ir2, ir3c, ir3, ir4, ir5, ir6
  INTEGER                 :: ncr1, ncr2, ncp1, ncp2
  INTEGER                 :: itest, jj

  PRINT *,"Build C13 chemistry"

! 15 march 2004 - JLB - Add 3 body reactions

  DO jj = 1, neqt

     ! 23 oct 2006 - JLB - skip direct photodestruction
     IF (itype(jj) == 7) CYCLE

     ! Definition des reactants et produits:
     ir0 = react(jj,0)
     ir1 = react(jj,1)
     ir2 = react(jj,2)
     ir3c = react(jj,3)
     ir3 = react(jj,4)
     ir4 = react(jj,5)
     ir5 = react(jj,6)
     ir6 = react(jj,7)

!-----------------------------------------------------------------------------
!--- IL N'Y A PAS DE CARBONE DANS LES PHOTONS OU LES CRP
!    ou les photons secondaires
!            (POUR LESQUELS AEL N'EST PAS DEFINI)

     IF (ir2 == n_var+1 .OR. ir2 == n_var+2 .OR. ir2 == n_var+3) THEN
        ncr2 = 0
        go to 550
     ENDIF
!-----------------------------------------------------------------------------
!---Y-A-t IL DEJA DES ESPECES ISOTOPEES (C* OU O*) DANS LA REACTION ?
!         SI OUI, ON PASSE A LA REACTION SUIVANTE
!-----------------------------------------------------------------------------

     itest = ael(ir1,7) + ael(ir2,7) + ael(ir1,9) + ael(ir2,9)

     IF (itest > 0)    THEN
        idiff = jj
        CYCLE
     ENDIF
!-----------------------------------------------------------------------------
!---Y-A-t IL DU CARBONE DANS LA REACTION CONSIDEREE ?
!                                        ET COMBIEN ?
!
!          NCR1, NCR2 = NOMBRE DE C DANS LES REACTANTS R1 ET R2
!          NCP1, NCP2 = NOMBRE DE C DANS LES REACTANTS P1 ET P2
!
!-----------------------------------------------------------------------------

     IF (ael(ir2,1) /= ael(ir2+1,1)   .OR.  ael(ir2,2) /= ael(ir2+1,2)+1 .OR. &
        ael(ir2,7) /= ael(ir2+1,7)-1) THEN
        ncr2 = 0
     ELSE
        ncr2 = ael(ir2,2)
     ENDIF

  550   continue

     IF (ael(ir1,1) /= ael(ir1+1,1)   .OR.  ael(ir1,2) /= ael(ir1+1,2)+1 .OR. &
        ael(ir1,7) /= ael(ir1+1,7)-1) THEN
        ncr1 = 0
     ELSE
        ncr1 = ael(ir1,2)
     ENDIF

     IF (ael(ir3,1) /= ael(ir3+1,1)   .OR.  ael(ir3,2) /= ael(ir3+1,2)+1 .OR. &
        ael(ir3,7) /= ael(ir3+1,7)-1) THEN
        ncp1 = 0
     ELSE
        ncp1 = ael(ir3,2)
     ENDIF

     IF (ir4 /= 0) THEN
        IF (ael(ir4,1) /= ael(ir4+1,1)   .OR.  ael(ir4,2) /= ael(ir4+1,2)+1 .OR. &
           ael(ir4,7) /= ael(ir4+1,7)-1) THEN
           ncp2 = 0
        ELSE
           ncp2 = ael(ir4,2)
        ENDIF
     ELSE
        ncp2 = 0
     ENDIF

!--- IL N'Y A PAS DE CARBONE DANS LES PHOTONS OU LES CRP
!    ou les photons secondaires
!           (POUR LESQUELS AEL N EST PAS DEFINI)

     IF (ir4 == n_var+1 .OR. ir4 == n_var+2 .OR. ir4 == n_var+3) THEN
        ncp2 = 0
     ENDIF

!-----------------------------------------------------------------------------
!--- 1) PREMIER CAS : IL Y A DU C (isotopables) DANS LES DEUX REACTANTS
!       ***********                                      ****
!
!--- trois POSSIBILITES:
!
!    A) IL Y A DU C DANS LES DEUX PRODUITS ==> 2 REACTIONS ISOTOPEES
!    B) OU BIEN DANS UN SEUL               ==> 2 REACTIONS ISOTOPEES
!    c) il n'y a pas de C isotopable       ==> On saute la reaction
!
!    ==> PAS DE RAPPORTS DE BRANCHEMENT
!
!-----------------------------------------------------------------------------

     IF (ncr1 > 0 .AND. ncr2 > 0) THEN

!-----------------------------------------------------------------------------
!------------ A) PREMIERE POSSIBILITE : IL Y A DU C DANS LES DEUX PRODUITS :
!                                                        ********
!
!  CONVENTION : LES REACTIONS DOIVENT ETRE ECRITE DANS l ORDRE :
!  ----------
!
!      AC   +  BC   -->  A'C   +  B'C
!
!                   TEL QUE  =====>      AC*  +  BC   -->  A'C   +  B'C*
!                        ET  =====>      AC   +  BC*  -->  A'C*  +  B'C
!
!                          ==> 2 REACTIONS ISOTOPEES
!-----------------------------------------------------------------------------
     IF (ncp1 > 0 .AND. ncp2 > 0) THEN
!-----------------------------------------------------------------------------
!
!--- PREMIERE REACTION ISOTOPEE:

        nreact = nreact + 1

        react (nreact,0) = ir0
        react (nreact,1) = ir1 + 1
        react (nreact,2) = ir2
        react (nreact,3) = ir3c
        react (nreact,4) = ir3
        react (nreact,5) = ir4 + 1
        react (nreact,6) = ir5
        react (nreact,7) = ir6

        gamm(nreact)  = gamm(jj)
        alpha(nreact) = alpha(jj)
        bet(nreact)   = bet(jj)
        itype(nreact) = itype(jj)

!--- DEUXIEME REACTION ISOTOPEE:

        nreact = nreact + 1

        react (nreact,0) = ir0
        react (nreact,1) = ir1
        react (nreact,2) = ir2 + 1
        react (nreact,3) = ir3c
        react (nreact,4) = ir3 + 1
        react (nreact,5) = ir4
        react (nreact,6) = ir5
        react (nreact,7) = ir6

        gamm(nreact)  = gamm(jj)
        alpha(nreact) = alpha(jj)
        bet(nreact)   = bet(jj)
        itype(nreact) = itype(jj)

!-----------------------------------------------------------------------------
!------------ B- DEUXIEME POSSIBILITE : IL Y A DU C DANS UN SEUL PRODUIT :
!                                                        *******
!
!      AC  +  BC  -->  A'C  +  B'
!  OU  AC  +  BC  -->  A'   +  B'C
!
!                            =====>      AC*  +  BC   -->  A'C*  +  B'
!                                    OU  AC*  +  BC   -->  A'    +  B'C*
!
!                        ET  =====>      AC   +  BC*  -->  A'C*  +  B
!                                    OU  AC   +  BC*  -->  A'    +  B'C*
!
!                       ==> 2 REACTIONS ISOTOPEES
!-----------------------------------------------------------------------------

     ELSE IF (ncp1 > 0 .OR. ncp2 > 0) THEN

!-----------------------------------------------------------------------------
!
!--- PREMIERE REACTION ISOTOPEE :

        nreact = nreact + 1

        react (nreact,0) = ir0
        react (nreact,1) = ir1 + 1
        react (nreact,2) = ir2
        react (nreact,3) = ir3c

        IF (ncp1 == 0) THEN
           react (nreact,4) = ir3
           react (nreact,5) = ir4 + 1
        ELSE
           react (nreact,4) = ir3 + 1
           react (nreact,5) = ir4
        ENDIF

        react (nreact,6) = ir5
        react (nreact,7) = ir6

        gamm(nreact)  = gamm(jj)
        alpha(nreact) = alpha(jj)
        bet(nreact)   = bet(jj)
        itype(nreact) = itype(jj)

!--- DEUXIEME REACTION ISOTOPEE :

        nreact = nreact + 1

        react (nreact,0) = ir0
        react (nreact,1) = ir1
        react (nreact,2) = ir2 + 1
        react (nreact,3) = ir3c

        IF (ncp1 == 0) THEN
           react (nreact,4) = ir3
           react (nreact,5) = ir4 + 1
        ELSE
           react (nreact,4) = ir3 + 1
           react (nreact,5) = ir4
        ENDIF

        react (nreact,6) = ir5
        react (nreact,7) = ir6

        gamm(nreact)  = gamm(jj)
        alpha(nreact) = alpha(jj)
        bet(nreact)   = bet(jj)
        itype(nreact) = itype(jj)

     ENDIF

!--- ON PASSE A l EXAMEN DE LA REACTION SUIVANTE :

     CYCLE

  ENDIF

!-----------------------------------------------------------------------------
!--- 2- DEUXIEME CAS : IL Y A DU C DANS UN SEUL REACTANT
!       ************                    *******
!
!---DEUX POSSIBILITES:
!
!    A- IL Y A DU C DANS LES DEUX PRODUITS ==> 2 REACTIONS ISOTOPEES
!    B- OU BIEN DANS UN SEUL               ==> 1 REACTION ISOTOPEE
!    c- Ou dans aucun                      ==> On passe
!-----------------------------------------------------------------------------

  IF (ncr1 > 0 .OR. ncr2 > 0) THEN

!-----------------------------------------------------------------------------
!
!                     LES REACTIONS s ECRIVENT ALORS :
!-----------------------------------------------------------------------------
!------------ A) PREMIERE POSSIBILITE : IL Y A DU C DANS LES DEUX PRODUITS :
!                                                        ********
!
!      AC  +  B   -->  A'C  +  B'C
!  OU  A   +  BC  -->  A'C  +  B'C
!
!                            =====>      AC*  +  B    -->  A'C*  +  B'C
!                                    OU  A    +  BC   -->  A'C*  +  B'C
!
!
!                        ET  =====>      AC*  +  B    -->  A'C   +  B'C*
!                                    OU  A    +  BC   -->  A'C   +  B'C*
!
!                          ==> 2 REACTIONS ISOTOPEES
!-----------------------------------------------------------------------------

     IF (ncp1 > 0 .AND. ncp2 > 0) THEN

!-----------------------------------------------------------------------------
!
!--- PREMIERE REACTION ISOTOPEE:

        nreact = nreact + 1

        react (nreact,0) = ir0
        IF (ncr1 == 0) THEN
           react (nreact,1) = ir1
           react (nreact,2) = ir2 + 1
        ELSE
           react (nreact,1) = ir1 + 1
           react (nreact,2) = ir2
        ENDIF
        react (nreact,3) = ir3c

        react (nreact,4) = ir3 + 1
        react (nreact,5) = ir4
        react (nreact,6) = ir5
        react (nreact,7) = ir6

        gamm(nreact)  = gamm(jj)/2.0
        alpha(nreact) = alpha(jj)
        bet(nreact)   = bet(jj)
        itype(nreact) = itype(jj)

!--- DEUXIEME REACTION ISOTOPEE:

        nreact = nreact + 1

        react (nreact,0) = ir0
        IF (ncr1 == 0) THEN
           react (nreact,1) = ir1
           react (nreact,2) = ir2 + 1
        ELSE
           react (nreact,1) = ir1 + 1
           react (nreact,2) = ir2
        ENDIF
        react (nreact,3) = ir3c

        react (nreact,4) = ir3
        react (nreact,5) = ir4 + 1
        react (nreact,6) = ir5
        react (nreact,7) = ir6

        gamm(nreact)  = gamm(jj)/2.0
        alpha(nreact) = alpha(jj)
        bet(nreact)   = bet(jj)
        itype(nreact) = itype(jj)

!-----------------------------------------------------------------------------
!------------ B) DEUXIEME POSSIBILITE : IL Y A DU C DANS UN SEUL PRODUIT :
!                                                        *******
!
!      AC  +  B   -->  A'C  +  B'
!  OU  A   +  BC  -->  A'C  +  B'
!  OU  AC  +  B   -->  A'   +  B'C
!  OU  A   +  BC  -->  A'   +  B'C
!
!                            =====>      AC*  +  B    -->  A'C*  +  B'
!                                    OU  A    +  BC*  -->  A'C*  +  B'
!                                    OU  AC*  +  B    -->  A'    +  B'C*
!                                    OU  A    +  BC*  -->  A'    +  B'C*
!
!                       ==> 1 REACTION ISOTOPEE
!-----------------------------------------------------------------------------
     ELSE IF (ncp1 > 0 .OR. ncp2 > 0) THEN
!-----------------------------------------------------------------------------
!
!--- UNE SEULE REACTION ISOTOPEE :

        nreact = nreact + 1

        react (nreact,0) = ir0
        IF (ncr1 == 0) THEN
           react (nreact,1) = ir1
           react (nreact,2) = ir2 + 1
        ELSE
           react (nreact,1) = ir1 + 1
           react (nreact,2) = ir2
        ENDIF
        react (nreact,3) = ir3c

        IF (ncp1 == 0) THEN
           react (nreact,4) = ir3
           react (nreact,5) = ir4 + 1
        ELSE
           react (nreact,4) = ir3 + 1
           react (nreact,5) = ir4
        ENDIF

        react (nreact,6) = ir5
        react (nreact,7) = ir6

        gamm(nreact)  = gamm(jj)
        alpha(nreact) = alpha(jj)
        bet(nreact)   = bet(jj)
        itype(nreact) = itype(jj)

     ENDIF
  ENDIF

  ENDDO

!----------------------------------------------------------------------------
!                      FIN DE l EXAMEN DES REACTIONS
!          ET DE LA CONSTRUCTION DE LA CHIMIE ISOTOPEE DU CARBONE
!----------------------------------------------------------------------------

  PRINT *,"End of construction of C13 chemistry"

END SUBROUTINE ISOTC

!******************************
SUBROUTINE ISOTO (nreact,idiff)
!******************************

  USE PXDR_CONSTANTES
  USE PXDR_CHEM_DATA
  USE PXDR_AUXILIAR
  USE PXDR_STR_DATA

! *************************************************************************
!          CONSTRUCTION DE LA CHIMIE ISOTOPEE DE l OXYGENE
! *************************************************************************
!
!         NEQT   = NOMBRE TOTAL DE REACTIONS SANS ISOTOPE
!         IRC    = NOMBRE TOTAL DE REACTIONS SANS ISOTOPE DE l OXYGENE
!         NREACT = NUMERO DE LA REACTION ISOTOPEE
!         IDIFF  = NUMERO DE LA DERNIERE REACTION DIFFERENTIELLE
!         JJ     = NUMERO DE LA REACTION CORRESPONDANTE (SS ISOTOPE)
!
! *************************************************************************
!
!   CONVENTION :    DANS LA LISTE DES ESPECES, CHAQUE ESPECE ISOTOPEE DOIT
!   ----------      SUIVRE IMMEDIATEMENT l ESPECE MERE
!                   DANS LE CAS OU l ESPECE CONTIENT DE l OXYGENE ET DU
!                   CARBONE (C OU C*) ==> l ESPECE ISOTOPEE: O* SUIT C*
!                   EX :    HCO2
!                           HC*O2
!                           HCO*O
!                           HC*O*O
!
! *************************************************************************

  IMPLICIT NONE

  INTEGER, INTENT (INOUT) :: nreact
  INTEGER, INTENT (IN)    :: idiff
  INTEGER                 :: ir0, ir1, ir2, ir3c, ir3, ir4, ir5, ir6
  INTEGER                 :: jj
  INTEGER                 :: nor1, nor2, nop1, nop2, is1, is2, is3, is4

  PRINT *,"Build O18 chemistry"

  DO jj = idiff+1, nreact

! 23 oct 2006 - JLB - skip direct photodestruction

    IF (itype(jj) == 7) CYCLE

!  reactants et produits

    ir0 = react(jj,0)
    ir1 = react(jj,1)
    ir2 = react(jj,2)
    ir3c = react(jj,3)
    ir3 = react(jj,4)
    ir4 = react(jj,5)
    ir5 = react(jj,6)
    ir6 = react(jj,7)

!-----------------------------------------------------------------------------
!--- IL N'Y A PAS d OXYGENE DANS LES PHOTONS OU LES CRP
!    ou les photons secondaires
!            (POUR LESQUELS AEL N'EST PAS DEFINI)
!-----------------------------------------------------------------------------
!---Y-A-t IL DE l OXYGENE DANS LA REACTION CONSIDEREE ?
!                                          ET COMBIEN ?
!
!          NOR1, NOR2 = NOMBRE DE O DANS LES REACTANTS R1 ET R2
!          NOP1, NOP2 = NOMBRE DE O DANS LES REACTANTS P1 ET P2
!
!---Y-A-t IL un isotope carbone a sauter? si oui, is1234 = 1
!
!          NCR1, NCR2 = NOMBRE DE C (OU C*) DANS LES REACTANTS R1 ET R2
!          NCP1, NCP2 = NOMBRE DE C (OU C*) DANS LES REACTANTS P1 ET P2
!-----------------------------------------------------------------------------

    IF (ir2 == n_var+1 .OR. ir2 == n_var+2 .OR. ir2 == n_var+3) THEN
      nor2=0
    ELSE IF (ael(ir2,1) == ael(ir2+1,1)   .AND.  ael(ir2,2) == ael(ir2+1,2)   .AND. &
        ael(ir2,4) == ael(ir2+1,4)+1 .AND.  ael(ir2,9) == ael(ir2+1,9)-1) THEN
      nor2 = ael(ir2,4)
      is2 = 0
    ELSE IF (ael(ir2,1) == ael(ir2+2,1) .AND.  ael(ir2,2) == ael(ir2+2,2)   .AND. &
        ael(ir2,4) == ael(ir2+2,4)+1 .AND.  ael(ir2,9) == ael(ir2+2,9)-1) THEN
      nor2 = ael(ir2,4)
      is2 = 1
    ELSE
      nor2 = 0
    ENDIF

    IF (ael(ir1,1) == ael(ir1+1,1)   .AND.  ael(ir1,2) == ael(ir1+1,2)   .AND. &
        ael(ir1,4) == ael(ir1+1,4)+1 .AND.  ael(ir1,9) == ael(ir1+1,9)-1) THEN
      nor1 = ael(ir1,4)
      is1 = 0
    ELSE IF (ael(ir1,1) == ael(ir1+2,1) .AND.  ael(ir1,2) == ael(ir1+2,2)   .AND. &
        ael(ir1,4) == ael(ir1+2,4)+1 .AND.  ael(ir1,9) == ael(ir1+2,9)-1) THEN
      nor1 = ael(ir1,4)
      is1 = 1
    ELSE
      nor1 = 0
    ENDIF

    IF (ael(ir3,1) == ael(ir3+1,1)   .AND.  ael(ir3,2) == ael(ir3+1,2)   .AND. &
        ael(ir3,4) == ael(ir3+1,4)+1 .AND.  ael(ir3,9) == ael(ir3+1,9)-1) THEN
      nop1 = ael(ir3,4)
      is3 = 0
    ELSE IF (ael(ir3,1) == ael(ir3+2,1) .AND.  ael(ir3,2) == ael(ir3+2,2)   .AND. &
        ael(ir3,4) == ael(ir3+2,4)+1 .AND.  ael(ir3,9) == ael(ir3+2,9)-1) THEN
      nop1 = ael(ir3,4)
      is3 = 1
    ELSE
      nop1 = 0
    ENDIF

    IF (ir4 /= 0) THEN
      IF (ael(ir4,1) == ael(ir4+1,1)   .AND.  ael(ir4,2) == ael(ir4+1,2)   .AND. &
          ael(ir4,4) == ael(ir4+1,4)+1 .AND.  ael(ir4,9) == ael(ir4+1,9)-1) THEN
        nop2 = ael(ir4,4)
        is4 = 0
      ELSE IF (ael(ir4,1) == ael(ir4+2,1) .AND.  ael(ir4,2) == ael(ir4+2,2)   .AND. &
          ael(ir4,4) == ael(ir4+2,4)+1 .AND.  ael(ir4,9) == ael(ir4+2,9)-1) THEN
        nop2 = ael(ir4,4)
        is4 = 1
      ELSE
        nop2 = 0
      ENDIF
    ELSE
      nop2 = 0
    ENDIF

!--- IL N'Y A PAS d OXYGENE DANS LES PHOTONS OU LES CRP
!    ou les photons secondaires
!           (POUR LESQUELS AEL N'EST PAS DEFINI)

    IF (ir4 == n_var+1 .OR.  ir4 == n_var+2 .OR. ir4 == n_var+3) THEN
      nop2 = 0
    ENDIF

!-----------------------------------------------------------------------------
!--- 1) PREMIER CAS : IL Y A DE l O DANS LES DEUX REACTANTS
!       ***********                          ****
!
!--- trois POSSIBILITES:
!
!    A) IL Y A DE l O DANS LES DEUX PRODUITS ==> 2 REACTIONS ISOTOPEES
!    B) OU BIEN DANS UN SEUL                 ==> 2 REACTIONS ISOTOPEES
!    c) ou bien dans aucun                   ==> on passe
!
!    ==> PAS DE RAPPORTS DE BRANCHEMENT
!
!-----------------------------------------------------------------------------

    IF (nor1 > 0 .AND. nor2 > 0) THEN

!-----------------------------------------------------------------------------
!------------ A) PREMIERE POSSIBILITE : IL Y A DE l O DANS LES DEUX PRODUITS :
!                                                          ********
!
!  CONVENTION : LES REACTIONS DOIVENT ETRE ECRITE DANS l ORDRE (????) :
!  ----------
!
!      AO   +  BO   -->  A'O   +  B'O
!
!                   TEL QUE  =====>      AO*  +  BO   -->  A'O   +  B'O*
!                        ET  =====>      AO   +  BO*  -->  A'O*  +  B'O
!
!                          ==> 2 REACTIONS ISOTOPEES
!-----------------------------------------------------------------------------

      IF (nop1 > 0 .AND. nop2 > 0) THEN

!-----------------------------------------------------------------------------
!
!--- PREMIERE REACTION ISOTOPEE:

        nreact = nreact + 1

        react (nreact,0) = ir0
        react (nreact,1) = ir1 + 1 + is1
        react (nreact,2) = ir2
        react (nreact,3) = ir3c
        react (nreact,4) = ir3
        react (nreact,5) = ir4 + 1 + is4
        react (nreact,6) = ir5
        react (nreact,7) = ir6

        gamm(nreact)  = gamm(jj)
        alpha(nreact) = alpha(jj)
        bet(nreact)   = bet(jj)
        itype(nreact) = itype(jj)

!--- DEUXIEME REACTION ISOTOPEE:

        nreact = nreact + 1

        react (nreact,0) = ir0
        react (nreact,1) = ir1
        react (nreact,2) = ir2 + 1 + is2
        react (nreact,3) = ir3c
        react (nreact,4) = ir3 + 1 + is3
        react (nreact,5) = ir4
        react (nreact,6) = ir5
        react (nreact,7) = ir6

        gamm(nreact)  = gamm(jj)
        alpha(nreact) = alpha(jj)
        bet(nreact)   = bet(jj)
        itype(nreact) = itype(jj)

!-----------------------------------------------------------------------------
!------------ B) DEUXIEME POSSIBILITE : IL Y A DE l O DANS UN SEUL PRODUIT :
!                                                          *******
!
!      AO  +  BO  -->  A'O  +  B'
!  OU  AO  +  BO  -->  A'   +  B'O
!
!                            =====>      AO*  +  BO   -->  A'O*  +  B'
!                                    OU  AO*  +  BO   -->  A'    +  B'O*
!
!                        ET  =====>      AO   +  BO*  -->  A'O*  +  B
!                                    OU  AO   +  BO*  -->  A'    +  B'O*
!
!                       ==> 2 REACTIONS ISOTOPEES
!-----------------------------------------------------------------------------

      ELSE IF (nop1 > 0 .OR. nop2 > 0) THEN

!-----------------------------------------------------------------------------
!
!--- PREMIERE REACTION ISOTOPEE :

        nreact = nreact + 1

        react (nreact,0) = ir0
        react (nreact,1) = ir1 + 1 + is1
        react (nreact,2) = ir2
        react (nreact,3) = ir3c

        IF (nop1 == 0) THEN
          react (nreact,4) = ir3
          react (nreact,5) = ir4 + 1 + is4
        ELSE
          react (nreact,4) = ir3 + 1 + is3
          react (nreact,5) = ir4
        ENDIF

        react (nreact,6) = ir5
        react (nreact,7) = ir6

        gamm(nreact)  = gamm(jj)
        alpha(nreact) = alpha(jj)
        bet(nreact)   = bet(jj)
        itype(nreact) = itype(jj)

!--- DEUXIEME REACTION ISOTOPEE :

        nreact = nreact + 1

        react (nreact,0) = ir0
        react (nreact,1) = ir1
        react (nreact,2) = ir2 + 1 + is2
        react (nreact,3) = ir3c

        IF (nop1==0) THEN
          react (nreact,4) = ir3
          react (nreact,5) = ir4 + 1 + is4
        ELSE
          react (nreact,4) = ir3 + 1 + is3
          react (nreact,5) = ir4
        ENDIF

        react (nreact,6) = ir5
        react (nreact,7) = ir6

        gamm(nreact)  = gamm(jj)
        alpha(nreact) = alpha(jj)
        bet(nreact)   = bet(jj)
        itype(nreact) = itype(jj)

      ENDIF

!--- ON PASSE A l EXAMEN DE LA REACTION SUIVANTE :

      CYCLE

    ENDIF

!-----------------------------------------------------------------------------
!--- 2- DEUXIEME CAS : IL Y A DE l O DANS UN SEUL REACTANT
!       ************                      *******
!
!---DEUX POSSIBILITES:
!
!    A- IL Y A DE l O DANS LES DEUX PRODUITS ==> 2 REACTIONS ISOTOPEES
!    B- OU BIEN DANS UN SEUL                 ==> 1 REACTION ISOTOPEE
!-----------------------------------------------------------------------------

    IF (nor1 > 0 .OR. nor2 > 0) THEN

!-----------------------------------------------------------------------------
!
!                     LES REACTIONS s ECRIVENT ALORS :
!-----------------------------------------------------------------------------
!------------ A) PREMIERE POSSIBILITE : IL Y A DE l O DANS LES DEUX PRODUITS :
!                                                          ********
!
!      AO  +  B   -->  A'O  +  B'O
!  OU  A   +  BO  -->  A'O  +  B'O
!
!                            =====>      AO*  +  B    -->  A'O*  +  B'O
!                                    OU  A    +  BO   -->  A'O*  +  B'O
!
!
!                        ET  =====>      AO*  +  B    -->  A'O   +  B'O*
!                                    OU  A    +  BO   -->  A'O   +  B'O*
!
!                          ==> 2 REACTIONS ISOTOPEES
!-----------------------------------------------------------------------------

      IF (nop1 > 0 .AND. nop2 > 0) THEN

!-----------------------------------------------------------------------------
!
!--- PREMIERE REACTION ISOTOPEE:

        nreact = nreact + 1

        react (nreact,0) = ir0
        IF (nor1==0) THEN
          react (nreact,1) = ir1
          react (nreact,2) = ir2 + 1 + is2
        ELSE
          react (nreact,1) = ir1 + 1 + is1
          react (nreact,2) = ir2
        ENDIF
        react (nreact,3) = ir3c

        react (nreact,4) = ir3 + 1 + is3
        react (nreact,5) = ir4
        react (nreact,6) = ir5
        react (nreact,7) = ir6

        gamm(nreact)  = gamm(jj)/2.0
        alpha(nreact) = alpha(jj)
        bet(nreact)   = bet(jj)
        itype(nreact) = itype(jj)

!--- DEUXIEME REACTION ISOTOPEE:

        nreact = nreact + 1

        react (nreact,0) = ir0
        IF (nor1==0) THEN
          react (nreact,1) = ir1
          react (nreact,2) = ir2 + 1 + is2
        ELSE
          react (nreact,1) = ir1 + 1 + is1
          react (nreact,2) = ir2
        ENDIF
        react (nreact,3) = ir3c

        react (nreact,4) = ir3
        react (nreact,5) = ir4 + 1 + is4
        react (nreact,6) = ir5
        react (nreact,7) = ir6

        gamm(nreact)  = gamm(jj)/2.0
        alpha(nreact) = alpha(jj)
        bet(nreact)   = bet(jj)
        itype(nreact) = itype(jj)

!-----------------------------------------------------------------------------
!------------ B) DEUXIEME POSSIBILITE : IL Y A DE l O DANS UN SEUL PRODUIT :
!                                                          *******
!
!      AO  +  B   -->  A'O  +  B'
!  OU  A   +  BO  -->  A'O  +  B'
!  OU  AO  +  B   -->  A'   +  B'O
!  OU  A   +  BO  -->  A'   +  B'O
!
!                            =====>      AO*  +  B    -->  A'O*  +  B'
!                                    OU  A    +  BO*  -->  A'O*  +  B'
!                                    OU  AO*  +  B    -->  A'    +  B'O*
!                                    OU  A    +  BO*  -->  A'    +  B'O*
!
!                       ==> 1 REACTION ISOTOPEE
!-----------------------------------------------------------------------------

      ELSE IF (nop1 > 0 .OR. nop2 > 0) THEN

!-----------------------------------------------------------------------------
!
!--- UNE SEULE REACTION ISOTOPEE :

        nreact = nreact + 1

        react (nreact,0) = ir0
        IF (nor1==0) THEN
          react (nreact,1) = ir1
          react (nreact,2) = ir2 + 1 + is2
        ELSE
          react (nreact,1) = ir1 + 1 + is1
          react (nreact,2) = ir2
        ENDIF
        react (nreact,3) = ir3c

        IF (nop1==0) THEN
          react (nreact,4) = ir3
          react (nreact,5) = ir4 + 1 + is4
        ELSE
          react (nreact,4) = ir3 + 1 + is3
          react (nreact,5) = ir4
        ENDIF

        react (nreact,6) = ir5
        react (nreact,7) = ir6

        gamm(nreact)  = gamm(jj)
        alpha(nreact) = alpha(jj)
        bet(nreact)   = bet(jj)
        itype(nreact) = itype(jj)

      ENDIF
    ENDIF
  ENDDO

!----------------------------------------------------------------------------
!                     FIN DE l EXAMEN DES REACTIONS
!          ET DE LA CONSTRUCTION DE LA CHIMIE ISOTOPEE DE l OXYGENE
!----------------------------------------------------------------------------

  PRINT *,"End of construction of O18 chemistry"

END SUBROUTINE ISOTO

END MODULE PXDR_ISOTCO
