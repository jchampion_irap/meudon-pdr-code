
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

! JLB - 24 May 2008

MODULE PREP_ANALYS

  PRIVATE

  PUBLIC :: ANALYS

CONTAINS

!%%%%%%%%%%%%%%%%
SUBROUTINE ANALYS
!%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_STR_DATA
   USE PREP_VAR
   USE PREP_AUTRE

   IMPLICIT NONE

   REAL (KIND=dp), DIMENSION(nqm) :: ab1, ab2, ab3
   REAL (KIND=dp), DIMENSION(nqm) :: fabric, destru

   CHARACTER (LEN=8) :: yspec
   REAL (KIND=dp) :: dmax, fmax
   REAL (KIND=dp) :: dpc, fpc, toto, dtot, ftot, eq_rel
   REAL (KIND=dp) :: seuil, answ
   INTEGER        :: ii, jj, jmax
   INTEGER        :: j1, j2, j3
   INTEGER        :: iff, id, k
   INTEGER        :: nrxi, isp, lev
   INTEGER        :: ii0, iopt

   abnua(n_var+1,0:npo) = 1.0_dp
   abnua(n_var+2,0:npo) = 1.0_dp
   abnua(n_var+3,0:npo) = 1.0_dp
   abnua(n_var+4,0:npo) = 1.0_dp
   abnua(n_var+5,0:npo) = 1.0_dp
   abnua(n_var+6,0:npo) = 1.0_dp

   PRINT *, '*******************************'
   PRINT *, '    Chemistry Analysis'
   PRINT *, '*******************************'
   PRINT *,'Do you want evolution of reaction rates with Av?'
   PRINT *,'1-yes, 0-no'
   READ *,ii0
   IF (ii0 == 1) THEN
      CALL EVOLREACT
   ENDIF

   PRINT *, '-----------------------------------------'
   PRINT *, '     Chemistry Analysis at a fixed Av'
   PRINT *, '-----------------------------------------'

   answ = 0
   DO WHILE (answ /= 1)
      PRINT *, 'Give Av position index'
      PRINT *, 'max value: (iopt):', npo
      PRINT *, 'End analysis: -1'
      READ *, iopt

      IF (iopt == -1) THEN
         RETURN
      ENDIF

      PRINT *
      av = tau(iopt) * tautav
      PRINT *, 'Point ', iopt, ', Av =', av, ", T = ", tgaz(iopt)
      PRINT *, 'Keep that point?'
      PRINT *, '1- yes   0- no'
      READ  *, answ
   ENDDO

   PRINT *, 'Give threshold (Higher => more reactions. All => 0)'
   READ *, seuil

   temp = tgaz(iopt)

   h2para = 0.0_dp
   h2orth = 0.0_dp
   DO lev = 1, l0h2
      jj = njl_h2(lev)
      IF (MOD(jj,2) == 0) THEN
         h2para = h2para + spec_lin(in_sp(i_h2))%xre(lev,iopt)
      ELSE
         h2orth = h2orth + spec_lin(in_sp(i_h2))%xre(lev,iopt)
      ENDIF
   ENDDO
   PRINT *, " H2 para:", h2para, " h2 ortho:", h2orth

   CALL XKFILL (iopt)

   DO
      PRINT *
      PRINT *, 'Which species? (fin: -1)'
      READ (*,'(a8)') yspec
      IF (yspec == '-1') THEN
         EXIT
      ENDIF

      DO jj = 1, n_var
         IF (speci(jj) == yspec) THEN
            isp = jj
            EXIT
         ENDIF
      ENDDO
      IF (jj > nsp_n1 .AND. jj <= nsp_n1+nsp_g1) THEN
         PRINT *, yspec, " is not computed directly"
         PRINT *, "      Use and check the corresponding moment equations."
         CYCLE
      ELSE IF (jj > n_var) THEN
         PRINT *, yspec, " is not in chemistry"
         CYCLE
       ENDIF

      fabric(1:neqt) = 0.0_dp
      destru(1:neqt) = 0.0_dp

      nrxi = xmatk(isp)%nbt
      PRINT *, "  Number of reactions: ", nrxi

      DO k = 1, nrxi
         j1 = xmatk(isp)%fdt(k)%r1
         j2 = xmatk(isp)%fdt(k)%r2
         j3 = xmatk(isp)%fdt(k)%r3
         ab1(k) = abnua(j1,iopt)
         ab2(k) = abnua(j2,iopt)
         ab3(k) = abnua(j3,iopt)
      ENDDO

!    Formation and destruction

      iff = 0
      id = 0
      ftot = 0.0_dp
      dtot = 0.0_dp
      DO k = 1, nrxi
         toto = xmatk(isp)%fdt(k)%k
         IF (toto > 0.0_dp) THEN
            iff = iff + 1
            fabric(k) = toto * ab1(k) * ab2(k) * ab3(k)
            ftot = ftot + fabric(k)
         ELSE IF (toto < 0.0_dp) THEN
            id = id + 1
            destru(k) = -toto * ab1(k) * ab2(k) * ab3(k)
            dtot = dtot + destru(k)
         ENDIF
      ENDDO

      PRINT *
      PRINT *, '-----------------------------------------------'
      PRINT *, '        Chemistry of:  ', yspec
      PRINT *, '-----------------------------------------------'
      PRINT *
      PRINT *, "Av= ", av, " T =", tgaz(iopt), " (K)"
      IF (isp == nspec) THEN
         WRITE (6,'(" Photo-ejection of electrons by grains:",1p,E18.6," (cm-3 s-1)")') relgr(1,iopt)
         WRITE (6,'(" Recombination of electrons on grains: ",1p,E18.6," (cm-3 s-1)")') relgr(2,iopt)
         ftot = ftot + relgr(1,iopt)
         dtot = dtot + relgr(2,iopt)
      ENDIF
      IF (isp <= nspec) THEN
         WRITE (6,1003) ftot, dtot, abnua(isp,iopt)
      ELSE
         WRITE (6,1004) ftot, dtot, abnua(isp,iopt)
      ENDIF
      eq_rel = ABS(ftot - dtot) / (ftot + dtot)
      IF (eq_rel > 1.0e-2_dp) THEN
         WRITE (6,'("  WARNING: poor chemical balance:",1P,E15.6," %")') 100.0_dp * ABS(ftot - dtot) / (ftot + dtot)
      ENDIF
      PRINT *
      fpc = ftot
      dpc = dtot
      IF (seuil /= 0.0_dp) THEN
         ftot = ftot / seuil
         dtot = dtot / seuil
      ELSE
         ftot = 0.0_dp
         dtot = 0.0_dp
      ENDIF

      PRINT *, '-- Formation  Processes of ', yspec,':'

      DO ii = 1, nrxi
         fmax = 0.0_dp
         DO jj = 1, nrxi
            IF (fabric(jj) > fmax) THEN
               fmax = fabric(jj)
               jmax = jj
            ENDIF
         ENDDO

         IF (fmax < ftot .OR. fmax == 0.0_dp) EXIT

         WRITE (6,1001) xmatk(isp)%fdt(jmax)%it &
                   , speci(xmatk(isp)%fdt(jmax)%r1), speci(xmatk(isp)%fdt(jmax)%r2) &
                   , speci(xmatk(isp)%fdt(jmax)%r3), speci(xmatk(isp)%fdt(jmax)%p1) &
                   , speci(xmatk(isp)%fdt(jmax)%p2), speci(xmatk(isp)%fdt(jmax)%p3) &
                   , speci(xmatk(isp)%fdt(jmax)%p4), fmax, fmax / abnua(isp,iopt) &
                   , 100.0_dp*fmax/fpc
         fabric(jmax) = 0.0_dp
      ENDDO

      PRINT *, ' '
      PRINT *, '-- Destruction of ', yspec,':'

      DO ii = 1, nrxi
         dmax = 0.0_dp
         DO jj = 1, nrxi
           IF (destru(jj) > dmax) THEN
             dmax = destru(jj)
             jmax = jj
           ENDIF
         ENDDO

         IF (dmax < dtot .OR. dmax == 0.0_dp) EXIT

         WRITE (6,1001) xmatk(isp)%fdt(jmax)%it &
                    , speci(xmatk(isp)%fdt(jmax)%r1), speci(xmatk(isp)%fdt(jmax)%r2) &
                    , speci(xmatk(isp)%fdt(jmax)%r3), speci(xmatk(isp)%fdt(jmax)%p1) &
                    , speci(xmatk(isp)%fdt(jmax)%p2), speci(xmatk(isp)%fdt(jmax)%p3) &
                    , speci(xmatk(isp)%fdt(jmax)%p4), dmax, dmax / abnua(isp,iopt) &
                    , 100.0_dp*dmax/dpc
         destru(jmax) = 0.0_dp
      ENDDO

      PRINT *, '-----------------------------------------------'

   ENDDO

 1001 format (1x,i4,1x,a8,' + ',a8,' + ',a8,' = ',4a8,5x,1pe12.4,'  cm-3 s-1',e12.4,' s-1',0pf8.2,' %',i4)
 1003 format (1x,'fabric:',1pe15.6,' (cm-3 s-1), destru:',e15.6,' (cm-3 s-1), abond:',e15.6,' (cm-3)')
 1004 format (1x,'fabric:',1pe15.6,' (cm-3 s-1), destru:',e15.6,' (cm-3 s-1), abond:',e15.6,' (part / grain)')

END SUBROUTINE ANALYS

!%%%%%%%%%%%%%%%%%%%
SUBROUTINE EVOLREACT
!%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PREP_VAR
   USE PREP_AUTRE

   IMPLICIT NONE

!-------------------------------------------------------------
! DECLARATIONS
!-------------------------------------------------------------

   REAL (KIND=dp)     :: seuil, toto
   INTEGER            :: numdest2, numfabr2
   INTEGER            :: j1, j2, j3
   INTEGER            :: lev, iopt, k, isp
   INTEGER            :: jj, nrxi

   CHARACTER (LEN=75) :: fichier1, fichier2, fichier3
   CHARACTER (LEN=8)  :: yspec

   REAL (KIND=dp)     :: fabri(1:neqt,0:npo), destr(1:neqt,0:npo)
   REAL (KIND=dp)     :: fabtot(0:npo), destot(0:npo)
   REAL (KIND=dp)     :: taudest, taufabr
   REAL (KIND=dp)     :: pourfabr(0:npo,1:neqt)
   REAL (KIND=dp)     :: pourdest(0:npo,1:neqt)
   REAL (KIND=dp)     :: pourfabr2(0:npo,1:neqt)
   REAL (KIND=dp)     :: pourdest2(0:npo,1:neqt)
   REAL (KIND=dp)     :: ab1(nqm), ab2(nqm), ab3(nqm)

   INTEGER            :: numdest, numfabr
   INTEGER            :: flagreact(neqt)
!------------------------------------------------------------
! xmatk(isp)%fdt(k)%k : cste de vitesse de form (si >0), de dest (si <0)
! de l'espece isp dans la k ieme reaction ou elle intervient.

!------------------------------------------------------------
! ESPECE
!------------------------------------------------------------
   PRINT *,'Which species do you want?'
   READ (*,'(a8)') yspec
   DO jj = 1, nspec
      IF (speci(jj) == yspec) THEN
         isp = jj               !num de l'esp choisie
      ENDIF
   ENDDO

   PRINT *,'Enter a threshold (%)'
   READ(*,*) seuil

!------------------------------------------------------------
! INITIALISATION
!------------------------------------------------------------
   nrxi = xmatk(isp)%nbt      ! nbre de reaction concernant isp
   DO k = 1, nrxi
      flagreact(k) = 0        ! on met un flag a zero a toutes ces reaction
   ENDDO                      ! qd flag vaut 1, on garde la reaction

   numfabr = 0                ! nbre de reactions de fabr qu'il faudra sortir
   numdest = 0                ! idem pour destruction

!----------------------------------------------------------
! CALCUL DES TAUX DE FABRICATION ET DE DESTRUCTION TOTAL.
!----------------------------------------------------------
   DO iopt = 0, npo
      temp = tgaz(iopt)
      av = tau(iopt) * tautav

      h2para = 0.0_dp
      h2orth = 0.0_dp
      DO lev = 1, spec_lin(in_sp(i_h2))%use
         jj = njl_h2(lev)
         IF (MOD(jj,2) == 0) THEN
            h2para = h2para + spec_lin(in_sp(i_h2))%xre(lev,iopt)
         ELSE
            h2orth = h2orth + spec_lin(in_sp(i_h2))%xre(lev,iopt)
         ENDIF
      ENDDO

      CALL XKFILL (iopt)

      fabri(1:neqt,iopt) = 0.0_dp
      destr(1:neqt,iopt) = 0.0_dp

      DO k = 1, nrxi
         j1 = xmatk(isp)%fdt(k)%r1
         j2 = xmatk(isp)%fdt(k)%r2
         j3 = xmatk(isp)%fdt(k)%r3
         ab1(k) = abnua(j1,iopt)              ! abond du premier reactif
         ab2(k) = abnua(j2,iopt)              ! abond du second reactif
         ab3(k) = abnua(j3,iopt)              ! abond du troisieme reactif
      ENDDO

! Calcul du taux de fabrication total et du tau de destruction total
! au point iopt.

      fabtot(iopt) = 0.0_dp
      destot(iopt) = 0.0_dp

      DO k = 1, nrxi
         toto = xmatk(isp)%fdt(k)%k       !taux de la kieme reaction
         IF (toto > 0.0_dp) THEN
            fabri(k,iopt) = toto * ab1(k) * ab2(k) * ab3(k)
            fabtot(iopt) = fabtot(iopt) + fabri(k,iopt)
         ELSE IF (toto < 0.0_dp) THEN
            destr(k,iopt) = -toto * ab1(k) * ab2(k) * ab3(k)
            destot(iopt) = destot(iopt) + destr(k,iopt)
         ENDIF
      ENDDO

   ENDDO

!--------------------------------------------------------------------
! RECHERCHE DES REACTIONS A CONSERVER
!--------------------------------------------------------------------
! En chaque point iopt, on calcul le pourcentage que represente chaque
! reaction et si ce pourcentage est superieur au seuil on la garde.
! Les reaction qu'on garde sont associees a un indice different de 0
! dans le tableau flagreact. Si cet indice vaut 1, c'est une reaction
! de formation, s'il vaut -1, de destruction.
! Que la reaction soit conservee ou non, les pourcentages sont conserves
! dans deux tableaux: pourdest et pourfabr.

   DO iopt = 0, npo
      DO k = 1, nrxi
         toto = xmatk(isp)%fdt(k)%k
         IF (toto < 0.0_dp) THEN
            taudest = destr(k, iopt)
            pourdest(iopt,k) = 100.0_dp * taudest / destot(iopt)
            IF (pourdest(iopt,k) > seuil) THEN
               IF (flagreact(k) == 0) THEN
                  flagreact(k)=-1
                  numdest = numdest+1
               ENDIF
            ENDIF
         ELSE IF (toto > 0.0_dp) THEN
            taufabr = fabri(k, iopt)
            pourfabr(iopt,k) = 100.0_dp * taufabr / fabtot(iopt)
            IF (pourfabr(iopt,k) > seuil) THEN
               IF (flagreact(k) == 0) THEN
                  flagreact(k)=1
                  numfabr = numfabr +1
               ENDIF
            ENDIF
         ENDIF
      ENDDO
   ENDDO


! Pour les sorties, il ne faut garder dans deux tableaux que les pourcentages
! des reactions conservees. On stocke ces pourcentages dans deux nouveaux
! tableaux pourdest2 et pourfabr2.
! On releve egalement a cette occasion les nombre de reaction de formation
! et le nombre de reaction de destruction.

   DO iopt = 0, npo
      numfabr2 = 0
      numdest2 = 0
      DO k = 1, nrxi
         IF (flagreact(k) == 1) THEN
            numfabr2 = numfabr2 +1
            pourfabr2(iopt,numfabr2)  = pourfabr(iopt,k)
         ENDIF
         IF (flagreact(k) == -1) THEN
            numdest2 = numdest2 +1
            pourdest2(iopt,numdest2) = pourdest(iopt,k)
         ENDIF
      ENDDO
   ENDDO

!---------------------------------------------------------------------
! SORTIE DES RESULTATS
!---------------------------------------------------------------------
   fichier1 = 'chimevold'
   fichier2 = 'chimevolf'
   fichier3 = 'chimevolr'

   OPEN(10,file=fichier1,status='unknown')
   OPEN(20,file=fichier2,status='unknown')
   OPEN(30,file=fichier3,status='unknown')

   WRITE(30,*) 'Reactions de fabrication'
   DO k = 1, nrxi
      IF (flagreact(k) == 1) THEN
         WRITE(30,*) speci(xmatk(isp)%fdt(k)%r1), speci(xmatk(isp)%fdt(k)%r2) &
                   , speci(xmatk(isp)%fdt(k)%r3), speci(xmatk(isp)%fdt(k)%p1) &
                   , speci(xmatk(isp)%fdt(k)%p2), speci(xmatk(isp)%fdt(k)%p3) &
                   , speci(xmatk(isp)%fdt(k)%p4)
      ENDIF
   ENDDO
   WRITE(30,*)
   WRITE(30,*) 'Reactions de destruction'
   DO k = 1, nrxi
      IF (flagreact(k) == -1) THEN
         WRITE(30,*) speci(xmatk(isp)%fdt(k)%r1), speci(xmatk(isp)%fdt(k)%r2) &
                   , speci(xmatk(isp)%fdt(k)%r3), speci(xmatk(isp)%fdt(k)%p1) &
                   , speci(xmatk(isp)%fdt(k)%p2), speci(xmatk(isp)%fdt(k)%p3) &
                   , speci(xmatk(isp)%fdt(k)%p4)
      ENDIF
   ENDDO

   PRINT *,'Nbre of formation reaction kept:',numfabr
   PRINT *,'Nbre of destruction reaction kept:',numdest
   PRINT *,'Results are in files:'
   PRINT *,' - chimevolr:    list of reactions'
   PRINT *,' - chimevolf:    formation rates'
   PRINT *,' - chimevold:    destructionrates '
   PRINT *,'Rem: Columns order are the same in all files'
   PRINT *

   DO iopt = 0, npo
      av = tau(iopt) * 1.085736
      WRITE(10,'(1p,100e12.5)') av, (pourdest2(iopt,k), k=1,numdest)
      WRITE(20,'(1p,100e12.5)') av, (pourfabr2(iopt,k), k=1,numfabr)
   ENDDO

   CLOSE(10)
   CLOSE(20)
   CLOSE(30)

!-------------------------------------------------------------------
! FIN
!-------------------------------------------------------------------

END SUBROUTINE EVOLREACT

END MODULE PREP_ANALYS
