
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

!--------+---------+---------+---------+---------+---------+---------+-*-------+

MODULE PREP_VAR

  USE PXDR_CONSTANTES
  USE PXDR_STR_DATA

  IMPLICIT NONE

  INTEGER, PUBLIC, PARAMETER :: ncolf  = 150  ! Max number of columns in output file
  INTEGER, PUBLIC, PARAMETER :: lenmax = 128  ! Max length of character string

  REAL (KIND=dp), PUBLIC, DIMENSION (0:nvxhd,0:noptm)             :: codhdv
  REAL (KIND=dp), PUBLIC, ALLOCATABLE, DIMENSION(:,:)             :: codeh3p
  REAL (KIND=dp), PUBLIC, DIMENSION (3,0:noptm)                   :: cdoph2

  CHARACTER(LEN=10), PUBLIC, DIMENSION (:), ALLOCATABLE           :: ndiesp

  REAL (KIND=dp), PUBLIC, DIMENSION(0:nvxh2,0:noptm)              :: h2v

  ! Quantites associees a des especes particulieres
  REAL (KIND=dp), PUBLIC                                          :: Emaxh3p_lu
  INTEGER,        PUBLIC, DIMENSION(:,:),   ALLOCATABLE           :: levh3p(:,:)
  INTEGER,        PUBLIC, DIMENSION(:,:,:), ALLOCATABLE           :: levh2o(:,:,:)

  ! grains:
  REAL (KIND=dp), PUBLIC                                          :: dustio
  REAL (KIND=dp), PUBLIC                                          :: depl
  REAL (KIND=dp), PUBLIC                                          :: rath2g

  ! output file
  REAL (KIND=dp), PUBLIC, DIMENSION (0:noptm)                     :: x
  REAL (KIND=dp), PUBLIC, DIMENSION (ncolf,0:noptm)               :: y
  CHARACTER (LEN=14), PUBLIC                                      :: textx
  CHARACTER (LEN=14), PUBLIC, DIMENSION (ncolf)                   :: texty
  INTEGER                                                         :: irec
  INTEGER                                                         :: iimax

  ! Convenient, but not used (see Franck)
  INTEGER, PUBLIC                                                 :: i_nvbc, i_njbc, i_natom

CONTAINS

!-------------------------------------------------------------------------
! SUBROUTINE : CASTIS
!-------------------------------------------------------------------------
SUBROUTINE CASTIS(n,sn,lsn)

INTEGER,           INTENT (IN)  :: n   ! INTEGER to transform in string
CHARACTER(len=10), INTENT (OUT) :: sn  ! string corresponding to the INTEGER
INTEGER,           INTENT (OUT) :: lsn ! number of figures in n

INTEGER :: ntemp ! temporary INTEGER
INTEGER :: div   ! division factor
INTEGER :: nn    ! modulo of ntemp by 10
INTEGER :: i

!--- Number of string to get
lsn = 1
div = 1
DO WHILE (n/div >= 10)
   div = div * 10
   lsn = lsn + 1
ENDDO

IF (lsn > 10) THEN
   PRINT *,"PROBLEM : Increase size of sn in CASTIS"
   STOP
ENDIF

!--- Cast from INTEGER to STRING
ntemp = n
DO i = 1, lsn
   nn = MOD(ntemp,10)
   sn(lsn-i+1:lsn-i+1) = ACHAR(nn+48)
   ntemp = (ntemp-nn)/10
ENDDO

END SUBROUTINE CASTIS

!-----------------------------------------------------------------------------
! SUBROUTINE : BUILD_LEVH2O
!-----------------------------------------------------------------------------
SUBROUTINE BUILD_LEVH2O

  USE PXDR_CHEM_DATA
  USE PXDR_AUXILIAR

  IMPLICIT NONE

  INTEGER :: nlvu
  INTEGER :: jmax, kamax, kcmax
  INTEGER :: qj, qka, qkc ! quantum numbers J, Ka, Kc
  INTEGER :: i, j_sp

   j_sp = in_sp(i_h2o)
   nlvu = spec_lin(j_sp)%use

   jmax = 0; kamax = 0; kcmax = 0
   DO i = 1, nlvu
      CALL STR_INT(spec_lin(j_sp)%quant(i,1),qj)
      CALL STR_INT(spec_lin(j_sp)%quant(i,2),qka)
      CALL STR_INT(spec_lin(j_sp)%quant(i,3),qkc)
      IF (qj  > jmax)  THEN
         CALL STR_INT(spec_lin(j_sp)%quant(i,1),jmax)
      ENDIF
      IF (qka > kamax) THEN
         CALL STR_INT(spec_lin(j_sp)%quant(i,2),kamax)
      ENDIF
      IF (qkc > kcmax) THEN
         CALL STR_INT(spec_lin(j_sp)%quant(i,3),kcmax)
      ENDIF
   ENDDO

   IF (.NOT. ALLOCATED(levh2o)) ALLOCATE(levh2o(0:jmax,0:kamax,0:kcmax))
   levh2o(:,:,:) = -1 ! A stupid value

   DO i = 1, spec_lin(j_sp)%use
      CALL STR_INT(spec_lin(j_sp)%quant(i,1),qj)
      CALL STR_INT(spec_lin(j_sp)%quant(i,2),qka)
      CALL STR_INT(spec_lin(j_sp)%quant(i,3),qkc)
      levh2o(qj, qka, qkc) = i
      PRINT *,"H2O - lev=",i,qj, qka, qkc
   ENDDO

END SUBROUTINE BUILD_LEVH2O


!-------------------------------------------------------------------------------
! SUBROUTINE : BUILD_LEVH3p
!-------------------------------------------------------------------------------

SUBROUTINE BUILD_LEVH3p

  USE PXDR_CHEM_DATA
  USE PXDR_AUXILIAR

  IMPLICIT NONE

  INTEGER :: nlvu
  INTEGER :: jmax, kmax
  INTEGER :: qj, qk ! quantum numbers J, K
  INTEGER :: i, j_sp

   j_sp = in_sp(i_h3p)
   nlvu  = spec_lin(j_sp)%use

   jmax = 0; kmax = 0
   DO i = 1, nlvu
      CALL STR_INT(spec_lin(j_sp)%quant(i,1), qj)
      CALL STR_INT(spec_lin(j_sp)%quant(i,2), qk)
      IF (qj > jmax) THEN
         CALL STR_INT(spec_lin(j_sp)%quant(i,1), jmax)
      ENDIF
      IF (qk > kmax) THEN
         CALL STR_INT(spec_lin(j_sp)%quant(i,2),kmax)
      ENDIF
   ENDDO

   IF (.NOT. ALLOCATED(levh3p)) ALLOCATE(levh3p(0:jmax,0:kmax))
   levh3p(:,:) = -1 ! A stupid value

   DO i = 1, spec_lin(j_sp)%use
      CALL STR_INT(spec_lin(j_sp)%quant(i,1),qj)
      CALL STR_INT(spec_lin(j_sp)%quant(i,2),qk)
      levh3p(qj, qk) = i
   ENDDO

END SUBROUTINE BUILD_LEVH3p

END MODULE PREP_VAR
