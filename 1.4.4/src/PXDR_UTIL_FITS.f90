!
!	PXDR_UTIL_FITS.f90
!	PDR_1.4_fits_Xcode
!
!	Created by Franck on 06/12/08.
!	Copyright 2008 LUTH - Observatoire de Paris. All rights reserved.
!

!******************************************************************************************************************************
! MODULES                                                                                                                     *
!******************************************************************************************************************************
MODULE PXDR_UTIL_FITS
USE PXDR_CONSTANTES
USE PXDR_UTIL_XML

INTEGER, PUBLIC, PARAMETER :: lentw = 10      ! Size string : type_write - Example : "str2int"
INTEGER, PUBLIC, PARAMETER :: lenkeyname = 8  ! Size string : name of a fits keyword
INTEGER, PUBLIC, PARAMETER :: lenkeyval  = 20 ! Size string : value of a fits keyword
INTEGER, PUBLIC, PARAMETER :: lenkeycom  = 52 ! Size string : commentary associated to a fits keyword
INTEGER, PUBLIC, PARAMETER :: lenkeyline = 80 ! Size string : keyword line

INTEGER, PUBLIC            :: statusfits
INTEGER, PUBLIC            :: unitfits

!******************************************************************************************************************************
!                                                     CONTAINS
!******************************************************************************************************************************
CONTAINS


!***************************************************************************************
! SUBROUTINES D'ECRITURE DANS LES FITS
!***************************************************************************************
! 0 - WRITE_HDU      : Principale subroutine qui fait appel aux autres
! Liste des routines ecrivant les HDU
! 1 - WRITE_STR      : Tableau 1D de STRING
! 2 - WRITE_STRINT   : Une colonne STRING + 1 colonne INT
! 3 - WRITE_STRDOUB  : Une colonne STRING + 1 colonne DOUBLE
! 3 - WRITE_STR2S    : Deux colonnes STRING (sval2D) + 1 colonne string (sval)
! 3 - WRITE_STR2I    : Deux colonnes STRING (sval2D) + 1 colonne integer (ival)
! 3 - WRITE_STR2D    : N colonnes STRING (sval2D)
! 4 - WRITE_INT      : Une colonne INT
! 5 - WRITE_INT2D    : Tableau 2D d'entiers
! 6 - WRITE_DOUBLE   : Une colonne double
! 7 - WRITE_DOUBLE2D : Tableau 2D de double
! 8 - WRITE_PARAMOD  : Ecriture des parametres du modele
! 9 - WRITE_CHIM     : Ecriture de la chimie
!*************************************************************************************

!=====================================================================================
! ECRITURE D'UN TABLEAU une colonne string, une colonne int
!=====================================================================================
SUBROUTINE WRITE_HDU(numhdu, extname, nlgn, ncol, type_write)

IMPLICIT NONE

INTEGER,                    INTENT(IN) :: numhdu          ! numero du hdu a creer
CHARACTER(len=lenID),       INTENT(IN) :: extname         ! nom de la table - ID du DU
INTEGER,                    INTENT(IN) :: nlgn            ! nbre de lignes dans la table
INTEGER,                    INTENT(IN) :: ncol            ! nbre de colonnes dans la table
CHARACTER(len=lentw),       INTENT(IN) :: type_write      ! type d'ecriture

INTEGER                       :: hdutype         !
INTEGER                       :: varidat         ! ???

CHARACTER(len=lenID)  , DIMENSION(:), ALLOCATABLE :: ttype     ! titre de colonne (ID)
CHARACTER(len=16)     , DIMENSION(:), ALLOCATABLE :: tform     ! format des quantites dans les colonnes
CHARACTER(len=lenunit), DIMENSION(:), ALLOCATABLE :: tunit     ! unite des quantites dans la colonne

INTEGER                       :: i

     ALLOCATE(ttype(ncol),tform(ncol),tunit(ncol))

!------------------------------------------------------------------------
! INITIALISATION DU FICHIER FITS
!------------------------------------------------------------------------

     !--- obtention d'un numero d'unite
     ! This has been moved before the writting of all DU
     !call ftgiou(unit,status)
     !call printerrfits(status)

     !--- ouverture avec autorisation d'ecrite ---
     ! This has been moved before the writting of all DU
     !readwrite = 1 ! autorisation d'ecriture
     !blocksize = 1 ! antiquite ne servant plus
     !call ftopen(unit,filename,readwrite,blocksize,status)
     !call printerrfits(status)

!-----------------------------------------------------------------------
! ECRITURE DU HDU
!-----------------------------------------------------------------------
     !--- deplacement au hdu avant la position ou on veut ecrire
     call ftmahd(unitfits,numhdu-1,hdutype,statusfits)
     call printerrfits(statusfits)

     !--- creation d'un nouvel hdu
     call ftcrhd(unitfits,statusfits)
     call printerrfits(statusfits)

     !--- definition des parametres pour la bintable
     ! ncols   = ncol_hdu ! Premiere colonne titres, Seconde entiers
     ! nrows   = nlgn_hdu
     ! extname = "Arrays dimensions"
     varidat = 0

     !--- ecriture des keywords -------------------------------
     ! ttype : nom des colonnes
     ! tform : format des colonnes
     ! tunit : unite des quantites dans les colonnes
     DO i = 1, ncol
        ttype(i) = descol(i)%IDFD
        tform(i) = descol(i)%form
        tunit(i) = descol(i)%unite
     ENDDO
     call ftphbn(unitfits,nlgn,ncol,ttype,tform,tunit,extname,varidat,statusfits)
     call printerrfits(statusfits)

     !--- ecriture des donnees --------------------------------
     SELECT CASE(type_write)
        CASE("string")
           CALL WRITE_STR(unitfits,nlgn, statusfits)
        CASE("string2D")
           CALL WRITE_STR2D(unitfits, nlgn, ncol, statusfits)
        CASE("str2str") ! 2 colonnes string + une colonne string
           CALL WRITE_STR2STR(unitfits, nlgn, statusfits)
        CASE("strint") ! une colonne string + une colonne int
           CALL WRITE_STRINT(unitfits, nlgn, statusfits)
        CASE("str2int") ! 2 colonnes string + une colonne int
           CALL WRITE_STR2INT(unitfits, nlgn, statusfits)
        CASE("strdbl")
           CALL WRITE_STRDBL(unitfits, nlgn, statusfits)
        CASE("strdbln") ! 1 colonne string + N colonnes double
           CALL WRITE_STRDBLN(unitfits, nlgn, ncol, statusfits)
        CASE("str2dbl") ! 2 colonnes string + N colonnes doubles
           CALL WRITE_STR2DBL(unitfits, nlgn, statusfits)
        CASE("str2dbln") ! 2 colonnes strings + N colonnes double
           CALL WRITE_STR2DBLN(unitfits, nlgn, ncol, statusfits)
        CASE("int")
           CALL WRITE_INT(unitfits, nlgn, statusfits)
        CASE("int2D")
           CALL WRITE_INT2D(unitfits, nlgn, ncol, statusfits)
        CASE("double") ! une colonne de double
           CALL WRITE_DOUBLE(unitfits, nlgn, statusfits)
        CASE("double2D") ! tableau 2D de reels
           CALL WRITE_DOUBLE2D(unitfits, nlgn, ncol, statusfits)
        CASE("paramod") ! parametres du modele
           CALL WRITE_PARAMOD(unitfits, nlgn, statusfits)
        CASE("chim") ! chimie
           CALL WRITE_CHIM(unitfits, nlgn, statusfits)
        CASE("line_data")
           CALL WRITE_LINEDATA(unitfits,nlgn,statusfits)
        CASE("level_data")
           CALL WRITE_LEVELDATA(unitfits,nlgn,ncol,statusfits)
        CASE("level_info")
           CALL WRITE_LEVELINFO(unitfits,nlgn,statusfits)
        CASE DEFAULT
           PRINT *,"Problem : writting type unknown"
           PRINT *,"type_write = ",type_write
           STOP
     END SELECT

!-----------------------------------------------------------------------
! FERMETURE DU FICHIER FITS
!-----------------------------------------------------------------------
     !--- fermeture ----------------------------------------------------
     ! This has been moved after the writting of all DU in OUTFITS
     !call ftclos(unitfits, statusfits)
     !call printerrfits(statusfits)
     !call ftfiou(unitfits, statusfits)
     !call printerrfits(statusfits)

     DEALLOCATE(ttype,tunit,tform)
     PRINT *,"Write Data Units, Status=",statusfits,", DU= ",TRIM(extname)

END SUBROUTINE WRITE_HDU

!***********************************************************************
!                   SUBROUTINES D'ECRITURE DES DONNEES
!***********************************************************************

!=======================================================================
! --- WRITE_STR : ECRITURE UNE COLONNE STRING
!=======================================================================

SUBROUTINE WRITE_STR(unit, nlgn, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(OUT) :: status

INTEGER              :: frow
INTEGER              :: felem
INTEGER              :: colnum

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     colnum = 1
     call ftpcls(unit,colnum,frow,felem,nlgn,sval,status)
     call printerrfits(status)

END SUBROUTINE WRITE_STR

!=======================================================================
! --- WRITE_STR2D : ECRITURE de N COLONNE STRING
!=======================================================================

SUBROUTINE WRITE_STR2D(unit, nlgn, ncol, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(IN)  :: ncol
INTEGER, INTENT(OUT) :: status

INTEGER              :: frow
INTEGER              :: felem
INTEGER              :: colnum
CHARACTER(len=lenstring), DIMENSION(:),ALLOCATABLE :: sval_col

     ALLOCATE(sval_col(nlgn))

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     DO colnum = 1, ncol
        sval_col(:) = sval2D(:,colnum)
        call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
        call printerrfits(status)
     ENDDO

     DEALLOCATE(sval_col)

END SUBROUTINE WRITE_STR2D

!=======================================================================
! --- WRITE_STR : ECRITURE DE DEUX COLONNES STRING + 1 COLONNE STRING
!=======================================================================
SUBROUTINE WRITE_STR2STR(unit, nlgn, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(OUT) :: status

INTEGER              :: frow
INTEGER              :: felem
INTEGER              :: colnum

CHARACTER(len=lenstring), DIMENSION(:),ALLOCATABLE :: sval_col

     ALLOCATE(sval_col(nlgn))

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     colnum = 1
     sval_col(:) = sval2D(:,1)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)
     colnum = 2
     sval_col(:) = sval2D(:,2)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)
     colnum = 3
     sval_col(:) = sval(:)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)

     DEALLOCATE(sval_col)

END SUBROUTINE WRITE_STR2STR

!=======================================================================
! --- WRITE_STRINT : ECRITURE UNE COLONNE STRING + UNE COLONNE INT
!=======================================================================
SUBROUTINE WRITE_STRINT(unit, nlgn, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(OUT) :: status

INTEGER :: frow
INTEGER :: felem
INTEGER :: colnum

CHARACTER(len=lenstring), DIMENSION(:),ALLOCATABLE :: sval_col
INTEGER, DIMENSION(:), ALLOCATABLE :: ival_col

     ALLOCATE(sval_col(nlgn),ival_col(nlgn))

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     colnum = 1
     sval_col(:) = sval(:)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)
     colnum = 2
     ival_col(:) = ival(:)
     call ftpclj(unit,colnum,frow,felem,nlgn,ival_col,status)
     call printerrfits(status)

     DEALLOCATE(ival_col, sval_col)

END SUBROUTINE WRITE_STRINT

!=======================================================================
! --- WRITE_STRINT : ECRITURE DEUX COLONNES STRING + UNE COLONNE INT
!=======================================================================
SUBROUTINE WRITE_STR2INT(unit, nlgn, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(OUT) :: status

INTEGER :: frow
INTEGER :: felem
INTEGER :: colnum

CHARACTER(len=lenstring), DIMENSION(:),ALLOCATABLE :: sval_col
INTEGER, DIMENSION(:), ALLOCATABLE :: ival_col

     ALLOCATE(sval_col(nlgn),ival_col(nlgn))

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     colnum = 1
     sval_col(:) = sval2D(:,1)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)
     colnum = 2
     sval_col(:) = sval2D(:,2)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)
     colnum = 3
     ival_col(:) = ival(:)
     call ftpclj(unit,colnum,frow,felem,nlgn,ival_col,status)
     call printerrfits(status)

     DEALLOCATE(ival_col, sval_col)

END SUBROUTINE WRITE_STR2INT

!=======================================================================
! --- WRITE_STRDOUB : ECRITURE UNE COLONNE STRING + UNE COLONNE DOUBLE
!=======================================================================
SUBROUTINE WRITE_STRDBL(unit, nlgn, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(OUT) :: status

INTEGER :: frow
INTEGER :: felem
INTEGER :: colnum

CHARACTER(len=lenstring), DIMENSION(:),ALLOCATABLE :: sval_col
REAL(kind=DP), DIMENSION(:), ALLOCATABLE :: rval_col

     ALLOCATE(sval_col(nlgn),rval_col(nlgn))

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     colnum = 1
     sval_col(:) = sval(:)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)
     colnum = 2
     rval_col(:) = rval(:)
     call ftpcld(unit,colnum,frow,felem,nlgn,rval_col,status)
     call printerrfits(status)

     DEALLOCATE(rval_col, sval_col)

END SUBROUTINE WRITE_STRDBL

!=======================================================================
! --- WRITE_STRDBLN : ECRITURE UNE COLONNE STRING + N COLONNE DOUBLE
!=======================================================================
SUBROUTINE WRITE_STRDBLN(unit, nlgn, ncol, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(IN)  :: ncol
INTEGER, INTENT(OUT) :: status

INTEGER :: frow
INTEGER :: felem
INTEGER :: colnum

CHARACTER(len=lenstring), DIMENSION(:),ALLOCATABLE :: sval_col
REAL(kind=DP), DIMENSION(:), ALLOCATABLE :: rval_col
INTEGER                       :: i

     ALLOCATE(sval_col(nlgn),rval_col(nlgn))

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     colnum = 1
     sval_col(:) = sval(:)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)
     DO i = 2, ncol
        colnum = i
        rval_col(:) = rval2D(:,i-1)
        call ftpcld(unit,colnum,frow,felem,nlgn,rval_col,status)
        call printerrfits(status)
     ENDDO

     DEALLOCATE(rval_col, sval_col)

END SUBROUTINE WRITE_STRDBLN

!=======================================================================
! --- WRITE_STR2DBLN : ECRITURE DE DEUX COLONNES STRING + N COLONNE DOUBLE
!=======================================================================
SUBROUTINE WRITE_STR2DBLN(unit, nlgn, ncol, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(IN)  :: ncol
INTEGER, INTENT(OUT) :: status

INTEGER :: frow
INTEGER :: felem
INTEGER :: colnum

CHARACTER(len=lenstring), DIMENSION(:),ALLOCATABLE :: sval_col
REAL(kind=DP), DIMENSION(:), ALLOCATABLE :: rval_col
INTEGER                       :: i

     ALLOCATE(sval_col(nlgn),rval_col(nlgn))

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     colnum = 1
     sval_col(:) = sval2D(:,1)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)
     colnum = 2
     sval_col(:) = sval2D(:,2)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)
     DO i = 3, ncol
        colnum = i
        rval_col(:) = rval2D(:,i-2)
        call ftpcld(unit,colnum,frow,felem,nlgn,rval_col,status)
        call printerrfits(status)
     ENDDO

     DEALLOCATE(rval_col, sval_col)

END SUBROUTINE WRITE_STR2DBLN

!=======================================================================
! --- WRITE_STRDOUB : ECRITURE DE DEUX COLONNES STRING + UNE COLONNE DOUBLE
!=======================================================================
SUBROUTINE WRITE_STR2DBL(unit, nlgn, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(OUT) :: status

INTEGER :: frow
INTEGER :: felem
INTEGER :: colnum

CHARACTER(len=lenstring), DIMENSION(:),ALLOCATABLE :: sval_col
REAL(kind=DP), DIMENSION(:), ALLOCATABLE :: rval_col

     ALLOCATE(sval_col(nlgn),rval_col(nlgn))

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     colnum = 1
     sval_col(:) = sval2D(:,1)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)
     colnum = 2
     sval_col(:) = sval2D(:,2)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)
     colnum = 3
     rval_col(:) = rval(:)
     call ftpcld(unit,colnum,frow,felem,nlgn,rval_col,status)
     call printerrfits(status)

     DEALLOCATE(rval_col, sval_col)

END SUBROUTINE WRITE_STR2DBL

!=======================================================================
! --- WRITE_INT : ECRITURE D'UNE COLONNE ENTIER
!=======================================================================
SUBROUTINE WRITE_INT(unit, nlgn, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(OUT) :: status

INTEGER              :: frow
INTEGER              :: felem
INTEGER              :: colnum


     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     colnum = 1
     call ftpclj(unit,colnum,frow,felem,nlgn,ival,status)
     call printerrfits(status)

END SUBROUTINE WRITE_INT

!=======================================================================
! --- WRITE_INT2D : ECRITURE D'UN TABLEAU 2D D'ENTIER
!=======================================================================
SUBROUTINE WRITE_INT2D(unit, nlgn, ncol, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(IN)  :: ncol
INTEGER, INTENT(OUT) :: status

INTEGER              :: frow
INTEGER              :: felem
INTEGER              :: colnum

INTEGER, DIMENSION(:), ALLOCATABLE :: ival_col

     ALLOCATE(ival_col(nlgn))

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     DO colnum = 1, ncol
        ival_col(:) = ival2D(:,colnum)
        call ftpclj(unit,colnum,frow,felem,nlgn,ival_col,status)
        call printerrfits(status)
     ENDDO

     DEALLOCATE(ival_col)

END SUBROUTINE WRITE_INT2D

!=======================================================================
! --- WRITE_DOUBLE : ECRITURE UNE COLONNE DOUBLE
!=======================================================================
SUBROUTINE WRITE_DOUBLE(unit, nlgn, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(OUT) :: status

INTEGER              :: frow
INTEGER              :: felem
INTEGER              :: colnum


     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     colnum = 1
     call ftpcld(unit,colnum,frow,felem,nlgn,rval,status)
     call printerrfits(status)

END SUBROUTINE WRITE_DOUBLE

!=======================================================================
! --- WRITE_DOUBLE2D : ECRITURE D'UN TABLEAU 2D DE DOUBLE
!=======================================================================
SUBROUTINE WRITE_DOUBLE2D(unit, nlgn, ncol, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(IN)  :: ncol
INTEGER, INTENT(OUT) :: status

INTEGER :: frow
INTEGER :: felem
INTEGER :: colnum

REAL(kind=DP), DIMENSION(:), ALLOCATABLE :: val_col

     ALLOCATE(val_col(nlgn))

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     DO colnum = 1, ncol
        val_col(:) = rval2D(:,colnum)
        call ftpcld(unit,colnum,frow,felem,nlgn,val_col,status)
        call printerrfits(status)
     ENDDO

     DEALLOCATE(val_col)

END SUBROUTINE WRITE_DOUBLE2D

!=======================================================================
! --- WRITE_PARAMOD : ECRITURE DES PARAMETRES DU MODELE
!=======================================================================
SUBROUTINE WRITE_PARAMOD(unit, nlgn, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(OUT) :: status

INTEGER :: frow
INTEGER :: felem
INTEGER :: colnum

REAL(KIND=DP)                 :: rval_col
INTEGER                       :: ival_col

     !--- ecriture des donnees --------------------------------------
     frow   = 1
     felem  = 1
     ! On ecrit d'abord les valeurs reelles puis entieres
     DO colnum = 1, nparam_mod_r
        rval_col = rval(colnum)
        call ftpcld(unit,colnum,frow,felem,nlgn,rval_col,status)
        print *,status
        call printerrfits(status)
     ENDDO
     DO colnum = nparam_mod_r+1, nparam_mod_r+nparam_mod_i
        ival_col = ival(colnum-nparam_mod_r)
        call ftpclj(unit,colnum,frow,felem,nlgn,ival_col,status)
        call printerrfits(status)
     ENDDO

END SUBROUTINE WRITE_PARAMOD

!=======================================================================
! --- WRITE_CHIM : ECRITURE DE LA CHIMIE
!=======================================================================
SUBROUTINE WRITE_CHIM(unit, nlgn, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(OUT) :: status

INTEGER :: frow
INTEGER :: felem
INTEGER :: colnum

REAL(kind=DP), DIMENSION(:), ALLOCATABLE :: rval2D_col
INTEGER, DIMENSION(:), ALLOCATABLE :: ival_col

     ALLOCATE(rval2D_col(nlgn),ival_col(nlgn))

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     DO colnum = 1, 8 ! Ecriture des especes chimiques
        ival_col(:) = ival2D(:,colnum)
        call ftpclj(unit,colnum,frow,felem,nlgn,ival_col,status)
        call printerrfits(status)
     ENDDO
     DO colnum = 9, 12 ! gamma, alpha, beta, de
        rval2D_col(:) = rval2D(:,colnum-8)
        call ftpcld(unit,colnum,frow,felem,nlgn,rval2D_col,status)
        call printerrfits(status)
     ENDDO
     ! ecriture de itype
     colnum = 13
     call ftpclj(unit,colnum,frow,felem,nlgn,ival,status)
     call printerrfits(status)

     DEALLOCATE(rval2D_col,ival_col)

END SUBROUTINE WRITE_CHIM

!=======================================================================
! --- WRITE_LINEDATA : ECRITURE DES LINEDATA
!=======================================================================
SUBROUTINE WRITE_LINEDATA(unit, nlgn, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(OUT) :: status

INTEGER :: frow
INTEGER :: felem
INTEGER :: colnum

CHARACTER(len=100), DIMENSION(:), ALLOCATABLE :: sval_col
REAL(kind=DP),      DIMENSION(:), ALLOCATABLE :: rval2D_col
INTEGER,            DIMENSION(:), ALLOCATABLE :: ival_col

     ALLOCATE(rval2D_col(nlgn),ival_col(nlgn),sval_col(nlgn))

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1
     DO colnum = 1, 3 ! Ecriture des index (transition, iup, jlo)
        ival_col(:) = ival2D(:,colnum)
        call ftpclj(unit,colnum,frow,felem,nlgn,ival_col,status)
        call printerrfits(status)
     ENDDO

     DO colnum = 4, 6 ! lam(metre), lam(K), Aij
        rval2D_col(:) = rval2D(:,colnum-3)
        call ftpcld(unit,colnum,frow,felem,nlgn,rval2D_col,status)
        call printerrfits(status)
     ENDDO

     colnum = 7 ! Information sur la transition
     sval_col(:) = sval2D(:,1)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)

     colnum = 8 ! ID de la transition
     sval_col(:) = sval2D(:,2)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)

     DEALLOCATE(rval2D_col,ival_col,sval_col)

END SUBROUTINE WRITE_LINEDATA

!=======================================================================
! --- WRITE_LEVELDATA : ECRITURE DES LINEDATA
!=======================================================================
SUBROUTINE WRITE_LEVELDATA(unit, nlgn, ncol, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(IN)  :: ncol
INTEGER, INTENT(OUT) :: status

INTEGER :: frow
INTEGER :: felem
INTEGER :: colnum

INTEGER                  :: nquant_number ! (spec_lin%nqn)

INTEGER :: i
CHARACTER(len=100), DIMENSION(:), ALLOCATABLE :: sval_col
REAL(kind=DP),      DIMENSION(:), ALLOCATABLE :: rval2D_col
INTEGER,            DIMENSION(:), ALLOCATABLE :: ival_col

     ALLOCATE(rval2D_col(nlgn),ival_col(nlgn),sval_col(nlgn))

     nquant_number = ncol - 4 ! Reminder : ncol = nqn + 4

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1

     colnum = 1 ! Indice des niveaux
     ival_col(:) = ival2D(:,colnum)
     call ftpclj(unit,colnum,frow,felem,nlgn,ival_col,status)
     call printerrfits(status)

     DO colnum = 2, 3 ! Energy (K) et Degenerescy
        rval2D_col(:) = rval2D(:,colnum-1)
        call ftpcld(unit,colnum,frow,felem,nlgn,rval2D_col,status)
        call printerrfits(status)
     ENDDO

     colnum = 3
     IF (nquant_number .NE. 0) THEN ! Quantum numbers described as string
        DO i = 1, nquant_number
           colnum = colnum + 1
           sval_col(:) = sval2D(:,i)
           call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
           call printerrfits(status)
        ENDDO
     ENDIF

     colnum = colnum + 1
     sval_col(:) = sval2D(:,nquant_number+1)
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)

     DEALLOCATE(sval_col,rval2D_col,ival_col)

END SUBROUTINE WRITE_LEVELDATA

!=======================================================================
! --- WRITE_LEVELINFO : ECRITURE DES LINEDATA
!=======================================================================
SUBROUTINE WRITE_LEVELINFO(unit, nlgn, status)

IMPLICIT NONE
INTEGER, INTENT(IN)  :: unit
INTEGER, INTENT(IN)  :: nlgn
INTEGER, INTENT(OUT) :: status

INTEGER :: frow
INTEGER :: felem
INTEGER :: colnum

CHARACTER(len=100), DIMENSION(:), ALLOCATABLE :: sval_col
REAL(kind=DP),      DIMENSION(:), ALLOCATABLE :: rval2D_col
INTEGER,            DIMENSION(:), ALLOCATABLE :: ival_col

     ALLOCATE(rval2D_col(nlgn),ival_col(nlgn),sval_col(nlgn))

     !--- ecriture des donnees -------------------------------
     frow   = 1
     felem  = 1

     ! VOIR TRANSFOBIN POUR COMPRENDRE L'ORDRE DES COLONNES
     colnum = 1 ! nom de l'espece
     sval_col(:) = sval2D(:,1) ! nom de l'espece
     call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
     call printerrfits(status)

     colnum = 2 ! mass de l'espece
     rval2D_col(:) = rval2D(:,1)
     call ftpcld(unit,colnum,frow,felem,nlgn,rval2D_col,status)
     call printerrfits(status)

     DO colnum = 3, 7 ! ind, nlv, nlv_use, nqn, ntr
        ival_col(:) = ival2D(:,colnum-2)
        call ftpclj(unit,colnum,frow,felem,nlgn,ival_col,status)
        call printerrfits(status)
     ENDDO

     DO colnum = 8, 8+(ival2D(1,4)-1)    ! ival2D(1,4) = nombre de nombres quantiques
        sval_col(:) = sval2D(:,colnum-6) ! la premiere colonne a deja ete copiee on commence donc a sval2D(:,2)
        call ftpcls(unit,colnum,frow,felem,nlgn,sval_col,status)
        call printerrfits(status)
     ENDDO

     DEALLOCATE(rval2D_col,sval_col,ival_col)

END SUBROUTINE WRITE_LEVELINFO

!==========================================================================
! INITIALISATION DE L'IMAGE BIDON
!==========================================================================
SUBROUTINE INIIMAGE(filename)

IMPLICIT NONE
CHARACTER(len=lenfilename), INTENT(IN) :: filename

INTEGER           :: bitpix
LOGICAL           :: simple
LOGICAL           :: extend
INTEGER           :: blocksize
INTEGER           :: naxis
INTEGER           :: naxes(1)
INTEGER           :: nelements
INTEGER           :: fpixel
INTEGER           :: group


!=========================================================
! ECRITURE DU FICHIER .FITS
!=========================================================
! Initialisation

! Open the fits file with write access
       blocksize = 1 ! artefact historique de fits - pas utilise
       call ftinit(unitfits,filename,blocksize,statusfits)
       call printerrfits(statusfits)

! INITALISATION DES PARAMETRES
       simple=.true.  ! ???
       bitpix=32      ! Codage 32 bits
       naxis=1        ! Nbre d'axe : 1 pour un spectre
       naxes(1)=1     ! Nbre de points sur l'axe
       extend=.true.  ! Le fichier fits peut contenir des extensions

       call ftphpr(unitfits,simple,bitpix,naxis,naxes,0,1,extend,statusfits)
       call printerrfits(statusfits)

       group  = 1
       fpixel = 1
       nelements = 1
       call ftpprj(unitfits,group,fpixel,nelements,0,statusfits)
       call printerrfits(statusfits)

 END SUBROUTINE INIIMAGE

!============================================================================
! SUBROUTINE DELETEFILE
!============================================================================
SUBROUTINE DELETEFILE(filename)

IMPLICIT NONE
CHARACTER(len=lenfilename), INTENT(IN)    :: filename

INTEGER :: unit
INTEGER :: blocksize
! A simple little routine to delete a FITS file

!  Simply return if status is greater than zero
      IF (statusfits .gt. 0)return

!  Get an unused Logical Unit Number to use to open the FITS file
     call ftgiou(unit,statusfits)

!  Try to open the file, to see if it exists
      call ftopen(unit,filename,1,blocksize,statusfits)

      IF (statusfits .eq. 0)THEN
!         file was opened;  so now delete it
          call ftdelt(unit,statusfits)
      ELSE IF (statusfits .eq. 104)THEN
!         file doesn't exist, so just reset status to zero and clear errors
          statusfits=0
          call ftcmsg
      ELSE
!         there was some other error opening the file; delete the file anyway
          statusfits=0
          call ftcmsg
          call ftdelt(unit,statusfits)
          print *,"Removed old file : - status ",statusfits
      ENDIF

!  Free the unit number for later reuse
      call ftfiou(unit, statusfits)

END SUBROUTINE DELETEFILE

!=============================================================================
! SUBROUTINE PRINTERRFITS (from cookbook.f)
!=============================================================================
SUBROUTINE PRINTERRFITS(status)

!  This subroutine prints out the descriptive text corresponding to the
!  error status value and prints out the contents of the internal
!  error message stack generated by FITSIO whenever an error occurs.

INTEGER, INTENT(INOUT) :: status

CHARACTER(len=30) :: errtext
CHARACTER(len=80) :: errmessage

!  Check if status is OK (no error); if so, simply return
      IF (status .le. 0)return

!  The FTGERR subroutine returns a descriptive 30-character text string that
!  corresponds to the integer error status number.  A complete list of all
!  the error numbers can be found in the back of the FITSIO User's Guide.
      call ftgerr(status,errtext)
      print *,'FITSIO Error Status =',status,': ',errtext

!  FITSIO usually generates an internal stack of error messages whenever
!  an error occurs.  These messages provide much more information on the
!  cause of the problem than can be provided by the single integer error
!  status value.  The FTGMSG subroutine retrieves the oldest message from
!  the stack and shifts any remaining messages on the stack down one
!  position.  FTGMSG is called repeatedly until a blank message is
!  returned, which indicates that the stack is empty.  Each error message
!  may be up to 80 characters in length.  Another subroutine, called
!  FTCMSG, is available to simply clear the whole error message stack in
!  cases where one is not interested in the contents.
      call ftgmsg(errmessage)
      DO WHILE (errmessage .ne. ' ')
          print *,errmessage
          call ftgmsg(errmessage)
      ENDDO
      STOP

END SUBROUTINE PRINTERRFITS

END MODULE PXDR_UTIL_FITS !------------------------------------------------------------------------
