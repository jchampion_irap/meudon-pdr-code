
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

! JLB - 22 May 2008

MODULE PXDR_EXCITH2

  PRIVATE

  PUBLIC :: EQLH2V, COLH2V

CONTAINS

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE EQLH2V (ttry, iopt)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PXDR_RF_TOOLS

   ! Computes equilibrium matrix of H2 levels and
   ! solves for detailed balace
   IMPLICIT NONE

   REAL (KIND=dp), INTENT (IN)                  :: ttry
   INTEGER, INTENT (IN)                         :: iopt

   REAL (KIND=dp), DIMENSION (:,:), ALLOCATABLE :: fr_des
   INTEGER                                      :: j_sp, nused, nrxi
   INTEGER                                      :: mm, lev, nrendo, if_ac
   INTEGER                                      :: k, k1, k2, k3, ii, ij, iv
   INTEGER                                      :: leva, levb, levu, iju, ivu
   REAL (KIND=dp)                               :: toto, fabric, destru
   REAL (KIND=dp)                               :: rateah2, ratedh2, rateeh2, ratefh2, rateerh2
   REAL (KIND=dp)                               :: endo_rxn, rat_l, taux, dumy

   j_sp = in_sp(i_h2)
   nused = spec_lin(in_sp(i_h2))%use

   IF ( .NOT. ALLOCATED(fr_des)) THEN
      ALLOCATE (fr_des(1:nused,npg))
   ENDIF

   ! Desorption occurs following a Boltzman distribution at grain temperature.
   DO mm = 1, npg
      toto = 0.0_dp
      DO lev = 1, nused
         fr_des(lev,mm) = spec_lin(j_sp)%gst(lev) * EXP(-spec_lin(in_sp(i_h2))%elk(lev) / tdust(mm,iopt))
         toto = toto + fr_des(lev,mm)
      ENDDO
      fr_des(:,mm) = fr_des(:,mm) / toto
   ENDDO

!  TIMH2 useful datas for caracteristic time scales of H2 (in s-1 cm-3)
!      TIMH2(iopt,1) : FABRIC : H2 formation by gas phase chemistry
!      TIMH2(iopt,2) : DESTRU : H2 destruction by gas phase chemistry
!      TIMH2(iopt,3) : RATEGR : H2 formation by Langmuir-Hinshelwood process
!      TIMH2(iopt,4) : DESPHO : H2 destruction by photons

   create(1:nused) = 0.0_dp
   remove(1:nused) = 0.0_dp

   ! Pure chemical contributions
   fabric = 0.0_dp
   destru = 0.0_dp

   nrxi = xmatk(i_h2)%nbt

   ! New version. JLB 26 II 2008
   nrendo = nh2for + ncrion + npsion + nasrad + nrest + 1
   rateah2 = 0.0_dp   ! Total accretion on grains
   if_ac = 0          ! 0 for accretion, 1 for rejection
   ratedh2 = 0.0_dp   ! Total desorption from grains
   rateeh2 = 0.0_dp   ! Total destruction by endothermal reactions
   ratefh2 = 0.0_dp   ! Total H2 formation on grains (physisorption)
   rateerh2 = 0.0_dp  ! Total H2 formation by Eley-Rideal process
   DO k = 1, nrxi
      k1 = xmatk(i_h2)%fdt(k)%r1
      k2 = xmatk(i_h2)%fdt(k)%r2
      k3 = xmatk(i_h2)%fdt(k)%r3
      SELECT CASE (xmatk(i_h2)%fdt(k)%it)
      CASE (0)  ! H2 formation - Old style
                ! Scaled by SQRT(T) and metallicity
         ratefh2 = gamm(nh2for) * densh(iopt) * (temp / bet(nh2for))**alpha(nh2for) &
                 * (g_ratio / 0.01_dp) / (istiny + ab(i_h))          ! cm+3 s-1
         ratefh2 = ratefh2 * ab(i_h) * ab(i_h)                      ! cm-3 s-1
      CASE (6)  ! Energy dependent rxns (endothermal)
         DO ii = 1, nused
            endo_rxn = MAX(bet(nrendo) - spec_lin(in_sp(i_h2))%elk(ii), 0.0_dp)
            rat_l = gamm(nrendo) * (ttry/300.0_dp)**alpha(nrendo) * EXP(-endo_rxn/ttry)
            remove(ii) = remove(ii) + rat_l * ab(k1) * ab(k2)
            rateeh2 = rateeh2 + rat_l * ab(k1) * ab(k2) * xreh2(ii,iopt)
         ENDDO
         nrendo = nrendo + 1
      CASE (61) ! Formation of CH+ following Agundez et al. ApJ (2010)
         IF (nused <= 8 ) THEN
            WRITE (*,*) ' Error in PXDR_EXCITH2'
            STOP
         ENDIF
         DO ii = 1, 8 ! Endothermal reaction with the 8 first levels of H2
            endo_rxn = MAX(4827.0_dp - spec_lin(in_sp(i_h2))%elk(ii), 0.0_dp)
            rat_l      = 1.58e-10_dp * EXP(-endo_rxn/ttry)
            remove(ii) = remove(ii) + rat_l * ab(k1) * ab(k2)
            rateeh2    = rateeh2 + rat_l * ab(k1) * ab(k2) * xreh2(ii,iopt)
         ENDDO
         DO ii = 9, nused
            remove(ii) = remove(ii) + 1.6e-9_dp * ab(k1) * ab(k2)
            rateeh2 = rateeh2 + 1.6e-9_dp * ab(k1) * ab(k2) * xreh2(ii,iopt)
         ENDDO
      CASE (7)  ! Photo-dissociation
         ! Matrix evrdbc does not include dissociation any more
         destru = destru + xmatk(i_h2)%fdt(k)%k*ab(k1)*ab(k2)*ab(k3)
      CASE (111) ! Formation on grains
         ratefh2 = ratefh2 + xmatk(i_h2)%fdt(k)%k * ab(k1) * ab(k2)
      CASE (20) ! Formation on grains by Eley-Rideal process
         rateerh2 = xmatk(i_h2)%fdt(k)%k * ab(k1) * ab(k2)
      CASE (113)  ! Accretion on grains
         IF (if_ac == 0) THEN
            taux = xmatk(i_h2)%fdt(k)%k
            rateah2 = rateah2 + taux * ab(k1)
            if_ac = 1
         ELSE
            taux = xmatk(i_h2)%fdt(k)%k
            rateah2 = rateah2 + taux * ab(k1) * ab(k2)
         ENDIF
      CASE (116, 117, 118)  ! Various desorption processes
         taux = xmatk(i_h2)%fdt(k)%k
         mm = MOD(k1-nspec,npg)
         IF (mm == 0) THEN
            mm = npg
         ENDIF
         DO lev = 1, nused
            create(lev) = create(lev) + taux * ab(k1) * fr_des(lev,mm)
         ENDDO
         ratedh2 = ratedh2 + taux * ab(k1)
      CASE DEFAULT
         taux = xmatk(i_h2)%fdt(k)%k
         IF (taux > 0.0_dp) THEN
            fabric = fabric + taux * ab(k1) * ab(k2) * ab(k3)
         ELSE
            destru = destru + taux * ab(k1) * ab(k2) * ab(k3)
         ENDIF
      END SELECT
   ENDDO

!--------------------up to here

   ! H2 formation on grains (beware signs of destru and rateeh2)
   timh21(iopt) = fabric
   timh22(iopt) = - destru + rateeh2 - pdiesp(kdeph0,iopt) * ab(i_h2)
   timh23(iopt) = ratefh2
   timh24(iopt) = pdiesp(kdeph0,iopt) * ab(i_h2)
   timh25(iopt) = rateerh2

   DO lev = 1, nused
      ij = njl_h2(lev)
      iv = nvl_h2(lev)
      dumy = 0.0_dp
      DO levu = nused+1, nlevh2
         iju = njl_h2(levu)
         ivu = nvl_h2(levu)
         dumy = dumy + pophm1(ivu,iju) * cashm(levu,lev)
      ENDDO
      create(lev) = create(lev) + (dumy + pophm1(iv,ij)) * ratefh2
   ENDDO

   ! Distribution of H2 formed by Eley-Rideal process
   DO lev = 1, nused
      ij = njl_h2(lev)
      iv = nvl_h2(lev)
      dumy = 0.0_dp
      DO levu = nused+1, nlevh2
         iju = njl_h2(levu)
         ivu = nvl_h2(levu)
         dumy = dumy + pophm2(ivu,iju) * cashm(levu,lev)
      ENDDO
      create(lev) = create(lev) + (dumy + pophm2(iv,ij)) * rateerh2
   ENDDO

   ! Distribution of H2 formed in the gas phase
   ! This is an ad hoc hypothesys
   !     2/3 on j = 0, 1/3 on j = 1
   !     because rxn h3+ + x -> h2 + xh+ dominates
   leva = lev_h2(0,0)
   levb = lev_h2(0,1)
   create(leva) = create(leva) + 2.0_dp * fabric / 3.0_dp
   create(levb) = create(levb) + fabric / 3.0_dp

   ! or: all on j = 0
!  leva = lev_h2(0,0)
!  create(leva) = create(leva) + fabric

   ! Destruction proportional to population
   remove = remove - destru - rateah2

END SUBROUTINE EQLH2V

!---------------------------------------------------------------------

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE COLH2V (ttry, iopt)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA

!---------------------------------------------------------------------
!      xnprot is no. density of protons (cm-3)
!      ttry is temperature of neutral fluid

!      called from sub. EQLH2V
!
!      collision rates for H2 + H come from Martin & Mandy.
!      New fits by Jacques Le Bourlot (july 1996)
!
!          or
!
!      from D. Flower (cf MNRAS)
!
!      Collision rates for H2 + He, H2 + H2o, H2 + H2p
!      come from D. Flower
!
!---------------------------------------------------------------------

   IMPLICIT NONE

   REAL (KIND=dp), INTENT (IN)                      :: ttry
   INTEGER, INTENT (IN)                             :: iopt

   REAL (KIND=dp), DIMENSION (30)                   :: gerlic
   INTEGER                                          :: nqc
   REAL (KIND=dp)                                   :: xchh2, fac, ctod
   REAL (KIND=dp)                                   :: decal, tdd, tdd2
   REAL (KIND=dp)                                   :: gmm, ga1, ga2
   REAL (KIND=dp)                                   :: ch2h_nr, ch2h_r, ch2he, ch2h
   REAL (KIND=dp)                                   :: ch2ph2, ch2oh2, cophp, c, d
   INTEGER                                          :: levu, levl, ndej, lev1, lev2
   INTEGER                                          :: ju, jl, ivu, ivl

   ! With David's prescription for O/P balance (3/1)
   ! Data from Gerlich for ortho-para transfer with h+.
   DATA gerlic / 2.024e-10_dp, 9.575e-10_dp, 2.729e-10_dp, 7.910e-10_dp &
         , 2.246e-10_dp, 6.223e-10_dp, 1.828e-10_dp, 4.975e-10_dp, 1.484e-10_dp &
         , 3.000e-10_dp, 1.000e-10_dp, 3.000e-10_dp, 1.000e-10_dp, 3.000e-10_dp &
         , 1.000e-10_dp, 3.000e-10_dp, 1.000e-10_dp, 3.000e-10_dp, 1.000e-10_dp &
         , 3.000e-10_dp, 1.000e-10_dp, 3.000e-10_dp, 1.000e-10_dp, 3.000e-10_dp &
         , 1.000e-10_dp, 3.000e-10_dp, 1.000e-10_dp, 3.000e-10_dp, 1.000e-10_dp &
         , 3.000e-10_dp /

!  Up to now collisions with H2 and He are used only for v = 0
!  Educated guesses may obtain from collisions with H
!  WARNING: c: desexcitation, d: excitation
!            h2h: H2 + H
!            h2h2: H2 + H2
!            h2he: H2 + He
!            ophp: H2 + H+ (ortho - para)

   IF (ichh2 == 0) THEN
      nqc = 4
   ELSE
      nqc = 3
   ENDIF
   IF (ichh2 <= 1) THEN
      xchh2 = 0.0_dp
   ELSE
      xchh2 = 1.0_dp
   ENDIF

   DO levu = 2, l0h2
      ju = njl_h2(levu)
      ivu = nvl_h2(levu)

      DO levl = 1, levu-1
         jl = njl_h2(levl)
         ivl = nvl_h2(levl)

         ! ctod is the Boltzmann factor used to compute
         !    excitation rates from desexcitation rates
         ndej = ABS(ju - jl)
         fac = (spec_lin(in_sp(i_h2))%elk(levu) - spec_lin(in_sp(i_h2))%elk(levl)) / ttry
         ctod = EXP(-fac) * (2.0_dp*ju+1.0_dp) / (2.0_dp*jl+1.0_dp)
         IF (MOD(ju,2) == 1) THEN
            ctod = ctod * 3.0_dp
         ENDIF
         IF (MOD(jl,2) == 1) THEN
            ctod = ctod / 3.0_dp
         ENDIF

         ! Fit of H2 + H rates. The reduced variable tdd is offset by a
         ! different value for reactive (resp. non-reactive) collisions.
         IF (MOD(ndej,2) == 0) THEN
            decal = 1.0_dp
         ELSE
            decal = 0.3_dp
         ENDIF
         tdd = decal + ttry * 1.0e-3_dp
         tdd2 = tdd*tdd

         ! Reactive and non-reactive collisions are mixed in Martin and Mandy
         gmm = q_h2_h(1,levu,levl) + q_h2_h(2,levu,levl) / tdd + q_h2_h(3,levu,levl) / tdd2
         IF (nqc == 4) THEN
            gmm = gmm +  q_h2_h(4,levu,levl) * tdd
         ENDIF

         ch2h_nr = 10.0_dp**gmm
         ch2h_r  = 0.0_dp

         ! Add reactive collisions (David's rule) if ichh2 = 3
         IF (ichh2 == 3 .AND. tau(iopt) <= 1.0_dp) THEN

            IF (MOD(ndej,2) == 0) THEN
               ch2h_r = 10.0_dp**gmm * EXP(-MAX(0.0_dp,(3900.0_dp - spec_lin(in_sp(i_h2))%elk(levu) &
                      + spec_lin(in_sp(i_h2))%elk(levl))/ttry))
            ELSE
               IF (jl > 0) THEN
                  lev1 = lev_h2(ivl,jl-1)
               ELSE
                  lev1 = lev_h2(ivl,jl+1)
               ENDIF
               lev2 = lev_h2(ivl,jl+1)
               IF (lev2 == 0) THEN
                  lev2 = lev_h2(ivl,jl-1)
               ENDIF
               ga1 = q_h2_h(1,levu,lev1) + q_h2_h(2,levu,lev1) / tdd + q_h2_h(3,levu,lev1) / tdd2
               IF (nqc == 4) THEN
                  ga1 = ga1 + q_h2_h(4,levu,lev1) * tdd
               ENDIF
               ga2 = q_h2_h(1,levu,lev2) + q_h2_h(2,levu,lev2) / tdd + q_h2_h(3,levu,lev2) / tdd2
               IF (nqc == 4) THEN
                  ga2 = ga2 + q_h2_h(4,levu,lev2) * tdd
               ENDIF
               ch2h_r = (10.0_dp**ga1 + 10.0_dp**ga2) * 0.5 * EXP(-MAX(0.0_dp,(3900.0_dp - spec_lin(in_sp(i_h2))%elk(levu) &
                      + spec_lin(in_sp(i_h2))%elk(levl))/ttry))

               IF (MOD(ju,2) == 1) THEN
                  ch2h_r = ch2h_r / 3.0_dp
               ENDIF
            ENDIF
         ENDIF

         ! Ortho/Para transfer with H from Schofield
         ! plus David's rule (for a few delta v = 0)
         IF (ichh2 >= 2 .AND.  ivu == ivl .AND. ndej < 3) THEN
            IF (MOD(ju,2) == 1 .AND. MOD(ndej,2) == 1) THEN
               ch2h_r = 8.0e-11_dp * EXP(-3900.0_dp/ttry) / 3.0_dp
            ELSE
               ch2h_r = 8.0e-11_dp * EXP(-3900.0_dp/ttry)
            ENDIF
         ENDIF

         ch2h = ch2h_nr + xchh2 * ch2h_r

         ! He - H2
         gmm = q_h2_he(1,levu,levl) + q_h2_he(2,levu,levl) / tdd + q_h2_he(3,levu,levl) / tdd2
         ch2he = 10.0_dp**gmm

         ! pH2 - H2
         gmm = q_h2_ph2(1,levu,levl) + q_h2_ph2(2,levu,levl) / tdd + q_h2_ph2(3,levu,levl) / tdd2
         ch2ph2 = 10.0_dp**gmm

         ! oH2 - H2
         gmm = q_h2_oh2(1,levu,levl) + q_h2_oh2(2,levu,levl) / tdd + q_h2_oh2(3,levu,levl) / tdd2
         ch2oh2 = 10.0_dp**gmm

         ! c- Ortho to Para transfer withions (Gerlich), only for v = 0
         ! IF (ivu == 0 .AND. ndej == 1) THEN
         IF (jl == ju-1) THEN
            cophp = gerlic(ju)
         ELSE
            cophp = 0.0_dp
         ENDIF

         ! Bringing it all back home
         c = ch2ph2 * ab(i_h2) * h2para + ch2oh2 * ab(i_h2) * h2orth &
           + ch2he * ab(i_he) + ch2h * ab(i_h)
         IF (i_hp /= 0) THEN
            c = c + cophp * ab(i_hp)
         ENDIF
         IF (i_h3p /= 0) THEN
            c = c + cophp * ab(i_h3p)
         ENDIF
         d = c * ctod

         eqcol(levl,levu) = eqcol(levl,levu) - c
         eqcol(levu,levu) = eqcol(levu,levu) + c
         eqcol(levu,levl) = eqcol(levu,levl) - d
         eqcol(levl,levl) = eqcol(levl,levl) + d
      ENDDO
   ENDDO

END SUBROUTINE COLH2V

END MODULE PXDR_EXCITH2
