
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

! 20 mai 2008 - JLB

MODULE PXDR_CHEM_DATA

! Chemistry related quantities
! Introduced 30 aug 2002 - JLB

  USE PXDR_CONSTANTES

! Different type of chemical reactions

  INTEGER, PUBLIC                                   :: nh2for   ! type 0 : H2 and HD formation on grains
                                                                ! Obsolete old style ! DON'T USE ANY MORE !
  INTEGER, PUBLIC                                   :: ncrion   ! type 1 : Cosmic rays processes
  INTEGER, PUBLIC                                   :: npsion   ! type 2 : Secondary photons processes
  INTEGER, PUBLIC                                   :: nasrad   ! type 3 : Radiative association
  INTEGER, PUBLIC                                   :: nrest    ! type 4 : Ordinary gas phase reactions
  INTEGER, PUBLIC                                   :: nh2endo  ! type 6 : Endothermal reactions with H2 (NEW!)
  INTEGER, PUBLIC                                   :: nphot_f  ! type 5 : Photoreactions (external fit)
  INTEGER, PUBLIC                                   :: nphot_i  ! type 7 : Photoreactions (direct integration)
  INTEGER, PUBLIC                                   :: i_mandat ! type 7 : Photoreactions (mandatory rxns -> not in sections)
  INTEGER, PUBLIC                                   :: kdeph0   ! type 7 : index of first mandatory photorxn
  INTEGER, PUBLIC                                   :: n3body   ! type 8 : 3 body reactions

  INTEGER, PUBLIC                                   :: ncpart   ! type 101, ... : Exceptions and special cases

  INTEGER, PUBLIC                                   :: ngsurf   ! type 11 : Reactions on grain surface
  INTEGER, PUBLIC                                   :: ngphot   ! type 12 : Photoreactions on grain surface
  INTEGER, PUBLIC                                   :: ngads    ! type 13 : Adsorption on to grains
  INTEGER, PUBLIC                                   :: ngneut   ! type 14 : Neutralization
  INTEGER, PUBLIC                                   :: nggdes   ! type 15 : Explosive desorption from grains
  INTEGER, PUBLIC                                   :: ncrdes   ! type 16 : Cosmic rays induced desorption
  INTEGER, PUBLIC                                   :: nphdes   ! type 17 : Photodesorption from grains
  INTEGER, PUBLIC                                   :: ngevap   ! type 18 : Evaporation from grains
  INTEGER, PUBLIC                                   :: ngchs    ! type 19 : Chemisorption on grains
  INTEGER, PUBLIC                                   :: ngelri   ! type 20 : Eley-Rideal process

  INTEGER, PUBLIC                                   :: nsp_ne   ! Number of neutral species
  INTEGER, PUBLIC                                   :: nsp_ip   ! Number of ionised species
  INTEGER, PUBLIC                                   :: nsp_im   ! Number of negatively charged species
  INTEGER, PUBLIC                                   :: nsp_cs   ! Number of chemisorbed species on grains (mono layer)
  INTEGER, PUBLIC                                   :: nsp_gm   ! Number of species on grains (multi layers)
  INTEGER, PUBLIC                                   :: nsp_g1   ! Number of species on grains (mono layer)
  INTEGER, PUBLIC                                   :: nsp_n1   ! = nsp_ne + nsp_ip + nsp_im + nsp_cs + nsp_gm
  INTEGER, PUBLIC                                   :: nspec    ! Number chemical species (total)
  INTEGER, PUBLIC                                   :: n_var    ! Number of variables in NR system
  INTEGER, PUBLIC                                   :: neqt     ! Number of chemical equations
  INTEGER, PUBLIC, PARAMETER                        :: nbrm = 3 ! Max number of reactants
  INTEGER, PUBLIC, PARAMETER                        :: nbpm = 4 ! Max number of products
  INTEGER, PUBLIC, PARAMETER                        :: nbsm = 7 ! Max number of species in a reaction

  REAL (KIND=dp), PUBLIC, DIMENSION (0:np)          :: enth     ! Reaction enthalpies
  INTEGER, PUBLIC, DIMENSION(nqm,0:nbsm)            :: react    ! Chemical reactions

  REAL (KIND=dp), PUBLIC, DIMENSION (0:nes,0:noptm) :: rxigr  ! Recombination rate of ions on grains
  REAL (KIND=dp), PUBLIC, DIMENSION (0:nes)         :: xkneut ! Size specific recombination rate of ions on grains
  REAL (KIND=dp), PUBLIC, DIMENSION (2,0:noptm)     :: relgr  ! Production and destruction rates of electrons by grains
  REAL (KIND=dp), PUBLIC, DIMENSION (2)             :: xkphel ! Size specific electrons - grains rates
                                                              ! 1: Photo-electric production rate
                                                              ! 2: Recombination destruction rate

! 12 Feb 2008 - Change the way to compute reaction rates and Jacobian
  TYPE (SPDF), DIMENSION (:), ALLOCATABLE           :: xmatk       ! New full reaction rates
  REAL (KIND=dp), PUBLIC, DIMENSION(:), ALLOCATABLE :: creat       ! Creation rate of species
  INTEGER, PUBLIC                                   :: k_fco_c_oh  ! index of reaction C + OH -> CO + H
  REAL (KIND=dp), PUBLIC                            :: t_fco_c_oh  ! exothermicity in K
  INTEGER, PUBLIC                                   :: k_fco_hcop  ! index of reaction HCO+ + e- -> CO + H
  REAL (KIND=dp), PUBLIC                            :: t_fco_hcop  ! exothermicity in K
  REAL (KIND=dp), PUBLIC                            :: T2_stick_ER ! Critical gas temperature for stick of H = 0.5
  REAL (KIND=dp), PUBLIC                            :: ex_stick_ER ! Temperature exponent for sticking coefficient

  INTEGER, PUBLIC                                   :: iadhgr ! H adsorption on grains reaction number
  INTEGER, PUBLIC                                   :: iaddgr ! D adsorption on grains reaction number
  INTEGER, PUBLIC                                   :: ifh2gr ! H2 formation on grains reaction number
  INTEGER, PUBLIC                                   :: ifhdgr ! HD formation on grains reaction number

  REAL (KIND=dp), PUBLIC, DIMENSION (nqm)           :: gamm   ! Arrhenius coefficient
  REAL (KIND=dp), PUBLIC, DIMENSION (nqm)           :: alpha  ! Arrhenius coefficient
  REAL (KIND=dp), PUBLIC, DIMENSION (nqm)           :: bet    ! Arrhenius coefficient
  REAL (KIND=dp), PUBLIC, DIMENSION (nqm)           :: de     ! Endothermicity
  INTEGER, PUBLIC, DIMENSION (nqm)                  :: itype  ! Type of reaction

  ! Tabular used to check conservation in chemical reactions
  REAL (KIND=dp), DIMENSION(nqm)                    :: bilanm ! bilan masse
  REAL (KIND=dp), DIMENSION(nqm)                    :: bilanc ! bilan charge
  REAL (KIND=dp), DIMENSION(0:np)                   :: charge ! charge of species

  ! Species related quantities
  CHARACTER (LEN=8), PUBLIC, DIMENSION (natom+6)    :: sp             ! List of atomic species + reaction partners

  DATA sp / 'h       ', 'c       ', 'n       ', 'o       ', &
            'he      ', 'd       ', 'c*      ', 'n*      ', &
            'o*      ', 'pah     ', 'f       ', 'na      ', &
            'mg      ', 'al      ', 'si      ', 'p       ', &
            's       ', 'cl      ', 'ca      ', 'fe      ', &
            'electr  ', 'photon  ', 'crp     ', 'phosec  ', &
            'grain   ', '2h      ', '        '/

  ! index of specific species that require some particular attention
  INTEGER, PUBLIC                                   :: i_h    = 0
  INTEGER, PUBLIC                                   :: i_d    = 0
  INTEGER, PUBLIC                                   :: i_hgr  = 0               ! H on grains
  INTEGER, PUBLIC                                   :: i_dgr  = 0               ! D on grains
  INTEGER, PUBLIC                                   :: i_hgrc = 0               ! H chemisorbed on grains
  INTEGER, PUBLIC                                   :: i_h2   = 0
  INTEGER, PUBLIC                                   :: i_hd   = 0
  INTEGER, PUBLIC                                   :: i_h2gr = 0               ! H2 on grains
  INTEGER, PUBLIC                                   :: i_hdgr = 0               ! HD on grains
  INTEGER, PUBLIC                                   :: i_he   = 0
  INTEGER, PUBLIC                                   :: i_c    = 0
  INTEGER, PUBLIC                                   :: i_n    = 0
  INTEGER, PUBLIC                                   :: i_o    = 0
  INTEGER, PUBLIC                                   :: i_o2   = 0
  INTEGER, PUBLIC                                   :: i_s    = 0
  INTEGER, PUBLIC                                   :: i_si   = 0
  INTEGER, PUBLIC                                   :: i_c13  = 0
  INTEGER, PUBLIC                                   :: i_o18  = 0
  INTEGER, PUBLIC                                   :: i_h2o  = 0
  INTEGER, PUBLIC                                   :: i_h2ogr = 0              ! H2O on grains
  INTEGER, PUBLIC                                   :: i_oh   = 0
  INTEGER, PUBLIC                                   :: i_c2   = 0
  INTEGER, PUBLIC                                   :: i_co   = 0               ! 12C16O
  INTEGER, PUBLIC                                   :: i_c13o = 0               ! 13C16O
  INTEGER, PUBLIC                                   :: i_co18 = 0               ! 12C18O
  INTEGER, PUBLIC                                   :: i_c13o18 = 0             ! 13C18O
  INTEGER, PUBLIC                                   :: i_cogr = 0               ! CO on grains
  INTEGER, PUBLIC                                   :: i_cs   = 0
  INTEGER, PUBLIC                                   :: i_hcn  = 0
  INTEGER, PUBLIC                                   :: i_hp   = 0
  INTEGER, PUBLIC                                   :: i_cp   = 0
  INTEGER, PUBLIC                                   :: i_c13p = 0
  INTEGER, PUBLIC                                   :: i_np   = 0
  INTEGER, PUBLIC                                   :: i_op   = 0
  INTEGER, PUBLIC                                   :: i_sp   = 0
  INTEGER, PUBLIC                                   :: i_sip  = 0
  INTEGER, PUBLIC                                   :: i_fep  = 0
  INTEGER, PUBLIC                                   :: i_h3p  = 0
  INTEGER, PUBLIC                                   :: i_h2dp = 0
  INTEGER, PUBLIC                                   :: i_hcop = 0
  INTEGER, PUBLIC                                   :: i_chp  = 0
  INTEGER, PUBLIC                                   :: j_c13o = 0
  INTEGER, PUBLIC                                   :: j_co18 = 0
  INTEGER, PUBLIC                                   :: j_c13o18 = 0

  REAL (KIND=dp), PUBLIC                            :: r_c13 = 90.0_dp          ! 12C / 13C ratio
  REAL (KIND=dp), PUBLIC                            :: r_o18 = 499.0_dp         ! 16O / 18O ratio

  CHARACTER (LEN=8),      DIMENSION(0:np)           :: speci   ! List of species in chemistry
  REAL (KIND=dp), PUBLIC, DIMENSION(0:np)           :: abin    ! Initial abundances od species

  REAL (KIND=dp), PUBLIC, DIMENSION(0:np)           :: mol     ! Molecular weight
  REAL (KIND=dp), PUBLIC, DIMENSION(0:natom)        :: abtot   ! Total relative abundances of atoms
  INTEGER, PUBLIC, DIMENSION(0:nes,0:natom)         :: ael     ! Chemical composition of species
  TYPE(ATOM), PUBLIC, DIMENSION(natom-1)            :: l_atom  ! List of atoms properties
  INTEGER, PUBLIC, DIMENSION(np)                    :: lcon    ! Conservation of species (in the gas)
                                                               ! if lcon(i) = 0 THEN species i is not used for conservation
                                                               ! if lcon(i) = j /= 0 THEN atom j is conserved thru species i
  INTEGER, PUBLIC, DIMENSION(0:natom,2)             :: icx     ! Conservation of atoms
                                                               ! icx(i,1) : index of species used for conservation of i
                                                               ! icx(i,2) : index of second most abundant species containing i
  REAL (KIND=dp), PUBLIC, DIMENSION(0:np)           :: t_diff  ! Diffusion barrier on grains
  REAL (KIND=dp), PUBLIC, DIMENSION(0:np)           :: t_evap  ! Evaporation barrier on grains
                                                               ! t_diff and t_evap are used only for species on grains
  INTEGER                                           :: ijkl

  DATA (l_atom(ijkl)%name, ijkl=1,natom-1) / "H", "C", "N", "O", "He", "D", "C13", "N15", "O18", "PAH", "F", "Na", "Mg" &
                                           , "Al", "Si", "P", "S", "Cl", "Ca", "Fe" /

END MODULE PXDR_CHEM_DATA
