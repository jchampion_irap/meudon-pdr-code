
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

! JLB - 23 May 2008

MODULE PREP_LECTUR

  PRIVATE

  PUBLIC :: PLECTUR

CONTAINS

!%%%%%%%%%%%%%%%%%
SUBROUTINE PLECTUR
!%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PXDR_RF_TOOLS
   USE PXDR_TRANSFER
   USE PREP_VAR

   IMPLICIT NONE

   INTEGER                         :: i, j, k, mm
   INTEGER                         :: iv, ij, iopt

!--------+---------+---------+---------+---------+---------+---------+-*-------+

   ! Read binary file
   PRINT *
   PRINT *,'----------------------------------------'
   PRINT *,' Reading : ', TRIM(fichier)
   PRINT *,'----------------------------------------'

   PRINT *, "Opening file :", TRIM(fichier)
   OPEN (26, file = fichier, status = 'old', form = 'unformatted')

   READ (26) version_output
   READ (26) version_code

   READ (26) i_natom
   READ (26) npo, nspec, n_var
   READ (26) cdunit, rrv
   !PRINT *,"Number of points : ", npo
   tautav = 2.5_dp * LOG10(EXP(1.0_dp))
   coef2 = tautav * cdunit / rrv
   READ (26) l0h2, l0hd
   !PRINT *,"l0h2 = ",l0h2,"l0hd =", l0hd

   ! Those dimensions are read but not used (identical parameters set in module)
   READ (26) i_nvbc, i_njbc
   READ (26) i_h, i_d, i_hgr, i_dgr, i_hgrc, i_h2, i_hd, i_h2gr, i_hdgr, i_he, i_c, i_n, i_o, i_s, &
             i_si, i_cp, i_np, i_op, i_sp, i_sip, i_h2o, i_h2ogr, i_oh, i_co, i_c13o, i_co18,  &
             i_c13o18, i_cogr, i_hp, i_h2dp, i_h3p, i_fep, i_c2, i_cs, i_c13, i_o18, i_hcop, &
             i_chp, i_hcn, i_o2

   READ (26) nspl
   ALLOCATE (in_sp(nspec+3))
   READ (26) in_sp
   READ (26) j_c13o, j_co18, j_c13o18
   ALLOCATE (spec_lin(nspl))
   DO i = 1, nspl
      CALL READ_LIN(spec_lin(i))
   ENDDO

   READ (26) (speci(i),   i=0,n_var+5) ! speci(nspec) : electr

   READ (26) npg
   ALLOCATE (surfgr(npg),tdust(npg,0:npo), zmoy(npg,0:npo))
   ALLOCATE (adust(npg), wwmg(npg))

   READ (26) ((zmoy(i,j), i=1,npg), j=0,npo)
   READ (26) (relgr(1:2,j), j=0,npo)
   READ (26) (adust(i), i=1,npg)
   READ (26) (wwmg(i), i=1,npg)
   READ (26) (surfgr(i), i=1,npg)
   READ (26) (tau(i), densh(i), tgaz(i), (tdust(j,i),j=1,npg), i=0,npo)

   READ (26) (reffep(i), i=0,npo)
   READ (26) (chophg(i), choh2g(i), chohdg(i), chopht(i), chorc(i), chophs(i), i=0,npo)
   READ (26) (choeqg(i), chochi(i), chotur(i), reftot(i), chotot(i), i=0,npo)

   READ (26) (timh21(i), timh22(i), timh23(i), timh24(i), timh25(i), i=0,npo)
   IF (i_hd /= 0) THEN
      READ (26) (timhd1(i), timhd2(i), timhd3(i), timhd4(i), i=0,npo)
   ENDIF

   READ (26) (fluph(i), int_rad(i), i=0,npo)

   READ (26) ((abnua(j,i), j=0,n_var), i=0,npo)
   ALLOCATE(codesp(0:nspec,0:npo))
   READ (26) ((codesp(j,i), j=0,nspec), i=0,npo)

   READ (26) Avmax, nh_init, tg_init, d_sour, fmrc, presse
   READ (26) vturb, F_dustem
   READ (26) neqt, F_ISRF, radm, radp, radm_ini, radp_ini
   READ (26) isoneside
   READ (26) modele, chimie, fprofil, srcpp, los_ext
   READ (26) ifafm, ifeqth, ifisob, itrfer, lfgkh2, ichh2
   READ (26) iforh2, ifaf

   READ (26) ((rxigr(i,j), i=0,nspec), j=0,npo)
   READ (26) (t_evap(i), i=0,nspec)
   READ (26) (t_diff(i), i=0,nspec)
   READ (26) T2_stick_ER, ex_stick_ER
   ALLOCATE (xmatk(n_var))
   READ (26) (xmatk(i)%nbt, i=1,n_var)
   DO i = 1, n_var
      ALLOCATE (xmatk(i)%fdt(1:xmatk(i)%nbt))
   ENDDO
   READ (26) ((xmatk(i)%fdt(k), k=1,xmatk(i)%nbt), i=1,n_var)

   READ (26) ((react(j,i), i=0,7), j=1,neqt)
   READ (26) (gamm(i), i=1,neqt)
   READ (26) (alpha(i), i=1,neqt)
   READ (26) (bet(i), i=1,neqt)
   READ (26) (de(i), i=1,neqt)
   READ (26) (itype(i), i=1,neqt)
   READ (26) nh2for, ncrion, npsion, nasrad, nrest
   READ (26) nh2endo, nphot_f, nphot_i, n3body, ncpart, ngsurf, ngphot, i_mandat
   READ (26) ngads, ngneut, nggdes, ncrdes, nphdes, ngevap, ngchs, ngelri
   READ (26) nsp_ne, nsp_ip, nsp_im, nsp_cs, nsp_gm, nsp_g1
   ALLOCATE (pdiesp(nphot_i,0:npo), ndiesp(nphot_i))
   READ (26) ((pdiesp(k,i), k=1,nphot_i), i=0,npo)
   READ (26) (ndiesp(k), k=1,nphot_i)

   READ (26) g_ratio, rhogr, alpgr, rgrmin, rgrmax, zeta
   f3 = (rgrmax**(3.0_dp-alpgr)  - rgrmin**(3.0_dp-alpgr))  / (3.0_dp-alpgr)
   READ (26) s_gr_t
   READ (26) xngr, agrnrm, signgr
   READ (26) vmaxw, fphsec, sngclb, rdeso
   READ (26) dsite
   READ (26) istic
   READ (26) iadhgr, ifh2gr, iaddgr, ifhdgr

   ! JLB - 15 X 2010 - Compute mean grain charge
   grcharge = 0.0_dp
   DO j = 0, npo
      DO mm = 1, npg
         grcharge(j) = grcharge(j) + zmoy(mm,j) * wwmg(mm)
      ENDDO
      grcharge(j) = grcharge(j) * agrnrm * densh(j)
   ENDDO

   READ (26) (mol(i), i=0,n_var+5)
   READ (26) ((ael(i,j), i=0,n_var+5), j=1,natom)
   READ (26) (enth(i), i=0,n_var+5)
   READ (26) (abin(i), i=0,n_var+5)
   READ (26) (l_atom(i), i=1,natom-1)

   ! Read relative densities of B and C bands of H2
   READ (26) (((h2bnua(iv,ij,iopt), iv=0,nvbc), ij=0,njbc), iopt=0,npo)
   READ (26) (((h2cnua(iv,ij,iopt), iv=0,nvbc), ij=0,njbc), iopt=0,npo)
   ! Fitzpatrick and Massa extinction coefficients
   READ (26) cc1, cc2, cc3, cc4, xgam, y0

   ! Informations on H2 et HD
   READ (26) eform_h2, emoyh2
   READ (26) eform_hd, emoyhd

   ! Radiative energy density
   READ (26) wl_6eV, wl_hab
   READ (26) int_rad0, flu_rad0

   ! Radiation on one side of the cloud only
   IF (isoneside == 1) THEN
      READ (26) int_rad1, flu_rad1
   ELSE
      READ (26) int_rad1, flu_rad1
      READ (26) int_rad2, flu_rad2
   ENDIF

   READ (26) G0m, G0p

   READ (26) nwlg
   ALLOCATE (rf_wl%Val(0:nwlg))
   READ (26) rf_wl%Val(0:nwlg)
   ALLOCATE (du_prop%abso(0:nwlg,0:npo))
   ALLOCATE (du_prop%scat(0:nwlg,0:npo))
   ALLOCATE (du_prop%emis(0:nwlg,0:npo))
   ALLOCATE (du_prop%gcos(0:nwlg,0:npo))
   ALLOCATE (du_prop%albe(0:nwlg,0:npo))
   READ (26) (du_prop%abso(:,iopt), iopt=0,npo)
   READ (26) (du_prop%scat(:,iopt), iopt=0,npo)
   READ (26) (du_prop%emis(:,iopt), iopt=0,npo)
   READ (26) (du_prop%gcos(:,iopt), iopt=0,npo)
   READ (26) (du_prop%albe(:,iopt), iopt=0,npo)

   ! H2 molecular fraction
   ALLOCATE (molfrac_ab(0:npo))
   READ (26) (molfrac_ab(iopt),iopt=0,npo)
   ALLOCATE (molfrac_cd(0:npo))
   READ (26) (molfrac_cd(iopt),iopt=0,npo)
   ! H2 Excitation temperature
   ALLOCATE (T01_ab(0:npo))
   READ (26) (T01_ab(iopt),iopt=0,npo)
   ALLOCATE (T01_cd(0:npo))
   READ (26) (T01_cd(iopt),iopt=0,npo)

   ! Ionization degree, gas pressure, total density, distance
   ALLOCATE (ionidegree(0:npo))
   READ (26) (ionidegree(iopt),iopt=0,npo)
   ALLOCATE (gaspressure(0:npo))
   READ (26) (gaspressure(iopt),iopt=0,npo)
   ALLOCATE (totdensity(0:npo))
   READ (26) (totdensity(iopt),iopt=0,npo)
   ALLOCATE (d_cm(0:npo))
   ALLOCATE (dd_cm(0:npo))
   READ (26) (d_cm(iopt),iopt=0,npo)
   READ (26) (dd_cm(iopt),iopt=0,npo)
   ALLOCATE (visualext(0:npo))
   READ (26) (visualext(iopt),iopt=0,npo)
   ALLOCATE (protoncd(0:npo))
   READ (26) (protoncd(iopt),iopt=0,npo)

   CLOSE (26)

   PRINT *, 'Data are now in memory'
   !PRINT *, '    Number of grid points:     ', npo
   !PRINT *, '    Number of chemical species:',nspec

   PRINT *, "Excitation for :"
   IF (i_c /= 0) THEN
      IF (in_sp(i_c) /= 0) THEN
         PRINT *, "C    - Number of levels : ", spec_lin(in_sp(i_c))%use
      ENDIF
   ENDIF
   IF (i_n /= 0) THEN
      IF (in_sp(i_n) /= 0) THEN
         PRINT *, "N    - Number of levels : ", spec_lin(in_sp(i_n))%use
      ENDIF
   ENDIF
   IF (i_o /= 0) THEN
      IF (in_sp(i_o) /= 0) THEN
         PRINT *, "O    - Number of levels : ", spec_lin(in_sp(i_o))%use
      ENDIF
   ENDIF
   IF (i_s /= 0) THEN
      IF (in_sp(i_s) /= 0) THEN
         PRINT *, "S    - Number of levels : ", spec_lin(in_sp(i_s))%use
      ENDIF
   ENDIF
   IF (i_si /= 0) THEN
      IF (in_sp(i_sip) /= 0) THEN
         PRINT *, "Si   - Number of levels : ", spec_lin(in_sp(i_si))%use
      ENDIF
   ENDIF
   IF (i_cp /= 0) THEN
      IF (in_sp(i_cp) /= 0) THEN
         PRINT *, "C+   - Number of levels : ", spec_lin(in_sp(i_cp))%use
      ENDIF
   ENDIF
   IF (i_np /= 0) THEN
      IF (in_sp(i_np) /= 0) THEN
         PRINT *, "N+   - Number of levels : ", spec_lin(in_sp(i_np))%use
      ENDIF
   ENDIF
   IF (i_op /= 0) THEN
      IF (in_sp(i_op) /= 0) THEN
         PRINT *, "O+   - Number of levels : ", spec_lin(in_sp(i_op))%use
      ENDIF
   ENDIF
   IF (i_sp /= 0) THEN
      IF (in_sp(i_sp) /= 0) THEN
         PRINT *, "S+   - Number of levels : ", spec_lin(in_sp(i_sp))%use
      ENDIF
   ENDIF
   IF (i_sip /= 0) THEN
      IF (in_sp(i_sip) /= 0) THEN
         PRINT *, "Si+  - Number of levels : ", spec_lin(in_sp(i_sip))%use
      ENDIF
   ENDIF
   IF (i_co /= 0) THEN
      IF (in_sp(i_co) /= 0) THEN
         PRINT *, "CO   - Number of levels : ", spec_lin(in_sp(i_co))%use
      ENDIF
   ENDIF
   IF (j_c13o /= 0) THEN
      IF (in_sp(j_c13o) /= 0) THEN
         PRINT *, "13CO  - Number of levels : ", spec_lin(in_sp(j_c13o))%use
      ENDIF
   ENDIF
   IF (j_co18 /= 0) THEN
      IF (in_sp(j_co18) /= 0) THEN
         PRINT *, "C18O  - Number of levels : ", spec_lin(in_sp(j_co18))%use
      ENDIF
   ENDIF
   IF (j_c13o18 /= 0) THEN
      IF (in_sp(j_c13o18) /= 0) THEN
         PRINT *, "13C18O  - Number of levels : ", spec_lin(in_sp(j_c13o18))%use
      ENDIF
   ENDIF
   IF (i_hcop /= 0) THEN
      IF (in_sp(i_hcop) /= 0) THEN
         PRINT *, "HCO+ - Number of levels : ", spec_lin(in_sp(i_hcop))%use
      ENDIF
   ENDIF
   IF (i_chp /= 0) THEN
      IF (in_sp(i_chp) /= 0) THEN
         PRINT *, "CH+ - Number of levels : ", spec_lin(in_sp(i_chp))%use
      ENDIF
   ENDIF
   IF (i_hcn /= 0) THEN
      IF (in_sp(i_hcn) /= 0) THEN
         PRINT *, "HCN  - Number of levels : ", spec_lin(in_sp(i_hcn))%use
      ENDIF
   ENDIF
   IF (i_cs /= 0) THEN
      IF (in_sp(i_cs) /= 0) THEN
         PRINT *, "CS   - Number of levels : ", spec_lin(in_sp(i_cs))%use
      ENDIF
   ENDIF
   IF (i_oh /= 0) THEN
      IF (in_sp(i_oh) /= 0) THEN
         PRINT *, "OH   - Number of levels : ", spec_lin(in_sp(i_oh))%use
      ENDIF
   ENDIF
   IF (i_h2o /= 0) THEN
      IF (in_sp(i_h2o) /= 0) THEN
         PRINT *, "H2O  - Number of levels : ", spec_lin(in_sp(i_h2o))%use
      ENDIF
   ENDIF
   IF (i_h3p /= 0) THEN
      IF (in_sp(i_h3p) /= 0) THEN
         PRINT *, "H3+  - Number of levels : ", spec_lin(in_sp(i_h3p))%use
      ENDIF
   ENDIF
   IF (i_o2 /= 0) THEN
      IF (in_sp(i_o2) /= 0) THEN
         PRINT *, "O2   - Number of levels : ", spec_lin(in_sp(i_o2))%use
      ENDIF
   ENDIF
   IF (i_h2 /= 0) THEN
      IF (in_sp(i_h2) /= 0) THEN
         PRINT *, "H2   - Number of levels : ", spec_lin(in_sp(i_h2))%use
      ENDIF
   ENDIF
   IF (i_hd /= 0) THEN
      IF (in_sp(i_hd) /= 0) THEN
         PRINT *, "HD   - Number of levels : ", spec_lin(in_sp(i_hd))%use
      ENDIF
   ENDIF

END SUBROUTINE PLECTUR

!%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE READ_LIN (spelin)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_PROFIL
   USE PXDR_STR_DATA

   IMPLICIT NONE

   TYPE (SPECTRA), INTENT (OUT) :: spelin
   INTEGER                      :: i, n

   INTEGER                      :: nlv, ntr, nqn

   !--- Species
   READ (26) spelin%nam
   READ (26) spelin%mom
   READ (26) spelin%ind

   !--- Properties
   READ (26) spelin%nlv
   READ (26) spelin%use
   READ (26) spelin%nqn
   READ (26) spelin%ntr

   nlv  = spelin%nlv
   ntr  = spelin%ntr
   nqn  = spelin%nqn

   !--- Allocation of memory
   ! Taken from READ_DATALEVEL
   ALLOCATE(spelin%gst(spelin%nlv))
   ALLOCATE(spelin%elk(spelin%nlv))
   ALLOCATE(spelin%qnam(spelin%nqn))
   ALLOCATE(spelin%quant(spelin%nlv,spelin%nqn))
   ! Taken from READ_DATALINES
   ALLOCATE(spelin%iup(spelin%ntr))
   ALLOCATE(spelin%jlo(spelin%ntr))
   ALLOCATE(spelin%wlk(spelin%ntr))
   ALLOCATE(spelin%aij(spelin%ntr))
   ALLOCATE(spelin%inftr(spelin%ntr))

   CALL ALLOC_SPECTRA(spelin, nlv, ntr)

   !--- Levels properties
   READ (26) (spelin%qnam(n), n=1,nqn)
   DO i = 1, spelin%use
      READ (26) (spelin%quant(i,n), n=1,nqn)
   ENDDO

   READ (26) (spelin%gst(i), i=1, spelin%use)
   READ (26) (spelin%elk(i), i=1, spelin%use)

   !--- Lines properties
   DO i = 1, spelin%ntr
      READ (26) spelin%inftr(i)
   ENDDO
   DO i = 1, spelin%ntr
      READ (26) spelin%iup(i)
   ENDDO
   DO i = 1, spelin%ntr
      READ (26) spelin%jlo(i)
   ENDDO
   DO i = 1, spelin%ntr
      READ (26) spelin%lac(i)
   ENDDO
   DO i = 1, spelin%ntr
      READ (26) spelin%wlk(i)
   ENDDO
   DO i = 1, spelin%ntr
      READ (26) spelin%aij(i)
   ENDDO

   !--- Emission
   DO i = 1, spelin%ntr
      READ (26) (spelin%emi(i,n), n=0,npo)
   ENDDO

   !--- Abundances
   READ (26) (spelin%abu(n), n=0,npo)
   DO i = 1, spelin%use
      READ (26) (spelin%xre(i,n), n=0,npo)
   ENDDO

   READ (26) (spelin%ref(n), n=0,npo)
   READ (26) (spelin%vel(n), n=0,npo)

   !--- Others
   DO i = 1, spelin%ntr
      READ (26) spelin%hot(i)
      READ (26) (spelin%siD(i,n), n=0,npo)
      READ (26) (spelin%odl(i,n), n=0,npo)
      READ (26) (spelin%odr(i,n), n=0,npo)
   ENDDO
   DO i = 1, spelin%use
      READ (26) (spelin%XXl(i,n), n=0,npo)
      READ (26) (spelin%XXr(i,n), n=0,npo)
      READ (26) (spelin%CoD(i,n), n=0,npo)
   ENDDO
   DO i = 1, spelin%ntr
      READ (26) spelin%exl(i)
      READ (26) spelin%exr(i)
      READ (26) (spelin%dnu(i,n), n=0,npo)
      READ (26) (spelin%pum(i,n), n=0,npo)
      READ (26) (spelin%jiD(i,n), n=0,npo)
      READ (26) (spelin%bel(i,n), n=0,npo)
      READ (26) (spelin%ber(i,n), n=0,npo)
   ENDDO

END SUBROUTINE READ_LIN

!=============================================================

END MODULE PREP_LECTUR
