!
!	PXDR_UTIL_XML.f90
!	PDR_1.4_fits_Xcode
!
!	Created by Franck on 06/12/08.
!	Copyright 2008 LUTH - Observatoire de Paris. All rights reserved.
!

MODULE PXDR_UTIL_XML
USE PXDR_CONSTANTES
USE PXDR_AUXILIAR

!======================================================================================================
! VO-TABLE RELATED QUANTITIES
!======================================================================================================

INTEGER, PARAMETER, PUBLIC :: nattmax = 20  ! Max number of attibuts

!--- GENERAL QUANTITIES ASSOCIATED TO LINES OF THE VO-TABLE
TYPE LINE_TEMPLATE !----------------------------------------------------------------------------------
   CHARACTER(len=lenstring)      :: ligne                                                            !
   INTEGER                       :: ttype     ! 1: RESSOURCE, 2: TABLE; 3: PARAM, 4: FIELD, ...      !
   INTEGER                       :: decal     ! decalage pour tabulations                            !
END TYPE LINE_TEMPLATE !------------------------------------------------------------------------------

TYPE RESOURCE !---------------------------------------------------------------------------------------
   CHARACTER(len=lenname)        :: name                                                             !
   CHARACTER(len=lenID)          :: ID                                                               !
END TYPE RESOURCE !-----------------------------------------------------------------------------------

TYPE TABLE !------------------------------------------------------------------------------------------
   CHARACTER(len=lenname)        :: name                                                             !
   CHARACTER(len=lenID)          :: ID                                                               !
END TYPE TABLE !--------------------------------------------------------------------------------------

TYPE PARAM !------------------------------------------------------------------------------------------
!  <PARAM name="name"  value="value" />                                                              !
   CHARACTER(len=lenname)        :: name                                                             !
   CHARACTER(len=lenID)          :: value                                                            !
END TYPE PARAM !--------------------------------------------------------------------------------------

TYPE FIELD !-------------------------------------------------------------------------------------------------
!  <FIELD name="nameFD" ID="IDFD" datatype="datatypD" unit="unitFD" ucd="ucdFD" utype="utypeFD" />          !
   CHARACTER(len=lenname)     :: nameFD                                                                     !
   CHARACTER(len=lenID)       :: IDFD       ! Must be the same the in the .fits file                        !
   CHARACTER(len=lendatatype) :: datatypFD                                                                  !
   CHARACTER(len=lenunit)     :: unitFD                                                                     !
   CHARACTER(len=lenucd)      :: ucdFD                                                                      !
   CHARACTER(len=lenutype)    :: utypeFD                                                                    !
   INTEGER                    :: flag_desc  ! 1 if a description is associated to the FIELD                 !
   CHARACTER(len=lendescrip)  :: descripFD                                                                  !
END TYPE FIELD !---------------------------------------------------------------------------------------------

TYPE VOT_DATA !----------------------------------------------------------------------------------------------
   ! <DATA><FITS extnum="VOT_DATA%extnum">                                                                  !
   INTEGER                    :: extnum ! Number of the DU. We do not use it for simulation                 !
END TYPE VOT_DATA !------------------------------------------------------------------------------------------


TYPE VOT_STREAM !--------------------------------------------------------------------------------------------
   CHARACTER(len=lenencoding) :: encoding ! Encoding, Example : gzip, base64, dynamic or "" for nothing     !
   CHARACTER(len=lenpath)     :: href     ! Reference. Example : href="file///usr/home/toto/toto.fits"      !
END TYPE VOT_STREAM !----------------------------------------------------------------------------------------


TYPE DUXML !-------------------------------------------------------------------------------------------------
! TYPE DESCRIBING A DATA-UNIT                                                                               !
   CHARACTER(len=lenname)     :: nameDU      ! Name of the DU           -> TABLE line : name                !
   CHARACTER(len=lenID)       :: IDDU        ! ID of the DU             -> ID of the D.U. = extname of fits !
   CHARACTER(len=lenID)       :: CNTIDDU     ! ID of the container      -> TABLE line : ID                  !
   CHARACTER(len=lenmode)     :: modeDU      ! Mode                     -> PARAM line : value               !
   INTEGER                    :: flag_desc   ! 1 if a description is associated to the TABLE                !
   CHARACTER(len=lendescrip)  :: descripDU   ! Description of the table                                     !
   INTEGER                    :: nrows       ! nber of lines in the table                                   !
   INTEGER                    :: ncol        ! nber of columns in the table                                 !
   INTEGER                    :: flag_stream ! 1 if a STREAM towards a FITS is associated to the TABLE      !
   INTEGER                    :: extnum      ! extnum of the DU containing data see TYPE VOT_DATA           !
   CHARACTER(len=lenencoding) :: encoding    ! encoding for data see TYPE VOT_STREAM                        !
   CHARACTER(len=lenpath)     :: href        ! Reference. See TYPE VOT_STREAM                               !
END TYPE DUXML !---------------------------------------------------------------------------------------------

TYPE AXE !---------------------------------------------------------------------------------------------------
! TYPE DESCRIBING AN AXE                                                                                    !
   CHARACTER(len=lenname)     :: name  ! Nom de l'axe sous forme "human readable"                           !
   CHARACTER(len=lenID)       :: ID    ! ID de l'axe                                                        !
   CHARACTER(len=lenID)       :: IDDU  ! ID du DU donnant la quantite physique caracterisant l'axe          !
   CHARACTER(len=lenID)       :: IDFD  ! ID du FIELD donnant la quantite physique caracterisant l'axe       !
END TYPE AXE !-----------------------------------------------------------------------------------------------

TYPE DUAXE !-------------------------------------------------------------------------------------------------
! TYPE DESCRIBING AN AXE DATA-UNIT                                                                          !
   CHARACTER(len=lenname)    :: name   ! Nom de l'axe                                                       !
   CHARACTER(len=lenID)      :: ID     ! ID de l'axe associe a la quantite                                  !
END TYPE DUAXE !---------------------------------------------------------------------------------------------


!*******************************************************************************************************
!                                            CONTAINS
!*******************************************************************************************************
CONTAINS

!!!=====================================================================================================
!!! SUBROUTINE INIT_xmlline
!!!=====================================================================================================
!!! Initialisation of a XML line.
!!! This SUBROUTINE creates a template line
!!!=====================================================================================================
SUBROUTINE INIT_xmlline(xmlline)

USE PXDR_CONSTANTES
IMPLICIT NONE
TYPE(LINE_TEMPLATE), INTENT(INOUT) :: xmlline

CHARACTER(len=lenname) :: string
INTEGER                :: poswrit
INTEGER                :: lenstr
INTEGER                :: i

     DO i = 1, lenstring
        xmlline%ligne(i:i) = " "
     ENDDO

     SELECT CASE (xmlline%ttype)

     CASE (1) ! TYPE RESSOURCE --------------------------------------------------
        poswrit = xmlline%decal + 2 ! decal = 0, on ecrit dans la case 2 (il faut un espace)

        string = '<RESOURCE name='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenname) = " "

        poswrit = poswrit + lenname + 1
        string = ' ID='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenID) = " "

        poswrit = poswrit + lenID + 1
        string = ' >'
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

     CASE (2) ! TYPE TABLE ------------------------------------------------------
        poswrit = xmlline%decal + 2 ! decal = 0, on ecrit dans la case 2 (il faut un espace)

        string = '<TABLE name='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenname) = " "

        poswrit = poswrit + lenname + 1
        string = ' ID='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenID) = " "

        poswrit = poswrit + lenID + 1
        string = ' nrows='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenstrint) = " "

        poswrit = poswrit + lenstrint + 1
        string = ' >'
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

     CASE (3) ! TYPE PARAM -----------------------------------------------------
        poswrit = xmlline%decal + 2 ! decal = 0, on ecrit dans la case 2 (il faut un espace)

        string = '<PARAM name='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenname) = " "

        poswrit = poswrit + lenname + 1
        string = ' value='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenID) = " "

        poswrit = poswrit + lenID + 1
        string = ' />'
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

     CASE (4) ! TYPE FIELD without description -----------------------------------
        !<FIELD name="" ID="" datatype="" unit="" ucd="" utype=""/>
        poswrit = xmlline%decal + 2 ! decal = 0, on ecrit dans la case 2 (il faut un espace en premier)

        string = '<FIELD name='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenname) = " "

        poswrit = poswrit + lenname + 1
        string = ' ID='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenID) = " "

        poswrit = poswrit + lenID + 1
        string = ' datatype='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lendatatype) = " "

        poswrit = poswrit + lendatatype + 1
        string = ' unit='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenunit) = " "

        poswrit = poswrit + lenunit + 1
        string = ' ucd='
        lenstr = LEN_TRIM(string)
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenucd) = " "

        poswrit = poswrit + lenucd + 1
        string = ' utype='
        lenstr = LEN_TRIM(string)
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenutype) = " "

        poswrit = poswrit + lenutype + 1
        string = ' />'
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

     CASE (5) ! TYPE FIELD with description -----------------------------------
        poswrit = xmlline%decal + 2 ! decal = 0, on ecrit dans la case 2 (il faut un espace en premier)

        string = '<FIELD name='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenname) = " "

        poswrit = poswrit + lenname + 1
        string = ' ID='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenID) = " "

        poswrit = poswrit + lenID + 1
        string = ' datatype='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lendatatype) = " "

        poswrit = poswrit + lendatatype + 1
        string = ' unit='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenunit) = " "

        poswrit = poswrit + lenunit + 1
        string = ' ucd='
        lenstr = LEN_TRIM(string)
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenucd) = " "

        poswrit = poswrit + lenucd + 1
        string = ' utype='
        lenstr = LEN_TRIM(string)
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenutype) = " "

        poswrit = poswrit + lenutype + 1
        string = ' >' ! Not /> this time
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

     CASE (6) ! <FITS extnum="value">

        poswrit = xmlline%decal + 2 ! decal = 0, on ecrit dans la case 2 (il faut un espace en premier)

        string = '<FITS extnum='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenextnum) = " "

        poswrit = poswrit + lenextnum + 1
        string = '>'
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

     CASE (7) ! <STREAM encoding="value" href="value" />

        poswrit = xmlline%decal + 2 ! decal = 0, on ecrit dans la case 2 (il faut un espace en premier)
        string = '<STREAM encoding='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenencoding) = " "

        poswrit = poswrit + lenencoding + 1
        string = ' href='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenpath) = " "

        poswrit = poswrit + lenpath + 1
        string = ' />'
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

     CASE (8) ! <DESCRIPTION> ...... </DESCRIPTION>

        poswrit = xmlline%decal + 2
        string = '<DESCRIPTION> '
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

        xmlline%ligne(lenstring-13:lenstring) = "</DESCRIPTION>"

     CASE (9) ! TYPE TABLE END : <TABLE ............/> ----------------------------------
        poswrit = xmlline%decal + 2 ! decal = 0, on ecrit dans la case 2 (il faut un espace)

        string = '<TABLE name='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenname) = " "

        poswrit = poswrit + lenname + 1
        string = ' ID='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenID) = " "

        poswrit = poswrit + lenID + 1
        string = ' />'
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

     CASE (10) ! TYPE GROUP <GROUP name="     " ucd="     ">
        poswrit = xmlline%decal + 2

        string = '<GROUP name='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenname) = " "

        poswrit = poswrit + lenname + 1
        string = ' ucd='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenucd) = " "

        poswrit = poswrit + lenucd + 1
        string = ' >'
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

     CASE (11) ! LIGNE FIELDref <FIELDref ref="ID_FIELD">
        poswrit = xmlline%decal+2

        string  = '<FIELDref ref='
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

        poswrit = poswrit + lenstr + 1
        xmlline%ligne(poswrit:poswrit+lenID) = " "

        poswrit = poswrit + lenID + 1
        string = ' />'
        lenstr = LEN_TRIM(ADJUSTL(string))
        xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

     END SELECT


END SUBROUTINE INIT_xmlline

!============================================================================================================
! WRITLINEXML
!============================================================================================================
SUBROUTINE WRITE_LINEXML(unit,line)
IMPLICIT NONE

INTEGER, INTENT(IN)                  :: unit
CHARACTER(len=lenstring), INTENT(IN) :: line

INTEGER :: i

   DO i = 1, LEN_TRIM(line)
      WRITE(unit,"(A1)",ADVANCE="NO") line(i:i)
   ENDDO
   WRITE(unit,*)


END SUBROUTINE WRITE_LINEXML

!===========================================================================================================
! ECRITURE D'UNE LIGNE TYPE : name="name" ID="ID"
!===========================================================================================================
SUBROUTINE WRITE_nameID(unit,xmlline,name,ID)

   IMPLICIT NONE
   INTEGER,                  INTENT(IN)    :: unit
   TYPE(LINE_TEMPLATE),      INTENT(INOUT) :: xmlline
   CHARACTER(len=lenname),   INTENT(IN)    :: name
   CHARACTER(len=lenID),     INTENT(IN)    :: ID

   INTEGER :: i
   INTEGER :: poswrit

   !--- Search for first character :=
   i = 1
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lenname) = name(1:lenname)

   !--- Search for third character :=
   i = poswrit + lenname + 2
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lenID) = ID(1:lenID)

   CALL WRITE_LINEXML(unit,xmlline%ligne)

END SUBROUTINE WRITE_nameID

!===========================================================================================================
! ECRITURE D'UNE LIGNE TYPE : name="name" ID="ID" nrows="nrows"
!===========================================================================================================
SUBROUTINE WRITE_TABLE(unit,xmlline,name,ID,snrows)

   IMPLICIT NONE
   INTEGER,                  INTENT(IN)    :: unit
   TYPE(LINE_TEMPLATE),      INTENT(INOUT) :: xmlline
   CHARACTER(len=lenname),   INTENT(IN)    :: name
   CHARACTER(len=lenID),     INTENT(IN)    :: ID
   CHARACTER(len=lenstrint), INTENT(IN)    :: snrows

   INTEGER :: i
   INTEGER :: poswrit

   !--- Search for first character :=
   i = 1
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lenname) = name(1:lenname)

   !--- Search for second character :=
   i = poswrit + lenname + 2
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lenID) = ID(1:lenID)

   !--- Search for third character :=
   i = poswrit + lenID + 2
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lenstrint) = snrows(1:lenstrint)



   CALL WRITE_LINEXML(unit,xmlline%ligne)

END SUBROUTINE WRITE_TABLE


!===========================================================================================================
! ECRITURE D'UNE LIGNE TYPE : name="name" ucd="ucd"
!===========================================================================================================
SUBROUTINE WRITE_nameucd(unit,xmlline,name,ucd)

   IMPLICIT NONE
   INTEGER,                  INTENT(IN)    :: unit
   TYPE(LINE_TEMPLATE),      INTENT(INOUT) :: xmlline
   CHARACTER(len=lenname),   INTENT(IN)    :: name
   CHARACTER(len=lenucd),    INTENT(IN)    :: ucd

   INTEGER :: i
   INTEGER :: poswrit

   !--- Search for first character :=
   i = 1
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lenname) = name(1:lenname)

   !--- Search for third character :=
   i = poswrit + lenname + 2
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lenucd) = ucd(1:lenucd)

   CALL WRITE_LINEXML(unit,xmlline%ligne)

END SUBROUTINE WRITE_nameucd


!========================================================================================================
! ECRITURE D'UNE LIGNE TYPE FIELD
!========================================================================================================
SUBROUTINE WRITE_field(unit,xmlline,name,ID,datatype,unite,ucd,utype)

IMPLICIT NONE
INTEGER,                    INTENT(IN)    :: unit
TYPE(LINE_TEMPLATE),        INTENT(INOUT) :: xmlline
CHARACTER(len=lenname),     INTENT(IN)    :: name
CHARACTER(len=lenID),       INTENT(IN)    :: ID
CHARACTER(len=lendatatype), INTENT(IN)    :: datatype
CHARACTER(len=lenunit),     INTENT(IN)    :: unite
CHARACTER(len=lenucd),      INTENT(IN)    :: ucd
CHARACTER(len=lenutype),    INTENT(IN)    :: utype

INTEGER :: i
INTEGER :: poswrit

   !--- Search for first character :=
   i = 1
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lenname) = name(1:lenname)

   !--- Search for second character :=
   i = poswrit + lenname + 2
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lenID) = ID(1:lenID)

   !--- Search for second character :=
   i = poswrit + lenID + 2
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lendatatype) = datatype(1:lendatatype)

   i = poswrit + lendatatype + 2
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lenunit) = unite(1:lenunit)

   i = poswrit + lenunit + 2
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lenucd) = ucd(1:lenucd)

   i = poswrit + lenucd + 2
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lenutype) = utype(1:lenutype)

   CALL WRITE_LINEXML(unit,xmlline%ligne)

END SUBROUTINE WRITE_field


!========================================================================================================
! ECRITURE D'UNE LIGNE TYPE DESCRIPTION
!========================================================================================================
SUBROUTINE WRITE_description(unit,xmlline,description)

IMPLICIT NONE
INTEGER,                    INTENT(IN)    :: unit
TYPE(LINE_TEMPLATE),        INTENT(INOUT) :: xmlline
CHARACTER(len=lendescrip),  INTENT(IN)    :: description

INTEGER :: i
INTEGER :: poswrit

   !--- Search for first character :">"
   i = 1
   DO WHILE (xmlline%ligne(i:i) .NE. '>')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lendescrip) = description(1:lendescrip)

   CALL WRITE_LINEXML(unit,xmlline%ligne)

END SUBROUTINE WRITE_description

!=================================================================================
! SUBROUTINE ENDFIELD : ECRIT </FIELD>
!=================================================================================
SUBROUTINE ENDFIELD(unit,decal)
IMPLICIT NONE

   INTEGER, INTENT(IN)      :: unit
   INTEGER, INTENT(IN)      :: decal

   CHARACTER(len=lenstring) :: string
   CHARACTER(len=lenstring) :: ligne
   INTEGER                  :: poswrit
   INTEGER                  :: lenstr
   INTEGER                  :: i

   DO i = 1, lenstring
      string(i:i) = " "
      ligne(i:i)  = " "
   ENDDO
   string  = "</FIELD>"
   poswrit = decal + 1
   lenstr  = LEN_TRIM(ADJUSTL(string))
   ligne(poswrit:poswrit+lenstr) = string(1:lenstr)

   lenstr = LEN_TRIM(ligne)

   DO i = 1, lenstr
      WRITE(unit,"(A1)",ADVANCE="NO") ligne(i:i)
   ENDDO
   WRITE(unit,*)

END SUBROUTINE ENDFIELD

!=================================================================================
! SUBROUTINE ENDTABLE : ECRIT </TABLE>
!=================================================================================
SUBROUTINE ENDTABLE(unit,decal)
IMPLICIT NONE

   INTEGER, INTENT(IN)      :: unit
   INTEGER, INTENT(IN)      :: decal

   CHARACTER(len=lenstring) :: string
   CHARACTER(len=lenstring) :: ligne
   INTEGER                  :: poswrit
   INTEGER                  :: lenstr
   INTEGER                  :: i

   DO i = 1, lenstring
      string(i:i) = " "
      ligne(i:i)  = " "
   ENDDO
   string  = "</TABLE>"
   poswrit = decal + 1
   lenstr  = LEN_TRIM(ADJUSTL(string))
   ligne(poswrit:poswrit+lenstr) = string(1:lenstr)

   lenstr = LEN_TRIM(ligne)

   DO i = 1, lenstr
      WRITE(unit,"(A1)",ADVANCE="NO") ligne(i:i)
   ENDDO
   WRITE(unit,*)

END SUBROUTINE ENDTABLE


!=================================================================================
! SUBROUTINE ENDRESOURCE : ECRIT </RESOURCE>
!=================================================================================
SUBROUTINE ENDRESOURCE(unit,decal)
IMPLICIT NONE

   INTEGER, INTENT(IN)      :: unit
   INTEGER, INTENT(IN)      :: decal

   CHARACTER(len=lenstring) :: string
   CHARACTER(len=lenstring) :: ligne
   INTEGER                  :: poswrit
   INTEGER                  :: lenstr
   INTEGER                  :: i

   DO i = 1, lenstring
      string(i:i) = " "
      ligne(i:i)  = " "
   ENDDO
   string = "</RESOURCE>"
   poswrit = decal + 1
   lenstr = LEN_TRIM(ADJUSTL(string))
   ligne(poswrit:poswrit+lenstr) = string(1:lenstr)

   lenstr = LEN_TRIM(ligne)

   DO i = 1, lenstr
      WRITE(unit,"(A1)",ADVANCE="NO") ligne(i:i)
   ENDDO
   WRITE(unit,*)

END SUBROUTINE ENDRESOURCE

!=================================================================================
! SUBROUTINE ENDGROUP : ECRIT </GROUP>
!=================================================================================
SUBROUTINE ENDGROUP(unit,decal)
IMPLICIT NONE

   INTEGER, INTENT(IN)      :: unit
   INTEGER, INTENT(IN)      :: decal

   CHARACTER(len=lenstring) :: string
   CHARACTER(len=lenstring) :: ligne
   INTEGER                  :: poswrit
   INTEGER                  :: lenstr
   INTEGER                  :: i

   DO i = 1, lenstring
      string(i:i) = " "
      ligne(i:i)  = " "
   ENDDO
   string = "</GROUP>"
   poswrit = decal + 1
   lenstr = LEN_TRIM(ADJUSTL(string))
   ligne(poswrit:poswrit+lenstr) = string(1:lenstr)

   lenstr = LEN_TRIM(ligne)

   DO i = 1, lenstr
      WRITE(unit,"(A1)",ADVANCE="NO") ligne(i:i)
   ENDDO
   WRITE(unit,*)

END SUBROUTINE ENDGROUP


!*************************************************************************************
! List of subroutines to write the XML file
! - WRITE_DUXML      : write TABLE informations including PARAM and FIELD
! - WRITE_DUTITLE    : write the TABLE line in the xml file
! - WRITE_DUPARAMFD  : write the PARAM lines in the xml file
! - WRITE_DUAXE      ! write the AXE RESSOURCE
! - WRITE_DUFIELD    : write the FIELD lines in the xml file
!*************************************************************************************
!=====================================================================================
! SUBROUTINE WRITE_DUXML
!-------------------------------------------------------------------------------------
! Ecrit les information d'un DataUnit
! <TABLE .....
!  <PARAM ....
!  <FIELD ....
! </TABLE>
!=====================================================================================

SUBROUTINE WRITE_DUXML(unitxml, blocDUxml, ncol, naxe, descaxe, nodata)

!-------------------------------------------------------------------------------------
! DECLARATIONS
!-------------------------------------------------------------------------------------
    IMPLICIT NONE
    INTEGER,     INTENT(IN)            :: unitxml
    TYPE(DUXML), INTENT(IN)            :: blocDUxml
    INTEGER,     INTENT(IN)            :: ncol            ! Nombre de colonnes
    INTEGER,     INTENT(IN)            :: naxe            ! Nombre d'axes auxquelles les donnees sont associees
    TYPE(DUAXE), INTENT(IN)            :: descaxe(naxmax) ! Description des axes
    INTEGER,     INTENT(IN)            :: nodata          ! Flag if 1 <DATA> is not written
    ! use descol transmitted in module DESC_FITS

    CHARACTER(len=lenname)    :: name
    CHARACTER(len=lenID)      :: ID
    CHARACTER(len=lendescrip) :: description

    TYPE(FIELD)              :: FDxml
    INTEGER                  :: i

    !--- Ecriture du <TABLE ..........>
    CALL WRITE_DUTITLE(unitxml,blocDUxml)

    !--- Ecriture de DESCRIPTION de la TABLE(ordre mandatory)
    IF (blocDUxml%flag_desc .EQ. 1) THEN
       description = blocDUxml%descripDU
       CALL WRITE_DUDESCRIP(unitxml,description,9)
    ENDIF

    !--- Ecriture des PARAM de la TABLE
    CALL WRITE_DUPARAMFD(unitxml,blocDUxml)

    !--- Ecriture des PARAM pour les AXES
    DO i = 1, naxe
       name = '"IDAXE"'
       ID   = descaxe(i)%ID
       CALL WRITE_DUAXE(unitxml,name,ID)
    ENDDO

    !--- Ecriture des FIELDS -----------------------
    DO i = 1, ncol
       FDxml%nameFD     = descol(i)%titre
       FDxml%IDFD       = descol(i)%IDFD
       FDxml%datatypFD  = descol(i)%datatyp
       FDxml%unitFD     = descol(i)%unite
       FDxml%ucdFD      = descol(i)%ucd
       FDxml%utypeFD    = descol(i)%utype
       FDxml%flag_desc  = descol(i)%flag_desc
       FDxml%descripFD  = descol(i)%descrip
       IF (FDxml%flag_desc .EQ. 0) THEN      ! Si le FIELD n'a pas de description
          CALL WRITE_DUFIELD(unitxml,FDxml,blocDUxml) ! Ecrit <FIELD ... />
       ELSE IF (FDxml%flag_desc .EQ. 1) THEN ! Si le FIELD a une DESCRIPTION
          CALL WRITE_DUFIELD_DESC(unitxml,FDxml,blocDUxml) ! Ecrit <FIELD .... >
          description = FDxml%descripFD
          CALL WRITE_DUDESCRIP(unitxml,description,12)     ! Ecrit <DESCRIPTION .... />
          CALL ENDFIELD(unitxml,10)                        ! Ecrit </FIELD>
       ELSE
          PRINT *,"PROBLEM : FDxml%flag_desc not recognized :", FDxml%flag_desc
          STOP
       ENDIF
    ENDDO

    !--- Ecriture des DATA => Chemin vers le .fits
    IF (nodata .NE. 1) THEN
    CALL WRITE_VOT_DATA(unitxml,blocDUxml%extnum,blocDUxml%encoding,blocDUxml%href)
    ENDIF

    WRITE(unitxml,"(A11)") '   </TABLE>'
END SUBROUTINE WRITE_DUXML

!======================================================================================
! SUBROUTINE WRITE_DUTITLE
!======================================================================================
SUBROUTINE WRITE_DUTITLE(unitxml, blocDUxml)

   IMPLICIT NONE
   INTEGER, INTENT(IN)     :: unitxml
   TYPE(DUXML), INTENT(IN) :: blocDUxml

   CHARACTER(len=lenname)   :: name
   CHARACTER(len=lenID)     :: ID
   CHARACTER(len=lenstrint) :: nrows
   TYPE(LINE_TEMPLATE)      :: xmlline

   !--- ECRITURE DE <TABLE name="   "  ID="   " nrows=" "> ---------------------------
   xmlline%ttype = 2 ! TABLE
   xmlline%decal = 6 ! Decalage
   name(:)  = " "
   ID(:)    = " "
   nrows(:) = " "
   name     = '"'//TRIM(blocDUxml%nameDU)//'"'
   ID       = '"'//TRIM(blocDUxml%IDDU)//'"'
   CALL INT_STR(blocDUxml%nrows,nrows)
   nrows    = '"'//TRIM(ADJUSTL(nrows))//'"'
   CALL INIT_xmlline(xmlline)
   CALL WRITE_TABLE(unitxml,xmlline,name,ID,nrows)

END SUBROUTINE WRITE_DUTITLE

!===============================================================================
! SUBROUTINE WRITE_DUPARAMFD : ECRIT LES PARAM DES FIELDS
!===============================================================================
SUBROUTINE WRITE_DUPARAMFD(unitxml, blocDUxml)

   IMPLICIT NONE
   INTEGER,     INTENT(IN)  :: unitxml
   TYPE(DUXML), INTENT(IN)  :: blocDUxml

   CHARACTER(len=lenname)   :: name
   CHARACTER(len=lenID)     :: ID
   TYPE(LINE_TEMPLATE)      :: xmlline


   !--- ECRITURE DE <PARAM name="mode" value="   " /> ---------------------------
   xmlline%ttype = 3 ! PARAM
   xmlline%decal = 9 ! Decalage
   name(:) = " "
   ID(:)   = " "
   name    = '"mode"'
   ID      = '"'//TRIM(blocDUxml%modeDU)//'"'
   CALL INIT_xmlline(xmlline)
   CALL WRITE_nameID(unitxml,xmlline,name,ID)

   !--- ECRITURE DE <PARAM name="container" value="   " /> ---------------------------
   IF (TRIM(blocDUxml%CNTIDDU) .NE. "") THEN
      xmlline%ttype = 3 ! PARAM
      xmlline%decal = 9 ! Decalage
      name(:) = " "
      ID(:)   = " "
      name    = '"container"'
      ID      = '"'//TRIM(blocDUxml%CNTIDDU)//'"'
      CALL INIT_xmlline(xmlline)
      CALL WRITE_nameID(unitxml,xmlline,name,ID)
   ENDIF

END SUBROUTINE WRITE_DUPARAMFD

!=======================================================================================
! SUBROUTINE WRITE_DUAXE
!=======================================================================================
SUBROUTINE WRITE_DUAXE(unitxml,name,ID)

   INTEGER,                INTENT(IN)    :: unitxml
   CHARACTER(len=lenname), INTENT(INOUT) :: name
   CHARACTER(len=lenID),   INTENT(INOUT) :: ID

   TYPE(LINE_TEMPLATE)                   :: xmlline

   xmlline%ttype = 3 ! type de ligne : PARAM
   xmlline%decal = 9
   name(:) = " "
   name    = '"axis"'
   CALL INIT_xmlline(xmlline)
   CALL WRITE_nameID(unitxml,xmlline,name,ID)


END SUBROUTINE WRITE_DUAXE

!=======================================================================================
! SUBROUTINE WRITE_DUFIELD
! WRITE a FIELD LINE WITHOUT DESCRIPTION
!=======================================================================================
SUBROUTINE WRITE_DUFIELD(unitxml,FDxml,blocDUxml)

   IMPLICIT NONE
   INTEGER,     INTENT(IN)    :: unitxml
   TYPE(FIELD), INTENT(IN)    :: FDxml
   TYPE(DUXML), INTENT(IN)    :: blocDUxml

   CHARACTER(len=lenname)     :: name
   CHARACTER(len=lenID)       :: ID
   CHARACTER(len=lendatatype) :: datatype
   CHARACTER(len=lenunit)     :: unite
   CHARACTER(len=lenucd)      :: ucd
   CHARACTER(len=lenutype)    :: utype

   TYPE(LINE_TEMPLATE)        :: xmlline

   xmlline%ttype = 4 ! FIELD without description
   xmlline%decal = 9
   CALL INIT_xmlline(xmlline)
   name(:)     = " "
   ID(:)       = " "
   datatype(:) = " "
   unite(:)    = " "
   ucd(:)      = " "
   utype(:)    = " "
   name     = '"'//TRIM(FDxml%nameFD)//'"'
   ID       = '"'//TRIM(blocDUxml%IDDU)//"."//TRIM(FDxml%IDFD)//'"'
   datatype = '"'//TRIM(FDxml%datatypFD)//'"'
   unite    = '"'//TRIM(FDxml%unitFD)//'"'
   ucd      = '"'//TRIM(FDxml%ucdFD)//'"'
   utype    = '"'//TRIM(FDxml%utypeFD)//'"'
   CALL WRITE_field(unitxml,xmlline,name,ID,datatype,unite,ucd,utype)

END SUBROUTINE WRITE_DUFIELD

!=======================================================================================
! SUBROUTINE WRITE_DUFIELD_DESC
! WRITE a FIELD LINE WITH DESCRIPTION
!=======================================================================================
SUBROUTINE WRITE_DUFIELD_DESC(unitxml,FDxml,blocDUxml)

   IMPLICIT NONE
   INTEGER,     INTENT(IN)    :: unitxml
   TYPE(FIELD), INTENT(IN)    :: FDxml
   TYPE(DUXML), INTENT(IN)    :: blocDUxml

   CHARACTER(len=lenname)     :: name
   CHARACTER(len=lenID)       :: ID
   CHARACTER(len=lendatatype) :: datatype
   CHARACTER(len=lenunit)     :: unite
   CHARACTER(len=lenucd)      :: ucd
   CHARACTER(len=lenutype)    :: utype
   TYPE(LINE_TEMPLATE)        :: xmlline

   xmlline%ttype = 5 ! FIELD with description
   xmlline%decal = 9
   CALL INIT_xmlline(xmlline)
   name(:)     = " "
   ID(:)       = " "
   datatype(:) = " "
   unite(:)    = " "
   ucd(:)      = " "
   utype(:)    = " "
   name        = '"'//TRIM(FDxml%nameFD)//'"'
   ID          = '"'//TRIM(blocDUxml%IDDU)//"."//TRIM(FDxml%IDFD)//'"'
   datatype    = '"'//TRIM(FDxml%datatypFD)//'"'
   unite       = '"'//TRIM(FDxml%unitFD)//'"'
   ucd         = '"'//TRIM(FDxml%ucdFD)//'"'
   utype       = '"'//TRIM(FDxml%utypeFD)//'"'
   CALL WRITE_field(unitxml,xmlline,name,ID,datatype,unite,ucd,utype)
   xmlline%ttype = 6 ! DESCRIPTION LINE
   xmlline%decal = 9
   CALL INIT_xmlline(xmlline)

END SUBROUTINE WRITE_DUFIELD_DESC

!=======================================================================================
! SUBROUTINE WRITE_DUDESCRIP
! WRITE A DESCRIPTION LINE
!=======================================================================================
SUBROUTINE WRITE_DUDESCRIP(unitxml,description,decal)

   IMPLICIT NONE
   INTEGER,                   INTENT(IN) :: unitxml
   CHARACTER(len=lendescrip), INTENT(IN) :: description
   INTEGER,                   INTENT(IN) :: decal

   INTEGER                    :: ncaract
   CHARACTER(len=lenstring)   :: string
   INTEGER                    :: lenstr
   INTEGER                    :: poswrit
   INTEGER                    :: i
   TYPE(LINE_TEMPLATE)        :: xmlline

   xmlline%ttype = 8 ! Initialise une ligne DESCRIPTION
   xmlline%decal = decal
   CALL INIT_xmlline(xmlline)

   ncaract = LEN_TRIM(ADJUSTL(description))

   poswrit = decal + 2 ! decal = 0, on ecrit dans la case 2 (il faut un espace en premier)

   string = '<DESCRIPTION> '
   lenstr = LEN_TRIM(ADJUSTL(string))
   xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(string)

   poswrit = poswrit + lenstr + 1
   xmlline%ligne(poswrit:poswrit+ncaract) = description(1:ncaract)

   poswrit = poswrit + ncaract + 1
   string = ' </DESCRIPTION>'
   lenstr = LEN_TRIM(ADJUSTL(string))
   xmlline%ligne(poswrit:poswrit+lenstr) = TRIM(ADJUSTL(string))

   DO i = 1, poswrit+lenstr
      WRITE(unitxml,"(A1)",ADVANCE="NO") xmlline%ligne(i:i)
   ENDDO
   WRITE(unitxml,*)

END SUBROUTINE WRITE_DUDESCRIP


!==================================================================================
! SUBROUTINE : WRITE_VOT_DATA
!              <DATA>
!                <FITS extnum=value>
!                  <STREAM encoding=value href=value />
!                </FITS>
!              </DATA>
!==================================================================================
SUBROUTINE WRITE_VOT_DATA(unitxml, extnum, encoding, href)

IMPLICIT NONE
INTEGER,                    INTENT(IN) :: unitxml
INTEGER,                    INTENT(IN) :: extnum
CHARACTER(len=lenencoding), INTENT(IN) :: encoding
CHARACTER(len=lenpath),     INTENT(IN) :: href

CHARACTER(len=lenstrint)               :: s_extnum

   TYPE(LINE_TEMPLATE)        :: xmlline
   CHARACTER(len=lenextnum)   :: s_extnum2
   CHARACTER(len=lenencoding) :: encoding2
   CHARACTER(len=lenpath)     :: href2
   INTEGER                    :: i
   INTEGER                    :: poswrit
   INTEGER                    :: lenstr

   !--------------------------------------------------------------------------
   ! WRITE LINE : <DATA>
   !--------------------------------------------------------------------------
   WRITE(unitxml,"(A16)") "          <DATA>"

   !--------------------------------------------------------------------------
   ! WRITE LINE : <FITS extnum=value>
   !--------------------------------------------------------------------------
   xmlline%ttype        = 6 ! TYPE FITS
   xmlline%decal        = 12
   CALL INIT_xmlline(xmlline)
   CALL INT_STR(extnum,s_extnum)
   s_extnum2 = '"'//TRIM(ADJUSTL(s_extnum))//'"'

   !--- Search for first character :=
   i = 1
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   xmlline%ligne(poswrit:poswrit+lenextnum) = s_extnum2(1:lenextnum)
   !--- Write the line
   DO i = 1, lenstring
      WRITE(unitxml,"(A1)",ADVANCE="NO") xmlline%ligne(i:i)
   ENDDO
   WRITE(unitxml,*)

   !---------------------------------------------------------------------------
   ! WRITE LINE : <STREAM encoding=value href=value />
   !---------------------------------------------------------------------------
   xmlline%ttype = 7  ! TYPE STREAM
   xmlline%decal = 15
   CALL INIT_xmlline(xmlline)
   !--- Search for first character "="
   i = 1
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   encoding2 = '"'//TRIM(ADJUSTL(encoding))//'"'
   lenstr = LEN_TRIM(encoding2)
   xmlline%ligne(poswrit:poswrit+lenstr) = encoding2(1:lenstr)
   !--- Search for second character "="
   i = poswrit + lenstr + 2
   DO WHILE (xmlline%ligne(i:i) .NE. '=')
      i = i + 1
   ENDDO
   poswrit = i + 1
   href2   = '"'//TRIM(ADJUSTL(href))//'"'
   lenstr  = LEN_TRIM(href2)
   xmlline%ligne(poswrit:poswrit+lenstr) = href2(1:lenstr)
   DO i = 1, lenstring
      WRITE(unitxml,"(A1)",ADVANCE="NO") xmlline%ligne(i:i)
   ENDDO
   WRITE(unitxml,*)

   ! WRITE </FITS>
   WRITE(unitxml,"(A20)") "             </FITS>"

   ! WRITE </DATA>
   WRITE(unitxml,"(A17)") "         </DATA>"



END SUBROUTINE WRITE_VOT_DATA

END MODULE PXDR_UTIL_XML
