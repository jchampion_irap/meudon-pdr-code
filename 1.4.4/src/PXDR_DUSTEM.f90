
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

! JLB - 23 May 2008

MODULE PXDR_DUSTEM

  PRIVATE

  PUBLIC :: PDR_DUSTEM, DUST_PROPERTIES, EXTEND_DUST

CONTAINS

!%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE PDR_DUSTEM(nav)
!%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PXDR_RF_TOOLS

   IMPLICIT NONE

   INTEGER, INTENT (IN)                              :: nav          ! Nombre de profondeurs optiques

   INTEGER                                           :: i, j, n, iloc
   REAL(KIND=dp), DIMENSION(:,:), ALLOCATABLE        :: isrf         ! Spectre UV (grille DUSTEM)(entree)
   REAL(KIND=dp)                                     :: wl
   REAL(KIND=dp)                                     :: dum1, dum2, dum3, dum4, dum5
   INTEGER                                           :: n_comp       ! Number of dust component in DUSTEM
   INTEGER, DIMENSION(:), ALLOCATABLE                :: n_si         ! Number of sizes per coponent
   INTEGER                                           :: i1, i2, mm
   REAL(KIND=dp), DIMENSION(:,:), ALLOCATABLE        :: siz_cmp, wei_cmp, teq_cmp, tmean_cmp
   REAL(KIND=dp), DIMENSION(:), ALLOCATABLE          :: tt_interp, wei_interp
   REAL(KIND=dp), DIMENSION(:,:), ALLOCATABLE        :: t_mean_dust

   ALLOCATE (isrf(0:nwlg,nav))

   ! The radiation field is computed in erg cm-3 Angstr-1 in main (energy density)
   ! The quantity used in dustem is I_nu in erg cm-2 s-1 Hz-1
   ! It is safer not to change units of such a sensitive quantity within the code
   ! Remember wavelength is in micron in DUSTEM

   DO n = 1, nav
      DO i = 0, nwlg
        wl = rf_wl%Val(i)
        CALL RF_LOCATE (rf_wl, wl, iloc)
        isrf(i,n) = J_dust(n,iloc) * J_to_u * 1.0e-8_dp * rf_wl%Val(i)**2
      ENDDO
   ENDDO

   IF (ALLOCATED (dustem_in%abso)) THEN
      DEALLOCATE (dustem_in%abso)
      DEALLOCATE (dustem_in%scat)
      DEALLOCATE (dustem_in%emis)
      DEALLOCATE (dustem_in%gcos)
      DEALLOCATE (dustem_in%albe)
   ENDIF
   ALLOCATE (dustem_in%abso(nwl_ir+1,nav))
   ALLOCATE (dustem_in%scat(nwl_ir+1,nav))
   ALLOCATE (dustem_in%emis(nwl_ir+1,nav))
   ALLOCATE (dustem_in%gcos(nwl_ir+1,nav))
   ALLOCATE (dustem_in%albe(nwl_ir+1,nav))

   PRINT *, " nav_tot =", nav
   IF (ALLOCATED (t_mean_dust)) THEN
      DEALLOCATE(t_mean_dust)
   ENDIF
   ALLOCATE (t_mean_dust(npg,nav))
   DO n = 1, nav
      ! Write radiation field to Dustem
      OPEN(iwrtmp,FILE=TRIM(dustem_dat)//'ISRF.DAT',STATUS="unknown")
      WRITE(iwrtmp,'("#",/,"#",/,"#",/,"#",/,"#",/,"#",/,i8)') nwlg+1
      DO i = 0, nwlg
         WRITE(iwrtmp,*) rf_wl%Val(i)*1.0e-4_dp, isrf(i,n)      !! MGG 19 March 2008: ISRF.DAT is I_nu
      ENDDO
      CLOSE(iwrtmp)

      !  Comment: JLB 9 IX 08.
      !  The quantity computed in DUSTEM (read here) is expressed in erg s-1 H-1
      !  It is first converted to erg s-1 H-1 ster-1 A-1
      !  Then (in TRANSFER - ETIN90) it is converted to erg s-1 cm-3 ster-1 A-1 by multiplication by densh

      PRINT *, "  Calling DUSTEM 3.6, nav =", n

      CALL SYSTEM(lancer_dustem)

      OPEN(iwrtmp,FILE=TRIM(data_dir)//'EXTINCTION_DUSTEM.RES',STATUS="unknown")
      READ(iwrtmp,*)  nwl_ir
      DO i = 1, nwl_ir
         !! Warning: unit of emis is (erg s-1 H-1)
         !! Use logarithmic variables for interpolation
         READ(iwrtmp,*) wl, dum1, dum2, dum3, dum4, dum5
         wl = wl * 1.0e4_dp
         wl_ir(i) = wl
         wl_ir_l(i) = LOG(wl)
         dustem_in%abso(i,n) = LOG(dum1)
         dustem_in%scat(i,n) = LOG(dum2)
         dustem_in%emis(i,n) = LOG(dum3 / (4.0_dp * xpi * wl))
         dustem_in%albe(i,n) = LOG(dum4)
         dustem_in%gcos(i,n) = dum5
      ENDDO
      CLOSE(iwrtmp)

      OPEN(iwrtmp,FILE=TRIM(data_dir)//'T_MEAN.RES',STATUS="unknown")
      READ(iwrtmp,*) n_comp
      ALLOCATE (n_si(n_comp))
      READ(iwrtmp,*) (n_si(i), i = 1, n_comp)
      ALLOCATE (siz_cmp(n_comp,MAXVAL(n_si)))
      ALLOCATE (wei_cmp(n_comp,MAXVAL(n_si)))
      ALLOCATE (teq_cmp(n_comp,MAXVAL(n_si)))
      ALLOCATE (tmean_cmp(n_comp,MAXVAL(n_si)))
      DO i = 1, n_comp
         READ(iwrtmp,*)
         DO j = 1, n_si(i)
            READ(iwrtmp,*) i1, i2, siz_cmp(i,j), wei_cmp(i,j), teq_cmp(i,j), tmean_cmp(i,j)
         ENDDO
      ENDDO
      CLOSE(iwrtmp)
      ALLOCATE (tt_interp(n_comp))
      ALLOCATE (wei_interp(n_comp))
      DO mm = 1, npg
         DO i = 1, n_comp
            IF (adust(mm) < siz_cmp(i,1) .OR. adust(mm) > siz_cmp(i,n_si(i))) THEN
               wei_interp(i) = 0.0_dp
               tt_interp(i) = 0.0_dp
            ELSE
               DO j = 2, n_si(i)
                  IF (siz_cmp(i,j) > adust(mm)) THEN
                     EXIT
                  ENDIF
               ENDDO
               wei_interp(i) = wei_cmp(i,j-1) + (wei_cmp(i,j) - wei_cmp(i,j-1)) &
                                * (siz_cmp(i,j) - adust(mm)) / (siz_cmp(i,j) - siz_cmp(i,j-1))
               tt_interp(i) = tmean_cmp(i,j-1) + (tmean_cmp(i,j) - tmean_cmp(i,j-1)) &
                                * (siz_cmp(i,j) - adust(mm)) / (siz_cmp(i,j) - siz_cmp(i,j-1))
            ENDIF
         ENDDO
         t_mean_dust(mm,n) = SUM(wei_interp*tt_interp) / SUM(wei_interp)
      ENDDO
      DEALLOCATE (n_si)
      DEALLOCATE (siz_cmp, wei_cmp, teq_cmp, tmean_cmp)
      DEALLOCATE (tt_interp, wei_interp)
   ENDDO
   wl_ir(nwl_ir+1)   = wlthm
   wl_ir_l(nwl_ir+1) = LOG(wlthm)
   CALL EXTEND_DUST(nav)
   IF (ALLOCATED (tdust_dus_o)) THEN
      DEALLOCATE (tdust_dus_o)
   ENDIF
   ALLOCATE (tdust_dus_o(npg,0:npo))
   CALL INTERP_TDUST(nav, t_mean_dust)

   DEALLOCATE (isrf)

END SUBROUTINE PDR_DUSTEM

! This subroutine extends dust characterisitcs to the last point in the wavelength grid
!%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE EXTEND_DUST(nav)
!%%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PXDR_RF_TOOLS

   IMPLICIT NONE

   INTEGER, INTENT (IN)                               :: nav        !! Nombre de profondeurs optiques
   INTEGER                                            :: n
   INTEGER                                            :: i1, i2, i3
   REAL (KIND=dp)                                     :: rap_t

   i1 = nwl_ir-1
   i2 = nwl_ir
   i3 = nwl_ir+1
   rap_t = (wl_ir_l(i3) - wl_ir_l(i1)) / (wl_ir_l(i2) - wl_ir_l(i1))

   DO n = 1, nav
      dustem_in%abso(i3,n) = dustem_in%abso(i1,n) + (dustem_in%abso(i2,n) - dustem_in%abso(i1,n)) * rap_t
      dustem_in%scat(i3,n) = dustem_in%scat(i1,n) + (dustem_in%scat(i2,n) - dustem_in%scat(i1,n)) * rap_t
      dustem_in%emis(i3,n) = dustem_in%emis(i1,n) + (dustem_in%emis(i2,n) - dustem_in%emis(i1,n)) * rap_t
      dustem_in%albe(i3,n) = dustem_in%albe(i1,n) + (dustem_in%albe(i2,n) - dustem_in%albe(i1,n)) * rap_t
      dustem_in%gcos(i3,n) = 0.0_dp
   ENDDO

END SUBROUTINE EXTEND_DUST

! this routine computes the mean dust temperature from the individual means provided by DUSTEM
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE INTERP_TDUST(nav, t_mean_dust)
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PXDR_RF_TOOLS

   IMPLICIT NONE

   INTEGER, INTENT (IN)                                :: nav
   REAL(KIND=dp), DIMENSION(npg,nav), INTENT (IN)      :: t_mean_dust
   INTEGER                                             :: iopt, i_int, mm
   REAL(KIND=dp)                                       :: rapp

   ! JLB - 16 I 2012: change tau_o to tau here
   i_int = 1
   DO iopt = 0, npo
      CALL LOC_IND (i_int, 1, nav, tau(iopt), av_dust)
      rapp = (tau(iopt) - av_dust(i_int)) / (av_dust(i_int+1) - av_dust(i_int))
      DO mm = 1, npg
         tdust_dus_o(mm,iopt) = t_mean_dust(mm,i_int) &
                              + ( t_mean_dust(mm,i_int+1) -  t_mean_dust(mm,i_int)) * rapp
      ENDDO
   ENDDO

END SUBROUTINE INTERP_TDUST

! This routine initialise "du_prop" with dust properties (absorption, scatering, emission,
!      albedo and anisotropy g factor) for each wavelength and each position in the cloud.
! Three different inputs are available:
!      * Fitzpatrick and Massa fits to extinction curves in the UV
!      * Draine's data for graphite and silicates
!      * A call to DUSTEM for any mixture of grains there defined.

!%%%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE DUST_PROPERTIES
!%%%%%%%%%%%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_STR_DATA
   USE PXDR_RF_TOOLS
   USE PXDR_DRAINE_DUST

   IMPLICIT NONE

   INTEGER                      :: iopt, mm                      ! Index for position and grain size
   INTEGER                      :: ilocb, ilocv                  ! Index for wavelengths
   INTEGER                      :: i, j                          ! Loop index
   REAL (KIND=dp)               :: wlth                          ! A wavelength
   REAL (KIND=dp)               :: s_wg_a2                       ! Normalisation factor for mean BB radiation
   INTEGER                      :: il1, il2                      ! Index for interpolation
   REAL (KIND=dp)               :: fact                          ! Interpolation ratio
   REAL (KIND=dp), DIMENSION(5) :: fe                            ! Auxiliary vector for interpolation
   REAL (KIND=dp)               :: Dr_int1, Dr_int2, Dr_int3     ! Auxiliaries for mean over grain size
   REAL (KIND=dp)               :: absor, scater                 ! Partial summation variables
   REAL (KIND=dp)               :: emi, le_g                     ! Partial summation variables
   REAL (KIND=dp)               :: B_ext, V_ext                  ! Extinction at B and V bands
   REAL (KIND=dp)               :: cd_eff                        ! Effective CD_unit

! For comparison purpose only
   TYPE (DU_DIST)       :: du_Fitzpa, du_Draine, du_Dustem

   ! Properties set at all points in wavelength and position, based on previous iteration.

   PRINT *, " Deallocate du_prop"
   IF (ALLOCATED (du_prop%abso)) THEN
      DEALLOCATE (du_prop%abso)
      DEALLOCATE (du_prop%scat)
      DEALLOCATE (du_prop%emis)
      DEALLOCATE (du_prop%gcos)
      DEALLOCATE (du_prop%albe)
   ENDIF
   ALLOCATE (du_prop%abso(0:nwlg,0:npo))
   ALLOCATE (du_prop%scat(0:nwlg,0:npo))
   ALLOCATE (du_prop%emis(0:nwlg,0:npo))
   ALLOCATE (du_prop%gcos(0:nwlg,0:npo))
   ALLOCATE (du_prop%albe(0:nwlg,0:npo))
   ALLOCATE (du_Draine%abso(0:nwlg,0:npo))
   ALLOCATE (du_Draine%scat(0:nwlg,0:npo))
   ALLOCATE (du_Draine%emis(0:nwlg,0:npo))
   ALLOCATE (du_Draine%gcos(0:nwlg,0:npo))
   ALLOCATE (du_Draine%albe(0:nwlg,0:npo))
   ALLOCATE (du_Fitzpa%abso(0:nwlg,0:npo))
   ALLOCATE (du_Fitzpa%scat(0:nwlg,0:npo))
   ALLOCATE (du_Fitzpa%emis(0:nwlg,0:npo))
   ALLOCATE (du_Fitzpa%gcos(0:nwlg,0:npo))
   ALLOCATE (du_Fitzpa%albe(0:nwlg,0:npo))
   ALLOCATE (du_Dustem%abso(0:nwlg,0:npo))
   ALLOCATE (du_Dustem%scat(0:nwlg,0:npo))
   ALLOCATE (du_Dustem%emis(0:nwlg,0:npo))
   ALLOCATE (du_Dustem%gcos(0:nwlg,0:npo))
   ALLOCATE (du_Dustem%albe(0:nwlg,0:npo))

   ! Absorption, scatering and emission coefficient do not include the factor n_H (densh(iopt))
   ! Do no forget to scale when using.

   ! Black-Body radiation of grains are weighted by their surface.
   s_wg_a2 = 0.0_dp
   DO mm=1,npg
      s_wg_a2 = s_wg_a2 + wwmg(mm) * adust(mm)**2
   ENDDO

   ! du_Draine and du_Fitzpa do not depend on position (yet)
   ! Except emissivity (by T_dust)
   DO iopt = 0, npo

      ! 1 - Compute coefficients from Draine's datafile
      DO j = 0, nwlg
         wlth = rf_wl%Val(j)

         DO i = jwlmin_gr, jwlmax_gr+1
            IF (wavgrd(i) > wlth) THEN
               il1 = i-1
               il2 = i-jwlmin_gr
               EXIT
            ENDIF
            IF (wavgrd(jwlmax_gr) < wlth) THEN
               il1 = jwlmax_gr
               il2 = jwlmax_gr - jwlmin_gr + 1
            ENDIF
         ENDDO

         ! longward the last point in Draine's file an interpolation in Log is required
         IF (il1 < jwlmax_gr) THEN
            fact = (wlth - wavgrd(il1)) / (wavgrd(il1+1) - wavgrd(il1))

            emi = 0.0_dp
            absor = 0.0_dp
            scater = 0.0_dp
            le_g = 0.0_dp
            DO mm = 1, npg
               ! Absorption: sum over size
               Dr_int1 = Qabs_Dr(il2,mm) + (Qabs_Dr(il2+1,mm) - Qabs_Dr(il2,mm)) * fact
               absor = absor + wwmg(mm) * Dr_int1 * xpi * adust(mm)**2
               ! Scattering: sum over size
               Dr_int2 = Qsca_Dr(il2,mm) + (Qsca_Dr(il2+1,mm) - Qsca_Dr(il2,mm)) * fact
               scater = scater + wwmg(mm) * Dr_int2 * xpi * adust(mm)**2
               ! g factor: sum over scatering
               Dr_int3 = g_Dr(il2,mm) + (g_Dr(il2+1,mm) - g_Dr(il2,mm)) * fact
               le_g = le_g + wwmg(mm) * Dr_int3 * Dr_int2 * xpi * adust(mm)**2
               ! Emissivity: sum over size and Black Body
               emi = emi + wwmg(mm) * BBL (wlth, tdust(mm,iopt)) * Dr_int1 * xpi * adust(mm)**2
            ENDDO
         ELSE
            fact = (LOG(wlth) - LOG(wavgrd(il1))) / (LOG(wavgrd(il1+1)) - LOG(wavgrd(il1)))

            emi = 0.0_dp
            absor = 0.0_dp
            scater = 0.0_dp
            le_g = 0.0_dp
            DO mm = 1, npg
               ! Absorption: sum over size
               Dr_int1 = LOG(Qabs_Dr(il2,mm)) + (LOG(Qabs_Dr(il2+1,mm)) - LOG(Qabs_Dr(il2,mm))) * fact
               absor = absor + wwmg(mm) * EXP(Dr_int1) * xpi * adust(mm)**2
               ! Scattering: sum over size
               Dr_int2 = LOG(Qsca_Dr(il2,mm)) + (LOG(Qsca_Dr(il2+1,mm)) - LOG(Qsca_Dr(il2,mm))) * fact
               scater = scater + wwmg(mm) * EXP(Dr_int2) * xpi * adust(mm)**2
               ! g factor: sum over scatering
               Dr_int3 = g_Dr(il2,mm) + (g_Dr(il2+1,mm) - g_Dr(il2,mm)) &
                       * (wlth - wavgrd(il1)) / (wavgrd(il1+1) - wavgrd(il1))
               le_g = le_g + wwmg(mm) * Dr_int3 * EXP(Dr_int2) * xpi * adust(mm)**2
               ! Emissivity: sum over size and Black Body
               emi = emi + wwmg(mm) * BBL (wlth, tdust(mm,iopt)) * EXP(Dr_int1) * xpi * adust(mm)**2
            ENDDO
         ENDIF

         ! Warning: agrnrm is not in "le_g". Divide by scater before normalisation
         absor = agrnrm * absor
         le_g = le_g / scater
         scater = agrnrm * scater
         du_Draine%abso(j,iopt) = absor
         du_Draine%scat(j,iopt) = scater
         du_Draine%albe(j,iopt) = scater / (absor + scater)
         du_Draine%gcos(j,iopt) = le_g
         du_Draine%emis(j,iopt) = agrnrm * emi
      ENDDO

      ! 2 - Compute coefficients from input data file and Fitzpatrick and Massa fit
      ! Replace constant g by the one from Draine
      du_Fitzpa%gcos(:,iopt) = du_Draine%gcos(:,iopt)
      ! Initialise albedo with the one from Draine
      du_Fitzpa%albe(:,iopt) = du_Draine%albe(:,iopt)
      ! Absorption, Scatering and Emission do not depend on position into the cloud (yet)
      ! Here, they are recovered from the Fitzpatrick and Massa extinction curve.
      du_Fitzpa%abso(:,iopt) = (1.0_dp - du_Fitzpa%albe(:,iopt)) * rf_extpou%Val(:) / coef2
      du_Fitzpa%scat(:,iopt) = du_Fitzpa%albe(:,iopt) * rf_extpou%Val(:) / coef2
      ! Emission is computed by Kirshoff's law at dust temperature.
      DO j = 0, nwlg
         wlth = rf_wl%Val(j)
         emi = 0.0_dp
         ! Compute mean Black Body radiation (over dust temperature)
         DO mm = 1, npg
            emi = emi + wwmg(mm) * BBL (wlth, tdust(mm,iopt)) * adust(mm)**2
         ENDDO
         emi = emi / s_wg_a2
         du_Fitzpa%emis(j,iopt) = du_Fitzpa%abso(j,iopt) * emi
      ENDDO
   ENDDO

   !======================================================================================
   ! 3 - IF WE USE DUSTEM, COMPUTE HERE
   !======================================================================================
   IF (F_dustem == 1 .AND. ALLOCATED (J_dust)) THEN
      CALL PDR_DUSTEM(nav_dust)
      il2 = 1
      DO iopt = 0, npo
         il1 = 1
         DO j = 0, nwlg
            wlth = rf_wl%Val(j)
            CALL INTERP_BILIN (LOG(wlth), tau(iopt), il1, il2, wl_ir_l, nwl_ir+1, av_dust, nav_dust, dustem_in, fe)
            du_Dustem%abso(j,iopt) = EXP(fe(1))
            du_Dustem%scat(j,iopt) = EXP(fe(2))
            du_Dustem%emis(j,iopt) = EXP(fe(3))
            du_Dustem%albe(j,iopt) = EXP(fe(4))
            du_Dustem%gcos(j,iopt) = fe(5)
         ENDDO
      ENDDO
   ENDIF

   !======================================================================================
   ! DEBUG, WRITE ALL THE DATA IN THE du_pro.dat FILE
   !======================================================================================
   OPEN (iwrtmp, FILE="du_pro.dat", STATUS="unknown")
      DO j = 0, nwlg
         WRITE (iwrtmp,'(1p,22e16.7E3)') tau(0)*tautav, rf_wl%Val(j) &
                                  , du_Fitzpa%abso(j,0), du_Fitzpa%scat(j,0), du_Fitzpa%emis(j,0) &
                                  , du_Fitzpa%albe(j,0), du_Fitzpa%gcos(j,0) &
                                  , du_Draine%abso(j,0), du_Draine%scat(j,0), du_Draine%emis(j,0) &
                                  , du_Draine%albe(j,0), du_Draine%gcos(j,0) &
                                  , du_Dustem%abso(j,0), du_Dustem%scat(j,0), du_Dustem%emis(j,0) &
                                  , du_Dustem%albe(j,0), du_Dustem%gcos(j,0) &
                                  , du_Dustem%abso(j,npo), du_Dustem%scat(j,npo), du_Dustem%emis(j,npo) &
                                  , du_Dustem%albe(j,npo), du_Dustem%gcos(j,npo)
      ENDDO
      WRITE (iwrtmp,'(" ")')
   CLOSE (iwrtmp)

   ! Select which data are used in the code
   IF (F_dustem == 1 .AND. ALLOCATED (J_dust)) THEN
      du_prop = du_Dustem
   ELSE
      du_prop = du_Fitzpa
   ENDIF

   CALL RF_LOCATE (rf_wl, B_band_wl, ilocb)
   B_ext = du_prop%abso(ilocb,0) + du_prop%scat(ilocb,0)
   CALL RF_LOCATE (rf_wl, V_band_wl, ilocv)
   V_ext = du_prop%abso(ilocv,0) + du_prop%scat(ilocv,0)
   IF (B_ext /= V_ext) THEN
      PRINT *, "  Effective R_V at edge:", V_ext / (B_ext - V_ext)
      PRINT *, "  R_V from input file:", rrv
   ELSE
      PRINT *, "  R_V not defined:"
      PRINT *, "  B_ext = V_ext =", B_ext
   ENDIF

   fichier = TRIM(out_dir)//TRIM(ADJUSTL(modele))//'.extinct'
   OPEN (iwrtmp, FILE=fichier, STATUS="unknown")
      WRITE (iwrtmp,'("# Extinction at edge")')
      WRITE (iwrtmp,'("# 1/wl (micron-1), Fitzpa, Draine, Dustem, Input")')
      DO j = 0, nwlg
         IF (F_dustem == 1 .AND. ALLOCATED (J_dust)) THEN
            WRITE (iwrtmp,'(1p,5e16.7E3)') 1.0e4_dp/rf_wl%Val(j) &
                                  , ((du_Fitzpa%abso(j,0)+du_Fitzpa%scat(j,0)) &
                                  - (du_Fitzpa%abso(ilocv,0)+du_Fitzpa%scat(ilocv,0))) &
                                  / ((du_Fitzpa%abso(ilocb,0)+du_Fitzpa%scat(ilocb,0)) &
                                  - (du_Fitzpa%abso(ilocv,0)+du_Fitzpa%scat(ilocv,0))) &
                                  , ((du_Draine%abso(j,0)+du_Draine%scat(j,0)) &
                                  - (du_Draine%abso(ilocv,0)+du_Draine%scat(ilocv,0))) &
                                  / ((du_Draine%abso(ilocb,0)+du_Draine%scat(ilocb,0)) &
                                  - (du_Draine%abso(ilocv,0)+du_Draine%scat(ilocv,0))) &
                                  , ((du_Dustem%abso(j,0)+du_Dustem%scat(j,0)) &
                                  - (du_Dustem%abso(ilocv,0)+du_Dustem%scat(ilocv,0))) &
                                  / ((du_Dustem%abso(ilocb,0)+du_Dustem%scat(ilocb,0)) &
                                  - (du_Dustem%abso(ilocv,0)+du_Dustem%scat(ilocv,0))) &
                                  , (rf_extpou%Val(j) - 1.0_dp) * rrv
         ELSE
            WRITE (iwrtmp,'(1p,5e16.7E3)') 1.0e4_dp/rf_wl%Val(j) &
                                  , ((du_Fitzpa%abso(j,0)+du_Fitzpa%scat(j,0)) &
                                  - (du_Fitzpa%abso(ilocv,0)+du_Fitzpa%scat(ilocv,0))) &
                                  / ((du_Fitzpa%abso(ilocb,0)+du_Fitzpa%scat(ilocb,0)) &
                                  - (du_Fitzpa%abso(ilocv,0)+du_Fitzpa%scat(ilocv,0))) &
                                  , ((du_Draine%abso(j,0)+du_Draine%scat(j,0)) &
                                  - (du_Draine%abso(ilocv,0)+du_Draine%scat(ilocv,0))) &
                                  / ((du_Draine%abso(ilocb,0)+du_Draine%scat(ilocb,0)) &
                                  - (du_Draine%abso(ilocv,0)+du_Draine%scat(ilocv,0))) &
                                  , (rf_extpou%Val(j) - 1.0_dp) * rrv
         ENDIF
      ENDDO
      WRITE (iwrtmp,'(" ")')
   CLOSE (iwrtmp)
   PRINT *, "  cdunit from input file:", cdunit
   PRINT *, "  Estimated cdunit:"
   PRINT *, "     from Fitzpa:" &
          , 1.0_dp / (tautav * ((du_Fitzpa%abso(ilocb,0)+du_Fitzpa%scat(ilocb,0)) &
                              - (du_Fitzpa%abso(ilocv,0)+du_Fitzpa%scat(ilocv,0))))
   PRINT *, "         (", du_Fitzpa%abso(ilocb,0), du_Fitzpa%scat(ilocb,0), ")"
   PRINT *, "         (", du_Fitzpa%abso(ilocv,0), du_Fitzpa%scat(ilocv,0), ")"
   PRINT *, "     from Draine:" &
          , 1.0_dp / (tautav * ((du_Draine%abso(ilocb,0)+du_Draine%scat(ilocb,0)) &
                              - (du_Draine%abso(ilocv,0)+du_Draine%scat(ilocv,0))))
   PRINT *, "         (", du_Draine%abso(ilocb,0), du_Draine%scat(ilocb,0), ")"
   PRINT *, "         (", du_Draine%abso(ilocv,0), du_Draine%scat(ilocv,0), ")"
   IF (F_dustem /= 0 .AND. ALLOCATED (J_dust)) THEN
      PRINT *, "     from DUSTEM:" &
             , 1.0_dp / (tautav * ((du_Dustem%abso(ilocb,0)+du_Dustem%scat(ilocb,0)) &
                                 - (du_Dustem%abso(ilocv,0)+du_Dustem%scat(ilocv,0))))
      PRINT *, "         (", du_Dustem%abso(ilocb,0), du_Dustem%scat(ilocb,0), ")"
      PRINT *, "         (", du_Dustem%abso(ilocv,0), du_Dustem%scat(ilocv,0), ")"
   ENDIF

   cd_eff  = 1.0_dp / (tautav * (( du_prop%abso(ilocb,0)+du_prop%scat(ilocb,0)) &
                               - (du_prop%abso(ilocv,0)+du_prop%scat(ilocv,0))))
   IF (ABS(cdunit - cd_eff)/cdunit > 1.0e-3_dp) THEN
      PRINT *, " WARNING !"
      PRINT *, " Inconsistency in input file:"
      PRINT *, "     input r_min and r_max =", rgrmin, rgrmax
      PRINT *, "                     rhogr =", rhogr
      PRINT *, "             input g_ratio =", g_ratio
      PRINT *, "              input cdunit =", cdunit
      PRINT *, "          effective cdunit =", cd_eff
      PRINT *, "           computed rgazgr =", rgazgr
      PRINT *, "             computed xngr =", xngr
      PRINT *, "           computed agrnrm =", agrnrm
      PRINT *, "            computed coef2 =", coef2
      PRINT *, " Adjust cdunit, rgrmax or g_ratio to consistent values"
      IF (F_dustem /= 1) THEN
         STOP
      ENDIF
   ENDIF

   PRINT *, " "
   PRINT *, " coef2 =", coef2
   PRINT *, " 1/(k+s) =", 1.0 / (du_Fitzpa%abso(ilocv,0)+du_Fitzpa%scat(ilocv,0))

   DEALLOCATE (du_Draine%abso)
   DEALLOCATE (du_Draine%scat)
   DEALLOCATE (du_Draine%emis)
   DEALLOCATE (du_Draine%gcos)
   DEALLOCATE (du_Draine%albe)
   DEALLOCATE (du_Fitzpa%abso)
   DEALLOCATE (du_Fitzpa%scat)
   DEALLOCATE (du_Fitzpa%emis)
   DEALLOCATE (du_Fitzpa%gcos)
   DEALLOCATE (du_Fitzpa%albe)
   DEALLOCATE (du_Dustem%abso)
   DEALLOCATE (du_Dustem%scat)
   DEALLOCATE (du_Dustem%emis)
   DEALLOCATE (du_Dustem%gcos)
   DEALLOCATE (du_Dustem%albe)

END SUBROUTINE DUST_PROPERTIES

END MODULE PXDR_DUSTEM
