
! Copyright (C) 2002 - Observatoire de Paris/Meudon
!                      Universite Paris 7
!                      CNRS (France)
!                      Contact : Jacques.Lebourlot@obspm.fr
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License version 2
! as published by the Free Software Foundation.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

! JLB - 24 May 2008

MODULE PREP_AUTRE

  PRIVATE

  PUBLIC :: AUTRE, XKFILL

CONTAINS

!%%%%%%%%%%%%%%%
SUBROUTINE AUTRE
!%%%%%%%%%%%%%%%

   USE PXDR_CONSTANTES
   USE PXDR_CHEM_DATA
   USE PXDR_PROFIL
   USE PXDR_AUXILIAR
   USE PXDR_RF_TOOLS
   USE PREP_VAR

   IMPLICIT NONE

   CHARACTER (LEN=8)  :: yspec
   INTEGER            :: voie_disso
   INTEGER            :: mm, jj, j, k
   INTEGER            :: ii2, ii3

!--------+---------+---------+---------+---------+---------+---------+-*-------+

   ii2 = 0
   DO WHILE (ii2 /= -1)
      PRINT *, '  1: Gas Temperature'
      PRINT *, '  2: Dust Temperatures'
      PRINT *, '  3: Proton Density (n(H) + 2 n(H2) + n(H+) + ...)'
      PRINT *, '  4: Total gas density (n(H) + n(H2) + n(He) + ...)'
      PRINT *, '  5: Ionisation degree'
      PRINT *, '  6: Pressure'
      PRINT *, '  7: l (distance in cm)'
      PRINT *, '  8: UV photodestruction from integration of cross sections'
      PRINT *, '  9: formation destruction of H2 and HD'
      PRINT *, ' 10: Dust charge'
      PRINT *, ' 12: Radiation (photon and energy densities from Lyman to Habing)'
      PRINT *, ' 13: Integrated molecular fraction'
      PRINT *, ' -1: end'
      READ *, ii2
      IF (irec == 1) THEN
         WRITE (8,*) ii2
      ENDIF

      IF (ii2 == -1) THEN
         EXIT
      ENDIF

      IF (ii2 == 1) THEN
         iimax = iimax + 1
         y(iimax,0:npo) = tgaz(0:npo)
         texty(iimax) = 'Temp.Gaz      '
      ELSE IF (ii2 == 2) THEN
         DO mm = 1, npg
            iimax = iimax + 1
            y(iimax,0:npo) = tdust(mm,0:npo)
            texty(iimax) = 'Td.'//TRIM(ADJUSTL(ii2c(mm)))
         ENDDO
      ELSE IF (ii2 == 3) THEN
         iimax = iimax + 1
         y(iimax,0:npo) = densh(0:npo)
         texty(iimax) = 'Dens.H        '
      ELSE IF (ii2 == 4) THEN
         iimax = iimax + 1
         y(iimax,0:npo) = totdensity(0:npo)
         texty(iimax) = 'Dens.Tot      '
      ELSE IF (ii2 == 5) THEN
         iimax = iimax + 1
         y(iimax,0:npo) = ionidegree(0:npo)
         texty(iimax) = 'Ionisation    '
      ELSE IF (ii2 == 6) THEN
         iimax = iimax + 1
         y(iimax,0:npo) = gaspressure(0:npo)
         texty(iimax) = 'Pression      '
      ELSE IF (ii2 == 7) THEN
         iimax = iimax + 1
         y(iimax,0) = 0.0_dp
         y(iimax,0:npo) = d_cm(0:npo)
         texty(iimax) = 'Taille.(cm)   '

      ELSE IF (ii2 == 8) THEN

2006  CONTINUE
         PRINT *, ' which species ?'
         PRINT *, '-1: fin'
         READ (*,'(a8)') yspec
         IF (irec == 1) THEN
            WRITE (8,'(a8)') yspec
         ENDIF

         IF (yspec == "-1") THEN
            CYCLE
         ELSE
            voie_disso = 0
            DO jj = 1, nphot_i
               IF (ndiesp(jj) == yspec) THEN
                  voie_disso = voie_disso + 1
               ENDIF
            ENDDO
            ii3 = 0
            DO jj = 1, nphot_i
               IF (ndiesp(jj) == yspec) THEN
                  ii3 = jj
                  EXIT
               ENDIF
            ENDDO
            PRINT*,'nbre de voies de disso: ', voie_disso
            IF (voie_disso >= 1) THEN
               IF (ii3 /= 0) THEN
                  iimax = iimax + 1
                  y(iimax,:) = 0.0_dp
                  DO j = 0, npo
                     DO k = 1, voie_disso
                        y(iimax,j) = y(iimax,j) + pdiesp(ii3+(k-1),j)
                     ENDDO
                  ENDDO
                  texty(iimax) = 'Pd.'//yspec
                  GOTO 2006
               ENDIF
            ELSE
               PRINT *, '  No cross-section for ', yspec
               GOTO 2006
            ENDIF
         ENDIF

      ELSE IF (ii2 == 9) THEN

         PRINT *, ' 1: H2, 2: CO, 3: HD'
         READ *, ii3
         IF (irec == 1) THEN
            WRITE (8,*) ii3
         ENDIF

         IF (ii3 == 1) THEN
            iimax = iimax + 1
            y(iimax,0) = 0.0_dp
            y(iimax,0:npo) = timh21(0:npo)
            texty(iimax) = 'FH2chi        '
            iimax = iimax + 1
            y(iimax,0) = 0.0_dp
            y(iimax,0:npo) = timh22(0:npo)
            texty(iimax) = 'DH2chi        '
            iimax = iimax + 1
            y(iimax,0) = 0.0_dp
            y(iimax,0:npo) = timh23(0:npo)
            texty(iimax) = 'FH2-g-LH      '
            iimax = iimax + 1
            y(iimax,0) = 0.0_dp
            y(iimax,0:npo) = timh24(0:npo)
            texty(iimax) = 'DH2phot       '
            iimax = iimax + 1
            y(iimax,0) = 0.0_dp
            y(iimax,0:npo) = timh25(0:npo)
            texty(iimax) = 'FH2-g-ER      '
            iimax = iimax + 1
            y(iimax,0:npo) = (timh23(0:npo) + timh25(0:npo)) &
                           / (vmaxw * SQRT(tgaz(0:npo)) * s_gr_t * densh(0:npo) * abnua(i_h,0:npo) / 8.0_dp)
            texty(iimax) = 'FH2-gr-eff   '                   ! Grain efficiency
         ELSE IF (ii3 == 2) THEN
            PRINT *, " To be upgraded (not done yet)"
         ELSE  IF (ii3 == 3) THEN
            iimax = iimax + 1
            y(iimax,0) = 0.0_dp
            y(iimax,0:npo) = timhd1(0:npo)
            texty(iimax) = 'FHDchi        '
            iimax = iimax + 1
            y(iimax,0) = 0.0_dp
            y(iimax,0:npo) = timhd2(0:npo)
            texty(iimax) = 'DHDchi        '
            iimax = iimax + 1
            y(iimax,0) = 0.0_dp
            y(iimax,0:npo) = timhd3(0:npo)
            texty(iimax) = 'FHDgrain      '
            iimax = iimax + 1
            y(iimax,0) = 0.0_dp
            y(iimax,0:npo) = timhd4(0:npo)
            texty(iimax) = 'DHDphot       '
         ELSE
            PRINT *, 'WARNING: illegal value for ii3:', ii3
            CYCLE
         ENDIF

      ELSE IF (ii2 == 10) THEN
         iimax = iimax + 1
         y(iimax,0:npo) = grcharge(0:npo)
         texty(iimax) = 'Gr.Char(cm-3) '
         iimax = iimax + 1
         y(iimax,0:npo) = zmoy(1,0:npo)
         texty(iimax) = 'R.gr.max   '
         iimax = iimax + 1
         y(iimax,0:npo) = zmoy(npg/2,0:npo)
         texty(iimax) = 'R.gr.moy   '
         iimax = iimax + 1
         y(iimax,0:npo) = zmoy(npg,0:npo)
         texty(iimax) = 'R.gr.min   '
      ELSE IF (ii2 == 12) THEN
         ! 7 I 2013 - JLB - Stop integration at 2400 Angstrom (Habing limit)
         iimax = iimax + 1
         y(iimax,0:npo) = fluph(0:npo)
         texty(iimax) = 'Flu.PHOT      '
         iimax = iimax + 1
         y(iimax,0:npo) = int_rad(0:npo)
         texty(iimax) = 'U.(erg.cm-3)  '
      ELSE IF (ii2 == 13) THEN
         iimax = iimax + 1
         IF (i_hp /= 0) THEN
            y(iimax,0:npo) = 2.0_dp * codesp(i_h2,0:npo) / (2.0_dp*codesp(i_h2,0:npo) &
                           + codesp(i_h,0:npo) + codesp(i_hp,0:npo))
         ELSE
            y(iimax,0:npo) = 2.0_dp * codesp(i_h2,0:npo) / (2.0_dp*codesp(i_h2,0:npo) &
                           + codesp(i_h,0:npo))
         ENDIF
         texty(iimax) = ' f.int '
      ELSE
         PRINT *, 'WARNING: Illegal value for ii2:', ii2
         CYCLE
      ENDIF
   ENDDO

END SUBROUTINE AUTRE

!%%%%%%%%%%%%%%%%%%%%%%%
SUBROUTINE XKFILL (iopt)
!%%%%%%%%%%%%%%%%%%%%%%%

  USE PXDR_CONSTANTES
  USE PXDR_CHEM_DATA
  USE PXDR_PROFIL
  USE PXDR_AUXILIAR
  USE PXDR_STR_DATA
  USE PXDR_RF_TOOLS
  USE PXDR_CHEMISTRY
  USE PREP_VAR

  IMPLICIT NONE

  INTEGER, INTENT (IN)      :: iopt

  INTEGER, DIMENSION(0:nes) :: indy
  REAL (KIND=dp)            :: stickH
  REAL (KIND=dp)            :: stick_ER
  REAL (KIND=dp)            :: rate1, rate2, rate
  REAL (KIND=dp)            :: rat_l, endo_rxn, expon
  REAL (KIND=dp)            :: frach2, avr
  INTEGER                   :: mm, ir3, lev
  INTEGER                   :: i, j, ik, kkk, ideb, ifin
  INTEGER                   :: ir1, ir2, iprod
  REAL (KIND=dp)            :: nu0, nu0_1, nu0_2
  REAL (KIND=dp)            :: fr_free, tgr
  REAL (KIND=dp)            :: summ, r_ad_i, r_rej, tm1_1, tm1_2
  REAL (KIND=dp)            :: xldb_1, xldb_2
  INTEGER                   :: iv1, iv2, ivp

  indy = 0

  ! Extinction from the "other" side
  av  = tau(iopt) * tautav
  avr = tau(npo) * tautav - av

  ! Fraction of H2 (for secondary photons flux)
  frach2 = abnua(i_h2,iopt) / (abnua(i_h,iopt) + abnua(i_h2,iopt))

! chemical reactions are reordered:
! (0) formation of H2 and HD on grains [Old Style] (nh2for)
! (1) cosmic ray processes (ncrion)
! (2) secondary photon processes (npsion)
! (3) radiative association  (nasrad)
! (4) gas phase reactions (nrest)
! (6) Endothermal reactions with H2 (nh2endo) (NEW!)
! (5) photoreactions avec fit exterieur (nphot_f)
! (7) photoreactions avec integration directe (nphot_i)
! (8) 3 body reactions (n3body)
! (101...) exceptions (ncpart)

! (11) reactions on grain surface (ngsurf)
! (12) photoreactions on grain surface (ngphot)
! (13) adsorption on to grains (ngads)
! (14) neutralization (ngneut)
! (15) explosive desorption from grains (nggdes)
! (16) cosmic ray induced desorption from grains (ncrdes)
! (17) photodesorption from grains (nphdes)
! (18) evaporation from grains (ngevap)
! (19) chemisorption on grains (ngchs)
! (20) Eley-Rideal process (ngelri)

!-------------------------------------------------------------------------
!  Special cases       : NSPEC + 1 : ELECTRON
!                        NSPEC + 2 : PHOTON
!                        NSPEC + 3 : CRP
!                        NSPEC + 4 : GRAIN

!       -------------------
!-- 1 - Gas phase reactions
!       -------------------

!-------------------------------------------------------------------------
! (0) formation of H2 and HD on grains [Old Style] (nh2for)  (cm3 s-1)
!
! This type allows users to force the H2 formation rate.
! Example : to have R = 3E-17 cm+3 s-1 in the whole cloud, use in chemistry :
! OLD   h       h       h2                             3.00d-17  0.00     0.00   0
! and remove all h:, h2:, ... species
! The rate is scaled by SQRT(T/100) and the metallicity (g_ratio/0.01)
!-------------------------------------------------------------------------

  DO i = 1, nh2for
     ir1 = react(i,1)
     ir2 = react(i,2)
     iprod = react(i,4)

     ! The scaling (100.0 or 300.0 K) and the exponent (0.0 or 0.5) are set in the chemistry file
     rate = gamm(i) * densh(iopt) * (tgaz(iopt) / bet(i))**alpha(i) &
          * (g_ratio / 0.01_dp) / (istiny + abnua(i_h,iopt))

     kkk = indy(iprod) + 1
     indy(iprod) = kkk
     xmatk(iprod)%fdt(kkk)%k = rate

     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate

     kkk = indy(ir2) + 1
     indy(ir2) = kkk
     xmatk(ir2)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (1) cosmic ray processes (ncrion)   (s-1)
!-------------------------------------------------------------------------
  ideb = nh2for + 1
  ifin = nh2for + ncrion
  DO i = ideb, ifin
     rate = gamm(i) * zeta

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (2) secondary photon processes (npsion)   (s-1)
!-------------------------------------------------------------------------

  ideb = ifin + 1
  ifin = ifin + npsion
  DO i = ideb, ifin
     rate = gamm(i) * frach2 * zeta * (tgaz(iopt)/300.0_dp)**alpha(i)

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (3) radiative association  (nasrad)   (cm3 s-1)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + nasrad
  DO i = ideb, ifin
     rate = gamm(i) * (tgaz(iopt)/300.0_dp)**alpha(i) * EXP(-bet(i)/tgaz(iopt))

     iprod = react(i,4)
     kkk = indy(iprod) + 1
     indy(iprod) = kkk
     xmatk(iprod)%fdt(kkk)%k = rate

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate

     ir2 = react(i,2)
     kkk = indy(ir2) + 1
     indy(ir2) = kkk
     xmatk(ir2)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (4) gas phase reactions (nrest)   (cm3 s-1)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + nrest
  DO i = ideb, ifin
     IF (bet(i) < 0.0_dp) THEN
        expon = EXPBOR(-bet(i)/MAX(tgaz(iopt),200.0_dp))
     ELSE
        expon = EXPBOR(-bet(i)/tgaz(iopt))
     ENDIF

     rate = gamm(i) * (tgaz(iopt)/300.0_dp)**alpha(i) * expon

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate

     ir2 = react(i,2)
     kkk = indy(ir2) + 1
     indy(ir2) = kkk
     xmatk(ir2)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (6) Endothermal reactions with H2 (nh2endo)   (cm3 s-1)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + nh2endo
  DO i = ideb, ifin
     rate = 0.0_dp
     IF (itype(i) == 6) THEN
        DO lev = 1, l0h2
           endo_rxn = MAX(bet(i) - spec_lin(in_sp(i_h2))%elk(lev), 0.0_dp)
           rat_l = gamm(i) * (tgaz(iopt)/300.0_dp)**alpha(i) * EXP(-endo_rxn/tgaz(iopt))
           rate = rate + rat_l * spec_lin(in_sp(i_h2))%xre(lev,iopt)
        ENDDO
     ELSE IF (itype(i) == 61) THEN ! Formation of CH+ following Agundez et al. ApJ (2010)
        DO lev = 1, 8
           endo_rxn = MAX(4827.0e0_dp-spec_lin(in_sp(i_h2))%elk(lev),0.0e0_dp)
           rat_l = 1.58e-10_dp * EXP(-endo_rxn/tgaz(iopt))
           rate  = rate + rat_l * spec_lin(in_sp(i_h2))%xre(lev,iopt)
        ENDDO
        DO lev = 9, l0h2
           rate = rate + 1.6e-9_dp * spec_lin(in_sp(i_h2))%xre(lev,iopt)
        ENDDO
     ENDIF

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate

     ir2 = react(i,2)
     kkk = indy(ir2) + 1
     indy(ir2) = kkk
     xmatk(ir2)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (5) photoreactions (external fit) (nphot_f)    (s-1)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + nphot_f
  DO i = ideb, ifin
     ir1 = react(i,1)
     ! The effective radm (or radp) coefficient is computed in PXDR_TRANSFER.f90
     rate1 = gamm(i) * EXP(-bet(i) * av) * radm
     rate2 = gamm(i) * EXP(-bet(i) * avr) * radp
     rate = rate1 + rate2

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (7) photoreactions with direct integration (nphot_i)    (s-1)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + nphot_i
  DO i = ideb, ifin
     rate = pdiesp(i-ideb+1,iopt)

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (8) 3 body reactions (n3body)
!-------------------------------------------------------------------------

  ideb = ifin + 1
  ifin = ifin + n3body
  DO i = ideb, ifin
     ir1 = react(i,1)
     ir2 = react(i,2)
     ir3 = react(i,3)
     expon = EXP(-bet(i)/tgaz(iopt))
     rate = gamm(i) * (tgaz(iopt)/300.0_dp)**alpha(i) * expon

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate
     kkk = indy(ir2) + 1
     indy(ir2) = kkk
     xmatk(ir2)%fdt(kkk)%k = - rate
     kkk = indy(ir3) + 1
     indy(ir3) = kkk
     xmatk(ir3)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (101...) exceptions (ncpart)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + ncpart
  DO i = ideb, ifin
     ir1 = react(i,1)
     ir2 = react(i,2)

     ! 101: N+ + H2:
     IF (ir1 == i_h2 .AND. ir2 == i_np) THEN
        rate = gamm(i) * (h2para * EXP(-bet(i) / tgaz(iopt)) + h2orth) * ((tgaz(iopt) / 300.0_dp)**alpha(i))

     ! 102: H3+ + HD:
     ELSE IF (ir1 == i_h2 .AND. ir2 == i_h2dp .AND. tgaz(iopt)>= 20.0_dp)   THEN
        rate = gamm(i) / 10**(62.481_dp *tgaz(iopt)**(-0.94871_dp))
     ELSE IF (ir1 == i_h2 .AND. ir2 == i_h2dp .AND. tgaz(iopt)< 20.0_dp) THEN
        rate = gamm(i) / (6.3407e35_dp * tgaz(iopt) **(-25.203_dp))

     ! 103: O+ + H:
     ELSE IF (ir1 == i_h .AND. ir2 == i_op) THEN
        rate = (gamm(i) + alpha(i) * tgaz(iopt)) * EXP(-bet(i) / tgaz(iopt))

     ! 104: H+ + O:
     ELSE IF (ir1 == i_o .AND. ir2 ==i_hp) THEN
        rate = (gamm(i) + alpha(i) * tgaz(iopt)) * EXP(-bet(i) / tgaz(iopt))

     ! 105: reaction EXP(-T/E)  (formation de H- : Black)
     ELSE IF (itype(i) == 105) THEN
        rate = gamm(i) * (tgaz(iopt)/300.0_dp)**alpha(i) * EXP(-tgaz(iopt)/bet(i))

     ! 106: cl + h2 -> hcl + h :
     ELSE IF (itype(i) == 106 .AND. tgaz(iopt)< 354.0_dp) THEN
        rate = 2.52e-11_dp * EXP(-2214.0_dp / tgaz(iopt))
     ELSE IF (itype(i) == 106 .AND. tgaz(iopt)>= 354.0_dp)   THEN
        rate = 2.86e-11_dp * (tgaz(iopt)/300.0_dp)**1.72_dp * EXP(-1544.0_dp / tgaz(iopt))

     ! 107: hcl + h  -> cl + h2 :
     ELSE IF (itype(i) == 107 .AND. tgaz(iopt)< 354.0_dp) THEN
        rate = 1.49e-11_dp * EXP(-1763.0_dp / tgaz(iopt))
     ELSE IF (itype(i) == 107 .AND. tgaz(iopt)>= 354.0_dp)   THEN
        rate = 1.69e-12_dp * (tgaz(iopt)/300.0_dp)**1.72_dp * EXP(-1093.0_dp / tgaz(iopt))

     ! 108: f + h2 -> hf + h :
     ELSE IF (itype(i) == 108) THEN
     ! The rate below can become negative for large values of T.
     ! rate = 8.8909e-13_dp + 4.6259e-14_dp * tgaz(iopt) + 3.2187e-16_dp * tgaz(iopt)**2 &
     !      - 4.1299e-19_dp * tgaz(iopt)**3
     ! Expression from Neufeld 2009
       rate = 1.0e-10_dp * ( exp(-450.0_dp/tgaz(iopt)) + 0.078_dp*exp(-80.0_dp/tgaz(iopt)) &
            + 0.01555_dp*exp(-10.0_dp/tgaz(iopt)) )

     ELSE
        rate = 0.0_dp
     ENDIF

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate

     kkk = indy(ir2) + 1
     indy(ir2) = kkk
     xmatk(ir2)%fdt(kkk)%k = - rate
  ENDDO

!       ---------------------
!-- 2 - Reactions with grains
!       ---------------------

   nsp_n1 = nsp_ne + nsp_ip + nsp_im + nsp_cs + nsp_gm
!-------------------------------------------------------------------------
! (11) reactions on grain surface (ngsurf)
!-------------------------------------------------------------------------
! 10 Feb 2008 - Those reaction are built for only one product
!               This could be changed later

  ideb = ifin + 1
  ifin = ifin + ngsurf
  DO i = ideb, ifin
     ir1 = react(i,1)
     ir2 = react(i,2)

     IF (itype(i) == 11) THEN

        nu0_2 = SQRT(2.0_dp*xk*t_evap(ir2)/(xmh*mol(ir2))) / (xpi * dsite)
        fr_free = MIN(dsite**2 / (s_gr_t * densh(iopt)), 1.0_dp / abnua(ir1,iopt))

        rate = 0.0_dp
        iprod = react(i,4)
        DO mm = 1, npg
           iv2 = nspec + (ir2 - nsp_n1 - 1) * npg + mm

           tgr = tdust(mm,iopt)
           xldb_2 =  2.0_dp * xpi * SQRT(2.0_dp * xmh * mol(ir2) * xk * (t_diff(ir2) - tgr)) / xhp
           tm1_2 = nu0_2 * (EXP(-t_diff(ir2) / tgr) + 1.0_dp / (1.0_dp + t_diff(ir2)**2 &
                 * SINH(xldb_2 * dsite)**2 / (4.0_dp * tgr * (t_diff(ir2) - tgr))))

           rate = gamm(i) * tm1_2 * fr_free
           rate = rate * EXP(-bet(i)/tgr)

           kkk = indy(iprod) + 1
           indy(iprod) = kkk
           xmatk(iprod)%fdt(kkk)%k = rate * agrnrm * wwmg(mm) * densh(iopt)

           kkk = indy(ir1) + 1
           indy(ir1) = kkk
           xmatk(ir1)%fdt(kkk)%k = - rate * agrnrm * wwmg(mm) * densh(iopt)

           kkk = indy(iv2) + 1
           indy(iv2) = kkk
           xmatk(iv2)%fdt(kkk)%k = - rate
        ENDDO

     ELSE IF (itype(i) == 111) THEN

        nu0_1 = SQRT(2.0_dp*xk*t_evap(ir1)/(xmh*mol(ir1))) / (xpi * dsite)
        nu0_2 = SQRT(2.0_dp*xk*t_evap(ir2)/(xmh*mol(ir2))) / (xpi * dsite)

        iprod = react(i,4)
        DO mm = 1, npg
           iv1 = nspec + (ir1 - nsp_n1 - 1) * npg + mm
           iv2 = nspec + (ir2 - nsp_n1 - 1) * npg + mm

           tgr = tdust(mm,iopt)
           xldb_1 =  2.0_dp * xpi * SQRT(2.0_dp * xmh * mol(ir1) * xk * (t_diff(ir1) - tgr)) / xhp
           xldb_2 =  2.0_dp * xpi * SQRT(2.0_dp * xmh * mol(ir2) * xk * (t_diff(ir2) - tgr)) / xhp

           tm1_1 = nu0_1 * (EXP(-t_diff(ir1) / tgr) + 1.0_dp / (1.0_dp + t_diff(ir1)**2 &
                 * SINH(xldb_1 * dsite)**2 / (4.0_dp * tgr * (t_diff(ir1) - tgr))))
           tm1_2 = nu0_2 * (EXP(-t_diff(ir2) / tgr) + 1.0_dp / (1.0_dp + t_diff(ir2)**2 &
                 * SINH(xldb_2 * dsite)**2 / (4.0_dp * tgr * (t_diff(ir2) - tgr))))

           ! This should be 1 / (8 * pi) (see notes)
           rate = gamm(i) * (tm1_1 + tm1_2) * dsite**2 / (8.0_dp * xpi * adust(mm)**2)
           rate = rate * EXP(-bet(i)/tgr)

           kkk = indy(iprod) + 1
           indy(iprod) = kkk
           xmatk(iprod)%fdt(kkk)%k = rate * agrnrm * wwmg(mm) * densh(iopt)

           kkk = indy(iv1) + 1
           indy(iv1) = kkk
           xmatk(iv1)%fdt(kkk)%k = - rate

           kkk = indy(iv2) + 1
           indy(iv2) = kkk
           xmatk(iv2)%fdt(kkk)%k = - rate
        ENDDO
     ENDIF
  ENDDO

!-------------------------------------------------------------------------
! (12) photoreactions on grain surface (ngphot)
!-------------------------------------------------------------------------
!  11 Feb 2008: not revised yet. Do not use

  ideb = ifin + 1
  ifin = ifin + ngphot
  DO i = ideb, ifin
     rate1 = gamm(i) * EXP(-bet(i)*av) * radm
     rate2 = gamm(i) * EXP(-bet(i)*avr) * radp
     rate = rate1 + rate2

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (13) adsorption on to grains (ngads)
!-------------------------------------------------------------------------

  ideb = ifin + 1
  ifin = ifin + ngads
  DO i = ideb, ifin
     ir1 = react(i,1)
     ! Same sticking probability for all species (not only for H and D)
     CALL STICKING(iopt, stickH)
     gamm(i) = stickH

     IF (itype(i) == 13) THEN

        rate = gamm(i) * vmaxw * SQRT(tgaz(iopt) / mol(ir1)) * s_gr_t * densh(iopt) / 4.0_dp

        iprod = react(i,4)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate

        kkk = indy(ir1) + 1
        indy(ir1) = kkk
        xmatk(ir1)%fdt(kkk)%k = - rate

     ELSE IF (itype(i) == 113) THEN

        rate = gamm(i) * vmaxw * SQRT(tgaz(iopt) / mol(ir1)) * s_gr_t * densh(iopt) / 4.0_dp

        kkk = indy(ir1) + 1
        indy(ir1) = kkk
        xmatk(ir1)%fdt(kkk)%k = - rate

        r_rej  = gamm(i) * vmaxw * SQRT(tgaz(iopt) / mol(ir1)) * dsite*dsite / 4.0_dp
        r_ad_i = gamm(i) * vmaxw * SQRT(tgaz(iopt) / mol(ir1)) * xpi

        iprod = react(i,4)
        DO mm = 1, npg
           ivp = nspec + (iprod - nsp_n1 - 1) * npg + mm
           kkk = indy(ivp) + 1
           indy(ivp) = kkk
           xmatk(ivp)%fdt(kkk)%k = r_ad_i * adust(mm)**2
           kkk = indy(ivp) + 1
           indy(ivp) = kkk
           xmatk(ivp)%fdt(kkk)%k = - r_rej

           kkk = indy(ir1) + 1
           indy(ir1) = kkk
           xmatk(ir1)%fdt(kkk)%k = r_rej * agrnrm * densh(iopt) * wwmg(mm)
        ENDDO
     ENDIF
  ENDDO

!-------------------------------------------------------------------------
! (14) neutralization (ngneut)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + ngneut

  DO i = ideb, ifin
     ir1 = react(i,1)
     rate = rxigr(ir1,iopt)

     iprod = react(i,4)
     kkk = indy(iprod) + 1
     indy(iprod) = kkk
     xmatk(iprod)%fdt(kkk)%k = rate

     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------
! (15) explosive desorption from grains (nggdes)
!    ref: Shalabiea & Greenberg 1994
!-------------------------------------------------------------------------
!  11 Feb 2008: not revised yet. Do not use

  ideb = ifin + 1
  ifin = ifin + nggdes
  DO i = ideb, ifin
     ir1 = react(i,1)
     rate1 = gamm(i) * rdeso * densh(iopt)
     rate2 = gamm(i) * 3.0d4 * zeta * (signgr / 7.0e-21_dp)
     rate = rate1 + rate2

     IF (itype(i) == 15) THEN
        iprod = react(i,4)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate

        kkk = indy(ir1) + 1
        indy(ir1) = kkk
        xmatk(ir1)%fdt(kkk)%k = - rate
     ENDIF
  ENDDO

!-------------------------------------------------------------------------
! (16) cosmic ray induced desorption from grains (ncrdes)
!      (sans les photons secondaires)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + ncrdes
  DO i = ideb, ifin
     rate = gamm(i) * zeta * dsite**2 / 4.0_dp
     ir1 = react(i,1)
     iprod = react(i,4)

     IF (itype(i) == 16) THEN

        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate

        kkk = indy(ir1) + 1
        indy(ir1) = kkk
        xmatk(ir1)%fdt(kkk)%k = - rate

     ELSE IF (itype(i) == 116) THEN

        DO mm = 1, npg
           ivp = nspec + (ir1 - nsp_n1 - 1) * npg + mm
           kkk = indy(ivp) + 1
           indy(ivp) = kkk
           xmatk(ivp)%fdt(kkk)%k = - rate

           kkk = indy(iprod) + 1
           indy(iprod) = kkk
           xmatk(iprod)%fdt(kkk)%k = rate * agrnrm * densh(iopt) * wwmg(mm)
        ENDDO
     ENDIF
  ENDDO

!-------------------------------------------------------------------------
! (17) photodesorption from grains (nphdes)
!      (les photons secondaires sont isotropes, pas les photons incidents)
!-------------------------------------------------------------------------
!  flux de photons secondaires due aux rayons cosmiques:
!     1E3 cm-2 s-1 (Roberge, in Dalgarno's book)
!     fphsec = 1.0e3_dp * (zeta / 1.0e-17_dp) = 1.0e3_dp * fmrc

!  flux de Mathis et al de 911 a 2400 A (avec F(l) = 5e-17 erg cm-3 A-1)
!  flupho = 1e8 photon cm-2 s-1      (Tielens & Hagen, 1982, A&A 114, 245)
!     flupho = 1.0d8
!     flupho = flupho * RAD * EXP(-2.0_dp * AV)

  ideb = ifin + 1
  ifin = ifin + nphdes
  DO i = ideb, ifin
     rate = gamm(i) * (fphsec * frach2 + fluph(iopt)) * dsite**2 / 4.0_dp
     ir1 = react(i,1)
     iprod = react(i,4)

     IF (itype(i) == 17) THEN

        kkk = indy(ir1) + 1
        indy(ir1) = kkk
        xmatk(ir1)%fdt(kkk)%k = - rate

        ik = react(i,0)
        DO j = 4, ik+1
           iprod = react(i,j)
           kkk = indy(iprod) + 1
           indy(iprod) = kkk
           xmatk(iprod)%fdt(kkk)%k = rate
        ENDDO

     ELSE IF (itype(i) == 117) THEN

        DO mm = 1, npg
           ivp = nspec + (ir1 - nsp_n1 - 1) * npg + mm
           kkk = indy(ivp) + 1
           indy(ivp) = kkk
           xmatk(ivp)%fdt(kkk)%k = - rate

           ik = react(i,0)
           DO j = 4, ik+1
              iprod = react(i,j)
              kkk = indy(iprod) + 1
              indy(iprod) = kkk
              xmatk(iprod)%fdt(kkk)%k = rate * agrnrm * densh(iopt) * wwmg(mm)
           ENDDO
        ENDDO
     ENDIF
  ENDDO

!-------------------------------------------------------------------------
! (18) evaporation from grains (ngevap)
!-------------------------------------------------------------------------
  ideb = ifin + 1
  ifin = ifin + ngevap
  DO i = ideb, ifin
     ir1 = react(i,1)
     iprod = react(i,4)
     nu0 = SQRT(2.0_dp*xk*t_evap(ir1)/(xmh*mol(ir1))) / (xpi * dsite)

     IF (itype(i) == 18) THEN

        summ = 0.0_dp
        DO mm = 1, npg
!          tgr = MIN(tdust(mm,iopt), 25.0_dp)
           tgr = tdust(mm,iopt)
           summ = summ + wwmg(mm) * adust(mm)**2 * EXP(-t_evap(ir1) / tgr)
        ENDDO
!       Alternative expression (Evelyne) => no divide by zero for rmin = rmax
        rate = nu0 * summ * 4.0_dp * xpi * agrnrm / s_gr_t

        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate

        kkk = indy(ir1) + 1
        indy(ir1) = kkk
        xmatk(ir1)%fdt(kkk)%k = - rate

     ELSE IF (itype(i) == 118) THEN

        DO mm = 1, npg
           ! Remove limitation in tgr. This was used when only LH was
           ! implemented
           ! tgr = MIN(tdust(mm,iopt), 25.0_dp)
           tgr = tdust(mm,iopt)
           rate = nu0 * EXP(-t_evap(ir1) / tgr)
           ivp = nspec + (ir1 - nsp_n1 - 1) * npg + mm
           kkk = indy(ivp) + 1
           indy(ivp) = kkk
           xmatk(ivp)%fdt(kkk)%k = - rate

           kkk = indy(iprod) + 1
           indy(iprod) = kkk
           xmatk(iprod)%fdt(kkk)%k = rate * agrnrm * densh(iopt) * wwmg(mm)
        ENDDO
     ENDIF
  ENDDO

!-------------------------------------------------------------------------
! (19) Chemisorption on grains (ngchs)
!-------------------------------------------------------------------------

  ideb = ifin + 1
  ifin = ifin + ngchs
  DO i = ideb, ifin
     ! stick_ER = 1.0_dp / (1.0_dp + 1.0e-4_dp * tgaz(iopt)**1.5_dp)
     stick_ER = 1.0_dp / (1.0_dp + (tgaz(iopt)/T2_stick_ER)**ex_stick_ER)
     ir1 = react(i,1)

     expon = EXP(-bet(i)/tgaz(iopt))
     rate = stick_ER * gamm(i) * vmaxw * SQRT(tgaz(iopt) / mol(ir1)) * s_gr_t * densh(iopt) * expon / 4.0_dp
     r_rej = stick_ER * gamm(i) * vmaxw * SQRT(tgaz(iopt) / mol(ir1)) * dsite**2 *expon / 4.0_dp

     iprod = react(i,4)
     kkk = indy(iprod) + 1
     indy(iprod) = kkk
     xmatk(iprod)%fdt(kkk)%k = rate

     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate

     ! Rejection
     kkk = indy(iprod) + 1
     indy(iprod) = kkk
     xmatk(iprod)%fdt(kkk)%k = - r_rej
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = r_rej
  ENDDO

!-------------------------------------------------------------------------
! (20) Eley-Rideal process (ngelri)   (cm3 s-1)
!-------------------------------------------------------------------------
! Note : the total number of sites is: s_gr_t * densh(iopt) / dsite**2

  ideb = ifin + 1
  ifin = ifin + ngelri
  DO i = ideb, ifin

     rate = gamm(i) * vmaxw * SQRT(tgaz(iopt) / mol(ir1)) * dsite**2 / 4.0_dp

     ik = react(i,0)
     DO j = 4, ik+1
        iprod = react(i,j)
        kkk = indy(iprod) + 1
        indy(iprod) = kkk
        xmatk(iprod)%fdt(kkk)%k = rate
     ENDDO

     ir1 = react(i,1)
     kkk = indy(ir1) + 1
     indy(ir1) = kkk
     xmatk(ir1)%fdt(kkk)%k = - rate

     ir2 = react(i,2)
     kkk = indy(ir2) + 1
     indy(ir2) = kkk
     xmatk(ir2)%fdt(kkk)%k = - rate
  ENDDO

!-------------------------------------------------------------------------

END SUBROUTINE XKFILL

END MODULE PREP_AUTRE
