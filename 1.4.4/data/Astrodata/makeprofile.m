clear all
close all
clc

l = 9.5; % Length in arcsec.
d = 2.3e3; % Distance in pc.
pc2cm = 1.5e13/tand(1/3600); % 1 parsec in cm.
rd = tand(l/2/3600)*d*pc2cm;

n0 = 1e3; % Density in cm^-3 at the IF.
nd = 5e6; % Density in cm^-3 at the disk surface.
nstop = 1.01*5e6; % Maximum density in cm^-3 in the profile.
R = rd/log(nd/n0); % Characteristic length in cm.

nbpts = 1000;
% r = linspace(0,rd,nbpts);
% r = r(:);
% n = n0*exp(r/R);
% plot(r,n)
n = linspace(n0,nstop,nbpts);
n = n(:);
r = R*log(n/n0);

T = ones(size(r))*100;

% Av = zeros(size(r));
% for i=1:length(r)
%     nH = trapz(r(1:i),n(1:i));
%     Av(i) = nH/2.5e21;
% end
Av = (n.*R)/2.5e21;
Avlim = nd*R/2.5e21;

fileID = fopen('expprofile.pfl','w');
fprintf(fileID,'%5u\n',nbpts);
for i=1:length(r)
    fprintf(fileID,'%21.10E %19.10E %19.10E \n',Av(i),T(i),n(i));
end
fclose(fileID);

