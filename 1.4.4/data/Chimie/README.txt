=== Chemistry files ===
This directory contains several chemistry files.

One of these chemistry files has to be selected in pdr6.in
Users should pay attention to :
- modify the elementary abundances 
- select a chemistry file compatible with the ISRF selected in pdr6.in (Mathis or Draine)

*** Notations
ch              : chemistry
1109            : 2011 september
cnos(d)         : Carbon, nitrogen, oxygen, sulfur, (deuterium) 
ER              : Formation of H2 by Langmuir-Hinshelwood + Eley-Rideal by default
Mathis / Draine : photo-reactions cross sections provided for Mathis or Draine ISRF

*** References
These chemical networks have been build from different sources :
* OSU database    : http://www.physics.ohio-state.edu/~eric/research.html
* KIDA database   : http://kida.obs.u-bordeaux1.fr
* UMIST database  : http://www.udfa.net
* LAMDA database  : http://www.strw.leidenuniv.nl/~ewine/photo
* several articles



