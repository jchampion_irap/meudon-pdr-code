
MODULE IN_OUT

  USE CONSTANTS
!  USE EXTENSION
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: READ_DATA, WRITE_DATA, READ_COM, EXTINCTION

CONTAINS

SUBROUTINE READ_DATA
! reads the .DAT files

  ! global variables modules
  USE CONSTANTS
  USE UTILITY
  USE MGET_QEXT
  USE MGET_TDIST

  IMPLICIT NONE

  ! local variables
  CHARACTER (LEN=max_len)     :: filename_tmp
  CHARACTER (LEN=max_len)     :: c_tmp
  CHARACTER (LEN=max_len)     :: the_char

  ! data files
  TYPE (FICH)                 :: fgrain     = FICH ('GRAIN.DAT', 11)
  TYPE (FICH)                 :: fmix       = FICH ('MIX_', 12)
  TYPE (FICH)                 :: fpol       = FICH ('POL_', 13)
  TYPE (FICH)                 :: fsize      = FICH ('SIZE_', 14)
  TYPE (FICH)                 :: fspz       = FICH ('SPZ_', 15)
  TYPE (FICH)                 :: fisrf      = FICH ('ISRF.DAT', 16)
  TYPE (FICH)                 :: fgas       = FICH ('GAS.DAT', 17)
  TYPE (FICH)                 :: fcalor     = FICH ('C_', 19)
  TYPE (FICH)                 :: fbeta      = FICH ('BETA_', 20)

  ! wavelength dependent data files
  TYPE (FICH)                 :: flamb_qabs = FICH ('LAMBDA.DAT', 50)
  TYPE (FICH)                 :: fqext      = FICH ('Q_', 51)
  TYPE (FICH)                 :: fgfac      = FICH ('G_', 52)

  INTEGER                     :: nmat             ! nr of grain material in a composite grain
  INTEGER                     :: i, j, k, u       ! loop indices
  INTEGER                     :: n_gas            ! check nr to read GAS.DAT only first time
  REAL (KIND=dp)              :: da, aux, argu
  REAL (KIND=dp)              :: xx(nsize_max), yy(nsize_max)
  INTEGER                     :: ns_i
  REAL (KIND=dp), ALLOCATABLE :: lambisrf(:)      ! les longueurs d'ondes (lues en micron)
  REAL (KIND=dp), ALLOCATABLE :: isrf(:)          ! radiation field
  INTEGER                     :: fl_freq
  REAL (KIND=dp), ALLOCATABLE :: tmp1(:),tmp2(:), tmp3(:)

  !=====================================================
  ! init run keywords
  n_ftemp = 0
  n_pdr_o = 0
  n_quiet = 0
  n_res_a = 0

  ! init type counters
  n_spin = 0
  n_beta = 0

  ! fichier GRAIN.DAT
  !-----------------------------------------------------------------
!  CALL GETENVIRONMENT("DUSTEM_DATA_PATH",data_path)
  filename_tmp = TRIMCAT (data_path, dir_dat)
  filename_tmp = TRIMCAT (filename_tmp, fgrain%nom)
  OPEN (UNIT = fgrain%unit, FILE = filename_tmp, STATUS = 'old')
  the_char = READ_COM (fgrain%unit)
  the_char = UPCASE ( TRIM (the_char) )

  ! get global keywords (type insensitive)
  n_res_a = INDEX (the_char, 'RES_A')
  n_ftemp = INDEX (the_char, 'TEMP')
  n_quiet = INDEX (the_char, 'QUIET')
  n_pdr_o = INDEX (the_char, 'PDR')

  IF (n_quiet == 0) THEN
     WRITE (*,*) '==========================================='
     WRITE (*,*) '             Running DUSTEM'
     WRITE (*,*) ''
     WRITE (*, FMT='(A18)')' >> read GRAIN.DAT'
  ENDIF

  ! get G0 factor for radiation field
  READ (UNIT=fgrain%unit, FMT=*) g0
  IF (n_quiet == 0) THEN
     WRITE (*, FMT='(A12,1X,A20,1x,A3,1x,1PE9.2)') 'Run Keywords', the_char, 'G0=', g0
     WRITE (*, FMT='(25x,A93)') 'nsize        t_opt             M_dust/M_H  rho(g/cm3)      a-range(cm)      alpha-a0    sigma'
  ENDIF

  READ (UNIT=fgrain%unit, FMT=*) ntype
  ALLOCATE (gtype(ntype))
  ALLOCATE (mprop(ntype))
  ALLOCATE (rhom(ntype))
  ALLOCATE (as1(ntype))
  ALLOCATE (as2(ntype))
  ALLOCATE (alpha(ntype))
  ALLOCATE (sig(ntype))
  ALLOCATE (nsize(ntype))
  ALLOCATE (t_opt(ntype))
  ALLOCATE (size_ava(ntype,nsize_max))
  ALLOCATE (si_ava_l(ntype,nsize_max))
  ALLOCATE (sect_eff(ntype,nsize_max))
  ALLOCATE (ava(ntype,nsize_max))
  ALLOCATE (dloga(ntype,nsize_max))
  ALLOCATE (rho(ntype,nsize_max))
  ALLOCATE (f_mix(ntype,nsize_max))
  ALLOCATE (f_pol(ntype,nsize_max))
  ALLOCATE (beta0(ntype),abeta(ntype),gbeta(ntype),bmax(ntype))
  ALLOCATE (nbeta(ntype),tbeta(ntype,nbeta_max),betav(ntype,nbeta_max))
  ALLOCATE (ltresh(ntype),lstiff(ntype))

  n_gas = 0
  DO i=1,ntype
     READ (UNIT=fgrain%unit,FMT=*) gtype(i), mprop(i), rhom(i), as1(i), as2(i), alpha(i), sig(i), nsize(i), c_tmp
     t_opt(i) = UPCASE(TRIM(c_tmp))
     IF (n_quiet == 0) THEN
!       WRITE (*, FMT='(A20,5x,1P,E9.2,3x,E9.2,3x,2(E9.2,1x),2(E9.2,1x),2x,I3,5X,A20)') &
        WRITE (*, *) &
             & nsize(i), t_opt(i), gtype(i), mprop(i), rhom(i), as1(i), as2(i), alpha(i), sig(i)
     ENDIF

     ! get the size distribution from GRAIN.DAT parameters
     IF (INDEX(t_opt(i),'SIZE') == 0) THEN
        n_fsize = INDEX(t_opt(i),'PLAW') + INDEX(t_opt(i),'LOGN')
        IF (n_fsize == 0) THEN
           WRITE(*,*)'(F) DM_inout/READ_DATA:',TRIM(gtype(i)),' undefined size distribution '
           STOP
        ENDIF

        IF (INDEX(t_opt(i),'PLAW') > 0 ) THEN
           IF (nsize(i) /= 1) THEN
              da = ( LOG(as2(i)) - LOG(as1(i)) ) / DBLE(nsize(i)-1)
           ELSE
              da = LOG(as2(i)) - LOG(as1(i))
           ENDIF
           DO j=1,nsize(i)
              aux = LOG(as1(i)) + DBLE(j-1) * da
              argu = (4.0_dp + alpha(i)) * aux
              IF (argu > -350.0_dp) THEN
                 ava(i,j) = EXP(argu)
              ELSE
                 ava(i,j) = 0.0_dp
              ENDIF
              si_ava_l(i,j) = aux
              size_ava(i,j) = EXP(aux)
              sect_eff(i,j) = xpi * size_ava(i,j)**2
              dloga(i,j) = da
              rho(i,j) = rhom(i)
              f_mix(i,j) = 1.0_dp
              f_pol(i,j) = 1.0_dp
           ENDDO
           IF (size_ava(i,nsize(i)) /= as2(i)) THEN ! correct rounding
              aux = LOG(as2(i))
              argu = (4.0_dp + alpha(i)) * aux
              IF (argu > -350.0_dp) THEN
                 ava(i,nsize(i)) = EXP(argu)
              ELSE
                 ava(i,nsize(i)) = 0.0_dp
              ENDIF
              si_ava_l(i,nsize(i)) = aux
              size_ava(i,nsize(i)) = EXP(aux)
              sect_eff(i,nsize(i)) = xpi * size_ava(i,nsize(i))**2
           ENDIF

        ELSE IF (INDEX(t_opt(i),'LOGN') > 0 ) THEN
           IF ((sig(i) == 0.0_dp) .OR. (alpha(i) == 0.0_dp)) THEN
              WRITE(*,*)'(F) DM_inout/READ_DATA:',TRIM(gtype(i)),' centroid or sigma of log-normal cannot be 0'
              STOP
           ENDIF
           IF (nsize(i) /= 1) THEN
              da = ( LOG(as2(i)) - LOG(as1(i)) ) / DBLE(nsize(i)-1)
           ELSE
              da = LOG(as2(i)) - LOG(as1(i))
           ENDIF
           DO j=1,nsize(i)
              aux = LOG(as1(i)) + DBLE(j-1) * da
              argu = 3.0_dp*aux - 0.5_dp * ( (aux - LOG(alpha(i))) / sig(i) )**2
              IF (argu > -350.0_dp) THEN
                 ava(i,j) = EXP(argu)
              ELSE
                 ava(i,j) = 0.0_dp
              ENDIF
              si_ava_l(i,j) = aux
              size_ava(i,j) = EXP(aux)
              sect_eff(i,j) = xpi * size_ava(i,j)**2
              dloga(i,j) = da
              rho(i,j) = rhom(i)
              f_mix(i,j) = 1.0_dp
              f_pol(i,j) = 1.0_dp
           ENDDO
           IF (size_ava(i,nsize(i)) /= as2(i)) THEN ! correct rounding
              aux = LOG(as2(i))
              argu = 3.0_dp*aux - 0.5_dp * ( (aux - LOG(alpha(i))) / sig(i) )**2
              IF (argu > -350.0_dp) THEN
                 ava(i,nsize(i)) = EXP(argu)
              ELSE
                 ava(i,nsize(i)) = 0.0_dp
              ENDIF
              si_ava_l(i,nsize(i)) = aux
              size_ava(i,nsize(i)) = EXP(aux)
              sect_eff(i,nsize(i)) = xpi * size_ava(i,nsize(i))**2
           ENDIF
        ENDIF

     ELSE IF (INDEX(t_opt(i),'SIZE') > 0) THEN

        ALLOCATE (geom(ntype), boa(ntype))

       ! open SIZE_TYPE.DAT
        filename_tmp = TRIMCAT (data_path, dir_dat)
        filename_tmp = TRIMCAT (filename_tmp, fsize%nom)
        filename_tmp = TRIMCAT (filename_tmp, gtype(i))
        filename_tmp = TRIMCAT (filename_tmp, '.DAT')
        c_tmp = TRIMCAT (fsize%nom, gtype(i))
        c_tmp = TRIMCAT (c_tmp, '.DAT')
        OPEN (UNIT = fsize%unit, FILE = filename_tmp, STATUS = 'old')

        ! read doc
        the_char = READ_COM (fsize%unit)
        READ (the_char, FMT=*) geom(i),boa(i)
        READ (UNIT=fsize%unit, FMT=*) nmat
        ALLOCATE (sbulk(nmat))
        READ (UNIT=fsize%unit, FMT=*) (sbulk(u), u=1,nmat)
        READ (UNIT=fsize%unit, FMT=*) the_char
        READ (UNIT=fsize%unit, FMT=*) the_char

        ! read data
        READ (UNIT=fsize%unit, FMT=*) nsize(i)
        IF (n_quiet == 0) THEN
           WRITE (*, FMT='(A7,2x,A30,A40)') '>>read ',TRIM(c_tmp), &
                & '    a-range (cm)     nsize   geom   boa'
        ENDIF
        DO j=1,nsize(i)
           READ (UNIT=fsize%unit, FMT=*) size_ava(i,j), dloga(i,j), ava(i,j), rho(i,j)
        ENDDO
        si_ava_l(i,1:nsize(i)) = LOG(size_ava(i,1:nsize(i)))
        sect_eff(i,1:nsize(i)) = xpi * size_ava(i,1:nsize(i))**2
        IF (n_quiet == 0) THEN
           c_tmp = 'bulk'
           DO u=1,nmat
              c_tmp = TRIMCAT(c_tmp, '-'//sbulk(u))
           ENDDO
           WRITE (*, FMT='(A39,1P,2(E9.2,1x),1x,I3,8x,i1,2x,1PE9.2)') c_tmp, size_ava(i,1), size_ava(i,nsize(i)), nsize(i), &
                & geom(i), boa(i)
        ENDIF
        CLOSE (UNIT=fsize%unit)
     ENDIF ! size distribution

     ! treat the other type options
     IF (INDEX(t_opt(i),'MIX') > 0) THEN
        filename_tmp = TRIMCAT (data_path, dir_dat)
        filename_tmp = TRIMCAT (filename_tmp, fmix%nom)
        filename_tmp = TRIMCAT (filename_tmp, gtype(i))
        filename_tmp = TRIMCAT (filename_tmp, '.DAT')
        the_char = TRIMCAT (fmix%nom, gtype(i))
        the_char = TRIMCAT (the_char, '.DAT')
        IF (n_quiet == 0) THEN
           WRITE (*, FMT='(A7,2X,A30)') '>>read ', TRIM(the_char)
        ENDIF
        OPEN (UNIT=fmix%unit, FILE=filename_tmp, STATUS='old')
        the_char = READ_COM (fmix%unit)
        READ (the_char, FMT=*) f_mix(i,1)
        DO j=2,nsize(i)
           READ (UNIT=fmix%unit, FMT=*) f_mix(i,j)
        ENDDO
        CLOSE (UNIT=fmix%unit)
     ELSE IF (INDEX(t_opt(i),'MIX') == 0) THEN
        f_mix(i,:) = 1.0_dp
     ENDIF

     ! spinning dust emission
     IF (INDEX(t_opt(i),'SPIN') > 0) THEN
        n_spin = n_spin + 1
        IF (n_gas == 0) THEN
           ! first time get the gas parameters
           filename_tmp = TRIMCAT (data_path, dir_dat)
           filename_tmp = TRIMCAT (filename_tmp, fgas%nom)
           the_char = TRIMCAT (fgas%nom, '.DAT')
           IF (n_quiet == 0) THEN
              WRITE (*, FMT='(A7,2X,A30)') '>>read ', TRIM(the_char)
           ENDIF
           OPEN (UNIT=fgas%unit, FILE=filename_tmp, STATUS='old')
           the_char = READ_COM (fgas%unit)
           READ (the_char, FMT=*) hden
           READ(UNIT=fgas%unit, FMT=*) t_gas
           READ(UNIT=fgas%unit, FMT=*) h2den
           READ(UNIT=fgas%unit, FMT=*) nion
           ALLOCATE (iden(nion))
           ALLOCATE (mi(nion))
           ALLOCATE (pol_i(nion))
           ALLOCATE (zi(nion))
           DO j=1,nion
              READ(UNIT=fgas%unit, FMT=*) iden(j), mi(j), pol_i(j), zi(j)
           ENDDO
           READ(UNIT=fgas%unit, FMT=*) eden
           CLOSE (UNIT=fgas%unit)
           ALLOCATE (m0(ntype))
           n_gas = n_gas + 1
        ENDIF

        ! get the spin and charge parameters
        filename_tmp = TRIMCAT (data_path, dir_dat)
        filename_tmp = TRIMCAT (filename_tmp, fspz%nom)
        filename_tmp = TRIMCAT (filename_tmp, gtype(i))
        filename_tmp = TRIMCAT (filename_tmp, '.DAT')
        the_char = TRIMCAT (fspz%nom, gtype(i))
        the_char = TRIMCAT (the_char, '.DAT')
        IF (n_quiet == 0) THEN
           WRITE (*, FMT='(A7,2X,A30)') '>>read ', TRIM(the_char)
        ENDIF
        OPEN (UNIT=fspz%unit, FILE=filename_tmp, STATUS='old')
        the_char = READ_COM (fspz%unit)
        READ (the_char, FMT=*) m0(n_spin)
        CLOSE (UNIT=fspz%unit)
     ENDIF

     ! beta(T)-correction
     IF (INDEX(t_opt(i),'BETA') > 0) THEN
        n_beta = n_beta + 1
        filename_tmp = TRIMCAT (data_path, dir_dat)
        filename_tmp = TRIMCAT (filename_tmp, fbeta%nom)
        filename_tmp = TRIMCAT (filename_tmp, gtype(i))
        filename_tmp = TRIMCAT (filename_tmp, '.DAT')
        the_char = TRIMCAT (fbeta%nom, gtype(i))
        the_char = TRIMCAT (the_char, '.DAT')
        IF (n_quiet == 0) THEN
           WRITE (*,FMT='(A7,2X,A30)')'>>read ', TRIM(the_char)
        ENDIF
        OPEN (UNIT=fbeta%unit, FILE=filename_tmp, STATUS='old')
        the_char = READ_COM (fbeta%unit)
        READ (the_char,FMT=*) beta0(i), abeta(i), gbeta(i), bmax(i)
        READ (UNIT=fbeta%unit, FMT=*) ltresh(i), lstiff(i)
        READ (UNIT=fbeta%unit, FMT=*) nbeta(i)
        IF (nbeta(i) > 0) THEN
           DO k=1, nbeta(i)
              READ (UNIT=fbeta%unit, FMT=*) tbeta(i,k), betav(i,k)
           ENDDO
        ENDIF
        CLOSE (UNIT=fbeta%unit)
        ltresh(i) = ltresh(i) * 1.00e-4_dp   ! microns to cm
     ENDIF

     ! polarisation
     IF (INDEX(t_opt(i),'POL') > 0) THEN
        filename_tmp = TRIMCAT (data_path, dir_dat)
        filename_tmp = TRIMCAT (filename_tmp, fpol%nom)
        filename_tmp = TRIMCAT (filename_tmp, gtype(i))
        filename_tmp = TRIMCAT (filename_tmp, '.DAT')
        the_char = TRIMCAT (fpol%nom, gtype(i))
        the_char = TRIMCAT (the_char, '.DAT')
        IF (n_quiet == 0) THEN
           WRITE (*,FMT='(A7,2X,A30)')'>>read ', TRIM(the_char)
        ENDIF
        OPEN (UNIT=fpol%unit, FILE=filename_tmp, STATUS='old')
        the_char = READ_COM (fpol%unit)
        READ (the_char,FMT=*) f_pol(i,1)
        DO j=2,nsize(i)
           READ (UNIT=fpol%unit, FMT=*) f_pol(i,j)
        ENDDO
        CLOSE (UNIT=fpol%unit)
     ELSE IF (INDEX(t_opt(i),'POL') == 0) THEN
        f_pol(i,:) = 1.0_dp
     ENDIF

  ENDDO   ! type loop
  CLOSE (UNIT = fgrain%UNIT)

  ! reading heat capacities (C_*.DAT)
  !-----------------------------------------------------------------
  ALLOCATE (nsize_type(ntype),nsz1(ntype))
  ALLOCATE (size_type(ntype,nsize_max_qabs))
  ALLOCATE (calor(ntype,nsize_max_qabs,ntempmax))
  ALLOCATE (temp_cal(ntype,ntempmax))
  ALLOCATE (n_temp(ntype))

  ! initialize
  temp_cal(:,:) = 0.0_dp
  calor(:,:,:) = 0.0_dp

  IF (n_quiet == 0) THEN
     WRITE (*,*)
     WRITE (*, FMT='(A80)') '>>read C_TYPE.DAT            a-range (cm)     nsize       T-range (K)      NTEMP'
  ENDIF

  DO i=1,ntype

     ! open file
     filename_tmp = TRIMCAT (data_path, dir_capa)
     filename_tmp = TRIMCAT (filename_tmp, fcalor%nom)
     filename_tmp = TRIMCAT (filename_tmp, gtype(i))
     filename_tmp = TRIMCAT (filename_tmp, '.DAT')
     OPEN ( UNIT=fcalor%unit,FILE =filename_tmp,STATUS='old' )

     ! read doc
     the_char = READ_COM(fcalor%unit)
     READ (the_char, FMT=*) nsize_type(i)

     ! checking C file for nr of sizes
     IF (nsize_type(i) > nsize_max_qabs) THEN
        WRITE (*,*) ''
        WRITE (*,*) '(F) DM_inout/READ_DATA: too many sizes required in ', filename_tmp
        WRITE (*,*) '                        nsize_max_qabs=', nsize_max_qabs, ' while nsize_type=', nsize_type(i)
        WRITE (*,*) '                        nsize_max_qabs PARAMETER can be changed in DM_utility.f90'
        WRITE (*,*) ''
        STOP
     ENDIF

     ! get sizes
     READ (UNIT = fcalor%unit,FMT = *) (size_type(i,u), u = 1,nsize_type(i))

     ! read data
     READ (UNIT = fcalor%unit,FMT = *) n_temp(i)
     DO k=1,n_temp(i)
        READ (unit = fcalor%unit, FMT = *) temp_cal(i,k), (calor(i,j,k), j=1,nsize_type(i))
     ENDDO
     IF (n_quiet == 0) THEN
        WRITE (*, FMT='((A20,5x,1P,2( 2(E9.2,1x),1x,I3,5X)))') &
               gtype(i), 1.0e-4_dp*size_type(i,1), 1.0e-4_dp*size_type(i,nsize_type(i)), nsize_type(i), &
               10.0_dp**temp_cal(i,1), 10.0_dp**temp_cal(i,n_temp(i)), n_temp(i)
     ENDIF
     CLOSE (UNIT=fcalor%UNIT)
  ENDDO

  ! reading lambda grid for all Q files (LAMBDA.DAT)
  !---------------------------------------------------------------------------------
  filename_tmp = TRIMCAT (data_path, dir_qabs)
  filename_tmp = TRIMCAT (filename_tmp, flamb_qabs%nom)
  OPEN (UNIT=flamb_qabs%unit, FILE=filename_tmp, STATUS='old')
  the_char = READ_COM (flamb_qabs%unit)
  READ (the_char, FMT=*) n_qabs
  ALLOCATE (lamb_qabs(n_qabs))
  ALLOCATE (freq_qabs(n_qabs))
  ALLOCATE (lfrq_qabs(n_qabs))
  ALLOCATE (tmp1(n_qabs))
  ALLOCATE (btresh(ntype,n_qabs))
  DO k=1,n_qabs
    READ (UNIT=flamb_qabs%unit, FMT=*) lamb_qabs(k)
  ENDDO
  IF (n_quiet == 0) THEN
     WRITE (*,*)
     WRITE (*, FMT='(A51)') '>>read LAMBDA.DAT          w-range (microns)  nwave'
     WRITE (*, FMT='(A25,1P,2(E9.2,1x),1x,I4)') '', lamb_qabs(1), lamb_qabs(n_qabs), n_qabs
  ENDIF
  CLOSE (UNIT=flamb_qabs%unit)
  lamb_qabs = lamb_qabs * 1.0e-4_dp              ! Convert from micron to cm
  freq_qabs = clight / lamb_qabs
  ! We sort frequencies in reverse order
  tmp1 = freq_qabs
  DO k=1,n_qabs
     freq_qabs(k) = tmp1(n_qabs-k+1)
  ENDDO
  lfrq_qabs = LOG(freq_qabs)

  ! define threshold for beta-correction
  IF ( n_beta > 0) THEN
     DO i=1,ntype
        IF (INDEX(t_opt(i),'BETA') > 0) THEN
           btresh(i,:) = 0.5_dp * ( 1.0_dp + TANH( 4.0_dp*lstiff(i)*(lamb_qabs(:)/ltresh(i)-1.0_dp)) )
        ENDIF
     ENDDO
  ENDIF

  ! reading Qabs, Qsca (Q_*.DAT) and G_*.DAT
  !---------------------------------------------------------------
  ALLOCATE (q_abs(ntype, nsize_max_qabs, n_qabs))
  ALLOCATE (qdiff(ntype, nsize_max_qabs, n_qabs))
  ALLOCATE (gfac(ntype, nsize_max_qabs, n_qabs))
  ALLOCATE (qi_abs(ntype, nsize_max, n_qabs))
  ALLOCATE (qidiff(ntype, nsize_max, n_qabs))
  ALLOCATE (gifac(ntype, nsize_max, n_qabs))
  ALLOCATE (tmp2(n_qabs))
  ALLOCATE (tmp3(n_qabs))
  ALLOCATE (masstot(ntype))

  ! initialise q_abs, g & size_type
  q_abs(:,:,:)   = 0.0_dp
  qdiff(:,:,:)   = 0.0_dp
  gfac(:,:,:)    = 0.0_dp
  qi_abs(:,:,:)  = 0.0_dp
  qidiff(:,:,:)  = 0.0_dp
  gifac(:,:,:)   = 0.0_dp
  size_type(:,:) = 0.0_dp

  IF (n_quiet == 0) THEN
     WRITE (*,*)
     WRITE (*, FMT='(A50)') '>>read Q_TYPE.DAT           a-range (cm)     nsize'

     IF (n_pdr_o /= 0) THEN
        WRITE (*, FMT='(A50)') '>>read G_TYPE.DAT           a-range (cm)     nsize'
     ENDIF
  ENDIF

  DO i=1,ntype
    filename_tmp = TRIMCAT (data_path, dir_qabs)
    filename_tmp = TRIMCAT (filename_tmp, fqext%nom)
    filename_tmp = TRIMCAT (filename_tmp, gtype(i))
    filename_tmp = TRIMCAT (filename_tmp, '.DAT')
    OPEN (UNIT=fqext%unit, FILE=filename_tmp, STATUS='old')

    ! read doc
    the_char = READ_COM (fqext%unit)
    READ (the_char, FMT=*) nsz1(i)

    ! checking Q file
    IF (nsz1(i) > nsize_max_qabs) THEN
       WRITE (*,*) ''
       WRITE (*,*) '(F) DM_inout/READ_DATA: too many sizes required in ', TRIM(filename_tmp)
       WRITE (*,*) '                        nsize_max_qabs=', nsize_max_qabs, ' while nsize_type=', nsize_type(i)
       WRITE (*,*) '                        nsize_max_qabs PARAMETER can be changed in DM_utility.f90'
       WRITE (*,*) ''
       STOP
    ENDIF
    IF (nsz1(i) /= nsize_type(i)) THEN
       WRITE (*,*) ''
       WRITE (*,*) '(F) DM_inout/READ_DATA: odd number of sizes in ', TRIM(filename_tmp)
       WRITE (*,*) '                        found', nsz1(i), ' sizes but ', nsize_type(i), ' sizes expected'
       WRITE (*,*) '                        Q, C and G files must have same number of sizes'
       WRITE (*,*) ''
       STOP
    ENDIF
    nsize_type(i) = nsz1(i)

    ! get sizes
    READ (UNIT=fqext%unit, FMT=*) (size_type(i,u), u=1,nsize_type(i))
    IF (n_quiet == 0) THEN
       WRITE(*, FMT='(A20,5x,1P,2(E9.2,1x),1x,I3)') gtype(i), 1.0e-4_dp*size_type(i,1), &
             1.0e-4_dp*size_type(i,(nsize_type(i))), nsize_type(i)
    ENDIF

    ! get Qabs
    the_char = READ_COM (fqext%unit)
    BACKSPACE fqext%unit   ! READ_COM only reads 1st column
    DO k=1,n_qabs
       READ (UNIT=fqext%unit, FMT=*) (q_abs(i,u,k), u=1,nsize_type(i))
    ENDDO

    ! get Qsca
    the_char = READ_COM (fqext%unit)
    BACKSPACE fqext%unit   ! READ_COM only reads 1st column
    DO k=1,n_qabs
       READ (UNIT=fqext%unit, FMT=*) (qdiff(i,u,k), u=1,nsize_type(i))
    ENDDO

    CLOSE (UNIT=fqext%unit)

    IF (n_pdr_o /= 0) THEN
       filename_tmp = TRIMCAT (data_path, dir_qabs)
       filename_tmp = TRIMCAT (filename_tmp, fgfac%nom)
       filename_tmp = TRIMCAT (filename_tmp, gtype(i))
       filename_tmp = TRIMCAT (filename_tmp, '.DAT')
       OPEN (UNIT=fgfac%unit, FILE=filename_tmp, STATUS='old')

       ! read doc
       the_char = READ_COM (fgfac%unit)
       READ (the_char, FMT=*) nsz1(i)

       ! checking G file
       IF (nsz1(i) > nsize_max_qabs) THEN
          WRITE (*,*) ''
          WRITE (*,*) '(F) DM_inout/READ_DATA: too many sizes required in ', TRIM(filename_tmp)
          WRITE (*,*) '                        nsize_max_qabs=', nsize_max_qabs, ' while nsize_type=', nsize_type(i)
          WRITE (*,*) '                        nsize_max_qabs PARAMETER can be changed in DM_utility.f90'
          WRITE (*,*) ''
          STOP
       ENDIF
       IF (nsz1(i) /= nsize_type(i)) THEN
          WRITE (*,*) ''
          WRITE (*,*) '(F) DM_inout/READ_DATA: odd number of sizes in ', TRIM(filename_tmp)
          WRITE (*,*) '                        found', nsz1(i), ' sizes but ', nsize_type(i), ' sizes expected'
          WRITE (*,*) '                        Q, C and G files must have same number of sizes'
          WRITE (*,*) ''
          STOP
       ENDIF
       nsize_type(i) = nsz1(i)

       ! get sizes
       READ (UNIT=fgfac%unit, FMT=*) (size_type(i,u), u=1,nsize_type(i))
       IF (n_quiet == 0) THEN
          WRITE(*, FMT='(A20,5x,1P,2(E9.2,1x),1x,I3)') gtype(i), 1.0e-4_dp*size_type(i,1), &
               1.0e-4_dp*size_type(i,(nsize_type(i))), nsize_type(i)
       ENDIF

       ! get g factors
       the_char = READ_COM (fgfac%unit)
       BACKSPACE fgfac%unit   ! READ_COM only reads 1st column
       DO k=1,n_qabs
          READ (UNIT=fgfac%unit, FMT=*) (gfac(i,u,k), u=1,nsize_type(i))
       ENDDO

       CLOSE (UNIT=fgfac%unit)
    ENDIF

    ! size interpolation (once for all)
    ! mass and g-normalization for each grain type
    DO j=1,nsize(i)
       aux  = 1.e4_dp*size_ava(i,j)
       CALL GET_QEXT (aux, i, tmp1, tmp2, tmp3)
       qi_abs(i,j,:) = tmp1(:)
       qidiff(i,j,:) = tmp2(:)
       gifac(i,j,:)  = tmp3(:)
       ns_i = nsize(i)
       xx(1:ns_i) = si_ava_l(i,1:ns_i)
       yy(1:ns_i) = ava(i,1:ns_i) * rho(i,1:ns_i) * 4.0_dp * xpi / 3.0_dp
       masstot(i) = XINTEG2 (1, ns_i, ns_i, xx(1:ns_i), yy(1:ns_i))
    ENDDO

  ENDDO ! TYPE loop

  ! convert sizes microns --> cm
  size_type(:,:) = size_type(:,:) * 1.0e-4_dp

  ! reading radiation field (ISRF.DAT)
  !-----------------------------------------------------------------
  filename_tmp = TRIMCAT (data_path, dir_dat)
  filename_tmp = TRIMCAT (filename_tmp, fisrf%nom)
  OPEN (UNIT=fisrf%unit, FILE=filename_tmp, STATUS='old')
  the_char = READ_COM (fisrf%unit)
  READ (the_char, FMT=*) nisrf
  ALLOCATE (lambisrf(nisrf))
  ALLOCATE (isrf(nisrf))

  DO k=1,nisrf
     READ (unit=fisrf%unit, FMT=*) lambisrf(k), isrf(k)
!    READ (unit=fisrf%unit, FMT='(E13.6,2x,E13.6)') lambisrf(k), isrf(k)
  ENDDO
  isrf = g0 * isrf

  IF (n_quiet == 0) THEN
     WRITE (*,*)
     WRITE (*, FMT='(A51)') '>>read ISRF.DAT            w-range (microns)  nwave'
     WRITE (*, FMT='(A25,1P,2(E9.2,1x),1x,I4)') '', lambisrf(1), lambisrf(nisrf), nisrf
  ENDIF
  CLOSE (UNIT=fisrf%unit)
  lambisrf = lambisrf * 1.0e-4_dp

  ! ISRF interpolation - extrapolation outside lambisrf bounds is set to 0
  ! beware of flux=0 above lyman limit
  ALLOCATE (isrfuv(n_qabs))
  fl_freq = 0
  jfreqmax = 1
  DO i=1,n_qabs
     isrfuv(i) = INTPOL (isrf, lambisrf, nisrf, lamb_qabs(i))
     IF (fl_freq == 0 .AND. isrfuv(i) < 1.0e-50_dp) THEN
       jfreqmax = i
     ELSE IF (fl_freq == 0 .AND. isrfuv(i) >= 1.0e-50_dp) THEN
       fl_freq = 1
     ENDIF
  ENDDO
  tmp1 = isrfuv
  DO i=1,n_qabs
     isrfuv(i) = tmp1(n_qabs-i+1)
  ENDDO
  jfreqmax = n_qabs -jfreqmax +1
  hnumin = xhp * freq_qabs(1)
  hnumax = xhp * freq_qabs(jfreqmax)

  ! allocate arrays for grain emission
  !-----------------------------------------------------------------
  ALLOCATE (nuinuemtot(n_qabs))
  ALLOCATE (nuinuem(ntype,n_qabs))
  nuinuem = 0.0_dp
  nuinuemtot = 0.0_dp

  DEALLOCATE (lambisrf)
  DEALLOCATE (isrf)
  DEALLOCATE (tmp1,tmp2,tmp3)

END SUBROUTINE READ_DATA

!----------------------------------------------------------------

 FUNCTION READ_COM(file_input) RESULT(line)

! reads comment lines beginning with #
! returns 1st line that is not a comment as
! CHARACTER

  USE CONSTANTS

  IMPLICIT NONE

  INTEGER, INTENT (IN)    :: file_input
  LOGICAL                 :: comment_found
  CHARACTER (LEN=max_len) :: line

  ! -----------------------------------------
  ! Skip comments
  ! -----------------------------------------
  comment_found = .TRUE.
  DO WHILE (comment_found)
     READ (file_input, FMT='(A200)') line
     IF (line(1:1) /= '#') comment_found = .FALSE.
  ENDDO

END FUNCTION READ_COM

!----------------------------------------------------------------

 FUNCTION UPCASE(string) RESULT(upper)
  CHARACTER(LEN=*), INTENT(IN) :: string
  CHARACTER(LEN=len(string))   :: upper
  INTEGER                      :: j

  DO j = 1,len(string)
     IF(string(j:j) >= "a" .AND. string(j:j) <= "z") THEN
        upper(j:j) = achar(iachar(string(j:j)) - 32)
     ELSE
        upper(j:j) = string(j:j)
     ENDIF
  ENDDO
 END FUNCTION UPCASE

!----------------------------------------------------------------

SUBROUTINE WRITE_DATA
! WRITEs DUSTEM outputs integrated over all sizes

  !global variables modules
  USE CONSTANTS
  USE UTILITY
  USE MGET_QEXT
  USE MGET_TDIST
  USE MCOMPUTE

  IMPLICIT NONE

  TYPE(FICH)                  :: fsed   = FICH ('SED.RES', 18)
  TYPE(FICH)                  :: fext   = FICH ('EXT.RES', 22)
  TYPE(FICH)                  :: fpdr_e = FICH ('EXTINCTION_DUSTEM.RES', 32)

  CHARACTER (LEN=max_len)     :: filename_tmp

  INTEGER                     :: i,k
  REAL (KIND=dp), ALLOCATABLE :: abscsuv(:,:)               ! absorption cross-section cm2/gram integrated over sizes
  REAL (KIND=dp), ALLOCATABLE :: diffcsuv(:,:)              ! scattering cross-section cm2/gram integrated over sizes
  REAL (KIND=dp), ALLOCATABLE :: ggfac(:,:)                 ! g-factor for scattering integrated over sizes
  REAL (KIND=dp), ALLOCATABLE :: sdiff(:,:)                 ! weight for scattering integrated over sizes
  REAL (KIND=dp), ALLOCATABLE :: tmp1(:), tmp2(:), tmp3(:), tmp4(:)
  LOGICAL                     :: isthere

 !---------- WRITE THE OUTPUT FILES -----------

  ALLOCATE (abscsuv(ntype,n_qabs))
  ALLOCATE (diffcsuv(ntype,n_qabs))
  ALLOCATE (ggfac(ntype,n_qabs))
  ALLOCATE (sdiff(ntype,n_qabs))
  ALLOCATE (tmp1(n_qabs),tmp2(n_qabs),tmp3(n_qabs),tmp4(n_qabs))

  filename_tmp = TRIMCAT(data_path,dir_res)
  filename_tmp = TRIMCAT(filename_tmp,fsed%nom)
  OPEN  (UNIT=fsed%unit, FILE=filename_tmp, STATUS='unknown')
  WRITE (UNIT=fsed%unit, FMT='(A40)')            '# DUSTEM SED:  4*pi*nu*I_nu/NH (erg/s/H)'
  WRITE (UNIT=fsed%unit, FMT='(A1)')             '#'
  WRITE (UNIT=fsed%unit, FMT='(A14)')            '# Grain types '
  WRITE (UNIT=fsed%unit, FMT='(A34)')            '# nr of grain types   nr of lambda'
  WRITE (UNIT=fsed%unit, FMT='(A52)')            '# lambda (microns)   SED(1)...SED(ntype)   SED total'
  WRITE (UNIT=fsed%unit, FMT='(A1)')             '#'
  WRITE (UNIT=fsed%unit, FMT='(A2,10(A20))')     '# ', (gtype(i), i=1,ntype)
  WRITE (UNIT=fsed%unit, FMT='(I2,2x,I4)')       ntype, n_qabs
  DO k=1,n_qabs
     WRITE (UNIT=fsed%unit, FMT='(1P,25E16.6E3)') lamb_qabs(k)*1.0e4_dp, (nuinuem(i,k), i= 1,ntype), &
                                                      nuinuemtot(k)
  ENDDO
  CLOSE (UNIT=fsed%unit)

! get and write extinction
  DO i=1,ntype
     CALL EXTINCTION (i, tmp1, tmp2, tmp3, tmp4)
     abscsuv(i,:)  = tmp1(:)
     diffcsuv(i,:) = tmp2(:)
     ggfac(i,:) = tmp3(:)
     sdiff(i,:) = tmp4(:)
  ENDDO

  filename_tmp = TRIMCAT(data_path,dir_res)
  filename_tmp = TRIMCAT(filename_tmp,fext%nom)
  OPEN  (UNIT=fext%unit, FILE=filename_tmp, STATUS='unknown')
  WRITE (UNIT=fext%unit, FMT='(A43)')            '# DUSTEM extinction per mass: sigma (cm2/g)'
  WRITE (UNIT=fext%unit, FMT='(A2)')             '# '
  WRITE (UNIT=fext%unit, FMT='(A14)')            '# Grain types '
  WRITE (UNIT=fext%unit, FMT='(A34)')            '# nr of grain types - nr of lambda'
  WRITE (UNIT=fext%unit, FMT='(A60)')            '# lambda (microns)   ABS(1) ... ABS(ntype)  SCA(1) ... SCA(ntype)'
  WRITE (UNIT=fext%unit, FMT='(A2)')             '# '
  WRITE (UNIT=fext%unit, FMT='(A2,50(A20,1x))')  '# ', (gtype(i), i=1,ntype)
  WRITE (UNIT=fext%unit, FMT='(I2,2X,I4)')       ntype, n_qabs
  DO k = 1,n_qabs
     WRITE (UNIT=fext%unit, FMT='(1P,50E15.6E3)') lamb_qabs(k)*1.0e4_dp, (abscsuv(i,n_qabs-k+1), i=1,ntype), &
                                                    (diffcsuv(i,n_qabs-k+1), i=1,ntype)
  ENDDO
  CLOSE (UNIT=fext%unit)

  IF (n_pdr_o /= 0) THEN
  ! write transfer stuff: cross-sections, SED per H atom and mean g-factor
     filename_tmp = TRIMCAT(dir_PDR,fpdr_e%nom)
     INQUIRE (FILE=filename_tmp, EXIST=isthere)
     IF (.NOT. isthere) THEN
        PRINT *, " file:", filename_tmp, " does not exist!"
        PRINT *, " Modify DM_constantes.f90 to set PATH to PDR files"
        STOP
     ENDIF
     OPEN (UNIT=fpdr_e%unit, FILE=filename_tmp, STATUS='unknown')
     WRITE(UNIT=fpdr_e%unit,FMT='(1i5)') n_qabs
     ! 6 columns: Wavelength, Absorption, Scattering, Emission, Albedo, g (Mean of cos(theta))
     DO k = 1,n_qabs
        WRITE (UNIT=fpdr_e%unit, FMT='(1P,50E15.6E3)') lamb_qabs(k)*1.0e4_dp, &
              SUM( (/ (abscsuv(i,n_qabs-k+1)*mprop(i)/Na, i=1,ntype) /) ), &
              SUM( (/ (diffcsuv(i,n_qabs-k+1)*mprop(i)/Na, i=1,ntype) /) ), &
              nuinuemtot(k), &
              SUM( (/ ((diffcsuv(i,n_qabs-k+1))*mprop(i)/Na, i=1,ntype) /) )/ &
              & SUM( (/ ((abscsuv(i,n_qabs-k+1)+diffcsuv(i,n_qabs-k+1))*mprop(i)/Na, i=1,ntype) /) ), &
              SUM( (/ (ggfac(i,k), i=1,ntype) /) ) / SUM( (/ (sdiff(i,k), i=1,ntype) /) )
     ENDDO
     CLOSE (UNIT=fpdr_e%unit)
  ENDIF

  DEALLOCATE (nuinuemtot, nuinuem, isrfuv)
  DEALLOCATE (calor, temp_cal, n_temp, lamb_qabs, freq_qabs, lfrq_qabs, q_abs, qdiff)
  DEALLOCATE (ggfac, sdiff)
  DEALLOCATE (tmp1, tmp2, tmp3, tmp4)
  DEALLOCATE (rho, rhom, gtype, mprop, as2, as1, alpha, t_opt, nsize)
  DEALLOCATE (f_pol, size_ava, ava, f_mix, dloga, sect_eff, si_ava_l, masstot)
  DEALLOCATE (nsize_type, size_type)
  DEALLOCATE (qi_abs)
  DEALLOCATE (qidiff)
  DEALLOCATE (gfac)
  DEALLOCATE (gifac)
  DEALLOCATE (beta0,abeta,gbeta,bmax)
  DEALLOCATE (nbeta,tbeta,betav)
  DEALLOCATE (sig)
  DEALLOCATE (nsz1)
  DEALLOCATE (btresh)
  DEALLOCATE (ltresh,lstiff)

END SUBROUTINE WRITE_DATA

!----------------------------------------------------------------

!************* SPECIFICATIONS****************************
! cette fonction calcule la section efficace pour l'absorption
! absUV par unite de masse de grain de type TYPE(nt),( donc integre sur
! la distribution en taille de ce type de grain ) pour les longueurs
! d'onde UV/Visible. Resultat en cm2/g.
!***********************************************************

SUBROUTINE EXTINCTION (nt, absuv, diffuv, ggfac, sdiff)

  USE CONSTANTS
  USE UTILITY
  USE MGET_QEXT

  IMPLICIT NONE

! in-out arguments
  INTEGER, INTENT (IN)         :: nt                 ! index for grain type
  REAL (KIND=dp), INTENT (OUT) :: absuv(n_qabs)      ! absorption cross-section in cm2/g
  REAL (KIND=dp), INTENT (OUT) :: diffuv(n_qabs)     ! scattering cross-section in cm2/g
  REAL (KIND=dp), INTENT (OUT) :: ggfac(n_qabs)      ! g-factor for scattering
  REAL (KIND=dp), INTENT (OUT) :: sdiff(n_qabs)      ! weight for scattering

! local
  INTEGER                      :: i, ns_i
  REAL (KIND=dp)               :: yy(nsize_max), xx(nsize_max)

!*************** CALCUL **********************

  absuv = 0.0_dp
  diffuv= 0.0_dp
  ggfac = 0.0_dp

! sum over sizes
  DO i=1,n_qabs
    ns_i = nsize(nt)
    xx(1:ns_i) = si_ava_l(nt,1:ns_i)
    yy(1:ns_i) = sect_eff(nt,1:ns_i) * qi_abs(nt,1:ns_i,i) * ava(nt,1:ns_i) * f_mix(nt,1:ns_i) / &
                & size_ava(nt,1:ns_i)**3 / masstot(nt)
    absuv(i)  = XINTEG2 (1, ns_i, ns_i, xx(1:ns_i), yy(1:ns_i))
    yy(1:ns_i) = sect_eff(nt,1:ns_i) * qidiff(nt,1:ns_i,i) * ava(nt,1:ns_i) * f_mix(nt,1:ns_i) / &
                & size_ava(nt,1:ns_i)**3 / masstot(nt)
    diffuv(i) = XINTEG2 (1, ns_i, ns_i, xx(1:ns_i), yy(1:ns_i))
    yy(1:ns_i) = gifac(nt,1:ns_i,i)*sect_eff(nt,1:ns_i)*qidiff(nt,1:ns_i,n_qabs-i+1)*f_mix(nt,1:ns_i)* &
                & ava(nt,1:ns_i) / size_ava(nt,1:ns_i)**3
    ggfac(i)  = XINTEG2 (1, ns_i, ns_i, xx(1:ns_i), yy(1:ns_i))
! NB: qi_abs and qi_diff are reversed wrt lamb_qabs but NOT gifac !!!
    yy(1:ns_i) = sect_eff(nt,1:ns_i)*qidiff(nt,1:ns_i,n_qabs-i+1)*ava(nt,1:ns_i)*f_mix(nt,1:ns_i)/ &
                & size_ava(nt,1:ns_i)**3
    sdiff(i) =  XINTEG2 (1, ns_i, ns_i, xx(1:ns_i), yy(1:ns_i))
  ENDDO

END SUBROUTINE EXTINCTION

END MODULE IN_OUT
