
MODULE MCOMPUTE

  USE CONSTANTS
  IMPLICIT NONE

  PRIVATE
  PUBLIC :: COMPUTE

CONTAINS

! ------------------------------------------------------------------------------------

SUBROUTINE COMPUTE

  ! global variables modules

  USE CONSTANTS
  USE UTILITY
  USE MGET_QEXT
  USE MGET_TDIST
  USE MSPIN

  !------- VARIABLES LOCALES

  IMPLICIT NONE

  TYPE (FICH)                 :: ftemp   = FICH('TEMP.RES',21)
  TYPE (FICH)                 :: fsed_a  = FICH('SED_A.RES',31)
  TYPE (FICH)                 :: fspd_a  = FICH('SPIN_A.RES',33)
  CHARACTER (LEN=max_len)     :: filename_tmp

  ! loop indices
  INTEGER                     :: i             ! grain type index
  INTEGER                     :: j             ! grain size index
  INTEGER                     :: kt

  REAL (KIND=dp)              :: enerabs(nsize_max)  ! l'energie totale absorbee par le grain
  REAL (KIND=dp)              :: enerem(nsize_max)   ! l'energie totale emise par le grain
  REAL (KIND=dp), ALLOCATABLE :: tempmoy(:,:)        ! S : la temperature moyenne
  REAL (KIND=dp), ALLOCATABLE :: tempequi(:,:)       ! S : la temperature d'equilibre thermique
  REAL (KIND=dp)              :: tempmax(nsize_max)  ! S : la temperature maximale
  REAL (KIND=dp)              :: t_rot(nsize_max)    ! rotational temperature

  REAL (KIND=dp)              :: temp(ndist)   ! S : T et dP/dlnT
  REAL (KIND=dp)              :: t2(ndist)     ! S : T et dP/dlnT
  REAL (KIND=dp)              :: hcap(ndist)   ! S : T et dP/dlnT
  REAL (KIND=dp)              :: dpt(ndist)    ! S : T et dP/dlnT
  REAL (KIND=dp)              :: uint(ndist)   ! S : T et dP/dlnT
  REAL (KIND=dp)              :: dpu(ndist)    ! S : T et dP/dlnT

  ! for dust IR (vibrational) SED
  REAL (KIND=dp), ALLOCATABLE :: nuflux_j(:)   ! flux*frequency for 1 type and 1 size
  REAL (KIND=dp), ALLOCATABLE :: nuflux(:,:)   ! flux*frequency for 1 type and all sizes
  REAL (KIND=dp), ALLOCATABLE :: nufluxgr(:,:) ! flux*frequency for all types summed over sizes

  ! for spinning dust SED
  REAL (KIND=dp), ALLOCATABLE :: nuf(:)
  REAL (KIND=dp), ALLOCATABLE :: spnuflux_j(:)  ! spinning dust, flux*frequence
  REAL (KIND=dp), ALLOCATABLE :: spnuflux(:,:)  ! flux * frequence

  ! JLB - Grand menage - 1 X 2009
  INTEGER                     :: ns_i
  REAL (KIND=dp)              :: xx(nsize_max), yy(nsize_max)       ! Integrande
  REAL (KIND=dp)              :: bnorm                              ! Normalisation factor for mean

  !-------------------------------------------------------------------------

  ! allocate IR SED arrays 
  ALLOCATE (nuflux_j(n_qabs))
  ALLOCATE (nuflux(nsize_max,n_qabs))
  ALLOCATE (nufluxgr(ntype,n_qabs))

  ! allocate spinning arrays
  ALLOCATE (spnuflux_j(n_qabs))
  ALLOCATE (spnuflux(nsize_max, n_qabs))

  IF (n_ftemp > 0) THEN
     filename_tmp = TRIMCAT(data_path,dir_res)
     filename_tmp = trimcat(filename_tmp,ftemp%nom)
     OPEN (UNIT=ftemp%unit, FILE = filename_tmp, STATUS = 'unknown')
     WRITE (UNIT=ftemp%unit, FMT='(A40)')'# DUSTEM: grain temperature distribution'
     WRITE (UNIT=ftemp%unit, FMT='(A1)') '#'
     WRITE (UNIT=ftemp%unit, FMT='(A119)') &
     & '# TYPE                   size(cm)          Teq(K)              Tmoy(K)            Tmax(K)        Trot(K)          ndist'
     WRITE (UNIT=ftemp%unit, FMT='(A87)') &
     & '#       T(K)             dP/dlnT           C(T)(erg/K)         U(erg)             dP/dU'
     WRITE (UNIT=ftemp%unit, FMT='(A1)') '#'
  ENDIF

  IF (n_res_a > 0) THEN
     filename_tmp = TRIMCAT(data_path, dir_res)
     filename_tmp = trimcat(filename_tmp,fsed_a%nom)
     OPEN (UNIT = fsed_a%unit, FILE = filename_tmp, STATUS = 'unknown')
     WRITE (UNIT = fsed_a%unit, FMT='(A55)') '# DUSTEM: grain SED per size 4*pi*nu*I_nu (erg/cm2/s/H)'
     WRITE (UNIT = fsed_a%unit, FMT='(A1)')  '#'
     WRITE (UNIT = fsed_a%unit, FMT='(A20)') '# TYPE  nr of sizes   nr of waves'
     WRITE (UNIT = fsed_a%unit, FMT='(A23)') '# size(1)...size(nsize) (cm)'
     WRITE (UNIT = fsed_a%unit, FMT='(A22)') '# lambda(microns) SED(1)....SED(nsize)'
     WRITE (UNIT = fsed_a%unit, FMT='(A1)')  '#'
  ENDIF


  IF (n_spin > 0) THEN
     filename_tmp = TRIMCAT(data_path, dir_res)
     filename_tmp = trimcat(filename_tmp,fspd_a%nom)
     OPEN (UNIT = fsed_a%unit, FILE = filename_tmp, STATUS = 'unknown')
     WRITE (UNIT = fsed_a%unit, FMT='(A55)') '# DUSTEM: grain spinning SED per type 4*pi*nu*I_nu (erg/cm2/s/H)'
     WRITE (UNIT = fsed_a%unit, FMT='(A1)')  '#'
     WRITE (UNIT = fsed_a%unit, FMT='(A20)') '# nr of types   nr of waves'
     WRITE (UNIT = fsed_a%unit, FMT='(A20)') '# TYPES'
     WRITE (UNIT = fsed_a%unit, FMT='(A22)') '# lambda(microns) SED(1)....SED(ntype)'
     WRITE (UNIT = fsed_a%unit, FMT='(A1)')  '#'
  ENDIF

  ALLOCATE (tempmoy(ntype,nsize_max))
  ALLOCATE (tempequi(ntype,nsize_max))

  ! compute and write
  DO i=1,ntype                                     ! loop on grain types

     nufluxgr(i,:) = 0.0_dp
     t_rot(:) = 0.0_dp
     ns_i = nsize(i)
     n_beta = 0
     IF (INDEX(t_opt(i),'BETA') > 0) n_beta = 1 

     DO j=1,nsize(i)                               ! loop on sizes

        CALL GET_TDIST( &
             & i,             &                    ! (I): index of grain TYPE
             & j,             &                    ! (I): index of grain size
             & size_ava(i,j), &                    ! (I): grain size
             & enerabs(j),    &                    ! (O): power absorbed by grain
             & uint,          &                    ! (O): internal energy grid
             & dpu,           &                    ! (O): dP/dU
             & temp,          &                    ! (O): grain temperature T
             & dpt,           &                    ! (O): dP/dln T
             & hcap,          &                    ! (O): grain heat capacity vs temp
             & tempmoy(i,j),  &                    ! (O): average temperature 
             & tempequi(i,j), &                    ! (O): equilibrium temperature 
             & tempmax(j) )                        ! (O): max temperature 

        t2(:) = LOG(temp(:))
        CALL GET_ASED (i,j,size_ava(i,j),ndist,temp(1:ndist),t2(1:ndist),dpt(1:ndist),nuflux_j(:),enerem(j))
        nuflux(j,:) = nuflux_j(:)

        IF (INDEX(t_opt(i),'SPIN') > 0 ) THEN
           ALLOCATE (nuf(n_qabs))
           nuf(:) = nuflux_j(:) * enerabs(j)/enerem(j) / 4.0_dp/xpi
           DO kt=1,n_qabs 
              nuf(kt) = nuflux_j(n_qabs-kt+1) * enerabs(j)/enerem(j) / 4.0_dp/xpi / freq_qabs(kt)
           ENDDO
           CALL SPIN(i, j, size_ava(i,j), tempequi(i,j), nuf, spnuflux_j, t_rot(j))
           DEALLOCATE (nuf)
           spnuflux(j,:) = spnuflux_j(:)
           nuflux(j,:) = nuflux(j,:) + spnuflux(j,:)
        ENDIF

        IF (n_ftemp > 0) THEN
           WRITE (UNIT = ftemp%unit, FMT='(/,A2,A20,1x,5(1PE12.4,6X),i4)') '# ',gtype(i),size_ava(i,j),tempequi(i,j), &
                & tempmoy(i,j),tempmax(j),t_rot(j),ndist
           DO kt = 1, ndist
              WRITE (UNIT = ftemp%unit, FMT='(1P,5(E18.10E3,1X))') temp(kt), dpt(kt), hcap(kt), uint(kt), dpu(kt)
           ENDDO
        ENDIF

     ENDDO    ! end of size loop (j)

     xx(1:ns_i) = si_ava_l(i,1:ns_i)
     DO kt=1,n_qabs
       yy(1:ns_i) = ( nuflux(1:ns_i,kt)/ size_ava(i,1:ns_i)**3 ) &
                  * (enerabs(1:ns_i) / enerem(1:ns_i)) &
                  * f_mix(i,1:ns_i) * ava(i,1:ns_i)
       nufluxgr(i,kt) = XINTEG2 (1, ns_i, ns_i, xx(1:ns_i), yy(1:ns_i))
     ENDDO
     nufluxgr(i,:) = nufluxgr(i,:) / masstot(i)

     IF (n_res_a > 0) THEN
        WRITE(UNIT = fsed_a%unit, FMT='(A2,a20,1x,i3,1X,I4)') '# ', gtype(i), nsize(i), n_qabs
        WRITE(UNIT = fsed_a%unit, FMT='(100(1PE12.4,1X))')  (size_ava(i,j), j=1,nsize(i))
        DO kt=1,n_qabs
           WRITE(UNIT = fsed_a%unit, FMT='(101(1PE14.6E3,1X))') lamb_qabs(kt)*1.e4_dp, (nuflux(j,kt), j=1,nsize(i))
        ENDDO
     ENDIF

  ! get SED in erg/s/H (4*pi*nu*inu/NH)
     nuinuem(i,:) = nufluxgr(i,:) * mprop(i) / Na
     nuinuemtot = nuinuemtot + nuinuem(i,:)

  ENDDO  ! end type loop (i)

  ! Compute mean temperature as a function of size (for PDR code)
  IF (n_pdr_o /= 0) THEN
     filename_tmp = TRIMCAT(dir_PDR,"T_MEAN.RES")
     OPEN (10, FILE=filename_tmp, STATUS="unknown")
     WRITE (10,*) ntype
     WRITE (10,*) (nsize(i), i = 1, ntype)
     WRITE (10,'("#Type j   Size    B*ava     Teq    Tmoy")')
     DO i=1,ntype
        bnorm = mprop(i) * (4.0_dp+alpha(i)) / (ava(i,nsize(i)) - ava(i,1))
        DO j=1,nsize(i)
           WRITE (10,'(2i4,1p,6e15.6)') i, j, size_ava(i,j), bnorm*ava(i,j), tempequi(i,j), tempmoy(i,j)
        ENDDO
        WRITE (10,'(" ")')
     ENDDO
     CLOSE (10)
  ENDIF

  DEALLOCATE(nuflux_j)
  DEALLOCATE(nuflux)
  DEALLOCATE(nufluxgr)
  DEALLOCATE(spnuflux_j)
  DEALLOCATE(spnuflux)
  DEALLOCATE(tempmoy)
  DEALLOCATE(tempequi)

  IF (n_ftemp > 0) CLOSE (UNIT = ftemp%unit)
  IF (n_res_a > 0) CLOSE (UNIT = fsed_a%unit)

END SUBROUTINE COMPUTE

! -------------------------------------------------------------------------------

SUBROUTINE GET_ASED (nt, ns, a, ntsed, t, ti, p, xnufnu, energiemise)

! computes the SED (erg/s) emitted by a grain of given size and with  
! temperature distribution p (usually dp/dlnT). 
! Temperature scale for integration is ti (usually LOG(T))
! uses lambda*B_lambda(T)

  USE CONSTANTS
  USE UTILITY
  USE MGET_QEXT

  IMPLICIT NONE

  INTEGER, INTENT (IN)         :: nt               ! index of grain type
  INTEGER, INTENT (IN)         :: ns               ! index of grain size
  INTEGER, INTENT (IN)         :: ntsed            ! nr of T-values
  REAL (KIND=dp), INTENT (IN)  :: a                ! grain size
  REAL (KIND=dp), INTENT (IN)  :: t(ntsed)
  REAL (KIND=dp), INTENT (IN)  :: ti(ntsed)
  REAL (KIND=dp), INTENT (IN)  :: p(ntsed)
  REAL (KIND=dp), INTENT (OUT) :: xnufnu(n_qabs)
  REAL (KIND=dp), INTENT (OUT) :: energiemise

  INTEGER        :: l, ktemp
  REAL (KIND=dp) :: al1
  REAL (KIND=dp) :: fnusom
  REAL (KIND=dp) :: fnut(ntsed), xx(ntsed)
  REAL (KIND=dp) :: yy(n_qabs)
  REAL (KIND=dp) :: coef

  coef = xpi * a**2 * cte1

  ! main loop on wavelengths
  energiemise = 0.0_dp
  IF (n_beta == 0) THEN
     DO l=1,n_qabs
        al1 = lamb_qabs(l)
        ! temperature loop
        xx(:) = hcsurk / (al1 * t(:))
        DO ktemp=1,ntsed
           fnut(ktemp) = p(ktemp) * F_BB (xx(ktemp))
        ENDDO
        fnusom = XINTEG2 (1, ntsed, ntsed, ti, fnut)
        ! qauv is backward...
        xnufnu(l) = coef * fnusom * qi_abs(nt,ns,n_qabs-l+1) / al1**4    
     ENDDO
  ELSE      ! apply BETA(T)
     DO l=1,n_qabs
        al1 = lamb_qabs(l)
        ! temperature loop
        xx(:) = hcsurk / (al1 * t(:))
        DO ktemp=1,ntsed
           fnut(ktemp) = p(ktemp) * F_BB (xx(ktemp)) * &
           ! faster
           & EXP( DBETA(t(ktemp),nt)*btresh(nt,l)*LOG(ltresh(nt)/al1) )
        ENDDO
        fnusom = XINTEG2 (1, ntsed, ntsed, ti, fnut)
        ! qauv is backward...
        xnufnu(l) = coef * fnusom * qi_abs(nt,ns,n_qabs-l+1) / al1**4 
     ENDDO
  ENDIF

  yy(:) = xnufnu(:) / lamb_qabs(:)
  l = 1
  energiemise = XINTEG2 (l, n_qabs, n_qabs, lamb_qabs, yy)

END SUBROUTINE GET_ASED

END MODULE MCOMPUTE
