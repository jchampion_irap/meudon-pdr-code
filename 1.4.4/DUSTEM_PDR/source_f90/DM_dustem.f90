
! DUSTEM - Dust Emissivity
! Copyright: F-X Desert et al. 
! See: Desert et al., 1986, A&A 160, 295 for the theory
! New presentation and rewriting
! 13 X 2009 - by Laurent Verstraete and Jacques Le Bourlot

PROGRAM DUSTEM

  USE CONSTANTS
  USE UTILITY
  USE MCOMPUTE
  USE IN_OUT

  IMPLICIT NONE

  REAL (KIND=dp) t1

  ! WRITE (*,*) '--> Reading...'
  CALL READ_DATA

  ! WRITE (*,*) '--> WORKING...'
  CALL COMPUTE

  ! WRITE (*,*) '--> writing...'
  CALL WRITE_DATA

  CALL CPU_TIME (t1)

  IF (n_quiet == 0) THEN
     WRITE (*,*) ''
     WRITE (*,FMT='(A20,1PE8.2,A4)')'Exit OK - CPU Time ', t1,' sec'
     WRITE (*,*) '==========================================='
  ENDIF

END PROGRAM DUSTEM
