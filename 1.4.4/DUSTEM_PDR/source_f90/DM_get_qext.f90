
MODULE MGET_QEXT

  USE CONSTANTS
  IMPLICIT NONE

  PRIVATE

  PUBLIC :: GET_QEXT, COOLING, COOL1, DCOOL1, DBETA

CONTAINS

SUBROUTINE GET_QEXT(a, nt, q_ab, q_di, g_fa)
!     gets Qabs and Qsca of grains

  USE CONSTANTS
  USE UTILITY

  IMPLICIT NONE

  ! arguments
  INTEGER, INTENT (IN)         :: nt
  REAL (KIND=dp), INTENT (IN)  :: a
  REAL (KIND=dp), INTENT (OUT) :: q_ab(n_qabs)
  REAL (KIND=dp), INTENT (OUT) :: q_di(n_qabs)
  REAL (KIND=dp), INTENT (OUT) :: g_fa(n_qabs)

  INTEGER                      :: i
  REAL (KIND=dp)               :: sizemax(1), sizemin(1)
  REAL (KIND=dp), ALLOCATABLE  :: qabs_tmp(:,:)
  REAL (KIND=dp), ALLOCATABLE  :: size_tmp(:)
  REAL (KIND=dp), ALLOCATABLE  :: qdiff_tmp(:,:)
  REAL (KIND=dp), ALLOCATABLE  :: gfac_tmp(:,:)
  INTEGER                      :: nsize_tmp
  REAL (KIND=dp), ALLOCATABLE  :: tempa(:), tempd(:), tempg(:)

!============================================================================

  nsize_tmp = nsize_type(nt)
  ALLOCATE (qabs_tmp(nsize_tmp, n_qabs))
  ALLOCATE (size_tmp(nsize_tmp))
  ALLOCATE (qdiff_tmp(nsize_tmp, n_qabs))
  ALLOCATE (gfac_tmp(nsize_tmp, n_qabs))
  ALLOCATE (tempa(n_qabs))
  ALLOCATE (tempd(n_qabs))
  ALLOCATE (tempg(n_qabs))
  size_tmp       = size_type(nt, 1:nsize_tmp)
  qabs_tmp(:,:)  = q_abs(nt, 1:nsize_tmp, :)
  qdiff_tmp(:,:) = qdiff(nt, 1:nsize_tmp, :)
  gfac_tmp(:,:)  = gfac(nt, 1:nsize_tmp, :)

  ! check that size exist in Q_TYPE.DAT
  sizemax = size_tmp(MAXLOC(size_tmp))
  sizemin = size_tmp(MINLOC(size_tmp))
  IF (a > sizemax(1) .OR. a < sizemin(1) ) THEN
     WRITE (*,*) ''
     WRITE (*,*) '(F) DM_get_qext/GET_EXT: size out of range available in Q and G files'
     WRITE (*,*) '                         for grain type ', TRIM(gtype(nt))
     WRITE (*,*) '                         min size (microns)      = ', sizemin(1)*1.0e4_dp
     WRITE (*,*) '                         max size (microns)      = ', sizemax(1)*1.0e4_dp
     WRITE (*,*) '                         required size (microns) = ', a*1.0e4_dp
     WRITE (*,*) ''
     STOP
  ENDIF

  ! interpolate Q to size a -> q_ab(n_qabs), q_di(n_qabs)
  IF (a == size_tmp(nsize_tmp)) THEN
     q_ab(:) = qabs_tmp(nsize_tmp,:)
     q_di(:) = qdiff_tmp(nsize_tmp,:)
  ELSE
     DO i = 1, n_qabs
        q_ab(i) = INTPOL ( qabs_tmp(:,i), size_tmp(:), nsize_tmp, a )
        q_di(i) = INTPOL ( qdiff_tmp(:,i), size_tmp(:), nsize_tmp, a )
        g_fa(i) = INTPOL ( gfac_tmp(:,i), size_tmp(:), nsize_tmp, a )
     ENDDO
  ENDIF
  tempa = q_ab
  tempd = q_di
  tempg = g_fa
  DO i=1,n_qabs
     q_ab(i) = tempa(n_qabs-i+1)
     q_di(i) = tempd(n_qabs-i+1)
  ENDDO

  DEALLOCATE (qabs_tmp)
  DEALLOCATE (size_tmp)
  DEALLOCATE (qdiff_tmp)
  DEALLOCATE (gfac_tmp)
  DEALLOCATE (tempa, tempd, tempg)

END SUBROUTINE GET_QEXT

!---------------------------------------------------------------------

SUBROUTINE COOLING (nt, t, f, nn)

! cooling power (erg/s) emitted by a grain at given temperature t
! NB uses BB_lambda(T)

  USE CONSTANTS
  USE UTILITY

  IMPLICIT NONE

  INTEGER, INTENT (IN)         :: nt   ! index of grain type
  INTEGER, INTENT (IN)         :: nn   ! nr of T-values
  REAL (KIND=dp), INTENT (IN)  :: t(nn)
  REAL (KIND=dp), INTENT (OUT) :: f(nn)

  INTEGER                      :: l, ktemp
  REAL (KIND=dp)               :: energiemise
  REAL (KIND=dp)               :: xx(n_qabs), fnut(n_qabs)

  IF (n_beta == 0) THEN
     ! temperature loop
     DO ktemp=1,nn
        ! frequency loop
        xx(:) = hcsurk / (t(ktemp) * lamb_qabs(:))
        DO l=1,n_qabs
           fnut(l) = F_BB (xx(l)) * qauv(n_qabs-l+1) / lamb_qabs(l)**5
        ENDDO
        energiemise = XINTEG2(1, n_qabs, n_qabs, lamb_qabs, fnut)
        f(ktemp) = cte1 * energiemise
     ENDDO
  ELSE      ! apply BETA(T)
     ! temperature loop
     DO ktemp=1,nn
        ! frequency loop
        xx(:) = hcsurk / (t(ktemp) * lamb_qabs(:))
        DO l=1,n_qabs
           fnut(l) = F_BB (xx(l)) * qauv(n_qabs-l+1) * & 
           ! faster
           & EXP( DBETA(t(ktemp),nt)*btresh(nt,l)*LOG(ltresh(nt)/lamb_qabs(l)) ) / &
           & lamb_qabs(l)**5
        ENDDO
        energiemise = XINTEG2(1, n_qabs, n_qabs, lamb_qabs, fnut)
        f(ktemp) = cte1 * energiemise
     ENDDO
  ENDIF

END SUBROUTINE COOLING

!---------------------------------------------------------------------

FUNCTION COOL1 (tt, nt)

! cooling power (erg/s) emitted by a grain at given temperature t
! used to find Tequil
! NB uses BB_nu(T)

  USE CONSTANTS
  USE UTILITY

  IMPLICIT NONE

  INTEGER, INTENT (IN)         :: nt      ! index of grain type
  REAL (KIND=dp), INTENT (IN)  :: tt
  REAL (KIND=dp)               :: COOL1

  INTEGER                      :: l
  REAL (KIND=dp)               :: xx(n_qabs), fnut(n_qabs)

  ! loop on frequencies
  xx(:) = xhp * freq_qabs(:) / (xkb * tt)
  IF (n_beta == 0) THEN
     DO l=1,n_qabs
        fnut(l) = F_BB (xx(l)) * qauv(l) * freq_qabs(l)**3 
     ENDDO
  ELSE     ! apply BETA(T)
     DO l=1,n_qabs
        fnut(l) = F_BB (xx(l)) * qauv(l) * freq_qabs(l)**3 * &
        ! faster
        & EXP( DBETA(tt,nt)*btresh(nt,n_qabs-l+1)*LOG(freq_qabs(l)*ltresh(nt)/clight) )
     ENDDO
  ENDIF

  COOL1 = cte2 * XINTEG2(1, n_qabs, n_qabs, freq_qabs, fnut)

END FUNCTION COOL1

!---------------------------------------------------------------------

FUNCTION DCOOL1 (tt, nt)

! derivative of COOL1: used to find Tequil

  USE CONSTANTS
  USE UTILITY

  IMPLICIT NONE

  INTEGER, INTENT (IN)         :: nt   ! index of grain type
  REAL (KIND=dp), INTENT (IN)  :: tt
  REAL (KIND=dp)               :: DCOOL1

  INTEGER                      :: l
  REAL (KIND=dp)               :: xx(n_qabs), fnut(n_qabs)

  ! loop on frequencies
  xx(:) = xhp  * freq_qabs(:) / (xkb * tt)
  IF (n_beta == 0) THEN
     DO l=1,n_qabs
        fnut(l) = G_BB (xx(l)) * qauv(l) * freq_qabs(l)**4
     ENDDO
  ELSE   ! apply BETA(T)
     DO l=1,n_qabs
        fnut(l) = G_BB (xx(l)) * qauv(l) * freq_qabs(l)**4 * &
        ! faster
        & EXP( DBETA(tt,nt)*btresh(nt,n_qabs-l+1)*LOG(freq_qabs(l)*ltresh(nt)/clight) )
     ENDDO
  ENDIF

  DCOOL1 = cte2 * XINTEG2(1, n_qabs, n_qabs, freq_qabs, fnut) * 0.5_dp * (xhp / xkb) / tt**2

END FUNCTION DCOOL1

!---------------------------------------------------------------------

FUNCTION DBETA (tt, nt)

! correction to generate a beta(T) behaviour of Qabs above some lambda(nu) threshold
!
! Q(nu)=Q0(nu)*(nu/nutresh)**(DBETA(T)*BTRESH(nu,nutresh)) 
! with BETA(T) = BETA0 + DBETA(T)
!

  USE CONSTANTS
  USE UTILITY

  IMPLICIT NONE

  INTEGER, INTENT (IN)         :: nt   ! index of grain type
  REAL (KIND=dp), INTENT (IN)  :: tt
  REAL (KIND=dp)               :: DBETA

  INTEGER                      :: jlo
  REAL (KIND=dp)               :: tmp, bm

  if (nbeta(nt) == 0) THEN
     bm = -beta0(nt) + bmax(nt)
     !  tmp  = -beta0(nt) + abeta(nt) * tt**(gbeta(nt))
     tmp  = -beta0(nt) + abeta(nt) * EXP(gbeta(nt)*LOG(tt))  ! faster
     IF (tmp > bm) tmp = bm
  ELSE 
     jlo = 1
     ! constant at edges (extrapolation)
     tmp = -beta0(nt) + INTPOL2( betav(nt,1:nbeta(nt)), tbeta(nt,1:nbeta(nt)), nbeta(nt), tt )    
  ENDIF

  DBETA = tmp

END FUNCTION DBETA

!---------------------------------------------------------------------

END MODULE MGET_QEXT
